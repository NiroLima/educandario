<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);
if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
                            FROM mod_acolhimento ma   
                            WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];

//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result2->bindValue(1, $acolhimento_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_id = $resultado2['id'];

//------------------------------------------------------------------------------
    $result3 = $db->prepare("SELECT *      
                            FROM mod_acolhimento_crianca_educacao mace   
                            WHERE mace.acolhimento_crianca_id = ?");
    $result3->bindValue(1, $acolhimento_crianca_id);
    $result3->execute();
    $resultado3 = $result3->fetch(PDO::FETCH_ASSOC);

    $educacao_id = $resultado3['id'];

    $justique_ausencia = $resultado3['justique_ausencia'];

    $justifique = $resultado3['justifique'];
    $justifique2 = $resultado3['justifique2'];

    $idade_escolar = $resultado3['idade_escolar'];
    $idade_escolar_atual = $resultado3['idade_escolar_atual'];
    $escola = $resultado3['escola'];
    $serie = $resultado3['serie'];
    $turno = $resultado3['turno'];
    $frequenta_instituicao = $resultado3['frequenta_instituicao'];
    $justificacao_ausencia = $resultado3['justificacao_ausencia'];

    $frequencia = $resultado3['frequencia'];
    $sobre_frequencia = $resultado3['sobre_frequencia'];
    $socializacao = $resultado3['socializacao'];
    $sobre_socializacao = $resultado3['sobre_socializacao'];
    $interesse = $resultado3['interesse'];
    $sobre_interesse = $resultado3['sobre_interesse'];
    $rendimento = $resultado3['rendimento'];
    $sobre_rendimento = $resultado3['sobre_rendimento'];
    $participacao = $resultado3['participacao'];
    $sobre_participacao = $resultado3['sobre_participacao'];

    $apresenta_desenvolvimento = $resultado3['apresenta_desenvolvimento'];
    $descricao_aspectos = $resultado3['descricao_aspectos'];

    $contexto_geral = $resultado3['contexto_geral'];
    $relacao_escola = $resultado3['relacao_escola'];
    $descricao_relacao_escola = $resultado3['descricao_relacao_escola'];
    $descricao_intervencoes = $resultado3['descricao_intervencoes'];

    $espaco_estrutura = $resultado3['espaco_estrutura'];
    $rotinas = $resultado3['rotinas'];
    $acolhimento_responsavel = $resultado3['responsavel_id'];
    $vf = $resultado3['vf'];
    $desenvolvimento_tipico = "";
    $atividade_complementar = "";
} else {
    $acolhimento_id = "";
    $justique_ausencia = "";
    $justifique = "";
    $justifique2 = "";
    $acolhimento_crianca_id = "";
    $educacao_id = "";
    $idade_escolar = "";
    $idade_escolar_atual = "";
    $escola = "";
    $serie = "";
    $turno = "";
    $frequenta_instituicao = "";
    $justificacao_ausencia = "";
    $frequencia = "";
    $sobre_frequencia = "";
    $socializacao = "";
    $sobre_socializacao = "";
    $interesse = "";
    $sobre_interesse = "";
    $rendimento = "";
    $sobre_rendimento = "";
    $participacao = "";
    $sobre_participacao = "";
    $apresenta_desenvolvimento = "";
    $descricao_aspectos = "";
    $contexto_geral = "";
    $relacao_escola = "";
    $descricao_relacao_escola = "";
    $descricao_intervencoes = "";
    $espaco_estrutura = "";
    $rotinas = "";
    $acolhimento_responsavel = $_SESSION['id'];
    $vf = 0;
    $desenvolvimento_tipico = "";
    $atividade_complementar = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/educacao/lista">Educação</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_consulta" name="form_consulta" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $consulta_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-clipboard-user"></i> <strong>NOVO PLANO EDUCACIONAL</strong></h4>
                    </div>
                    <div class="box-body">
                        <!-- TRIAGEM -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>TRIAGEM</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-10">
                                        <div id="div_nome_educando" class="form-group">
                                            <label for="nome_educando">NOME DO EDUCANDO</label>
                                            <select name="nome_educando" id="nome_educando" class="form-control select2">
                                                <option rel2="" value="">Selecione o Educando</option>
                                                <option value="">ILDENIRO DE OLIVEIRA LIMA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="div_idade_educando" class="form-group">
                                            <label for="idade_educando">IDADE</label>
                                            <input type="text" class="form-control" name="idade_educando" id="idade_educando" placeholder="Idade" value="" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row mt-3">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="genero">CRIANÇA EM IDADE ESCOLAR?</label>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <input type="radio" <?= $idade_escolar == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_idade_escolar" id="sim_idade_escolar" value="1" disabled />
                                                    <label for="sim_idade_escolar">SIM</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="radio" <?= $idade_escolar == "" || $idade_escolar == 0 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_idade_escolar" id="nao_idade_escolar" value="0" disabled />
                                                    <label for="nao_idade_escolar">NÃO</label>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                                <div id="div_escolar" class="row mt-3">
                                    <div class="col-md-4">
                                        <div id="div_escola" class="form-group">
                                            <label for="escola">ESCOLA</label>
                                            <input type="text" class="form-control" name="escola" id="escola" placeholder="Nome da escola" value="<?= $escola; ?>" disabled />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_serie" class="form-group">
                                            <label for="serie">SÉRIE</label>
                                            <input type="text" class="form-control" name="serie" id="serie" placeholder="Série" value="<?= $serie; ?>" disabled />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_turno" class="form-group">
                                            <label for="turno">TURNO</label>
                                            <input type="text" class="form-control" name="turno" id="turno" placeholder="Turno" value="<?= $turno; ?>" disabled />
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_desenvolvimento_tipico" class="form-group">
                                            <label for="desenvolvimento_tipico">A CRIANÇA APRESENTA DESENVOLVIMENTO TÍPICO?</label>
                                            <textarea name="desenvolvimento_tipico" id="desenvolvimento_tipico" class="form-control" cols="30" rows="10"><?= $desenvolvimento_tipico; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_atividade_complementar" class="form-group">
                                            <label for="atividade_complementar">QUAL A ATIVIDADE COMPLEMENTAR A CRIANÇA TEM PREFERÊNCIA?</label>
                                            <textarea name="atividade_complementar" id="atividade_complementar" class="form-control" cols="30" rows="10"><?= $atividade_complementar; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_avaliacao_inicial" class="form-group">
                                            <label for="avaliacao_inicial">AVALIAÇÃO INICIAL</label>
                                            <textarea name="avaliacao_inicial" id="avaliacao_inicial" class="form-control" cols="30" rows="10"><?= $atividade_complementar; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM TRIAGEM -->


                        <!-- BOX DE PROJETOS DE EDUCACAO CUTURA E LAZER -->
                        <div class="box box-outline-primary mt-30">
                            <div class="box-header">
                                <strong>PROJETOS DE EDUCAÇÃO, CULTURA E LAZER</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-3">
                                        <input type="checkbox" id="estimulacao_precoce" name="estimulacao_precoce" class="filled-in chk-col-info" value="1" />
                                        <label for="estimulacao_precoce">ESTIMULAÇÃO PRECOCE</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="recreacao" name="recreacao" class="filled-in chk-col-info" value="1" />
                                        <label for="recreacao">RECREAÇÃO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="estimulacao" name="estimulacao" class="filled-in chk-col-info" value="1" />
                                        <label for="estimulacao">ESTIMULAÇÃO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="passeio_recreativo" name="passeio_recreativo" class="filled-in chk-col-info" value="1" />
                                        <label for="passeio_recreativo">PASSEIO RECREATIVO CUTURAL</label>
                                    </div>


                                    <div class="col-md-3 mt-10">
                                        <input type="checkbox" id="leitura" name="leitura" class="filled-in chk-col-info" value="1" />
                                        <label for="leitura">LEITURA</label>
                                    </div>
                                    <div class="col-md-3 mt-10">
                                        <input type="checkbox" id="reforco" name="reforco" class="filled-in chk-col-info" value="1" />
                                        <label for="reforco">REFORÇO</label>
                                    </div>
                                    <div class="col-md-3 mt-10">
                                        <input type="checkbox" id="alfabetizacao" name="alfabetizacao" class="filled-in chk-col-info" value="1" />
                                        <label for="alfabetizacao">ALFABETIZAÇÃO</label>
                                    </div>
                                    <div class="col-md-3 mt-10">
                                        <input type="checkbox" id="atividades_complementares" name="atividades_complementares" class="filled-in chk-col-info" value="1" />
                                        <label for="atividades_complementares">ATIVIDADES PEDAGÓGICAS COMPLEMENTARES</label>
                                    </div>

                                    <div class="col-md-3 mt-10">
                                        <input type="checkbox" id="apd" name="apd" class="filled-in chk-col-info" value="1" />
                                        <label for="apd">APD</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM BOX DE PROJETOS DE EDUCACAO CUTURA E LAZER -->

                        <!-- MONITORAMENTO DO DESEMPENHO / PARTICIPAÇÃO DA CRIANÇA NO(S) PROJETO(S) -->
                        <div class="box box-outline-primary mt-30">
                            <div class="box-header">
                                <strong>MONITORAMENTO DO DESEMPENHO / PARTICIPAÇÃO DA CRIANÇA NO(S) PROJETO(S)</strong>
                            </div>
                            <div class="box-body">
                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> ESTIMULAÇÃO PRECOCE</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="div_monitoramento_educando" class="form-group">
                                                    <label for="monitoramento_educando">DESCREVA</label>
                                                    <textarea name="monitoramento_educando" id="monitoramento_educando" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> RECREAÇÃO</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div id="div_recreacao" class="form-group">
                                                    <label for="recreacao">DESCREVA</label>
                                                    <textarea name="recreacao" id="recreacao" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> ESTIMULAÇÃO</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div id="div_estimulacao" class="form-group">
                                                    <label for="estimulacao">DESCREVA</label>
                                                    <textarea name="estimulacao" id="estimulacao" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> PASSEIO RECREATIVO</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div id="div_passeio_recreativo" class="form-group">
                                                    <label for="passeio_recreativo">DESCREVA</label>
                                                    <textarea name="passeio_recreativo" id="passeio_recreativo" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> LEITURA</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div id="div_leitura" class="form-group">
                                                    <label for="leitura">DESCREVA</label>
                                                    <textarea name="leitura" id="leitura" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> REFORÇO</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div id="div_reforco" class="form-group">
                                                    <label for="reforco">DESCREVA</label>
                                                    <textarea name="reforco" id="reforco" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> ALFABETIZAÇÃO</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div id="div_alfabetizacao" class="form-group">
                                                    <label for="alfabetizacao">DESCREVA</label>
                                                    <textarea name="alfabetizacao" id="alfabetizacao" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> ATIVIDADES PEDAGÓGICAS COMPLEMENTARES</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div id="div_atividades_pedagogicas" class="form-group">
                                                    <label for="atividades_pedagogicas">DESCREVA</label>
                                                    <textarea name="atividades_pedagogicas" id="atividades_pedagogicas" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> APD</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div id="div_apd" class="form-group">
                                                    <label for="apd">DESCREVA</label>
                                                    <textarea name="apd" id="apd" class="form-control" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM MONITORAMENTO DO DESEMPENHO / PARTICIPAÇÃO DA CRIANÇA NO(S) PROJETO(S) -->

                        <!-- AVALIAÇÃO DO DESEMPENHO / PARTICIPAÇÃO DA CRIANÇA NO(S) PROJETO(S) -->
                        <div class="box box-outline-primary mt-30">
                            <div class="box-header">
                                <div class="row">
                                    <div class="col-md-8"><strong>AVALIAÇÃO DO DESEMPENHO / PARTICIPAÇÃO DA CRIANÇA NO(S) PROJETO(S)</strong></div>
                                    <div class="col-md-4 text-right"><div class="badge badge-warning">AVALIAR EM: 90 Dias</div></div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> ESTIMULAÇÃO PRECOCE</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="div_monitoramento_educando" class="form-group">
                                                        <label for="monitoramento_educando">AVALIAÇÃO</label>
                                                        <textarea name="monitoramento_educando" id="monitoramento_educando" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>


                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> RECREAÇÃO</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_recreacao" class="form-group">
                                                        <label for="recreacao">AVALIAÇÃO</label>
                                                        <textarea name="recreacao" id="recreacao" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> ESTIMULAÇÃO</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_estimulacao" class="form-group">
                                                        <label for="estimulacao">AVALIAÇÃO</label>
                                                        <textarea name="estimulacao" id="estimulacao" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> PASSEIO RECREATIVO</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_passeio_recreativo" class="form-group">
                                                        <label for="passeio_recreativo">AVALIAÇÃO</label>
                                                        <textarea name="passeio_recreativo" id="passeio_recreativo" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> LEITURA</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_leitura" class="form-group">
                                                        <label for="leitura">AVALIAÇÃO</label>
                                                        <textarea name="leitura" id="leitura" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> REFORÇO</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_reforco" class="form-group">
                                                        <label for="reforco">AVALIAÇÃO</label>
                                                        <textarea name="reforco" id="reforco" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> ALFABETIZAÇÃO</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_alfabetizacao" class="form-group">
                                                        <label for="alfabetizacao">AVALIAÇÃO</label>
                                                        <textarea name="alfabetizacao" id="alfabetizacao" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> ATIVIDADES PEDAGÓGICAS COMPLEMENTARES</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_atividades_pedagogicas" class="form-group">
                                                        <label for="atividades_pedagogicas">AVALIAÇÃO</label>
                                                        <textarea name="atividades_pedagogicas" id="atividades_pedagogicas" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="box box-outline-info bg-info mt-20">
                                    <div class="box-header">
                                        <strong class="text-white"><i class="fal fa-check-square font-size-20"></i> APD</strong>
                                    </div>
                                    <div class="box-body">
                                        <form action="">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>AVALIAÇÃO</th>
                                                            <th width="15%">DATA DA AVALIAÇÃO</th>
                                                            <th>RESPONSÁVEL</th>
                                                            <th width="15%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Lorem, ipsum dolor sit amet consectetur, adipisicing elit. At doloremque quaerat voluptatum aut voluptate repellendus rerum amet exercitationem rem eum accusamus unde nesciunt architecto dolor ducimus fugit consequuntur, ab? Aperiam.
                                                            </td>
                                                            <td>31/10/2021</td>
                                                            <td>ROBERTO VALLE DOS SANTOS</td>
                                                            <td class="text-center">
                                                                <a href="" title="Editar" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-pencil-alt"></i></a>
                                                                <a href="" title="Apagar" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>   
                                                </table>
                                                
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_apd" class="form-group">
                                                        <label for="apd">DESCREVA</label>
                                                        <textarea name="apd" id="apd" class="form-control" cols="30" rows="10"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-success">AVALIAR</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM AVALIAÇÃO DO DESEMPENHO / PARTICIPAÇÃO DA CRIANÇA NO(S) PROJETO(S) -->
                    </div>
                </div>

                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $acolhimento_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CONFIRMAR</button>
                        <button <?= $acolhimento_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/consultas/novo.js"></script>