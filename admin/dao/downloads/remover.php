<?php

$db = Conexao::getInstance();

$error = false;

$codigo = strip_tags(@$_POST['codigo']);

try {

    $db->beginTransaction();

    $caminho_arquivo = pesquisar("arquivo", "mod_downloads", "id", "=", $codigo, "");

    if (unlink($caminho_arquivo)) {
        $stmt5 = $db->prepare("UPDATE mod_downloads SET arquivo = NULL WHERE id = ?");
        $stmt5->bindValue(1, $codigo);
        $stmt5->execute();

        $db->commit();

        //MENSAGEM DE SUCESSO
        $msg['id'] = $codigo;
        $msg['msg'] = 'success';
        $msg['retorno'] = 'Arquivo removido com sucesso!';
        echo json_encode($msg);
        exit();
    } else {
        $msg['msg'] = 'error';
        $msg['retorno'] = "Não foi possíve remover o arquivo desejado, por favor contate o administrador do sistema.";
        echo json_encode($msg);
        exit();
    }
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar remover o arquivo desejado:" . $e->getMessage();
    echo json_encode($msg);
    exit();
}
?>


