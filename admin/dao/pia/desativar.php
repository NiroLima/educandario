<?php

$db = Conexao::getInstance();

$error = false;

$id = strip_tags(@$_POST['id']);
$motivo = $_POST['motivo'];

try {

    $db->beginTransaction();

    $stmt5 = $db->prepare("UPDATE mod_acolhimento SET status = 0, motivo_cancelamento = ?, resp_cancelamento = ? WHERE id = ?");
    $stmt5->bindValue(1, $motivo);
    $stmt5->bindValue(2, $_SESSION['id']);
    $stmt5->bindValue(3, $id);
    $stmt5->execute();

    $db->commit();

    //MENSAGEM DE SUCESSO
    $msg['msg'] = 'success';
    $msg['retorno'] = 'PIA cancelado com sucesso!';
    echo json_encode($msg);
    exit();
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar cancelar o PIA desejado:" . $e->getMessage();
    echo json_encode($msg);
    exit();
}
?>


