<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$educacao_id = isset($_POST['educacao_id']) && $_POST['educacao_id'] != "" ? $_POST['educacao_id'] : 0;

$sim_idade_escolar = isset($_POST['sim_idade_escolar']) && $_POST['sim_idade_escolar'] != "" ? $_POST['sim_idade_escolar'] : 0;
$escola = isset($_POST['escola']) && $_POST['escola'] != "" ? $_POST['escola'] : NULL;
$serie = isset($_POST['serie']) && $_POST['serie'] != "" ? $_POST['serie'] : NULL;
$turno = isset($_POST['turno']) && $_POST['turno'] != "" ? $_POST['turno'] : NULL;

$escola1 = isset($_POST['escola1']) && $_POST['escola1'] != "" ? $_POST['escola1'] : NULL;
$serie1 = isset($_POST['serie1']) && $_POST['serie1'] != "" ? $_POST['serie1'] : NULL;
$turno1 = isset($_POST['turno1']) && $_POST['turno1'] != "" ? $_POST['turno1'] : NULL;

$sim_frequenta_instituicao = isset($_POST['sim_frequenta_instituicao']) && $_POST['sim_frequenta_instituicao'] != "" ? $_POST['sim_frequenta_instituicao'] : 0;
$justificativa_ausencia = isset($_POST['justificativa_ausencia']) && $_POST['justificativa_ausencia'] != "" ? $_POST['justificativa_ausencia'] : NULL;
$frequencia = isset($_POST['frequencia']) && $_POST['frequencia'] != "" ? $_POST['frequencia'] : 0;
$sobre_frequencia = isset($_POST['sobre_frequencia']) && $_POST['sobre_frequencia'] != "" ? $_POST['sobre_frequencia'] : NULL;
$socializacao = isset($_POST['socializacao']) && $_POST['socializacao'] != "" ? $_POST['socializacao'] : 0;
$sobre_socializacao = isset($_POST['sobre_socializacao']) && $_POST['sobre_socializacao'] != "" ? $_POST['sobre_socializacao'] : NULL;
$interesse_atividade_escolar = isset($_POST['interesse_atividade_escolar']) && $_POST['interesse_atividade_escolar'] != "" ? $_POST['interesse_atividade_escolar'] : 0;
$sobre_interesse_atividade_escolar = isset($_POST['sobre_interesse_atividade_escolar']) && $_POST['sobre_interesse_atividade_escolar'] != "" ? $_POST['sobre_interesse_atividade_escolar'] : NULL;
$rendimento = isset($_POST['rendimento']) && $_POST['rendimento'] != "" ? $_POST['rendimento'] : 0;
$sobre_rendimento = isset($_POST['sobre_rendimento']) && $_POST['sobre_rendimento'] != "" ? $_POST['sobre_rendimento'] : NULL;
$participacao_familia = isset($_POST['participacao_familia']) && $_POST['participacao_familia'] != "" ? $_POST['participacao_familia'] : 0;
$sobre_participacao_familia = isset($_POST['sobre_participacao_familia']) && $_POST['sobre_participacao_familia'] != "" ? $_POST['sobre_participacao_familia'] : NULL;

$sim_apresenta_desenvolvimento = isset($_POST['sim_apresenta_desenvolvimento']) && $_POST['sim_apresenta_desenvolvimento'] != "" ? $_POST['sim_apresenta_desenvolvimento'] : 0;
$descreva_aspectos = isset($_POST['descreva_aspectos']) && $_POST['descreva_aspectos'] != "" ? $_POST['descreva_aspectos'] : NULL;
$contexto_geral = isset($_POST['contexto_geral']) && $_POST['contexto_geral'] != "" ? $_POST['contexto_geral'] : NULL;
$sim_vontade = isset($_POST['sim_vontade']) && $_POST['sim_vontade'] != "" ? $_POST['sim_vontade'] : 0;
$descricao_relacao_escola = isset($_POST['descricao_relacao_escola']) && $_POST['descricao_relacao_escola'] != "" ? $_POST['descricao_relacao_escola'] : NULL;
$descricao_intervencoes = isset($_POST['descricao_intervencoes']) && $_POST['descricao_intervencoes'] != "" ? $_POST['descricao_intervencoes'] : NULL;
$sim_estrutura = isset($_POST['sim_estrutura']) && $_POST['sim_estrutura'] != "" ? $_POST['sim_estrutura'] : 0;
$sim_rotinas_estabelecidas = isset($_POST['sim_rotinas_estabelecidas']) && $_POST['sim_rotinas_estabelecidas'] != "" ? $_POST['sim_rotinas_estabelecidas'] : 0;
$sim_idade_escolar_atual = isset($_POST['sim_idade_escolar_atual']) && $_POST['sim_idade_escolar_atual'] != "" ? $_POST['sim_idade_escolar_atual'] : 0;
$justique_ausencia = isset($_POST['justique_ausencia']) && $_POST['justique_ausencia'] != "" ? $_POST['justique_ausencia'] : NULL;
$justifique = isset($_POST['justifique']) && $_POST['justifique'] != "" ? $_POST['justifique'] : NULL;
$justifique2 = isset($_POST['justifique2']) && $_POST['justifique2'] != "" ? $_POST['justifique2'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($educacao_id) && $educacao_id > 0) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_educacao SET idade_escolar = ?, escola = ?, serie = ?, turno  = ?, frequenta_instituicao = ?, justificacao_ausencia = ?, frequencia = ?, sobre_frequencia = ?,
                                  socializacao = ?, sobre_socializacao = ?, interesse = ?, sobre_interesse = ?, rendimento = ?, sobre_rendimento = ?, participacao = ?, sobre_participacao = ?, apresenta_desenvolvimento = ?, 
                                  descricao_aspectos = ?, contexto_geral = ?, relacao_escola = ?, descricao_relacao_escola = ?, descricao_intervencoes = ?, espaco_estrutura = ?, rotinas = ?, idade_escolar_atual = ?, 
                                  justique_ausencia = ?, justifique = ?, justifique2 = ?, escola1 = ?, serie1 = ?, turno1  = ?, responsavel_id = ? 
                                  WHERE id = ?");
            $sql4->bindValue(1, $sim_idade_escolar);
            $sql4->bindValue(2, $escola);
            $sql4->bindValue(3, $serie);
            $sql4->bindValue(4, $turno);
            $sql4->bindValue(5, $sim_frequenta_instituicao);
            $sql4->bindValue(6, $justificativa_ausencia);
            $sql4->bindValue(7, $frequencia);
            $sql4->bindValue(8, $sobre_frequencia);
            $sql4->bindValue(9, $socializacao);
            $sql4->bindValue(10, $sobre_socializacao);
            $sql4->bindValue(11, $interesse_atividade_escolar);
            $sql4->bindValue(12, $sobre_interesse_atividade_escolar);
            $sql4->bindValue(13, $rendimento);
            $sql4->bindValue(14, $sobre_rendimento);
            $sql4->bindValue(15, $participacao_familia);
            $sql4->bindValue(16, $sobre_participacao_familia);
            $sql4->bindValue(17, $sim_apresenta_desenvolvimento);
            $sql4->bindValue(18, $descreva_aspectos);
            $sql4->bindValue(19, $contexto_geral);
            $sql4->bindValue(20, $sim_vontade);
            $sql4->bindValue(21, $descricao_relacao_escola);
            $sql4->bindValue(22, $descricao_intervencoes);
            $sql4->bindValue(23, $sim_estrutura);
            $sql4->bindValue(24, $sim_rotinas_estabelecidas);
            $sql4->bindValue(25, $sim_idade_escolar_atual);
            $sql4->bindValue(26, $justique_ausencia);
            $sql4->bindValue(27, $justifique);
            $sql4->bindValue(28, $justifique2);
            $sql4->bindValue(29, $escola1);
            $sql4->bindValue(30, $serie1);
            $sql4->bindValue(31, $turno1);
            $sql4->bindValue(32, $_SESSION['id']);
            $sql4->bindValue(33, $educacao_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_educacao (idade_escolar, escola, serie, turno, frequenta_instituicao, justificacao_ausencia, frequencia, sobre_frequencia,
                                  socializacao, sobre_socializacao, interesse, sobre_interesse, rendimento, sobre_rendimento, participacao, sobre_participacao, apresenta_desenvolvimento, 
                                  descricao_aspectos, contexto_geral, relacao_escola, descricao_relacao_escola, descricao_intervencoes, espaco_estrutura, rotinas, idade_escolar_atual, justique_ausencia, justifique, justifique2,
                                  escola, serie, turno, 
                                  responsavel_id, acolhimento_crianca_id, data_cadastro, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1)");
            $sql4->bindValue(1, $sim_idade_escolar);
            $sql4->bindValue(2, $escola);
            $sql4->bindValue(3, $serie);
            $sql4->bindValue(4, $turno);
            $sql4->bindValue(5, $sim_frequenta_instituicao);
            $sql4->bindValue(6, $justificativa_ausencia);
            $sql4->bindValue(7, $frequencia);
            $sql4->bindValue(8, $sobre_frequencia);
            $sql4->bindValue(9, $socializacao);
            $sql4->bindValue(10, $sobre_socializacao);
            $sql4->bindValue(11, $interesse_atividade_escolar);
            $sql4->bindValue(12, $sobre_interesse_atividade_escolar);
            $sql4->bindValue(13, $rendimento);
            $sql4->bindValue(14, $sobre_rendimento);
            $sql4->bindValue(15, $participacao_familia);
            $sql4->bindValue(16, $sobre_participacao_familia);
            $sql4->bindValue(17, $sim_apresenta_desenvolvimento);
            $sql4->bindValue(18, $descreva_aspectos);
            $sql4->bindValue(19, $contexto_geral);
            $sql4->bindValue(20, $sim_vontade);
            $sql4->bindValue(21, $descricao_relacao_escola);
            $sql4->bindValue(22, $descricao_intervencoes);
            $sql4->bindValue(23, $sim_estrutura);
            $sql4->bindValue(24, $sim_rotinas_estabelecidas);
            $sql4->bindValue(25, $sim_idade_escolar_atual);
            $sql4->bindValue(26, $justique_ausencia);
            $sql4->bindValue(27, $justifique);
            $sql4->bindValue(28, $justifique2);
            $sql4->bindValue(29, $escola1);
            $sql4->bindValue(30, $serie1);
            $sql4->bindValue(31, $turno1);
            $sql4->bindValue(32, $_SESSION['id']);
            $sql4->bindValue(33, $id);
            $sql4->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>