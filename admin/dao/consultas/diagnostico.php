<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$diagnostico_id = isset($_POST['diagnostico_id']) && $_POST['diagnostico_id'] != "" ? $_POST['diagnostico_id'] : 0;
$consulta_id = isset($_POST['consulta_id']) && $_POST['consulta_id'] != "" ? $_POST['consulta_id'] : 0;

$data_diagnostico = isset($_POST['data_diagnostico']) ? $_POST['data_diagnostico'] : NULL;
$diagnostico_medico = isset($_POST['diagnostico_medico']) ? $_POST['diagnostico_medico'] : NULL;
$orientacoes_medicas = isset($_POST['orientacoes_medicas']) ? $_POST['orientacoes_medicas'] : NULL;
$exames_solicitados = isset($_POST['exames_solicitados']) ? $_POST['exames_solicitados'] : NULL;
$medicacoes_posologia = isset($_POST['medicacoes_posologia']) ? $_POST['medicacoes_posologia'] : NULL;
$especialistas = isset($_POST['especialistas']) ? $_POST['especialistas'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($diagnostico_id == 0) {

            if ($consulta_id == 0) {
                $sql = $db->prepare("INSERT INTO mod_saude_consulta_diagnostico (data_diagnostico, diagnostico, orientacoes, medicacoes, responsavel_id) VALUES (?, ?, ?, ?, ?)");
                $sql->bindValue(1, $data_diagnostico);
                $sql->bindValue(2, $diagnostico_medico);
                $sql->bindValue(3, $orientacoes_medicas);
                $sql->bindValue(4, $medicacoes_posologia);
                $sql->bindValue(5, $_SESSION['id']);
                $sql->execute();

                $diagnostico_id = $db->lastInsertId();
            } else {
                $sql = $db->prepare("INSERT INTO mod_saude_consulta_diagnostico (data_diagnostico, diagnostico, orientacoes, medicacoes, consulta_id, responsavel_id) VALUES (?, ?, ?, ?, ?, ?)");
                $sql->bindValue(1, $data_diagnostico);
                $sql->bindValue(2, $diagnostico_medico);
                $sql->bindValue(3, $orientacoes_medicas);
                $sql->bindValue(4, $medicacoes_posologia);
                $sql->bindValue(5, $consulta_id);
                $sql->bindValue(6, $_SESSION['id']);
                $sql->execute();

                $diagnostico_id = $db->lastInsertId();
            }
        } else {
            $sql = $db->prepare("UPDATE mod_saude_consulta_diagnostico SET data_diagnostico = ?, diagnostico = ?, orientacoes = ?, medicacoes = ?, responsavel_id = ? WHERE id = ?");
            $sql->bindValue(1, $data_diagnostico);
            $sql->bindValue(2, $diagnostico_medico);
            $sql->bindValue(3, $orientacoes_medicas);
            $sql->bindValue(4, $medicacoes_posologia);
            $sql->bindValue(5, $_SESSION['id']);
            $sql->bindValue(6, $diagnostico_id);
            $sql->execute();
        }

        //EXAME
        $sql2 = $db->prepare("DELETE FROM mod_saude_consulta_exame WHERE diagnostico_id = ?");
        $sql2->bindValue(1, $diagnostico_id);
        $sql2->execute();

        if ($exames_solicitados != null) {
            foreach ($exames_solicitados AS $key => $val) {
                if ($val != "" && is_numeric($val) && $val != 0) {
                    $sql3 = $db->prepare("INSERT INTO mod_saude_consulta_exame (diagnostico_id, exame_id) VALUES (?, ?)");
                    $sql3->bindValue(1, $diagnostico_id);
                    $sql3->bindValue(2, $val);
                    $sql3->execute();
                }
            }
        }

        //ESPECIALISTA
        $sql4 = $db->prepare("DELETE FROM mod_saude_consulta_especialistas WHERE diagnostico_id = ?");
        $sql4->bindValue(1, $diagnostico_id);
        $sql4->execute();

        if ($especialistas != null) {
            foreach ($especialistas AS $key2 => $val2) {
                if ($val2 != "" && is_numeric($val2) && $val2 != 0) {
                    $sql5 = $db->prepare("INSERT INTO mod_saude_consulta_especialistas (diagnostico_id, especialista_id) VALUES (?, ?)");
                    $sql5->bindValue(1, $diagnostico_id);
                    $sql5->bindValue(2, $val2);
                    $sql5->execute();
                }
            }
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>