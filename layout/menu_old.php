<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">	
        <div class="user-profile px-10 py-15">
            <div class="d-flex align-items-center">			
                <div class="image">
                    <img src="<?= isset($_SESSION['foto']) != '' ? $_SESSION['foto'] : PORTAL_URL . 'avatar/sem_foto.jpg'; ?>" class="avatar avatar-lg" alt="User Image">
                </div>
                <div class="info ml-10">
                    <p class="mb-0">Bem-Vindo(a)</p>
                    <h5 class="mb-0"><?= $_SESSION['nome']; ?></h5>
                </div>
            </div>
        </div>	

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="<?= PORTAL_URL; ?>admin/view/painel/dashboard">
                    <i class="fal fa-desktop-alt"></i>
                    <span>DASHBOARD</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#"> <i class="fal fa-clipboard-list-check"></i> <span>ACOLHIMENTO</span>
                    <span class="pull-right-container">
                        <i class="ti-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= PORTAL_URL ?>admin/view/acolhimento/lista"><i class="ti-more"></i>ACOLHIDOS</a></li>
                    <li><a href="<?= PORTAL_URL ?>admin/view/acolhimento/cancelados"><i class="ti-more"></i>LISTA DE CANCELADOS</a></li>
                    <li><a href="<?= PORTAL_URL ?>admin/view/acolhimento/novo"><i class="ti-more"></i>NOVO ACOLHIMENTO</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"> <i class="fal fa-file-contract"></i> <span>PIA</span>
                    <span class="pull-right-container">
                        <i class="ti-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= PORTAL_URL ?>admin/view/pia/lista"><i class="ti-more"></i>PIA's</a></li>
                    <li><a href="<?= PORTAL_URL ?>admin/view/pia/cancelados"><i class="ti-more"></i>PIA's CANCELADOS</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"> <i class="fal fa-hospital"></i> <span>SAÚDE</span>
                    <span class="pull-right-container">
                        <i class="ti-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#"> <i class="fal fa-clipboard-user"></i> <span>CONSULTAS</span>
                            <span class="pull-right-container">
                                <i class="ti-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= PORTAL_URL ?>admin/view/consultas/lista"><i class="ti-more"></i>CONSULTAS</a></li>
                            <li><a href="<?= PORTAL_URL ?>admin/view/consultas/novo"><i class="ti-more"></i>NOVA CONSULTA</a></li>
                        </ul>
                    </li>
<!--                    <li class="treeview">
                        <a href="#"> <i class="fal fa-user-md"></i> <span>MÉDICOS</span>
                            <span class="pull-right-container">
                                <i class="ti-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= PORTAL_URL ?>admin/view/medicos/lista"><i class="ti-more"></i>MÉDICOS</a></li>
                            <li><a href="<?= PORTAL_URL ?>admin/view/medicos/novo"><i class="ti-more"></i>NOVO MÉDICO</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#"> <i class="fal fa-tooth"></i> <span>ODONTÓLOGOS</span>
                            <span class="pull-right-container">
                                <i class="ti-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= PORTAL_URL ?>admin/view/odontologos/lista"><i class="ti-more"></i>ODONTÓLOGOS</a></li>
                            <li><a href="<?= PORTAL_URL ?>admin/view/odontologos/novo"><i class="ti-more"></i>NOVO ODONTÓLOGO</a></li>
                        </ul>
                    </li>-->
                    <li class="treeview">
                        <a href="#"> <i class="fal fa-box"></i> <span>MEDICAMENTOS</span>
                            <span class="pull-right-container">
                                <i class="ti-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= PORTAL_URL ?>admin/view/medicamentos/lista"><i class="ti-more"></i>MEDICAMENTOS</a></li>
                            <li><a href="<?= PORTAL_URL ?>admin/view/medicamentos/novo"><i class="ti-more"></i>NOVO MEDICAMENTO</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#"> <i class="fal fa-microscope"></i> <span>EXAMES</span>
                            <span class="pull-right-container">
                                <i class="ti-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= PORTAL_URL ?>admin/view/exames/lista"><i class="ti-more"></i>EXAMES</a></li>
                            <li><a href="<?= PORTAL_URL ?>admin/view/exames/novo"><i class="ti-more"></i>NOVO EXAME</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#"> <i class="fal fa-syringe"></i> <span>VACINAS</span>
                            <span class="pull-right-container">
                                <i class="ti-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= PORTAL_URL ?>admin/view/vacinas/lista"><i class="ti-more"></i>VACINAS</a></li>
                            <li><a href="<?= PORTAL_URL ?>admin/view/vacinas/novo"><i class="ti-more"></i>NOVO VACINA</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
            <li class="header">ADMINISTRAÇÃO</li>
            <li class="treeview">
                <a href="#"> <i class="fal fa-barcode-read"></i> <span>PATRIMÔNIO</span>
                    <span class="pull-right-container">
                        <i class="ti-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= PORTAL_URL ?>admin/view/patrimonios/lista"><i class="ti-more"></i>PATRIMÔNIOS</a></li>
                    <li><a href="<?= PORTAL_URL ?>admin/view/patrimonios/novo"><i class="ti-more"></i>NOVO PATRIMÔNIO</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"> <i class="fal fa-user"></i> <span>FUNCIONÁRIOS</span>
                    <span class="pull-right-container">
                        <i class="ti-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= PORTAL_URL ?>admin/view/usuarios/lista"><i class="ti-more"></i>FUNCIONÁRIOS</a></li>
                    <li><a href="<?= PORTAL_URL ?>admin/view/usuarios/novo"><i class="ti-more"></i>NOVO FUNCIONÁRIO</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fal fa-id-card"></i> <span>FUNÇÕES</span>
                    <span class="pull-right-container">
                        <i class="ti-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= PORTAL_URL ?>admin/view/funcoes/lista"><i class="ti-more"></i>FUNÇÕES</a></li>
                    <li><a href="<?= PORTAL_URL ?>admin/view/funcoes/novo"><i class="ti-more"></i>NOVA FUNÇÃO</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fal fa-chair-office"></i> <span>SETORES</span>
                    <span class="pull-right-container">
                        <i class="ti-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= PORTAL_URL ?>admin/view/setores/lista"><i class="ti-more"></i>SETORES</a></li>
                    <li><a href="<?= PORTAL_URL ?>admin/view/setores/novo"><i class="ti-more"></i>NOVO SETOR</a></li>
                </ul>
            </li>
<!--            <li class="treeview">
                <a href="#"> 
                    <i class="fal fa-cubes"></i> <span>MÓDULOS</span> 
                    <span class="pull-right-container"> <i class="ti-angle-right pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="error_maintenance.html"><i class="ti-more"></i>Criança</a></li>
                    <li><a href="error_404.html"><i class="ti-more"></i>Educação</a></li>
                    <li><a href="error_500.html"><i class="ti-more"></i>Saúde</a></li>
                    <li><a href="error_maintenance.html"><i class="ti-more"></i>Sócios</a></li>
                    <li><a href="error_maintenance.html"><i class="ti-more"></i>Voluntários</a></li>	
                </ul>
            </li>-->
            <li class="treeview">
                <a href="#"> 
                    <i class="fal fa-file-chart-pie"></i> <span>RELATÓRIOS</span> 
                    <span class="pull-right-container"> <i class="ti-angle-right pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="error_maintenance.html"><i class="ti-more"></i>Criança</a></li>
                    <li><a href="error_404.html"><i class="ti-more"></i>Educação</a></li>
                    <li><a href="error_500.html"><i class="ti-more"></i>Saúde</a></li>
                    <li><a href="error_maintenance.html"><i class="ti-more"></i>Sócios</a></li>
                    <li><a href="error_maintenance.html"><i class="ti-more"></i>Voluntários</a></li> 
                </ul>
            </li>  
        </ul>
    </section>
    <div class="sidebar-footer">
        <!-- item-->
        <a href="#" class="link" data-toggle="tooltip" title="" data-original-title="Configurações" aria-describedby="tooltip92529"><i class="ti-settings"></i></a>
        <!-- item-->
        <a href="#" class="link" data-toggle="tooltip" title="" data-original-title="E-mail"><i class="ti-email"></i></a>
        <!-- item-->
        <a href="<?= PORTAL_URL; ?>logout" class="link" data-toggle="tooltip" title="" data-original-title="Deslogar"><i class="ti-lock"></i></a>
    </div>
</aside>