<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT * 
                 FROM mod_saude_medicos msm  
                 WHERE msm.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_medico = $result->fetch(PDO::FETCH_ASSOC);

    $medico_id = $dados_medico['id'];
    $medico_nome = $dados_medico['nome'];
    $medico_especialidade = $dados_medico['especialidade_id'];
    $medico_tipo = $dados_medico['tipo'];
} else {
    $medico_id = "";
    $medico_nome = "";
    $medico_especialidade = "";
    $medico_tipo = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/medicos/lista">Médicos</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_medico" name="v" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $medico_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-user-md"></i> <strong>NOVO MÉDICO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div id="div_nome_medico" class="form-group">
                                    <label for="nome_medico">NOME</label>
                                    <input type="text" class="form-control" name="nome_medico" id="nome_medico" placeholder="Nome do Médico" value="<?= $medico_nome; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_especialidade" class="form-group">
                                    <label for="especialidade">ESPECIALIDADE</label>
                                    <select name="especialidade" id="especialidade" class="form-control select2">
                                        <option selected="true" value="">Selecione a especialidade</option>
                                        <?php
                                        $result = $db->prepare("SELECT *        
                                                                FROM mod_saude_especialidade   
                                                                WHERE status = 1  
                                                                ORDER BY nome");
                                        $result->execute();
                                        while ($especialidades = $result->fetch(PDO::FETCH_ASSOC)) {
                                            if ($medico_especialidade == $especialidades['id']) {
                                                ?>
                                                <option selected="true" value="<?= $especialidades['id']; ?>"><?= $especialidades['nome']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="<?= $especialidades['id']; ?>"><?= $especialidades['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-4">
                                <div id="div_tipo_funcionario" class="form-group">
                                    <label for="tipo_funcionario">TIPO DE FUNCIONÁRIO</label>
                                    <select name="tipo_funcionario" id="tipo_funcionario" class="form-control select2">
                                        <option value="">Selecione tipo de funcionário</option>
                                        <option <?= $medico_tipo == 1 ? "selected='true'" : ""; ?> value="1">Funcionário do Quadro</option>
                                        <option <?= $medico_tipo == 2 ? "selected='true'" : ""; ?> value="2">Funcionário Extra-Quadro</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $medico_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $medico_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/medicos/novo.js"></script>