//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("select#nome_crianca").change(function () {
        var idade = $(this).find('option:selected').attr('rel2');
        $("#idade_crianca").val(idade);
    });

    $("select#nome_acompanhante").change(function () {
        var funcao = $(this).find('option:selected').attr('rel2');
        $("#funcao").val(funcao);
    });

    $("select#nome_profissional").change(function () {
        var funcao = $(this).find('option:selected').attr('rel2');
        $("#especialidade").val(funcao);
    });

    $("select#unidade_saude").change(function () {
        var contato = $(this).find('option:selected').attr('contato');
        var endereco = $(this).find('option:selected').attr('endereco');
        var numero = $(this).find('option:selected').attr('numero');
        var bairro = $(this).find('option:selected').attr('bairro');

        $("#contato_unidade_saude").val(contato);
        $("#endereco_unidade_saude").val(endereco);
        $("#numero_unidade_saude").val(numero);
        $("#bairro_unidade_saude").val(bairro);
    });

    $("#add").click(function () {

        var terapia_id = $("#id").val();
        var diagnostico_id = $("#diagnostico_id").val();
        var data_diagnostico = $("#data_diagnostico").val();
        var nome_profissional = $("#nome_profissional").val();
        var nome_acompanhante = $("#nome_acompanhante").val();
        var observacao_sessao = $("#observacao_sessao").val();

        if (formulario_validator("")) {
            $.post(PORTAL_URL + "admin/dao/terapia/diagnostico.php", {terapia_id: terapia_id, diagnostico_id: diagnostico_id, nome_profissional: nome_profissional,
                nome_acompanhante: nome_acompanhante, observacao_sessao: observacao_sessao, data_diagnostico: data_diagnostico}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Terapia",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Terapia",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        $.post(PORTAL_URL + "admin/dao/terapia/carregar_diagnosticos.php",
                                {id: terapia_id},
                                function (valor) {
                                    $("button#add").html("INSERIR");
                                    $("input#diagnostico_id").val("");
                                    $("input#data_diagnostico").val("");
                                    $("select#nome_profissional").val("");
                                    $("select#nome_profissional").select2();
                                    $("select#nome_acompanhante").val("");
                                    $("select#nome_acompanhante").select2();
                                    $("textarea#observacao_sessao").val("");
                                    $("input#especialidade").val("");
                                    $("input#funcao").val("");
                                    $("tbody#resultado_diagnostico").html(valor);
                                    var topPosition = $("div#div_diagnostico2").offset().top - 135;
                                    $('html, body').animate({
                                        scrollTop: topPosition
                                    }, 800);

                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $('#form_terapia').submit(function () {
        if (formulario_validator_2("")) {
            projetouniversal.util.getjson({
                url: PORTAL_URL + "admin/dao/terapia/novo.php",
                type: "POST",
                data: $('#form_terapia').serialize(),
                enctype: 'multipart/form-data',
                success: onSuccessSend2,
                error: onError2
            });
            return false;
        } else {
            return false;
        }
    });
});
//------------------------------------------------------------------------------    
function onSuccessSend2(obj) {
    if (obj.msg == 'success') {
        swal({
            title: "Formulário de Terapia",
            text: "Ação realizada com sucesso!",
            type: "success",
            confirmButtonClass: "btn btn-success",
            confirmButtonText: "Ok"
        }).then(function () {
            postToURL(PORTAL_URL + 'admin/view/terapia/lista');
        });
    } else if (obj.msg == 'error') {
        formulario_validator_2(obj);
    }
    return false;
}
//------------------------------------------------------------------------------
function onError2(args) {
    swal({
        title: "Error!",
        text: "" + args.retorno + "",
        type: "error",
        confirmButtonClass: "btn btn-danger",
        confirmButtonText: "Ok"
    });
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_2(obj) {
    var valido = true;

    var data_terapia = $('#data_terapia').val();
    var hora_terapia = $('#hora_terapia').val();
    var nome_crianca = $('#nome_crianca').val();
    var unidade_saude = $('#unidade_saude').val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (unidade_saude == "") {
            $('div#div_unidade_saude').after('<label id="erro_unidade_saude" class="error">Unidade é obrigatório.</label>');
            valido = false;
            element = $('div#div_unidade_saude');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome_crianca == "") {
            $('div#div_nome_crianca').after('<label id="erro_nome_crianca" class="error">Paciente é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_crianca');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (hora_terapia == "") {
            $('div#div_hora_terapia').after('<label id="erro_hora_terapia" class="error">Hora da terapia é obrigatório.</label>');
            valido = false;
            element = $('div#div_hora_terapia');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (data_terapia == "") {
            $('div#div_data_terapia').after('<label id="erro_data_terapia" class="error">Data da terapia é obrigatório.</label>');
            valido = false;
            element = $('div#div_data_terapia');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var data_diagnostico = $("#data_diagnostico").val();
    var nome_profissional = $("#nome_profissional").val();
    var nome_acompanhante = $("#nome_acompanhante").val();
    var observacao_sessao = $("#observacao_sessao").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (observacao_sessao == "") {
            $('div#div_observacao_sessao').after('<label id="erro_observacao_sessao" class="error">Observação é obrigatório.</label>');
            valido = false;
            element = $('div#div_observacao_sessao');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome_acompanhante == "") {
            $('div#div_nome_acompanhante').after('<label id="erro_nome_acompanhante" class="error">Acompanhante é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_acompanhante');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome_profissional == "") {
            $('div#div_nome_profissional').after('<label id="erro_nome_profissional" class="error">Profissional é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_profissional');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (data_diagnostico == "" || data_diagnostico == "dd/mm/aaaa") {
            $('div#div_data_diagnostico').after('<label id="erro_data_diagnostico" class="error">Data do diagnóstico é obrigatório.</label>');
            valido = false;
            element = $('div#div_data_diagnostico');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function editar(id, data_diagnostico, profissional_id, prof_funcao, acompanhante_id, acomp_funcao, obs) {
    $("input#diagnostico_id").val(id);
    $("input#data_diagnostico").val(data_diagnostico);
    $("select#nome_profissional").val(profissional_id);
    $("select#nome_profissional").select2();
    $("input#especialidade").val(prof_funcao);
    $("select#nome_acompanhante").val(acompanhante_id);
    $("select#nome_acompanhante").select2();
    $("input#funcao").val(acomp_funcao);
    $("textarea#observacao_sessao").val(obs);
    $("button#add").html("ALTERAR");
}
//------------------------------------------------------------------------------------------------------
function remover(obj, codigo) {
    swal({
        title: "Deseja mesmo remover este diagnóstico?",
        text: "Obs: Caso escolha remover o diagnóstico, não será mais possível recuperar os dados novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/terapia/remover_diagnostico",
            type: "POST",
            data: {id: codigo},
            enctype: 'multipart/form-data',
            success: onSuccessSend22(obj),
            error: onSuccessSend22(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend22(obj) {
    swal({
        title: "Sucesso!",
        text: "Diagnóstico removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#diagnostico_id").val("");
        $("input#data_diagnostico").val("");
        $("select#nome_profissional").val("");
        $("select#nome_profissional").select2();
        $("select#nome_acompanhante").val("");
        $("select#nome_acompanhante").select2();
        $("textarea#observacao_sessao").val("");
        $("input#especialidade").val("");
        $("input#funcao").val("");
        $("button#add").html("INSERIR");
        $(obj).parents('tr#remover_diagnostico').remove();
    });

    return false;
}
//------------------------------------------------------------------------------------------------------