<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;

$tipo_atendimento = isset($_POST['opcao_retono_atendimento']) ? $_POST['opcao_retono_atendimento'] : 0;
$tipo = isset($_POST['opcao_retono_tipo']) ? $_POST['opcao_retono_tipo'] : 0;
$tipo_consulta = isset($_POST['opcao_retono_consulta']) ? $_POST['opcao_retono_consulta'] : 0;

$data_consulta = isset($_POST['data_consulta']) ? $_POST['data_consulta'] : NULL;
$hora_consulta = isset($_POST['hora_consulta']) ? $_POST['hora_consulta'] : NULL;
$observacao = isset($_POST['observacao']) ? $_POST['observacao'] : NULL;
$nome_crianca = isset($_POST['nome_crianca']) ? $_POST['nome_crianca'] : NULL;
$nome_acompanhante = isset($_POST['nome_acompanhante']) ? $_POST['nome_acompanhante'] : NULL;
$preparativos = isset($_POST['preparativos']) ? $_POST['preparativos'] : NULL;
$nome_profissional = isset($_POST['nome_profissional']) ? $_POST['nome_profissional'] : NULL;
$unidade_saude = isset($_POST['unidade_saude']) ? $_POST['unidade_saude'] : NULL;
$endereco_unidade_saude = isset($_POST['endereco_unidade_saude']) ? $_POST['endereco_unidade_saude'] : NULL;
$numero_unidade_saude = isset($_POST['numero_unidade_saude']) ? $_POST['numero_unidade_saude'] : NULL;
$bairro_unidade_saude = isset($_POST['bairro_unidade_saude']) ? $_POST['bairro_unidade_saude'] : NULL;
$contato = isset($_POST['contato_unidade_saude']) ? $_POST['contato_unidade_saude'] : NULL;

$vacinas_aplicadas = isset($_POST['vacinas_aplicadas']) ? $_POST['vacinas_aplicadas'] : NULL;

$opcao_solicitado_retorno = isset($_POST['opcao_solicitado_retorno']) ? $_POST['opcao_solicitado_retorno'] : 0;
$data_provavel_retorno = isset($_POST['data_provavel_retorno']) ? $_POST['data_provavel_retorno'] : NULL;
$data_agendada_retorno = isset($_POST['data_agendada_retorno']) ? $_POST['data_agendada_retorno'] : NULL;
$hora_retorno = isset($_POST['hora_retorno']) ? $_POST['hora_retorno'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_saude_consultas (tipo_atendimento, tipo, tipo_consulta, data_consulta, hora_consulta, obs, crianca_id, acomp_id, preparativos, profissional_id, unidade_id, endereco, numero, bairro, contato, retorno_consulta, data_retorno, data_agendada, hora_retorno, data_update, responsavel_id, data_cadastro, status, situacao) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, NOW(), 1, 1)");
            $sql->bindValue(1, $tipo_atendimento);
            $sql->bindValue(2, $tipo);
            $sql->bindValue(3, $tipo_consulta);
            $sql->bindValue(4, $data_consulta);
            $sql->bindValue(5, $hora_consulta);
            $sql->bindValue(6, $observacao);
            $sql->bindValue(7, $nome_crianca);
            $sql->bindValue(8, $nome_acompanhante);
            $sql->bindValue(9, $preparativos);
            $sql->bindValue(10, $nome_profissional);
            $sql->bindValue(11, $unidade_saude);
            $sql->bindValue(12, $endereco_unidade_saude);
            $sql->bindValue(13, $numero_unidade_saude);
            $sql->bindValue(14, $bairro_unidade_saude);
            $sql->bindValue(15, $contato);
            $sql->bindValue(16, $opcao_solicitado_retorno);
            $sql->bindValue(17, $data_provavel_retorno);
            $sql->bindValue(18, $data_agendada_retorno);
            $sql->bindValue(19, $hora_retorno);
            $sql->bindValue(20, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();

            $stmp = $db->prepare("SELECT *           
                                  FROM mod_saude_consulta_diagnostico mscd   
                                  WHERE mscd.consulta_id IS NULL AND mscd.responsavel_id = ?");
            $stmp->bindValue(1, $_SESSION['id']);
            $stmp->execute();
            while ($diag = $stmp->fetch(PDO::FETCH_ASSOC)) {
                $sql = $db->prepare("UPDATE mod_saude_consulta_diagnostico SET consulta_id = ? WHERE id = ?");
                $sql->bindValue(1, $id);
                $sql->bindValue(2, $diag['id']);
                $sql->execute();
            }
        } else {
            $sql = $db->prepare("UPDATE mod_saude_consultas SET tipo_atendimento = ?, tipo = ?, tipo_consulta = ?, data_consulta = ?, hora_consulta = ?, obs = ?, crianca_id = ?, acomp_id = ?, preparativos = ?, profissional_id = ?, unidade_id = ?, endereco = ?, numero = ?, bairro = ?, contato = ?, retorno_consulta = ?, data_retorno = ?, data_agendada = ?, hora_retorno = ?, responsavel_id = ? WHERE id = ?");
            $sql->bindValue(1, $tipo_atendimento);
            $sql->bindValue(2, $tipo);
            $sql->bindValue(3, $tipo_consulta);
            $sql->bindValue(4, $data_consulta);
            $sql->bindValue(5, $hora_consulta);
            $sql->bindValue(6, $observacao);
            $sql->bindValue(7, $nome_crianca);
            $sql->bindValue(8, $nome_acompanhante);
            $sql->bindValue(9, $preparativos);
            $sql->bindValue(10, $nome_profissional);
            $sql->bindValue(11, $unidade_saude);
            $sql->bindValue(12, $endereco_unidade_saude);
            $sql->bindValue(13, $numero_unidade_saude);
            $sql->bindValue(14, $bairro_unidade_saude);
            $sql->bindValue(15, $contato);
            $sql->bindValue(16, $opcao_solicitado_retorno);
            $sql->bindValue(17, $data_provavel_retorno);
            $sql->bindValue(18, $data_agendada_retorno);
            $sql->bindValue(19, $hora_retorno);
            $sql->bindValue(20, $_SESSION['id']);
            $sql->bindValue(21, $id);
            $sql->execute();
        }

        //VACINAS
        $sql = $db->prepare("DELETE FROM mod_saude_consulta_vacina WHERE consulta_id = ?");
        $sql->bindValue(1, $id);
        $sql->execute();

        if ($vacinas_aplicadas != null) {
            foreach ($vacinas_aplicadas AS $key => $val) {
                if ($val != "" && $val != null && is_numeric($val)) {
                    $sql = $db->prepare("INSERT INTO mod_saude_consulta_vacina (consulta_id, vacina_id) VALUES (?, ?)");
                    $sql->bindValue(1, $id);
                    $sql->bindValue(2, $val);
                    $sql->execute();
                }
            }
        }

        $db->commit();

        $msg['id'] = $id;
        $msg['msg'] = 'success';
        $msg['retorno'] = 'Consulta marcada com sucesso!';
        echo json_encode($msg);
        exit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>