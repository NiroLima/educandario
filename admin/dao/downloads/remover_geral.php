<?php

$db = Conexao::getInstance();

$error = false;

$codigo = strip_tags(@$_POST['id']);

try {

    $db->beginTransaction();

    $caminho_arquivo = pesquisar("arquivo", "mod_downloads", "id", "=", $codigo, "");

    if ($caminho_arquivo != "" && $caminho_arquivo != null && $caminho_arquivo != " ") {
        if (unlink($caminho_arquivo)) {
            $stmt5 = $db->prepare("DELETE FROM mod_downloads WHERE id = ?");
            $stmt5->bindValue(1, $codigo);
            $stmt5->execute();

            $db->commit();

            //MENSAGEM DE SUCESSO
            $msg['id'] = $codigo;
            $msg['msg'] = 'success';
            $msg['retorno'] = 'Arquivo removido com sucesso!';
            echo json_encode($msg);
            exit();
        } else {
            $msg['msg'] = 'error';
            $msg['retorno'] = "Não foi possíve remover o arquivo desejado, por favor contate o administrador do sistema.";
            echo json_encode($msg);
            exit();
        }
    } else {
        $stmt5 = $db->prepare("DELETE FROM mod_downloads WHERE id = ?");
        $stmt5->bindValue(1, $codigo);
        $stmt5->execute();

        $db->commit();

        //MENSAGEM DE SUCESSO
        $msg['id'] = $codigo;
        $msg['msg'] = 'success';
        $msg['retorno'] = 'Arquivo removido com sucesso!';
        echo json_encode($msg);
        exit();
    }
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar remover o arquivo desejado:" . $e->getMessage();
    echo json_encode($msg);
    exit();
}
?>


