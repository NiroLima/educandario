<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
                            FROM mod_acolhimento ma   
                            WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
    $acolhimento_quem_trouxe = $resultado['quem_trouxe'];
    $opcao_conselho = $acolhimento_quem_trouxe == 2 ? $resultado['opcao_conselho'] : 1;
    $comarca_id = $acolhimento_quem_trouxe == 2 ? $resultado['comarca_id'] : "";
    $acolhimento_responsavel_entrega = $resultado['responsavel_entrega'];
    $acolhimento_funcao_responsavel = $resultado['funcao_responsavel'];
    $acolhimento_celular = $resultado['celular'];
    $acolhimento_data_entrada = $resultado['data_entrada'];
    $acolhimento_hora_entrada = $resultado['hora_entrada'];
    $acolhimento_profissional_id = $resultado['profissional_id'];
    $obs = $resultado['obs'];
    $motivo_aba = $resultado['motivo_aba'];
    $motivo_neg = $resultado['motivo_neg'];
    $motivo_abu = $resultado['motivo_abu'];
    $motivo_out = $resultado['motivo_out'];
    $descricao = $resultado['descricao'];
    $numero_guia = $resultado['numero_guia'];
    $numero_processo = $resultado['numero_processo'];
    $numero_destituicao = $resultado['numero_destituicao'];
//------------------------------------------------------------------------------    
    $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result2->bindValue(1, $acolhimento_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);
    $acolhimento_responsavel = $resultado2['usuario_id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
//------------------------------------------------------------------------------
    $result3 = $db->prepare("SELECT *      
                            FROM mod_acolhimento_situacao mas   
                            WHERE mas.mod_acolhimento_id = ?");
    $result3->bindValue(1, $acolhimento_id);
    $result3->execute();
    $resultado3 = $result3->fetch(PDO::FETCH_ASSOC);
    $acolhimento_situacao_id = $resultado3['id'];
    $acolhimento_residia = $resultado3['residia'];
    $acolhimento_tempo_rua = $resultado3['tempo_rua'];
    $acolhimento_local = $resultado3['local'];
    $acolhimento_acolhida_anteriomente = $resultado3['acolhida_anteriomente'];
    $acolhimento_servico_acolhimento = $resultado3['servico_acolhimento'];
    $acolhimento_parentes_vinculos = $resultado3['parentes_vinculos'];
    $acolhimento_pessoas_vivia_rua = $resultado3['pessoas_vivia_rua'];
    $vf = $resultado3['vf'] == "" ? 0 : $resultado3['vf'];
//------------------------------------------------------------------------------
} else {
    $acolhimento_id = "";
    $acolhimento_situacao_id = "";
    $acolhimento_quem_trouxe = "";
    $opcao_conselho = 1;
    $comarca_id = "";
    $acolhimento_responsavel_entrega = "";
    $acolhimento_funcao_responsavel = "";
    $acolhimento_celular = "";
    $acolhimento_data_entrada = "";
    $acolhimento_hora_entrada = "";
    $acolhimento_profissional_id = "";
    $obs = "";
    $motivo_aba = "";
    $motivo_neg = "";
    $motivo_abu = "";
    $motivo_out = "";
    $descricao = "";
    $numero_guia = "";
    $numero_processo = "";
    $numero_destituicao = "";

    $acolhimento_residia = 0;
    $acolhimento_tempo_rua = "";
    $acolhimento_local = "";
    $acolhimento_acolhida_anteriomente = "";
    $acolhimento_servico_acolhimento = "";
    $acolhimento_parentes_vinculos = "";
    $acolhimento_pessoas_vivia_rua = "";
    $acolhimento_responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
    $vf = 0;
}
//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);
//------------------------------------------------------------------------------
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/pia/lista">PIA</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="text-center">PLANO INDIVIDUAL DE ATENDIMENTO - PIA</h4>

        <!-- Main content -->
        <section class="content">

            <!-- Step wizard -->
            <div id="impressao" class="box box-solid bg-info">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="box-title">PIA</h4>
                            <h6 class="box-subtitle">PLANO INDIVIDUAL DE ATENDIMENTO</h6>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-right"><?= $barra; ?>%</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $barra; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $barra; ?>%">
                                    <span class="sr-only"><?= $barra; ?>% Complete</span>
                                </div>
                            </div>
                        </div>      
                    </div>    
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="box-title">RESPONSÁVEL DO PIA</h4>

                            <div id="responsavel_edite_1">
                                <h6 class="subtitle"><?= $nome_responsavel; ?> <a href="#" class="edit-responsavel"><i onclick="editar_responsavel()" class="fal fa-pencil-alt"></i></a></h6>
                            </div>

                            <div id="responsavel_edite_2" style="display: none">
                                <form id="form_responsavel" name="form_responsavel" method="POST" action="#" class="tab-wizard wizard-circle">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required="true" id="responsavel_id" name="responsavel_id">
                                                <option value="">Selecione o responsável</option>
                                                <?php
                                                $result = $db->prepare("SELECT su.id, su.nome           
                                                            FROM seg_usuario su    
                                                            WHERE su.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_responsavel == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button href="#" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <a <?= is_numeric($vf) && $vf == 0 ? "" : "style='display: none'"; ?> href="#" onclick="validar(<?= $acolhimento_id; ?>)" class="btn-arquivo btn-success mt-2"><i class="fas fa-check"></i> VALIDAR</a>
                            <a <?= is_numeric($vf) && $vf == 1 ? "" : "style='display: none'"; ?> href="#" onclick="desvalidar(<?= $acolhimento_id; ?>)" class="btn-arquivo btn-warning mt-2"><i class="fas fa-remove"></i> DESVALIDAR</a>
                        </div>
                        <div class="col-md-2 text-right">
                            <a onclick="gerar_pdf()" style="cursor: pointer" class="btn-arquivo mt-2"><i class="fas fa-file-pdf"></i> GERAR PDF</a>
                        </div>
                    </div>   
                </div>
                <!-- /.box-header -->
                <div class="box-body wizard-content">

                    <?php include 'layout/menu_wizard.php'; ?>

                    <br/>

                    <form id="form_acolhimento" name="form_acolhimento" method="POST" action="#" class="tab-wizard wizard-circle">

                        <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>
                        <!-- Step 2 -->
                        <h6>ACOLHIMENTO</h6>
                        <section>
                            <!-- QUEM TROUXE -->
                            <div class="box box-outline-info">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>QUEM TROUXE?</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="radio" <?= $acolhimento_quem_trouxe == 2 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="conselho_tutelar" value="2">
                                            <label for="conselho_tutelar">CONSELHO TUTELAR</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="radio" <?= $acolhimento_quem_trouxe == 1 || $acolhimento_quem_trouxe == "" ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="juizado" value="1">
                                            <label for="juizado">PODER JUDICIÁRIO</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="radio" <?= $acolhimento_quem_trouxe == 3 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="policia_militar" value="3">
                                            <label for="policia_militar">POLÍCIAS</label>
                                        </div>
                                    </div>
                                    <hr>
                                    <div id="div_opcao_conselho" <?= $acolhimento_quem_trouxe == 2 ? "" : "style='display: none'" ?> class="row mt-3">
                                        <div class="col-md-3">
                                            <input type="radio" <?= $opcao_conselho == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_conselho" id="1conselho" value="1">
                                            <label for="1conselho">1º CONSELHO</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="radio" <?= $opcao_conselho == 2 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_conselho" id="2conselho" value="2">
                                            <label for="2conselho">2º CONSELHO</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="radio" <?= $opcao_conselho == 3 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_conselho" id="3conselho" value="3">
                                            <label for="3conselho">3º CONSELHO</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="radio" <?= $opcao_conselho == 4 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_conselho" id="conselhos_interior" value="4">
                                            <label for="conselhos_interior">CONSELHO DO INTERIOR</label>
                                        </div>
                                    </div>

                                    <div id="div_comarca_interior" <?= $acolhimento_quem_trouxe == 2 ? "" : "style='display: none'" ?> class="row mt-3">
                                        <div class="col-md-3">
                                            <div id="div_conselhos_interior" class="form-group">
                                                <label for="comarca_interior">COMARCAS</label>
                                                <select name="comarca_interior" id="comarca_interior" class="form-control select2">
                                                    <option value="">Selecione a Comarca</option>
                                                    <?php
                                                    $result = $db->prepare("SELECT *           
                                                            FROM mod_comarcas mc    
                                                            WHERE mc.status = 1");
                                                    $result->execute();
                                                    while ($comarcas = $result->fetch(PDO::FETCH_ASSOC)) {
                                                        if ($comarca_id == $comarcas['id']) {
                                                            ?>
                                                            <option selected="true" value="<?= $comarcas['id']; ?>"><?= $comarcas['nome']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="<?= $comarcas['id']; ?>"><?= $comarcas['nome']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div id="div_responsavel_entrega" class="form-group">
                                                <label for="responsavel_entrega">RESPONSÁVEL PELA ENTREGA DA CRIANÇA</label>
                                                <input type="text" class="form-control" name="responsavel_entrega" id="responsavel_entrega" placeholder="Responsável pela entraga da criança" value="<?= $acolhimento_responsavel_entrega; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_funcao_responsavel" class="form-group">
                                                <label for="funcao_responsavel">FUNÇÃO DO RESPONSÁVEL</label>
                                                <input type="text" class="form-control" name="funcao_responsavel" id="funcao_responsavel" placeholder="Função do Responsável" value="<?= $acolhimento_funcao_responsavel; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_contato_responsavel" class="form-group">
                                                <label for="contato_responsavel">CELULAR</label>
                                                <input type="text" class="form-control" name="contato_responsavel" id="contato_responsavel" data-mask="(99)99999-9999" placeholder="Celular do Responsável" value="<?= $acolhimento_celular; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIM QUEM TROUXE -->

                            <!-- ENTRADA -->
                            <div class="box box-outline-info">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>ENTRADA</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div id="div_data_entrada" class="form-group">
                                                <label for="data_entrada">DATA DE ENTRADA</label>
                                                <input class="form-control" name="data_entrada" id="data_entrada" type="date" value="<?= convertDataBR2ISO(obterDataBRTimestamp($acolhimento_data_entrada)); ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div i="div_hora_entrada" class="form-group">
                                                <label for="hora_entrada">HORA DE ENTRADA</label>
                                                <input type="text" name="hora_entrada" id="hora_entrada"  data-mask="99:99" class="form-control" value="<?= $acolhimento_hora_entrada; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div id="div_profissional" class="form-group">
                                                <label for="profissional">NOME DO PROFISSIONAL</label>
                                                <select disabled="true" name="profissional" id="profissional" class="form-control select2">
                                                    <?php
                                                    $registro_classe = "";
                                                    $setor_profissional = "";
                                                    $result = $db->prepare("SELECT sp.id, sp.registro, sp.nome AS profissional, ss.nome AS setor    
                                                            FROM seg_profissional sp 
                                                            LEFT JOIN seg_funcao AS fc ON fc.id = sp.funcao_id    
                                                            LEFT JOIN seg_setor AS ss ON ss.id = fc.setor_id  
                                                            WHERE sp.status = 1");
                                                    $result->execute();
                                                    while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
                                                        if ($acolhimento_profissional_id == $prof['id']) {
                                                            $setor_profissional = $prof['setor'];
                                                            $registro_classe = $prof['registro'];
                                                            ?>
                                                            <option selected="true" rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div id="div_profissional" class="form-group">
                                                <label for="profissional">FUNÇÃO</label>
                                                <select disabled="true" name="profissional" id="profissional" class="form-control select2">
                                                    <?php
                                                    $setor_profissional = "";
                                                    $result = $db->prepare("SELECT fc.id, fc.nome AS profissional, ss.nome AS setor          
                                                            FROM seg_funcao fc  
                                                            LEFT JOIN seg_setor AS ss ON ss.id = fc.setor_id  
                                                            WHERE fc.status = 1");
                                                    $result->execute();
                                                    while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
                                                        if ($acolhimento_profissional_id == $prof['id']) {
                                                            $setor_profissional = $prof['setor'];
                                                            ?>
                                                            <option selected="true" rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="setor_profissional">SETOR DO PROFISSIONAL</label>
                                                <input disabled="true" type="text" name="setor_profissional" id="setor_profissional" class="form-control" value="<?= $setor_profissional; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div id="div_bairro" class="form-group">
                                                <label for="registro_classe">REGISTRO DE CLASSE</label>
                                                <input disabled="true" type="text" class="form-control" name="registro_classe" id="registro_classe" placeholder="Informe o registro de classe" value="<?= $registro_classe; ?>">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="observacao_acolhimento">OBSERVAÇÃO(ÕES) SOBRE A CRIANÇA NO MOMENTO DO ACOLHIMENTO</label>
                                                <textarea name="observacao_acolhimento" id="observacao_acolhimento" class="form-control" cols="30" rows="10"><?= $obs; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- FIM ENTRADA -->

                            <!-- MOTIVO DO ACOLHIMENTO -->
                            <div class="box box-outline-info">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>MOTIVO DO ACOLHIMENTO</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="checkbox" id="abandono" name="abandono" class="filled-in chk-col-info" value="1" <?= $motivo_aba == 1 ? "checked='true'" : ""; ?> />
                                            <label for="abandono">ABANDONO</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="checkbox" id="negligencia" name="negligencia" class="filled-in chk-col-info" value="1" <?= $motivo_neg == 1 ? "checked='true'" : ""; ?> />
                                            <label for="negligencia">NEGLIGÊNCIA</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="checkbox" id="abuso_sexual" name="abuso_sexual" class="filled-in chk-col-info" value="1" <?= $motivo_abu == 1 ? "checked='true'" : ""; ?> />
                                            <label for="abuso_sexual">ABUSO SEXUAL</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="checkbox" id="outros" name="outros" class="filled-in chk-col-info" value="1" <?= $motivo_out == 1 ? "checked='true'" : ""; ?> />
                                            <label for="outros">OUTROS</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="descricao">DESCRIÇÃO</label>
                                                <textarea name="descricao" id="descricao" class="form-control" cols="30" rows="10"><?= $descricao; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- FIM MOTIVO DO ACOLHIMENTO -->

                            <!-- GUIA DE ACOLHIMENTO -->
                            <div class="box box-outline-info">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>DADOS PROCESSUAIS</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div id="div_numero_guia_acolhimento" class="form-group">
                                                <label for="numero_guia_acolhimento">Nº DA GUIA DE ACOLHIMENTO</label>
                                                <input type="text" class="form-control" name="numero_guia_acolhimento" id="numero_guia_acolhimento" placeholder="Número da guia de acolhimento" value="<?= $numero_guia; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_numero_processo" class="form-group">
                                                <label for="numero_processo">Nº DO PROCESSO</label>
                                                <input type="text" class="form-control" name="numero_processo" id="numero_processo" placeholder="Número do processo" value="<?= $numero_processo; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_numero_destituicao" class="form-group">
                                                <label for="numero_destituicao">Nº DA DESTITUIÇÃO</label>
                                                <input type="text" class="form-control" name="numero_destituicao" id="numero_destituicao" placeholder="Número da destituição" value="<?= $numero_destituicao; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIM GUIA DE ACOLHIMENTO -->

                            <!-- SOBRE A SITUAÇÃO DA CRIANÇA QUANDO ACOLHIDA -->
                            <div class="box box-outline-info">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>SOBRE A SITUAÇÃO DA CRIANÇA QUANDO ACOLHIDA</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">RESIDIA COM A FAMÍLIA?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $acolhimento_residia == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_residia" id="sim_residia" value="1">
                                                        <label for="sim_residia">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $acolhimento_residia == 1 ? "" : "checked='true'"; ?> class="with-gap radio-col-info" name="opcao_residia" id="nao_residia" value="0">
                                                        <label for="nao_residia">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div id="div_rua" <?= $acolhimento_residia == 1 ? "style='display: none'" : ""; ?> class="row">
                                        <div class="col-md-4">
                                            <div id="div_tempo_rua" class="form-group">
                                                <label for="tempo_rua">POR QUANTO TEMPO ESTEVE EM SITUAÇÃO DE RUA?</label>
                                                <select name="tempo_rua" id="tempo_rua" class="form-control select2">
                                                    <option value="">Selecione o tempo</option>
                                                    <?php
                                                    $result = $db->prepare("SELECT *           
                                                            FROM mod_acolhimento_tempo 
                                                            WHERE status = 1 
                                                            ORDER BY id ASC");
                                                    $result->execute();
                                                    while ($tempo = $result->fetch(PDO::FETCH_ASSOC)) {
                                                        if ($acolhimento_tempo_rua == $tempo['id']) {
                                                            ?>
                                                            <option selected="true" value="<?= $tempo['id']; ?>"><?= $tempo['nome']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="<?= $tempo['id']; ?>"><?= $tempo['nome']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_rua_local" <?= $acolhimento_residia == 1 ? "style='display: none'" : ""; ?> class="row">
                                        <div class="col-md-12">
                                            <div id="div_costumava_abrigar" class="form-group">
                                                <label for="costumava_abrigar">LOCAL ONDE COSTUMAVA SE ABRIGAR?</label>
                                                <input type="text" class="form-control" name="costumava_abrigar" id="costumava_abrigar" placeholder="Onde costumava se abrigar" value="<?= $acolhimento_local; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_rua_pessoas" <?= $acolhimento_residia == 1 ? "style='display: none'" : ""; ?> class="row">
                                        <div class="col-md-12">
                                            <div id="div_pessoas_vivia_rua" class="form-group">
                                                <label for="pessoas_vivia_rua">PESSOAS COM QUEM VIVIA NA RUA?</label>
                                                <input type="text" id="pessoas_vivia_rua" name="pessoas_vivia_rua" value="<?= $acolhimento_pessoas_vivia_rua; ?>" data-role="tagsinput" placeholder="Insira o nome">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="div_parentes_vinculo" class="form-group">
                                                <label for="parentes_vinculo">PARENTES/CONHECIDOS COM QUEM MANTINHA VÍCULOS</label>
                                                <input type="text" id="parentes_vinculo" name="parentes_vinculo" value="<?= $acolhimento_parentes_vinculos; ?>" data-role="tagsinput" placeholder="Insira o nome">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- FIM SOBRE A SITUAÇÃO DA CRIANÇA QUANDO ACOLHIDA -->

                            <!-- ACOLHIMENTOS ANTERIORES -->
                            <div class="box box-outline-info">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>ACOLHIMENTOS ANTERIORES</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">A CRIANÇA JÁ FOI ACOLHIDA ANTERIORMENTE?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $acolhimento_acolhida_anteriomente == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_acolhida_anterior" id="sim_acolhida_anterior" value="1">
                                                        <label for="sim_acolhida_anterior">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $acolhimento_acolhida_anteriomente == 1 ? "" : "checked='true'"; ?> class="with-gap radio-col-info" name="opcao_acolhida_anterior" id="nao_acolhida_anterior" value="0">
                                                        <label for="nao_acolhida_anterior">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="div_servico_acolhimento" <?= $acolhimento_acolhida_anteriomente == 1 ? "" : "style='display: none'" ?> class="form-group">
                                                <label for="servico_acolhimento">INFORME O SERVIÇO DE ACOLHIMENTO</label>
                                                <input type="text" class="form-control" name="servico_acolhimento" id="servico_acolhimento" placeholder="informe o serviço de acolhimento..." value="<?= $acolhimento_servico_acolhimento; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIM ACOLHIMENTOS ANTERIORES -->
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="row mb-2">
                <div class="col-md-12 text-center">
                    <button id="atualizar" type="button" class="btn btn-info">ATUALIZAR</button>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/pia/acolhimento.js"></script>