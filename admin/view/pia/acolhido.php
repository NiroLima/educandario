<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- CSS DO PLUGIN DE UPLOAD DE FOTOS -->
<link href="<?= PORTAL_URL; ?>assets/cropper/css/cropper.min.css" rel="stylesheet">
<link href="<?= PORTAL_URL; ?>assets/cropper/css/main.css" rel="stylesheet">

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
                            FROM mod_acolhimento ma   
                            WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result2->bindValue(1, $acolhimento_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $crianca_id = $resultado2['id'];
    $crianca_foto = $resultado2['crianca_foto'];
    $crianca_nome = $resultado2['nome'];
    $pai = $resultado2['pai'];
    $mae = $resultado2['mae'];
    $genero = $resultado2['genero'];
    $nascimento = $resultado2['nascimento'];
    $nascionalidade = $resultado2['nascionalidade'];
    $naturalidade = $resultado2['naturalidade'];
    $religiao = $resultado2['religiao_id'];
    $cor_id = $resultado2['cor_id'];
    $rua = $resultado2['rua'];
    $numero = $resultado2['numero'];
    $bairro = $resultado2['bairro'];
    $estado_id = $resultado2['estado_id'];
    $cidade_id = $resultado2['cidade_id'];
    $responsavel = $resultado2['usuario_id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $resultado2['usuario_id'], "");
    $vf = $resultado2['vf'] == "" ? 0 : $resultado2['vf'];
} else {
    $acolhimento_id = "";
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $crianca_foto = "";
    $crianca_id = "";
    $crianca_nome = "";
    $pai = "";
    $mae = "";
    $genero = "";
    $nascimento = "";
    $nascionalidade = "";
    $naturalidade = "";
    $religiao = "";
    $cor_id = "";
    $rua = "";
    $numero = "";
    $bairro = "";
    $estado_id = "";
    $cidade_id = "";
    $responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $resultado2['usuario_id'], "");
    $vf = 0;
}
//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);
//------------------------------------------------------------------------------
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/pia/lista">PIA</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="text-center">PLANO INDIVIDUAL DE ATENDIMENTO - PIA</h4>

        <div id="crop-avatar">
            <!-- Current avatar -->
            <div style="display: none" id="div_clicado" class="avatar-view" title="Trocar o Foto"></div>
            <!-- Cropping modal -->
            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">

                            <input type="hidden" id="crop_codigo" name="crop_codigo" value="<?= $acolhimento_id; ?>"/>

                            <div class="modal-header">
                                <button class="close" data-dismiss="modal" type="button">&times;</button>
                                <h4 class="modal-title" id="avatar-modal-label">Trocar Foto</h4>
                            </div>
                            <div class="modal-body">
                                <div class="avatar-body">

                                    <!-- Upload image and data -->
                                    <div class="avatar-upload">
                                        <input class="avatar-src" name="avatar_src" type="hidden">
                                        <input class="avatar-data" name="avatar_data" type="hidden">
                                        <label for="avatarInput">Local upload</label>
                                        <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
                                    </div>

                                    <!-- Crop and preview -->
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="avatar-wrapper"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="avatar-preview preview-lg"></div>
                                            <div class="avatar-preview preview-md"></div>
                                            <div class="avatar-preview preview-sm"></div>
                                        </div>
                                    </div>

                                    <div class="row avatar-btns">
                                        <div class="col-md-9">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
                                            </div>
                                            <div class="btn-group">
                                                <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-primary btn-block avatar-save" type="submit">Salvar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="modal-footer">
                              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div><!-- /.modal -->

            <!-- Loading state -->
            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
        </div>

        <!-- Main content -->
        <section class="content">

            <!-- Step wizard -->
            <div id="impressao" class="box box-solid bg-info">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="box-title">PIA</h4>
                            <h6 class="box-subtitle">PLANO INDIVIDUAL DE ATENDIMENTO</h6>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-right"><?= $barra; ?>%</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $barra; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $barra; ?>%">
                                    <span class="sr-only"><?= $barra; ?>% Complete</span>
                                </div>
                            </div>
                        </div>      
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="box-title">RESPONSÁVEL DO PIA</h4>

                            <div id="responsavel_edite_1">
                                <h6 class="subtitle"><?= $nome_responsavel; ?> <a href="#" class="edit-responsavel"><i onclick="editar_responsavel()" class="fal fa-pencil-alt"></i></a></h6>
                            </div>

                            <div id="responsavel_edite_2" style="display: none">
                                <form id="form_responsavel" name="form_responsavel" method="POST" action="#" class="tab-wizard wizard-circle">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required="true" id="responsavel_id" name="responsavel_id">
                                                <option value="">Selecione o responsável</option>
                                                <?php
                                                $result = $db->prepare("SELECT su.id, su.nome           
                                                            FROM seg_usuario su    
                                                            WHERE su.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($responsavel == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button href="#" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <a <?= $vf == 0 ? "" : "style='display: none'"; ?> href="#" onclick="validar(<?= $acolhimento_id; ?>)" class="btn-arquivo btn-success mt-2"><i class="fas fa-check"></i> VALIDAR</a>
                            <a <?= $vf == 1 ? "" : "style='display: none'"; ?> href="#" onclick="desvalidar(<?= $acolhimento_id; ?>)" class="btn-arquivo btn-warning mt-2"><i class="fas fa-remove"></i> DESVALIDAR</a>
                        </div>
                        <div class="col-md-2 text-right">
                            <a onclick="gerar_pdf()" style="cursor: pointer" class="btn-arquivo mt-2"><i class="fas fa-file-pdf"></i> GERAR PDF</a>
                        </div>
                    </div>   
                </div>
                <!-- /.box-header -->
                <div class="box-body wizard-content">

                    <?php include 'layout/menu_wizard.php'; ?>

                    <br/>

                    <form id="form_acolhido" name="form_acolhido" method="POST" action="#" class="tab-wizard wizard-circle">

                        <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>

                        <!-- Step 1 -->
                        <h6>ACOLHIDO(A)</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-3">

                                    <?php
                                    if ($crianca_foto != "" && file_exists($_SERVER["DOCUMENT_ROOT"] . "/educandario/" . str_replace("../../../", "", $crianca_foto))) {
                                        ?>
                                        <img title="Trocar a Foto" style="cursor: pointer; width: 100%;" id="click_foto" src="<?= "../" . $crianca_foto ?>" alt = "Avatar"/>
                                        <?php
                                    } else {
                                        ?>
                                        <img title="Trocar a Foto" style="cursor: pointer; width: 100%;" id="click_foto" src="<?= PORTAL_URL; ?>assets/avatar/picture.jpg" alt="Avatar"/>
                                        <?php
                                    }
                                    ?>

                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="div_crianca_nome" class="form-group">
                                                <label for="crianca_nome">NOME</label>
                                                <input type="text" class="form-control" name="crianca_nome" id="crianca_nome" placeholder="Nome Completo" value="<?= $crianca_nome; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="genero">GÊNERO</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_sexo" id="masculino" <?= $genero == 1 || $genero == "" ? "checked='true'" : ""; ?> value="1">
                                                        <label for="masculino">MASCULINO</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_sexo" id="feminino" <?= $genero == 2 ? "checked='true'" : ""; ?> value="2">
                                                        <label for="feminino">FEMININO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="div_nascimento" class="form-group">
                                                <label for="nascimento">NASCIMENTO</label>
                                                <input class="form-control" name="nascimento" id="nascimento" type="date" value="<?= $nascimento; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div id="div_nacionalidade" class="form-group">
                                                <label for="nacionalidade">NACIONALIDADE</label>
                                                <input type="text" class="form-control" name="nacionalidade" id="nacionalidade" placeholder="Nacionalidade" value="<?= $nascionalidade; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_naturalidade" class="form-group">
                                                <label for="naturalidade">NATURALIDADE</label>
                                                <input type="text" class="form-control" name="naturalidade" id="naturalidade" placeholder="Naturalidade" value="<?= $naturalidade; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="religiao">RELIGIÃO</label>
                                                <select name="religiao" id="religiao" class="form-control select2">
                                                    <option>Selecione a religião</option>
                                                    <?php
                                                    $result = $db->prepare("SELECT sr.id, sr.nome           
                                                            FROM seg_religiao sr   
                                                            WHERE sr.status = 1");
                                                    $result->execute();
                                                    while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                        if ($religiao == $rel['id']) {
                                                            ?>
                                                            <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- COR/ETNIA -->
                                <div class="box box-outline-info mt-3">
                                    <div class="box-header">
                                        <h5 class="box-title mb-0"><strong>COR/ETNIA</strong></h5>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 1 || $cor_id == "" ? "checked='true'" : ""; ?> name="opcao_cor" id="branca" value="1">
                                                <label for="branca">BRANCA</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 2 ? "checked='true'" : ""; ?> name="opcao_cor" id="preta" value="2">
                                                <label for="preta">PRETA</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 3 ? "checked='true'" : ""; ?> name="opcao_cor" id="parda" value="3">
                                                <label for="parda">PARDA</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 4 ? "checked='true'" : ""; ?> name="opcao_cor" id="amarela" value="4">
                                                <label for="amarela">AMARELA</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 5 ? "checked='true'" : ""; ?> name="opcao_cor" id="indigena" value="5">
                                                <label for="indigena">INDÍGENA</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- FILIACAO -->
                                <div class="box box-outline-info mt-2">
                                    <div class="box-header">
                                        <h5 class="box-title mb-0"><strong>FILIAÇÃO</strong></h5>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nome_pai">PAI</label>
                                                    <input type="text" class="form-control" name="nome_pai" id="nome_pai" placeholder="Nome completo do pai" value="<?= $pai; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nome_mae">MÃE</label>
                                                    <input type="text" class="form-control" name="nome_mae" id="nome_mae" placeholder="Nome completo da mãe" value="<?= $mae; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIM FILIACAO -->

                                <!-- ENDEREÇO -->
                                <div class="box box-outline-info mt-2">
                                    <div class="box-header">
                                        <h5 class="box-title mb-0"><strong>ENDEREÇO</strong></h5>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label for="rua">RUA</label>
                                                    <input type="text" class="form-control" name="rua" id="rua" placeholder="Rua, Avenida e etc..." value="<?= $rua ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="numero">NÚMERO</label>
                                                    <input type="text" class="form-control" name="numero" id="numero" placeholder="1.260" value="<?= $numero ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="bairro">BAIRRO</label>
                                                    <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro" value="<?= $bairro ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="estado">ESTADO</label>
                                                    <select name="estado" id="estado" class="form-control select2">
                                                        <option value="">Selecione o estado</option>
                                                        <?php
                                                        $result = $db->prepare("SELECT id, nome            
                                                                        FROM bsc_estado    
                                                                        WHERE 1 ");
                                                        $result->execute();
                                                        while ($estado = $result->fetch(PDO::FETCH_ASSOC)) {
                                                            if ($estado_id == $estado['id']) {
                                                                ?>
                                                                <option selected="true" value="<?= $estado['id']; ?>"><?= $estado['nome']; ?></option>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <option value="<?= $estado['id']; ?>"><?= $estado['nome']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="cidade">CIDADE</label>
                                                    <select name="cidade" id="cidade" class="form-control select2">
                                                        <option value="">Escolha primeiro o estado</option>
                                                        <?php
                                                        $result2 = $db->prepare("SELECT nome, id
                                                                     FROM bsc_cidade 
                                                                     WHERE 1 
                                                                     ORDER BY nome ASC");
                                                        $result2->execute();
                                                        while ($municipio = $result2->fetch(PDO::FETCH_ASSOC)) {
                                                            if ($cidade_id == $municipio['id']) {
                                                                ?>
                                                                <option selected="true" value='<?= $municipio['id']; ?>'><?= $municipio['nome']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIM ENDEREÇO -->
                            </div>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="row mb-2">
                <div class="col-md-12 text-center">
                    <button id="atualizar" type="button" class="btn btn-info">ATUALIZAR</button>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JAVASCRIPT BÁSICOS -->
<script src="<?= PORTAL_URL ?>assets/cropper/js/cropper.min.js"></script>
<script src="<?= PORTAL_URL ?>assets/cropper/js/main_servidor.js"></script>
<script src="<?= PORTAL_URL ?>assets/vendor_components/jquery-steps-master/build/jquery.steps.js"></script>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/pia/acolhido.js"></script>