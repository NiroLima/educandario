<?php

//------------------------------------------------------------------------------
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$id = $_POST['id'];
$opcao = $_POST['opcao'];

$stmp = $db->prepare("SELECT *           
                     FROM mod_acolhimento_crianca_plano macp   
                     WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = ?");
$stmp->bindValue(1, $id);
$stmp->bindValue(2, $opcao);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    
} else {
    while ($document = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '       <div id="div_remover_documentacao" class="box box-outline-primary">
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table table-monitoramento mb-2">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="3">
                                                                <strong class="text-primary">OBJETIVO</strong>
                                                                <p>' . $document['objetivo'] . '</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <strong class="text-primary">AÇÃO</strong>
                                                                <p>' . $document['acao'] . '</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <strong class="text-primary">RESPONSÁVEL</strong>
                                                                <p>' . $document['responsavel'] . '</p>
                                                            </td>
                                                            <td width="15%">
                                                                <strong class="text-primary">PRAZO INICIAL</strong>
                                                                <p>' . obterDataBRTimestamp($document['prazo_inicial']) . '</p>
                                                            </td>
                                                            <td width="15%">
                                                                <strong class="text-primary">PRAZO FINAL</strong>
                                                                <p>' . obterDataBRTimestamp($document['prazo_final']) . '</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <strong class="text-primary">MONITORAMENTO</strong>
                                                                <p ' . ($document['realizada'] == 1 ? "" : "style='display: none'") . '>Ação realizada</p>
                                                                <p ' . ($document['alcancado'] == 1 ? "" : "style='display: none'") . '>Objetivo alcançado</p>
                                                                <p ' . ($document['finalizada'] == 1 ? "" : "style='display: none'") . '>Pode ser finalizada</p>
                                                                <p ' . ($document['prazo'] == 1 ? "" : "style='display: none'") . '>Redefinir prazo</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                <p>' . $document['obs'] . '</p>
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-center">
                                                    <a style="cursor: pointer" onclick="editar(' . $opcao . ', ' . $document['id'] . ', ' . "'" . $document['objetivo'] . "'" . ', ' . "'" . $document['acao'] . "'" . ', ' . "'" . $document['responsavel'] . "'" . ', ' . "'" . $document['prazo_inicial'] . "'" . ', ' . "'" . $document['prazo_final'] . "'" . ', ' . "'" . $document['realizada'] . "'" . ', ' . "'" . $document['alcancado'] . "'" . ', ' . "'" . $document['finalizada'] . "'" . ', ' . "'" . $document['prazo'] . "'" . ', ' . "'" . $document['obs'] . "'" . ')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                    <a style="cursor: pointer" onclick="remover(' . $opcao . ', this, ' . $document['id'] . ')" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>';
    }
}
?>

