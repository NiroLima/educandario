<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$patrimonio = isset($_POST['patrimonio']) ? $_POST['patrimonio'] : NULL;
$codigo = isset($_POST['codigo']) ? $_POST['codigo'] : NULL;
$setor = isset($_POST['setor']) ? $_POST['setor'] : NULL;
$responsavel = isset($_POST['responsavel']) ? $_POST['responsavel'] : NULL;
$situacao = isset($_POST['situacao']) ? $_POST['situacao'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo2 = pesquisar("id", "mod_patrimonio", "nome", "=", $patrimonio, "");
if (is_numeric($codigo2) && $codigo2 != $id) {
    $error = true;
    echo "O nome do patrimônio informado já existe no sistema.";
    exit();
}

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo3 = pesquisar("id", "mod_patrimonio", "codigo", "=", $codigo, "");
if (is_numeric($codigo3) && $codigo3 != $id) {
    $error = true;
    echo "O código do patrimônio informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_patrimonio (nome, codigo, setor_id, responsavel_id, situacao, data_update, usuario_id, data_cadastro, status) VALUES (?, ?, ?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $patrimonio);
            $sql->bindValue(2, $codigo);
            $sql->bindValue(3, $setor);
			$sql->bindValue(4, $responsavel);
		    $sql->bindValue(5, $situacao);
            $sql->bindValue(6, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE mod_patrimonio SET nome = ?, codigo = ?, setor_id = ?, responsavel_id = ?, situacao = ?, usuario_id = ? WHERE id = ?");
            $sql->bindValue(1, $patrimonio);
            $sql->bindValue(2, $codigo);
            $sql->bindValue(3, $setor);
			$sql->bindValue(4, $responsavel);
		    $sql->bindValue(5, $situacao);
            $sql->bindValue(6, $_SESSION['id']);
            $sql->bindValue(7, $id);
            $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>