<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$diagnostico_id = isset($_POST['diagnostico_id']) && $_POST['diagnostico_id'] != "" ? $_POST['diagnostico_id'] : 0;
$terapia_id = isset($_POST['terapia_id']) && $_POST['terapia_id'] != "" ? $_POST['terapia_id'] : 0;
$data_diagnostico = isset($_POST['data_diagnostico']) ? $_POST['data_diagnostico'] : NULL;
$nome_profissional = isset($_POST['nome_profissional']) ? $_POST['nome_profissional'] : NULL;
$nome_acompanhante = isset($_POST['nome_acompanhante']) ? $_POST['nome_acompanhante'] : NULL;
$observacao_sessao = isset($_POST['observacao_sessao']) ? $_POST['observacao_sessao'] : NULL;

//if ($diagnostico_id == 0 && is_numeric(pesquisar2("id", "mod_saude_terapia_diagnostico", "profissional_id", "=", $nome_profissional, "terapia_id", "=", $terapia_id, "")) ||
//        $diagnostico_id == 0 && is_numeric(pesquisar2("id", "mod_saude_terapia_diagnostico", "profissional_id", "=", $nome_profissional, "responsavel_id", "=", $_SESSION['id'], "AND terapia_id IS NULL"))) {
//    $error = true;
//    echo "O profissional escolhido já está adicionado.";
//    exit();
//}
//
//if ($diagnostico_id == 0 && is_numeric(pesquisar2("id", "mod_saude_terapia_diagnostico", "acompanhante_id", "=", $nome_acompanhante, "terapia_id", "=", $terapia_id, "")) ||
//        $diagnostico_id == 0 && is_numeric(pesquisar2("id", "mod_saude_terapia_diagnostico", "acompanhante_id", "=", $nome_acompanhante, "responsavel_id", "=", $_SESSION['id'], "AND terapia_id IS NULL"))) {
//    $error = true;
//    echo "O acompanhante escolhido já está adicionado.";
//    exit();
//}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($diagnostico_id == 0) {

            if ($terapia_id == 0) {
                $sql = $db->prepare("INSERT INTO mod_saude_terapia_diagnostico (data_diagnostico, profissional_id, acompanhante_id, obs, responsavel_id, data_cadastro, data_update) VALUES (?, ?, ?, ?, ?, NOW(), NOW())");
                $sql->bindValue(1, $data_diagnostico);
                $sql->bindValue(2, $nome_profissional);
                $sql->bindValue(3, $nome_acompanhante);
                $sql->bindValue(4, $observacao_sessao);
                $sql->bindValue(5, $_SESSION['id']);
                $sql->execute();

                $diagnostico_id = $db->lastInsertId();
            } else {
                $sql = $db->prepare("INSERT INTO mod_saude_terapia_diagnostico (data_diagnostico, profissional_id, acompanhante_id, obs, terapia_id, responsavel_id, data_cadastro, data_update) VALUES (?, ?, ?, ?, ?, ?, NOW(), NOW())");
                $sql->bindValue(1, $data_diagnostico);
                $sql->bindValue(2, $nome_profissional);
                $sql->bindValue(3, $nome_acompanhante);
                $sql->bindValue(4, $observacao_sessao);
                $sql->bindValue(5, $terapia_id);
                $sql->bindValue(6, $_SESSION['id']);
                $sql->execute();

                $diagnostico_id = $db->lastInsertId();
            }
        } else {
            $sql = $db->prepare("UPDATE mod_saude_terapia_diagnostico SET data_diagnostico = ?, profissional_id = ?, acompanhante_id = ?, obs = ?, responsavel_id = ? WHERE id = ?");
            $sql->bindValue(1, $data_diagnostico);
            $sql->bindValue(2, $nome_profissional);
            $sql->bindValue(3, $nome_acompanhante);
            $sql->bindValue(4, $observacao_sessao);
            $sql->bindValue(5, $_SESSION['id']);
            $sql->bindValue(6, $diagnostico_id);
            $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>