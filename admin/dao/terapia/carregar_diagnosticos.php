<?php

//------------------------------------------------------------------------------
@session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$id = $_POST['id'];

$stmp = $db->prepare("SELECT mscd.id, mscd.data_diagnostico, mscd.profissional_id, mscd.acompanhante_id, mscd.obs, sf.nome AS especialidade, sf2.nome AS funcao            
                      FROM mod_saude_terapia_diagnostico mscd 
                      LEFT JOIN seg_profissional AS sp ON sp.id = mscd.profissional_id 
                      LEFT JOIN seg_funcao AS sf ON sf.id = sp.funcao_id 
                      LEFT JOIN seg_usuario AS su ON su.id = mscd.acompanhante_id
                      LEFT JOIN seg_funcao AS sf2 ON sf2.id = su.funcao_id  
                      WHERE mscd.terapia_id = ? OR mscd.terapia_id IS NULL AND mscd.responsavel_id = ?
                      GROUP BY mscd.id 
                      ORDER BY mscd.id ASC");
$stmp->bindValue(1, $id);
$stmp->bindValue(2, $_SESSION['id']);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    
} else {
    while ($diag = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr id="remover_diagnostico">
                     <th class="text-center">' . $diag['id'] . '</th>
                     <td>' . obterDataBRTimestamp($diag['data_diagnostico']) . '</td>
                     <td>' . pesquisar("nome", "seg_profissional", "id", "=", $diag['profissional_id'], "") . '</td>
                     <td>' . pesquisar("nome", "seg_usuario", "id", "=", $diag['acompanhante_id'], "") . '</td>
                     <td>' . $diag['obs'] . '</td>
                     <td width="100px">
                        <a style="cursor: pointer" id="editar" onclick="editar(' . $diag['id'] . ', ' . "'" . convertDataBR2ISO(obterDataBRTimestamp($diag['data_diagnostico'])) . "'" . ', ' . "'" . $diag['profissional_id'] . "'" . ', ' . "'" . $diag['especialidade'] . "'" . ', ' . "'" . $diag['acompanhante_id'] . "'" . ', ' . "'" . $diag['funcao'] . "'" . ', ' . "'" . $diag['obs'] . "'" . ')" class="text-warning"><i class="fa fa-pencil"></i></a>
                        <a style="cursor: pointer" id="remover" onclick="remover(this, ' . $diag['id'] . ')" class="text-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>';
    }
}
//------------------------------------------------------------------------------
?>

