<?php
@session_start();

include_once('conf/config.php');

// VERIFICAÇÕES DE SESSÕES
if (isset($_SESSION['id'])) {
    echo "<script>window.location = '" . PORTAL_URL . "admin/view/painel/dashboard';</script>";
    exit();
}
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= IMG_FOLDER; ?>favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= IMG_FOLDER; ?>favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= IMG_FOLDER; ?>favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= IMG_FOLDER; ?>favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= IMG_FOLDER; ?>favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= IMG_FOLDER; ?>favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= IMG_FOLDER; ?>favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= IMG_FOLDER; ?>favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= IMG_FOLDER; ?>favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= IMG_FOLDER; ?>favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= IMG_FOLDER; ?>favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= IMG_FOLDER; ?>favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= IMG_FOLDER; ?>favicon/favicon-16x16.png">
        <link rel="manifest" href="<?= IMG_FOLDER; ?>favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#000000">
        <meta name="msapplication-TileImage" content="<?= IMG_FOLDER; ?>favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#000000">

        <title>EDUCANDÁRIO SANTA MARGARIDA</title>

        <!-- Vendors Style-->
        <link rel="stylesheet" href="assets/css/vendors_css.css">

        <!-- Style-->  
        <link rel="stylesheet" href="<?= PORTAL_URL ?>assets/css/style.css">
        <link rel="stylesheet" href="<?= PORTAL_URL ?>assets/css/skin_color.css">
        <link rel="stylesheet" href="<?= PORTAL_URL ?>assets/fontawesome/css/all.css">
        <link rel="stylesheet" href="<?= PORTAL_URL ?>assets/fonts/fonts.css">

    </head>

    <body class="hold-transition theme-primary bg-img login" data-overlay="5">

        <div class="container h-p100">
            <div class="row align-items-center justify-content-md-center h-p100">	

                <div class="col-12">
                    <div class="row justify-content-center no-gutters">
                        <div class="col-lg-5 col-md-5 col-12">
                            <div class="bg-white shadow-lg">
                                <img src="<?= IMG_FOLDER ?>logo.svg" class="logo-login" alt="" />

                                <div class="p-40">
                                    <form id="form_login" name="form_login" method="post" action="#">
                                        <div class="form-group">
                                            <div id="div_login" class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text bg-transparent"><i class="fal fa-user"></i></span>
                                                </div>
                                                <input id="login" name="login" type="text" class="form-control pl-15 bg-transparent" placeholder="Login">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div id="div_senha" class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text  bg-transparent"><i class="fal fa-lock-alt"></i></span>
                                                </div>
                                                <input id="senha" name="senha" type="password" class="form-control pl-15 bg-transparent" placeholder="Senha">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6"></div>
                                            <!-- /.col -->
                                            <div class="col-12 text-center mt-20">
                                                <button type="submit" class="btn btn-info mt-10">ACESSAR</button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                    </form>
                                </div>						
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
    </body>
</html>

<!-- jQuery  -->
<script src="<?= PORTAL_URL; ?>assets/js/jquery-1.9.1.min.js"></script>
<!-- Livequery -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>assets/js/livequery.js"></script>
<!-- Vendor JS -->
<script src="<?= PORTAL_URL ?>js/vendors.min.js"></script>
<script src="<?= PORTAL_URL ?>assets/icons/feather-icons/feather.min.js"></script>

<!-- Utils Js -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>utils/utils.js"></script>
<script type="text/javascript" src="<?= PORTAL_URL ?>utils/projeto.utils.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#form_login').submit(function () {

            if (login_validator()) {
                $.ajax({
                    type: "POST",
                    url: PORTAL_URL + 'autenticar',
                    data: $('#form_login').serialize(),
                    cache: false,
                    success: function (obj) {
                        obj = JSON.parse(obj);
                        if (obj.msg == 'success') {
                            setTimeout("location.href='" + PORTAL_URL + "admin/view/painel/dashboard'", 1);
                        } else if (obj.msg == 'error') {
                            $('div#div_senha').after('<center><label id="erro_senha" style="color: red">' + obj.retorno + '</label></center>');
                        }
                    },
                    error: function (obj) {
                        alert(obj.retorno);
                    }
                });
                return false;
            } else {
                return false;
            }
        });
    });

    //VALIDATOR DO LOGIN
    function login_validator() {
        var valido = true;
        var login = $("#login").val();
        var senha = $("#senha").val();

        //LIMPA MENSAGENS DE ERRO
        $('label#erro_login').remove();
        $('label#erro_senha').remove();

        //VERIFICANDO SE OS CAMPOS LOGIN E SENHA FORAM INFORMADOS
        if (login == "") {
            $('div#div_login').after('<label id="erro_login" class="error" style="color: red; text-align: center; width: 100%;">O campo login é obrigatório.</label>');
            valido = false;
        }
        if (senha == "") {
            $('div#div_senha').after('<label id="erro_senha" class="error" style="color: red; text-align: center; width: 100%;">O campo senha é obrigatório.</label>');
            valido = false;
        }
        return valido;
    }
</script>
