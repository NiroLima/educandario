<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;

$tipo_atendimento = isset($_POST['opcao_retono_atendimento']) ? $_POST['opcao_retono_atendimento'] : 0;
$tipo = isset($_POST['opcao_retono_tipo']) ? $_POST['opcao_retono_tipo'] : 0;
$tipo_terapia = isset($_POST['opcao_retono_terapia']) ? $_POST['opcao_retono_terapia'] : 0;

$data_terapia = isset($_POST['data_terapia']) ? $_POST['data_terapia'] : NULL;
$hora_terapia = isset($_POST['hora_terapia']) ? $_POST['hora_terapia'] : NULL;
$observacao = isset($_POST['observacao']) ? $_POST['observacao'] : NULL;
$nome_crianca = isset($_POST['nome_crianca']) ? $_POST['nome_crianca'] : NULL;
$nome_acompanhante = isset($_POST['nome_acompanhante']) ? $_POST['nome_acompanhante'] : NULL;
$preparativos = isset($_POST['preparativos']) ? $_POST['preparativos'] : NULL;
$nome_profissional = isset($_POST['nome_profissional']) ? $_POST['nome_profissional'] : NULL;
$unidade_saude = isset($_POST['unidade_saude']) ? $_POST['unidade_saude'] : NULL;
$endereco_unidade_saude = isset($_POST['endereco_unidade_saude']) ? $_POST['endereco_unidade_saude'] : NULL;
$numero_unidade_saude = isset($_POST['numero_unidade_saude']) ? $_POST['numero_unidade_saude'] : NULL;
$bairro_unidade_saude = isset($_POST['bairro_unidade_saude']) ? $_POST['bairro_unidade_saude'] : NULL;
$contato = isset($_POST['contato_unidade_saude']) ? $_POST['contato_unidade_saude'] : NULL;
$opniao_medica = isset($_POST['opniao_medica']) ? $_POST['opniao_medica'] : NULL;

$vacinas_aplicadas = isset($_POST['vacinas_aplicadas']) ? $_POST['vacinas_aplicadas'] : NULL;

$avaliacao_inicial = isset($_POST['avaliacao_inicial']) ? $_POST['avaliacao_inicial'] : NULL;
$diagnostico_inicial = isset($_POST['diagnostico_inicial']) ? $_POST['diagnostico_inicial'] : NULL;
$observacoes_inicial = isset($_POST['observacoes_inicial']) ? $_POST['observacoes_inicial'] : NULL;
$avaliacao_final = isset($_POST['avaliacao_final']) ? $_POST['avaliacao_final'] : NULL;
$diagnostico_final = isset($_POST['diagnostico_final']) ? $_POST['diagnostico_final'] : NULL;
$observacoes_final = isset($_POST['observacoes_final']) ? $_POST['observacoes_final'] : NULL;

$especialistas = isset($_POST['especialistas']) ? $_POST['especialistas'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_saude_terapias (tipo_terapia, data_terapia, hora_terapia, obs, crianca_id, acomp_id, preparativos, profissional_id, unidade_id, endereco, numero, bairro, contato, diagnostico, avaliacao_inicial, diagnostico_inicial, obs_inicial, avaliacao_final, diagnostico_final, obs_final, data_update, responsavel_id, data_cadastro, status, situacao) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, NOW(), 1, 1)");
            $sql->bindValue(1, $tipo_terapia);
            $sql->bindValue(2, $data_terapia);
            $sql->bindValue(3, $hora_terapia);
            $sql->bindValue(4, $observacao);
            $sql->bindValue(5, $nome_crianca);
            $sql->bindValue(6, $nome_acompanhante);
            $sql->bindValue(7, $preparativos);
            $sql->bindValue(8, $nome_profissional);
            $sql->bindValue(9, $unidade_saude);
            $sql->bindValue(10, $endereco_unidade_saude);
            $sql->bindValue(11, $numero_unidade_saude);
            $sql->bindValue(12, $bairro_unidade_saude);
            $sql->bindValue(13, $contato);
            $sql->bindValue(14, $opniao_medica);
            $sql->bindValue(15, $avaliacao_inicial);
            $sql->bindValue(16, $diagnostico_inicial);
            $sql->bindValue(17, $observacoes_inicial);
            $sql->bindValue(18, $avaliacao_final);
            $sql->bindValue(19, $diagnostico_final);
            $sql->bindValue(20, $observacoes_final);
            $sql->bindValue(21, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();

//            $stmp = $db->prepare("SELECT *           
//                                  FROM mod_saude_terapia_diagnostico mscd   
//                                  WHERE mscd.terapia_id IS NULL AND mscd.responsavel_id = ?");
//            $stmp->bindValue(1, $_SESSION['id']);
//            $stmp->execute();
//            while ($diag = $stmp->fetch(PDO::FETCH_ASSOC)) {
//                $sql = $db->prepare("UPDATE mod_saude_terapia_diagnostico SET terapia_id = ? WHERE id = ?");
//                $sql->bindValue(1, $id);
//                $sql->bindValue(2, $diag['id']);
//                $sql->execute();
//            }
        } else {
            $sql = $db->prepare("UPDATE mod_saude_terapias SET tipo_terapia = ?, data_terapia = ?, hora_terapia = ?, obs = ?, crianca_id = ?, acomp_id = ?, preparativos = ?, profissional_id = ?, unidade_id = ?, endereco = ?, numero = ?, bairro = ?, contato = ?, diagnostico = ?, avaliacao_inicial = ?, diagnostico_inicial = ?, obs_inicial = ?, avaliacao_final = ?, diagnostico_final = ?, obs_final = ?, responsavel_id = ? WHERE id = ?");
            $sql->bindValue(1, $tipo_terapia);
            $sql->bindValue(2, $data_terapia);
            $sql->bindValue(3, $hora_terapia);
            $sql->bindValue(4, $observacao);
            $sql->bindValue(5, $nome_crianca);
            $sql->bindValue(6, $nome_acompanhante);
            $sql->bindValue(7, $preparativos);
            $sql->bindValue(8, $nome_profissional);
            $sql->bindValue(9, $unidade_saude);
            $sql->bindValue(10, $endereco_unidade_saude);
            $sql->bindValue(11, $numero_unidade_saude);
            $sql->bindValue(12, $bairro_unidade_saude);
            $sql->bindValue(13, $contato);
            $sql->bindValue(14, $opniao_medica);
            $sql->bindValue(15, $avaliacao_inicial);
            $sql->bindValue(16, $diagnostico_inicial);
            $sql->bindValue(17, $observacoes_inicial);
            $sql->bindValue(18, $avaliacao_final);
            $sql->bindValue(19, $diagnostico_final);
            $sql->bindValue(20, $observacoes_final);
            $sql->bindValue(21, $_SESSION['id']);
            $sql->bindValue(22, $id);
            $sql->execute();
        }

        //ESPECIALISTAS
        $sql = $db->prepare("DELETE FROM mod_saude_terapia_especialistas WHERE terapia_id = ?");
        $sql->bindValue(1, $id);
        $sql->execute();

        if ($especialistas != null) {
            foreach ($especialistas AS $key => $val) {
                if ($val != "" && $val != null && is_numeric($val)) {
                    $sql = $db->prepare("INSERT INTO mod_saude_terapia_especialistas (terapia_id, especialista_id) VALUES (?, ?)");
                    $sql->bindValue(1, $id);
                    $sql->bindValue(2, $val);
                    $sql->execute();
                }
            }
        }

        $db->commit();

        $msg['id'] = $id;
        $msg['msg'] = 'success';
        $msg['retorno'] = 'Terapia marcada com sucesso!';
        echo json_encode($msg);
        exit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>