<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
                            FROM mod_acolhimento ma   
                            WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result2->bindValue(1, $acolhimento_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_id = $resultado2['id'];
    $crianca_id = $resultado2['id'];
    $qual_deficiencia = $resultado2['qual_deficiencia'];
    $necessita_equipamentos = $resultado2['necessita_equipamentos'];
    $qual_tratamento = $resultado2['qual_tratamento'];
    $indicio_disturbio = $resultado2['indicio_disturbio'];
    $qual_doenca_infectocontagiosa = $resultado2['qual_doenca_infectocontagiosa'];
    $obs_saude = $resultado2['obs_saude'];
    $acolhimento_responsavel = $resultado2['usuario_id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
//------------------------------------------------------------------------------
//Resultado do mod_acolhimento_crianca_geral
    $result3 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca_geral macg   
                            WHERE macg.acolhimento_crianca_id = ?");
    $result3->bindValue(1, $acolhimento_crianca_id);
    $result3->execute();
    $resultado3 = $result3->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_geral_id = $resultado3['id'];
    $peso = $resultado3['peso'];
    $altura = $resultado3['altura'];
    $tipo_sanguineo = $resultado3['tipo_sanguineo'];
    $condicoes_saude = $resultado3['condicao_geral'];
    $atvd_psicologia = $resultado3['atvd'];
    $justificativa = $resultado3['justificativa'];
    $atividade_psicologia = $resultado3['atividade_psicologia'];
    $vf = $resultado3['vf'] == "" ? 0 : $resultado3['vf'];
    $def_descricao = $resultado3['def_descricao'];
    $bcp = $resultado3['def_beneficiaria'];
    $responsavel_recebimento = $resultado3['def_responsavel_recebimento'];
} else {
    $acolhimento_id = "";
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $acolhimento_crianca_id = "";
    $qual_deficiencia = "";
    $def_descricao = "";
    $necessita_equipamentos = "";
    $qual_tratamento = "";
    $indicio_disturbio = "";
    $qual_doenca_infectocontagiosa = "";
    $obs_saude = "";
//------------------------------------------------------------------------------   
//Resultado do mod_acolhimento_crianca_geral
    $acolhimento_crianca_geral_id = "";
    $peso = "";
    $altura = "";
    $tipo_sanguineo = "";
    $condicoes_saude = "";
    $atvd_psicologia = 0;
    $justificativa = "";
    $atividade_psicologia = 0;
    $acolhimento_responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
    $vf = 0;
    $bcp = "";
    $responsavel_recebimento = "";
}
//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);
//------------------------------------------------------------------------------
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/pia/lista">PIA</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="text-center">PLANO INDIVIDUAL DE ATENDIMENTO - PIA</h4>

        <!-- Main content -->
        <section class="content">

            <!-- Step wizard -->
            <div id="impressao" class="box box-solid bg-info">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="box-title">PIA</h4>
                            <h6 class="box-subtitle">PLANO INDIVIDUAL DE ATENDIMENTO</h6>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-right"><?= $barra; ?>%</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $barra; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $barra; ?>%">
                                    <span class="sr-only"><?= $barra; ?>% Complete</span>
                                </div>
                            </div>
                        </div>      
                    </div> 
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="box-title">RESPONSÁVEL DO PIA</h4>

                            <div id="responsavel_edite_1">
                                <h6 class="subtitle"><?= $nome_responsavel; ?> <a href="#" class="edit-responsavel"><i onclick="editar_responsavel()" class="fal fa-pencil-alt"></i></a></h6>
                            </div>

                            <div id="responsavel_edite_2" style="display: none">
                                <form id="form_responsavel" name="form_responsavel" method="POST" action="#" class="tab-wizard wizard-circle">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required="true" id="responsavel_id" name="responsavel_id">
                                                <option value="">Selecione o responsável</option>
                                                <?php
                                                $result = $db->prepare("SELECT su.id, su.nome           
                                                            FROM seg_usuario su    
                                                            WHERE su.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_responsavel == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button href="#" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <a <?= is_numeric($vf) && $vf == 0 ? "" : "style='display: none'"; ?> href="#" onclick="validar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-success mt-2"><i class="fas fa-check"></i> VALIDAR</a>
                            <a <?= is_numeric($vf) && $vf == 1 ? "" : "style='display: none'"; ?> href="#" onclick="desvalidar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-warning mt-2"><i class="fas fa-remove"></i> DESVALIDAR</a>
                        </div>
                        <div class="col-md-2 text-right">
                            <a onclick="gerar_pdf()" style="cursor: pointer" class="btn-arquivo mt-2"><i class="fas fa-file-pdf"></i> GERAR PDF</a>
                        </div>
                    </div>  
                </div>
                <!-- /.box-header -->
                <div class="box-body wizard-content">

                    <?php include 'layout/menu_wizard.php'; ?>

                    <br/>

                    <form id="form_saude" name="form_saude" method="POST" action="#" class="tab-wizard wizard-circle">

                        <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>
                        <input type="hidden" id="acolhimento_crianca_id" name="acolhimento_crianca_id" value="<?= $acolhimento_crianca_id; ?>"/>
                        <input type="hidden" id="acolhimento_crianca_geral_id" name="acolhimento_crianca_geral_id" value="<?= $acolhimento_crianca_geral_id; ?>"/>

                        <h6>SAÚDE</h6>
                        <section>
                            <!-- DEFICIENCIA -->
                            <div class="box box-outline-info mt-2">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>NECESSIDADES ESPECÍFICAS DE SAÚDE</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="genero">POSSUI ALGUMA DEFICIÊNCIA E/OU TRANSTORNO?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_possui_def" <?= $qual_deficiencia == 1 || $qual_deficiencia == "" ? "checked='checked'" : ""; ?> id="possui_def_sim" value="1">
                                                        <label for="possui_def_sim">SIM, COM LAUDO</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_possui_def" <?= $qual_deficiencia == 0 ? "checked='checked'" : ""; ?> id="possui_def_nao" value="0">
                                                        <label for="possui_def_nao">NÃO SE APLICA</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_possui_def" <?= $qual_deficiencia == 2 ? "checked='checked'" : ""; ?> id="em_def_investigacao" value="2">
                                                        <label for="em_def_investigacao">EM INVESTIGAÇÃO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_laudo" <?= $qual_deficiencia == 1 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <label class="col-form-label">LAUDO(S)</label>
                                            <div class="form-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="customFileLaudo" name="customFileLaudo" multiple="true">
                                                    <label class="custom-file-label" for="customFile">Selecione o arquivo</label>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>

                                    <div id="div_descricao_def" <?= $qual_deficiencia == 1 || $qual_deficiencia == 2 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="descreva_tipo_def">DESCREVA</label>
                                                <textarea name="descreva_tipo_def" id="descreva_tipo_def" class="form-control" placeholder="Descreva..." cols="30" rows="10"><?= $def_descricao ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="genero">A CRIANÇA É BENEFICIÁRIA DO BENEFÍCIO DE PRESTAÇÃO CONTINUADA (BPC)?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="bcp" <?= $bcp == 1 || $bcp == "" ? "checked='checked'" : ""; ?> id="bcp_sim" value="1">
                                                        <label for="bcp_sim">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="bcp" <?= $bcp == 0 ? "checked='checked'" : ""; ?> id="bcp_nao" value="0">
                                                        <label for="bcp_nao">NÃO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div <?= $bcp == 1 || $bcp == "" ? "" : "style='display: none'"; ?> id="div_responsavel_recebimento">
                                                <div class="form-group">
                                                    <label for="responsavel_recebimento">QUEM É O RESPONSÁVEL PELO RECEBIMENTO?</label>
                                                    <input type="text" class="form-control" name="responsavel_recebimento" id="responsavel_recebimento" placeholder="Quem é o responsável pelo recebimento..." value="<?= $responsavel_recebimento ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="recursos">NECESSITA DE EQUIPAMENTOS/RECURSOS DE TECNOLOGIA ASSISTIVA?</label>
                                                <textarea name="recursos" id="recursos" class="form-control" placeholder="Recursos e Equipamentos" cols="30" rows="10"><?= $necessita_equipamentos; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- FIM DEFICIÊNCIA -->

                            <!-- SAÚDE -->
                            <div class="box box-outline-info mt-2">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>TRATAMENTO</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">FAZ TRATAMENTO MÉDICO?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_tratamento" <?= $qual_tratamento == "" ? "" : "checked='true'"; ?> id="sim_tratamento">
                                                        <label for="sim_tratamento">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_tratamento" <?= $qual_tratamento == "" ? "checked='true'" : ""; ?> id="nao_tratamento">
                                                        <label for="nao_tratamento">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div id="div_qual_tratamento" <?= $qual_tratamento == "" ? "style='display: none'" : ""; ?> class="col-md-8">
                                            <div class="form-group">
                                                <label for="qual_tratamento">QUAL TRATAMENTO?</label>
                                                <input type="text" class="form-control" name="qual_tratamento" id="qual_tratamento" placeholder="Informe o tratamento" value="<?= $qual_tratamento; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">INDÍCIOS DE DISTÚRBIO MENTAL?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" <?= $indicio_disturbio == 1 ? "checked='true'" : ""; ?> name="opcao_indicios" id="sim_indicios" value="1">
                                                        <label for="sim_indicios">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" <?= $indicio_disturbio == 1 ? "" : "checked='true'"; ?> name="opcao_indicios" id="nao_indicios" value="2">
                                                        <label for="nao_indicios">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">POSSUI DOENÇA INFECTOCONTAGIOSA?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" <?= $qual_doenca_infectocontagiosa == "" ? "" : "checked='true'"; ?> name="opcao_doenca" id="sim_doenca">
                                                        <label for="sim_doenca">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" <?= $qual_doenca_infectocontagiosa == "" ? "checked='true'" : ""; ?> name="opcao_doenca" id="nao_doenca">
                                                        <label for="nao_doenca">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div id="div_infectocontagiosa" <?= $qual_doenca_infectocontagiosa == "" ? "style='display: none'" : ""; ?> class="col-md-8">
                                            <div class="form-group">
                                                <label for="qual_doenca_infectocontagiosa">QUAL DOENÇA INFECTOCONTAGIOSA?</label>
                                                <input type="text" class="form-control" name="qual_doenca_infectocontagiosa" id="qual_doenca_infectocontagiosa" placeholder="Informe a doença infectocontagiosa" value="<?= $qual_doenca_infectocontagiosa; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $result3 = $db->prepare("SELECT mm.id, mm.nome           
                                                    FROM mod_medicamento mm   
                                                    WHERE mm.status = 1 AND mm.id IN (SELECT medicamento_id FROM mod_acolhimento_crianca_medicacao WHERE acolhimento_crianca_id = ?)");
                                    $result3->bindValue(1, $crianca_id);
                                    $result3->execute();
                                    $qtd_medicamento = $result3->rowCount();
                                    ?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">VIERAM MEDICAÇÕES COM A CRIANÇA?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $qtd_medicamento > 0 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_medicacao" id="sim_medicacoes">
                                                        <label for="sim_medicacoes">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $qtd_medicamento > 0 ? "" : "checked='true'"; ?> class="with-gap radio-col-info" name="opcao_medicacao" id="nao_medicacoes">
                                                        <label for="nao_medicacoes">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div id="div_medicacao_crianca" <?= $qtd_medicamento > 0 ? "" : "style='display: none'"; ?> class="col-md-8">
                                            <div class="form-group">
                                                <label for="qual_medicamentos">QUAL(IS) MEDICAMENTOS?</label>
                                                <select name="qual_medicamentos" id="qual_medicamentos" class="form-control select2" multiple="true">
                                                    <?php
                                                    while ($medicamentos = $result3->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                        <option selected="true" value="<?= $medicamentos['id']; ?>"><?= $medicamentos['nome']; ?></option>
                                                        <?php
                                                    }

                                                    $result33 = $db->prepare("SELECT mm.id, mm.nome           
                                                    FROM mod_medicamento mm   
                                                    WHERE mm.status = 1 AND mm.id NOT IN (SELECT medicamento_id FROM mod_acolhimento_crianca_medicacao WHERE acolhimento_crianca_id = ?)");
                                                    $result33->bindValue(1, $crianca_id);
                                                    $result33->execute();
                                                    while ($medicamentos = $result33->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                        <option value="<?= $medicamentos['id']; ?>"><?= $medicamentos['nome']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="obs_saude">OBSERVAÇÃO</label>
                                                <textarea name="obs_saude" id="obs_saude" class="form-control" placeholder="Observação sobre a saúde da Criança" cols="30" rows="10"><?= $obs_saude; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIM SAÚDE -->

                            <!-- SAÚDE -->
                            <div class="box box-outline-info mt-2">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>DADOS GERAIS DE SAÚDE</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div id="div_peso" class="form-group">
                                                <label for="peso">PESO(Kg)</label>
                                                <input type="text" class="form-control" name="peso" id="peso" placeholder="Peso" value="<?= $peso; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_altura" class="form-group">
                                                <label for="altura">ALTURA(Cm)</label>
                                                <input type="text" class="form-control" name="altura" id="altura" placeholder="Altura" value="<?= $altura; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_tipo_sanguineo" class="form-group">
                                                <label for="tipo_sanguineo">TIPO SANGUÍNEO (TIPO/ FATOR RH):</label>
                                                <input type="text" class="form-control" name="tipo_sanguineo" id="tipo_sanguineo" placeholder="Tipo sanguíneo" value="<?= $tipo_sanguineo; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="condicoes_saude">CONDIÇÕES GERAIS DE SAÚDE DA CRIANÇA?</label>
                                                <textarea name="condicoes_saude" id="condicoes_saude" class="form-control" placeholder="Condições gerais de saúde da criança" cols="30" rows="10"><?= $condicoes_saude; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="genero">A CRIANÇA FAZ USO DE ALGUMA MEDICAÇÃO? </label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $atividade_psicologia == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_atividade_psicologia" id="sim_atividade_psicologia" value="1">
                                                        <label for="sim_atividade_psicologia">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $atividade_psicologia == 1 ? "" : "checked='true'"; ?> class="with-gap radio-col-info" name="opcao_atividade_psicologia" id="nao_atividade_psicologia" value="0">
                                                        <label for="nao_atividade_psicologia">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_atividade_psicologica" <?= $atividade_psicologia == 1 ? "" : "style='display: none'"; ?> class="row">
                                        <div class="col-md-12">
                                            <div class="box box-outline-info">
                                                <div class="box-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-2">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th scope="col">MÉDICO</th>
                                                                    <th scope="col">MEDICAMENTO</th>
                                                                    <th scope="col">POSOLOGIA</th>
                                                                    <th scope="col">ADMINISTRAÇÃO</th>
                                                                    <th scope="col"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="resultado_servico">
                                                                <?php
                                                                $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_servico macs   
                                                                                        WHERE macs.acolhimento_id = ?");
                                                                $result->bindValue(1, $acolhimento_id);
                                                                $result->execute();
                                                                while ($servicos = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                    ?>
                                                                    <tr id="remover_servico">
                                                                        <td><?= $servicos['medico']; ?></td>
                                                                        <td><?= $servicos['medicamento']; ?></td>
                                                                        <td><?= $servicos['posologia']; ?></td>
                                                                        <td><?= $servicos['administracao']; ?></td>
                                                                        <td width="100px">
                                                                            <a style="cursor: pointer" onclick="editar(<?= $servicos['id']; ?>, '<?= $servicos['medico']; ?>', '<?= $servicos['medicamento']; ?>', '<?= $servicos['posologia']; ?>', '<?= $servicos['administracao']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                            <a style="cursor: pointer" onclick="remover(this, <?= $servicos['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-3">
                                                            <div id="div_nome_medico" class="form-group">
                                                                <label for="nome_medico">MÉDICO</label>
                                                                <input type="hidden" id="servico_id" name="servico_id" value=""/>
                                                                <input type="text" class="form-control" name="nome_medico" id="nome_medico" placeholder="Nome do Médico" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_medicamento" class="form-group">
                                                                <label for="medicamento">MEDICAMENTO</label>
                                                                <input type="text" class="form-control" name="medicamento" id="medicamento" placeholder="Medicamento" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_posologia" class="form-group">
                                                                <label for="posologia">POSOLOGIA</label>
                                                                <input type="text" class="form-control" name="posologia" id="posologia" placeholder="Posologia" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_administracao" class="form-group">
                                                                <label for="administracao">ADMINISTRAÇÃO</label>
                                                                <input type="text" class="form-control" name="administracao" id="administracao" placeholder="Administração" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_servico" name="inserir_servico" type="button" class="btn btn-success">INSERIR</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="genero">A CRIANÇA ESTÁ INSERIDA EM ALGUMA ATIVIDADE DO SERVIÇO DE PSICOLOGIA? </label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $atvd_psicologia == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_atvd_psicologia" id="sim_atvd_psicologia" value="1">
                                                        <label for="sim_atvd_psicologia">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $atvd_psicologia == 1 ? "" : "checked='true'"; ?> class="with-gap radio-col-info" name="opcao_atvd_psicologia" id="nao_atvd_psicologia" value="0">
                                                        <label for="nao_atvd_psicologia">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="justificativa">JUSTIFIQUE/ESPECIFIQUE</label>
                                                <textarea name="justificativa" id="justificativa" class="form-control" placeholder="Observação sobre a saúde da Criança" cols="30" rows="10"><?= $justificativa; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="row mb-2">
                <div class="col-md-12 text-center">
                    <button id="atualizar" type="button" class="btn btn-info">ATUALIZAR</button>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/pia/saude.js"></script>