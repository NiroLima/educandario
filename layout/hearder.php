<?php
vf_usuario_login(); //VERIFICA SE O USUÁRIO ESTÁ LOGADO PELA SESSION
?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="apple-touch-icon" sizes="57x57" href="<?= IMG_FOLDER; ?>favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= IMG_FOLDER; ?>favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= IMG_FOLDER; ?>favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= IMG_FOLDER; ?>favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= IMG_FOLDER; ?>favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= IMG_FOLDER; ?>favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= IMG_FOLDER; ?>favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= IMG_FOLDER; ?>favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= IMG_FOLDER; ?>favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= IMG_FOLDER; ?>favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= IMG_FOLDER; ?>favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= IMG_FOLDER; ?>favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= IMG_FOLDER; ?>favicon/favicon-16x16.png">
        <link rel="manifest" href="<?= IMG_FOLDER; ?>favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#000000">
        <meta name="msapplication-TileImage" content="<?= IMG_FOLDER; ?>favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#000000">

        <title>EDUCANDÁRIO SANTA MARGARIDA :: RIO BRANCO - AC</title>

        <!-- Vendors Style-->
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/css/vendors_css.css">

        <!-- Sweet Alert -->
        <link href="<?= PORTAL_URL; ?>assets/vendor_components/sweetalert/sweetalert2.min.css" rel="stylesheet" type="text/css">

        <!-- Style-->  
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/css/style.css">
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/css/skin_color.css">
        <link rel="stylesheet" href="<?= PORTAL_URL; ?>assets/fontawesome/css/all.css">
        <link rel="stylesheet" href="<?= PORTAL_URL ?>assets/fonts/fonts.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.5.3/css/perfect-scrollbar.min.css">

    </head>

    <body class="hold-transition light-skin sidebar-mini theme-primary">

        <div class="wrapper">

            <header class="main-header">
                <div class="d-flex align-items-center logo-box justify-content-between">
                    <a href="#" class="waves-effect waves-light nav-link rounded d-none d-md-inline-block mx-10 push-btn" data-toggle="push-menu" role="button">
                        <i class="fal fa-bars"></i>
                    </a>	
                    <!-- Logo -->
                    <a href="<?= PORTAL_URL; ?>admin/view/painel/dashboard" class="logo">
                        <!-- logo-->
                        <div class="logo-lg">
                            <span class="light-logo"><img src="<?= IMG_FOLDER; ?>logo.png" alt="logo"></span>
                            <span class="dark-logo"><img src="<?= IMG_FOLDER; ?>logo.png" alt="logo"></span>
                        </div>
                    </a>	
                </div>  
                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top pl-10">
                    <!-- Sidebar toggle button-->
                    <div class="app-menu">
                        <!-- <ul class="header-megamenu nav">
                                <li class="btn-group nav-item d-md-none">
                                        <a href="#" class="waves-effect waves-light nav-link rounded push-btn" data-toggle="push-menu" role="button">
                                                <i class="ti-menu"></i>
                                    </a>
                                </li>
                                <li class="btn-group nav-item d-none d-xl-inline-block">
                                        <a href="contact_app_chat.html" class="waves-effect waves-light nav-link rounded svg-bt-icon" title="">
                                                <i class="ti-comments"></i>
                                    </a>
                                </li>
                                <li class="btn-group nav-item d-none d-xl-inline-block">
                                        <a href="mailbox.html" class="waves-effect waves-light nav-link rounded svg-bt-icon" title="">
                                                <i class="ti-email"></i>
                                    </a>
                                </li>
                                <li class="btn-group nav-item d-none d-xl-inline-block">
                                        <a href="extra_taskboard.html" class="waves-effect waves-light nav-link rounded svg-bt-icon" title="">
                                                <i class="ti-check-box"></i>
                                    </a>
                                </li>
                                <li class="btn-group nav-item d-none d-xl-inline-block">
                                        <a href="extra_calendar.html" class="waves-effect waves-light nav-link rounded svg-bt-icon" title="">
                                                <i class="ti-calendar"></i>
                                    </a>
                                </li>
                        </ul>  -->
                    </div>

                    <div class="navbar-custom-menu r-side">
                        <ul class="nav navbar-nav">	
                            <li class="btn-group nav-item d-lg-inline-flex d-none">
                                <a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link rounded full-screen" title="Full Screen">
                                    <i class="fal fa-expand-arrows"></i>
                                </a>
                            </li>	  
                            <!-- <li class="btn-group d-lg-inline-flex d-none">
                                    <div class="app-menu">
                                            <div class="search-bx mx-5">
                                                    <form>
                                                            <div class="input-group">
                                                              <input type="search" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
                                                              <div class="input-group-append">
                                                                    <button class="btn" type="submit" id="button-addon3"><i class="ti-search"></i></button>
                                                              </div>
                                                            </div>
                                                    </form>
                                            </div>
                                    </div>
                            </li> -->
                            <!-- Notifications -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="waves-effect waves-light dropdown-toggle" data-toggle="dropdown" title="Notificações">
                                    <i class="fal fa-bell"></i>
                                </a>
                                <ul class="dropdown-menu animated bounceIn">

                                    <li class="header">
                                        <div class="p-20">
                                            <div class="flexbox">
                                                <div>
                                                    <h4 class="mb-0 mt-0">Notificação</h4>
                                                </div>
                                                <div>
                                                    <a href="#" class="text-danger">Clear All</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                         inner menu: contains the actual data 
                                        <ul class="menu sm-scrol">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-info"></i> Curabitur id eros quis nunc suscipit blandit.
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-warning text-warning"></i> Duis malesuada justo eu sapien elementum, in semper diam posuere.
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-danger"></i> Donec at nisi sit amet tortor commodo porttitor pretium a erat.
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-shopping-cart text-success"></i> In gravida mauris et nisi
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-user text-danger"></i> Praesent eu lacus in libero dictum fermentum.
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-user text-primary"></i> Nunc fringilla lorem 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-user text-success"></i> Nullam euismod dolor ut quam interdum, at scelerisque ipsum imperdiet.
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="#">View all</a>
                                    </li>
                                </ul>
                            </li>	

                            <!-- User Account-->
                            <li class="dropdown user user-menu">
                                <a href="#" class="waves-effect waves-light dropdown-toggle" data-toggle="dropdown" title="User">
                                    <i class="fal fa-user"></i>
                                </a>
                                <ul class="dropdown-menu animated flipInX">
                                    <li class="user-body">
                                        <a class="dropdown-item" href="<?= PORTAL_URL; ?>admin/view/usuarios/novo/<?= $_SESSION['id']; ?>"><i class="fal fa-user text-muted mr-2"></i> Perfil</a>
                                        <a class="dropdown-item" href="#"><i class="ti-settings text-muted mr-2"></i> Configurações</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="<?= PORTAL_URL; ?>logout"><i class="ti-lock text-muted mr-2"></i> Deslogar</a>
                                    </li>
                                </ul>
                            </li>	

                            <!-- Control Sidebar Toggle Button -->
                            <!-- <li>
                                <a href="#" data-toggle="control-sidebar" title="Setting" class="waves-effect waves-light">
                                                  <i class="ti-settings"></i>
                                            </a>
                            </li> -->

                        </ul>
                    </div>
                </nav>
            </header>