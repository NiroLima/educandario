<?php

//------------------------------------------------------------------------------
include_once('../conf/config.php');
$db = Conexao::getInstance();

$lei = $_POST['lei'];

$stmp = $db->prepare("SELECT * FROM artigos WHERE lei_id = ? ORDER BY nome ASC");
$stmp->bindValue(1, $lei);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    echo '<option value="">Nenhum artigo encontrado</option>';
} else {
    echo '<option value="">' . 'Escolha um artigo' . '</option>';
    while ($row = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<option value="' . $row['id'] . '">Art. ' . ($row['nome']) . '</option>';
    }
}
//------------------------------------------------------------------------------
?>

