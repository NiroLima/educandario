<?php

//------------------------------------------------------------------------------
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$id = $_POST['id'];

$stmp = $db->prepare("SELECT *           
                     FROM mod_saude_consulta_especialistas msce 
                     LEFT JOIN mod_saude_especialidade AS mse ON mse.id = msce.especialista_id
                     WHERE msce.diagnostico_id = ?
                     GROUP BY mse.id");
$stmp->bindValue(1, $id);
$stmp->execute();

if ($stmp->rowCount() > 0) {
    while ($diag = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<option selected="true" value="' . $diag['id'] . '">' . $diag['nome'] . '</option>';
    }
}

$stmp2 = $db->prepare("SELECT *    
                       FROM mod_saude_especialidade mse  
                       WHERE mse.status = 1 AND mse.id NOT IN(SELECT especialista_id FROM mod_saude_consulta_especialistas WHERE diagnostico_id = ?)
                       ORDER BY mse.nome");
$stmp2->bindValue(1, $id);
$stmp2->execute();

if ($stmp2->rowCount() > 0) {
    while ($diag2 = $stmp2->fetch(PDO::FETCH_ASSOC)) {
        echo '<option value="' . $diag2['id'] . '">' . $diag2['nome'] . '</option>';
    }
}
//------------------------------------------------------------------------------
?>

