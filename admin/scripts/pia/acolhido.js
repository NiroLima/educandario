//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $("img#click_foto").click(function () {
        $("div#div_clicado").click();
    });

    $("button#atualizar").click(function () {
        $("form#form_acolhido").submit();
    });

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    //COMBO ESTADO E MUNICIPIO
    $("select#estado").change(function () {
        $("select#cidade").html('<option value="0">Carregando...</option>');
        $.post(PORTAL_URL + "utils/combo_cidades.php",
                {estado: $(this).val()},
                function (valor) {
                    $("select#cidade").html(valor);
                    $("select#cidade").select2();
                });
    });

    $('#form_responsavel').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var responsavel_id = $("#responsavel_id").val();

        $.post(PORTAL_URL + "admin/dao/pia/acolhido_responsavel.php", {id: id, responsavel_id: responsavel_id}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário PIA",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário PIA",
                    text: "Informações do acolhido atualizadas com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/acolhido/' + id);
                });
            }
        }
        , "html");
        return false;
    });


    $('#form_acolhido').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var crianca_nome = $("#crianca_nome").val();
            var sexo = $("#masculino:checked").val() == 1 ? 1 : ($("#feminino:checked").val() == 2 ? 2 : 0);
            var nascimento = $("#nascimento").val();
            var nacionalidade = $("#nacionalidade").val();
            var naturalidade = $("#naturalidade").val();
            var religiao = $("#religiao").val();
            var cor = $("#branca:checked").val() == 1 ? 1 : ($("#preta:checked").val() == 2 ? 2 : ($("#parda:checked").val() == 3 ? 3 : ($("#amarela:checked").val() == 4 ? 4 : ($("#indigina:checked").val() == 5 ? 5 : 0))));
            var nome_pai = $("#nome_pai").val();
            var nome_mae = $("#nome_mae").val();
            var rua = $("#rua").val();
            var numero = $("#numero").val();
            var bairro = $("#bairro").val();
            var estado = $("#estado").val();
            var cidade = $("#cidade").val();

            $.post(PORTAL_URL + "admin/dao/pia/acolhido.php", {id: id, cidade: cidade, estado: estado, bairro: bairro,
                numero: numero, rua: rua, cor: cor, nome_pai: nome_pai, nome_mae: nome_mae, religiao: religiao, naturalidade: naturalidade,
                nacionalidade: nacionalidade, nascimento: nascimento, crianca_nome: crianca_nome, sexo: sexo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário PIA",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário PIA",
                        text: "Informações do acolhido atualizadas com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/pia/acolhido/' + id);
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var nascimento = $("#nascimento").val();
    var nacionalidade = $("#nacionalidade").val();
    var naturalidade = $("#naturalidade").val();
    var crianca_nome = $("#crianca_nome").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (crianca_nome == "") {
            $('div#div_crianca_nome').after('<label id="erro_crianca_nome" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_crianca_nome');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nacionalidade == "") {
            $('div#div_nacionalidade').after('<label id="erro_nacionalidade" class="error">Nacionalidade é obrigatório.</label>');
            valido = false;
            element = $('div#div_nacionalidade');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function editar_responsavel() {
    $("div#responsavel_edite_1").hide();
    $("div#responsavel_edite_2").show();
}
//------------------------------------------------------------------------------------------------------
function validar(codigo) {
    swal({
        title: "Deseja mesmo confirmar a Etapa 1 Acolhido(a)?",
        text: "Obs: Caso escolha confirmar, o sistema irá identificar essa etapa como concluída!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode confirmar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/confirmar_acolhido",
            type: "POST",
            data: {id: codigo, op: 1},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------------------------------
function desvalidar(codigo) {
    swal({
        title: "Deseja mesmo desvalidar a Etapa 1 Acolhido(a)?",
        text: "Obs: Caso escolha desvalidar, o sistema irá identificar essa etapa como pendente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode desvalidar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/desvalidar_acolhido",
            type: "POST",
            data: {id: codigo, op: 1},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/acolhido/' + obj.id);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
function gerar_pdf() {
    //pega o Html da DIV
    var divElements = document.getElementById("impressao").innerHTML;
    //pega o HTML de toda tag Body
    var oldPage = document.body.innerHTML;
    //Alterna o body 
    document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
    //Imprime o body atual
    window.print();
    //Retorna o conteudo original da página. 
    document.body.innerHTML = oldPage;
}
//------------------------------------------------------------------------------