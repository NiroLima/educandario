<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$nome = isset($_POST['nome']) ? $_POST['nome'] : NULL;
$cpf = isset($_POST['cpf']) ? $_POST['cpf'] : NULL;
$municipio_id = isset($_POST['municipio_id']) ? $_POST['municipio_id'] : NULL;
$nascimento = isset($_POST['nascimento']) ? $_POST['nascimento'] : NULL;
$nivel = isset($_POST['nivel']) && $_POST['nivel'] != "" ? $_POST['nivel'] : null;

$email = isset($_POST['email']) ? $_POST['email'] : NULL;
$celular = isset($_POST['celular']) ? $_POST['celular'] : NULL;
$fixo = isset($_POST['fixo']) ? $_POST['fixo'] : NULL;
$rua = isset($_POST['rua']) ? $_POST['rua'] : NULL;
$numero = isset($_POST['numero']) ? $_POST['numero'] : NULL;
$bairro = isset($_POST['bairro']) ? $_POST['bairro'] : NULL;
$funcao = isset($_POST['funcao']) ? $_POST['funcao'] : NULL;
$periodo = isset($_POST['periodo']) ? $_POST['periodo'] : NULL;

$registro = isset($_POST['registro']) ? $_POST['registro'] : NULL;
$crm = isset($_POST['crm']) ? ($funcao == 6 ? $_POST['crm'] : NULL) : NULL;

$categoria = isset($_POST['categoria']) ? $_POST['categoria'] : NULL;

$especialidade = isset($_POST['especialidade']) && $_POST['especialidade'] != "" ? $_POST['especialidade'] : null;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "seg_profissional", "nome", "=", $nome, "");
if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome do profissional informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO seg_profissional (nome, cpf, cidade_id, nascimento, email, celular, contato, rua, numero, bairro, funcao_id, periodo, registro, categoria_id, crm, data_update, usuario_id, data_cadastro, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $nome);
            $sql->bindValue(2, $cpf);
            $sql->bindValue(3, $municipio_id);
            $sql->bindValue(4, $nascimento);
            $sql->bindValue(5, $email);
            $sql->bindValue(6, $celular);
            $sql->bindValue(7, $fixo);
            $sql->bindValue(8, $rua);
            $sql->bindValue(9, $numero);
            $sql->bindValue(10, $bairro);
            $sql->bindValue(11, $funcao);
            $sql->bindValue(12, $periodo);
            $sql->bindValue(13, $registro);
            $sql->bindValue(14, $categoria);
            $sql->bindValue(15, $crm);
            $sql->bindValue(16, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE seg_profissional SET nome = ?, cpf = ?, cidade_id = ?, nascimento = ?, email = ?, celular = ?, contato = ?, rua = ?, numero = ?, bairro = ?, funcao_id = ?, periodo = ?, registro = ?, categoria_id = ?, crm = ?, usuario_id = ? WHERE id = ?");
            $sql->bindValue(1, $nome);
            $sql->bindValue(2, $cpf);
            $sql->bindValue(3, $municipio_id);
            $sql->bindValue(4, $nascimento);
            $sql->bindValue(5, $email);
            $sql->bindValue(6, $celular);
            $sql->bindValue(7, $fixo);
            $sql->bindValue(8, $rua);
            $sql->bindValue(9, $numero);
            $sql->bindValue(10, $bairro);
            $sql->bindValue(11, $funcao);
            $sql->bindValue(12, $periodo);
            $sql->bindValue(13, $registro);
            $sql->bindValue(14, $categoria);
            $sql->bindValue(15, $crm);
            $sql->bindValue(16, $_SESSION['id']);
            $sql->bindValue(17, $id);
            $sql->execute();
        }

        //ESPECIALIDADE
        $sql3 = $db->prepare("DELETE FROM seg_profissional_especialidade WHERE profissional_id = ?");
        $sql3->bindValue(1, $id);
        $sql3->execute();

        if ($especialidade != null) {
            foreach ($especialidade AS $key => $val) {
                if ($val != null && $val != "" && is_numeric($val) && $val != 0) {
                    $sql4 = $db->prepare("INSERT INTO seg_profissional_especialidade (profissional_id, especialidade_id) VALUES (?, ?)");
                    $sql4->bindValue(1, $id);
                    $sql4->bindValue(2, $val);
                    $sql4->execute();
                }
            }
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>