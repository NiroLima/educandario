<?php

$db = Conexao::getInstance();

$error = false;

$id = strip_tags(@$_POST['id']);
$op = strip_tags(@$_POST['op']);

try {

    $db->beginTransaction();

    if ($op == 1) {//ACOLHIDO
        $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca SET vf = 0, vf_responsavel = ? WHERE acolhimento_id = ?");
        $stmt5->bindValue(1, $_SESSION['id']);
        $stmt5->bindValue(2, $id);
        $stmt5->execute();
    } else if ($op == 2) {//ACOLHIMENTO
        $stmt5 = $db->prepare("UPDATE mod_acolhimento_situacao SET vf = 0, vf_responsavel = ? WHERE mod_acolhimento_id = ?");
        $stmt5->bindValue(1, $_SESSION['id']);
        $stmt5->bindValue(2, $id);
        $stmt5->execute();
    } else if ($op == 3) {//SAÚDE
        $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca_geral SET vf = 0, vf_responsavel = ? WHERE acolhimento_crianca_id = ?");
        $stmt5->bindValue(1, $_SESSION['id']);
        $stmt5->bindValue(2, $id);
        $stmt5->execute();
    } else if ($op == 4) {//EDUCAÇÃO
        $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca_educacao SET vf = 0, vf_responsavel = ? WHERE acolhimento_crianca_id = ?");
        $stmt5->bindValue(1, $_SESSION['id']);
        $stmt5->bindValue(2, $id);
        $stmt5->execute();
    } else if ($op == 5) {//FAMÍLIA
        $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca_familia SET vf = 0, vf_responsavel = ? WHERE acolhimento_crianca_id = ?");
        $stmt5->bindValue(1, $_SESSION['id']);
        $stmt5->bindValue(2, $id);
        $stmt5->execute();
    } else if ($op == 6) {//REDE
        $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca_rede SET vf = 0, vf_responsavel = ? WHERE acolhimento_crianca_id = ?");
        $stmt5->bindValue(1, $_SESSION['id']);
        $stmt5->bindValue(2, $id);
        $stmt5->execute();
    }else if ($op == 7) {//PLANO DE AÇÃO
        $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca_plano SET vf = 0, vf_responsavel = ? WHERE acolhimento_crianca_id = ?");
        $stmt5->bindValue(1, $_SESSION['id']);
        $stmt5->bindValue(2, $id);
        $stmt5->execute();
    }else if ($op == 8) {//AVALIAÇÃO
        $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca_avaliacao SET vf = 0, vf_responsavel = ? WHERE acolhimento_crianca_id = ?");
        $stmt5->bindValue(1, $_SESSION['id']);
        $stmt5->bindValue(2, $id);
        $stmt5->execute();
    }

    $db->commit();

    //MENSAGEM DE SUCESSO
    $msg['id'] = $id;
    $msg['msg'] = 'success';
    $msg['retorno'] = 'Desvalidação realizada com sucesso!';
    echo json_encode($msg);
    exit();
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar realizar a desvalidação desejada:" . $e->getMessage();
    echo json_encode($msg);
    exit();
}
?>


