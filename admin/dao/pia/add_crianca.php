<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$acolhimento_crianca_id = isset($_POST['acolhimento_crianca_id']) && $_POST['acolhimento_crianca_id'] != "" ? $_POST['acolhimento_crianca_id'] : 0;

$descreva_objetivo_monitoramento = isset($_POST['descreva_objetivo_monitoramento']) && $_POST['descreva_objetivo_monitoramento'] != "" ? $_POST['descreva_objetivo_monitoramento'] : NULL;
$descreva_acao = isset($_POST['descreva_acao']) && $_POST['descreva_acao'] != "" ? $_POST['descreva_acao'] : NULL;
$responsavel = isset($_POST['responsavel']) && $_POST['responsavel'] != "" ? $_POST['responsavel'] : NULL;
$prazo_inicial = isset($_POST['prazo_inicial']) && $_POST['prazo_inicial'] != "" ? $_POST['prazo_inicial'] : NULL;
$prazo_final = isset($_POST['prazo_final']) && $_POST['prazo_final'] != "" ? $_POST['prazo_final'] : NULL;
$descreva_observacao = isset($_POST['descreva_observacao']) && $_POST['descreva_observacao'] != "" ? $_POST['descreva_observacao'] : NULL;

$documentacao_id = isset($_POST['documentacao_id']) && $_POST['documentacao_id'] != "" ? $_POST['documentacao_id'] : "";

$acao_realizada = is_numeric($_POST['acao_realizada']) && $_POST['acao_realizada'] == 1 ? 1 : 0;
$objetivo_alcancada = is_numeric($_POST['objetivo_alcancada']) && $_POST['objetivo_alcancada'] == 1 ? 1 : 0;
$finalizada = is_numeric($_POST['finalizada']) && $_POST['finalizada'] == 1 ? 1 : 0;
$redefinir_prazo = is_numeric($_POST['redefinir_prazo']) && $_POST['redefinir_prazo'] == 1 ? 1 : 0;

$opcao = is_numeric($_POST['opcao']) ? $_POST['opcao'] : "";

if ($error == false && is_numeric($opcao)) {
    try {

        $db->beginTransaction();

        if (is_numeric($documentacao_id)) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_plano SET objetivo = ?, acao = ?, responsavel = ?, prazo_inicial = ?, prazo_final = ?, realizada = ?, alcancado = ?, finalizada = ?, prazo = ?, obs = ?, responsavel_id = ? WHERE id = ?");
            $sql4->bindValue(1, $descreva_objetivo_monitoramento);
            $sql4->bindValue(2, $descreva_acao);
            $sql4->bindValue(3, $responsavel);
            $sql4->bindValue(4, $prazo_inicial);
            $sql4->bindValue(5, $prazo_final);
            $sql4->bindValue(6, $acao_realizada);
            $sql4->bindValue(7, $objetivo_alcancada);
            $sql4->bindValue(8, $finalizada);
            $sql4->bindValue(9, $redefinir_prazo);
            $sql4->bindValue(10, $descreva_observacao);
            $sql4->bindValue(11, $_SESSION['id']);
            $sql4->bindValue(12, $documentacao_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_plano (tipo, subtipo, objetivo, acao, responsavel, prazo_inicial, prazo_final, realizada, alcancado, finalizada, prazo, obs, acolhimento_crianca_id, responsavel_id, data_cadastro) VALUES (2, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())");
            $sql4->bindValue(1, $opcao);
            $sql4->bindValue(2, $descreva_objetivo_monitoramento);
            $sql4->bindValue(3, $descreva_acao);
            $sql4->bindValue(4, $responsavel);
            $sql4->bindValue(5, $prazo_inicial);
            $sql4->bindValue(6, $prazo_final);
            $sql4->bindValue(7, $acao_realizada);
            $sql4->bindValue(8, $objetivo_alcancada);
            $sql4->bindValue(9, $finalizada);
            $sql4->bindValue(10, $redefinir_prazo);
            $sql4->bindValue(11, $descreva_observacao);
            $sql4->bindValue(12, $acolhimento_crianca_id);
            $sql4->bindValue(13, $_SESSION['id']);
            $sql4->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>