<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Educação</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-clipboard-user"></i> <strong>PLANOS EDUCACIONAIS</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/educacao/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVO PLANO EDUCACIONAL</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>EDUCANDO</th>
                                            <th>IDADE</th>
                                            <th>DATA</th>
                                            <th>RESPONSÁVEL</th>
                                            <th>AVALIA EM</th>
                                            <th width="15%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>ILDENIRO DE OLIVEIRA LIMA</td>
                                            <td>12 anos</td>
                                            <td>21/05/2021</td>
                                            <td>ALFREDINA ROCHA CAMPOS GONZAGA</td>
                                            <td><div class="badge badge-warning">90 Dias</div></td>
                                            <td class="text-center">
                                                <a href="" title="Editar Consulta" class="mr-2 waves-effect waves-light btn btn-xs btn-info">FINALIZAR</a>
                                                <a rel="" title="Desbloquear Consulta" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                <a rel="" title="Bloquear Consulta" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                   
                                                <a href="<?= PORTAL_URL ?>admin/view/consultas/novo/<?= $medicamento['id']; ?>" title="Editar Consulta" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>ILDENIRO DE OLIVEIRA LIMA</td>
                                            <td>12 anos</td>
                                            <td>21/05/2021</td>
                                            <td>ALFREDINA ROCHA CAMPOS GONZAGA</td>
                                            <td><div class="badge badge-danger">09 Dias</div></td>
                                            <td class="text-center">
                                                <a href="" title="Editar Consulta" class="mr-2 waves-effect waves-light btn btn-xs btn-info">FINALIZAR</a>
                                                <a rel="" title="Desbloquear Consulta" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                <a rel="" title="Bloquear Consulta" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                   
                                                <a href="<?= PORTAL_URL ?>admin/view/consultas/novo/<?= $medicamento['id']; ?>" title="Editar Consulta" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>ILDENIRO DE OLIVEIRA LIMA</td>
                                            <td>12 anos</td>
                                            <td>21/05/2021</td>
                                            <td>ALFREDINA ROCHA CAMPOS GONZAGA</td>
                                            <td><div class="badge badge-warning">30 Dias</div></td>
                                            <td class="text-center">
                                                <a href="" title="Editar Consulta" class="mr-2 waves-effect waves-light btn btn-xs btn-info">FINALIZAR</a>
                                                <a rel="" title="Desbloquear Consulta" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                <a rel="" title="Bloquear Consulta" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                   
                                                <a href="<?= PORTAL_URL ?>admin/view/consultas/novo/<?= $medicamento['id']; ?>" title="Editar Consulta" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>ILDENIRO DE OLIVEIRA LIMA</td>
                                            <td>12 anos</td>
                                            <td>21/05/2021</td>
                                            <td>ALFREDINA ROCHA CAMPOS GONZAGA</td>
                                            <td><div class="badge badge-danger">00 Dias</div></td>
                                            <td class="text-center">
                                                <a href="" title="Editar Consulta" class="mr-2 waves-effect waves-light btn btn-xs btn-info">FINALIZAR</a>
                                                <a rel="" title="Desbloquear Consulta" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                <a rel="" title="Bloquear Consulta" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                   
                                                <a href="<?= PORTAL_URL ?>admin/view/consultas/novo/<?= $medicamento['id']; ?>" title="Editar Consulta" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/consultas/lista.js"></script>
