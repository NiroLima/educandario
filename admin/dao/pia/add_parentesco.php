<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$acolhimento_crianca_id = isset($_POST['acolhimento_crianca_id']) && $_POST['acolhimento_crianca_id'] != "" ? $_POST['acolhimento_crianca_id'] : 0;
$nome_parente = isset($_POST['nome_parente']) && $_POST['nome_parente'] != "" ? $_POST['nome_parente'] : NULL;
$nascimento_parente = isset($_POST['nascimento_parente']) && $_POST['nascimento_parente'] != "" ? $_POST['nascimento_parente'] : NULL;
$rg_parente = isset($_POST['rg_parente']) && $_POST['rg_parente'] != "" ? $_POST['rg_parente'] : NULL;
$cpf_parente = isset($_POST['cpf_parente']) && $_POST['cpf_parente'] != "" ? $_POST['cpf_parente'] : NULL;
$celular_parente = isset($_POST['celular_parente']) && $_POST['celular_parente'] != "" ? $_POST['celular_parente'] : NULL;
$endereco = isset($_POST['endereco']) && $_POST['endereco'] != "" ? $_POST['endereco'] : NULL;
$ocupacao_parente = isset($_POST['ocupacao_parente']) && $_POST['ocupacao_parente'] != "" ? $_POST['ocupacao_parente'] : NULL;
$grau_parentesco = isset($_POST['grau_parentesco']) && $_POST['grau_parentesco'] != "" ? $_POST['grau_parentesco'] : NULL;
$parente_id = isset($_POST['parente_id']) && $_POST['parente_id'] != "" ? $_POST['parente_id'] : "";

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($parente_id)) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_parentes SET nome = ?, nascimento = ?, rg = ?, cpf = ?, celular = ?, endereco = ?, ocupacao = ?, grau = ?, responsavel_id = ? WHERE id = ?");
            $sql4->bindValue(1, $nome_parente);
            $sql4->bindValue(2, $nascimento_parente);
            $sql4->bindValue(3, $rg_parente);
            $sql4->bindValue(4, $cpf_parente);
            $sql4->bindValue(5, $celular_parente);
            $sql4->bindValue(6, $endereco);
            $sql4->bindValue(7, $ocupacao_parente);
            $sql4->bindValue(8, $grau_parentesco);
            $sql4->bindValue(9, $_SESSION['id']);
            $sql4->bindValue(10, $parente_id);
            $sql4->execute();
        } else {

            if ($id == 0) {
                $sql44 = $db->prepare("INSERT INTO mod_acolhimento_crianca_familia (acolhimento_crianca_id, responsavel_id, data_cadastro, status) 
                                     VALUES (?, ?, NOW(), 1)");
                $sql44->bindValue(1, $acolhimento_crianca_id);
                $sql44->bindValue(2, $_SESSION['id']);
                $sql44->execute();

                $id = $db->lastInsertId();
            }

            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_parentes (familia_id, nome, nascimento, rg, cpf, celular, endereco, ocupacao, grau, tipo, responsavel_id, data_cadastro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?, NOW())");
            $sql4->bindValue(1, $id);
            $sql4->bindValue(2, $nome_parente);
            $sql4->bindValue(3, $nascimento_parente);
            $sql4->bindValue(4, $rg_parente);
            $sql4->bindValue(5, $cpf_parente);
            $sql4->bindValue(6, $celular_parente);
            $sql4->bindValue(7, $endereco);
            $sql4->bindValue(8, $ocupacao_parente);
            $sql4->bindValue(9, $grau_parentesco);
            $sql4->bindValue(10, $_SESSION['id']);
            $sql4->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>