<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
        FROM mod_acolhimento ma   
        WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result22 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca mac   
        WHERE mac.acolhimento_id = ?");
    $result22->bindValue(1, $acolhimento_id);
    $result22->execute();
    $resultado22 = $result22->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_id = $resultado22['id'];
    $acolhimento_responsavel = $resultado22['usuario_id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_familia macf   
        WHERE macf.acolhimento_crianca_id = ?");
    $result2->bindValue(1, $acolhimento_crianca_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $mod_acolhimento_crianca_familia_id = $resultado2['id'];
    $mae_nome = $resultado2['mae_nome'];
    $mae_nascimento = $resultado2['mae_nascimento'];
    $mae_endereco = $resultado2['mae_endereco'];
    $mae_numero = $resultado2['mae_numero'];
    $mae_bairro = $resultado2['mae_bairro'];
    $mae_rg = $resultado2['mae_rg'];
    $mae_cpf = $resultado2['mae_cpf'];
    $mae_celular = $resultado2['mae_celular'];
    $mae_ocupacao = $resultado2['mae_ocupacao'];
    $mae_situacao = $resultado2['mae_situacao'];

    $pai_nome = $resultado2['pai_nome'];
    $pai_nascimento = $resultado2['pai_nascimento'];
    $pai_endereco = $resultado2['pai_endereco'];
    $pai_numero = $resultado2['pai_numero'];
    $pai_bairro = $resultado2['pai_bairro'];
    $pai_rg = $resultado2['pai_rg'];
    $pai_cpf = $resultado2['pai_cpf'];
    $pai_celular = $resultado2['pai_celular'];
    $pai_ocupacao = $resultado2['pai_ocupacao'];
    $pai_situacao = $resultado2['pai_situacao'];

    $oferecem_condicoes = $resultado2['oferecem_condicoes'];
    $avaliacao_profunda = $resultado2['avaliacao_profunda'];
    $possui_irmaos = $resultado2['possui_irmaos'];

    $possui_irmaos_outro = $resultado2['possui_irmaos_outro'];
    $conhece_irmaos = $resultado2['conhece_irmaos'];

    $irmaos_adotados = $resultado2['irmaos_adotados'];

    $situacao_familiar = $resultado2['situacao_familiar'];

    $qual_outra_situacao = $resultado2['outra_situacao'];

    $demonstra_interesse = $resultado2['demonstra_interesse'];
    $descricao = $resultado2['descricao'];
    $crianca_demonstra_interesse = $resultado2['crianca_demonstra_interesse'];
    $crianca_demonstra_descricao = $resultado2['crianca_demonstra_descricao'];

    $medida_restricao = $resultado2['medida_restricao'];
    $quem_estabelelceu = $resultado2['quem_estabelelceu'];
    $recebe_visita = $resultado2['recebe_visita'];
    $frequencia_encontros = $resultado2['frequencia_encontros'];
    $como_realizados = $resultado2['como_realizados'];
    $pais_envolvidos_alcool = $resultado2['pais_envolvidos_alcool'];
    $pais_envolvidos_outros = $resultado2['pais_envolvidos_outros'];
    $pais_envolvidos_abuso = $resultado2['pais_envolvidos_abuso'];
    $situacao_rua = $resultado2['situacao_rua'];
    $pais_pena = $resultado2['pais_pena'];
    $pais_reabilitacao = $resultado2['pais_reabilitacao'];
    $pais_ameacados = $resultado2['pais_ameacados'];
    $pais_doencas = $resultado2['pais_doencas'];
    $pais_transtorno = $resultado2['pais_transtorno'];
    $qual_doenca = $resultado2['qual_doenca'];
    $qual_transtorno = $resultado2['qual_transtorno'];
    $vf = $resultado2['vf'] == "" ? 0 : $resultado2['vf'];

    $familiar_interesse = $resultado2['familiar_interesse'];

    $descricao_condicao_cuidado = $resultado2['descreva_condicao_cuidado'];
    $opniao_crianca = $resultado2['opiniao_crianca'];
    $familiar_condicao = $resultado2['opcao_familiar_condicao'];
} else {
    $acolhimento_id = "";
    $acolhimento_crianca_id = "";
//------------------------------------------------------------------------------
    $mod_acolhimento_crianca_familia_id = "";
    $mae_nome = "";
    $mae_nascimento = "";
    $mae_endereco = "";
    $mae_numero = "";
    $mae_bairro = "";
    $mae_rg = "";
    $mae_cpf = "";
    $mae_celular = "";
    $mae_ocupacao = "";
    $mae_situacao = "";

    $pai_nome = "";
    $pai_nascimento = "";
    $pai_endereco = "";
    $pai_numero = "";
    $pai_bairro = "";
    $pai_rg = "";
    $pai_cpf = "";
    $pai_celular = "";
    $pai_ocupacao = "";
    $pai_situacao = "";

    $oferecem_condicoes = "";
    $avaliacao_profunda = "";
    $possui_irmaos = "";

    $possui_irmaos_outro = "";
    $conhece_irmaos = "";
    $irmaos_adotados = "";
    $situacao_familiar = "";
    $qual_outra_situacao = "";
    $demonstra_interesse = "";
    $descricao = "";
    $crianca_demonstra_interesse = "";
    $crianca_demonstra_descricao = "";

    $medida_restricao = "";
    $quem_estabelelceu = "";
    $recebe_visita = "";
    $frequencia_encontros = "";
    $como_realizados = "";
    $pais_envolvidos_alcool = "";
    $pais_envolvidos_outros = "";
    $pais_envolvidos_abuso = "";
    $situacao_rua = "";
    $pais_pena = "";
    $pais_reabilitacao = "";
    $pais_ameacados = "";
    $pais_doencas = "";
    $pais_transtorno = "";
    $qual_doenca = "";
    $qual_transtorno = "";
    $acolhimento_responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
    $vf = 0;
    $familiar_interesse = "";
    $descricao_condicao_cuidado = "";
    $opniao_crianca = "";
    $familiar_condicao = "";
}

//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);
//------------------------------------------------------------------------------
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/pia/lista">PIA</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="text-center">PLANO INDIVIDUAL DE ATENDIMENTO - PIA</h4>

        <!-- Main content -->
        <section class="content">

            <!-- Step wizard -->
            <div id="impressao" class="box box-solid bg-info">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="box-title">PIA</h4>
                            <h6 class="box-subtitle">PLANO INDIVIDUAL DE ATENDIMENTO</h6>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-right"><?= $barra; ?>%</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <?= $barra; ?>%">
                                    <span class="sr-only"><?= $barra; ?>% Complete</span>
                                </div>
                            </div>
                        </div>      
                    </div> 
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="box-title">RESPONSÁVEL DO PIA</h4>

                            <div id="responsavel_edite_1">
                                <h6 class="subtitle"><?= $nome_responsavel; ?> <a href="#" class="edit-responsavel"><i onclick="editar_responsavel()" class="fal fa-pencil-alt"></i></a></h6>
                            </div>

                            <div id="responsavel_edite_2" style="display: none">
                                <form id="form_responsavel" name="form_responsavel" method="POST" action="#" class="tab-wizard wizard-circle">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required="true" id="responsavel_id" name="responsavel_id">
                                                <option value="">Selecione o responsável</option>
                                                <?php
                                                $result = $db->prepare("SELECT su.id, su.nome           
                                                            FROM seg_usuario su    
                                                            WHERE su.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_responsavel == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button href="#" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <a <?= is_numeric($vf) && $vf == 0 ? "" : "style='display: none'"; ?> href="#" onclick="validar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-success mt-2"><i class="fas fa-check"></i> VALIDAR</a>
                            <a <?= is_numeric($vf) && $vf == 1 ? "" : "style='display: none'"; ?> href="#" onclick="desvalidar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-warning mt-2"><i class="fas fa-remove"></i> DESVALIDAR</a>
                        </div>
                        <div class="col-md-2 text-right">
                            <a onclick="gerar_pdf()" style="cursor: pointer" class="btn-arquivo mt-2"><i class="fas fa-file-pdf"></i> GERAR PDF</a>
                        </div>
                    </div> 
                </div>
                <!-- /.box-header -->
                <div class="box-body wizard-content">

                    <?php include 'layout/menu_wizard.php'; ?>

                    <br/>

                    <form id="form_familia" name="form_familia" method="POST" action="#" class="tab-wizard wizard-circle">

                        <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>
                        <input type="hidden" id="acolhimento_crianca_id" name="acolhimento_crianca_id" value="<?= $acolhimento_crianca_id; ?>"/>
                        <input type="hidden" id="familia_id" name="familia_id" value="<?= $mod_acolhimento_crianca_familia_id; ?>"/>

                        <!-- Step 5 -->
                        <!-- <h6>FAMÍLIA</h6> -->
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-solid bg-info">
                                        <div class="box-header">
                                            <h4 class="box-title mb-0">PAIS/FAMILIARES/OUTROS</h4>
                                        </div>
                                        <div class="">
                                            <div class="box-body">
                                                <div class="box box-outline-info">
                                                    <div class="box-header">
                                                        <strong>FAMÍLIA DE ORIGEM</strong>
                                                    </div>
                                                    <div class="box-body">
                                                        <!-- INÍCIO MÃE -->
                                                        <div class="box bl-3 br-3 border-danger">
                                                            <div class="box-header">
                                                                <h4 class="box-title mb-0"><strong>MÃE</strong></h4>
                                                            </div>
                                                            <div class="box-body">
                                                                <div class="row mt-3">
                                                                    <div class="col-md-8">
                                                                        <div id="div_nome_mae" class="form-group">
                                                                            <label for="nome_mae">NOME</label>
                                                                            <input type="text" class="form-control" name="nome_mae" id="nome_mae" placeholder="Nome da mãe" value="<?= $mae_nome; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_nascimento_mae" class="form-group">
                                                                            <label for="nascimento_mae">NASCIMENTO</label>
                                                                            <input class="form-control" name="nascimento_mae" id="nascimento_mae" type="date" value="<?= $mae_nascimento; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row mt-3">
                                                                    <div class="col-md-6">
                                                                        <div id="div_endereco_mae" class="form-group">
                                                                            <label for="endereco_mae">ENDEREÇO</label>
                                                                            <input type="text" class="form-control" name="endereco_mae" id="endereco_mae" placeholder="Rua, Avenida, Travessa" value="<?= $mae_endereco; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div id="div_numero_mae" class="form-group">
                                                                            <label for="numero_mae">NÚMERO</label>
                                                                            <input type="text" class="form-control" name="numero_mae" id="numero_mae" placeholder="Número" value="<?= $mae_numero; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_bairro_mae" class="form-group">
                                                                            <label for="bairro_mae">BAIRRO</label>
                                                                            <input type="text" class="form-control" name="bairro_mae" id="bairro_mae" placeholder="Bairro" value="<?= $mae_bairro; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row mt-3">
                                                                    <div class="col-md-4">
                                                                        <div id="div_rg_mae" class="form-group">
                                                                            <label for="rg_mae">RG</label>
                                                                            <input type="text" class="form-control" name="rg_mae" id="rg_mae" placeholder="Registro Geral" value="<?= $mae_rg; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_cpf_mae" class="form-group">
                                                                            <label for="cpf_mae">CPF</label>
                                                                            <input type="text" class="form-control" data-mask="999.999.999-99" name="cpf_mae" id="cpf_mae" placeholder="CPF" value="<?= $mae_cpf; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_celular_mae" class="form-group">
                                                                            <label for="celular_mae">CELULAR</label>
                                                                            <input type="text" class="form-control" data-mask="(99)99999-9999" name="celular_mae" id="celular_mae" placeholder="Celular para contato" value="<?= $mae_celular; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <div id="div_ocupacao_mae" class="form-group">
                                                                            <label for="ocupacao_mae">OCUPAÇÃO</label>
                                                                            <input type="text" class="form-control" name="ocupacao_mae" id="ocupacao_mae" placeholder="Ocupação da mãe" value="<?= $mae_ocupacao; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <hr>

                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="genero">SITUAÇÃO DA MÃE DA CRIANÇA?</label>
                                                                            <div class="row">
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" class="with-gap radio-col-info" <?= $mae_situacao == 0 ? "checked='true'" : "" ?> name="opcao_vinculo" id="mae_falecida" value="1">
                                                                                    <label for="mae_falecida">MÃE FALECIDA</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" class="with-gap radio-col-info" <?= $mae_situacao == 1 ? "checked='true'" : "" ?> name="opcao_vinculo" id="mae_reclusa" value="1">
                                                                                    <label for="mae_reclusa">MÃE RECLUSA EM SISTEMA PRISIONAL</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" class="with-gap radio-col-info" <?= $mae_situacao == 2 ? "checked='true'" : "" ?> name="opcao_vinculo" id="mae_nao_encontrada" value="1">
                                                                                    <label for="mae_nao_encontrada">MÃE NÃO ENCONTRADA</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" class="with-gap radio-col-info" <?= $mae_situacao == 3 ? "checked='true'" : "" ?> name="opcao_vinculo" id="mae_desconhecida" value="1">
                                                                                    <label for="mae_desconhecida">MÃE DESCONHECIDA</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" class="with-gap radio-col-info" <?= $mae_situacao == 4 ? "checked='true'" : "" ?> name="opcao_vinculo" id="sem_vinculo_mae" value="1">
                                                                                    <label for="sem_vinculo_mae">SEM VÍNCULO COM A MÃE</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- FIM MÃE -->

                                                        <!-- INÍCIO PAI -->
                                                        <div class="box bl-3 br-3 border-primary mt-4">
                                                            <div class="box-header">
                                                                <h4 class="box-title mb-0"><strong>PAI</strong></h4>
                                                            </div>
                                                            <div class="box-body">
                                                                <div class="row mt-3">
                                                                    <div class="col-md-8">
                                                                        <div id="div_nome_pai" class="form-group">
                                                                            <label for="nome_pai">NOME</label>
                                                                            <input type="text" class="form-control" name="nome_pai" id="nome_pai" placeholder="Nome do pai" value="<?= $pai_nome; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_nascimento_pai" class="form-group">
                                                                            <label for="nascimento_pai">NASCIMENTO</label>
                                                                            <input class="form-control" name="nascimento_pai" id="nascimento_pai" type="date" value="<?= $pai_nascimento; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row mt-3">
                                                                    <div class="col-md-6">
                                                                        <div id="div_endereco_pai" class="form-group">
                                                                            <label for="endereco_pai">ENDEREÇO</label>
                                                                            <input type="text" class="form-control" name="endereco_pai" id="endereco_pai" placeholder="Rua, Avenida, Travessa" value="<?= $pai_endereco; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div id="div_numero_pai" class="form-group">
                                                                            <label for="numero_pai">NÚMERO</label>
                                                                            <input type="text" class="form-control" name="numero_pai" id="numero_pai" placeholder="Número" value="<?= $pai_numero; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_bairro_pai" class="form-group">
                                                                            <label for="bairro_pai">BAIRRO</label>
                                                                            <input type="text" class="form-control" name="bairro_pai" id="bairro_pai" placeholder="Bairro" value="<?= $pai_bairro; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row mt-3">
                                                                    <div class="col-md-4">
                                                                        <div id="div_rg_pai" class="form-group">
                                                                            <label for="rg_pai">RG</label>
                                                                            <input type="text" class="form-control" name="rg_pai" id="rg_pai" placeholder="Registro Geral" value="<?= $pai_rg; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_cpf_pai" class="form-group">
                                                                            <label for="cpf_pai">CPF</label>
                                                                            <input type="text" class="form-control" data-mask="999.999.999-99" name="cpf_pai" id="cpf_pai" placeholder="CPF" value="<?= $pai_cpf; ?>">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_celular_pai" class="form-group">
                                                                            <label for="celular_pai">CELULAR</label>
                                                                            <input type="text" class="form-control" data-mask="(99)99999-9999" name="celular_pai" id="celular_pai" placeholder="Celular para contato" value="<?= $pai_celular; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <div id="div_ocupacao_pai" class="form-group">
                                                                            <label for="ocupacao_pai">OCUPAÇÃO</label>
                                                                            <input type="text" class="form-control" name="ocupacao_pai" id="ocupacao_pai" placeholder="Ocupação" value="<?= $pai_ocupacao; ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <hr>

                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="genero">SITUAÇÃO DO PAI DA CRIANÇA?</label>
                                                                            <div class="row">
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" <?= $pai_situacao == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_pai" id="pai_falecido" value="1">
                                                                                    <label for="pai_falecido">PAI FALECIDO</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" <?= $pai_situacao == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_pai" id="pai_recluso" value="1">
                                                                                    <label for="pai_recluso">PAI RECLUSO EM SISTEMA PRISIONAL</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" <?= $pai_situacao == 2 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_pai" id="pai_nao_encontrado" value="1">
                                                                                    <label for="pai_nao_encontrado">PAI NÃO ENCONTRADO</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" <?= $pai_situacao == 3 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_pai" id="pai_desconhecido" value="1">
                                                                                    <label for="pai_desconhecido">PAI DESCONHECIDO</label>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <input type="radio" <?= $pai_situacao == 4 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_pai" id="sem_vinculo_pai" value="1">
                                                                                    <label for="sem_vinculo_pai">SEM VÍNCULO COM A PAI</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- FIM PAI -->

                                                        <!-- INÍCIO PARENTESCO -->
                                                        <div class="box bl-3 br-3 border-success mt-4">
                                                            <div class="box-header">
                                                                <h4 class="box-title mb-0"><strong>PARENTE(S) OU OUTRO(S)</strong></h4>
                                                            </div>
                                                            <div class="box-body">
                                                                <div class="table-responsive">
                                                                    <table class="table table-striped mb-2">
                                                                        <thead class="thead-light">
                                                                            <tr>
                                                                                <th scope="col">VÍNCULO</th>
                                                                                <th scope="col">NOME</th>
                                                                                <th scope="col">ENDEREÇO</th>
                                                                                <th scope="col">TELEFONE</th>
                                                                                <th scope="col">DOCUMENTOS</th>
                                                                                <th scope="col">OCUPAÇÃO</th>
                                                                                <th scope="col"></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody id="resultado_parentesco">
                                                                            <?php
                                                                            $result = $db->prepare("SELECT *           
                                                                                FROM mod_acolhimento_crianca_parentes macp   
                                                                                WHERE macp.familia_id = ? AND tipo = 1");
                                                                            $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                            $result->execute();
                                                                            while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <tr id="remover_parente">
                                                                                    <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                    <td><?= $parent['nome']; ?></td>
                                                                                    <td><?= $parent['endereco']; ?></td>
                                                                                    <td><?= $parent['celular']; ?></td>
                                                                                    <td>
                                                                                        <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                        <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                    </td>
                                                                                    <td><?= $parent['ocupacao']; ?></td>
                                                                                    <td width="100px">
                                                                                        <a style="cursor: pointer" onclick="editar(<?= $parent['id']; ?>, '<?= $parent['grau']; ?>', '<?= $parent['nome']; ?>', '<?= $parent['nascimento']; ?>', '<?= $parent['rg']; ?>', '<?= $parent['cpf']; ?>', '<?= $parent['celular']; ?>', '<?= $parent['endereco']; ?>', '<?= $parent['ocupacao']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                                        <a style="cursor: pointer" onclick="remover(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <hr>
                                                                <div class="row mt-3">
                                                                    <div class="col-md-3">
                                                                        <div id="div_grau_parentesco" class="form-group">
                                                                            <label for="grau_parentesco">VÍNCULO</label>
                                                                            <select name="grau_parentesco" id="grau_parentesco" class="form-control select2">
                                                                                <option value="">Selecione o Vínculo</option>
                                                                                <?php
                                                                                $result = $db->prepare("SELECT *           
                                                                                FROM mod_acolhimento_crianca_vinculo macv   
                                                                                WHERE macv.status = 1
                                                                                ORDER BY macv.nome ASC");
                                                                                $result->execute();
                                                                                while ($vinc = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                    ?>
                                                                                    <option value="<?= $vinc['id']; ?>"><?= $vinc['nome']; ?></option>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div id="div_nome_parente" class="form-group">
                                                                            <label for="nome_parente">NOME</label>
                                                                            <input type="hidden" id="parente_id" name="parente_id" value=""/>
                                                                            <input type="text" class="form-control" name="nome_parente" id="nome_parente" placeholder="Nome" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div id="div_nascimento_parente" class="form-group">
                                                                            <label for="nascimento_parente">NASCIMENTO</label>
                                                                            <input class="form-control" name="nascimento_parente" id="nascimento_parente" type="date" value="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-3">
                                                                    <div class="col-md-4">
                                                                        <div id="div_rg_parente" class="form-group">
                                                                            <label for="rg_parente">RG</label>
                                                                            <input type="text" class="form-control" name="rg_parente" id="rg_parente" placeholder="Registro Geral" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_cpf_parente" class="form-group">
                                                                            <label for="cpf_parente">CPF</label>
                                                                            <input type="text" class="form-control" data-mask="999.999.999-99" name="cpf_parente" id="cpf_parente" placeholder="CPF" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div id="div_celular_parente" class="form-group">
                                                                            <label for="celular_parente">CELULAR</label>
                                                                            <input type="text" class="form-control" data-mask="(99)99999-9999" name="celular_parente" id="celular_parente" placeholder="Celular para contato" value="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <div id="div_endereco" class="form-group">
                                                                            <label for="endereco">ENDEREÇO</label>
                                                                            <input type="text" class="form-control" name="endereco" id="endereco" placeholder="Endereço" value="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-3">
                                                                    <div class="col-md-12">
                                                                        <div id="div_ocupacao_parente" class="form-group">
                                                                            <label for="ocupacao_parente">OCUPAÇÃO</label>
                                                                            <input type="text" class="form-control" name="ocupacao_parente" id="ocupacao_parente" placeholder="Ocupação" value="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row mt-3 mb-2">
                                                                    <div class="col-md-12 text-center">
                                                                        <button id="inserir_parentesco" type="button" class="btn btn-success">ADICIONAR</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- FIM PARENTES -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <div class="box box-solid bg-info">
                                        <div class="box-header">
                                            <h4 class="box-title mb-0">INFORMAÇÃO SOBRE A SITUAÇÃO FAMILIAR DA CRIANÇA</h4>
                                        </div>
                                        <div class="">
                                            <div class="box-body">
                                                <div class="box box-outline-info">
                                                    <div class="box-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="genero">SITUAÇÃO FAMILIAR DA CRIANÇA?</label>
                                                                    <div class="row">
                                                                        <div class="col-sm-3">
                                                                            <input type="radio" <?= $situacao_familiar == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_situacao" onclick="outra_situacao2(0)" id="com_vinculo" value="1">
                                                                            <label for="com_vinculo">COM VÍNCULO</label>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <input type="radio" <?= $situacao_familiar == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_situacao" onclick="outra_situacao2(1)" id="sem_vinculo" value="1">
                                                                            <label for="sem_vinculo">SEM VÍNCULO</label>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <input type="radio" <?= $situacao_familiar == 2 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_situacao" onclick="outra_situacao2(2)" id="familia_desaparecida" value="1">
                                                                            <label for="familia_desaparecida">FAMÍLIA DESAPARECIDA</label>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <input type="radio" <?= $situacao_familiar == 3 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_situacao" onclick="outra_situacao2(3)" id="orfao" value="1">
                                                                            <label for="orfao">ÓRFÃO</label>
                                                                        </div>
                                                                    </div> 

                                                                    <div class="row mt-3">
                                                                        <div class="col-sm-3">
                                                                            <input type="radio" <?= $situacao_familiar == 4 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_situacao" onclick="outra_situacao2(4)" id="poder_familiar" value="1">
                                                                            <label for="poder_familiar">DESTITUÍDO DO PODER FAMILIAR</label>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <input type="radio" <?= $situacao_familiar == 5 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_situacao" onclick="outra_situacao2(5)" id="impedimento_judicial" value="1">
                                                                            <label for="impedimento_judicial">COM IMPEDIMENTO JUDICIAL DE CONTATO</label>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <input type="radio" <?= $situacao_familiar == 6 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_situacao" onclick="outra_situacao2(6)" id="sem_informacao" value="1">
                                                                            <label for="sem_informacao">SEM INFORMAÇÃO</label>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <input type="radio" <?= $situacao_familiar == 7 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_vinculo_situacao" onclick="outra_situacao2(7)" id="outra_situacao" value="1">
                                                                            <label for="outra_situacao">OUTRA SITUAÇÃO</label>
                                                                        </div>
                                                                    </div>

                                                                    <!-- SÓ IRÁ APARECER SE A OPÇAO OUTRA SITUACAO FOR MARCADA -->
                                                                    <div id="div_qual_outra_situacao" <?= $situacao_familiar == 7 ? "" : "style='display: none'" ?> class="row mt-3">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="qual_outra_situacao">QUAL OUTRA SITUAÇÃO?</label>
                                                                                <input type="text" class="form-control" name="qual_outra_situacao" id="qual_outra_situacao" placeholder="Informe a outra situação" value="<?= $qual_outra_situacao; ?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <hr>

                                                                    <div class="row mt-3">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>OS FAMILIARES DEMONSTRAM INTERESSE NO RETORNO DA CRIANÇA AO CONVÍVIO FAMILIAR?</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" <?= $demonstra_interesse == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_interesse" id="demonstram_interesse" value="1">
                                                                                        <label for="demonstram_interesse">SIM</label>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" <?= $demonstra_interesse == "" || $demonstra_interesse == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_interesse" id="nao_demonstram_interesse" value="1">
                                                                                        <label for="nao_demonstram_interesse">NÃO</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="div_descricao_interesse" <?= $demonstra_interesse == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                            <div id="div_qual_outra_situacao" class="form-group">
                                                                                <label for="descreva_interesse">DESCREVA</label>
                                                                                <textarea name="descreva_interesse" id="descreva_interesse" class="form-control" cols="30" rows="5"><?= $descricao ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <hr>

                                                                    <div class="row mt-3">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>A CRIANÇA DEMONSTRA INTERESSE EM RETORNAR AO CONVÍVIO FAMILIAR?</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" <?= $crianca_demonstra_interesse == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="crianca_opcao_retono" id="crianca_demonstra_interesse" value="1">
                                                                                        <label for="crianca_demonstra_interesse">SIM</label>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" <?= $crianca_demonstra_interesse == "" || $crianca_demonstra_interesse == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="crianca_opcao_retono" id="crianca_nao_demonstra_interesse">
                                                                                        <label for="crianca_nao_demonstra_interesse">NÃO</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="div_crianca_demonstra_interesse"  <?= $crianca_demonstra_interesse == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                            <div id="div_descreva" class="form-group">
                                                                                <label for="descreva_interesse">DESCREVA</label>
                                                                                <textarea name="descreva_demonstra_interesse" id="descreva_demonstra_interesse" class="form-control" cols="30" rows="5"><?= $crianca_demonstra_descricao ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <hr>

                                                                    <div class="row mt-3">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>EXISTE ALGUMA MEDIDA DE RESTRIÇÃO DE CONTATO À CRIANÇA / PROIBIÇÃO DE VISITA?</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $medida_restricao == 1 ? "checked='true'" : "" ?> name="opcao_retono_proibicao" id="sim_proibicao_visita" value="1">
                                                                                        <label for="sim_proibicao_visita">SIM</label>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $medida_restricao == "" || $medida_restricao == 0 ? "checked='true'" : "" ?> name="opcao_retono_proibicao" id="nao_proibicao_visita" value="1">
                                                                                        <label for="nao_proibicao_visita">NÃO</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                            <div id="div_quem_estabeleceu_impedimento" <?= $medida_restricao == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="quem_estabeleceu_impedimento">QUEM ESTABELECEU O IMPEDIMENTO?</label>
                                                                                    <input type="text" class="form-control" name="quem_estabeleceu_impedimento" id="quem_estabeleceu_impedimento" value="<?= $quem_estabelelceu ?>">
                                                                                </div>
                                                                            </div>

                                                                            <hr>

                                                                            <div id="div_tabela_medida" <?= $medida_restricao == 1 ? "" : "style='display: none'" ?> class="box bl-3 br-3 border-danger">
                                                                                <div class="box-header">
                                                                                    <h4 class="box-title mb-0"><strong>QUEM ESTÁ IMPEDIDO?</strong></h4>
                                                                                </div>
                                                                                <div class="box-body">
                                                                                    <div class="row mt-3">
                                                                                        <div class="col-md-12">
                                                                                            <div class="table-responsive">
                                                                                                <table class="table table-striped mb-2">
                                                                                                    <thead class="thead-light">
                                                                                                        <tr>
                                                                                                            <th scope="col">GRAU PARENTESCO</th>
                                                                                                            <th scope="col">NOME</th>
                                                                                                            <th scope="col">ENDEREÇO</th>
                                                                                                            <th scope="col">TELEFONE</th>
                                                                                                            <th scope="col">DOCUMENTOS</th>
                                                                                                            <th scope="col">OCUPAÇÃO</th>
                                                                                                            <th scope="col"></th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody id="resultado_parentesco2">
                                                                                                        <?php
                                                                                                        $result = $db->prepare("SELECT *           
                                                                                                            FROM mod_acolhimento_crianca_parentes macp   
                                                                                                            WHERE macp.familia_id = ? AND tipo = 2");
                                                                                                        $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                        $result->execute();
                                                                                                        while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                            ?>
                                                                                                            <tr id="remover_parente">
                                                                                                                <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                <td><?= $parent['nome']; ?></td>
                                                                                                                <td><?= $parent['endereco']; ?></td>
                                                                                                                <td><?= $parent['celular']; ?></td>
                                                                                                                <td>
                                                                                                                    <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                    <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                </td>
                                                                                                                <td><?= $parent['ocupacao']; ?></td>
                                                                                                                <td width="100px">
                                                                                                                    <!--                                                                    <a style="cursor: pointer" onclick="editar2(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                    <a style="cursor: pointer" onclick="remover2(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <?php
                                                                                                        }
                                                                                                        ?>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div id="div_quem_impede2" class="form-group">
                                                                                                <input type="hidden" id="parente_id2" name="parente_id2" value=""/>
                                                                                                <label for="quem_impede2">NOME</label>
                                                                                                <select name="quem_impede2" id="quem_impede2" class="form-control select2">
                                                                                                    <option value="">Selecione quem está impedindo</option>
                                                                                                    <?php
                                                                                                    $result8 = $db->prepare("SELECT id, nome 
                                                                                                       FROM mod_acolhimento_crianca_parentes
                                                                                                       WHERE familia_id = ? AND tipo = 1 
                                                                                                       ORDER BY nome ASC");
                                                                                                    $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                    $result8->execute();
                                                                                                    while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                        ?>
                                                                                                        <option rel="<?= $irm['id']; ?>" value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12 text-center">
                                                                                            <button id="inserir_parentesco2" class="btn btn-success">INSERIR</button>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- FIM COLUNA -->
                                                                    </div>
                                                                    <!-- FIM LINHA -->

                                                                    <div class="row mt-3">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>A CRIANÇA RECEBE VISITA?</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $recebe_visita == 1 ? "checked='true'" : "" ?> name="opcao_retono_visita" id="sim_recebe_visita" value="1">
                                                                                        <label for="sim_recebe_visita">SIM</label>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $recebe_visita == "" || $recebe_visita == 0 ? "checked='true'" : "" ?> name="opcao_retono_visita" id="nao_recebe_visita" value="1">
                                                                                        <label for="nao_recebe_visita">NÃO</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <br>
                                                                            <div <?= $recebe_visita == 1 ? "" : "style='display: none'" ?> id="div_sim_recebe_visita">
                                                                                <div class="box bl-3 br-3 border-success">
                                                                                    <div class="box-header">
                                                                                        <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA?</strong></h4>
                                                                                    </div>
                                                                                    <div class="box-body">
                                                                                        <div class="row mt-3">
                                                                                            <div class="col-md-12">
                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-striped mb-2">
                                                                                                        <thead class="thead-light">
                                                                                                            <tr>
                                                                                                                <th scope="col">GRAU PARENTESCO</th>
                                                                                                                <th scope="col">NOME</th>
                                                                                                                <th scope="col">ENDEREÇO</th>
                                                                                                                <th scope="col">TELEFONE</th>
                                                                                                                <th scope="col">DOCUMENTOS</th>
                                                                                                                <th scope="col">OCUPAÇÃO</th>
                                                                                                                <th scope="col"></th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        <tbody id="resultado_parentesco3">
                                                                                                            <?php
                                                                                                            $result = $db->prepare("SELECT *           
                                                                                                                FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                WHERE macp.familia_id = ? AND tipo = 3");
                                                                                                            $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result->execute();
                                                                                                            while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <tr id="remover_parente">
                                                                                                                    <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                    <td><?= $parent['nome']; ?></td>
                                                                                                                    <td><?= $parent['endereco']; ?></td>
                                                                                                                    <td><?= $parent['celular']; ?></td>
                                                                                                                    <td>
                                                                                                                        <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                        <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                    </td>
                                                                                                                    <td><?= $parent['ocupacao']; ?></td>
                                                                                                                    <td width="100px">
                                                                                                                        <!--                                                                                <a style="cursor: pointer" onclick="editar3(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                        <a style="cursor: pointer" onclick="remover3(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div id="div_quem_impede3" class="form-group">
                                                                                                    <label for="quem_impede3">NOME</label>
                                                                                                    <input type="hidden" id="parente_id3" name="parente_id3" value=""/>
                                                                                                    <select name="quem_impede3" id="quem_impede3" class="form-control select2">
                                                                                                        <option value="">Selecione o nome do irmão</option>
                                                                                                        <?php
                                                                                                        $result8 = $db->prepare("SELECT id, nome 
                                                                                                           FROM mod_acolhimento_crianca_parentes
                                                                                                           WHERE familia_id = ? AND tipo = 1 
                                                                                                           ORDER BY nome ASC");
                                                                                                        $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                        $result8->execute();
                                                                                                        while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                            ?>
                                                                                                            <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                            <?php
                                                                                                        }
                                                                                                        ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-md-12 text-center">
                                                                                                <button id="inserir_parentesco3" class="btn btn-success">INSERIR</button>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div id="div_abusivo_alcool" class="form-group">
                                                                                            <label for="encontros">Qual a frequência desses contatos/encontros?</label>
                                                                                            <input type="text" class="form-control" name="encontros" id="encontros" value="<?= $frequencia_encontros ?>">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div id="div_descreva" class="form-group">
                                                                                            <label for="como_realizados">Como são realizados? Como é a interação entre o(s) membros(s) da família e a criança durante esses contatos/encontros?</label>
                                                                                            <textarea name="como_realizados" id="como_realizados" class="form-control" cols="30" rows="10"><?= $como_realizados ?></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                            <hr>

                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Há indícios de que os pais/responsáveis ou familiares estejam envolvidos com uso abusivo de álcool?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" class="with-gap radio-col-info" <?= $pais_envolvidos_alcool == 1 ? "checked='true'" : "" ?> name="opcao_retono_alcool" id="sim_abusivo_alcool" value="1">
                                                                                                <label for="sim_abusivo_alcool">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" class="with-gap radio-col-info" <?= $pais_envolvidos_alcool == "" || $pais_envolvidos_alcool == 0 ? "checked='true'" : "" ?> name="opcao_retono_alcool" id="nao_abusivo_alcool" value="1">
                                                                                                <label for="nao_abusivo_alcool">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <br>
                                                                                <div id="div_sim_abusivo_alcool" <?= $pais_envolvidos_alcool == 1 ? "" : "style='display: none'" ?> class="box bl-3 br-3 border-danger">
                                                                                    <div class="box-header">
                                                                                        <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA TEM INDÍCIOS DE ENVOLVIMENTO COM USO ABUSIVO DE ÁLCOOL?</strong></h4>
                                                                                    </div>
                                                                                    <div class="box-body">
                                                                                        <div class="row mt-3">
                                                                                            <div class="col-md-12">
                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-striped mb-2">
                                                                                                        <thead class="thead-light">
                                                                                                            <tr>
                                                                                                                <th scope="col">GRAU PARENTESCO</th>
                                                                                                                <th scope="col">NOME</th>
                                                                                                                <th scope="col">ENDEREÇO</th>
                                                                                                                <th scope="col">TELEFONE</th>
                                                                                                                <th scope="col">DOCUMENTOS</th>
                                                                                                                <th scope="col">OCUPAÇÃO</th>
                                                                                                                <th scope="col"></th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        <tbody id="resultado_parentesco4">
                                                                                                            <?php
                                                                                                            $result = $db->prepare("SELECT *           
                                                                                                                FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                WHERE macp.familia_id = ? AND tipo = 4");
                                                                                                            $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result->execute();
                                                                                                            while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <tr id="remover_parente">
                                                                                                                    <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                    <td><?= $parent['nome']; ?></td>
                                                                                                                    <td><?= $parent['endereco']; ?></td>
                                                                                                                    <td><?= $parent['celular']; ?></td>
                                                                                                                    <td>
                                                                                                                        <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                        <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                    </td>
                                                                                                                    <td><?= $parent['ocupacao']; ?></td>
                                                                                                                    <td width="100px">
                                                                                                                        <!--                                                                                <a style="cursor: pointer" onclick="editar4(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                        <a style="cursor: pointer" onclick="remover4(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12">
                                                                                                <div id="div_quem_imped4" class="form-group">
                                                                                                    <label for="quem_impede4">NOME</label>
                                                                                                    <input type="hidden" id="parente_id4" name="parente_id4" value=""/>
                                                                                                    <select name="quem_impede4" id="quem_impede4" class="form-control select2">
                                                                                                        <option value="">Selecione o nome do irmão</option>
                                                                                                        <?php
                                                                                                        $result8 = $db->prepare("SELECT id, nome 
                                                                                                           FROM mod_acolhimento_crianca_parentes
                                                                                                           WHERE familia_id = ? AND tipo = 1 
                                                                                                           ORDER BY nome ASC");
                                                                                                        $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                        $result8->execute();
                                                                                                        while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                            ?>
                                                                                                            <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                            <?php
                                                                                                        }
                                                                                                        ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-md-12 text-center">
                                                                                                <button id="inserir_parentesco4" class="btn btn-success">INSERIR</button>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <hr>

                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Há indícios de que os pais/responsáveis ou familiares estejam envolvidos com uso abusivo de outra(s) droga(s)?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_envolvidos_outros == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_indicios" id="sim_abusivo_outras_drogas" value="1">
                                                                                                <label for="sim_abusivo_outras_drogas">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_envolvidos_outros == "" || $pais_envolvidos_outros == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_indicios" id="nao_abusivo_outras_drogas" value="1">
                                                                                                <label for="nao_abusivo_outras_drogas">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <div id="div_sim_abusivo_outras_drogas" <?= $pais_envolvidos_outros == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <br>
                                                                                    <div class="box bl-3 br-3 border-danger">
                                                                                        <div class="box-header">
                                                                                            <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA TEM INDÍCIOS DE ENVOLVIMENTO COM USO DE OUTRAS DROGAS?</strong></h4>
                                                                                        </div>
                                                                                        <div class="box-body">
                                                                                            <div class="row mt-3">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped mb-2">
                                                                                                            <thead class="thead-light">
                                                                                                                <tr>
                                                                                                                    <th scope="col">GRAU PARENTESCO</th>
                                                                                                                    <th scope="col">NOME</th>
                                                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                                                    <th scope="col">TELEFONE</th>
                                                                                                                    <th scope="col">DOCUMENTOS</th>
                                                                                                                    <th scope="col">OCUPAÇÃO</th>
                                                                                                                    <th scope="col"></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody id="resultado_parentesco5">
                                                                                                                <?php
                                                                                                                $result = $db->prepare("SELECT *           
                                                                                                                    FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                    WHERE macp.familia_id = ? AND tipo = 5");
                                                                                                                $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                                $result->execute();
                                                                                                                while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                    ?>
                                                                                                                    <tr id="remover_parente">
                                                                                                                        <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                        <td><?= $parent['nome']; ?></td>
                                                                                                                        <td><?= $parent['endereco']; ?></td>
                                                                                                                        <td><?= $parent['celular']; ?></td>
                                                                                                                        <td>
                                                                                                                            <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                            <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                        </td>
                                                                                                                        <td><?= $parent['ocupacao']; ?></td>
                                                                                                                        <td width="100px">
                                                                                                                            <!--                                                                                    <a style="cursor: pointer" onclick="editar5(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                            <a style="cursor: pointer" onclick="remover5(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div id="div_quem_imped5" class="form-group">
                                                                                                        <label for="quem_impede5">NOME</label>
                                                                                                        <input type="hidden" id="parente_id5" name="parente_id5" value=""/>
                                                                                                        <select name="quem_impede5" id="quem_impede5" class="form-control select2">
                                                                                                            <option value="">Selecione o nome do irmão</option>
                                                                                                            <?php
                                                                                                            $result8 = $db->prepare("SELECT id, nome 
                                                                                                               FROM mod_acolhimento_crianca_parentes
                                                                                                               WHERE familia_id = ? AND tipo = 1 
                                                                                                               ORDER BY nome ASC");
                                                                                                            $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result8->execute();
                                                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12 text-center">
                                                                                                    <button id="inserir_parentesco5" class="btn btn-success">INSERIR</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <hr>

                                                                            <!-- ABUSO / EXPLORAÇÃO SEXUAL -->
                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Há indícios de que os pais/responsáveis ou familiares estejam envolvidos com abuso/exploração sexual?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" class="with-gap radio-col-info" <?= $pais_envolvidos_abuso == 1 ? "checked='true'" : "" ?> name="opcao_retono" id="sim_exploracao_sexual" value="1">
                                                                                                <label for="sim_exploracao_sexual">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" class="with-gap radio-col-info" <?= $pais_envolvidos_abuso == "" || $pais_envolvidos_abuso == 0 ? "checked='true'" : "" ?> name="opcao_retono" id="nao_exploracao_sexual" value="1">
                                                                                                <label for="nao_exploracao_sexual">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <div id="div_sim_exploracao_sexual" <?= $pais_envolvidos_abuso == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <br>
                                                                                    <div class="box bl-3 br-3 border-danger">
                                                                                        <div class="box-header">
                                                                                            <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA TEM INDÍCIOS DE ENVOLVIMENTO COM ABUSO/EXPLORAÇÃO SEXUAL?</strong></h4>
                                                                                        </div>
                                                                                        <div class="box-body">
                                                                                            <div class="row mt-3">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped mb-2">
                                                                                                            <thead class="thead-light">
                                                                                                                <tr>
                                                                                                                    <th scope="col">GRAU PARENTESCO</th>
                                                                                                                    <th scope="col">NOME</th>
                                                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                                                    <th scope="col">TELEFONE</th>
                                                                                                                    <th scope="col">DOCUMENTOS</th>
                                                                                                                    <th scope="col">OCUPAÇÃO</th>
                                                                                                                    <th scope="col"></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody id="resultado_parentesco6">
                                                                                                                <?php
                                                                                                                $result = $db->prepare("SELECT *           
                                                                                                                    FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                    WHERE macp.familia_id = ? AND tipo = 6");
                                                                                                                $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                                $result->execute();
                                                                                                                while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                    ?>
                                                                                                                    <tr id="remover_parente">
                                                                                                                        <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                        <td><?= $parent['nome']; ?></td>
                                                                                                                        <td><?= $parent['endereco']; ?></td>
                                                                                                                        <td><?= $parent['celular']; ?></td>
                                                                                                                        <td>
                                                                                                                            <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                            <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                        </td>
                                                                                                                        <td><?= $parent['ocupacao']; ?></td>
                                                                                                                        <td width="100px">
                                                                                                                            <!--                                                                                    <a style="cursor: pointer" onclick="editar6(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                            <a style="cursor: pointer" onclick="remover6(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div id="div_quem_impede6" class="form-group">
                                                                                                        <label for="quem_impede6">NOME</label>
                                                                                                        <input type="hidden" id="parente_id6" name="parente_id6" value=""/>
                                                                                                        <select name="quem_impede6" id="quem_impede6" class="form-control select2">
                                                                                                            <option value="">Selecione o nome do irmão</option>
                                                                                                            <?php
                                                                                                            $result8 = $db->prepare("SELECT id, nome 
                                                                                                               FROM mod_acolhimento_crianca_parentes
                                                                                                               WHERE familia_id = ? AND tipo = 1 
                                                                                                               ORDER BY nome ASC");
                                                                                                            $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result8->execute();
                                                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12 text-center">
                                                                                                    <button id="inserir_parentesco6" class="btn btn-success">INSERIR</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- FIM ABUSO/EXPLORAÇÃO SEXUAL -->

                                                                            <hr>

                                                                            <!-- SITUAÇÃO DE RUA -->
                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Os pais/responsáveis ou familiares estão em situação de rua?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" class="with-gap radio-col-info" <?= $situacao_rua == 1 ? "checked='true'" : "" ?> name="opcao_retono_rua" id="sim_situacao_rua" value="1">
                                                                                                <label for="sim_situacao_rua">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" class="with-gap radio-col-info" <?= $situacao_rua == "" || $situacao_rua == 0 ? "checked='true'" : "" ?> name="opcao_retono_rua" id="nao_situacao_rua" value="1">
                                                                                                <label for="nao_situacao_rua">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <div id="div_sim_situacao_rua" <?= $situacao_rua == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <br>
                                                                                    <div class="box bl-3 br-3 border-danger">
                                                                                        <div class="box-header">
                                                                                            <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA TEM INDÍCIOS DE SITUAÇÃO DE RUA?</strong></h4>
                                                                                        </div>
                                                                                        <div class="box-body">
                                                                                            <div class="row mt-3">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped mb-2">
                                                                                                            <thead class="thead-light">
                                                                                                                <tr>
                                                                                                                    <th scope="col">GRAU PARENTESCO</th>
                                                                                                                    <th scope="col">NOME</th>
                                                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                                                    <th scope="col">TELEFONE</th>
                                                                                                                    <th scope="col">DOCUMENTOS</th>
                                                                                                                    <th scope="col">OCUPAÇÃO</th>
                                                                                                                    <th scope="col"></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody id="resultado_parentesco7">
                                                                                                                <?php
                                                                                                                $result = $db->prepare("SELECT *           
                                                                                                                    FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                    WHERE macp.familia_id = ? AND tipo = 7");
                                                                                                                $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                                $result->execute();
                                                                                                                while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                    ?>
                                                                                                                    <tr id="remover_parente">
                                                                                                                        <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                        <td><?= $parent['nome']; ?></td>
                                                                                                                        <td><?= $parent['endereco']; ?></td>
                                                                                                                        <td><?= $parent['celular']; ?></td>
                                                                                                                        <td>
                                                                                                                            <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                            <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                        </td>
                                                                                                                        <td><?= $parent['ocupacao']; ?></td>
                                                                                                                        <td width="100px">
                                                                                                                            <!--                                                                                    <a style="cursor: pointer" onclick="editar7(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                            <a style="cursor: pointer" onclick="remover7(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div id="div_quem_imped7" class="form-group">
                                                                                                        <label for="quem_impede7">NOME</label>
                                                                                                        <input type="hidden" id="parente_id7" name="parente_id7" value=""/>
                                                                                                        <select name="quem_impede7" id="quem_impede7" class="form-control select2">
                                                                                                            <option value="">Selecione o nome do irmão</option>
                                                                                                            <?php
                                                                                                            $result8 = $db->prepare("SELECT id, nome 
                                                                                                               FROM mod_acolhimento_crianca_parentes
                                                                                                               WHERE familia_id = ? AND tipo = 1 
                                                                                                               ORDER BY nome ASC");
                                                                                                            $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result8->execute();
                                                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12 text-center">
                                                                                                    <button id="inserir_parentesco7" class="btn btn-success">INSERIR</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- FIM SITUAÇÃO DE RUA -->

                                                                            <hr>
                                                                            <!-- cumprimento de pena no sistema prisional -->
                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Os pais/responsáveis ou familiares em cumprimento de pena no sistema prisional?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" class="with-gap radio-col-info" <?= $pais_pena == 1 ? "checked='true'" : "" ?> name="opcao_retono_prisional" id="sim_cumprimento_pena" value="1">
                                                                                                <label for="sim_cumprimento_pena">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" class="with-gap radio-col-info" <?= $pais_pena == "" || $pais_pena == 0 ? "checked='true'" : "" ?> name="opcao_retono_prisional" id="nao_cumprimento_pena" value="1">
                                                                                                <label for="nao_cumprimento_pena">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <div id="div_sim_cumprimento_pena" <?= $pais_pena == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <br>
                                                                                    <div class="box bl-3 br-3 border-danger">
                                                                                        <div class="box-header">
                                                                                            <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA EM CUMPRIMENTO DE PENA NO SISTEMA PRISIONAL?</strong></h4>
                                                                                        </div>
                                                                                        <div class="box-body">
                                                                                            <div class="row mt-3">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped mb-2">
                                                                                                            <thead class="thead-light">
                                                                                                                <tr>
                                                                                                                    <th scope="col">GRAU PARENTESCO</th>
                                                                                                                    <th scope="col">NOME</th>
                                                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                                                    <th scope="col">TELEFONE</th>
                                                                                                                    <th scope="col">DOCUMENTOS</th>
                                                                                                                    <th scope="col">OCUPAÇÃO</th>
                                                                                                                    <th scope="col"></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody id="resultado_parentesco8">
                                                                                                                <?php
                                                                                                                $result = $db->prepare("SELECT *           
                                                                                                                    FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                    WHERE macp.familia_id = ? AND tipo = 8");
                                                                                                                $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                                $result->execute();
                                                                                                                while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                    ?>
                                                                                                                    <tr id="remover_parente">
                                                                                                                        <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                        <td><?= $parent['nome']; ?></td>
                                                                                                                        <td><?= $parent['endereco']; ?></td>
                                                                                                                        <td><?= $parent['celular']; ?></td>
                                                                                                                        <td>
                                                                                                                            <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                            <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                        </td>
                                                                                                                        <td><?= $parent['ocupacao']; ?></td>
                                                                                                                        <td width="100px">
                                                                                                                            <!--                                                                                    <a style="cursor: pointer" onclick="editar8(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                            <a style="cursor: pointer" onclick="remover8(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div id="div_quem_imped8" class="form-group">
                                                                                                        <label for="quem_impede8">NOME</label>
                                                                                                        <input type="hidden" id="parente_id8" name="parente_id8" value=""/>
                                                                                                        <select name="quem_impede8" id="quem_impede8" class="form-control select2">
                                                                                                            <option value="">Selecione o nome do irmão</option>
                                                                                                            <?php
                                                                                                            $result8 = $db->prepare("SELECT id, nome 
                                                                                                               FROM mod_acolhimento_crianca_parentes
                                                                                                               WHERE familia_id = ? AND tipo = 1 
                                                                                                               ORDER BY nome ASC");
                                                                                                            $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result8->execute();
                                                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12 text-center">
                                                                                                    <button id="inserir_parentesco8" class="btn btn-success">INSERIR</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- FIM cumprimento de pena no sistema prisional -->

                                                                            <hr>
                                                                            <!-- REHABILITAÇÃO -->
                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Os pais/responsáveis ou familiares em processo de reabilitação?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_reabilitacao == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_reabilitacao" id="sim_reabilitacao" value="1">
                                                                                                <label for="sim_reabilitacao">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_reabilitacao == "" || $pais_reabilitacao == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_reabilitacao" id="nao_reabilitacao" value="1">
                                                                                                <label for="nao_reabilitacao">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <div id="div_sim_reabilitacao" <?= $pais_reabilitacao == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <br>
                                                                                    <div class="box bl-3 br-3 border-danger">
                                                                                        <div class="box-header">
                                                                                            <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA EM EM PROCESSO DE REABILITAÇÃO?</strong></h4>
                                                                                        </div>
                                                                                        <div class="box-body">
                                                                                            <div class="row mt-3">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped mb-2">
                                                                                                            <thead class="thead-light">
                                                                                                                <tr>
                                                                                                                    <th scope="col">GRAU PARENTESCO</th>
                                                                                                                    <th scope="col">NOME</th>
                                                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                                                    <th scope="col">TELEFONE</th>
                                                                                                                    <th scope="col">DOCUMENTOS</th>
                                                                                                                    <th scope="col">OCUPAÇÃO</th>
                                                                                                                    <th scope="col"></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody id="resultado_parentesco9">
                                                                                                                <?php
                                                                                                                $result = $db->prepare("SELECT *           
                                                                                                                    FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                    WHERE macp.familia_id = ? AND tipo = 9");
                                                                                                                $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                                $result->execute();
                                                                                                                while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                    ?>
                                                                                                                    <tr id="remover_parente">
                                                                                                                        <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                        <td><?= $parent['nome']; ?></td>
                                                                                                                        <td><?= $parent['endereco']; ?></td>
                                                                                                                        <td><?= $parent['celular']; ?></td>
                                                                                                                        <td>
                                                                                                                            <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                            <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                        </td>
                                                                                                                        <td><?= $parent['ocupacao']; ?></td>
                                                                                                                        <td width="100px">
                                                                                                                            <!--                                                                                    <a style="cursor: pointer" onclick="editar9(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                            <a style="cursor: pointer" onclick="remover9(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div id="div_quem_imped9" class="form-group">
                                                                                                        <label for="quem_impede9">NOME</label>
                                                                                                        <input type="hidden" id="parente_id9" name="parente_id9" value=""/>
                                                                                                        <select name="quem_impede9" id="quem_impede9" class="form-control select2">
                                                                                                            <option value="">Selecione o nome do irmão</option>
                                                                                                            <?php
                                                                                                            $result8 = $db->prepare("SELECT id, nome 
                                                                                                               FROM mod_acolhimento_crianca_parentes
                                                                                                               WHERE familia_id = ? AND tipo = 1 
                                                                                                               ORDER BY nome ASC");
                                                                                                            $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result8->execute();
                                                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12 text-center">
                                                                                                    <button id="inserir_parentesco9" class="btn btn-success">INSERIR</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- FIM REHABILITAÇÃO -->

                                                                            <hr>
                                                                            <!-- AMEAÇA DE MORTE -->
                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Os pais/responsáveis ou familiares em situação de ameaça de morte?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_ameacados == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_morte" id="sim_ameaca_morte" value="1">
                                                                                                <label for="sim_ameaca_morte">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_ameacados == "" || $pais_ameacados == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_morte" id="nao_ameaca_morte" value="1">
                                                                                                <label for="nao_ameaca_morte">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <div id="div_sim_ameaca_morte" <?= $pais_ameacados == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <br>
                                                                                    <div class="box bl-3 br-3 border-danger">
                                                                                        <div class="box-header">
                                                                                            <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA EM AMEAÇA DE MORTE?</strong></h4>
                                                                                        </div>
                                                                                        <div class="box-body">
                                                                                            <div class="row mt-3">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped mb-2">
                                                                                                            <thead class="thead-light">
                                                                                                                <tr>
                                                                                                                    <th scope="col">GRAU PARENTESCO</th>
                                                                                                                    <th scope="col">NOME</th>
                                                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                                                    <th scope="col">TELEFONE</th>
                                                                                                                    <th scope="col">DOCUMENTOS</th>
                                                                                                                    <th scope="col">OCUPAÇÃO</th>
                                                                                                                    <th scope="col"></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody id="resultado_parentesco10">
                                                                                                                <?php
                                                                                                                $result = $db->prepare("SELECT *           
                                                                                                                    FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                    WHERE macp.familia_id = ? AND tipo = 10");
                                                                                                                $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                                $result->execute();
                                                                                                                while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                    ?>
                                                                                                                    <tr id="remover_parente">
                                                                                                                        <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                        <td><?= $parent['nome']; ?></td>
                                                                                                                        <td><?= $parent['endereco']; ?></td>
                                                                                                                        <td><?= $parent['celular']; ?></td>
                                                                                                                        <td>
                                                                                                                            <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                            <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                        </td>
                                                                                                                        <td><?= $parent['ocupacao']; ?></td>
                                                                                                                        <td width="100px">
                                                                                                                            <!--                                                                                    <a style="cursor: pointer" onclick="editar10(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                            <a style="cursor: pointer" onclick="remover10(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div id="div_quem_imped10" class="form-group">
                                                                                                        <label for="quem_impede10">NOME</label>
                                                                                                        <input type="hidden" id="parente_id10" name="parente_id10" value=""/>
                                                                                                        <select name="quem_impede10" id="quem_impede10" class="form-control select2">
                                                                                                            <option value="">Selecione o nome do irmão</option>
                                                                                                            <?php
                                                                                                            $result8 = $db->prepare("SELECT id, nome 
                                                                                                               FROM mod_acolhimento_crianca_parentes
                                                                                                               WHERE familia_id = ? AND tipo = 1 
                                                                                                               ORDER BY nome ASC");
                                                                                                            $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result8->execute();
                                                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12 text-center">
                                                                                                    <button id="inserir_parentesco10" class="btn btn-success">INSERIR</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- FIM AMEAÇA DE MORTE -->

                                                                            <hr>
                                                                            <!-- AMEAÇA DE MORTE -->
                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Os pais/responsáveis ou familiares com doença grave/degenerativa?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_doencas == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_degenerativa" id="sim_doenca_grave" value="1">
                                                                                                <label for="sim_doenca_grave">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_doencas == "" || $pais_doencas == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_degenerativa" id="nao_doenca_grave" value="1">
                                                                                                <label for="nao_doenca_grave">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="div_qual_doença" <?= $pais_doencas == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="qual_doença">QUAL DOENÇA?</label>
                                                                                        <input type="text" class="form-control" name="qual_doença" id="qual_doença" placeholder="Informe a doença" value="<?= $qual_doenca; ?>">
                                                                                    </div>
                                                                                </div>
                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <div id="div_sim_doenca_grave" <?= $pais_doencas == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <br>
                                                                                    <div class="box bl-3 br-3 border-danger">
                                                                                        <div class="box-header">
                                                                                            <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA COM DOENÇA GRAVE/DEGENERATIVA?</strong></h4>
                                                                                        </div>
                                                                                        <div class="box-body">
                                                                                            <div class="row mt-3">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped mb-2">
                                                                                                            <thead class="thead-light">
                                                                                                                <tr>
                                                                                                                    <th scope="col">GRAU PARENTESCO</th>
                                                                                                                    <th scope="col">NOME</th>
                                                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                                                    <th scope="col">TELEFONE</th>
                                                                                                                    <th scope="col">DOCUMENTOS</th>
                                                                                                                    <th scope="col">OCUPAÇÃO</th>
                                                                                                                    <th scope="col"></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody id="resultado_parentesco11">
                                                                                                                <?php
                                                                                                                $result = $db->prepare("SELECT *           
                                                                                                                    FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                    WHERE macp.familia_id = ? AND tipo = 11");
                                                                                                                $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                                $result->execute();
                                                                                                                while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                    ?>
                                                                                                                    <tr id="remover_parente">
                                                                                                                        <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                        <td><?= $parent['nome']; ?></td>
                                                                                                                        <td><?= $parent['endereco']; ?></td>
                                                                                                                        <td><?= $parent['celular']; ?></td>
                                                                                                                        <td>
                                                                                                                            <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                            <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                        </td>
                                                                                                                        <td><?= $parent['ocupacao']; ?></td>
                                                                                                                        <td width="100px">
                                                                                                                            <!--                                                                                    <a style="cursor: pointer" onclick="editar11(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                            <a style="cursor: pointer" onclick="remover11(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div id="div_quem_imped11" class="form-group">
                                                                                                        <label for="quem_impede11">NOME</label>
                                                                                                        <input type="hidden" id="parente_id11" name="parente_id11" value=""/>
                                                                                                        <select name="quem_impede11" id="quem_impede11" class="form-control select2">
                                                                                                            <option value="">Selecione o nome do irmão</option>
                                                                                                            <?php
                                                                                                            $result8 = $db->prepare("SELECT id, nome 
                                                                                                               FROM mod_acolhimento_crianca_parentes
                                                                                                               WHERE familia_id = ? AND tipo = 1 
                                                                                                               ORDER BY nome ASC");
                                                                                                            $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result8->execute();
                                                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-12 text-center">
                                                                                                    <button id="inserir_parentesco11" class="btn btn-success">INSERIR</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- FIM DOENÇA DEGENERATIVA -->

                                                                            <hr>
                                                                            <!-- AMEAÇA DE MORTE -->
                                                                            <div class="row mt-3">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label>Os pais/responsáveis ou familiares com transtorno mental?</label>
                                                                                        <div class="row">
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_transtorno == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_mental" id="sim_transtorno" value="1">
                                                                                                <label for="sim_transtorno">SIM</label>
                                                                                            </div>
                                                                                            <div class="col-sm-2">
                                                                                                <input type="radio" <?= $pais_transtorno == "" || $pais_transtorno == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_mental" id="nao_transtorno" value="1">
                                                                                                <label for="nao_transtorno">NÃO</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div id="div_transtorno" <?= $pais_transtorno == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="transtorno">QUAL TRASNTORNO?</label>
                                                                                        <input type="text" class="form-control" name="transtorno" id="transtorno" placeholder="Informe o transtorno" value="<?= $qual_transtorno; ?>">
                                                                                    </div>
                                                                                </div>

                                                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                                                <div id="div_sim_transtorno" <?= $pais_transtorno == 1 ? "" : "style='display: none'" ?> class="col-md-12">
                                                                                    <br>
                                                                                    <div class="box bl-3 br-3 border-danger">
                                                                                        <div class="box-header">
                                                                                            <h4 class="box-title mb-0"><strong>QUAL(IS) MEMBRO(S) DA FAMÍLIA COM TRANSTORNO MENTAL?</strong></h4>
                                                                                        </div>
                                                                                        <div class="box-body">
                                                                                            <div class="row mt-3">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="table-responsive">
                                                                                                        <table class="table table-striped mb-2">
                                                                                                            <thead class="thead-light">
                                                                                                                <tr>
                                                                                                                    <th scope="col">GRAU PARENTESCO</th>
                                                                                                                    <th scope="col">NOME</th>
                                                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                                                    <th scope="col">TELEFONE</th>
                                                                                                                    <th scope="col">DOCUMENTOS</th>
                                                                                                                    <th scope="col">OCUPAÇÃO</th>
                                                                                                                    <th scope="col"></th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <tbody id="resultado_parentesco12">
                                                                                                                <?php
                                                                                                                $result = $db->prepare("SELECT *           
                                                                                                                    FROM mod_acolhimento_crianca_parentes macp   
                                                                                                                    WHERE macp.familia_id = ? AND tipo = 12");
                                                                                                                $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                                $result->execute();
                                                                                                                while ($parent = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                    ?>
                                                                                                                    <tr id="remover_parente">
                                                                                                                        <th><?= grau_parentesco($parent['grau']); ?></th>
                                                                                                                        <td><?= $parent['nome']; ?></td>
                                                                                                                        <td><?= $parent['endereco']; ?></td>
                                                                                                                        <td><?= $parent['celular']; ?></td>
                                                                                                                        <td>
                                                                                                                            <span style="display: block;"><small>CPF:</small> <?= $parent['cpf']; ?></span>
                                                                                                                            <span style="display: block;"><small>RG:</small> <?= $parent['rg']; ?></span>
                                                                                                                        </td>
                                                                                                                        <td><?= $parent['ocupacao']; ?></td>
                                                                                                                        <td width="100px">
                                                                                                                            <!--                                                                                    <a style="cursor: pointer" onclick="editar12(<?= $parent['id']; ?>, '<?= $parent['nome']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>-->
                                                                                                                            <a style="cursor: pointer" onclick="remover12(this, <?= $parent['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div id="div_quem_imped12" class="form-group">
                                                                                                        <label for="quem_impede12">NOME</label>
                                                                                                        <input type="hidden" id="parente_id12" name="parente_id12" value=""/>
                                                                                                        <select name="quem_impede12" id="quem_impede12" class="form-control select2">
                                                                                                            <option value="">Selecione o nome do irmão</option>
                                                                                                            <?php
                                                                                                            $result8 = $db->prepare("SELECT id, nome 
                                                                                                               FROM mod_acolhimento_crianca_parentes
                                                                                                               WHERE familia_id = ? AND tipo = 1 
                                                                                                               ORDER BY nome ASC");
                                                                                                            $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                                            $result8->execute();
                                                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                                                ?>
                                                                                                                <option value='<?= $irm['nome']; ?>'><?= $irm['nome']; ?></option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12 text-center">
                                                                                                    <button id="inserir_parentesco12" class="btn btn-success">INSERIR</button>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- FIM DOENÇA DEGENERATIVA -->

                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIM LINHA -->

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="box box-solid bg-info">
                                                <div class="box-header">
                                                    <h4 class="box-title mb-0">INFORMAÇÃO SOBRE OS IRMÃO DA CRIANÇA</h4>
                                                </div>
                                                <div class="box-body">
                                                    <div class="box box-outline-info">
                                                        <div class="box-body">
                                                            <div class="row">
                                                                <div class="col-md-12">

                                                                    <div class="row mt-4">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>A criança possui irmãos neste serviço de acolhimento?</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $possui_irmaos == 1 ? "checked='true'" : "" ?> name="opcao_irmaos" id="com_irmaos_servico_acolhimento" value="1">
                                                                                        <label for="com_irmaos_servico_acolhimento">SIM</label>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $possui_irmaos == "" || $possui_irmaos == 0 ? "checked='true'" : "" ?> name="opcao_irmaos" id="sem_irmaos_servico_acolhimento" value="1">
                                                                                        <label for="sem_irmaos_servico_acolhimento">NÃO</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>

                                                                    <div <?= $possui_irmaos == 1 ? "" : "style='display: none'" ?> id="div_com_irmaos_servico_acolhimento">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped mb-2">
                                                                                <thead class="thead-light">
                                                                                    <tr>
                                                                                        <th scope="col" width="30%">NOME</th>
                                                                                        <th scope="col" width="100px">IDADE</th>
                                                                                        <th scope="col">ACOLHIMENTO</th>
                                                                                        <th scope="col">OBSERVAÇÃO</th>
                                                                                        <th scope="col"></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="resultado_irmaos">
                                                                                    <?php
                                                                                    $result = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                                                                                        FROM mod_acolhimento_crianca_irmaos maci  
                                                                                        LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                                                                                        WHERE maci.familia_id = ? AND maci.tipo = 1");
                                                                                    $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                    $result->execute();
                                                                                    while ($irmaos = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                        ?>
                                                                                        <tr id="remover_irmao">
                                                                                            <td><?= $irmaos['irmao']; ?></td>
                                                                                            <td><?= $irmaos['idade']; ?> ANOS</td>
                                                                                            <td><?= obterDataBRTimestamp($irmaos['data_acolhimento']); ?></td>
                                                                                            <td>
                                                                                                <p><?= $irmaos['obs']; ?></p>
                                                                                            </td>
                                                                                            <td width="100px">
                                                                                                <a style="cursor: pointer" onclick="editar_irmao(<?= $irmaos['id']; ?>, '<?= $irmaos['irmao_id']; ?>', '<?= $irmaos['idade']; ?>', '<?= $irmaos['data_acolhimento']; ?>', '<?= $irmaos['obs']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                                                <a style="cursor: pointer" onclick="remover_irmao(this, <?= $irmaos['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>

                                                                        <hr>

                                                                        <div class="row mt-4">
                                                                            <div class="col-md-6">
                                                                                <div id="div_nome_irmao" class="form-group">
                                                                                    <label for="nome_irmao">NOME</label>
                                                                                    <input type="hidden" id="irmao_id" name="irmao_id" value=""/>
                                                                                    <select name="nome_irmao" id="nome_irmao" class="form-control select2">
                                                                                        <option value="">Selecione o nome do irmão</option>
                                                                                        <?php
                                                                                        $result8 = $db->prepare("SELECT id, nome 
                                                                                           FROM mod_acolhimento_crianca_parentes
                                                                                           WHERE grau = 1 AND familia_id = ?
                                                                                           GROUP BY nome
                                                                                           ORDER BY nome ASC");
                                                                                        $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                        $result8->execute();
                                                                                        while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                            ?>
                                                                                            <option value='<?= $irm['id']; ?>'><?= $irm['nome']; ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div id="div_idade_irmao" class="form-group">
                                                                                    <label for="idade_irmao">IDADE</label>
                                                                                    <input type="text" class="form-control" name="idade_irmao" id="idade_irmao" placeholder="Idade" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div id="div_data_acolhimento" class="form-group">
                                                                                    <label for="data_acolhimento">DATA DO ACOLHIMENTO</label>
                                                                                    <input class="form-control" name="data_acolhimento" id="data_acolhimento" type="date" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div id="div_descreva_observacao_irmaos_servico_acolhimento" class="form-group">
                                                                                    <label for="descreva_observacao_irmaos_servico_acolhimento">OBSERVAÇÃO</label>
                                                                                    <textarea name="descreva_observacao_irmaos_servico_acolhimento" id="descreva_observacao_irmaos_servico_acolhimento" class="form-control" cols="30" rows="10"></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mt-3">
                                                                            <div class="col-md-12 text-center">
                                                                                <button id="inserir_irmao" type="button" class="btn btn-success">INSERIR</button>
                                                                            </div>
                                                                        </div>

                                                                        <br><hr>

                                                                    </div>

                                                                    <div class="row mt-4">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>A criança possui irmãos em outros serviços de acolhimento?</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $possui_irmaos_outro == 1 ? "checked='true'" : "" ?> name="opcao_retono_outro" id="com_irmaos_outros_servico_acolhimento" value="1">
                                                                                        <label for="com_irmaos_outros_servico_acolhimento">SIM</label>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $possui_irmaos_outro == "" || $possui_irmaos_outro == 0 ? "checked='true'" : "" ?> name="opcao_retono_outro" id="sem_irmaos_outros_servico_acolhimento" value="1">
                                                                                        <label for="sem_irmaos_outros_servico_acolhimento">NÃO</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>

                                                                    <div <?= $possui_irmaos_outro == 1 ? "" : "style='display: none'" ?> id="div_com_irmaos_outros_servico_acolhimento">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped mb-2">
                                                                                <thead class="thead-light">
                                                                                    <tr>
                                                                                        <th scope="col" width="30%">NOME</th>
                                                                                        <th scope="col" width="100px">IDADE</th>
                                                                                        <th scope="col">ACOLHIMENTO</th>
                                                                                        <th scope="col">OBSERVAÇÃO</th>
                                                                                        <th scope="col"></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="resultado_irmaos2">
                                                                                    <?php
                                                                                    $result = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                                                                                        FROM mod_acolhimento_crianca_irmaos maci  
                                                                                        LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                                                                                        WHERE maci.familia_id = ? AND maci.tipo = 2");
                                                                                    $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                    $result->execute();
                                                                                    while ($irmaos = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                        ?>
                                                                                        <tr id="remover_irmao">
                                                                                            <td><?= $irmaos['irmao']; ?></td>
                                                                                            <td><?= $irmaos['idade']; ?> ANOS</td>
                                                                                            <td><?= obterDataBRTimestamp($irmaos['data_acolhimento']); ?></td>
                                                                                            <td>
                                                                                                <p><?= $irmaos['obs']; ?></p>
                                                                                            </td>
                                                                                            <td width="100px">
                                                                                                <a style="cursor: pointer" onclick="editar_irmao2(<?= $irmaos['id']; ?>, '<?= $irmaos['irmao_id']; ?>', '<?= $irmaos['idade']; ?>', '<?= $irmaos['data_acolhimento']; ?>', '<?= $irmaos['obs']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                                                <a style="cursor: pointer" onclick="remover_irmao2(this, <?= $irmaos['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </tbody>

                                                                            </table>
                                                                        </div>

                                                                        <hr>

                                                                        <div class="row mt-4">
                                                                            <div class="col-md-6">
                                                                                <div id="div_nome_irmao2" class="form-group">
                                                                                    <label for="nome_irmao2">NOME</label>
                                                                                    <input type="hidden" id="irmao_id2" name="irmao_id2" value=""/>
                                                                                    <select name="nome_irmao2" id="nome_irmao2" class="form-control select2">
                                                                                        <option value="">Selecione o nome do irmão</option>
                                                                                        <?php
                                                                                        $result8 = $db->prepare("SELECT id, nome 
                                                                                           FROM mod_acolhimento_crianca_parentes
                                                                                           WHERE grau = 1 AND familia_id = ?
                                                                                           GROUP BY nome
                                                                                           ORDER BY nome ASC");
                                                                                        $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                        $result8->execute();
                                                                                        while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                            ?>
                                                                                            <option value='<?= $irm['id']; ?>'><?= $irm['nome']; ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div id="div_idade_irmao2" class="form-group">
                                                                                    <label for="idade_irmao2">IDADE</label>
                                                                                    <input type="text" class="form-control" name="idade_irmao2" id="idade_irmao2" placeholder="Idade" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div id="div_data_acolhimento2" class="form-group">
                                                                                    <label for="data_acolhimento2">DATA DO ACOLHIMENTO</label>
                                                                                    <input class="form-control" name="data_acolhimento2" id="data_acolhimento2" type="date" value="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div id="div_descreva_observacao_irmaos_servico_acolhimento2" class="form-group">
                                                                                    <label for="descreva_observacao_irmaos_servico_acolhimento2">OBSERVAÇÃO</label>
                                                                                    <textarea name="descreva_observacao_irmaos_servico_acolhimento2" id="descreva_observacao_irmaos_servico_acolhimento2" class="form-control" cols="30" rows="10"></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mt-3">
                                                                            <div class="col-md-12 text-center">
                                                                                <button id="inserir_irmao2" type="button" class="btn btn-success">INSERIR</button>
                                                                            </div>
                                                                        </div>

                                                                        <br><hr>
                                                                    </div>

                                                                    <div class="row mt-4">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>A criança conhece ou tem vínculo com o(s) irmão(s) em outro(s) serviço(s) de acolhimento?</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $conhece_irmaos == 1 ? "checked='true'" : "" ?>  name="opcao_conhece" id="com_irmaos_em_servico_acolhimento" value="1">
                                                                                        <label for="com_irmaos_em_servico_acolhimento">SIM</label>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" class="with-gap radio-col-info" <?= $conhece_irmaos == "" || $conhece_irmaos == 0 ? "checked='true'" : "" ?> name="opcao_conhece" id="sem_irmaos_em_servico_acolhimento" value="1">
                                                                                        <label for="sem_irmaos_em_servico_acolhimento">NÃO</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>

                                                                    <div <?= $conhece_irmaos == 1 ? "" : "style='display: none'" ?> id="div_com_irmaos_em_servico_acolhimento">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped mb-2">
                                                                                <thead class="thead-light">
                                                                                    <tr>
                                                                                        <th scope="col">NOME</th>
                                                                                        <th scope="col">IDADE</th>
                                                                                        <th scope="col" width="50px;"></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="resultado_irmaos3">
                                                                                    <?php
                                                                                    $result = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                                                                                        FROM mod_acolhimento_crianca_irmaos maci  
                                                                                        LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                                                                                        WHERE maci.familia_id = ? AND maci.tipo = 3");
                                                                                    $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                    $result->execute();
                                                                                    while ($irmaos = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                        ?>
                                                                                        <tr id="remover_irmao">
                                                                                            <td><?= $irmaos['irmao']; ?></td>
                                                                                            <td><?= $irmaos['idade']; ?> ANOS</td>
                                                                                            <td width="100px">
                                                                                                <a style="cursor: pointer" onclick="editar_irmao3(<?= $irmaos['id']; ?>, '<?= $irmaos['irmao_id']; ?>', '<?= $irmaos['idade']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                                                <a style="cursor: pointer" onclick="remover_irmao3(this, <?= $irmaos['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>

                                                                        <hr>

                                                                        <div class="row mt-4">
                                                                            <div class="col-md-9">
                                                                                <div id="div_nome_irmao3" class="form-group">
                                                                                    <label for="nome_irmao3">NOME</label>
                                                                                    <input type="hidden" id="irmao_id3" name="irmao_id3" value=""/>
                                                                                    <select name="nome_irmao3" id="nome_irmao3" class="form-control select2">
                                                                                        <option value="">Selecione o nome do irmão</option>
                                                                                        <?php
                                                                                        $result8 = $db->prepare("SELECT id, nome 
                                                                                           FROM mod_acolhimento_crianca_parentes
                                                                                           WHERE grau = 1 AND familia_id = ?
                                                                                           GROUP BY nome
                                                                                           ORDER BY nome ASC");
                                                                                        $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                        $result8->execute();
                                                                                        while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                            ?>
                                                                                            <option value='<?= $irm['id']; ?>'><?= $irm['nome']; ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div id="div_idade_irmao3" class="form-group">
                                                                                    <label for="idade_irmao3">IDADE</label>
                                                                                    <input type="text" class="form-control" name="idade_irmao3" id="idade_irmao3" placeholder="Idade" value="">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mt-3">
                                                                            <div class="col-md-12 text-center">
                                                                                <button id="inserir_irmao3" type="button" class="btn btn-success">INSERIR</button>
                                                                            </div>
                                                                        </div>
                                                                        <br><hr>
                                                                    </div>

                                                                    <div class="row mt-4">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label>A criança possui irmão(s) que foi(ram) adotado(s)?</label>
                                                                                <div class="row">
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" <?= $irmaos_adotados == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_adotado" id="com_irmaos_adotados" value="1">
                                                                                        <label for="com_irmaos_adotados">SIM</label>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <input type="radio" <?= $irmaos_adotados == "" || $irmaos_adotados == 0 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="opcao_retono_adotado" id="sem_irmaos_adotados" value="1">
                                                                                        <label for="sem_irmaos_adotados">NÃO</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br>

                                                                    <div <?= $irmaos_adotados == 1 ? "" : "style='display: none'" ?> id="div_com_irmaos_adotados">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-striped mb-2">
                                                                                <thead class="thead-light">
                                                                                    <tr>
                                                                                        <th scope="col">NOME</th>
                                                                                        <th scope="col">IDADE</th>
                                                                                        <th scope="col" width="50px;"></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="resultado_irmaos4">
                                                                                    <?php
                                                                                    $result = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                                                                                        FROM mod_acolhimento_crianca_irmaos maci  
                                                                                        LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                                                                                        WHERE maci.familia_id = ? AND maci.tipo = 4");
                                                                                    $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                    $result->execute();
                                                                                    while ($irmaos = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                        ?>
                                                                                        <tr id="remover_irmao">
                                                                                            <td><?= $irmaos['irmao']; ?></td>
                                                                                            <td><?= $irmaos['idade']; ?> ANOS</td>
                                                                                            <td width="100px">
                                                                                                <a style="cursor: pointer" onclick="editar_irmao4(<?= $irmaos['id']; ?>, '<?= $irmaos['irmao_id']; ?>', '<?= $irmaos['idade']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                                                <a style="cursor: pointer" onclick="remover_irmao4(this, <?= $irmaos['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>

                                                                        <hr>

                                                                        <div class="row mt-4">
                                                                            <div class="col-md-9">
                                                                                <div id="div_nome_irmao4" class="form-group">
                                                                                    <label for="nome_irmao4">NOME</label>
                                                                                    <input type="hidden" id="irmao_id4" name="irmao_id4" value=""/>
                                                                                    <select name="nome_irmao4" id="nome_irmao4" class="form-control select2">
                                                                                        <option value="">Selecione o nome do irmão</option>
                                                                                        <?php
                                                                                        $result8 = $db->prepare("SELECT id, nome 
                                                                                           FROM mod_acolhimento_crianca_parentes
                                                                                           WHERE grau = 1 AND familia_id = ?
                                                                                           GROUP BY nome
                                                                                           ORDER BY nome ASC");
                                                                                        $result8->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                                        $result8->execute();
                                                                                        while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                            ?>
                                                                                            <option value='<?= $irm['id']; ?>'><?= $irm['nome']; ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div id="div_idade_irmao4" class="form-group">
                                                                                    <label for="idade_irmao4">IDADE</label>
                                                                                    <input type="text" class="form-control" name="idade_irmao4" id="idade_irmao4" placeholder="Idade" value="">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mt-3">
                                                                            <div class="col-md-12 text-center">
                                                                                <button id="inserir_irmao4" type="button" class="btn btn-success">INSERIR</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIM LINHA -->

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="box box-solid bg-info">
                                                <div class="box-header">
                                                    <h4 class="box-title mb-0">INFORMAÇÃO SOBRE O RESPONSÁVEL PELA CRIANÇA</h4>
                                                </div>
                                                <div class="box-body">
                                                    <div class="box box-outline-info">
                                                        <div class="box-body">
                                                            <div class="row mt-4 mb-4">
                                                                <!-- /.col -->
                                                                <div class="col-md-12 col-12">
                                                                    <div class="form-group">
                                                                        <label>PESSOA RESPONSÁVEL <small class="text-warning">(pessoa de referência com quem a criança morava, no caso de não residir com os pais)</small></label>
                                                                        <select class="form-control select2" id="pessoa_responsavel" name="pessoa_responsavel[]" multiple="multiple" data-placeholder="Selecione a(s) pessoas responsáveis pela Criança" style="width: 100%;">
                                                                            <?php
                                                                            $result = $db->prepare("SELECT *    
                                                                                                    FROM mod_acolhimento_crianca_familia_responsaveis macfr
                                                                                                    LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = macfr.responsavel_id  
                                                                                                    WHERE macp.familia_id = ? AND macp.tipo = 1");
                                                                            $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                            $result->execute();
                                                                            while ($resp = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <option selected="true" value="<?= $resp['id']; ?>"><?= $resp['nome']; ?></option>
                                                                                <?php
                                                                            }

                                                                            $result = $db->prepare("SELECT *           
                                                                                                    FROM mod_acolhimento_crianca_parentes AS macp   
                                                                                                    WHERE macp.familia_id = ? AND macp.tipo = 1 AND macp.id NOT IN(SELECT responsavel_id FROM mod_acolhimento_crianca_familia_responsaveis WHERE familia_id = ?)");
                                                                            $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                            $result->bindValue(2, $mod_acolhimento_crianca_familia_id);
                                                                            $result->execute();
                                                                            while ($resp = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <option value="<?= $resp['id']; ?>"><?= $resp['nome']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <!-- /.form-group -->
                                                                </div>
                                                                <!-- /.col -->
                                                            </div>

                                                            <div class="row mt-4">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Os pais/responsáveis ou familiares oferecem as condições de cuidado e proteção necessárias para o retorno ao convívio com a criança?</label>
                                                                        <div class="row">
                                                                            <div class="col-sm-2">
                                                                                <input type="radio" class="with-gap radio-col-info" <?= $oferecem_condicoes == 1 ? "checked='true'" : "" ?> name="opcao_retono_convivo" id="com_condicoes_convivio" value="1">
                                                                                <label for="com_condicoes_convivio">SIM</label>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <input type="radio" class="with-gap radio-col-info" <?= $oferecem_condicoes == "" || $oferecem_condicoes == 0 ? "checked='true'" : "" ?> name="opcao_retono_convivo" id="sem_condicoes_convivio" value="1">
                                                                                <label for="sem_condicoes_convivio">NÃO</label>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <input type="radio" class="with-gap radio-col-info" <?= $oferecem_condicoes == 2 ? "checked='true'" : "" ?> name="opcao_retono_convivo" id="avaliacao_profunda" value="1">
                                                                                <label for="avaliacao_profunda">FAZ-SE NECESSÁRIA UMA AVALIAÇÃO PROFUNDA</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="div_avaliacao_profunda" class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="descreva_avaliacao_profunda">Descreva</label>
                                                                        <textarea name="descreva_avaliacao_profunda" id="descreva_avaliacao_profunda" class="form-control" cols="30" rows="10"><?= $avaliacao_profunda; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="box box-solid bg-info">
                                                <div class="box-header">
                                                    <h4 class="box-title mb-0">INFORMAÇÃO SOBRE O RETORNO AO CONVÍVIO DA CRIANÇA</h4>
                                                </div>
                                                <div class="box-body">
                                                    <div class="box box-outline-info">
                                                        <div class="box-body">

                                                            <div class="row mt-4 mb-4">
                                                                <!-- /.col -->
                                                                <div class="col-md-12 col-12">
                                                                    <div id="div_crianca_convive" class="form-group">
                                                                        <label>Familiares com os quais a criança convive ou mantem vínculo de afinidade / afetividade</label>
                                                                        <select class="form-control select2" id="crianca_convive" name="crianca_convive[]" multiple="multiple" data-placeholder="Selecione a(s) pessoas responsáveis pela Criança" style="width: 100%;">
                                                                            <?php
                                                                            $result2 = $db->prepare("SELECT *    
                                                                                                    FROM mod_acolhimento_crianca_familia_convive macfr
                                                                                                    LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = macfr.responsavel_id  
                                                                                                    WHERE macp.familia_id = ? AND macp.tipo = 1");
                                                                            $result2->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                            $result2->execute();
                                                                            while ($resp2 = $result2->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <option selected="true" value="<?= $resp2['id']; ?>"><?= $resp2['nome']; ?></option>
                                                                                <?php
                                                                            }

                                                                            $result3 = $db->prepare("SELECT *            
                                                                                                    FROM mod_acolhimento_crianca_parentes AS macp 
                                                                                                    WHERE macp.familia_id = ? AND macp.tipo = 1 AND macp.id NOT IN(SELECT responsavel_id FROM mod_acolhimento_crianca_familia_convive WHERE familia_id = ?)");
                                                                            $result3->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                            $result3->bindValue(2, $mod_acolhimento_crianca_familia_id);
                                                                            $result3->execute();
                                                                            while ($resp3 = $result3->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <option value="<?= $resp3['id']; ?>"><?= $resp3['nome']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <!-- /.form-group -->
                                                                </div>
                                                                <!-- /.col -->
                                                            </div>

                                                            <hr>
                                                            <br>

                                                            <div class="row mt-2">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Caso haja familiares, há interessado(s) em acolher a criança?</label>
                                                                        <div class="row">
                                                                            <div class="col-sm-2">
                                                                                <input type="radio" class="with-gap radio-col-info" <?= $familiar_interesse == 1 ? "checked='true'" : "" ?> name="opcao_familiar_interesse" id="familiar_interesse_sim" value="1">
                                                                                <label for="familiar_interesse_sim">SIM</label>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <input type="radio" class="with-gap radio-col-info" <?= $familiar_interesse == "" || $familiar_interesse == 0 ? "checked='true'" : "" ?> name="opcao_familiar_interesse" id="familiar_interesse_nao" value="1">
                                                                                <label for="familiar_interesse_nao">NÃO</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div id="div_quem_acolhe" <?= $familiar_interesse == 1 ? "" : "style='display: none'" ?> class="row mt-2 mb-2">
                                                                <!-- /.col -->
                                                                <div class="col-md-12 col-12">
                                                                    <div class="form-group">
                                                                        <label>QUEM?</label>
                                                                        <select class="form-control select2" id="quem" name="quem[]" multiple="multiple" data-placeholder="Selecione quem" style="width: 100%;">
                                                                            <?php
                                                                            $result2 = $db->prepare("SELECT *    
                                                                                                    FROM mod_acolhimento_crianca_familia_interessados macfr
                                                                                                    LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = macfr.responsavel_id  
                                                                                                    WHERE macp.familia_id = ? AND macp.tipo = 1");
                                                                            $result2->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                            $result2->execute();
                                                                            while ($resp2 = $result2->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <option selected="true" value="<?= $resp2['id']; ?>"><?= $resp2['nome']; ?></option>
                                                                                <?php
                                                                            }

                                                                            $result3 = $db->prepare("SELECT *             
                                                                                                    FROM mod_acolhimento_crianca_parentes AS macp  
                                                                                                    WHERE macp.familia_id = ? AND macp.tipo = 1 AND macp.id NOT IN(SELECT responsavel_id FROM mod_acolhimento_crianca_familia_interessados WHERE familia_id = ?)");
                                                                            $result3->bindValue(1, $mod_acolhimento_crianca_familia_id);
                                                                            $result3->bindValue(2, $mod_acolhimento_crianca_familia_id);
                                                                            $result3->execute();
                                                                            while ($resp3 = $result3->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <option value="<?= $resp3['id']; ?>"><?= $resp3['nome']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <!-- /.form-group -->
                                                                </div>
                                                                <!-- /.col -->
                                                            </div>

                                                            <div class="row mt-4">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>Estes(s) parente(s) interessado(s) oferece(m) as condições de cuidado e proteção necessárias para o retorno ao convívio com a criança?</label>
                                                                        <div class="row">
                                                                            <div class="col-sm-2">
                                                                                <input type="radio" class="with-gap radio-col-info" <?= $familiar_condicao == 1 || $familiar_condicao == "" ? "checked='true'" : "" ?> name="opcao_familiar_condicao" id="familiar_condicoes_sim" value="1">
                                                                                <label for="familiar_condicoes_sim">SIM</label>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <input type="radio" class="with-gap radio-col-info" <?= $familiar_condicao == 2 ? "checked='true'" : "" ?> name="opcao_familiar_condicao" id="familia_condicoes_nao" value="2">
                                                                                <label for="familia_condicoes_nao">NÃO</label>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <input type="radio" class="with-gap radio-col-info" <?= $familiar_condicao == 3 ? "checked='true'" : "" ?> name="opcao_familiar_condicao" id="avaliacao_profunda2" value="3">
                                                                                <label for="avaliacao_profunda2">FAZ-SE NECESSÁRIA UMA AVALIAÇÃO PROFUNDA</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div id="div_avaliacao_profunda" class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="descreva_condicao_cuidado">Descreva</label>
                                                                        <textarea name="descreva_condicao_cuidado" id="descreva_condicao_cuidado" class="form-control" cols="30" rows="10"><?= $descricao_condicao_cuidado; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row mt-4">
                                                                <div class="col-md-12">
                                                                    <div id="div_opniniao_crianca">
                                                                        <div class="form-group">
                                                                            <label for="opiniao_crianca">Qual a opinião da criança quanto à possibilidade de ficar sob os cuidados desse(s) parente(s)?</label>
                                                                            <textarea name="opiniao_crianca" id="opiniao_crianca" class="form-control" cols="30" rows="10"><?= $opniao_crianca; ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    </section>
                                    </form>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->

                            <div class="row mb-2">
                                <div class="col-md-12 text-center">
                                    <button id="atualizar" type="button" class="btn btn-info">ATUALIZAR</button>
                                </div>
                            </div>
                        </section>
                        <!-- /.content -->
                </div>
            </div>
            <!-- /.content-wrapper -->

            <?php include 'layout/footer.php'; ?>

            <!-- JS -->
            <script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/pia/familia.js"></script>