<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;

$tipo_atendimento = isset($_POST['opcao_retono_atendimento']) ? $_POST['opcao_retono_atendimento'] : 0;
$tipo = isset($_POST['opcao_retono_tipo']) ? $_POST['opcao_retono_tipo'] : 0;
$tipo_consulta = isset($_POST['opcao_retono_consulta']) ? $_POST['opcao_retono_consulta'] : 0;

$data_consulta = isset($_POST['data_consulta']) ? $_POST['data_consulta'] : NULL;
$hora_consulta = isset($_POST['hora_consulta']) ? $_POST['hora_consulta'] : NULL;
$observacao = isset($_POST['observacao']) ? $_POST['observacao'] : NULL;
$nome_crianca = isset($_POST['nome_crianca']) ? $_POST['nome_crianca'] : NULL;
$nome_acompanhante = isset($_POST['nome_acompanhante']) ? $_POST['nome_acompanhante'] : NULL;
$preparativos = isset($_POST['preparativos']) ? $_POST['preparativos'] : NULL;
$nome_profissional = isset($_POST['nome_profissional']) ? $_POST['nome_profissional'] : NULL;
$unidade_saude = isset($_POST['unidade_saude']) ? $_POST['unidade_saude'] : NULL;
$endereco_unidade_saude = isset($_POST['endereco_unidade_saude']) ? $_POST['endereco_unidade_saude'] : NULL;
$numero_unidade_saude = isset($_POST['numero_unidade_saude']) ? $_POST['numero_unidade_saude'] : NULL;
$bairro_unidade_saude = isset($_POST['bairro_unidade_saude']) ? $_POST['bairro_unidade_saude'] : NULL;
$opniao_medica = isset($_POST['opniao_medica']) ? $_POST['opniao_medica'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_saude_consultas (tipo_atendimento, tipo, tipo_consulta, data_consulta, hora_consulta, obs, crianca_id, acomp_id, preparativos, profissional_id, unidade, endereco, numero, bairro, diagnostico, data_update, responsavel_id, data_cadastro, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $tipo_atendimento);
            $sql->bindValue(2, $tipo);
            $sql->bindValue(3, $tipo_consulta);
            $sql->bindValue(4, $data_consulta);
            $sql->bindValue(5, $hora_consulta);
            $sql->bindValue(6, $observacao);
            $sql->bindValue(7, $nome_crianca);
            $sql->bindValue(8, $nome_acompanhante);
            $sql->bindValue(9, $preparativos);
            $sql->bindValue(10, $nome_profissional);
            $sql->bindValue(11, $unidade_saude);
            $sql->bindValue(12, $endereco_unidade_saude);
            $sql->bindValue(13, $numero_unidade_saude);
            $sql->bindValue(14, $bairro_unidade_saude);
            $sql->bindValue(15, $opniao_medica);
            $sql->bindValue(16, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE mod_saude_consultas SET tipo_atendimento = ?, tipo = ?, tipo_consulta = ?, data_consulta = ?, hora_consulta = ?, obs = ?, crianca_id = ?, acomp_id = ?, preparativos = ?, profissional_id = ?, unidade = ?, endereco = ?, numero = ?, bairro = ?, diagnostico = ?, responsavel_id = ? WHERE id = ?");
            $sql->bindValue(1, $tipo_atendimento);
            $sql->bindValue(2, $tipo);
            $sql->bindValue(3, $tipo_consulta);
            $sql->bindValue(4, $data_consulta);
            $sql->bindValue(5, $hora_consulta);
            $sql->bindValue(6, $observacao);
            $sql->bindValue(7, $nome_crianca);
            $sql->bindValue(8, $nome_acompanhante);
            $sql->bindValue(9, $preparativos);
            $sql->bindValue(10, $nome_profissional);
            $sql->bindValue(11, $unidade_saude);
            $sql->bindValue(12, $endereco_unidade_saude);
            $sql->bindValue(13, $numero_unidade_saude);
            $sql->bindValue(14, $bairro_unidade_saude);
            $sql->bindValue(15, $opniao_medica);
            $sql->bindValue(16, $_SESSION['id']);
            $sql->bindValue(17, $id);
            $sql->execute();

            //Apagandno os Exames
            $sql2 = $db->prepare("DELETE FROM mod_saude_consulta_exame WHERE consulta_id = ?");
            $sql2->bindValue(1, $id);
            $sql2->execute();
            //Apagando as Vacinas 
            $sql3 = $db->prepare("DELETE FROM mod_saude_consulta_vacina WHERE consulta_id = ?");
            $sql3->bindValue(1, $id);
            $sql3->execute();
        }

        //EXAMES
        $result4 = $db->prepare("SELECT *  
                                 FROM mod_saude_exames   
                                 WHERE status = 1
                                 ORDER BY id ASC");
        $result4->execute();
        while ($exames = $result4->fetch(PDO::FETCH_ASSOC)) {
            if (isset($_POST['exame_' . $exames['id']]) && $_POST['exame_' . $exames['id']] == 1) {
                $sql = $db->prepare("INSERT INTO mod_saude_consulta_exame (consulta_id, exame_id) VALUES (?, ?)");
                $sql->bindValue(1, $id);
                $sql->bindValue(2, $exames['id']);
                $sql->execute();
            }
        }
        //VACINAS
        $result5 = $db->prepare("SELECT *  
                                  FROM mod_saude_vacinas   
                                WHERE status = 1
                                 ORDER BY id ASC");
        $result5->execute();
        while ($vacinas = $result5->fetch(PDO::FETCH_ASSOC)) {
            if (isset($_POST['vacina_' . $vacinas['id']]) && $_POST['vacina_' . $vacinas['id']] == 1) {
                $sql = $db->prepare("INSERT INTO mod_saude_consulta_vacina (consulta_id, vacina_id) VALUES (?, ?)");
                $sql->bindValue(1, $id);
                $sql->bindValue(2, $vacinas['id']);
                $sql->execute();
            }
        }

        $db->commit();

        $msg['id'] = $id;
        $msg['msg'] = 'success';
        $msg['retorno'] = 'Consulta marcada com sucesso!';
        echo json_encode($msg);
        exit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>