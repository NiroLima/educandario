<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$unidade_saude = isset($_POST['unidade_saude']) ? $_POST['unidade_saude'] : NULL;
$contato_unidade_saude = isset($_POST['contato_unidade_saude']) ? $_POST['contato_unidade_saude'] : NULL;
$endereco_unidade_saude = isset($_POST['endereco_unidade_saude']) ? $_POST['endereco_unidade_saude'] : NULL;
$numero_unidade_saude = isset($_POST['numero_unidade_saude']) ? $_POST['numero_unidade_saude'] : NULL;
$bairro_unidade_saude = isset($_POST['bairro_unidade_saude']) ? $_POST['bairro_unidade_saude'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "mod_unidade_saude", "nome", "=", $unidade_saude, "");
if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome do local informada informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_unidade_saude (nome, contato, endereco, bairro, numero, data_update, responsavel_id, data_cadastro, status) VALUES (?, ?, ?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $unidade_saude);
            $sql->bindValue(2, $contato_unidade_saude);
            $sql->bindValue(3, $endereco_unidade_saude);
            $sql->bindValue(4, $bairro_unidade_saude);
            $sql->bindValue(5, $numero_unidade_saude);
            $sql->bindValue(6, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE mod_unidade_saude SET nome = ?, contato = ?, endereco = ?, bairro = ?, numero = ?, responsavel_id = ? WHERE id = ?");
            $sql->bindValue(1, $unidade_saude);
            $sql->bindValue(2, $contato_unidade_saude);
            $sql->bindValue(3, $endereco_unidade_saude);
            $sql->bindValue(4, $bairro_unidade_saude);
            $sql->bindValue(5, $numero_unidade_saude);
            $sql->bindValue(6, $_SESSION['id']);
            $sql->bindValue(7, $id);
            $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>