<?php

header("Cache-Control: max-age=300, must-revalidate"); // DEFINIR TIMEZONE PADRÃO
date_default_timezone_set("Brazil/Acre");
// OCULTAR OS WARNING DO PHP
// error_reporting(E_ALL ^ E_WARNING);
// ini_set("display_errors", 0 );
// DEFININDO OS DADOS DE ACESSO AO BANCO DE DADOS
 define("DB", 'mysql');
 define("DB_HOST", "localhost");
 define("DB_NAME", "educandario");
 define("DB_USER", "root");
 define("DB_PASS", "");

// CONFIGURACOES PADRAO DO SISTEMA
define("PORTAL_URL", 'http://localhost/educandario/');
define("TITULOSISTEMA", 'EDUCANDÁRIO');
define("LOGO_FOLDER", 'http://localhost/educandario/assets/images/logo_dark.png');
define("CSS_FOLDER", 'http://localhost/educandario/assets/css/');
define("IMG_FOLDER", 'http://localhost/educandario/assets/img/');
define("JS_FOLDER", 'http://localhost/educandario/assets/js/');
define("FONTS_FOLDER", 'http://localhost/educandario/assets/fontes/');
define("PLUGINS_FOLDER", 'http://localhost/educandario/assets/plugins/');
define("UTILS_FOLDER", 'http://localhost/educandario/utils/');
define("ASSETS_FOLDER", 'http://localhost/educandario/assets/');
define("PORTAL_URL_SERVIDOR", 'http://localhost/educandario/');

// ADICIONAR CLASSE DE CONEÇÃO
include_once ("Conexao.class.php");
?>