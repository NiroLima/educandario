//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('#form_responsavel').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var responsavel_id = $("#responsavel_id").val();

        $.post(PORTAL_URL + "admin/dao/pia/acolhido_responsavel.php", {id: id, responsavel_id: responsavel_id}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário PIA",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário PIA",
                    text: "Informações do acolhido atualizadas com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/plano/' + id);
                });
            }
        }
        , "html");
        return false;
    });

    $("#inserir_crianca").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca").val();
        var descreva_acao = $("#descreva_acao_crianca").val();
        var responsavel = $("#responsavel_crianca").val();
        var prazo_inicial = $("#prazo_inicial_crianca").val();
        var prazo_final = $("#prazo_final_crianca").val();
        var descreva_observacao = $("#descreva_observacao_crianca").val();

        var acao_realizada = $("#acao_realizada_crianca:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada_crianca:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada_crianca:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo_crianca:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id_crianca").val();

        var opcao = 1;

        if (formulario_validator_crianca("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_crianca.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id_crianca").val("");
                                    $("button#inserir_crianca").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento_crianca").val("");
                                    $("#descreva_acao_crianca").val("");
                                    $("#responsavel_crianca").val("");
                                    $("#prazo_inicial_crianca").val("");
                                    $("#prazo_final_crianca").val("");
                                    $("#descreva_observacao_crianca").val("");
                                    $("#acao_realizada_crianca").prop('checked', false);
                                    $("#objetivo_alcancada_crianca").prop('checked', false);
                                    $("#finalizada_crianca").prop('checked', false);
                                    $("#redefinir_prazo_crianca").prop('checked', false);
                                    $("div#resultado_documentacao_crianca").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_crianca2").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca2").val();
        var descreva_acao = $("#descreva_acao_crianca2").val();
        var responsavel = $("#responsavel_crianca2").val();
        var prazo_inicial = $("#prazo_inicial_crianca2").val();
        var prazo_final = $("#prazo_final_crianca2").val();
        var descreva_observacao = $("#descreva_observacao_crianca2").val();

        var acao_realizada = $("#acao_realizada_crianca2:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada_crianca2:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada_crianca2:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo_crianca2:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id_crianca2").val();

        var opcao = 2;

        if (formulario_validator_crianca(2)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_crianca.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id_crianca2").val("");
                                    $("button#inserir_crianca2").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento_crianca2").val("");
                                    $("#descreva_acao_crianca2").val("");
                                    $("#responsavel_crianca2").val("");
                                    $("#prazo_inicial_crianca2").val("");
                                    $("#prazo_final_crianca2").val("");
                                    $("#descreva_observacao_crianca2").val("");
                                    $("#acao_realizada_crianca2").prop('checked', false);
                                    $("#objetivo_alcancada_crianca2").prop('checked', false);
                                    $("#finalizada_crianca2").prop('checked', false);
                                    $("#redefinir_prazo_crianca2").prop('checked', false);
                                    $("div#resultado_documentacao_crianca2").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_crianca3").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca3").val();
        var descreva_acao = $("#descreva_acao_crianca3").val();
        var responsavel = $("#responsavel_crianca3").val();
        var prazo_inicial = $("#prazo_inicial_crianca3").val();
        var prazo_final = $("#prazo_final_crianca3").val();
        var descreva_observacao = $("#descreva_observacao_crianca3").val();

        var acao_realizada = $("#acao_realizada_crianca3:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada_crianca3:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada_crianca3:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo_crianca3:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id_crianca3").val();

        var opcao = 3;

        if (formulario_validator_crianca(3)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_crianca.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id_crianca3").val("");
                                    $("button#inserir_crianca3").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento_crianca3").val("");
                                    $("#descreva_acao_crianca3").val("");
                                    $("#responsavel_crianca3").val("");
                                    $("#prazo_inicial_crianca3").val("");
                                    $("#prazo_final_crianca3").val("");
                                    $("#descreva_observacao_crianca3").val("");
                                    $("#acao_realizada_crianca3").prop('checked', false);
                                    $("#objetivo_alcancada_crianca3").prop('checked', false);
                                    $("#finalizada_crianca3").prop('checked', false);
                                    $("#redefinir_prazo_crianca3").prop('checked', false);
                                    $("div#resultado_documentacao_crianca3").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_crianca4").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca4").val();
        var descreva_acao = $("#descreva_acao_crianca4").val();
        var responsavel = $("#responsavel_crianca4").val();
        var prazo_inicial = $("#prazo_inicial_crianca4").val();
        var prazo_final = $("#prazo_final_crianca4").val();
        var descreva_observacao = $("#descreva_observacao_crianca4").val();

        var acao_realizada = $("#acao_realizada_crianca4:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada_crianca4:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada_crianca4:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo_crianca4:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id_crianca4").val();

        var opcao = 4;

        if (formulario_validator_crianca(4)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_crianca.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id_crianca4").val("");
                                    $("button#inserir_crianca4").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento_crianca4").val("");
                                    $("#descreva_acao_crianca4").val("");
                                    $("#responsavel_crianca4").val("");
                                    $("#prazo_inicial_crianca4").val("");
                                    $("#prazo_final_crianca4").val("");
                                    $("#descreva_observacao_crianca4").val("");
                                    $("#acao_realizada_crianca4").prop('checked', false);
                                    $("#objetivo_alcancada_crianca4").prop('checked', false);
                                    $("#finalizada_crianca4").prop('checked', false);
                                    $("#redefinir_prazo_crianca4").prop('checked', false);
                                    $("div#resultado_documentacao_crianca4").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_crianca7").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca7").val();
        var descreva_acao = $("#descreva_acao_crianca7").val();
        var responsavel = $("#responsavel_crianca7").val();
        var prazo_inicial = $("#prazo_inicial_crianca7").val();
        var prazo_final = $("#prazo_final_crianca7").val();
        var descreva_observacao = $("#descreva_observacao_crianca7").val();

        var acao_realizada = $("#acao_realizada_crianca7:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada_crianca7:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada_crianca7:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo_crianca7:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id_crianca7").val();

        var opcao = 7;

        if (formulario_validator_crianca(7)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_crianca.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id_crianca7").val("");
                                    $("button#inserir_crianca7").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento_crianca7").val("");
                                    $("#descreva_acao_crianca7").val("");
                                    $("#responsavel_crianca7").val("");
                                    $("#prazo_inicial_crianca7").val("");
                                    $("#prazo_final_crianca7").val("");
                                    $("#descreva_observacao_crianca7").val("");
                                    $("#acao_realizada_crianca7").prop('checked', false);
                                    $("#objetivo_alcancada_crianca7").prop('checked', false);
                                    $("#finalizada_crianca7").prop('checked', false);
                                    $("#redefinir_prazo_crianca7").prop('checked', false);
                                    $("div#resultado_documentacao_crianca7").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_crianca8").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca8").val();
        var descreva_acao = $("#descreva_acao_crianca8").val();
        var responsavel = $("#responsavel_crianca8").val();
        var prazo_inicial = $("#prazo_inicial_crianca8").val();
        var prazo_final = $("#prazo_final_crianca8").val();
        var descreva_observacao = $("#descreva_observacao_crianca8").val();

        var acao_realizada = $("#acao_realizada_crianca8:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada_crianca8:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada_crianca8:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo_crianca8:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id_crianca8").val();

        var opcao = 8;

        if (formulario_validator_crianca(8)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_crianca.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id_crianca8").val("");
                                    $("button#inserir_crianca8").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento_crianca8").val("");
                                    $("#descreva_acao_crianca8").val("");
                                    $("#responsavel_crianca8").val("");
                                    $("#prazo_inicial_crianca8").val("");
                                    $("#prazo_final_crianca8").val("");
                                    $("#descreva_observacao_crianca8").val("");
                                    $("#acao_realizada_crianca8").prop('checked', false);
                                    $("#objetivo_alcancada_crianca8").prop('checked', false);
                                    $("#finalizada_crianca8").prop('checked', false);
                                    $("#redefinir_prazo_crianca8").prop('checked', false);
                                    $("div#resultado_documentacao_crianca8").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_crianca9").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca9").val();
        var descreva_acao = $("#descreva_acao_crianca9").val();
        var responsavel = $("#responsavel_crianca9").val();
        var prazo_inicial = $("#prazo_inicial_crianca9").val();
        var prazo_final = $("#prazo_final_crianca9").val();
        var descreva_observacao = $("#descreva_observacao_crianca9").val();

        var acao_realizada = $("#acao_realizada_crianca9:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada_crianca9:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada_crianca9:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo_crianca9:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id_crianca9").val();

        var opcao = 9;

        if (formulario_validator_crianca(9)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_crianca.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id_crianca9").val("");
                                    $("button#inserir_crianca9").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento_crianca9").val("");
                                    $("#descreva_acao_crianca9").val("");
                                    $("#responsavel_crianca9").val("");
                                    $("#prazo_inicial_crianca9").val("");
                                    $("#prazo_final_crianca9").val("");
                                    $("#descreva_observacao_crianca9").val("");
                                    $("#acao_realizada_crianca9").prop('checked', false);
                                    $("#objetivo_alcancada_crianca9").prop('checked', false);
                                    $("#finalizada_crianca9").prop('checked', false);
                                    $("#redefinir_prazo_crianca9").prop('checked', false);
                                    $("div#resultado_documentacao_crianca9").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_crianca10").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca10").val();
        var descreva_acao = $("#descreva_acao_crianca10").val();
        var responsavel = $("#responsavel_crianca10").val();
        var prazo_inicial = $("#prazo_inicial_crianca10").val();
        var prazo_final = $("#prazo_final_crianca10").val();
        var descreva_observacao = $("#descreva_observacao_crianca10").val();

        var acao_realizada = $("#acao_realizada_crianca10:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada_crianca10:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada_crianca10:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo_crianca10:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id_crianca10").val();

        var opcao = 10;

        if (formulario_validator_crianca(10)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_crianca.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id_crianca10").val("");
                                    $("button#inserir_crianca10").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento_crianca10").val("");
                                    $("#descreva_acao_crianca10").val("");
                                    $("#responsavel_crianca10").val("");
                                    $("#prazo_inicial_crianca10").val("");
                                    $("#prazo_final_crianca10").val("");
                                    $("#descreva_observacao_crianca10").val("");
                                    $("#acao_realizada_crianca10").prop('checked', false);
                                    $("#objetivo_alcancada_crianca10").prop('checked', false);
                                    $("#finalizada_crianca10").prop('checked', false);
                                    $("#redefinir_prazo_crianca10").prop('checked', false);
                                    $("div#resultado_documentacao_crianca10").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_documentacao").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento").val();
        var descreva_acao = $("#descreva_acao").val();
        var responsavel = $("#responsavel").val();
        var prazo_inicial = $("#prazo_inicial").val();
        var prazo_final = $("#prazo_final").val();
        var descreva_observacao = $("#descreva_observacao").val();

        var acao_realizada = $("#acao_realizada:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id").val();

        var opcao = 1;//Documentação

        if (formulario_validator("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_descricao.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_descricao.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id").val("");
                                    $("button#inserir_documentacao").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento").val("");
                                    $("#descreva_acao").val("");
                                    $("#responsavel").val("");
                                    $("#prazo_inicial").val("");
                                    $("#prazo_final").val("");
                                    $("#descreva_observacao").val("");
                                    $("#acao_realizada").prop('checked', false);
                                    $("#objetivo_alcancada").prop('checked', false);
                                    $("#finalizada").prop('checked', false);
                                    $("#redefinir_prazo").prop('checked', false);
                                    $("div#resultado_documentacao").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_documentacao2").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento2").val();
        var descreva_acao = $("#descreva_acao2").val();
        var responsavel = $("#responsavel2").val();
        var prazo_inicial = $("#prazo_inicial2").val();
        var prazo_final = $("#prazo_final2").val();
        var descreva_observacao = $("#descreva_observacao2").val();

        var acao_realizada = $("#acao_realizada2:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada2:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada2:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo2:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id2").val();

        var opcao = 2;//Saúde

        if (formulario_validator(2)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_descricao.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_descricao.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id2").val("");
                                    $("button#inserir_documentacao2").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento2").val("");
                                    $("#descreva_acao2").val("");
                                    $("#responsavel2").val("");
                                    $("#prazo_inicial2").val("");
                                    $("#prazo_final2").val("");
                                    $("#descreva_observacao2").val("");
                                    $("#acao_realizada2").prop('checked', false);
                                    $("#objetivo_alcancada2").prop('checked', false);
                                    $("#finalizada2").prop('checked', false);
                                    $("#redefinir_prazo2").prop('checked', false);
                                    $("div#resultado_documentacao2").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });


    $("#inserir_documentacao3").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento3").val();
        var descreva_acao = $("#descreva_acao3").val();
        var responsavel = $("#responsavel3").val();
        var prazo_inicial = $("#prazo_inicial3").val();
        var prazo_final = $("#prazo_final3").val();
        var descreva_observacao = $("#descreva_observacao3").val();

        var acao_realizada = $("#acao_realizada3:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada3:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada3:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo3:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id3").val();

        var opcao = 3;//Saúde

        if (formulario_validator(3)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_descricao.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_descricao.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id3").val("");
                                    $("button#inserir_documentacao3").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento3").val("");
                                    $("#descreva_acao3").val("");
                                    $("#responsavel3").val("");
                                    $("#prazo_inicial3").val("");
                                    $("#prazo_final3").val("");
                                    $("#descreva_observacao3").val("");
                                    $("#acao_realizada3").prop('checked', false);
                                    $("#objetivo_alcancada3").prop('checked', false);
                                    $("#finalizada3").prop('checked', false);
                                    $("#redefinir_prazo3").prop('checked', false);
                                    $("div#resultado_documentacao3").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });


    $("#inserir_documentacao4").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento4").val();
        var descreva_acao = $("#descreva_acao4").val();
        var responsavel = $("#responsavel4").val();
        var prazo_inicial = $("#prazo_inicial4").val();
        var prazo_final = $("#prazo_final4").val();
        var descreva_observacao = $("#descreva_observacao4").val();

        var acao_realizada = $("#acao_realizada4:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada4:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada4:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo4:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id4").val();

        var opcao = 4;//Saúde

        if (formulario_validator(4)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_descricao.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_descricao.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id4").val("");
                                    $("button#inserir_documentacao4").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento4").val("");
                                    $("#descreva_acao4").val("");
                                    $("#responsavel4").val("");
                                    $("#prazo_inicial4").val("");
                                    $("#prazo_final4").val("");
                                    $("#descreva_observacao4").val("");
                                    $("#acao_realizada4").prop('checked', false);
                                    $("#objetivo_alcancada4").prop('checked', false);
                                    $("#finalizada4").prop('checked', false);
                                    $("#redefinir_prazo4").prop('checked', false);
                                    $("div#resultado_documentacao4").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_documentacao5").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento5").val();
        var descreva_acao = $("#descreva_acao5").val();
        var responsavel = $("#responsavel5").val();
        var prazo_inicial = $("#prazo_inicial5").val();
        var prazo_final = $("#prazo_final5").val();
        var descreva_observacao = $("#descreva_observacao5").val();

        var acao_realizada = $("#acao_realizada5:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada5:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada5:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo5:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id5").val();

        var opcao = 5;//Saúde

        if (formulario_validator(5)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_descricao.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_descricao.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id5").val("");
                                    $("button#inserir_documentacao5").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento5").val("");
                                    $("#descreva_acao5").val("");
                                    $("#responsavel5").val("");
                                    $("#prazo_inicial5").val("");
                                    $("#prazo_final5").val("");
                                    $("#descreva_observacao5").val("");
                                    $("#acao_realizada5").prop('checked', false);
                                    $("#objetivo_alcancada5").prop('checked', false);
                                    $("#finalizada5").prop('checked', false);
                                    $("#redefinir_prazo5").prop('checked', false);
                                    $("div#resultado_documentacao5").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_documentacao6").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento6").val();
        var descreva_acao = $("#descreva_acao6").val();
        var responsavel = $("#responsavel6").val();
        var prazo_inicial = $("#prazo_inicial6").val();
        var prazo_final = $("#prazo_final6").val();
        var descreva_observacao = $("#descreva_observacao6").val();

        var acao_realizada = $("#acao_realizada6:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada6:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada6:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo6:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id6").val();

        var opcao = 6;//Saúde

        if (formulario_validator(6)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_descricao.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_descricao.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id6").val("");
                                    $("button#inserir_documentacao6").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento6").val("");
                                    $("#descreva_acao6").val("");
                                    $("#responsavel6").val("");
                                    $("#prazo_inicial6").val("");
                                    $("#prazo_final6").val("");
                                    $("#descreva_observacao6").val("");
                                    $("#acao_realizada6").prop('checked', false);
                                    $("#objetivo_alcancada6").prop('checked', false);
                                    $("#finalizada6").prop('checked', false);
                                    $("#redefinir_prazo6").prop('checked', false);
                                    $("div#resultado_documentacao6").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_documentacao7").click(function () {

        var id = $("#plano_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento7").val();
        var descreva_acao = $("#descreva_acao7").val();
        var responsavel = $("#responsavel7").val();
        var prazo_inicial = $("#prazo_inicial7").val();
        var prazo_final = $("#prazo_final7").val();
        var descreva_observacao = $("#descreva_observacao7").val();

        var acao_realizada = $("#acao_realizada7:checked").val() == 1 ? 1 : 0;
        var objetivo_alcancada = $("#objetivo_alcancada7:checked").val() == 1 ? 1 : 0;
        var finalizada = $("#finalizada7:checked").val() == 1 ? 1 : 0;
        var redefinir_prazo = $("#redefinir_prazo7:checked").val() == 1 ? 1 : 0;

        var documentacao_id = $("#documentacao_id7").val();

        var opcao = 7;//Saúde

        if (formulario_validator(7)) {
            $.post(PORTAL_URL + "admin/dao/pia/add_descricao.php", {id: id, opcao: opcao, acolhimento_crianca_id: acolhimento_crianca_id, descreva_objetivo_monitoramento: descreva_objetivo_monitoramento, descreva_acao: descreva_acao,
                responsavel: responsavel, prazo_inicial: prazo_inicial, prazo_final: prazo_final, descreva_observacao: descreva_observacao, documentacao_id: documentacao_id,
                acao_realizada: acao_realizada, objetivo_alcancada: objetivo_alcancada, finalizada: finalizada, redefinir_prazo: redefinir_prazo}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Plano de Ação",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Plano de Ação",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_descricao.php",
                                {id: acolhimento_crianca_id, opcao: opcao},
                                function (valor) {
                                    $("div#div_validacao").show();
                                    $("input#documentacao_id7").val("");
                                    $("button#inserir_documentacao7").html("INSERIR");
                                    $("#descreva_objetivo_monitoramento7").val("");
                                    $("#descreva_acao7").val("");
                                    $("#responsavel7").val("");
                                    $("#prazo_inicial7").val("");
                                    $("#prazo_final7").val("");
                                    $("#descreva_observacao7").val("");
                                    $("#acao_realizada7").prop('checked', false);
                                    $("#objetivo_alcancada7").prop('checked', false);
                                    $("#finalizada7").prop('checked', false);
                                    $("#redefinir_prazo7").prop('checked', false);
                                    $("div#resultado_documentacao7").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
function editar(opcao, id, objetivo, acao, responsavel, prazo_inicial, prazo_final, realizada, alcancada, finalizada, prazo, obs) {

    if (opcao == 1) {
        opcao = "";
    }

    $("input#documentacao_id" + opcao).val(id);
    $("button#inserir_documentacao" + opcao).html("ALTERAR");
    $("#descreva_objetivo_monitoramento" + opcao).val(objetivo);
    $("#descreva_acao" + opcao).val(acao);
    $("#responsavel" + opcao).val(responsavel);
    $("#prazo_inicial" + opcao).val(prazo_inicial);
    $("#prazo_final" + opcao).val(prazo_final);
    $("#descreva_observacao" + opcao).val(obs);

    $("#acao_realizada" + opcao).prop('checked', (realizada == 1 ? true : false));
    $("#objetivo_alcancada" + opcao).prop('checked', (alcancada == 1 ? true : false));
    $("#finalizada" + opcao).prop('checked', (finalizada == 1 ? true : false));
    $("#redefinir_prazo" + opcao).prop('checked', (prazo == 1 ? true : false));
}
//------------------------------------------------------------------------------
function remover(opcao, obj, id) {

    if (opcao == 1) {
        opcao = "";
    }

    swal({
        title: "Deseja mesmo remover?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_plano",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend(obj, opcao),
            error: onSuccessSend(obj, opcao)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj, opcao) {

    var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();

    swal({
        title: "Sucesso!",
        text: "Removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#documentacao_id" + opcao).val("");
        $("button#inserir_documentacao" + opcao).html("INSERIR");
        $("#descreva_objetivo_monitoramento" + opcao).val("");
        $("#descreva_acao" + opcao).val("");
        $("#responsavel" + opcao).val("");
        $("#prazo_inicial" + opcao).val("");
        $("#prazo_final" + opcao).val("");
        $("#descreva_observacao" + opcao).val("");
        $("#acao_realizada" + opcao).prop('checked', false);
        $("#objetivo_alcancada" + opcao).prop('checked', false);
        $("#finalizada" + opcao).prop('checked', false);
        $("#redefinir_prazo" + opcao).prop('checked', false);

        if (opcao == "") {
            opcao = 1;
        }

        $.post(PORTAL_URL + "admin/dao/pia/carregar_descricao.php",
                {id: acolhimento_crianca_id, opcao: opcao},
                function (valor) {

                    if (opcao == 1) {
                        opcao = "";
                    }

                    $("div#resultado_documentacao" + opcao).html(valor);
                });

    });

    return false;
}
//------------------------------------------------------------------------------
function editar2(opcao, id, objetivo, acao, responsavel, prazo_inicial, prazo_final, realizada, alcancada, finalizada, prazo, obs) {

    if (opcao == 1) {
        opcao = "";
    }

    $("input#documentacao_id_crianca" + opcao).val(id);
    $("button#inserir_crianca" + opcao).html("ALTERAR");
    $("#descreva_objetivo_monitoramento_crianca" + opcao).val(objetivo);
    $("#descreva_acao_crianca" + opcao).val(acao);
    $("#responsavel_crianca" + opcao).val(responsavel);
    $("#prazo_inicial_crianca" + opcao).val(prazo_inicial);
    $("#prazo_final_crianca" + opcao).val(prazo_final);
    $("#descreva_observacao_crianca" + opcao).val(obs);

    $("#acao_realizada_crianca" + opcao).prop('checked', (realizada == 1 ? true : false));
    $("#objetivo_alcancada_crianca" + opcao).prop('checked', (alcancada == 1 ? true : false));
    $("#finalizada_crianca" + opcao).prop('checked', (finalizada == 1 ? true : false));
    $("#redefinir_prazo_crianca" + opcao).prop('checked', (prazo == 1 ? true : false));
}
//------------------------------------------------------------------------------
function remover2(opcao, obj, id) {

    if (opcao == 1) {
        opcao = "";
    }

    swal({
        title: "Deseja mesmo remover?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_plano",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend2(obj, opcao),
            error: onSuccessSend2(obj, opcao)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend2(obj, opcao) {

    var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();

    swal({
        title: "Sucesso!",
        text: "Removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#documentacao_id_crianca" + opcao).val("");
        $("button#inserir_crianca" + opcao).html("INSERIR");
        $("#descreva_objetivo_monitoramento_crianca" + opcao).val("");
        $("#descreva_acao_crianca" + opcao).val("");
        $("#responsavel_crianca" + opcao).val("");
        $("#prazo_inicial_crianca" + opcao).val("");
        $("#prazo_final_crianca" + opcao).val("");
        $("#descreva_observacao_crianca" + opcao).val("");
        $("#acao_realizada_crianca" + opcao).prop('checked', false);
        $("#objetivo_alcancada_crianca" + opcao).prop('checked', false);
        $("#finalizada_crianca" + opcao).prop('checked', false);
        $("#redefinir_prazo_crianca" + opcao).prop('checked', false);

        if (opcao == "") {
            opcao = 1;
        }

        $.post(PORTAL_URL + "admin/dao/pia/carregar_crianca.php",
                {id: acolhimento_crianca_id, opcao: opcao},
                function (valor) {

                    if (opcao == 1) {
                        opcao = "";
                    }

                    $("div#resultado_documentacao_crianca" + opcao).html(valor);
                });

    });

    return false;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(numero) {
    var valido = true;

    var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento" + numero).val();
    var descreva_acao = $("#descreva_acao" + numero).val();
    var responsavel = $("#responsavel" + numero).val();
    var prazo_inicial = $("#prazo_inicial" + numero).val();
    var prazo_final = $("#prazo_final" + numero).val();
    var descreva_observacao = $("#descreva_observacao" + numero).val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (descreva_observacao == "") {
        $('div#div_descreva_observacao' + numero).after('<label id="erro_descreva_observacao' + numero + '" class="error">Observação é obrigatório.</label>');
        valido = false;
        element = $('div#div_descreva_observacao' + numero);
    }

    if (prazo_final == "") {
        $('div#div_prazo_final' + numero).after('<label id="erro_prazo_final' + numero + '" class="error">Prazo final é obrigatório.</label>');
        valido = false;
        element = $('div#div_prazo_final' + numero);
    }

    if (prazo_inicial == "") {
        $('div#div_prazo_inicial' + numero).after('<label id="erro_prazo_inicial' + numero + '" class="error">Prazo inicial é obrigatório.</label>');
        valido = false;
        element = $('div#div_prazo_inicial' + numero);
    }

    if (responsavel == "") {
        $('div#div_responsavel' + numero).after('<label id="erro_responsavel' + numero + '" class="error">Responsável é obrigatório.</label>');
        valido = false;
        element = $('div#div_responsavel' + numero);
    }

    if (descreva_acao == "") {
        $('div#div_descreva_acao' + numero).after('<label id="erro_descreva_acao' + numero + '" class="error">Ação é obrigatório.</label>');
        valido = false;
        element = $('div#div_descreva_acao' + numero);
    }

    if (descreva_objetivo_monitoramento == "") {
        $('div#div_descreva_objetivo_monitoramento' + numero).after('<label id="erro_descreva_objetivo_monitoramento' + numero + '" class="error">Objetivo é obrigatório.</label>');
        valido = false;
        element = $('div#div_descreva_objetivo_monitoramento' + numero);
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_crianca(numero) {
    var valido = true;

    var descreva_objetivo_monitoramento = $("#descreva_objetivo_monitoramento_crianca" + numero).val();
    var descreva_acao = $("#descreva_acao_crianca" + numero).val();
    var responsavel = $("#responsavel_crianca" + numero).val();
    var prazo_inicial = $("#prazo_inicial_crianca" + numero).val();
    var prazo_final = $("#prazo_final_crianca" + numero).val();
    var descreva_observacao = $("#descreva_observacao_crianca" + numero).val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (descreva_observacao == "") {
        $('div#div_descreva_observacao_crianca' + numero).after('<label id="erro_descreva_observacao_crianca' + numero + '" class="error">Observação é obrigatório.</label>');
        valido = false;
        element = $('div#div_descreva_observacao_crianca' + numero);
    }

    if (prazo_final == "") {
        $('div#div_prazo_final_crianca' + numero).after('<label id="erro_prazo_final_crianca' + numero + '" class="error">Prazo final é obrigatório.</label>');
        valido = false;
        element = $('div#div_prazo_final_crianca' + numero);
    }

    if (prazo_inicial == "") {
        $('div#div_prazo_inicial_crianca' + numero).after('<label id="erro_prazo_inicial_crianca' + numero + '" class="error">Prazo inicial é obrigatório.</label>');
        valido = false;
        element = $('div#div_prazo_inicial_crianca' + numero);
    }

    if (responsavel == "") {
        $('div#div_responsavel_crianca' + numero).after('<label id="erro_responsavel_crianca' + numero + '" class="error">Responsável é obrigatório.</label>');
        valido = false;
        element = $('div#div_responsavel_crianca' + numero);
    }

    if (descreva_acao == "") {
        $('div#div_descreva_acao_crianca' + numero).after('<label id="erro_descreva_acao_crianca' + numero + '" class="error">Ação é obrigatório.</label>');
        valido = false;
        element = $('div#div_descreva_acao_crianca' + numero);
    }

    if (descreva_objetivo_monitoramento == "") {
        $('div#div_descreva_objetivo_monitoramento_crianca' + numero).after('<label id="erro_descreva_objetivo_monitoramento_crianca' + numero + '" class="error">Objetivo é obrigatório.</label>');
        valido = false;
        element = $('div#div_descreva_objetivo_monitoramento_crianca' + numero);
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function validar(codigo) {
    swal({
        title: "Deseja mesmo confirmar a Etapa 7 Plano de Ação?",
        text: "Obs: Caso escolha confirmar, o sistema irá identificar essa etapa como concluída!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode confirmar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/confirmar_acolhido",
            type: "POST",
            data: {id: codigo, op: 7},
            enctype: 'multipart/form-data',
            success: onSuccessSendV,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------------------------------
function desvalidar(codigo) {
    swal({
        title: "Deseja mesmo desvalidar a Etapa 7 Plano de Ação?",
        text: "Obs: Caso escolha desvalidar, o sistema irá identificar essa etapa como pendente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode desvalidar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/desvalidar_acolhido",
            type: "POST",
            data: {id: codigo, op: 7},
            enctype: 'multipart/form-data',
            success: onSuccessSendV,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSendV(obj) {

    var codigo = $("#id").val();

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/plano/' + codigo);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------------------------------
function editar_responsavel() {
    $("div#responsavel_edite_1").hide();
    $("div#responsavel_edite_2").show();
}
//------------------------------------------------------------------------------
function gerar_pdf() {
    //pega o Html da DIV
    var divElements = document.getElementById("impressao").innerHTML;
    //pega o HTML de toda tag Body
    var oldPage = document.body.innerHTML;
    //Alterna o body 
    document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
    //Imprime o body atual
    window.print();
    //Retorna o conteudo original da página. 
    document.body.innerHTML = oldPage;
}
//------------------------------------------------------------------------------

$('.open-menu').click(function() {
    if (document.querySelector('.menu-acesso-rapido.active') !== null) {
        $('.menu-acesso-rapido').removeClass('active');
        $('i.fal').addClass('fa-chevron-left');
        $('i.fal').removeClass('fa-chevron-right');
    } else{
        $('.menu-acesso-rapido').addClass('active');
        $('i.fal').removeClass('fa-chevron-left');
        $('i.fal').addClass('fa-chevron-right');
    }
});

$('.menu-acesso-rapido').perfectScrollbar();
