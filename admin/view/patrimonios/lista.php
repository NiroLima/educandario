<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Patrimônio</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                  
                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-barcode-read"></i> <strong>LISTA DE PATRIMÔNIO</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/patrimonios/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVO PATRIMÔNIO</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Patrimônio</th>
                                            <th>Código</th>
                                            <th>Setor</th>
                                            <th>Responsável</th>
                                            <th>Situação</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
									
									    <?php
                                        $result = $db->prepare("SELECT mp.id, mp.status, mp.nome, mp.codigo, mp.situacao, su.nome AS responsavel, ss.nome AS setor    
                                                                FROM mod_patrimonio mp 
                                                                LEFT JOIN seg_usuario AS su ON su.id = mp.responsavel_id  
																LEFT JOIN seg_setor AS ss ON ss.id = mp.setor_id 
                                                                WHERE 1
                                                                ORDER BY mp.nome");
                                        $result->execute();
                                        while ($patrimonio = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $patrimonio['nome']; ?></td>
                                                <td><?= $patrimonio['codigo']; ?></td>
                                                <td><?= $patrimonio['setor']; ?></td>
												<td><?= $patrimonio['responsavel']; ?></td>
                                                <td><span class="badge <?= $patrimonio['situacao'] == 1 ? "badge-success" : ($patrimonio['situacao'] == 2 ? "badge-danger" : ($patrimonio['situacao'] == 3 ? "badge-dark" : "")); ?>"><?= $patrimonio['situacao'] == 1 ? "EM UTILIZAÇÃO" : ($patrimonio['situacao'] == 2 ? "INUTILIZÁVEL" : ($patrimonio['situacao'] == 3 ? "BAIXADO" : "")); ?></span></td>
                                                <td class="text-center">

                                                    <?php
                                                    if (ver_nivel(1, "") || ver_nivel(3, "")) {
                                                        ?>
                                                        <a id="ativar" rel="<?= $patrimonio['id']; ?>" <?= $patrimonio['status'] == 0 ? "" : "style='display: none'"; ?> title="Desbloquear Patrimônio" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                        <a id="remover" rel="<?= $patrimonio['id']; ?>" <?= $patrimonio['status'] == 1 ? "" : "style='display: none'"; ?> title="Bloquear Patrimônio" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                        <?php
                                                    }
                                                    ?>

                                                    <a href="<?= PORTAL_URL ?>admin/view/patrimonios/novo/<?= $patrimonio['id']; ?>" title="Editar Patrimônio" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/patrimonios/lista.js"></script>
