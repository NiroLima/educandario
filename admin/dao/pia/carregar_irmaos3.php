<?php

//------------------------------------------------------------------------------
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$id = $_POST['id'];

$stmp = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                     FROM mod_acolhimento_crianca_irmaos maci  
                     LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                     WHERE maci.familia_id = ? AND maci.tipo = 3");
$stmp->bindValue(1, $id);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    
} else {
    while ($irmaos = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr id="remover_irmao3">
                     <td>' . $irmaos['irmao'] . '</td>
                     <td>' . $irmaos['idade'] . ' ANOS</td>
                     <td width="100px">
                        <a style="cursor: pointer" onclick="editar_irmao3(' . $irmaos['id'] . ', ' . "'" . $irmaos['irmao_id'] . "'" . ', ' . "'" . $irmaos['idade'] . "'" . ', ' . "'" . $irmaos['data_acolhimento'] . "'" . ', ' . "'" . $irmaos['obs'] . "'" . ')" class="text-warning"><i class="fa fa-pencil"></i></a>
                        <a style="cursor: pointer" onclick="remover_irmao3(this, ' . $irmaos['id'] . ')" class="text-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>';
    }
}
//------------------------------------------------------------------------------
?>

