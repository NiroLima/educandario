<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$setor = isset($_POST['setor']) ? $_POST['setor'] : NULL;
$responsavel = isset($_POST['responsavel']) ? $_POST['responsavel'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "seg_setor", "nome", "=", $setor, "");
if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome do setor informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO seg_setor (nome, data_update, responsavel, usuario_id, data_cadastro, status) VALUES (?, NOW(), ?, ?, NOW(), 1)");
            $sql->bindValue(1, $setor);
			$sql->bindValue(2, $responsavel);
            $sql->bindValue(3, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();

        } else {
                $sql = $db->prepare("UPDATE seg_setor SET nome = ?, responsavel = ?, usuario_id = ? WHERE id = ?");
                $sql->bindValue(1, $setor);
				$sql->bindValue(2, $responsavel);
                $sql->bindValue(3, $_SESSION['id']);
                $sql->bindValue(4, $id);
                $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>