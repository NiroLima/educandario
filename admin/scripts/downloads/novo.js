//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {
    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("#profissional").change(function () {
        var categoria = $(this).find("option:selected").attr("categoria");
        var especialidade = $(this).find("option:selected").attr("especialidade");

        $("#especialidade").val(especialidade);
        $("#tipo_categoria").val(categoria);

    });

});

$("#remover").click(function () {
    var codigo = $("#id").val();

    swal({
        title: "Deseja mesmo remover este arquivo?",
        text: "Obs: Caso escolha remover, não será mais possíve recuperar esse arquivo!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/downloads/remover",
            type: "POST",
            data: {codigo: codigo},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
});
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/downloads/novo/' + obj.id);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------