<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$avaliacao_id = isset($_POST['avaliacao_id']) && $_POST['avaliacao_id'] != "" ? $_POST['avaliacao_id'] : 0;

$descreva_contextualizacao_caso = isset($_POST['descreva_contextualizacao_caso']) && $_POST['descreva_contextualizacao_caso'] != "" ? $_POST['descreva_contextualizacao_caso'] : NULL;
$descreva_assumir = isset($_POST['descreva_assumir']) && $_POST['descreva_assumir'] != "" ? $_POST['descreva_assumir'] : NULL;
$sim_intervencao_tecnica = isset($_POST['sim_intervencao_tecnica']) && $_POST['sim_intervencao_tecnica'] != "" ? $_POST['sim_intervencao_tecnica'] : 0;
$modificacoes = isset($_POST['modificacoes']) && $_POST['modificacoes'] != "" ? $_POST['modificacoes'] : NULL;
$sim_reinsercao = isset($_POST['sim_reinsercao']) && $_POST['sim_reinsercao'] != "" ? $_POST['sim_reinsercao'] : 0;
$justificativa = isset($_POST['justificativa']) && $_POST['justificativa'] != "" ? $_POST['justificativa'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($avaliacao_id) && $avaliacao_id > 0) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_avaliacao SET contextualiziacao = ?, interesse = ?, intervencoes = ?, modificacoes  = ?, 
                                  indicativos = ?, justificacao = ?, responsavel_id = ? 
                                  WHERE id = ?");
            $sql4->bindValue(1, $descreva_contextualizacao_caso);
            $sql4->bindValue(2, $descreva_assumir);
            $sql4->bindValue(3, $sim_intervencao_tecnica);
            $sql4->bindValue(4, $modificacoes);
            $sql4->bindValue(5, $sim_reinsercao);
            $sql4->bindValue(6, $justificativa);
            $sql4->bindValue(7, $_SESSION['id']);
            $sql4->bindValue(8, $avaliacao_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_avaliacao (acolhimento_crianca_id, contextualiziacao, interesse, intervencoes, modificacoes, indicativos, justificacao, responsavel_id, data_cadastro, status)
                     VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1)");
            $sql4->bindValue(1, $id);
            $sql4->bindValue(2, $descreva_contextualizacao_caso);
            $sql4->bindValue(3, $descreva_assumir);
            $sql4->bindValue(4, $sim_intervencao_tecnica);
            $sql4->bindValue(5, $modificacoes);
            $sql4->bindValue(6, $sim_reinsercao);
            $sql4->bindValue(7, $justificativa);
            $sql4->bindValue(8, $_SESSION['id']);
            $sql4->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>