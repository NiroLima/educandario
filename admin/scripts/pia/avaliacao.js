//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("button#atualizar").click(function () {
        $("form#form_avaliacao").submit();
    });

    $("#sim_intervencao_tecnica").click(function () {
        $("#div_modificacoes").show();
    });

    $("#nao_intervencao_tecnica").click(function () {
        $("#modificacoes").val("");
        $("#div_modificacoes").hide();
    });

    $("#sim_reinsercao").click(function () {
        $("#div_justificativa").show();
    });

    $("#nao_reinsercao").click(function () {
        $("#justificativa").val("");
        $("#div_justificativa").hide();
    });

    $('#form_responsavel').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var responsavel_id = $("#responsavel_id").val();

        $.post(PORTAL_URL + "admin/dao/pia/acolhido_responsavel.php", {id: id, responsavel_id: responsavel_id}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário PIA",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário PIA",
                    text: "Informações do acolhido atualizadas com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/avaliacao/' + id);
                });
            }
        }
        , "html");
        return false;
    });

    $('#form_avaliacao').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var avaliacao_id = $("#avaliacao_id").val();
        var descreva_contextualizacao_caso = $("#descreva_contextualizacao_caso").val();
        var descreva_assumir = $("#descreva_assumir").val();
        var sim_intervencao_tecnica = $("#sim_intervencao_tecnica:checked").val() == 1 ? 1 : 0;
        var modificacoes = $("#modificacoes").val();
        var sim_reinsercao = $("#sim_reinsercao:checked").val() == 1 ? 1 : 0;
        var justificativa = $("#justificativa").val();

        $.post(PORTAL_URL + "admin/dao/pia/avaliacao.php", {id: id, avaliacao_id: avaliacao_id, descreva_contextualizacao_caso: descreva_contextualizacao_caso,
            descreva_assumir: descreva_assumir, sim_intervencao_tecnica: sim_intervencao_tecnica, modificacoes: modificacoes, sim_reinsercao: sim_reinsercao, justificativa: justificativa}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário de Avaliação",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário de Avaliação",
                    text: "Ação realizada com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/avaliacao/' + id);
                });
            }
        }
        , "html");
        return false;

    });

});
//------------------------------------------------------------------------------
function validar(codigo) {
    swal({
        title: "Deseja mesmo confirmar a Etapa 8 Avaliação?",
        text: "Obs: Caso escolha confirmar, o sistema irá identificar essa etapa como concluída!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode confirmar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/confirmar_acolhido",
            type: "POST",
            data: {id: codigo, op: 8},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------------------------------
function desvalidar(codigo) {
    swal({
        title: "Deseja mesmo desvalidar a Etapa 8 Avaliação?",
        text: "Obs: Caso escolha desvalidar, o sistema irá identificar essa etapa como pendente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode desvalidar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/desvalidar_acolhido",
            type: "POST",
            data: {id: codigo, op: 8},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    var codigo = $("#id").val();

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/avaliacao/' + codigo);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------------------------------
function editar_responsavel() {
    $("div#responsavel_edite_1").hide();
    $("div#responsavel_edite_2").show();
}
//------------------------------------------------------------------------------
function gerar_pdf() {
    //pega o Html da DIV
    var divElements = document.getElementById("impressao").innerHTML;
    //pega o HTML de toda tag Body
    var oldPage = document.body.innerHTML;
    //Alterna o body 
    document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
    //Imprime o body atual
    window.print();
    //Retorna o conteudo original da página. 
    document.body.innerHTML = oldPage;
}
//------------------------------------------------------------------------------