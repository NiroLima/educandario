<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$familia_id = isset($_POST['familia_id']) && $_POST['familia_id'] != "" ? $_POST['familia_id'] : 0;
$acolhimento_crianca_id = isset($_POST['acolhimento_crianca_id']) && $_POST['acolhimento_crianca_id'] != "" ? $_POST['acolhimento_crianca_id'] : 0;

$nome_mae = isset($_POST['nome_mae']) && $_POST['nome_mae'] != "" ? $_POST['nome_mae'] : NULL;
$nascimento_mae = isset($_POST['nascimento_mae']) && $_POST['nascimento_mae'] != "" ? $_POST['nascimento_mae'] : NULL;
$endereco_mae = isset($_POST['endereco_mae']) && $_POST['endereco_mae'] != "" ? $_POST['endereco_mae'] : NULL;
$numero_mae = isset($_POST['numero_mae']) && $_POST['numero_mae'] != "" ? $_POST['numero_mae'] : NULL;
$bairro_mae = isset($_POST['bairro_mae']) && $_POST['bairro_mae'] != "" ? $_POST['bairro_mae'] : NULL;
$rg_mae = isset($_POST['rg_mae']) && $_POST['rg_mae'] != "" ? $_POST['rg_mae'] : NULL;
$cpf_mae = isset($_POST['cpf_mae']) && $_POST['cpf_mae'] != "" ? $_POST['cpf_mae'] : NULL;
$celular_mae = isset($_POST['celular_mae']) && $_POST['celular_mae'] != "" ? $_POST['celular_mae'] : NULL;
$ocupacao_mae = isset($_POST['ocupacao_mae']) && $_POST['ocupacao_mae'] != "" ? $_POST['ocupacao_mae'] : NULL;

$mae_falecida = isset($_POST['mae_falecida']) && $_POST['mae_falecida'] != "" && $_POST['mae_falecida'] == 1 ? 0 : "";
$mae_reclusa = isset($_POST['mae_reclusa']) && $_POST['mae_reclusa'] != "" && $_POST['mae_reclusa'] == 1 ? 1 : "";
$mae_nao_encontrada = isset($_POST['mae_nao_encontrada']) && $_POST['mae_nao_encontrada'] != "" && $_POST['mae_nao_encontrada'] == 1 ? 2 : "";
$mae_desconhecida = isset($_POST['mae_desconhecida']) && $_POST['mae_desconhecida'] != "" && $_POST['mae_desconhecida'] == 1 ? 3 : "";
$sem_vinculo_mae = isset($_POST['sem_vinculo_mae']) && $_POST['sem_vinculo_mae'] != "" && $_POST['sem_vinculo_mae'] == 1 ? 4 : "";

$mae_situacao = $mae_falecida == "0" ? 0 : ($mae_reclusa == "1" ? 1 : ($mae_nao_encontrada == "2" ? 2 : ($mae_desconhecida == "3" ? 3 : ($sem_vinculo_mae == "4" ? 4 : NULL))));

$nome_pai = isset($_POST['nome_pai']) && $_POST['nome_pai'] != "" ? $_POST['nome_pai'] : NULL;
$nascimento_pai = isset($_POST['nascimento_pai']) && $_POST['nascimento_pai'] != "" ? $_POST['nascimento_pai'] : NULL;
$endereco_pai = isset($_POST['endereco_pai']) && $_POST['endereco_pai'] != "" ? $_POST['endereco_pai'] : NULL;
$numero_pai = isset($_POST['numero_pai']) && $_POST['numero_pai'] != "" ? $_POST['numero_pai'] : NULL;
$bairro_pai = isset($_POST['bairro_pai']) && $_POST['bairro_pai'] != "" ? $_POST['bairro_pai'] : NULL;
$rg_pai = isset($_POST['rg_pai']) && $_POST['rg_pai'] != "" ? $_POST['rg_pai'] : NULL;
$cpf_pai = isset($_POST['cpf_pai']) && $_POST['cpf_pai'] != "" ? $_POST['cpf_pai'] : NULL;
$celular_pai = isset($_POST['celular_pai']) && $_POST['celular_pai'] != "" ? $_POST['celular_pai'] : NULL;
$ocupacao_pai = isset($_POST['ocupacao_pai']) && $_POST['ocupacao_pai'] != "" ? $_POST['ocupacao_pai'] : NULL;

$pai_falecida = isset($_POST['pai_falecida']) && $_POST['pai_falecida'] != "" && $_POST['pai_falecida'] == 1 ? 0 : "";
$pai_reclusa = isset($_POST['pai_reclusa']) && $_POST['pai_reclusa'] != "" && $_POST['pai_reclusa'] == 1 ? 1 : "";
$pai_nao_encontrada = isset($_POST['pai_nao_encontrada']) && $_POST['pai_nao_encontrada'] != "" && $_POST['pai_nao_encontrada'] == 1 ? 2 : "";
$pai_desconhecida = isset($_POST['pai_desconhecida']) && $_POST['pai_desconhecida'] != "" && $_POST['pai_desconhecida'] == 1 ? 3 : "";
$sem_vinculo_pai = isset($_POST['sem_vinculo_pai']) && $_POST['sem_vinculo_pai'] != "" && $_POST['sem_vinculo_pai'] == 1 ? 4 : "";

$pai_situacao = $pai_falecida == "0" ? 0 : ($pai_reclusa == "1" ? 1 : ($pai_nao_encontrada == "2" ? 2 : ($pai_desconhecida == "3" ? 3 : ($sem_vinculo_pai == "4" ? 4 : NULL))));

$com_condicoes_convivio = isset($_POST['com_condicoes_convivio']) && $_POST['com_condicoes_convivio'] != "" && is_numeric($_POST['com_condicoes_convivio']) ? $_POST['com_condicoes_convivio'] : 0;
$descreva_avaliacao_profunda = isset($_POST['descreva_avaliacao_profunda']) && $_POST['descreva_avaliacao_profunda'] != "" ? $_POST['descreva_avaliacao_profunda'] : NULL;
$com_irmaos_servico_acolhimento = isset($_POST['com_irmaos_servico_acolhimento']) && $_POST['com_irmaos_servico_acolhimento'] != "" && $_POST['com_irmaos_servico_acolhimento'] == 1 ? 1 : 0;

$com_irmaos_outros_servico_acolhimento = isset($_POST['com_irmaos_outros_servico_acolhimento']) && $_POST['com_irmaos_outros_servico_acolhimento'] != "" && $_POST['com_irmaos_outros_servico_acolhimento'] == 1 ? 1 : 0;
$com_irmaos_em_servico_acolhimento = isset($_POST['com_irmaos_em_servico_acolhimento']) && $_POST['com_irmaos_em_servico_acolhimento'] != "" && $_POST['com_irmaos_em_servico_acolhimento'] == 1 ? 1 : 0;

$com_irmaos_adotados = isset($_POST['com_irmaos_adotados']) && $_POST['com_irmaos_adotados'] != "" && $_POST['com_irmaos_adotados'] == 1 ? 1 : 0;

$com_vinculo = isset($_POST['com_vinculo']) && $_POST['com_vinculo'] != "" && $_POST['com_vinculo'] == 1 ? 0 : "";
$sem_vinculo = isset($_POST['sem_vinculo']) && $_POST['sem_vinculo'] != "" && $_POST['sem_vinculo'] == 1 ? 1 : "";
$familia_desaparecida = isset($_POST['familia_desaparecida']) && $_POST['familia_desaparecida'] != "" && $_POST['familia_desaparecida'] == 1 ? 2 : "";
$orfao = isset($_POST['orfao']) && $_POST['orfao'] != "" && $_POST['orfao'] == 1 ? 3 : "";
$poder_familia = isset($_POST['poder_familia']) && $_POST['poder_familia'] != "" && $_POST['poder_familia'] == 1 ? 4 : "";
$impedimento_judicial = isset($_POST['impedimento_judicial']) && $_POST['impedimento_judicial'] != "" && $_POST['impedimento_judicial'] == 1 ? 5 : "";
$sem_informacao = isset($_POST['sem_informacao']) && $_POST['sem_informacao'] != "" && $_POST['sem_informacao'] == 1 ? 6 : "";
$outra_situacao = isset($_POST['outra_situacao']) && $_POST['outra_situacao'] != "" && $_POST['outra_situacao'] == 1 ? 7 : "";

$situacao_familiar = $com_vinculo == "0" ? 0 : ($sem_vinculo == "1" ? 1 : ($familia_desaparecida == "2" ? 2 : ($orfao == "3" ? 3 : ($poder_familia == "4" ? 4 : ($impedimento_judicial == "5" ? 5 : ($sem_informacao == "6" ? 6 : ($outra_situacao == "7" ? 7 : NULL)))))));

$qual_outra_situacao = isset($_POST['qual_outra_situacao']) && $_POST['qual_outra_situacao'] != "" ? $_POST['qual_outra_situacao'] : NULL;

$demonstram_interesse = isset($_POST['demonstram_interesse']) && $_POST['demonstram_interesse'] != "" && $_POST['demonstram_interesse'] == 1 ? 1 : 0;
$descreva_interesse = isset($_POST['descreva_interesse']) && $_POST['descreva_interesse'] != "" ? $_POST['descreva_interesse'] : NULL;
$crianca_demonstra_interesse = isset($_POST['crianca_demonstra_interesse']) && $_POST['crianca_demonstra_interesse'] != "" && $_POST['crianca_demonstra_interesse'] == 1 ? 1 : 0;
$descreva_demonstra_interesse = isset($_POST['descreva_demonstra_interesse']) && $_POST['descreva_demonstra_interesse'] != "" ? $_POST['descreva_demonstra_interesse'] : NULL;
$sim_proibicao_visita = isset($_POST['sim_proibicao_visita']) && $_POST['sim_proibicao_visita'] != "" && $_POST['sim_proibicao_visita'] == 1 ? 1 : 0;
$quem_estabeleceu_impedimento = isset($_POST['quem_estabeleceu_impedimento']) && $_POST['quem_estabeleceu_impedimento'] != "" ? $_POST['quem_estabeleceu_impedimento'] : NULL;

$sim_recebe_visita = isset($_POST['sim_recebe_visita']) && $_POST['sim_recebe_visita'] != "" && $_POST['sim_recebe_visita'] == 1 ? 1 : 0;
$encontros = isset($_POST['encontros']) && $_POST['encontros'] != "" ? $_POST['encontros'] : NULL;
$como_realizados = isset($_POST['como_realizados']) && $_POST['como_realizados'] != "" ? $_POST['como_realizados'] : NULL;
$sim_abusivo_alcool = isset($_POST['sim_abusivo_alcool']) && $_POST['sim_abusivo_alcool'] != "" && $_POST['sim_abusivo_alcool'] == 1 ? 1 : 0;

$sim_abusivo_outras_drogas = isset($_POST['sim_abusivo_outras_drogas']) && $_POST['sim_abusivo_outras_drogas'] != "" && $_POST['sim_abusivo_outras_drogas'] == 1 ? 1 : 0;
$sim_exploracao_sexual = isset($_POST['sim_exploracao_sexual']) && $_POST['sim_exploracao_sexual'] != "" && $_POST['sim_exploracao_sexual'] == 1 ? 1 : 0;
$sim_situacao_rua = isset($_POST['sim_situacao_rua']) && $_POST['sim_situacao_rua'] != "" && $_POST['sim_situacao_rua'] == 1 ? 1 : 0;
$sim_cumprimento_pena = isset($_POST['sim_cumprimento_pena']) && $_POST['sim_cumprimento_pena'] != "" && $_POST['sim_cumprimento_pena'] == 1 ? 1 : 0;
$sim_reabilitacao = isset($_POST['sim_reabilitacao']) && $_POST['sim_reabilitacao'] != "" && $_POST['sim_reabilitacao'] == 1 ? 1 : 0;
$sim_ameaca_morte = isset($_POST['sim_ameaca_morte']) && $_POST['sim_ameaca_morte'] != "" && $_POST['sim_ameaca_morte'] == 1 ? 1 : 0;
$sim_doenca_grave = isset($_POST['sim_doenca_grave']) && $_POST['sim_doenca_grave'] != "" && $_POST['sim_doenca_grave'] == 1 ? 1 : 0;
$sim_transtorno = isset($_POST['sim_transtorno']) && $_POST['sim_transtorno'] != "" && $_POST['sim_transtorno'] == 1 ? 1 : 0;

$qual_doença = isset($_POST['qual_doença']) && $_POST['qual_doença'] != "" ? $_POST['qual_doença'] : NULL;
$transtorno = isset($_POST['transtorno']) && $_POST['transtorno'] != "" ? $_POST['transtorno'] : NULL;
$pessoa_responsavel = isset($_POST['pessoa_responsavel']) && $_POST['pessoa_responsavel'] != "" ? $_POST['pessoa_responsavel'] : NULL;
$crianca_convive = isset($_POST['crianca_convive']) && $_POST['crianca_convive'] != "" ? $_POST['crianca_convive'] : NULL;
$familiar_interesse_sim = isset($_POST['familiar_interesse_sim']) && $_POST['familiar_interesse_sim'] != "" ? $_POST['familiar_interesse_sim'] : NULL;
$quem = isset($_POST['quem']) && $_POST['quem'] != "" ? $_POST['quem'] : NULL;

$familiar_condicoes_sim = isset($_POST['familiar_condicoes_sim']) && $_POST['familiar_condicoes_sim'] != "" ? $_POST['familiar_condicoes_sim'] : NULL;
$descreva_condicao_cuidado = isset($_POST['descreva_condicao_cuidado']) && $_POST['descreva_condicao_cuidado'] != "" ? $_POST['descreva_condicao_cuidado'] : NULL;
$opiniao_crianca = isset($_POST['opiniao_crianca']) && $_POST['opiniao_crianca'] != "" ? $_POST['opiniao_crianca'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($familia_id) && $familia_id > 0) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_familia SET mae_nome = ?, mae_nascimento = ?, mae_endereco = ?, mae_numero = ?, mae_bairro = ?, mae_rg = ?,
                                  mae_cpf = ?, mae_celular = ?, mae_ocupacao = ?, mae_situacao = ?, pai_nome = ?, pai_nascimento = ?, pai_endereco = ?, pai_numero = ?, pai_bairro = ?, pai_rg = ?,
                                  pai_cpf = ?, pai_celular = ?, pai_ocupacao = ?, pai_situacao = ?, oferecem_condicoes = ?, avaliacao_profunda = ?, possui_irmaos = ?, possui_irmaos_outro = ?, conhece_irmaos = ?, irmaos_adotados = ?,
                                  situacao_familiar = ?, outra_situacao = ?, demonstra_interesse = ?, descricao = ?, crianca_demonstra_interesse = ?, crianca_demonstra_descricao = ?, medida_restricao = ?, quem_estabelelceu = ?,
                                  recebe_visita = ?, frequencia_encontros = ?, como_realizados = ?, pais_envolvidos_alcool = ?, pais_envolvidos_outros = ?, pais_envolvidos_abuso = ?, situacao_rua = ?, pais_pena = ?, pais_reabilitacao = ?, pais_ameacados = ?, pais_doencas = ?, pais_transtorno = ?,
                                  qual_doenca = ?, qual_transtorno = ?, familiar_interesse = ?, opcao_familiar_condicao = ?, descreva_condicao_cuidado = ?, opiniao_crianca = ?, responsavel_id = ? 
                                  WHERE id = ?");
            $sql4->bindValue(1, $nome_mae);
            $sql4->bindValue(2, $nascimento_mae);
            $sql4->bindValue(3, $endereco_mae);
            $sql4->bindValue(4, $numero_mae);
            $sql4->bindValue(5, $bairro_mae);
            $sql4->bindValue(6, $rg_mae);
            $sql4->bindValue(7, $cpf_mae);
            $sql4->bindValue(8, $celular_mae);
            $sql4->bindValue(9, $ocupacao_mae);
            $sql4->bindValue(10, $mae_situacao);

            $sql4->bindValue(11, $nome_pai);
            $sql4->bindValue(12, $nascimento_pai);
            $sql4->bindValue(13, $endereco_pai);
            $sql4->bindValue(14, $numero_pai);
            $sql4->bindValue(15, $bairro_pai);
            $sql4->bindValue(16, $rg_pai);
            $sql4->bindValue(17, $cpf_pai);
            $sql4->bindValue(18, $celular_pai);
            $sql4->bindValue(19, $ocupacao_pai);
            $sql4->bindValue(20, $pai_situacao);

            $sql4->bindValue(21, $com_condicoes_convivio);
            $sql4->bindValue(22, $descreva_avaliacao_profunda);
            $sql4->bindValue(23, $com_irmaos_servico_acolhimento);

            $sql4->bindValue(24, $com_irmaos_outros_servico_acolhimento);
            $sql4->bindValue(25, $com_irmaos_em_servico_acolhimento);

            $sql4->bindValue(26, $com_irmaos_adotados);
            $sql4->bindValue(27, $situacao_familiar);

            $sql4->bindValue(28, $qual_outra_situacao);

            $sql4->bindValue(29, $demonstram_interesse);
            $sql4->bindValue(30, $descreva_interesse);
            $sql4->bindValue(31, $crianca_demonstra_interesse);
            $sql4->bindValue(32, $descreva_demonstra_interesse);
            $sql4->bindValue(33, $sim_proibicao_visita);
            $sql4->bindValue(34, $quem_estabeleceu_impedimento);

            $sql4->bindValue(35, $sim_recebe_visita);
            $sql4->bindValue(36, $encontros);
            $sql4->bindValue(37, $como_realizados);
            $sql4->bindValue(38, $sim_abusivo_alcool);

            $sql4->bindValue(39, $sim_abusivo_outras_drogas);
            $sql4->bindValue(40, $sim_exploracao_sexual);
            $sql4->bindValue(41, $sim_situacao_rua);
            $sql4->bindValue(42, $sim_cumprimento_pena);
            $sql4->bindValue(43, $sim_reabilitacao);
            $sql4->bindValue(44, $sim_ameaca_morte);
            $sql4->bindValue(45, $sim_doenca_grave);
            $sql4->bindValue(46, $sim_transtorno);

            $sql4->bindValue(47, $qual_doença);
            $sql4->bindValue(48, $transtorno);
            $sql4->bindValue(49, $familiar_interesse_sim);

            $sql4->bindValue(50, $familiar_condicoes_sim);
            $sql4->bindValue(51, $descreva_condicao_cuidado);
            $sql4->bindValue(52, $opiniao_crianca);

            $sql4->bindValue(53, $_SESSION['id']);
            $sql4->bindValue(54, $familia_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_familia (acolhimento_crianca_id, mae_nome, mae_nascimento, mae_endereco, mae_numero, mae_bairro, mae_rg,
                                  mae_cpf, mae_celular, mae_ocupacao, mae_situacao, pai_nome, pai_nascimento, pai_endereco, pai_numero, pai_bairro, pai_rg,
                                  pai_cpf, pai_celular, pai_ocupacao, pai_situacao, oferecem_condicoes, avaliacao_profunda, possui_irmaos, possui_irmaos_outro, conhece_irmaos, irmaos_adotados, situacao_familiar, outra_situacao,
                                  demonstra_interesse, descricao, crianca_demonstra_interesse, crianca_demonstra_descricao, medida_restricao, quem_estabelelceu,
                                  recebe_visita, frequencia_encontros, como_realizados, pais_envolvidos_alcool, pais_envolvidos_outros, pais_envolvidos_abuso, situacao_rua, pais_pena, pais_reabilitacao, pais_ameacados, pais_doencas, pais_transtorno,
                                  qual_doenca, qual_transtorno, familiar_interesse, opcao_familiar_condicao, descreva_condicao_cuidado, opiniao_crianca, responsavel_id, data_cadastro, status) 
                                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1)");
            $sql4->bindValue(1, $acolhimento_crianca_id);
            $sql4->bindValue(2, $nome_mae);
            $sql4->bindValue(3, $nascimento_mae);
            $sql4->bindValue(4, $endereco_mae);
            $sql4->bindValue(5, $numero_mae);
            $sql4->bindValue(6, $bairro_mae);
            $sql4->bindValue(7, $rg_mae);
            $sql4->bindValue(8, $cpf_mae);
            $sql4->bindValue(9, $celular_mae);
            $sql4->bindValue(10, $ocupacao_mae);
            $sql4->bindValue(11, $mae_situacao);

            $sql4->bindValue(12, $nome_pai);
            $sql4->bindValue(13, $nascimento_pai);
            $sql4->bindValue(14, $endereco_pai);
            $sql4->bindValue(15, $numero_pai);
            $sql4->bindValue(16, $bairro_pai);
            $sql4->bindValue(17, $rg_pai);
            $sql4->bindValue(18, $cpf_pai);
            $sql4->bindValue(19, $celular_pai);
            $sql4->bindValue(20, $ocupacao_pai);
            $sql4->bindValue(21, $pai_situacao);

            $sql4->bindValue(22, $com_condicoes_convivio);
            $sql4->bindValue(23, $descreva_avaliacao_profunda);
            $sql4->bindValue(24, $com_irmaos_servico_acolhimento);

            $sql4->bindValue(25, $com_irmaos_outros_servico_acolhimento);
            $sql4->bindValue(26, $com_irmaos_em_servico_acolhimento);

            $sql4->bindValue(27, $com_irmaos_adotados);
            $sql4->bindValue(28, $situacao_familiar);

            $sql4->bindValue(29, $qual_outra_situacao);

            $sql4->bindValue(30, $demonstram_interesse);
            $sql4->bindValue(31, $descreva_interesse);
            $sql4->bindValue(32, $crianca_demonstra_interesse);
            $sql4->bindValue(33, $descreva_demonstra_interesse);
            $sql4->bindValue(34, $sim_proibicao_visita);
            $sql4->bindValue(35, $quem_estabeleceu_impedimento);

            $sql4->bindValue(36, $sim_recebe_visita);
            $sql4->bindValue(37, $encontros);
            $sql4->bindValue(38, $como_realizados);
            $sql4->bindValue(39, $sim_abusivo_alcool);

            $sql4->bindValue(40, $sim_abusivo_outras_drogas);
            $sql4->bindValue(41, $sim_exploracao_sexual);
            $sql4->bindValue(42, $sim_situacao_rua);
            $sql4->bindValue(43, $sim_cumprimento_pena);
            $sql4->bindValue(44, $sim_reabilitacao);
            $sql4->bindValue(45, $sim_ameaca_morte);
            $sql4->bindValue(46, $sim_doenca_grave);
            $sql4->bindValue(47, $sim_transtorno);

            $sql4->bindValue(48, $qual_doença);
            $sql4->bindValue(49, $transtorno);
            $sql4->bindValue(50, $familiar_interesse_sim);

            $sql4->bindValue(51, $familiar_condicoes_sim);
            $sql4->bindValue(52, $descreva_condicao_cuidado);
            $sql4->bindValue(53, $opiniao_crianca);

            $sql4->bindValue(54, $_SESSION['id']);
            $sql4->execute();
        }

        //CONVIVE
        $sql7 = $db->prepare("DELETE FROM mod_acolhimento_crianca_familia_convive WHERE familia_id = ?");
        $sql7->bindValue(1, $familia_id);
        $sql7->execute();

        if ($crianca_convive != null) {
            foreach ($crianca_convive AS $key2 => $val2) {
                if (is_numeric($val2) && $val2 != "" && $val2 != null) {
                    $sql8 = $db->prepare("INSERT INTO mod_acolhimento_crianca_familia_convive (familia_id, responsavel_id) VALUES (?, ?)");
                    $sql8->bindValue(1, $familia_id);
                    $sql8->bindValue(2, $val2);
                    $sql8->execute();
                }
            }
        }

        //RESPONSAVEL PELA CRIANÇA
        $sql5 = $db->prepare("DELETE FROM mod_acolhimento_crianca_familia_responsaveis WHERE familia_id = ?");
        $sql5->bindValue(1, $familia_id);
        $sql5->execute();

        if ($pessoa_responsavel != null) {
            foreach ($pessoa_responsavel AS $key => $val) {
                if (is_numeric($val) && $val != "" && $val != null) {
                    $sql6 = $db->prepare("INSERT INTO mod_acolhimento_crianca_familia_responsaveis (familia_id, responsavel_id) VALUES (?, ?)");
                    $sql6->bindValue(1, $familia_id);
                    $sql6->bindValue(2, $val);
                    $sql6->execute();
                }
            }
        }

        //RESPONSAVEL PELA CRIANÇA
        $sql9 = $db->prepare("DELETE FROM mod_acolhimento_crianca_familia_interessados WHERE familia_id = ?");
        $sql9->bindValue(1, $familia_id);
        $sql9->execute();

        if ($quem != null) {
            foreach ($quem AS $key3 => $val3) {
                if (is_numeric($val3) && $val3 != "" && $val3 != null) {
                    $sql6 = $db->prepare("INSERT INTO mod_acolhimento_crianca_familia_interessados (familia_id, responsavel_id) VALUES (?, ?)");
                    $sql6->bindValue(1, $familia_id);
                    $sql6->bindValue(2, $val3);
                    $sql6->execute();
                }
            }
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>