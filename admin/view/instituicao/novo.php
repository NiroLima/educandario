<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- CSS DO PLUGIN DE UPLOAD DE FOTOS -->
<link href="<?= PORTAL_URL; ?>assets/vendor_components/cropper/css/cropper.min.css" rel="stylesheet">
<link href="<?= PORTAL_URL; ?>assets/vendor_components/cropper/css/main.css" rel="stylesheet">
<!-- FIM DO PLUGIN -->

<?php
$_SESSION['foto_cut'] = "";
$_SESSION['foto_origin'] = "";
unset($_SESSION['foto_cut']);
unset($_SESSION['foto_origin']);

$perfil = "";

$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT u.id, u.nome, u.contato, u.endereco, u.numero, u.status, u.bairro 
                 FROM seg_instituicao u 
                 WHERE u.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_instituicao = $result->fetch(PDO::FETCH_ASSOC);

    $instituicao_id = $dados_instituicao['id'];
    $instituicao_nome = ($dados_instituicao['nome']);
    $instituicao_status = $dados_instituicao['status'];
    $instituicao_endereco = $dados_instituicao['endereco'];
    $instituicao_contato = $dados_instituicao['contato'];
    $instituicao_numero = $dados_instituicao['numero'];
    $instituicao_bairro = $dados_instituicao['bairro'];
} else {
    $instituicao_id = "";
    $instituicao_nome = "";
    $instituicao_status = 1;
    $instituicao_endereco = "";
    $instituicao_contato = "";
    $instituicao_numero = "";
    $instituicao_bairro = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/instituicao/lista">Instituições</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <!-- PERFIL -->
            <div class="box box-solid bg-info">
                <div class="box-header">
                    <h4 class="box-title"><i class="fal fa-institution"></i> <strong>INSTITUIÇÃO</strong></h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="div_nome" class="form-group">
                                <label for="nome">NOME</label>
                                <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome Completo" value="<?= $instituicao_nome; ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="div_contato" class="form-group">
                                <label for="contato">CONTATO</label>
                                <input type="text" data-mask="(99) 9 9999-9999" class="form-control" name="contato" id="contato" placeholder="(99) 9 9999-9999" value="<?= $instituicao_contato; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIM PERFIL -->

            <form id="form_instituicao" name="form_instituicao" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $instituicao_id ?>"/>

                <!-- ENDEREÇO -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-home-lg"></i> <strong>ENDEREÇO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10">
                                <div id="div_endereco" class="form-group">
                                    <label for="endereco">ENDEREÇO</label>
                                    <input type="text" class="form-control" name="endereco" id="endereco" placeholder="Endereço" value="<?= $instituicao_endereco; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div id="div_numero" class="form-group">
                                    <label for="numero">NÚMERO</label>
                                    <input type="text" class="form-control" name="numero" id="numero" placeholder="1.260" value="<?= $instituicao_numero; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_bairro" class="form-group">
                                    <label for="bairro">BAIRRO</label>
                                    <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro" value="<?= $instituicao_bairro; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM ENDEREÇO -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $instituicao_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $instituicao_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/instituicao/novo.js"></script>