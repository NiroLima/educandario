<?php
//FUNÇÃO PARA VERIFICAR A PERMISSÃO NO MÓDULO
function vf_modulo() {
  @session_start();

  $db = Conexao::getInstance();

  $result = $db->prepare("SELECT moa.id
                          FROM seg_modulo_objeto_acao moa 
                          LEFT JOIN seg_modulo AS m ON m.id = moa.modulo_id 
                          LEFT JOIN seg_objeto AS o ON o.id = moa.objeto_id
                          WHERE m.nome = ? AND o.nome = ?");

  $result->bindValue(1, "" . Url::getURL(0) . "");
  $result->bindValue(2, "" . Url::getURL(3) . "");

  $result->execute();

  if ($result->rowCount() > 0) {//VERIFICA SE FOI CADASTRADO PERMISSÃO NO MÓDULO
    if (Url::getURL(0) != 'busca') {
      if (isset($_SESSION['permissao']["" . Url::getURL(0) . ""])) {
        return true; //TEM PERMISSÃO
      } else {
        // echo "<script 'text/javascript'>window.location = '" . PORTAL_URL . "modulos';</script>"; //NÃO TEM PERMISSÃO
      }
    } else {
      return true; //TEM PERMISSÃO
    }
  }
}

//FUNÇÃO PARA VERIFICAR A PERMISSÃO NO MÓDULO
function vf_modulos($modulo) {
  if (isset($_SESSION['permissao']["" . $modulo . ""])) {
    return true; //TEM PERMISSÃO
  } else {
    return false; //NÃO TEM PERMISSÃO
  }
}

//FUNÇÃO PARA VERIFICAR A PERMISSÃO DO OBJETO/AÇÃO 
function vf_objeto_acao($acao) {
  @session_start();

  if (isset($_SESSION['permissao']["" . Url::getURL(0) . ""]["" . Url::getURL(3) . ""]["" . $acao . ""])) {
    return true; //TEM PERMISSÃO
  } else {
    return false; //NÃO TEM PERMISSÃO
  }
}

//FUNÇÃO PARA VERIFICAR A PERMISSÃO DO OBJETO/AÇÃO PASSANDO A PÁGINA
function vf_objeto_acao_pagina($pagina, $acao) {
  @session_start();

  if (isset($_SESSION['permissao']["" . Url::getURL(0) . ""]["" . $pagina . ""]["" . $acao . ""])) {
    return true; //TEM PERMISSÃO
  } else {
    return false; //NÃO TEM PERMISSÃO
  }
}

//FUNÇÃO PARA VERIFICAR A PERMISSÃO DO OBJETO/AÇÃO PASSANDO A PÁGINA
function vf_modulo_objeto_acao_pagina($modulo, $pagina, $acao) {
  @session_start();

  if (isset($_SESSION['permissao']["" . $modulo . ""]["" . $pagina . ""]["" . $acao . ""])) {
    return true; //TEM PERMISSÃO
  } else {
    return false; //NÃO TEM PERMISSÃO
  }
}

//FUNÇÃO PARA VERIFICAR SE O USUÁRIO TEM PERMISSÃO PARA A PÁGINA
function vf_permissao_pagina($acao) {
  @session_start();

  $db = Conexao::getInstance();

  $result = $db->prepare("SELECT moa.id
                          FROM seg_modulo_objeto_acao moa 
                          LEFT JOIN seg_modulo AS m ON m.id = moa.modulo_id 
                          LEFT JOIN seg_objeto AS o ON o.id = moa.objeto_id
                          LEFT JOIN seg_acao AS a ON a.id = moa.acao_id
                          WHERE m.nome = ? AND o.nome = ? AND a.nome = ?");

  $result->bindValue(1, "" . Url::getURL(0) . "");
  $result->bindValue(2, "" . Url::getURL(3) . "");
  $result->bindValue(3, $acao);

  $result->execute();

  if ($result->rowCount() > 0) {//VERIFICA SE FOI CADASTRADO PERMISSÃO NO MÓDULO
    if (isset($_SESSION['permissao']["" . Url::getURL(0) . ""]["" . Url::getURL(3) . ""]["" . $acao . ""])) {
      return true; //TEM PERMISSÃO
    } else {
      echo "<script 'text/javascript'>window.location = '" . PORTAL_URL . "busca';</script>"; //NÃO TEM PERMISSÃO
    }
  }
}

//FUNÇÃO PARA VERIFICAR A PERMISSÃO NO MÓDULO DE USUÁRIOS
function vf_usuario_permissao($usuario_id) {
  @session_start();
  if (isset($_SESSION['permissao']["" . Url::getURL(0) . ""]["" . Url::getURL(3) . ""]["admin"]) || $_SESSION['id'] == $usuario_id) {
    return true; //TEM PERMISSÃO
  } else {
    return false; //NÃO TEM PERMISSÃO
  }
}

//VERIFICA SE O USUÁRIO TEM PERMISSÃO DE ADMINISTRADOR
function vf_administrador() {
  @session_start();
  if (isset($_SESSION['permissao']["" . Url::getURL(0) . ""]["" . Url::getURL(3) . ""]["admin"])) {
    return true; //TEM PERMISSÃO
  } else {
    return false; //NÃO TEM PERMISSÃO
  }
}

?>