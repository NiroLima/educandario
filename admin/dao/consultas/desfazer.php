<?php

$db = Conexao::getInstance();

$error = false;

$id = strip_tags(@$_POST['id']);

try {

    $db->beginTransaction();

    $stmt5 = $db->prepare("UPDATE mod_saude_consultas SET situacao = 1, data_finalizacao = NOW(), resp_finalizacao = ? WHERE id = ?");
    $stmt5->bindValue(1, $_SESSION['id']);
    $stmt5->bindValue(2, $id);
    $stmt5->execute();

    $db->commit();

    //MENSAGEM DE SUCESSO
    $msg['msg'] = 'success';
    $msg['retorno'] = 'Ação realizada com sucesso!';
    echo json_encode($msg);
    exit();
} catch (PDOException $e) {
    $db->rollback();
    $msg['msg'] = 'error';
    $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
    echo json_encode($msg);
    exit();
}
?>


