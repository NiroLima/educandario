//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {
    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('.custom-file-input').change(function () {
        var nome = $(this).val();
        $(this).parents("div.custom-file").find('label').html(nome);
    });

    $('input#registro_geral').click(function () {
        if ($(this).parents("div").find("input#registro_geral:checked").val() == "on") {
            $("div#div_rg").show();
        } else {
            $("input#rg_numero").val('');
            $("input#orgao_expedidor").val('');
            $("input#estado_expedidor").val('');
            $("input#expedicao").val('');
            $("input#rg_anexo").val('');
            $("input#rg_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_rg").hide();
        }
    });

    $('input#cpf').click(function () {
        if ($(this).parents("div").find("input#cpf:checked").val() == "on") {
            $("div#div_cpf").show();
        } else {
            $("input#numero_cpf").val('');
            $("input#cpf_anexo").val('');
            $("input#cpf_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_cpf").hide();
        }
    });

    $('input#certidao_nascimento').click(function () {
        if ($(this).parents("div").find("input#certidao_nascimento:checked").val() == "on") {
            $("div#div_certidao").show();
        } else {
            $("input#numero_certidao").val('');
            $("input#certidao_anexo").val('');
            $("input#certidao_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_certidao").hide();
        }
    });

    $('input#carteira_vacinacao').click(function () {
        if ($(this).parents("div").find("input#carteira_vacinacao:checked").val() == "on") {
            $("div#div_vacinacao").show();
        } else {
            $("input#numero_vacinacao").val('');
            $("input#vacinacao_anexo").val('');
            $("input#vacinacao_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_vacinacao").hide();
        }
    });

    $('input#historico_escolar').click(function () {
        if ($(this).parents("div").find("input#historico_escolar:checked").val() == "on") {
            $("div#div_historico").show();
        } else {
            $("input#guard_escolar").val('');
            $("input#historico_anexo").val('');
            $("input#historico_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_historico").hide();
        }
    });

    $('input#transferencia_escolar').click(function () {
        if ($(this).parents("div").find("input#transferencia_escolar:checked").val() == "on") {
            $("div#div_transferencia").show();
        } else {
            $("input#guard_transferencia").val('');
            $("input#transferencia_anexo").val('');
            $("input#transferencia_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_transferencia").hide();
        }
    });

    $('input#prontuario_medico').click(function () {
        if ($(this).parents("div").find("input#prontuario_medico:checked").val() == "on") {
            $("div#div_prontuario").show();
        } else {
            $("input#guard_pontuario").val('');
            $("input#prontuario_anexo").val('');
            $("input#prontuario_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_prontuario").hide();
        }
    });

    $('input#receituario_medico').click(function () {
        if ($(this).parents("div").find("input#receituario_medico:checked").val() == "on") {
            $("div#div_receituario").show();
        } else {
            $("input#guard_receituario").val('');
            $("input#receituario_anexo").val('');
            $("input#receituario_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_receituario").hide();
        }
    });

    $('input#mandado_busca').click(function () {
        if ($(this).parents("div").find("input#mandado_busca:checked").val() == "on") {
            $("div#div_mandado").show();
        } else {
            $("input#guard_mandado").val('');
            $("input#mandado_anexo").val('');
            $("input#mandado_anexo").parents("div.custom-file").find('label').html("Selecionar arquivo...");
            $("div#div_mandado").hide();
        }
    });

});
//------------------------------------------------------------------------------