//------------------------------------------------------------------------------
$('a#finalizar').click(function () {
    var id = $(this).attr('rel');

    swal({
        title: "Deseja mesmo finalizar?",
        text: "Obs: Caso escolha finalizar, o PIA irá para lista de finalizados!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode finalizar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/finalizar",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
});
//------------------------------------------------------------------------------
$('a#remover').click(function () {
    var id = $(this).attr('rel');

    swal({
        title: 'Deseja mesmo cancelar?',
        text: "Obs: Caso escolha cancelar, o PIA vai para lista de cancelados!",
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Sim, pode cancelar!',
        cancelButtonText: "Não",
        showLoaderOnConfirm: true,
        preConfirm: (motivo) => {

            return new Promise(function (resolve, reject) {
                if (motivo == "") {
                    reject('É necessário informar o motivo do cancelamento!');
                } else {
                    projetouniversal.util.getjson({
                        url: PORTAL_URL + "admin/dao/pia/desativar",
                        type: "POST",
                        data: {id: id, motivo: motivo},
                        enctype: 'multipart/form-data',
                        success: onSuccessSend,
                        error: onError
                    });
                    return false;
                }
            });
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        if (result.isConfirmed) {
            swal({
                title: `${result.value.login}'s avatar`,
                imageUrl: result.value.avatar_url
            });
        }
    });
});
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/lista');
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------