<?php

include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;

if ($error == false) {
    try {

        $db->beginTransaction();

        //RESPONSAVEL PELA CRIANÇA
        $sql1 = $db->prepare("DELETE FROM mod_acolhimento_crianca_familia_responsaveis WHERE responsavel_id = ?");
        $sql1->bindValue(1, $id);
        $sql1->execute();

        $sql2 = $db->prepare("DELETE FROM mod_acolhimento_crianca_familia_interessados WHERE responsavel_id = ?");
        $sql2->bindValue(1, $id);
        $sql2->execute();

        $sql3 = $db->prepare("DELETE FROM mod_acolhimento_crianca_familia_convive WHERE responsavel_id = ?");
        $sql3->bindValue(1, $id);
        $sql3->execute();

        $sql4 = $db->prepare("DELETE FROM mod_acolhimento_crianca_parentes WHERE id = ?");
        $sql4->bindValue(1, $id);
        $sql4->execute();

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>