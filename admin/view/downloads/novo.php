<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT md.descricao, md.id, md.nome_arquivo, md.arquivo, md.profissional_id, mse.nome AS especialidade_id, su.categoria_id   
                 FROM mod_downloads md  
                 LEFT JOIN seg_usuario AS su ON su.id = md.profissional_id 
                 LEFT JOIN seg_profissional AS sp ON sp.id =  md.profissional_id
                 LEFT JOIN mod_saude_especialidade AS mse ON mse.id = sp.especialidade_id 
                 WHERE md.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_medico = $result->fetch(PDO::FETCH_ASSOC);

    $downloads_id = $dados_medico['id'];
    $downloads_nome_arquivo = $dados_medico['nome_arquivo'];
    $downloads_arquivo = $dados_medico['arquivo'];
    $downloads_profissionais = $dados_medico['profissional_id'];
    $medico_especialidade = especialidade($dados_medico['profissional_id']);
    $medico_categoria = categoria_nome($dados_medico['profissional_id']);
    $descricao = $dados_medico['descricao'];
} else {
    $downloads_id = "";
    $downloads_nome_arquivo = "";
    $downloads_arquivo = "";
    $downloads_profissionais = "";
    $medico_especialidade = "";
    $medico_categoria = "";
    $descricao = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/downloads/lista">Arquivos</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_download" name="v" action="#" method="POST" enctype="multipart/form-data">

                <input type="hidden" id="id" name="id" value="<?= $downloads_id; ?>"/>

                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-arrow-square-down"></i> <strong>NOVO ARQUIVO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_nome_arquivo" class="form-group">
                                    <label for="nome_arquivo">NOME DO ARQUIVO</label>
                                    <input required="true" type="text" class="form-control" name="nome_arquivo" id="nome_arquivo" placeholder="Nome do Arquivo" value="<?= $downloads_nome_arquivo ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="div_nome_arquivo" class="form-group">
                                    <label for="selecione_arquivo">SELECIONE O ARQUIVO</label>
                                    <?php
                                    if ($downloads_arquivo != "" && $downloads_arquivo != null) {
                                        ?>
                                        <div class="row">
                                            <div class="col-md-10"><input readonly="true" type="text" class="form-control" name="arquivo" id="arquivo" value="<?= $downloads_arquivo ?>"></div>
                                            <div class="col-md-2"><a title="Remover Arquivo" style="cursor: pointer; margin-top: 5px;" id="remover" class="mr-2 waves-effect waves-light btn btn-md btn-danger"><i class="fal fa-trash-alt"></i></a></div>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <input required="true" type="file" class="form-control" name="file" id="file" placeholder="Selecione o Arquivo" value="">
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-10">
                            <div class="col-md-4">
                                <div id="div_profissional" class="form-group">
                                    <label for="profissional">PROFISSIONAL</label>
                                    <select required="true" name="profissional" id="profissional" class="form-control select2">
                                        <option value="">Selecione o profissional</option>
                                        <?php
                                        $result2 = $db->prepare("SELECT sp.nome, sp.id, sp.categoria_id 
                                                     FROM seg_profissional sp  
                                                     WHERE sp.status = 1 
                                                     ORDER BY sp.nome ASC");
                                        $result2->execute();
                                        while ($prof = $result2->fetch(PDO::FETCH_ASSOC)) {
                                            if ($downloads_profissionais == $prof['id']) {
                                                ?>
                                                <option categoria="<?= categoria_nome($prof['categoria_id']); ?>" especialidade="<?= especialidade($prof['id']); ?>" selected="true" value='<?= $prof['id']; ?>'><?= $prof['nome']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option categoria="<?= categoria_nome($prof['categoria_id']); ?>" especialidade="<?= especialidade($prof['id']); ?>" value='<?= $prof['id']; ?>'><?= $prof['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_especialidade" class="form-group">
                                    <label for="especialidade">ESPECIALIDADE</label>
                                    <input readonly="true" type="text" class="form-control" name="especialidade" id="especialidade" placeholder="Especialidade" value="<?= $medico_especialidade ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_tipo_categoria" class="form-group">
                                    <label for="tipo_categoria">CATEGORIA</label>
                                    <input readonly="true" type="text" class="form-control" name="tipo_categoria" id="tipo_categoria" placeholder="Categoria" value="<?= $medico_categoria ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-10">
                            <div class="col-md-12">
                                <div id="div_descricao" class="form-group">
                                    <label for="descricao">DESCRIÇÃO</label>
                                    <textarea required="true" name="descricao" id="descricao" class="form-control" cols="30" rows="10"><?= $descricao; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $downloads_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $downloads_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<?php
if (isset($_FILES['file'])) {

    $msg = "";

    $vf = false;

    $id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
    $nome_arquivo = isset($_POST['nome_arquivo']) && $_POST['nome_arquivo'] != "" ? $_POST['nome_arquivo'] : NULL;
    $profissional = isset($_POST['profissional']) && $_POST['profissional'] != "" ? $_POST['profissional'] : NULL;
    $descricao = isset($_POST['descricao']) && $_POST['descricao'] != "" ? $_POST['descricao'] : NULL;

    $countfiles = count($_FILES['file']['name']);

    if (isset($_FILES['file']['name']) && $_FILES['file']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['file']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['file']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg = "Somente arquivos PDF são permitidos.";
            }
        } else {
            $msg = "Você deve enviar um arquivo.";
        }
    }

    if ($msg == "") {

        if (isset($_FILES['file']['name']) && $_FILES['file']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "arquivos/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['file']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['file']['name'];
            $novo_nome = md5(time()) . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['file']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {
                    if ($id == 0) {
                        $stmt222 = $db->prepare('INSERT INTO mod_downloads (nome_arquivo, arquivo, profissional_id, descricao, responsavel_id, data_cadastro, data_update) VALUES(?, ?, ?, ?, ?, NOW(), NOW())');
                        $stmt222->bindValue(1, $nome_arquivo);
                        $stmt222->bindValue(2, "" . ($diretorio . $novo_nome) . "");
                        $stmt222->bindValue(3, $profissional);
                        $stmt222->bindValue(4, $descricao);
                        $stmt222->bindValue(5, $_SESSION['id']);
                        $stmt222->execute();

                        echo "<script 'text/javascript'>alert('Informações cadastradas com sucesso!');window.location = '" . PORTAL_URL . "admin/view/downloads/lista';</script>";
                    } else {
                        $stmt222 = $db->prepare('UPDATE mod_downloads SET nome_arquivo = ?, arquivo = ?, profissional_id = ?, descricao = ?, responsavel_id = ?, data_update = NOW() WHERE id = ?');
                        $stmt222->bindValue(1, $nome_arquivo);
                        $stmt222->bindValue(2, "" . ($diretorio . $novo_nome) . "");
                        $stmt222->bindValue(3, $profissional);
                        $stmt222->bindValue(4, $descricao);
                        $stmt222->bindValue(5, $_SESSION['id']);
                        $stmt222->bindValue(6, $id);
                        $stmt222->execute();

                        echo "<script 'text/javascript'>alert('Informações atualizada com sucesso!');window.location = '" . PORTAL_URL . "admin/view/downloads/lista';</script>";
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg = "Houve falha ao enviar os arquivos.";
            }
        }
    }

    if ($msg == "" && $msg != "Houve falha ao enviar os arquivos.") {
        $vf = true;
    }

    if ($msg != "") {
        echo "<script 'text/javascript'>alert('$msg');window.location = '" . PORTAL_URL . "admin/view/downloads/lista';</script>";
    } else if ($vf) {
        echo "<script 'text/javascript'>window.location = '" . PORTAL_URL . "admin/view/downloads/lista';</script>";
    }
} else if (isset($_POST['profissional']) && $_POST['profissional'] != "" && isset($_POST['descricao']) && $_POST['descricao'] != "") {

    $id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
    $nome_arquivo = isset($_POST['nome_arquivo']) && $_POST['nome_arquivo'] != "" ? $_POST['nome_arquivo'] : NULL;
    $profissional = isset($_POST['profissional']) && $_POST['profissional'] != "" ? $_POST['profissional'] : NULL;
    $descricao = isset($_POST['descricao']) && $_POST['descricao'] != "" ? $_POST['descricao'] : NULL;

    try {

        $stmt222 = $db->prepare('UPDATE mod_downloads SET nome_arquivo = ?, profissional_id = ?, descricao = ?, responsavel_id = ?, data_update = NOW() WHERE id = ?');
        $stmt222->bindValue(1, $nome_arquivo);
        $stmt222->bindValue(2, $profissional);
        $stmt222->bindValue(3, $descricao);
        $stmt222->bindValue(4, $_SESSION['id']);
        $stmt222->bindValue(5, $id);
        $stmt222->execute();

        echo "<script 'text/javascript'>alert('Informações atualizada com sucesso!');window.location = '" . PORTAL_URL . "admin/view/downloads/lista';</script>";
    } catch (PDOException $e) {
        $db->rollback();
        echo 'Error:' . $e->getMessage();
    }
}

function extensao($arquivo) {
    $arquivo = strtolower($arquivo);
    $explode = explode(".", $arquivo);
    $arquivo = end($explode);

    return ($arquivo);
}
?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/downloads/novo.js"></script>