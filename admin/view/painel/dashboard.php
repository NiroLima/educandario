<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="box bl-3 border-warning">

                <div class="row card-body pdl-2 pdr-2">

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/acolhimento/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Acolhimento</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/pia/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">PIA</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/consultas/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Consultas</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/exames/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Exames</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

                <div class="row card-body pdl-2 pdr-2">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/vacinas/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Vacinas</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/patrimonios/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Patrimônio</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/terapia/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Terapia</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/unidades/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Unidades de Saúde</h4>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="row card-body pdl-2 pdr-2">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/downloads/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Donwloads</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/funcoes/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Funções</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/setores/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Setores</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/medicamentos/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Medicamentos</h4>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

                <div class="row card-body pdl-2 pdr-2">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/usuarios/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Funcionários</h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/especialidades/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Especialidades</h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="<?= PORTAL_URL . "admin/view/profissionais/lista"; ?>">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Profissionais</h4>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <a href="#">
                            <div class="p-4 border border-light rounded d-flex align-items-center">
                                <i class="nav-icon i-File-Clipboard-File--Text text-32 mr-3"></i>
                                <div>
                                    <h4 class="text-18 mb-1">Relatórios</h4>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </section>
        <!-- /.content -->
    </div>
</div>

<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>
