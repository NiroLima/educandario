<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$medicamento = isset($_POST['medicamento']) ? $_POST['medicamento'] : NULL;
$quantidade = isset($_POST['quantidade']) ? $_POST['quantidade'] : 0;
$validade = isset($_POST['validade']) ? $_POST['validade'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "mod_medicamento", "nome", "=", $medicamento, "");
if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome do medicamento informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_medicamento (nome, qtd, data_validade, data_update, usuario_id, data_cadastro, status) VALUES (?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $medicamento);
            $sql->bindValue(2, $quantidade);
            $sql->bindValue(3, $validade);
            $sql->bindValue(4, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE mod_medicamento SET nome = ?, qtd = ?, data_validade = ?, usuario_id = ? WHERE id = ?");
            $sql->bindValue(1, $medicamento);
            $sql->bindValue(2, $quantidade);
            $sql->bindValue(3, $validade);
            $sql->bindValue(4, $_SESSION['id']);
            $sql->bindValue(5, $id);
            $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>