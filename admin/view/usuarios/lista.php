<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Funcionários</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-user"></i> <strong>LISTA DE FUNCIONÁRIOS</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/usuarios/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVO FUNCIONÁRIO</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Funcão</th>
                                            <th>Categoria</th>
                                            <th>Período</th>
                                            <th>E-mail</th>
                                            <th>Celular</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $db->prepare("SELECT u.id, u.status, u.periodo, u.nome, u.celular, u.email, sf.nome AS funcao , u.categoria_id 
                                                                FROM seg_usuario u  
                                                                LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id
                                                                WHERE 1
                                                                ORDER BY u.nome");
                                        $result->execute();
                                        while ($usuario = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $usuario['nome']; ?></td>
                                                <td><?= $usuario['funcao']; ?></td>
                                                <td><?= categoria_nome($usuario['categoria_id']); ?></td>
                                                <td><?= periodo_nome($usuario['periodo']); ?></td>
                                                <td><?= $usuario['email']; ?></td>
                                                <td><?= $usuario['celular']; ?></td>
                                                <td class="text-center">
                                                    <?php
                                                    if (ver_nivel(1, "") || ver_nivel(3, "")) {
                                                        ?>
                                                        <a id="ativar" rel="<?= $usuario['id']; ?>" <?= $usuario['status'] == 0 && $usuario['id'] != 1 || $usuario['status'] == 0 && $usuario['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> title="Desbloquear Funcionário" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                        <a id="remover" rel="<?= $usuario['id']; ?>" <?= $usuario['status'] == 1 && $usuario['id'] != 1 || $usuario['status'] == 1 && $usuario['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> title="Bloquear Funcionário" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>

                                                        <?php
                                                    }
                                                    ?>
                                                    <a <?= $usuario['id'] != 1 && ver_nivel(1, "") || $usuario['id'] != 1 && ver_nivel(3, "") || $usuario['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> href="<?= PORTAL_URL ?>admin/view/usuarios/novo/<?= $usuario['id']; ?>" title="Editar Funcionário" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/usuarios/lista.js"></script>
