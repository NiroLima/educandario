<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Terapias</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-clipboard-user"></i> <strong>TERAPIAS</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/terapia/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVA TERAPIA</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>NOME DO PACIENTE</th>
                                            <th>DATA</th>
                                            <th>HORA</th>
                                            <th width="200px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $result = $db->prepare("SELECT msc.id, mac.nome AS crianca, msc.data_terapia, msc.hora_terapia, msc.status  
                                                                FROM mod_saude_terapias msc 
                                                                LEFT JOIN mod_acolhimento_crianca AS mac ON mac.id = msc.crianca_id 
                                                                WHERE msc.situacao = 1 
                                                                ORDER BY mac.nome");
                                        $result->execute();
                                        while ($medicamento = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $medicamento['crianca']; ?></td>
                                                <td><?= obterDataBRTimestamp($medicamento['data_terapia']); ?></td>
                                                <td><?= $medicamento['hora_terapia']; ?></td>
                                                <td class="text-center">
                                                    <a id="finalizar" rel="<?= $medicamento['id']; ?>" href="#" title="Finalizar Terapia" class="mr-2 waves-effect waves-light btn btn-xs btn-info">FINALIZAR</a>
                                                    <?php
                                                    if (ver_nivel(1, "") || ver_nivel(3, "")) {
                                                        ?>
                                                        <a id="ativar" rel="<?= $medicamento['id']; ?>" <?= $medicamento['status'] == 0 ? "" : "style='display: none'"; ?> title="Desbloquear Terapia" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                        <a id="remover" rel="<?= $medicamento['id']; ?>" <?= $medicamento['status'] == 1 ? "" : "style='display: none'"; ?> title="Bloquear Terapia" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                        <?php
                                                    }
                                                    ?>

                                                    <a href="<?= PORTAL_URL ?>admin/view/terapia/novo/<?= $medicamento['id']; ?>" title="Editar Terapia" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/terapia/lista.js"></script>
