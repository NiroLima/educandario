<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;

$instituicao = isset($_POST['instituicao']) && $_POST['instituicao'] != "" ? $_POST['instituicao'] : NULL;
$contato = isset($_POST['contato']) && $_POST['contato'] != "" ? $_POST['contato'] : NULL;
$endereco = isset($_POST['endereco']) && $_POST['endereco'] != "" ? $_POST['endereco'] : NULL;
$numero = isset($_POST['numero']) && $_POST['numero'] != "" ? $_POST['numero'] : NULL;
$bairro = isset($_POST['bairro']) && $_POST['bairro'] != "" ? $_POST['bairro'] : NULL;
$nome_irmao = isset($_POST['nome_irmao']) && $_POST['nome_irmao'] != "" ? $_POST['nome_irmao'] : NULL;
$tipo_acompanhamento = isset($_POST['tipo_acompanhamento']) && $_POST['tipo_acompanhamento'] != "" ? $_POST['tipo_acompanhamento'] : NULL;
$frequencia_visita = isset($_POST['frequencia_visita']) && $_POST['frequencia_visita'] != "" ? $_POST['frequencia_visita'] : NULL;
$referencia_contato = isset($_POST['referencia_contato']) && $_POST['referencia_contato'] != "" ? $_POST['referencia_contato'] : NULL;

$membro_id = isset($_POST['membro_id']) && $_POST['membro_id'] != "" ? $_POST['membro_id'] : "";

$codigo_membro = pesquisar2("id", "mod_acolhimento_crianca_rede_membros", "nome", "=", $nome_irmao, "acolhimento_crianca_rede_id", "=", $id, "");

if (is_numeric($codigo_membro) && $membro_id != $codigo_membro) {
    $error = true;
    $msg['msg'] = 'error';
    $msg['retorno'] = "O membroo escolhido já está adicionado na lista";
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($membro_id)) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_rede_membros SET instituicao = ?, contato = ?, endereco = ?, numero = ?, bairro = ?, nome = ?, tipo = ?, frequencia = ?, referencia = ?, responsavel_id = ? WHERE id = ?");
            $sql4->bindValue(1, $instituicao);
            $sql4->bindValue(2, $contato);
            $sql4->bindValue(3, $endereco);
            $sql4->bindValue(4, $numero);
            $sql4->bindValue(5, $bairro);
            $sql4->bindValue(6, $nome_irmao);
            $sql4->bindValue(7, $tipo_acompanhamento);
            $sql4->bindValue(8, $frequencia_visita);
            $sql4->bindValue(9, $referencia_contato);
            $sql4->bindValue(10, $_SESSION['id']);
            $sql4->bindValue(11, $membro_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_rede_membros (instituicao, contato, endereco, numero, bairro, nome, tipo, frequencia, referencia, acolhimento_crianca_rede_id, responsavel_id, data_cadastro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())");
            $sql4->bindValue(1, $instituicao);
            $sql4->bindValue(2, $contato);
            $sql4->bindValue(3, $endereco);
            $sql4->bindValue(4, $numero);
            $sql4->bindValue(5, $bairro);
            $sql4->bindValue(6, $nome_irmao);
            $sql4->bindValue(7, $tipo_acompanhamento);
            $sql4->bindValue(8, $frequencia_visita);
            $sql4->bindValue(9, $referencia_contato);
            $sql4->bindValue(10, $id);
            $sql4->bindValue(11, $_SESSION['id']);
            $sql4->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
} else {
    echo json_encode($msg);
    exit();
}
?>