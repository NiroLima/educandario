<form action="#" class="tab-wizard wizard-circle wizard clearfix" role="application" id="steps-uid-0">
    <div class="steps clearfix">
        <ul role="tablist">
            <li role="tab" class="<?= $pagina == "acolhido" ? "first current" : "done" ?> <?= carregar_info_menu($acolhimento_id, 1) ? "complete" : ""; ?>" aria-disabled="false" aria-selected="true">
                <a id="steps-uid-0-t-0" href="<?= PORTAL_URL ?>admin/view/pia/acolhido/<?= $acolhimento_id ?>" aria-controls="steps-uid-0-p-0">
                    <span class="current-info audible">current step: </span>
                    <span class="step">1</span> ACOLHIDO(A)</a>
            </li>
            <li role="tab" class="<?= $pagina == "acolhimento" ? "current" : "done" ?> <?= carregar_info_menu($acolhimento_id, 2) ? "complete" : ""; ?>" aria-disabled="true">
                <a id="steps-uid-0-t-1" href="<?= PORTAL_URL ?>admin/view/pia/acolhimento/<?= $acolhimento_id ?>" aria-controls="steps-uid-0-p-1">
                    <span class="step">2</span> ACOLHIMENTO
                </a>
            </li>
            <li role="tab" class="<?= $pagina == "saude" ? "current" : "done" ?> <?= carregar_info_menu($acolhimento_id, 3) ? "complete" : ""; ?>" aria-disabled="true">
                <a id="steps-uid-0-t-2" href="<?= PORTAL_URL ?>admin/view/pia/saude/<?= $acolhimento_id ?>" aria-controls="steps-uid-0-p-2">
                    <span class="step">3</span> SAÚDE
                </a>
            </li>
            <li role="tab" class="<?= $pagina == "educacao" ? "current" : "done" ?> <?= carregar_info_menu($acolhimento_id, 4) ? "complete" : ""; ?>" aria-disabled="true">
                <a id="steps-uid-0-t-3" href="<?= PORTAL_URL ?>admin/view/pia/educacao/<?= $acolhimento_id ?>" aria-controls="steps-uid-0-p-3">
                    <span class="step">4</span>
                    EDUCAÇÃO
                </a>
            </li>
            <li role="tab" class="<?= $pagina == "familia" ? "current" : "done" ?> <?= carregar_info_menu($acolhimento_id, 5) ? "complete" : ""; ?>" aria-disabled="true">
                <a id="steps-uid-0-t-4" href="<?= PORTAL_URL ?>admin/view/pia/familia/<?= $acolhimento_id ?>" aria-controls="steps-uid-0-p-4">
                    <span class="step">5</span> FAMÍLIA
                </a>
            </li>
            <li role="tab" class="<?= $pagina == "rede" ? "current" : "done" ?> <?= carregar_info_menu($acolhimento_id, 6) ? "complete" : ""; ?>" aria-disabled="true">
                <a id="steps-uid-0-t-5" href="<?= PORTAL_URL ?>admin/view/pia/rede/<?= $acolhimento_id ?>" aria-controls="steps-uid-0-p-5">
                    <span class="step">6</span>
                    REDE
                </a>
            </li>
            <li role="tab" class="<?= $pagina == "plano" ? "current" : "done" ?> <?= carregar_info_menu($acolhimento_id, 7) ? "complete" : ""; ?>" aria-disabled="true">
                <a id="steps-uid-0-t-6" href="<?= PORTAL_URL ?>admin/view/pia/plano/<?= $acolhimento_id ?>" aria-controls="steps-uid-0-p-6">
                    <span class="step">7</span>
                    PLANO DE AÇÃO
                </a>
            </li>
            <li role="tab" class="<?= $pagina == "avaliacao" ? "current" : "done" ?> last <?= carregar_info_menu($acolhimento_id, 8) ? "complete" : ""; ?>" aria-disabled="true">
                <a id="steps-uid-0-t-7" href="<?= PORTAL_URL ?>admin/view/pia/avaliacao/<?= $acolhimento_id ?>" aria-controls="steps-uid-0-p-7">
                    <span class="step">8</span>
                    AVALIAÇÃO
                </a>
            </li>
        </ul>
    </div>
</form>