<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$rede_id = isset($_POST['rede_id']) && $_POST['rede_id'] != "" ? $_POST['rede_id'] : 0;

$sim_acolhimento_institucional = isset($_POST['sim_acolhimento_institucional']) && $_POST['sim_acolhimento_institucional'] != "" ? $_POST['sim_acolhimento_institucional'] : 0;
$sim_acolhimento_institucional2 = isset($_POST['sim_acolhimento_institucional2']) && $_POST['sim_acolhimento_institucional2'] != "" ? $_POST['sim_acolhimento_institucional2'] : 0;

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($rede_id) && $rede_id > 0) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_rede SET acompanhamento = ?, acompanhamento2 = ?, responsavel_id = ? 
                                  WHERE id = ?");
            $sql4->bindValue(1, $sim_acolhimento_institucional);
            $sql4->bindValue(2, $sim_acolhimento_institucional2);
            $sql4->bindValue(3, $_SESSION['id']);
            $sql4->bindValue(4, $rede_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_rede (acolhimento_crianca_id, acompanhamento, acompanhamento2, responsavel_id, data_cadastro, status)
                     VALUES (?, ?, ?, ?, NOW(), 1)");
            $sql4->bindValue(1, $id);
            $sql4->bindValue(2, $sim_acolhimento_institucional);
            $sql4->bindValue(3, $sim_acolhimento_institucional2);
            $sql4->bindValue(4, $_SESSION['id']);
            $sql4->execute();

            $rede_id = $db->lastInsertId();

            $stmp = $db->prepare("SELECT *           
                     FROM mod_acolhimento_crianca_rede_membros    
                     WHERE acolhimento_crianca_rede_id	 = 0 AND responsavel_id = ?");
            $stmp->bindValue(1, $_SESSION['id']);
            $stmp->execute();
            while ($membros = $stmp->fetch(PDO::FETCH_ASSOC)) {
                $sql5 = $db->prepare("UPDATE mod_acolhimento_crianca_rede_membros SET acolhimento_crianca_rede_id = ? WHERE id = ?");
                $sql5->bindValue(1, $rede_id);
                $sql5->bindValue(2, $membros['id']);
                $sql5->execute();
            }
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>