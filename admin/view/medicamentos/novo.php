<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT * 
                 FROM mod_medicamento mm  
                 WHERE mm.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_medicamento = $result->fetch(PDO::FETCH_ASSOC);

    $medicamento_id = $dados_medicamento['id'];
    $medicamento_nome = $dados_medicamento['nome'];
    $medicamento_qtd = $dados_medicamento['qtd'];
    $medicamento_validade = $dados_medicamento['data_validade'];
} else {
    $medicamento_id = "";
    $medicamento_nome = "";
    $medicamento_qtd = "";
    $medicamento_validade = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/medicamentos/lista">Medicamentos</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_medicamento" name="form_medicamento" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $medicamento_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-syringe"></i> <strong>NOVO MEDICAMENTO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10">
                                <div id="div_medicamento" class="form-group">
                                    <label for="medicamento">Medicamento</label>
                                    <input type="text" class="form-control" name="medicamento" id="medicamento" placeholder="Nome do Medicamento" value="<?= $medicamento_nome; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div id="div_quantidade" class="form-group">
                                    <label for="quantidade">Quantidade</label>
                                    <input type="text" class="form-control" name="quantidade" id="quantidade" placeholder="UN" value="<?= $medicamento_qtd; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div id="div_validade" class="form-group">
                                    <label for="validade">Validade</label>
                                    <input class="form-control" name="validade" id="validade" type="date" value="<?= $medicamento_validade; ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $medicamento_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $medicamento_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/medicamentos/novo.js"></script>