//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("select#nome_crianca").change(function () {
        var idade = $(this).find('option:selected').attr('rel2');

        $("#idade_crianca").val(idade);

    });

    $("select#nome_acompanhante").change(function () {
        var funcao = $(this).find('option:selected').attr('rel2');

        $("#funcao").val(funcao);

    });

    $("select#nome_profissional").change(function () {
        var funcao = $(this).find('option:selected').attr('rel2');

        $("#especialidade").val(funcao);

    });

    $("#atendimento_medico").click(function () {
        $("div#div_vacinas_aplicadas").hide();
    });

    $("#atendomento_odontologico").click(function () {
        $("div#div_vacinas_aplicadas").hide();
    });

    $("#vacinacao").click(function () {
        $("div#div_vacinas_aplicadas").show();
    });

    $("#cirurgia").click(function () {
        $("div#div_vacinas_aplicadas").hide();
    });

    $("#urgencia").click(function () {
        $("div#div_vacinas_aplicadas").hide();
    });

    $("#solicitacao_retorno_sim").click(function () {
        $("div#div_retorno").show();
    });

    $("#solicitacao_retorno_nao").click(function () {
        $("div#div_retorno").hide();
    });

    $("select#unidade_saude").change(function () {
        var contato = $(this).find('option:selected').attr('contato');
        var endereco = $(this).find('option:selected').attr('endereco');
        var numero = $(this).find('option:selected').attr('numero');
        var bairro = $(this).find('option:selected').attr('bairro');

        $("#contato_unidade_saude").val(contato);
        $("#endereco_unidade_saude").val(endereco);
        $("#numero_unidade_saude").val(numero);
        $("#bairro_unidade_saude").val(bairro);
    });

    $("#add").click(function () {

        var consulta_id = $("#id").val();
        var diagnostico_id = $("#diagnostico_id").val();

        var data_diagnostico = $("#data_diagnostico").val();
        var diagnostico_medico = $("#diagnostico_medico").val();
        var orientacoes_medicas = $("#orientacoes_medicas").val();
        var exames_solicitados = $("#exames_solicitados").val();
        var medicacoes_posologia = $("#medicacoes_posologia").val();
        var especialistas = $("#especialistas").val();

        if (formulario_validator("")) {
            $.post(PORTAL_URL + "admin/dao/consultas/diagnostico.php", {consulta_id: consulta_id, diagnostico_id: diagnostico_id, especialistas: especialistas, medicacoes_posologia: medicacoes_posologia, data_diagnostico: data_diagnostico, diagnostico_medico: diagnostico_medico, orientacoes_medicas: orientacoes_medicas, exames_solicitados: exames_solicitados}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Consulta",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Consulta",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        $.post(PORTAL_URL + "admin/dao/consultas/carregar_diagnosticos.php",
                                {id: consulta_id},
                                function (valor) {
                                    $("button#add").html("INSERIR");

                                    $("input#diagnostico_id").val("");
                                    $("textarea#diagnostico_medico").val("");
                                    $("textarea#orientacoes_medicas").val("");
                                    $("textarea#medicacoes_posologia").val("");
                                    $("select#exames_solicitados").val("");
                                    $("select#exames_solicitados").select2();
                                    $("select#especialistas").val("");
                                    $("select#especialistas").select2();

                                    $("tbody#resultado_diagnostico").html(valor);

                                    var topPosition = $("div#div_diagnostico").offset().top - 135;
                                    $('html, body').animate({
                                        scrollTop: topPosition
                                    }, 800);

                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $('#form_consulta').submit(function () {
        if (formulario_validator_2("")) {
            projetouniversal.util.getjson({
                url: PORTAL_URL + "admin/dao/consultas/novo.php",
                type: "POST",
                data: $('#form_consulta').serialize(),
                enctype: 'multipart/form-data',
                success: onSuccessSend2,
                error: onError2
            });
            return false;
        } else {
            return false;
        }

    });
});
//------------------------------------------------------------------------------    
function onSuccessSend2(obj) {
    if (obj.msg == 'success') {
        swal({
            title: "Formulário de Consulta",
            text: "Ação realizada com sucesso!",
            type: "success",
            confirmButtonClass: "btn btn-success",
            confirmButtonText: "Ok"
        }).then(function () {
            postToURL(PORTAL_URL + 'admin/view/consultas/lista');
        });
    } else if (obj.msg == 'error') {
        formulario_validator_2(obj);
    }
    return false;
}
//------------------------------------------------------------------------------
function onError2(args) {
    swal({
        title: "Error!",
        text: "" + args.retorno + "",
        type: "error",
        confirmButtonClass: "btn btn-danger",
        confirmButtonText: "Ok"
    });
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_2(obj) {
    var valido = true;

    var data_consulta = $("#data_consulta").val();
    var hora_consulta = $("#hora_consulta").val();
    var unidade_saude = $("#unidade_saude").val();
    var nome_crianca = $("#nome_crianca").val();
    var nome_acompanhante = $("#nome_acompanhante").val();
    var nome_profissional = $("#nome_profissional").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome_profissional == "") {
            $('div#div_nome_profissional').after('<label id="erro_nome_profissional" class="error">O profissional é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_profissional');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome_acompanhante == "") {
            $('div#div_nome_acompanhante').after('<label id="erro_nome_acompanhante" class="error">O acompanhante é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_acompanhante');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome_crianca == "") {
            $('div#div_nome_crianca').after('<label id="erro_nome_crianca" class="error">Nome da criança é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_crianca');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (unidade_saude == "") {
            $('div#div_unidade_saude').after('<label id="erro_unidade_saude" class="error">Unidade é obrigatório.</label>');
            valido = false;
            element = $('div#div_unidade_saude');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (hora_consulta == "") {
            $('div#div_hora_consulta').after('<label id="erro_hora_consulta" class="error">Hora é obrigatório.</label>');
            valido = false;
            element = $('div#div_hora_consulta');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (data_consulta == "" || data_consulta == "dd/mm/aaaa") {
            $('div#div_data_consulta').after('<label id="erro_data_consulta" class="error">Data é obrigatório.</label>');
            valido = false;
            element = $('div#div_data_consulta');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var diagnostico_medico = $("#diagnostico_medico").val();
    var orientacoes_medicas = $("#orientacoes_medicas").val();
    var exames_solicitados = $("#exames_solicitados").val();
    var medicacoes_posologia = $("#medicacoes_posologia").val();
    var especialistas = $("#especialistas").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (medicacoes_posologia == "") {
            $('div#div_medicacoes_posologia').after('<label id="erro_medicacoes_posologia" class="error">Medicações é obrigatório.</label>');
            valido = false;
            element = $('div#div_medicacoes_posologia');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (orientacoes_medicas == "") {
            $('div#div_orientacoes_medicas').after('<label id="erro_orientacoes_medicas" class="error">Orientações é obrigatório.</label>');
            valido = false;
            element = $('div#div_orientacoes_medicas');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (diagnostico_medico == "") {
            $('div#div_diagnostico_medico').after('<label id="erro_diagnostico_medico" class="error">Diagnóstico é obrigatório.</label>');
            valido = false;
            element = $('div#div_diagnostico_medico');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function editar(id, diagnostico, orientacoes, medicacoes) {
    $("input#diagnostico_id").val(id);
    $("textarea#diagnostico_medico").val(diagnostico);
    $("textarea#orientacoes_medicas").val(orientacoes);
    $("textarea#medicacoes_posologia").val(medicacoes);
    $("button#add").html("ALTERAR");

    $.post(PORTAL_URL + "admin/dao/consultas/carregar_exames.php",
            {id: id},
            function (valor) {
                $("select#exames_solicitados").html(valor);
                $("select#exames_solicitados").select2();
            });

    $.post(PORTAL_URL + "admin/dao/consultas/carregar_especialistas.php",
            {id: id},
            function (valor) {
                $("select#especialistas").html(valor);
                $("select#especialistas").select2();
            });
}
//------------------------------------------------------------------------------------------------------
function remover(obj, codigo) {
    swal({
        title: "Deseja mesmo remover este diagnóstico?",
        text: "Obs: Caso escolha remover o diagnóstico, não será mais possível recuperar os dados novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/consultas/remover_diagnostico",
            type: "POST",
            data: {id: codigo},
            enctype: 'multipart/form-data',
            success: onSuccessSend22(obj),
            error: onSuccessSend22(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend22(obj) {
    swal({
        title: "Sucesso!",
        text: "Diagnóstico removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("button#add").html("INSERIR");

        $("input#diagnostico_id").val("");
        $("textarea#diagnostico_medico").val("");
        $("textarea#orientacoes_medicas").val("");
        $("textarea#medicacoes_posologia").val("");
        $("select#exames_solicitados").val("");
        $("select#exames_solicitados").select2();
        $("select#especialistas").val("");
        $("select#especialistas").select2();
        
        $(obj).parents('tr#remover_diagnostico').remove();
    });

    return false;
}
//------------------------------------------------------------------------------------------------------