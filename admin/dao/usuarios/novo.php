<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$nome = isset($_POST['nome']) ? $_POST['nome'] : NULL;
$login = isset($_POST['login']) ? $_POST['login'] : NULL;
$cpf = isset($_POST['cpf']) ? $_POST['cpf'] : NULL;
$senha = isset($_POST['senha']) ? $_POST['senha'] : NULL;
$municipio_id = isset($_POST['municipio_id']) ? $_POST['municipio_id'] : NULL;
$nascimento = isset($_POST['nascimento']) ? $_POST['nascimento'] : NULL;
$nivel = isset($_POST['nivel']) && $_POST['nivel'] != "" ? $_POST['nivel'] : null;

$email = isset($_POST['email']) ? $_POST['email'] : NULL;
$celular = isset($_POST['celular']) ? $_POST['celular'] : NULL;
$fixo = isset($_POST['fixo']) ? $_POST['fixo'] : NULL;
$rua = isset($_POST['rua']) ? $_POST['rua'] : NULL;
$numero = isset($_POST['numero']) ? $_POST['numero'] : NULL;
$bairro = isset($_POST['bairro']) ? $_POST['bairro'] : NULL;

$funcao = isset($_POST['funcao']) ? $_POST['funcao'] : NULL;
$categoria = isset($_POST['categoria']) ? $_POST['categoria'] : NULL;
$periodo = isset($_POST['periodo']) ? $_POST['periodo'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "seg_usuario", "nome", "=", $nome, "");
if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome do funcionário informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO seg_usuario (nome, cpf, login, senha, cidade_id, nascimento, email, celular, contato, rua, numero, bairro, funcao_id, categoria_id, periodo, data_update, usuario_id, data_cadastro, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $nome);
            $sql->bindValue(2, $cpf);
            $sql->bindValue(3, $login);
            $sql->bindValue(4, sha1($senha));
            $sql->bindValue(5, $municipio_id);
            $sql->bindValue(6, $nascimento);
            $sql->bindValue(7, $email);
            $sql->bindValue(8, $celular);
            $sql->bindValue(9, $fixo);
            $sql->bindValue(10, $rua);
            $sql->bindValue(11, $numero);
            $sql->bindValue(12, $bairro);
            $sql->bindValue(13, $funcao);
            $sql->bindValue(14, $categoria);
            $sql->bindValue(15, $periodo);
            $sql->bindValue(16, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();

            //INSERINDO SESSÃO
            $useragent = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('|MSIE ([0-9].[0-9]{1,2})|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'IE';
            } elseif (preg_match('|Opera/([0-9].[0-9]{1,2})|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'Opera';
            } elseif (preg_match('|Firefox/([0-9\.]+)|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'Firefox';
            } elseif (preg_match('|Chrome/([0-9\.]+)|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'Chrome';
            } elseif (preg_match('|Safari/([0-9\.]+)|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'Safari';
            } else {
                $browser_version = 0;
                $browser = 'Desconhecido';
            }
            $separa = explode(";", $useragent);
            $so = $separa[1];

            $stmt1 = $db->prepare("INSERT INTO seg_sessao 
                     (usuario_id, usuario_pai_id, host, ip, navegador, sistema_operacional, numero_sessao)
                      VALUES (?, ?, ?, ?, ?, ?, ?)");
            $stmt1->bindValue(1, $id);
            $stmt1->bindValue(2, $_SESSION['id']);
            $stmt1->bindValue(3, $_SERVER["SERVER_NAME"]);
            $stmt1->bindValue(4, $_SERVER['REMOTE_ADDR']);
            $stmt1->bindValue(5, $browser . " " . $browser_version);
            $stmt1->bindValue(6, $so);
            $stmt1->bindValue(7, session_id());
            $stmt1->execute();
        } else {
            if ($senha != "" && $senha != NULL && !is_numeric(pesquisar2("id", "seg_usuario", "id", "=", $id, "senha", "=", $senha, ""))) {
                $sql = $db->prepare("UPDATE seg_usuario SET nome = ?, cpf = ?, login = ?, senha = ?, cidade_id = ?, nascimento = ?, email = ?, celular = ?, contato = ?, rua = ?, numero = ?, bairro = ?, funcao_id = ?, categoria_id = ?, periodo = ?, usuario_id = ? WHERE id = ?");
                $sql->bindValue(1, $nome);
                $sql->bindValue(2, $cpf);
                $sql->bindValue(3, $login);
                $sql->bindValue(4, sha1($senha));
                $sql->bindValue(5, $municipio_id);
                $sql->bindValue(6, $nascimento);
                $sql->bindValue(7, $email);
                $sql->bindValue(8, $celular);
                $sql->bindValue(9, $fixo);
                $sql->bindValue(10, $rua);
                $sql->bindValue(11, $numero);
                $sql->bindValue(12, $bairro);
                $sql->bindValue(13, $funcao);
                $sql->bindValue(14, $categoria);
                $sql->bindValue(15, $periodo);
                $sql->bindValue(16, $_SESSION['id']);
                $sql->bindValue(17, $id);
                $sql->execute();
            } else {
                $sql = $db->prepare("UPDATE seg_usuario SET nome = ?, cpf = ?, login = ?, cidade_id = ?, nascimento = ?, email = ?, celular = ?, contato = ?, rua = ?, numero = ?, bairro = ?, funcao_id = ?, categoria_id = ?, periodo = ?, usuario_id = ? WHERE id = ?");
                $sql->bindValue(1, $nome);
                $sql->bindValue(2, $cpf);
                $sql->bindValue(3, $login);
                $sql->bindValue(4, $municipio_id);
                $sql->bindValue(5, $nascimento);
                $sql->bindValue(6, $email);
                $sql->bindValue(7, $celular);
                $sql->bindValue(8, $fixo);
                $sql->bindValue(9, $rua);
                $sql->bindValue(10, $numero);
                $sql->bindValue(11, $bairro);
                $sql->bindValue(12, $funcao);
                $sql->bindValue(13, $categoria);
                $sql->bindValue(14, $periodo);
                $sql->bindValue(15, $_SESSION['id']);
                $sql->bindValue(16, $id);
                $sql->execute();
            }
        }

        //PERMISSÕES DE ACESSO AO SISTEMA
        if (ver_nivel(1) || ver_nivel(3)) {//ADMIN OU SUPER ADMIN
            //Removendo Níveis de Acesso
            $stmt3 = $db->prepare("DELETE FROM seg_permissoes WHERE user_id = ?");
            $stmt3->bindValue(1, $_POST['id']);
            $stmt3->execute();

            //Adicionando Níveis de Acesso
            if (isset($nivel)) {
                foreach ($nivel AS $key => $val) {
                    if (isset($val)) {
                        $stmt33 = $db->prepare("INSERT INTO seg_permissoes (user_id, nivel) VALUES (?, ?)");
                        $stmt33->bindValue(1, $id);
                        $stmt33->bindValue(2, $val);
                        $stmt33->execute();
                    }
                }
            }
        }

        if (isset($_SESSION['foto_cut']) && isset($_SESSION['foto_origin']) && $_SESSION['foto_cut'] != "" && $_SESSION['foto_origin'] != "") {
            $stmt5 = $db->prepare("UPDATE seg_usuario SET foto = ? WHERE id = ?");
            $stmt5->bindValue(1, PORTAL_URL . "" . $_SESSION['foto_cut']);
            $stmt5->bindValue(2, $id);
            $stmt5->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>