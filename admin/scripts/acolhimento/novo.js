//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $("img#click_foto").click(function () {
        $("div#div_clicado").click();
    });

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("#nome_profissional").change(function () {

        var funcao = $(this).find('option:selected').attr('funcao');
        var setor = $(this).find('option:selected').attr('rel');
        var registro = $(this).find('option:selected').attr('registro');

        $("input#funcao_profissional").val(funcao);
        $("input#registro_classe").val(registro);
        $("input#setor_profissional").val(setor);

    });

    //COMBO ESTADO E MUNICIPIO
    $("select#estado").change(function () {
        $("select#cidade").html('<option value="0">Carregando...</option>');
        $.post(PORTAL_URL + "utils/combo_cidades.php",
                {estado: $(this).val()},
                function (valor) {
                    $("select#cidade").html(valor);
                    $("select#cidade").select2();
                });
    });


    $("#possui_sim").click(function () {
        $("#div_deficiencia").show();
    });

    $("#possui_nao").click(function () {
        $("#div_deficiencia").hide();
    });

    $("#sim_tratamento").click(function () {
        $("#div_qual_tratamento").show();
    });

    $("#nao_tratamento").click(function () {
        $("#div_qual_tratamento").hide();
    });

    $("#sim_doenca").click(function () {
        $("#div_infectocontagiosa").show();
    });

    $("#nao_doenca").click(function () {
        $("#div_infectocontagiosa").hide();
    });

    $("#sim_medicacoes").click(function () {
        $("#div_medicacao_crianca").show();
    });

    $("#nao_medicacoes").click(function () {
        $("#div_medicacao_crianca").hide();
        $("select#qual_medicamentos").val("");
        $("select#qual_medicamentos").select2();
    });

    $('#form_acolhimento').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var quem_trouxe = $('#juizado:checked').val() == 1 ? 1 : ($('#conselho_tutelar:checked').val() == 2 ? 2 : ($('#policia_militar:checked').val() == 3 ? 3 : ($('#1conselho:checked').val() == 4 ? 4 : ($('#2conselho:checked').val() == 5 ? 5 : ($('#3conselho:checked').val() == 6 ? 6 : ($('#conselhos_interior:checked').val() == 7 ? 7 : 0))))));
            var responsavel_entrega = $("#responsavel_entrega").val();
            var funcao_responsavel = $("#funcao_responsavel").val();
            var contato_responsavel = $("#contato_responsavel").val();
            var data_entrada = $("#data_entrada").val();
            var hora_entrada = $("#hora_entrada").val();
            var profissional = $("#nome_profissional").val();
            var observacao_acolhimento = $("#observacao_acolhimento").val();
            var motivo_aba = $("#abandono:checked").val() == 1 ? 1 : 0;
            var motivo_neg = $("#negligencia:checked").val() == 1 ? 1 : 0;
            var motivo_abu = $("#abuso_sexual:checked").val() == 1 ? 1 : 0;
            var motivo_out = $("#outros:checked").val() == 1 ? 1 : 0;
            var descricao = $("#descricao").val();
            var numero_guia_acolhimento = $("#numero_guia_acolhimento").val();
            var numero_processo = $("#numero_processo").val();
            var numero_destituicao = $("#numero_destituicao").val();
            var crianca_nome = $("#crianca_nome").val();
            var sexo = $("#masculino:checked").val() == 1 ? 1 : ($("#feminino:checked").val() == 2 ? 2 : 0);
            var nascimento = $("#nascimento").val();
            var nacionalidade = $("#nacionalidade").val();
            var naturalidade = $("#naturalidade").val();
            var religiao = $("#religiao").val();
            var cor = $("#branca:checked").val() == 1 ? 1 : ($("#preta:checked").val() == 2 ? 2 : ($("#parda:checked").val() == 3 ? 3 : ($("#amarela:checked").val() == 4 ? 4 : ($("#indigina:checked").val() == 5 ? 5 : 0))));
            var nome_pai = $("#nome_pai").val();
            var nome_mae = $("#nome_mae").val();
            var qual_deficiencia = $("#qual_deficiencia").val();
            var recursos = $("#recursos").val();
            var qual_tratamento = $("#qual_tratamento").val();
            var disturbio = $("#sim_indicios:checked").val() == 1 ? 1 : ($("#nao_indicios:checked").val() == 2 ? 2 : 0);
            var qual_doenca_infectocontagiosa = $("#qual_doenca_infectocontagiosa").val();
            var qual_medicamentos = $("#qual_medicamentos").val();
            var obs_saude = $("#obs_saude").val();
            var rua = $("#rua").val();
            var numero = $("#numero").val();
            var bairro = $("#bairro").val();
            var estado = $("#estado").val();
            var cidade = $("#cidade").val();
            var comarca_interior = $("#comarca_interior").val();

            $.post(PORTAL_URL + "admin/dao/acolhimento/novo.php", {id: id, comarca_interior: comarca_interior, cidade: cidade, estado: estado, bairro: bairro, numero: numero, rua: rua, obs_saude: obs_saude,
                qual_medicamentos: qual_medicamentos, qual_doenca_infectocontagiosa: qual_doenca_infectocontagiosa, disturbio: disturbio, qual_deficiencia: qual_deficiencia,
                recursos: recursos, qual_tratamento: qual_tratamento, cor: cor, nome_pai: nome_pai, nome_mae: nome_mae, religiao: religiao, naturalidade: naturalidade,
                nacionalidade: nacionalidade, nascimento: nascimento, motivo_neg: motivo_neg, motivo_abu: motivo_abu, motivo_out: motivo_out,
                crianca_nome: crianca_nome, numero_destituicao: numero_destituicao, numero_processo: numero_processo, numero_guia_acolhimento: numero_guia_acolhimento,
                sexo: sexo, descricao: descricao, motivo_aba: motivo_aba, observacao_acolhimento: observacao_acolhimento, data_entrada: data_entrada, hora_entrada: hora_entrada,
                profissional: profissional, quem_trouxe: quem_trouxe, responsavel_entrega: responsavel_entrega,
                funcao_responsavel: funcao_responsavel, contato_responsavel: contato_responsavel}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Acolhimento",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Acolhimento",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/acolhimento/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var responsavel_entrega = $("#responsavel_entrega").val();
    var funcao_responsavel = $("#funcao_responsavel").val();
    var contato_responsavel = $("#contato_responsavel").val();
    var data_entrada = $("#data_entrada").val();
    var hora_entrada = $("#hora_entrada").val();
    var profissional = $("#nome_profissional").val();
    var numero_guia_acolhimento = $("#numero_guia_acolhimento").val();
    var numero_processo = $("#numero_processo").val();
    var nascimento = $("#nascimento").val();
    var nacionalidade = $("#nacionalidade").val();
    var naturalidade = $("#naturalidade").val();
    var crianca_nome = $("#crianca_nome").val();
    var comarca_interior = $("#comarca_interior").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (crianca_nome == "") {
            $('div#div_crianca_nome').after('<label id="erro_crianca_nome" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_crianca_nome');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nacionalidade == "") {
            $('div#div_nacionalidade').after('<label id="erro_nacionalidade" class="error">Nacionalidade é obrigatório.</label>');
            valido = false;
            element = $('div#div_nacionalidade');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (profissional == "") {
            $('div#div_nome_profissional').after('<label id="erro_nome_profissional" class="error">Profissional é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_profissional');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (hora_entrada == "") {
            $('div#div_hora_entrada').after('<label id="erro_hora_entrada" class="error">Hora da entrada é obrigatório.</label>');
            valido = false;
            element = $('div#div_hora_entrada');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (data_entrada == "") {
            $('div#div_data_entrada').after('<label id="erro_data_entrada" class="error">Data entrada é obrigatório.</label>');
            valido = false;
            element = $('div#div_data_entrada');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (contato_responsavel == "") {
            $('div#div_contato_responsavel').after('<label id="erro_contato_responsavel" class="error">Contato é obrigatório.</label>');
            valido = false;
            element = $('div#div_contato_responsavel');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (funcao_responsavel == "") {
            $('div#div_funcao_responsavel').after('<label id="erro_funcao_responsavel" class="error">Função é obrigatório.</label>');
            valido = false;
            element = $('div#div_funcao_responsavel');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (responsavel_entrega == "") {
            $('div#div_responsavel_entrega').after('<label id="erro_responsavel_entrega" class="error">Responsável é obrigatório.</label>');
            valido = false;
            element = $('div#div_responsavel_entrega');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (comarca_interior == "") {
            $('div#div_comarca_interior').after('<label id="erro_comarca_interior" class="error">Comarca é obrigatório.</label>');
            valido = false;
            element = $('div#div_comarca_interior');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------