<?php
include_once('conf/config.php');
include_once('utils/funcoes.php');
$db = Conexao::getInstance();
?>

<!-- App css -->
<link href="<?= PORTAL_URL; ?>assets/css/style.css" rel="stylesheet" type="text/css">

<br/>

<div id="button_top" class="scroll-top">
    <button><i class="fal fa-arrow-up"></i></button>
</div>

<section class="text">
    <div class="container">
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner"></div>
            </div>
        </div>
        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">
            <div class="card">
                <div class="card-block">
                    <div class="ex-page-content text-center">
                        <h1 class="">404!</h1>
                        <h4 class="">Desculpe, página não encontrada</h4>
                        <br><a class="btn btn-info mb-5 waves-effect waves-light" href="javascript:window.history.go(-1)"><i class="mdi mdi-home"></i> Voltar</a></div>
                </div>
            </div>
        </div>
	</div>
</section>	

<br/>