<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT * 
                 FROM seg_funcao sf  
                 WHERE sf.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_funcao = $result->fetch(PDO::FETCH_ASSOC);

    $funcao_id = $dados_funcao['id'];
    $funcao_nome = $dados_funcao['nome'];
    $funcao_setor = $dados_funcao['setor_id'];
} else {
    $funcao_id = "";
    $funcao_nome = "";
    $funcao_setor = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/funcoes/lista">Funções</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_funcao" name="form_funcao" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $funcao_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-id-card"></i> <strong>NOVA FUNÇÃO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_nome" class="form-group">
                                    <label for="funcao">FUNÇÃO</label>
                                    <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome da Função" value="<?= $funcao_nome ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="div_setor" class="form-group">
                                    <label for="setor">SETOR</label>
                                    <select name="setor" id="setor" class="form-control select2">
                                        <option value="">Selecione o setor</option>
                                        <?php
                                        $result8 = $db->prepare("SELECT nome, id 
                                                     FROM seg_setor
                                                     WHERE status = 1 
                                                     ORDER BY nome ASC");
                                        $result8->execute();
                                        while ($setor = $result8->fetch(PDO::FETCH_ASSOC)) {
                                            if ($funcao_setor == $setor['id']) {
                                                ?>
                                                <option selected="true" value='<?= $setor['id']; ?>'><?= $setor['nome']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value='<?= $setor['id']; ?>'><?= $setor['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $funcao_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $funcao_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/funcoes/novo.js"></script>