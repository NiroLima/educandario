<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
        FROM mod_acolhimento ma   
        WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result22 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result22->bindValue(1, $acolhimento_id);
    $result22->execute();
    $resultado22 = $result22->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_id = $resultado22['id'];
    $acolhimento_responsavel = $resultado22['usuario_id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_plano macp   
        WHERE macp.acolhimento_crianca_id = ?");
    $result2->bindValue(1, $acolhimento_crianca_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $plano_id = $resultado2['id'];
    $vf = $resultado2['vf'] == "" ? 0 : $resultado2['vf'];
} else {
    $acolhimento_id = "";
    $plano_id = "";
    $acolhimento_crianca_id = "";
    $acolhimento_responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
    $vf = 0;
}
//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);
//------------------------------------------------------------------------------
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <nav class="menu-acesso-rapido">
            <a href="#" class="open-menu"><i class="fal fa-chevron-left"></i></a>
            <div class="scroll">
                <h4 class="para-familia"><i class="fal fa-home-heart"></i> PARA A FAMÍLIA</h4>
                <ul class="links">
                    <li><a href="#documentacao_familia">DOCUMENTAÇÃO</a></li>
                    <li><a href="#saude_familia">SAÚDE</a></li>
                    <li><a href="#acesso_familia">ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA</a></li>
                    <li><a href="#participacao_familia">PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS</a></li>
                    <li><a href="#capacitacao_familia">CAPACITAÇÃO PROFISSIONAL/INSERÇÃO NO MUNDO DO TRABALHO</a></li>
                    <li><a href="#cuidado_familia">CUIDADO COM A CRIANÇA</a></li>
                    <li><a href="#outros_familia">OUTROS</a></li>
                </ul>
                <h4 class="para-crianca"><i class="fal fa-child"></i> PARA A CRIANÇA</h4>
                <ul class="links">
                    <li><a href="#documentacao_crianca">DOCUMENTAÇÃO</a></li>
                    <li><a href="#saude_crianca">SAÚDE</a></li>
                    <li><a href="#educacao_crianca">EDUCAÇÃO</a></li>
                    <li><a href="#esporte_crianca">ESPORTE/CULTURA/LAZER</a></li>
                    <li><a href="#convivencia_crianca">CONVIVÊNCIA FAMILIAR</a></li>
                    <li><a href="#acresso_crianca">ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA</a></li>
                    <li><a href="#participacao_crianca">PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS</a></li>
                    <li><a href="#outros_crianca">OUTROS</a></li>
                </ul>
            </div>
        </nav>
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/pia/lista">PIA</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="text-center">PLANO INDIVIDUAL DE ATENDIMENTO - PIA</h4>

        <!-- Main content -->
        <section class="content">

            <!-- Step wizard -->
            <div id="impressao" class="box box-solid bg-info">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="box-title">PIA</h4>
                            <h6 class="box-subtitle">PLANO INDIVIDUAL DE ATENDIMENTO</h6>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-right"><?= $barra; ?>%</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $barra; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $barra; ?>%">
                                    <span class="sr-only"><?= $barra; ?>% Complete</span>
                                </div>
                            </div>
                        </div>      
                    </div>  
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="box-title">RESPONSÁVEL DO PIA</h4>

                            <div id="responsavel_edite_1">
                                <h6 class="subtitle"><?= $nome_responsavel; ?> <a href="#" class="edit-responsavel"><i onclick="editar_responsavel()" class="fal fa-pencil-alt"></i></a></h6>
                            </div>

                            <div id="responsavel_edite_2" style="display: none">
                                <form id="form_responsavel" name="form_responsavel" method="POST" action="#" class="tab-wizard wizard-circle">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required="true" id="responsavel_id" name="responsavel_id">
                                                <option value="">Selecione o responsável</option>
                                                <?php
                                                $result = $db->prepare("SELECT su.id, su.nome           
                                                            FROM seg_usuario su    
                                                            WHERE su.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_responsavel == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button href="#" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="div_validacao" class="col-md-2 text-right">
                            <a <?= is_numeric($vf) && $vf == 0 ? "" : "style='display: none'"; ?> href="#" onclick="validar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-success mt-2"><i class="fas fa-check"></i> VALIDAR</a>
                            <a <?= is_numeric($vf) && $vf == 1 ? "" : "style='display: none'"; ?> href="#" onclick="desvalidar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-warning mt-2"><i class="fas fa-remove"></i> DESVALIDAR</a>
                        </div>
                        <div class="col-md-2 text-right">
                            <a onclick="gerar_pdf()" style="cursor: pointer" class="btn-arquivo mt-2"><i class="fas fa-file-pdf"></i> GERAR PDF</a>
                        </div>
                    </div> 
                </div>
                <!-- /.box-header -->
                <div class="box-body wizard-content">

                    <?php include 'layout/menu_wizard.php'; ?>

                    <br/>

                    <form id="form_plano" name="form_plano" method="POST" action="#" class="tab-wizard wizard-circle">

                        <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>
                        <input type="hidden" id="acolhimento_crianca_id" name="acolhimento_crianca_id" value="<?= $acolhimento_crianca_id; ?>"/>
                        <input type="hidden" id="plano_id" name="plano_id" value="<?= $plano_id; ?>"/>
                        <!-- Step 7 -->
                        <section>
                            <div class="box box-solid bg-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title mb-0"><i class="fal fa-home-heart"></i> PARA A FAMÍLIA</h4>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <!-- BOX DE DOCUMENTAÇÃO -->
                                            <div class="box box-outline-primary" id="documentacao_familia">
                                                <div class="box-header">
                                                    <strong>DOCUMENTAÇÃO</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 1");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar('', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover('', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id" name="documentacao_id" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento" id="descreva_objetivo_monitoramento" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao" class="form-group">
                                                                <label for="descreva_acao">AÇÃO</label>
                                                                <textarea name="descreva_acao" id="descreva_acao" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel" class="form-group">
                                                                <label for="responsavel">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel" id="responsavel" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial" class="form-group">
                                                                <label for="prazo_inicial">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial" id="prazo_inicial" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final" class="form-group">
                                                                <label for="prazo_final">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final" id="prazo_final" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada" name="acao_realizada" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada" name="objetivo_alcancada" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada" name="finalizada" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo" name="redefinir_prazo" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao" class="form-group">
                                                                <label for="descreva_observacao">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao" id="descreva_observacao" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_documentacao" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE DOCUMENTAÇÃO -->

                                            <br>

                                            <!-- BOX DE SAÚDE -->
                                            <div class="box box-outline-primary" id="saude_familia">
                                                <div class="box-header">
                                                    <strong>SAÚDE</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao2" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 2");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao2" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar('2', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover('2', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento2" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento2">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id2" name="documentacao_id2" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento2" id="descreva_objetivo_monitoramento2" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao2" class="form-group">
                                                                <label for="descreva_acao2">AÇÃO</label>
                                                                <textarea name="descreva_acao2" id="descreva_acao2" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel2" class="form-group">
                                                                <label for="responsavel2">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel2" id="responsavel2" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial2" class="form-group">
                                                                <label for="prazo_inicial2">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial2" id="prazo_inicial2" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final2" class="form-group">
                                                                <label for="prazo_final2">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final2" id="prazo_final2" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada2" name="acao_realizada2" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada2">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada2" name="objetivo_alcancada2" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada2">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada2" name="finalizada2" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada2">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo2" name="redefinir_prazo2" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo2">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao2" class="form-group">
                                                                <label for="descreva_observacao2">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao2" id="descreva_observacao2" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_documentacao2" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <!-- FIM BOX DE SAÚDE -->

                                            <br>

                                            <!-- BOX DE ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA -->
                                            <div class="box box-outline-primary" id="acesso_familia">
                                                <div class="box-header">
                                                    <strong>ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao3" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 3");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao3" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar('3', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover('3', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento3" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento3">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id3" name="documentacao_id3" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento3" id="descreva_objetivo_monitoramento3" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao3" class="form-group">
                                                                <label for="descreva_acao3">AÇÃO</label>
                                                                <textarea name="descreva_acao3" id="descreva_acao3" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel3" class="form-group">
                                                                <label for="responsavel3">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel3" id="responsavel3" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial3" class="form-group">
                                                                <label for="prazo_inicial3">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial3" id="prazo_inicial3" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final3" class="form-group">
                                                                <label for="prazo_final3">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final3" id="prazo_final3" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada3" name="acao_realizada3" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada3">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada3" name="objetivo_alcancada3" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada3">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada3" name="finalizada3" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada3">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo3" name="redefinir_prazo3" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo3">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao3" class="form-group">
                                                                <label for="descreva_observacao3">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao3" id="descreva_observacao3" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_documentacao3" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA -->

                                            <br>

                                            <!-- BOX DE PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS -->
                                            <div class="box box-outline-primary" id="participacao_familia">
                                                <div class="box-header">
                                                    <strong>PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao4" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 4");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao4" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar('4', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover('4', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento4" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento4">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id4" name="documentacao_id4" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento4" id="descreva_objetivo_monitoramento4" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao4" class="form-group">
                                                                <label for="descreva_acao4">AÇÃO</label>
                                                                <textarea name="descreva_acao4" id="descreva_acao4" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel4" class="form-group">
                                                                <label for="responsavel4">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel4" id="responsavel4" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial4" class="form-group">
                                                                <label for="prazo_inicial4">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial4" id="prazo_inicial4" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final4" class="form-group">
                                                                <label for="prazo_final4">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final4" id="prazo_final4" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada4" name="acao_realizada4" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada4">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada4" name="objetivo_alcancada4" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada4">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada4" name="finalizada4" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada4">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo4" name="redefinir_prazo4" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo4">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao4" class="form-group">
                                                                <label for="descreva_observacao4">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao4" id="descreva_observacao4" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_documentacao4" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS -->

                                            <br>

                                            <!-- BOX DE CAPACITAÇÃO PROFISSIONAL/INSERÇÃO NO MUNDO DO TRABALHO -->
                                            <div class="box box-outline-primary" id="capacitacao_familia">
                                                <div class="box-header">
                                                    <strong>CAPACITAÇÃO PROFISSIONAL/INSERÇÃO NO MUNDO DO TRABALHO</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao5" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 5");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao5" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar('5', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover('5', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento5" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento5">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id5" name="documentacao_id5" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento5" id="descreva_objetivo_monitoramento5" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao5" class="form-group">
                                                                <label for="descreva_acao5">AÇÃO</label>
                                                                <textarea name="descreva_acao5" id="descreva_acao5" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel5" class="form-group">
                                                                <label for="responsavel5">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel5" id="responsavel5" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial5" class="form-group">
                                                                <label for="prazo_inicial5">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial5" id="prazo_inicial5" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final5" class="form-group">
                                                                <label for="prazo_final5">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final5" id="prazo_final5" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada5" name="acao_realizada5" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada5">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada5" name="objetivo_alcancada5" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada5">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada5" name="finalizada5" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada5">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo5" name="redefinir_prazo5" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo5">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao5" class="form-group">
                                                                <label for="descreva_observacao5">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao5" id="descreva_observacao5" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_documentacao5" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE CAPACITAÇÃO PROFISSIONAL/INSERÇÃO NO MUNDO DO TRABALHO -->

                                            <br>

                                            <!-- BOX DE CUIDADO COM A CRIANÇA -->
                                            <div class="box box-outline-primary" id="cuidado_familia">
                                                <div class="box-header">
                                                    <strong>CUIDADO COM A CRIANÇA</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao6" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 6");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao6" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar('6', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover('6', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento6" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento6">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id6" name="documentacao_id6" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento6" id="descreva_objetivo_monitoramento6" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao6" class="form-group">
                                                                <label for="descreva_acao6">AÇÃO</label>
                                                                <textarea name="descreva_acao6" id="descreva_acao6" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel6" class="form-group">
                                                                <label for="responsavel6">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel6" id="responsavel6" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial6" class="form-group">
                                                                <label for="prazo_inicial6">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial6" id="prazo_inicial6" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final6" class="form-group">
                                                                <label for="prazo_final6">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final6" id="prazo_final6" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada6" name="acao_realizada6" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada6">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada6" name="objetivo_alcancada6" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada6">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada6" name="finalizada6" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada6">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo6" name="redefinir_prazo6" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo6">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao6" class="form-group">
                                                                <label for="descreva_observacao6">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao6" id="descreva_observacao6" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_documentacao6" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE CUIDADO COM A CRIANÇA -->

                                            <br>

                                            <!-- BOX DE OUTRO -->
                                            <div class="box box-outline-primary" id="outros_familia">
                                                <div class="box-header">
                                                    <strong>OUTRO</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao7" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 7");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao7" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar('7', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover('7', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento7" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento7">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id7" name="documentacao_id7" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento7" id="descreva_objetivo_monitoramento7" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao7" class="form-group">
                                                                <label for="descreva_acao7">AÇÃO</label>
                                                                <textarea name="descreva_acao7" id="descreva_acao7" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel7" class="form-group">
                                                                <label for="responsavel7">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel7" id="responsavel7" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial7" class="form-group">
                                                                <label for="prazo_inicial7">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial7" id="prazo_inicial7" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final7" class="form-group">
                                                                <label for="prazo_final7">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final7" id="prazo_final7" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada7" name="acao_realizada7" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada7">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada7" name="objetivo_alcancada7" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada7">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada7" name="finalizada7" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada7">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo7" name="redefinir_prazo7" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo7">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao7" class="form-group">
                                                                <label for="descreva_observacao7">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao7" id="descreva_observacao7" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_documentacao7" class="btn btn-success">INSERIR</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE OUTRO -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIM PARA FAMÍLIA -->

                            <br>

                            <!-- INÍCIO PARA CRIANÇA -->
                            <div class="box box-solid bg-success">
                                <div class="box-header with-border">
                                    <h4 class="box-title mb-0"><i class="fal fa-child"></i> PARA A CRIANÇA</h4>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <!-- BOX DE DOCUMENTAÇÃO -->
                                            <div class="box box-outline-success" id="documentacao_crianca">
                                                <div class="box-header">
                                                    <strong>DOCUMENTAÇÃO</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao_crianca" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 1");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao_crianca" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar2('', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover2('', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento_crianca" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento_crianca">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id_crianca" name="documentacao_id_crianca" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento_crianca" id="descreva_objetivo_monitoramento_crianca" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao_crianca" class="form-group">
                                                                <label for="descreva_acao_crianca">AÇÃO</label>
                                                                <textarea name="descreva_acao_crianca" id="descreva_acao_crianca" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel_crianca" class="form-group">
                                                                <label for="responsavel_crianca">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel_crianca" id="responsavel_crianca" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial_crianca" class="form-group">
                                                                <label for="prazo_inicial_crianca">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial_crianca" id="prazo_inicial_crianca" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final_crianca" class="form-group">
                                                                <label for="prazo_final_crianca">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final_crianca" id="prazo_final_crianca" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada_crianca" name="acao_realizada_crianca" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada_crianca">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada_crianca" name="objetivo_alcancada_crianca" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada_crianca">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada_crianca" name="finalizada_crianca" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada_crianca">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo_crianca" name="redefinir_prazo_crianca" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo_crianca">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao_crianca" class="form-group">
                                                                <label for="descreva_observacao_crianca">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao_crianca" id="descreva_observacao_crianca" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_crianca" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE DOCUMENTAÇÃO -->

                                            <br>

                                            <!-- BOX DE SAÚDE -->
                                            <div class="box box-outline-success" id="saude_crianca">
                                                <div class="box-header">
                                                    <strong>SAÚDE</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao_crianca2" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 2");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao_crianca2" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar2('2', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover2('2', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento_crianca2" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento_crianca2">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id_crianca2" name="documentacao_id_crianca2" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento_crianca2" id="descreva_objetivo_monitoramento_crianca2" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao_crianca2" class="form-group">
                                                                <label for="descreva_acao_crianca2">AÇÃO</label>
                                                                <textarea name="descreva_acao_crianca2" id="descreva_acao_crianca2" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel_crianca2" class="form-group">
                                                                <label for="responsavel_crianca2">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel_crianca2" id="responsavel_crianca2" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial_crianca2" class="form-group">
                                                                <label for="prazo_inicial_crianca2">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial_crianca2" id="prazo_inicial_crianca2" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final_crianca2" class="form-group">
                                                                <label for="prazo_final_crianca2">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final_crianca2" id="prazo_final_crianca2" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada_crianca2" name="acao_realizada_crianca2" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada_crianca2">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada_crianca2" name="objetivo_alcancada_crianca2" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada_crianca2">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada_crianca2" name="finalizada_crianca2" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada_crianca2">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo_crianca2" name="redefinir_prazo_crianca2" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo_crianca2">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao_crianca2" class="form-group">
                                                                <label for="descreva_observacao_crianca2">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao_crianca2" id="descreva_observacao_crianca2" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_crianca2" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE SAÚDE -->

                                            <br>

                                            <!-- BOX DE EDUCAÇÃO -->
                                            <div class="box box-outline-success" id="educacao_crianca">
                                                <div class="box-header">
                                                    <strong>EDUCAÇÃO</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao_crianca8" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 8");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao_crianca8" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar2('8', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover2('8', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento_crianca8" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento_crianca8">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id_crianca8" name="documentacao_id_crianca8" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento_crianca8" id="descreva_objetivo_monitoramento_crianca8" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao_crianca8" class="form-group">
                                                                <label for="descreva_acao_crianca8">AÇÃO</label>
                                                                <textarea name="descreva_acao_crianca8" id="descreva_acao_crianca8" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel_crianca8" class="form-group">
                                                                <label for="responsavel_crianca8">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel_crianca8" id="responsavel_crianca8" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial_crianca8" class="form-group">
                                                                <label for="prazo_inicial_crianca8">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial_crianca8" id="prazo_inicial_crianca8" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final_crianca8" class="form-group">
                                                                <label for="prazo_final_crianca8">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final_crianca8" id="prazo_final_crianca8" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada_crianca8" name="acao_realizada_crianca8" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada_crianca8">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada_crianca8" name="objetivo_alcancada_crianca8" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada_crianca8">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada_crianca8" name="finalizada_crianca8" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada_crianca8">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo_crianca8" name="redefinir_prazo_crianca8" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo_crianca8">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao_crianca8" class="form-group">
                                                                <label for="descreva_observacao_crianca8">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao_crianca8" id="descreva_observacao_crianca8" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_crianca8" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE EDUCAÇÃO -->

                                            <br>

                                            <!-- BOX DE ESPORTE/CULTURA/LAZER -->
                                            <div class="box box-outline-success" id="esporte_crianca">
                                                <div class="box-header">
                                                    <strong>ESPORTE/CULTURA/LAZER</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao_crianca9" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 9");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao_crianca9" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar2('9', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover2('9', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento_crianca9" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento_crianca9">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id_crianca9" name="documentacao_id_crianca9" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento_crianca9" id="descreva_objetivo_monitoramento_crianca9" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao_crianca9" class="form-group">
                                                                <label for="descreva_acao_crianca9">AÇÃO</label>
                                                                <textarea name="descreva_acao_crianca9" id="descreva_acao_crianca9" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel_crianca9" class="form-group">
                                                                <label for="responsavel_crianca9">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel_crianca9" id="responsavel_crianca9" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial_crianca9" class="form-group">
                                                                <label for="prazo_inicial_crianca9">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial_crianca9" id="prazo_inicial_crianca9" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final_crianca9" class="form-group">
                                                                <label for="prazo_final_crianca9">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final_crianca9" id="prazo_final_crianca9" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada_crianca9" name="acao_realizada_crianca9" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada_crianca9">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada_crianca9" name="objetivo_alcancada_crianca9" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada_crianca9">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada_crianca9" name="finalizada_crianca9" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada_crianca9">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo_crianca9" name="redefinir_prazo_crianca9" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo_crianca9">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao_crianca9" class="form-group">
                                                                <label for="descreva_observacao_crianca9">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao_crianca9" id="descreva_observacao_crianca9" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_crianca9" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE ESPORTE/CULTURA/LAZER -->

                                            <br>

                                            <!-- BOX DE CONVIVÊNCIA FAMILIAR -->
                                            <div class="box box-outline-success" id="convivencia_crianca">
                                                <div class="box-header">
                                                    <strong>CONVIVÊNCIA FAMILIAR</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao_crianca10" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 10");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao_crianca10" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar2('10', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover2('10', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento_crianca10" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento_crianca10">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id_crianca10" name="documentacao_id_crianca10" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento_crianca10" id="descreva_objetivo_monitoramento_crianca10" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao_crianca10" class="form-group">
                                                                <label for="descreva_acao_crianca10">AÇÃO</label>
                                                                <textarea name="descreva_acao_crianca10" id="descreva_acao_crianca10" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel_crianca10" class="form-group">
                                                                <label for="responsavel_crianca10">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel_crianca10" id="responsavel_crianca10" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial_crianca10" class="form-group">
                                                                <label for="prazo_inicial_crianca10">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial_crianca10" id="prazo_inicial_crianca10" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final_crianca10" class="form-group">
                                                                <label for="prazo_final_crianca10">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final_crianca10" id="prazo_final_crianca10" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada_crianca10" name="acao_realizada_crianca10" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada_crianca10">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada_crianca10" name="objetivo_alcancada_crianca10" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada_crianca10">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada_crianca10" name="finalizada_crianca10" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada_crianca10">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo_crianca10" name="redefinir_prazo_crianca10" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo_crianca10">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao_crianca10" class="form-group">
                                                                <label for="descreva_observacao_crianca10">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao_crianca10" id="descreva_observacao_crianca10" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_crianca10" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE CONVIVÊNCIA FAMILIAR -->

                                            <br>

                                            <!-- BOX DE ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA -->
                                            <div class="box box-outline-success" id="acesso_crianca">
                                                <div class="box-header">
                                                    <strong>ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao_crianca3" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 3");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao_crianca3" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar2('3', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover2('3', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento_crianca3" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento_crianca3">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id_crianca3" name="documentacao_id_crianca3" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento_crianca3" id="descreva_objetivo_monitoramento_crianca3" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao_crianca3" class="form-group">
                                                                <label for="descreva_acao_crianca3">AÇÃO</label>
                                                                <textarea name="descreva_acao_crianca3" id="descreva_acao_crianca3" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel_crianca3" class="form-group">
                                                                <label for="responsavel_crianca3">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel_crianca3" id="responsavel_crianca3" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial_crianca3" class="form-group">
                                                                <label for="prazo_inicial_crianca3">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial_crianca3" id="prazo_inicial_crianca3" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final_crianca3" class="form-group">
                                                                <label for="prazo_final_crianca3">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final_crianca3" id="prazo_final_crianca3" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada_crianca3" name="acao_realizada_crianca3" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada_crianca3">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada_crianca3" name="objetivo_alcancada_crianca3" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada_crianca3">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada_crianca3" name="finalizada_crianca3" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada_crianca3">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo_crianca3" name="redefinir_prazo_crianca3" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo_crianca3">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao_crianca3" class="form-group">
                                                                <label for="descreva_observacao_crianca3">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao_crianca3" id="descreva_observacao_crianca3" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_crianca3" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA -->

                                            <br>

                                            <!-- BOX DE PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS -->
                                            <div class="box box-outline-success" id="participacao_crianca">
                                                <div class="box-header">
                                                    <strong>PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao_crianca4" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 4");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao_crianca4" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar2('4', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover2('4', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento_crianca4" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento_crianca4">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id_crianca4" name="documentacao_id_crianca4" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento_crianca4" id="descreva_objetivo_monitoramento_crianca4" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao_crianca4" class="form-group">
                                                                <label for="descreva_acao_crianca4">AÇÃO</label>
                                                                <textarea name="descreva_acao_crianca4" id="descreva_acao_crianca4" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel_crianca4" class="form-group">
                                                                <label for="responsavel_crianca4">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel_crianca4" id="responsavel_crianca4" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial_crianca4" class="form-group">
                                                                <label for="prazo_inicial_crianca4">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial_crianca4" id="prazo_inicial_crianca4" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final_crianca4" class="form-group">
                                                                <label for="prazo_final_crianca4">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final_crianca4" id="prazo_final_crianca4" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada_crianca4" name="acao_realizada_crianca4" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada_crianca4">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada_crianca4" name="objetivo_alcancada_crianca4" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada_crianca4">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada_crianca4" name="finalizada_crianca4" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada_crianca4">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo_crianca4" name="redefinir_prazo_crianca4" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo_crianca4">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao_crianca4" class="form-group">
                                                                <label for="descreva_observacao_crianca4">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao_crianca4" id="descreva_observacao_crianca4" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_crianca4" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS -->

                                            <br>

                                            <!-- BOX DE OUTRO -->
                                            <div class="box box-outline-success" id="outros_crianca">
                                                <div class="box-header">
                                                    <strong>OUTRO</strong>
                                                    <h6 class="box-subtitle">Monitoramento: Sim ou Não. Observação: caso haja necessidade de redefinição dos prazos, inserir novamente a informação no quadro para manter o histórico das ações</h6>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row mt-3">
                                                        <div id="resultado_documentacao_crianca7" class="col-md-12">

                                                            <?php
                                                            $result = $db->prepare("SELECT *           
                                                                                        FROM mod_acolhimento_crianca_plano macp   
                                                                                        WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 7");
                                                            $result->bindValue(1, $acolhimento_crianca_id);
                                                            $result->execute();
                                                            while ($document = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <!-- CARD MONITORAMENTO -->
                                                                <div id="div_remover_documentacao_crianca7" class="box box-outline-primary">
                                                                    <div class="box-body">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-monitoramento mb-2">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBJETIVO</strong>
                                                                                            <p><?= $document['objetivo']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">AÇÃO</strong>
                                                                                            <p><?= $document['acao']; ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <strong class="text-primary">RESPONSÁVEL</strong>
                                                                                            <p><?= $document['responsavel']; ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO INICIAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_inicial']); ?></p>
                                                                                        </td>
                                                                                        <td width="15%">
                                                                                            <strong class="text-primary">PRAZO FINAL</strong>
                                                                                            <p><?= obterDataBRTimestamp($document['prazo_final']); ?></p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">MONITORAMENTO</strong>
                                                                                            <p <?= $document['realizada'] == 1 ? "" : "style='display: none'" ?>>Ação realizada</p>
                                                                                            <p <?= $document['alcancado'] == 1 ? "" : "style='display: none'" ?>>Objetivo alcançado</p>
                                                                                            <p <?= $document['finalizada'] == 1 ? "" : "style='display: none'" ?>>Pode ser finalizada</p>
                                                                                            <p <?= $document['prazo'] == 1 ? "" : "style='display: none'" ?>>Redefinir prazo</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3">
                                                                                            <strong class="text-primary">OBSERVAÇÃO(ES)</strong>
                                                                                            <p><?= $document['obs']; ?></p>
                                                                                        </td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-footer">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <a style="cursor: pointer" onclick="editar2('7', <?= $document['id']; ?>, '<?= $document['objetivo'] ?>', '<?= $document['acao']; ?>', '<?= $document['responsavel']; ?>', '<?= $document['prazo_inicial']; ?>', '<?= $document['prazo_final']; ?>', '<?= $document['realizada']; ?>', '<?= $document['alcancado']; ?>', '<?= $document['finalizada']; ?>', '<?= $document['prazo']; ?>', '<?= $document['obs']; ?>')" class="btn btn-warning"><i class="fa fa-pencil"></i> EDITAR</a>
                                                                                <a style="cursor: pointer" onclick="remover2('7', this, <?= $document['id']; ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> APAGAR</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- FIM CARD DE MONITORAMENTO -->
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <br><hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_objetivo_monitoramento_crianca7" class="form-group">
                                                                <label for="descreva_objetivo_monitoramento_crianca7">OBJETIVO</label>
                                                                <input type="hidden" id="documentacao_id_crianca7" name="documentacao_id_crianca7" value=""/>
                                                                <textarea name="descreva_objetivo_monitoramento_crianca7" id="descreva_objetivo_monitoramento_crianca7" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_acao_crianca7" class="form-group">
                                                                <label for="descreva_acao_crianca7">AÇÃO</label>
                                                                <textarea name="descreva_acao_crianca7" id="descreva_acao_crianca7" class="form-control" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_responsavel_crianca7" class="form-group">
                                                                <label for="responsavel_crianca7">RESPONSÁVEL</label>
                                                                <input type="text" class="form-control" name="responsavel_crianca7" id="responsavel_crianca7" placeholder="Responsável" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_inicial_crianca7" class="form-group">
                                                                <label for="prazo_inicial_crianca7">PRAZO INICIAL</label>
                                                                <input type="date" class="form-control" name="prazo_inicial_crianca7" id="prazo_inicial_crianca7" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div id="div_prazo_final_crianca7" class="form-group">
                                                                <label for="prazo_final_crianca7">PRAZO FINAL</label>
                                                                <input type="date" class="form-control" name="prazo_final_crianca7" id="prazo_final_crianca7" placeholder="00/00/0000" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <div class="row mt-4 mb-4">
                                                        <div class="col-md-12">
                                                            <h6>MONITORAMENTO</h6>
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="acao_realizada_crianca7" name="acao_realizada_crianca7" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="acao_realizada_crianca7">AÇÃO REALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="objetivo_alcancada_crianca7" name="objetivo_alcancada_crianca7" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="objetivo_alcancada_crianca7">OBJETIVO ALCANÇADO</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="finalizada_crianca7" name="finalizada_crianca7" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="finalizada_crianca7">PODE SER FINALIZADA</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="checkbox" id="redefinir_prazo_crianca7" name="redefinir_prazo_crianca7" class="filled-in chk-col-primary" value="1"/>
                                                                    <label for="redefinir_prazo_crianca7">REDEFINIR PRAZO</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="div_descreva_observacao_crianca7" class="form-group">
                                                                <label for="descreva_observacao_crianca7">OBSERVAÇÃO(OES)</label>
                                                                <textarea name="descreva_observacao_crianca7" id="descreva_observacao_crianca7" class="form-control" cols="30" rows="10"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_crianca7" class="btn btn-success">INSERIR</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM BOX DE OUTRO -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JAVASCRIPT BÁSICOS -->
<script src="<?= PORTAL_URL ?>assets/vendor_components/jquery-steps-master/build/jquery.steps.js"></script>

<!-- JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.5.3/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/pia/plano.js"></script>