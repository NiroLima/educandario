<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
                            FROM mod_acolhimento_documentacao mad    
                            WHERE mad.acolhimento_id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $rg_numero = $resultado['rg_numero'];
    $rg_orgao_expedidor = $resultado['rg_orgao_expedidor'];
    $rg_estado = $resultado['rg_estado'];
    $rg_data_expedicao = $resultado['rg_data_expedicao'];
    $rg_anexo = $resultado['rg_anexo'];
    $cpf = $resultado['cpf'];
    $cpf_anexo = $resultado['cpf_anexo'];
    $certidao = $resultado['certidao'];
    $certidao_anexo = $resultado['certidao_anexo'];
    $vacinacao = $resultado['vacinacao'];
    $vacinacao_anexo = $resultado['vacinacao_anexo'];
    $historico_anexo = $resultado['historico_anexo'];
    $transferencia_anexo = $resultado['transferencia_anexo'];
    $prontuario_anexo = $resultado['prontuario_anexo'];
    $receituario_anexo = $resultado['receituario_anexo'];
    $mandado_anexo = $resultado['mandado_anexo'];
} else {
    $rg_numero = "";
    $rg_orgao_expedidor = "";
    $rg_estado = "";
    $rg_data_expedicao = "";
    $rg_anexo = "";
    $cpf = "";
    $cpf_anexo = "";
    $certidao = "";
    $certidao_anexo = "";
    $vacinacao = "";
    $vacinacao_anexo = "";
    $historico_anexo = "";
    $transferencia_anexo = "";
    $prontuario_anexo = "";
    $receituario_anexo = "";
    $mandado_anexo = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/geral/dahsboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/acolhimento/lista">Acolhimento</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Documentação</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_acolhimento_anexo" name="form_acolhimento_anexo" method="POST" action="<?= PORTAL_URL; ?>admin/view/acolhimento/novo2/<?= $id; ?>" enctype="multipart/form-data">
                <!-- DOCUMENTOS -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-address-card"></i> <strong>DOCUMENTOS - ANEXOS SOMENTE EM ARQUIVOS EM PDF</strong></h4>
                    </div>
                    <div class="box-body">
                        <!-- DOCUMENTOS NECESSÁRIOS -->
                        <div class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>DOCUMENTOS APRESENTADOS</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="checkbox" id="registro_geral" <?= $rg_numero != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="registro_geral">REGISTRO GERAL (RG)</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="cpf" <?= $cpf != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="cpf">CPF</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="certidao_nascimento" <?= $certidao != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="certidao_nascimento">CERTIDÃO DE NASCIMENTO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="carteira_vacinacao" <?= $vacinacao != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="carteira_vacinacao">CARTEIRA DE VACINAÇÃO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="historico_escolar" <?= $historico_anexo != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="historico_escolar">HISTÓRICO ESCOLAR</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="transferencia_escolar" <?= $transferencia_anexo != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="transferencia_escolar">TRANSFERÊNCIA ESCOLAR</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="prontuario_medico" <?= $prontuario_anexo != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="prontuario_medico">PRONTUÁRIO MÉDICO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="receituario_medico" <?= $receituario_anexo != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="receituario_medico">RECEITUÁRIO MÉDICO</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="checkbox" id="mandado_busca" <?= $mandado_anexo != "" ? "checked='true'" : ""; ?> class="filled-in chk-col-info" />
                                        <label for="mandado_busca">MANDADO DE BUSCA E APREENSÃO</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM DOCUMENTOS NECESSÁRIOS -->

                        <!-- DOCUMENTO - RG -->
                        <div id="div_rg" <?= $rg_numero != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>REGISTRO GERAL (RG)</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="rg_numero">NÚMERO DO RG</label>
                                            <input type="text" class="form-control" name="rg_numero" id="rg_numero" placeholder="Número" value="<?= $rg_numero; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="orgao_expedidor">ÓRGÃO EXPEDIDOR</label>
                                            <input type="text" class="form-control" name="orgao_expedidor" id="orgao_expedidor" placeholder="Órgão Expedidor" value="<?= $rg_orgao_expedidor; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="estado_expedidor">ESTADO</label>
                                            <input type="text" class="form-control" name="estado_expedidor" id="estado_expedidor" placeholder="Estado" value="<?= $rg_estado; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="expedicao">EXPEDIÇÃO</label>
                                            <input class="form-control" name="expedicao" id="expedicao" type="date" value="<?= $rg_data_expedicao; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO REGISTRO GERAL (RG)</label>
                                            <div class="custom-file mb-2">
                                                <input type="file" class="custom-file-input" id="rg_anexo" name="rg_anexo" value="<?= $rg_anexo; ?>">
                                                <label class="custom-file-label" for="rg_anexo"><?= $rg_anexo == "" ? "Selecionar arquivo em PDF" : $rg_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM DOCUMENTO - RG -->

                        <!-- CPF -->
                        <div id="div_cpf" <?= $cpf != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>CPF</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="numero_cpf">NÚMERO DO CPF</label>
                                            <input type="text" class="form-control" data-mask="999.999.999-99" name="numero_cpf" id="numero_cpf" placeholder="Número" value="<?= $cpf; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO CPF</label>
                                            <div class="custom-file mb-2">
                                                <input type="file" class="custom-file-input" id="cpf_anexo" name="cpf_anexo" value="<?= $cpf_anexo; ?>">
                                                <label class="custom-file-label" for="cpf_anexo"><?= $cpf_anexo == "" ? "Selecionar arquivo em PDF" : $cpf_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM CPF -->

                        <!-- CERTIDÃO DE NASCIMENTO -->
                        <div id="div_certidao" <?= $certidao != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>CERTIDÃO DE NASCIMENTO</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="numero_certidao">NÚMERO DA CERTIDÃO DE NASCIMENTO</label>
                                            <input type="text" class="form-control" name="numero_certidao" id="numero_certidao" placeholder="Número" value="<?= $certidao; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO CERTIDÃO DE NASCIMENTO</label>
                                            <div class="custom-file mb-2">
                                                <input type="file" class="custom-file-input" id="certidao_anexo" name="certidao_anexo" value="<?= $certidao_anexo; ?>">
                                                <label class="custom-file-label" for="certidao_anexo"><?= $certidao_anexo == "" ? "Selecionar arquivo em PDF" : $certidao_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM CERTIDÃO DE NASCIMENTO -->

                        <!-- CARTEIRA DE VACINACAO -->
                        <div id="div_vacinacao" <?= $vacinacao != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>CARTEIRA DE VACINAÇÃO</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="numero_vacinacao">NÚMERO DO CARTÃO DE VACINAÇÃO</label>
                                            <input type="text" class="form-control" name="numero_vacinacao" id="numero_vacinacao" placeholder="Número" value="<?= $vacinacao; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO CARTEIRA DE VACINAÇÃO</label>
                                            <div class="custom-file mb-2">
                                                <input type="file" class="custom-file-input" id="vacinacao_anexo" name="vacinacao_anexo" value="<?= $vacinacao_anexo; ?>">
                                                <label class="custom-file-label" for="vacinacao_anexo"><?= $vacinacao_anexo == "" ? "Selecionar arquivo em PDF" : $vacinacao_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM CARTEIRA DE VACINACAO -->

                        <!-- HISTÓRICO ESCOLAR -->
                        <div id="div_historico" <?= $historico_anexo != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>HISTÓRICO ESCOLAR</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO HISTÓRICO ESCOLAR</label>
                                            <div class="custom-file mb-2">
                                                <input type="hidden" id="guard_escolar" name="guard_escolar" value="<?= $historico_anexo; ?>"/>
                                                <input type="file" class="custom-file-input" id="historico_anexo" name="historico_anexo" value="<?= $historico_anexo; ?>">
                                                <label class="custom-file-label" for="historico_anexo"><?= $historico_anexo == "" ? "Selecionar arquivo em PDF" : $historico_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM HISTÓRICO ESCOLAR -->

                        <!-- TRANSFERÊNCIA ESCOLAR -->
                        <div id="div_transferencia" <?= $transferencia_anexo != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>TRANSFERÊNCIA ESCOLAR</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO TRANSFERÊNCIA ESCOLAR</label>
                                            <div class="custom-file mb-2">
                                                <input type="hidden" id="guard_transferencia" name="guard_transferencia" value="<?= $transferencia_anexo; ?>"/>
                                                <input type="file" class="custom-file-input" id="transferencia_anexo" name="transferencia_anexo" value="<?= $transferencia_anexo; ?>">
                                                <label class="custom-file-label" for="transferencia_anexo"><?= $transferencia_anexo == "" ? "Selecionar arquivo em PDF" : $transferencia_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM TRANSFERÊNCIA ESCOLAR -->

                        <!-- PRONTUÁRIO MÉDICO -->
                        <div id="div_prontuario" <?= $prontuario_anexo != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>PRONTUÁRIO MÉDICO</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO PRONTUÁRIO MÉDICO</label>
                                            <div class="custom-file mb-2">
                                                <input type="hidden" id="guard_pontuario" name="guard_pontuario" value="<?= $prontuario_anexo; ?>"/>
                                                <input type="file" class="custom-file-input" id="prontuario_anexo" name="prontuario_anexo" value="<?= $prontuario_anexo; ?>">
                                                <label class="custom-file-label" for="prontuario_anexo"><?= $prontuario_anexo == "" ? "Selecionar arquivo em PDF" : $prontuario_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM PRONTUÁRIO MÉDICO -->

                        <!-- RECEITUÁRIO MÉDICO -->
                        <div id="div_receituario" <?= $receituario_anexo != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>RECEITUÁRIO MÉDICO</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO RECEITUÁRIO MÉDICO</label>
                                            <div class="custom-file mb-2">
                                                <input type="hidden" id="guard_receituario" name="guard_receituario" value="<?= $receituario_anexo; ?>"/>
                                                <input type="file" class="custom-file-input" id="receituario_anexo" name="receituario_anexo" value="<?= $receituario_anexo; ?>">
                                                <label class="custom-file-label" for="receituario_anexo"><?= $receituario_anexo == "" ? "Selecionar arquivo em PDF" : $receituario_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM PRONTUÁRIO MÉDICO -->

                        <!-- RECEITUÁRIO MÉDICO -->
                        <div id="div_mandado" <?= $mandado_anexo != "" ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>MANDADO DE BUSCA E APREENSÃO</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">IMAGEM DO MANDADO DE BUSCA E APREENSÃO</label>
                                            <div class="custom-file mb-2">
                                                <input type="hidden" id="guard_mandado" name="guard_mandado" value="<?= $mandado_anexo; ?>"/>
                                                <input type="file" class="custom-file-input" id="mandado_anexo" name="mandado_anexo" value="<?= $mandado_anexo; ?>">
                                                <label class="custom-file-label" for="mandado_anexo"><?= $mandado_anexo == "" ? "Selecionar arquivo em PDF" : $mandado_anexo; ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <!-- FIM PRONTUÁRIO MÉDICO -->
                    </div>
                </div>
                <!-- FIM DOCUMENTOS -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-success">Salvar</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php
$msg = "";
//------------------------------------------------------------------------------
//Anexo do RG
if (isset($_FILES['rg_anexo'])) {

    $rg_numero = $_POST['rg_numero'] != "" ? $_POST['rg_numero'] : NULL;
    $orgao_expedidor = $_POST['orgao_expedidor'] != "" ? $_POST['orgao_expedidor'] : NULL;
    $estado_expedidor = $_POST['estado_expedidor'] != "" ? $_POST['estado_expedidor'] : NULL;
    $expedicao = $_POST['expedicao'] != "" ? $_POST['expedicao'] : NULL;

    $vf = false;

    if (isset($_FILES['rg_anexo']['name']) && $_FILES['rg_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['rg_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['rg_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "RG: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg .= "RG: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['rg_anexo']['name']) && $_FILES['rg_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['rg_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['rg_anexo']['name'];
            $novo_nome = md5(time()) . "_rg_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['rg_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET rg_numero  = ?, rg_orgao_expedidor = ?, rg_estado = ?, rg_data_expedicao = ?, rg_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $rg_numero);
                        $stmt222->bindValue(2, $orgao_expedidor);
                        $stmt222->bindValue(3, $estado_expedidor);
                        $stmt222->bindValue(4, $expedicao);
                        $stmt222->bindValue(5, $diretorio . $novo_nome);
                        $stmt222->bindValue(6, $_SESSION['id']);
                        $stmt222->bindValue(7, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, rg_numero, rg_orgao_expedidor, rg_estado, rg_data_expedicao, rg_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, ?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $rg_numero);
                        $stmt222->bindValue(3, $orgao_expedidor);
                        $stmt222->bindValue(4, $estado_expedidor);
                        $stmt222->bindValue(5, $expedicao);
                        $stmt222->bindValue(6, $diretorio . $novo_nome);
                        $stmt222->bindValue(7, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "RG: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($rg_numero != NULL && $orgao_expedidor != NULL && $estado_expedidor != NULL && $expedicao != NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET rg_numero  = ?, rg_orgao_expedidor = ?, rg_estado = ?, rg_data_expedicao = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, $rg_numero);
                    $stmt222->bindValue(2, $orgao_expedidor);
                    $stmt222->bindValue(3, $estado_expedidor);
                    $stmt222->bindValue(4, $expedicao);
                    $stmt222->bindValue(5, $_SESSION['id']);
                    $stmt222->bindValue(6, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, rg_numero, rg_orgao_expedidor, rg_estado, rg_data_expedicao, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, $rg_numero);
                    $stmt222->bindValue(3, $orgao_expedidor);
                    $stmt222->bindValue(4, $estado_expedidor);
                    $stmt222->bindValue(5, $expedicao);
                    $stmt222->bindValue(6, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        } else {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET rg_numero  = ?, rg_orgao_expedidor = ?, rg_estado = ?, rg_data_expedicao = ?, rg_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, $rg_numero);
                    $stmt222->bindValue(2, $orgao_expedidor);
                    $stmt222->bindValue(3, $estado_expedidor);
                    $stmt222->bindValue(4, $expedicao);
                    $stmt222->bindValue(5, NULL);
                    $stmt222->bindValue(6, $_SESSION['id']);
                    $stmt222->bindValue(7, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, rg_numero, rg_orgao_expedidor, rg_estado, rg_data_expedicao, rg_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, ?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, $rg_numero);
                    $stmt222->bindValue(3, $orgao_expedidor);
                    $stmt222->bindValue(4, $estado_expedidor);
                    $stmt222->bindValue(5, $expedicao);
                    $stmt222->bindValue(6, NULL);
                    $stmt222->bindValue(7, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}
//------------------------------------------------------------------------------
//Anexo do CPF
if (isset($_FILES['cpf_anexo'])) {

    $numero_cpf = isset($_POST['numero_cpf']) != "" ? $_POST['numero_cpf'] : NULL;
    $rg_cpf = isset($_POST['cpf_anexo']) != "" ? $_POST['cpf_anexo'] : NULL;

    $vf = false;

    if (isset($_FILES['cpf_anexo']['name']) && $_FILES['cpf_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['cpf_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['cpf_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "CPF: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg = "CPF: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['cpf_anexo']['name']) && $_FILES['cpf_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['cpf_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['cpf_anexo']['name'];
            $novo_nome = md5(time()) . "_cpf_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['cpf_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET cpf  = ?, cpf_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $numero_cpf);
                        $stmt222->bindValue(2, $diretorio . $novo_nome);
                        $stmt222->bindValue(3, $_SESSION['id']);
                        $stmt222->bindValue(4, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, cpf, cpf_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $numero_cpf);
                        $stmt222->bindValue(3, $diretorio . $novo_nome);
                        $stmt222->bindValue(4, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "CPF: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($numero_cpf != NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET cpf  = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, $numero_cpf);
                    $stmt222->bindValue(2, $_SESSION['id']);
                    $stmt222->bindValue(3, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, cpf, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, $numero_cpf);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        } else {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET cpf  = ?, cpf_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, $numero_cpf);
                    $stmt222->bindValue(2, NULL);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->bindValue(4, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, cpf, cpf_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, $numero_cpf);
                    $stmt222->bindValue(3, NULL);
                    $stmt222->bindValue(4, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}
//------------------------------------------------------------------------------
//Anexo Certidão de Nascimento
if (isset($_FILES['certidao_anexo'])) {

    $numero_certidao = isset($_POST['numero_certidao']) != "" ? $_POST['numero_certidao'] : NULL;
    $certidao_nascimento = isset($_POST['certidao_anexo']) != "" ? $_POST['certidao_anexo'] : NULL;

    $vf = false;

    if (isset($_FILES['certidao_anexo']['name']) && $_FILES['certidao_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['certidao_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['certidao_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "Certidão de Nascimento: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg = "Certidão de Nascimento: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['certidao_anexo']['name']) && $_FILES['certidao_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['certidao_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['certidao_anexo']['name'];
            $novo_nome = md5(time()) . "_certidao_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['certidao_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET certidao  = ?, certidao_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $numero_certidao);
                        $stmt222->bindValue(2, $diretorio . $novo_nome);
                        $stmt222->bindValue(3, $_SESSION['id']);
                        $stmt222->bindValue(4, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, certidao, certidao_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $numero_certidao);
                        $stmt222->bindValue(3, $diretorio . $novo_nome);
                        $stmt222->bindValue(4, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "Certidão de Nascimento: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($numero_cpf != NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET certidao  = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, $numero_certidao);
                    $stmt222->bindValue(2, $_SESSION['id']);
                    $stmt222->bindValue(3, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, certidao, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, $numero_certidao);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        } else {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET certidao  = ?, certidao_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, $numero_certidao);
                    $stmt222->bindValue(2, NULL);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->bindValue(4, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, certidao, certidao_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, $numero_certidao);
                    $stmt222->bindValue(3, NULL);
                    $stmt222->bindValue(4, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}

//------------------------------------------------------------------------------
//Anexo Carteira de Vacinação
if (isset($_FILES['vacinacao_anexo'])) {

    $numero_vacinacao = isset($_POST['numero_vacinacao']) != "" ? $_POST['numero_vacinacao'] : NULL;
    $carteira_vacinacao = isset($_POST['vacinacao_anexo']) != "" ? $_POST['vacinacao_anexo'] : NULL;

    $vf = false;

    if (isset($_FILES['vacinacao_anexo']['name']) && $_FILES['vacinacao_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['vacinacao_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['vacinacao_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "Carteira de Vacinação: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg = "Carteira de Vacinação: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['vacinacao_anexo']['name']) && $_FILES['vacinacao_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['vacinacao_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['vacinacao_anexo']['name'];
            $novo_nome = md5(time()) . "_vacinacao_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['vacinacao_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET vacinacao  = ?, vacinacao_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $numero_vacinacao);
                        $stmt222->bindValue(2, $diretorio . $novo_nome);
                        $stmt222->bindValue(3, $_SESSION['id']);
                        $stmt222->bindValue(4, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, vacinacao, vacinacao_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $numero_vacinacao);
                        $stmt222->bindValue(3, $diretorio . $novo_nome);
                        $stmt222->bindValue(4, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "Carteira de Vacinação: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($numero_cpf != NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET vacinacao  = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, $numero_vacinacao);
                    $stmt222->bindValue(2, $_SESSION['id']);
                    $stmt222->bindValue(3, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, vacinacao, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, $numero_vacinacao);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        } else {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET vacinacao  = ?, vacinacao_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, $numero_vacinacao);
                    $stmt222->bindValue(2, NULL);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->bindValue(4, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, vacinacao, vacinacao_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, $numero_vacinacao);
                    $stmt222->bindValue(3, NULL);
                    $stmt222->bindValue(4, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}
//------------------------------------------------------------------------------
//Anexo Historico
if (isset($_FILES['historico_anexo'])) {

    $guard_escolar = isset($_POST['guard_escolar']) != "" ? $_POST['guard_escolar'] : NULL;
    $historico_escolar = isset($_POST['historico_anexo']) != "" ? $_POST['historico_anexo'] : NULL;

    $vf = false;

    if (isset($_FILES['historico_anexo']['name']) && $_FILES['historico_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['historico_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['historico_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "Histórico Escolar: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg = "Histórico Escolar: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['historico_anexo']['name']) && $_FILES['historico_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['historico_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['historico_anexo']['name'];
            $novo_nome = md5(time()) . "_historico_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['historico_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET historico_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $diretorio . $novo_nome);
                        $stmt222->bindValue(2, $_SESSION['id']);
                        $stmt222->bindValue(3, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, historico_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $diretorio . $novo_nome);
                        $stmt222->bindValue(3, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "Histórico Escolar: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($guard_escolar == "" || $guard_escolar == NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET historico_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, NULL);
                    $stmt222->bindValue(2, $_SESSION['id']);
                    $stmt222->bindValue(3, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, historico_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, NULL);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}

//------------------------------------------------------------------------------
//Anexo Transferência Escolar
if (isset($_FILES['transferencia_anexo'])) {

    $guard_transferencia = isset($_POST['guard_transferencia']) != "" ? $_POST['guard_transferencia'] : NULL;
    $transferencia_escolar = isset($_POST['transferencia_anexo']) != "" ? $_POST['transferencia_anexo'] : NULL;

    $vf = false;

    if (isset($_FILES['transferencia_anexo']['name']) && $_FILES['transferencia_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['transferencia_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['transferencia_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "Transferência Escolar: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg = "Transferência Escolar: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['transferencia_anexo']['name']) && $_FILES['transferencia_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['transferencia_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['transferencia_anexo']['name'];
            $novo_nome = md5(time()) . "_transferencia_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['transferencia_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET transferencia_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $diretorio . $novo_nome);
                        $stmt222->bindValue(2, $_SESSION['id']);
                        $stmt222->bindValue(3, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, transferencia_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $diretorio . $novo_nome);
                        $stmt222->bindValue(3, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "Transferência Escolar: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($guard_transferencia == "" || $guard_transferencia == NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET transferencia_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, NULL);
                    $stmt222->bindValue(2, $_SESSION['id']);
                    $stmt222->bindValue(3, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, transferencia_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, NULL);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}
//------------------------------------------------------------------------------
//Anexo Prontuário Médico
if (isset($_FILES['prontuario_anexo'])) {

    $guard_pontuario = isset($_POST['guard_pontuario']) != "" ? $_POST['guard_pontuario'] : NULL;
    $prontuario_medico = isset($_POST['prontuario_anexo']) != "" ? $_POST['prontuario_anexo'] : NULL;

    $vf = false;

    if (isset($_FILES['prontuario_anexo']['name']) && $_FILES['prontuario_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['prontuario_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['prontuario_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "Prontuário Médico: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg = "Prontuário Médico: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['prontuario_anexo']['name']) && $_FILES['prontuario_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['prontuario_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['prontuario_anexo']['name'];
            $novo_nome = md5(time()) . "_prontuario_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['prontuario_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET prontuario_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $diretorio . $novo_nome);
                        $stmt222->bindValue(2, $_SESSION['id']);
                        $stmt222->bindValue(3, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, prontuario_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $diretorio . $novo_nome);
                        $stmt222->bindValue(3, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "Prontuário Médico: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($guard_pontuario == "" || $guard_pontuario == NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET prontuario_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, NULL);
                    $stmt222->bindValue(2, $_SESSION['id']);
                    $stmt222->bindValue(3, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, prontuario_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, NULL);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}

//------------------------------------------------------------------------------
//Anexo Receituário Médico
if (isset($_FILES['receituario_anexo'])) {

    $guard_receituario = isset($_POST['guard_receituario']) != "" ? $_POST['guard_receituario'] : NULL;
    $prontuario_medico = isset($_POST['receituario_anexo']) != "" ? $_POST['receituario_anexo'] : NULL;

    $vf = false;

    if (isset($_FILES['receituario_anexo']['name']) && $_FILES['receituario_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['receituario_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['receituario_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "Receituário Médico: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg = "Receituário Médico: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['receituario_anexo']['name']) && $_FILES['receituario_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['receituario_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['receituario_anexo']['name'];
            $novo_nome = md5(time()) . "_receituario_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['receituario_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET receituario_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $diretorio . $novo_nome);
                        $stmt222->bindValue(2, $_SESSION['id']);
                        $stmt222->bindValue(3, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, receituario_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $diretorio . $novo_nome);
                        $stmt222->bindValue(3, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "Receituário Médico: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($guard_receituario == "" || $guard_receituario == NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET receituario_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, NULL);
                    $stmt222->bindValue(2, $_SESSION['id']);
                    $stmt222->bindValue(3, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, receituario_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, NULL);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}
//------------------------------------------------------------------------------
//Anexo Mandado de Busca e Apreensão
if (isset($_FILES['mandado_anexo'])) {

    $guard_mandado = isset($_POST['guard_mandado']) != "" ? $_POST['guard_mandado'] : NULL;
    $mandado_busca = isset($_POST['mandado_anexo']) != "" ? $_POST['mandado_anexo'] : NULL;

    $vf = false;

    if (isset($_FILES['mandado_anexo']['name']) && $_FILES['mandado_anexo']['size'] > 0) {

        // verifica se arquivo foi enviado e sem erros
        if ($_FILES['mandado_anexo']['error'] == UPLOAD_ERR_OK) {
            // pego a extensão do arquivo
            $extensao1 = extensao($_FILES['mandado_anexo']['name']);
            // valida a extensão
            if (!in_array($extensao1, array("pdf"))) {
                $msg .= "Mandado de Busca e Apreensão: Somente arquivos PDF são permitidos.\\n";
            }
        } else {
            $msg = "Mandado de Busca e Apreensão: Você deve enviar um arquivo.\\n";
        }
    }

    if ($msg == "") {
        if (isset($_FILES['mandado_anexo']['name']) && $_FILES['mandado_anexo']['size'] > 0) {

            //Define o diretorio para onde enviaremos o arquivo
            $diretorio = "anexos/" . $id . "/";
            // pego a extensão do arquivo
            $extensao = extensao($_FILES['mandado_anexo']['name']);
            // atribui novo nome ao arquivo
            $nome_original = $_FILES['mandado_anexo']['name'];
            $novo_nome = md5(time()) . "_mandado_" . $id . "." . $extensao;

            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777);
                chmod($diretorio, 0777);
            }

            // faz o upload
            $enviou = move_uploaded_file($_FILES['mandado_anexo']['tmp_name'], $diretorio . $novo_nome);

            if ($enviou) {
                try {

                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                        $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET mandado_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                        $stmt222->bindValue(1, $diretorio . $novo_nome);
                        $stmt222->bindValue(2, $_SESSION['id']);
                        $stmt222->bindValue(3, $id);
                        $stmt222->execute();
                    } else {
                        $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, mandado_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                        $stmt222->bindValue(1, $id);
                        $stmt222->bindValue(2, $diretorio . $novo_nome);
                        $stmt222->bindValue(3, $_SESSION['id']);
                        $stmt222->execute();
                    }
                } catch (PDOException $e) {
                    $db->rollback();
                    echo 'Error:' . $e->getMessage();
                }
            } else {
                $msg .= "Mandado de Busca e Apreensão: Houve falha ao enviar os arquivos.\\n";
            }
        } else if ($guard_mandado == "" || $guard_mandado == NULL) {
            try {
                if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $id, ""))) {
                    $stmt222 = $db->prepare('UPDATE mod_acolhimento_documentacao SET mandado_anexo = ?, usuario_id = ?, data_cadastro = NOW() WHERE acolhimento_id = ?');
                    $stmt222->bindValue(1, NULL);
                    $stmt222->bindValue(2, $_SESSION['id']);
                    $stmt222->bindValue(3, $id);
                    $stmt222->execute();
                } else {
                    $stmt222 = $db->prepare('INSERT INTO mod_acolhimento_documentacao (acolhimento_id, mandado_anexo, usuario_id, data_cadastro) VALUES (?, ?, ?, NOW())');
                    $stmt222->bindValue(1, $id);
                    $stmt222->bindValue(2, NULL);
                    $stmt222->bindValue(3, $_SESSION['id']);
                    $stmt222->execute();
                }
            } catch (PDOException $e) {
                $db->rollback();
                echo 'Error:' . $e->getMessage();
            }
        }
    }
}
//------------------------------------------------------------------------------
if (isset($_FILES['rg_anexo']) || isset($_FILES['cpf_anexo']) || isset($_FILES['certidao_anexo']) || isset($_FILES['vacinacao_anexo']) || isset($_FILES['historico_anexo']) ||
        isset($_FILES['transferencia_anexo']) || isset($_FILES['prontuario_anexo']) || isset($_FILES['receituario_anexo']) || isset($_FILES['mandado_anexo'])) {
    if ($msg == "") {
        $vf = true;
    }

    if ($msg != "") {
        echo "<script 'text/javascript'>alert('$msg');window.location = '" . PORTAL_URL . "admin/view/acolhimento/novo2/" . $id . "';</script>";
    } else if ($vf) {
        echo "<script 'text/javascript'>alert('Documentação Salva com Sucesso!');window.location = '" . PORTAL_URL . "admin/view/acolhimento/novo2/" . $id . "';</script>";
    }
}

function extensao($arquivo) {
    $arquivo = strtolower($arquivo);
    $explode = explode(".", $arquivo);
    $arquivo = end($explode);

    return ($arquivo);
}
?>

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/acolhimento/novo2.js"></script>

