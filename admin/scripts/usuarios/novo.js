//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    //COMBO ESTADO E MUNICIPIO
    $("select#estado_id").change(function () {
        $("select#municipio_id").html('<option value="0">Carregando...</option>');
        $.post(PORTAL_URL + "utils/combo_cidades.php",
                {estado: $(this).val()},
                function (valor) {
                    $("select#municipio_id").html(valor);
                    $("select#municipio_id").select2();
                });
    });

    $('#form_usuario').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var nome = $('#nome').val();
            var cpf = $('#cpf').val();
            var nascimento = $('#nascimento').val();
            var email = $('#email').val();
            var celular = $('#celular').val();
            var fixo = $('#fixo').val();
            var rua = $('#rua').val();
            var numero = $('#numero').val();
            var bairro = $('#bairro').val();
            var login = $('#login').val();
            var senha = $('#senha').val();
            var municipio_id = $('#municipio_id').val();
            var nivel = $('#nivel').val();
            var funcao = $('#funcao').val();
            var categoria = $('#categoria').val();
            var periodo = $('#periodo').val();

            $.post(PORTAL_URL + "admin/dao/usuarios/novo.php", {id: id, periodo: periodo, categoria: categoria, funcao: funcao, nivel: nivel, nome: nome, cpf: cpf, nascimento: nascimento, email: email, celular: celular, fixo: fixo, rua: rua, numero: numero, bairro: bairro, login: login, senha: senha, municipio_id: municipio_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Funcionário",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Funcionário",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/usuarios/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    var id = $("#id").val();
    var nome = $("#nome").val();
    var cpf = $("#cpf").val();
    var senha = $("#senha").val();
    var conf_senha = $("#confirmar_senha").val();
    var estado_id = $("#estado_id").val();
    var municipio_id = $("#municipio_id").val();
    var login = $("#login").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS
        //VERIFICANDO SE O CAMPO LOGIN FOI INFORMADO
        if (senha == "" && id == "") {
            $('div#div_senha').after('<label id="erro_senha" class="error">A senha é obrigatório.</label>');
            valido = false;
            element = $('div#div_senha');
        } else if (conf_senha == "" && id == "") {
            $('div#div_conf_senha').after('<label id="erro_conf_senha" class="error">Confirmação de senha é obrigatório.</label>');
            valido = false;
            element = $('div#div_conf_senha');
        } else {
            //VERIFICANDO SE O CAMPO LOGIN FOI INFORMADO
            if (senha != conf_senha && senha != "" && conf_senha != "") {
                valido = false;

                $('div#div_senha').after('<label id="erro_senha" class="error">A senha e confirmação de senha não coincidem.</label>');
                $('div#div_conf_senha').after('<label id="erro_conf_senha" class="error">A senha e confirmação de senha não coincidem.</label>');

                element = $('div#div_conf_senha');
            }
        }

        if (login == "") {
            $('div#div_login').after('<label id="erro_login" class="error">Login é obrigatório.</label>');
            valido = false;
            element = $('div#div_login');
        }

        //VERIFICANDO SE O CAMPO MUNICÍPIO FOI INFORMADO
        if (municipio_id == "") {
            $('div#div_municipio_id').after('<label id="erro_municipio_id" class="error">Município é obrigatório.</label>');
            valido = false;
            element = $('div#div_municipio_id');
        }

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (estado_id == "") {
            $('div#div_estado_id').after('<label id="erro_estado_id" class="error">Estado é obrigatório.</label>');
            valido = false;
            element = $('div#div_estado_id');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome == "") {
            $('div#div_nome').after('<label id="erro_nome" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome');
        }

    } else if (obj.tipo == "cpf") {//VALIDAÇÃO COM BANCO DE DADOS
        $('div#div_cpf').after('<label id="erro_cpf" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_cpf');
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------