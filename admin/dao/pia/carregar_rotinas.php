<?php

//------------------------------------------------------------------------------
include_once('../../../conf/config.php');
$db = Conexao::getInstance();

$id = $_POST['id'];

$stmp = $db->prepare("SELECT *           
                     FROM mod_acolhimento_crianca_educacao_rotinas macea   
                     WHERE macea.educacao_id = ?");
$stmp->bindValue(1, $id);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    
} else {
    while ($acomp = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr id="remover_rotinas">
                     <td>' . $acomp['quem_realiza'] . '</td>
                     <td>' . $acomp['forma_realiza'] . '</td>
                     <td>' . $acomp['frequencia'] . '</td>
                     <td width="100px">
                        <a style="cursor: pointer" onclick="editar2(' . $acomp['id'] . ', ' . "'" . $acomp['quem_realiza'] . "'" . ', ' . "'" . $acomp['forma_realiza'] . "'" . ', ' . "'" . $acomp['frequencia'] . "'" . ')" class="text-warning"><i class="fa fa-pencil"></i></a>
                        <a style="cursor: pointer" onclick="remover2(this, ' . $acomp['id'] . ')" class="text-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>';
    }
}
//------------------------------------------------------------------------------
?>

