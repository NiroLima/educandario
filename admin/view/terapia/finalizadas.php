<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Terapias</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-clipboard-user"></i> <strong>TERAPIAS FINALIZADAS</strong></h4></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>NOME DO PACIENTE</th>
                                            <th>DATA</th>
                                            <th>HORA</th>
                                            <th>MÉDICO</th>
                                            <th>ESPECIALIDADE</th>
                                            <th>FINALIZAÇÃO</th>
                                            <th>RESPONSÁVEL</th>
                                            <th width="100px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $result = $db->prepare("SELECT msc.id, mac.nome AS crianca, msc.data_terapia, msc.hora_terapia, msc.status, msc.resp_finalizacao, msc.data_finalizacao,  
                                                                u.nome AS profissional, u.id as profissional_id, f.nome AS funcao 
                                                                FROM mod_saude_terapias msc 
                                                                LEFT JOIN mod_acolhimento_crianca AS mac ON mac.id = msc.crianca_id 
                                                                LEFT JOIN seg_profissional AS u ON u.id = msc.profissional_id 
                                                                LEFT JOIN seg_funcao AS f ON f.id = u.funcao_id 
                                                                WHERE msc.situacao = 2 AND f.id IN(6) 
                                                                ORDER BY mac.nome");
                                        $result->execute();
                                        while ($medicamento = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $medicamento['crianca']; ?></td>
                                                <td><?= obterDataBRTimestamp($medicamento['data_terapia']); ?></td>
                                                <td><?= $medicamento['hora_terapia']; ?></td>
                                                <td><?= $medicamento['profissional']; ?></td>
                                                <td><?= especialidade($medicamento['profissional_id']); ?></td>
                                                <td><?= obterDataBRTimestamp($medicamento['data_finalizacao']); ?></td>
                                                <td><?= pesquisar("nome", "seg_usuario", "id", "=", $medicamento['resp_finalizacao'], ""); ?></td>
                                                <td class="text-center">
                                                    <a id="desfinalizar" rel="<?= $medicamento['id']; ?>" href="#" title="Desfazer Finalização da Terapia" class="mr-2 waves-effect waves-light btn btn-xs btn-danger">DESFAZER</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/terapia/finalizadas.js"></script>
