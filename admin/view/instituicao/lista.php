<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Instituição</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-user"></i> <strong>LISTA DE INSTITUIÇÃO</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/instituicao/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVA INSTITUIÇÃO</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Contato</th>
                                            <th>Endereço</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $db->prepare("SELECT si.id, si.status, si.nome, si.contato, si.endereco, si.numero, si.bairro   
                                                                FROM seg_instituicao si 
                                                                WHERE 1
                                                                ORDER BY si.nome");
                                        $result->execute();
                                        while ($instituicao = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $instituicao['nome']; ?></td>
                                                <td><?= $instituicao['contato']; ?></td>
                                                <td><?= $instituicao['endereco']; ?></td>
                                                <td class="text-center">
                                                    <?php
                                                    if (ver_nivel(1, "") || ver_nivel(3, "")) {
                                                        ?>
                                                        <a id="ativar" rel="<?= $instituicao['id']; ?>" <?= $instituicao['status'] == 0 && $instituicao['id'] != 1 || $instituicao['status'] == 0 && $instituicao['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> title="Desbloquear a Instituição" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                        <a id="remover" rel="<?= $instituicao['id']; ?>" <?= $instituicao['status'] == 1 && $instituicao['id'] != 1 || $instituicao['status'] == 1 && $instituicao['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> title="Bloquear a Instituição" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                        <a <?= $instituicao['id'] != 1 && ver_nivel(1, "") || $instituicao['id'] != 1 && ver_nivel(3, "") || $instituicao['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> href="<?= PORTAL_URL ?>admin/view/instituicao/novo/<?= $instituicao['id']; ?>" title="Editar Instituição" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                                            <?php
                                                        }
                                                        ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/instituicao/lista.js"></script>
