<?php

ob_start();
session_start();

//ADICIONAR A CONEXAO E URL AMIGAVEL
include_once("conf/Url.php");
include_once("conf/config.php");
include_once("conf/session.php");
include_once("utils/funcoes.php");
include_once('utils/permissoes.php');

//INSTANCIA A CONEXAO
$db = Conexao::getInstance();

if (@$_SESSION['redefinir_senha'] == 1 && Url::getURL(0) != "redefinir_senha" && Url::getURL(3) != "atualizar_senha" && Url::getURL(0) != "logout") {//CASO A SENHA SEJA 123456 JOGAR O USUÁRIO PARA A TELA DE REDEFINIÇÃO DE SENHA
    echo "<script 'text/javascript'>window.location = '" . PORTAL_URL . "admin/redefinir_senha';</script>";
}

$modulo = Url::getURL(0);
$mvc = Url::getURL(1);
$arquivomodulo = Url::getURL(2);
$parametromodulo = Url::getURL(3);


//VERIFICA SE O ARQUIVO EXISTE E EXIBI
if (file_exists($modulo . ".php") && $modulo != 'portal') {
    include_once $modulo . ".php";
    sessionOn();
    exit();
}

if ($modulo == 'index.php' || $modulo == 'index' || $modulo == '' || $modulo == null) {
    //$modulo = "sobre"; Add somente depois que o sistema estiver funcionando perfeitamente.
    $modulo = "login";
    include_once $modulo . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'login.php' || $mvc == 'login') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'painel.php' || $mvc == 'painel') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'logout.php' || $mvc == 'logout') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'autenticar.php' || $mvc == 'autenticar') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else if ( $mvc == 'recuperar.php' || $mvc == 'recuperar') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
} else if ($mvc == 'esqueceu_senha' || $mvc == 'esqueceu_senha.php') {
    include_once $modulo . "/" . $mvc . ".php";
    sessionOn();
    exit();
}   else {

    if ($parametromodulo == 'index.php' || $parametromodulo == 'index' || $parametromodulo == '' || $parametromodulo == null) {

        //VERIFICA SE O ARQUIVO EXISTE E EXIBI
        if (file_exists($modulo . '/' . $mvc . '/' . $arquivomodulo . '/' . "index.php")) {
            include_once $modulo . '/' . $mvc . '/' . $arquivomodulo . '/' . "index.php";
            sessionOn();
            exit();
        } else {
            include_once "404.php";
            sessionOn();
            exit();
        }
    } else {
        if ($arquivomodulo == '' || $arquivomodulo == null) {

            //VERIFICA SE O ARQUIVO EXISTE E EXIBI
            if (file_exists($modulo . '/' . $mvc . '/' . "index.php")) {
                include_once $modulo . '/' . $mvc . '/' . "index.php";
                sessionOn();
                exit();
            } else {
                include_once "404.php";
                sessionOn();
                exit();
            }
        } else {
            //VERIFICA SE O ARQUIVO EXISTE E EXIBI
            if (file_exists($modulo . '/' . $mvc . '/' . $arquivomodulo . '/' . $parametromodulo . ".php")) {
                include_once $modulo . '/' . $mvc . '/' . $arquivomodulo . '/' . $parametromodulo . ".php";
                sessionOn();
                exit();
            } else {
                include_once "404.php";
                sessionOn();
                exit();
            }
        }
    }//END IF
}//END IF
?>  