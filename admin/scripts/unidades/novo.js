//------------------------------------------------------------------------------
function SomenteNumero(e) {

    var tecla = (window.event) ? event.keyCode : e.which;
    if ((tecla > 47 && tecla < 58))
        return true;
    else {
        if (tecla == 8 || tecla == 0)
            return true;
        else
            return false;
    }
}
//------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('#form_unidade').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var unidade_saude = $('#unidade_saude').val();
            var contato_unidade_saude = $('#contato_unidade_saude').val();
            var endereco_unidade_saude = $('#endereco_unidade_saude').val();
            var numero_unidade_saude = $('#numero_unidade_saude').val();
            var bairro_unidade_saude = $('#bairro_unidade_saude').val();

            $.post(PORTAL_URL + "admin/dao/unidades/novo.php", {id: id, unidade_saude: unidade_saude, contato_unidade_saude: contato_unidade_saude, endereco_unidade_saude: endereco_unidade_saude,
            numero_unidade_saude: numero_unidade_saude, bairro_unidade_saude: bairro_unidade_saude}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário do Local de Atendimento",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário do Local de Atendimento",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/unidades/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var unidade_saude = $('#unidade_saude').val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (unidade_saude == "") {
            $('div#div_unidade_saude').after('<label id="erro_unidade_saude" class="error">Nome do local é obrigatório.</label>');
            valido = false;
            element = $('div#div_unidade_saude');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------