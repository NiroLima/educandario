<?php
//------------------------------------------------------------------------------
@session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$id = $_POST['id'];

$stmp = $db->prepare("SELECT *           
                     FROM mod_saude_consulta_diagnostico mscd   
                     WHERE mscd.consulta_id = ? OR mscd.consulta_id IS NULL AND mscd.responsavel_id = ?");
$stmp->bindValue(1, $id);
$stmp->bindValue(2, $_SESSION['id']);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    
} else {
    while ($diag = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr id="remover_diagnostico">
                     <td>' . $diag['diagnostico'] . '</td>
                     <td>' . $diag['orientacoes'] . '</td>
                     <td>';

        $stmp3 = $db->prepare("SELECT msce.id, mse.nome AS exame            
                              FROM mod_saude_consulta_exame msce 
                              LEFT JOIN mod_saude_exames AS mse ON mse.id = msce.exame_id 
                              WHERE msce.diagnostico_id = ?
                              ORDER BY mse.nome ASC");
        $stmp3->bindValue(1, $diag['id']);
        $stmp3->execute();
        while ($exames3 = $stmp3->fetch(PDO::FETCH_ASSOC)) {
            echo '<li>' . $exames3['exame'] . '</li>';
        }

        echo '</td>
                     <td>' . $diag['medicacoes'] . '</td>
                     <td>';

        $stmp4 = $db->prepare("SELECT msce.id, mse.nome AS especialista            
                              FROM mod_saude_consulta_especialistas msce 
                              LEFT JOIN mod_saude_especialidade AS mse ON mse.id = msce.especialista_id 
                              WHERE msce.diagnostico_id = ?
                              ORDER BY mse.nome ASC");
        $stmp4->bindValue(1, $diag['id']);
        $stmp4->execute();
        while ($especialistas4 = $stmp4->fetch(PDO::FETCH_ASSOC)) {
            echo '<li>' . $especialistas4['especialista'] . '</li>';
        }

        echo '</td>
                     <td width="100px">
                        <a style="cursor: pointer" id="editar" onclick="editar(' . $diag['id'] . ', ' . "'" . $diag['diagnostico'] . "'" . ', ' . "'" . $diag['orientacoes'] . "'" . ', ' . "'" . $diag['medicacoes'] . "'" . ')" class="text-warning"><i class="fa fa-pencil"></i></a>
                        <a style="cursor: pointer" id="remover" onclick="remover(this, ' . $diag['id'] . ')" class="text-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>';
    }
}
//------------------------------------------------------------------------------
?>

