<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
        FROM mod_acolhimento ma   
        WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result22 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result22->bindValue(1, $acolhimento_id);
    $result22->execute();
    $resultado22 = $result22->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_id = $resultado22['id'];
    $acolhimento_responsavel = $resultado22['usuario_id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_rede macr   
        WHERE macr.acolhimento_crianca_id = ?");
    $result2->bindValue(1, $acolhimento_crianca_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $rede_id = $resultado2['id'];
    $acompanhamento = $resultado2['acompanhamento'];
    $acompanhamento2 = $resultado2['acompanhamento2'];
    $vf = $resultado2['vf'] == "" ? 0 : $resultado2['vf'];
} else {
    $acolhimento_id = "";
    $rede_id = "";
    $acompanhamento = "";
    $acompanhamento2 = "";
    $acolhimento_crianca_id = "";
    $acolhimento_responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
    $vf = 0;
}
//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);
//------------------------------------------------------------------------------
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/pia/lista">PIA</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="text-center">PLANO INDIVIDUAL DE ATENDIMENTO - PIA</h4>

        <!-- Main content -->
        <section class="content">

            <!-- Step wizard -->
            <div id="impressao" class="box box-solid bg-info">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="box-title">PIA</h4>
                            <h6 class="box-subtitle">PLANO INDIVIDUAL DE ATENDIMENTO</h6>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-right"><?= $barra; ?>%</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $barra; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $barra; ?>%">
                                    <span class="sr-only"><?= $barra; ?>% Complete</span>
                                </div>
                            </div>
                        </div>      
                    </div>  
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="box-title">RESPONSÁVEL DO PIA</h4>

                            <div id="responsavel_edite_1">
                                <h6 class="subtitle"><?= $nome_responsavel; ?> <a href="#" class="edit-responsavel"><i onclick="editar_responsavel()" class="fal fa-pencil-alt"></i></a></h6>
                            </div>

                            <div id="responsavel_edite_2" style="display: none">
                                <form id="form_responsavel" name="form_responsavel" method="POST" action="#" class="tab-wizard wizard-circle">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required="true" id="responsavel_id" name="responsavel_id">
                                                <option value="">Selecione o responsável</option>
                                                <?php
                                                $result = $db->prepare("SELECT su.id, su.nome           
                                                            FROM seg_usuario su    
                                                            WHERE su.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_responsavel == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button href="#" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <a <?= is_numeric($vf) && $vf == 0 ? "" : "style='display: none'"; ?> href="#" onclick="validar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-success mt-2"><i class="fas fa-check"></i> VALIDAR</a>
                            <a <?= is_numeric($vf) && $vf == 1 ? "" : "style='display: none'"; ?> href="#" onclick="desvalidar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-warning mt-2"><i class="fas fa-remove"></i> DESVALIDAR</a>
                        </div>
                        <div class="col-md-2 text-right">
                            <a onclick="gerar_pdf()" style="cursor: pointer" class="btn-arquivo mt-2"><i class="fas fa-file-pdf"></i> GERAR PDF</a>
                        </div>
                    </div> 
                </div>
                <!-- /.box-header -->
                <div class="box-body wizard-content">

                    <?php include 'layout/menu_wizard.php'; ?>

                    <br/>

                    <form id="form_rede" name="form_rede" method="POST" action="#" class="tab-wizard wizard-circle">

                        <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>
                        <input type="hidden" id="acolhimento_crianca_id" name="acolhimento_crianca_id" value="<?= $acolhimento_crianca_id; ?>"/>
                        <input type="hidden" id="rede_id" name="rede_id" value="<?= $rede_id; ?>"/>
                        <!-- Step 6 -->
                        <!-- <h6>REDE</h6> -->
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-outline-info">
                                        <div class="box-header">
                                            <strong>ACOMPANHAMENTO DA REDE</strong>
                                        </div>
                                        <div class="box-body">
                                            <!-- INÍCIO -->
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Há instituições/serviços/programas que estiveram acompanhando os pais/responsáveis e/ou a criança antes do acolhimento institucional?</label>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <input type="radio" class="with-gap radio-col-info" <?= $acompanhamento == 1 ? "checked='true'" : ""; ?> name="opcao_retono" id="sim_acolhimento_institucional" value="1">
                                                                <label for="sim_acolhimento_institucional">SIM</label>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input type="radio" class="with-gap radio-col-info" <?= $acompanhamento == 0 || $acompanhamento == "" ? "checked='true'" : ""; ?> name="opcao_retono" id="nao_acolhimento_institucional" value="1">
                                                                <label for="nao_acolhimento_institucional">NÃO</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                <div id="div_membros" <?= $acompanhamento == 1 ? "" : "style='display: none'"; ?> class="col-md-12">
                                                    <br>
                                                    <div class="box bl-3 br-3 border-danger">
                                                        <div class="box-header">
                                                            <h4 class="box-title mb-0"><strong>INSTITUIÇÕES</strong></h4>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="row mt-3">
                                                                <div class="col-md-12">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped mb-2">
                                                                            <thead class="thead-light">
                                                                                <tr>
                                                                                    <th scope="col">INSTITUIÇÃO / SERVIÇO / PROGRAMA</th>
                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                    <th scope="col">TELEFONE</th>
                                                                                    <th scope="col">FAMILIAR / ACOMPANHANTE</th>
                                                                                    <th scope="col">METODOLOGIA DE ACOMPANHAMENTO</th>
                                                                                    <th scope="col"></th>
                                                                                </tr>
                                                                            </thead>

                                                                            <tbody id="resultado_membro">
                                                                                <?php
                                                                                $result = $db->prepare("SELECT macrm.id, macrm.instituicao, macrm.endereco, macrm.numero, macrm.bairro, macrm.contato, macrm.nome, macrm.referencia, sf.nome AS funcao            
                                                                                        FROM mod_acolhimento_crianca_rede_membros macrm   
                                                                                        LEFT JOIN seg_profissional_instituicao AS u ON u.id = macrm.nome  
                                                                                        LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id 
                                                                                        WHERE macrm.acolhimento_crianca_rede_id = ? OR macrm.acolhimento_crianca_rede_id = 0 AND macrm.responsavel_id = ?");
                                                                                $result->bindValue(1, $rede_id);
                                                                                $result->bindValue(2, $_SESSION['id']);
                                                                                $result->execute();
                                                                                while ($membros = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                    ?>
                                                                                    <tr id="remover_membro">
                                                                                        <th><?= $membros['instituicao']; ?></th>
                                                                                        <td><?= $membros['endereco']; ?>, <?= $membros['numero']; ?> - <?= $membros['bairro']; ?></td>
                                                                                        <td><?= $membros['contato']; ?></td>
                                                                                        <td><?= pesquisar("nome", "seg_profissional_instituicao", "id", "=", $membros['nome'], ""); ?></td>
                                                                                        <td><?= $membros['referencia']; ?></td>
                                                                                        <td width="100px">
                                                                                            <a style="cursor: pointer" onclick="editar(<?= $membros['id']; ?>, '<?= $membros['instituicao']; ?>', '<?= $membros['endereco']; ?>', '<?= $membros['numero']; ?>', '<?= $membros['bairro']; ?>', '<?= $membros['contato']; ?>', '<?= $membros['nome']; ?>', '<?= $membros['referencia']; ?>', '<?= $membros['funcao']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                                            <a style="cursor: pointer" onclick="remover(this, <?= $membros['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </tbody>

                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row mt-3">
                                                                <div class="col-md-9">
                                                                    <div id="div_instituicao" class="form-group">
                                                                        <label for="instituicao">INSTITUIÇÃO / SERVIÇO / PROGRAMA</label>
                                                                        <input type="hidden" id="membro_id" name="membro_id" value=""/>
                                                                        <input type="text" class="form-control" name="instituicao" id="instituicao" placeholder="Instituição, Serviço ou Programa" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div id="div_contato" class="form-group">
                                                                        <label for="contato">CONTATO</label>
                                                                        <input type="text" class="form-control" data-mask="(99)99999-9999" name="contato" id="contato" placeholder="Celular, fixo..." value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <div id="div_endereco" class="form-group">
                                                                        <label for="endereco">ENDEREÇO</label>
                                                                        <input type="text" class="form-control" name="endereco" id="endereco" placeholder="Rua, Travessa, Avenida..." value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div id="div_numero" class="form-group">
                                                                        <label for="numero">NÚMERO</label>
                                                                        <input type="text" class="form-control" name="numero" id="numero" placeholder="Número" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div id="div_bairro" class="form-group">
                                                                        <label for="bairro">BAIRRO</label>
                                                                        <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div id="div_nome_irmao" class="form-group">
                                                                        <label for="nome_irmao">NOME</label>
                                                                        <select name="nome_irmao" id="nome_irmao" class="form-control select2">
                                                                            <option value="">Selecione o profissional da rede</option>
                                                                            <?php
                                                                            $result8 = $db->prepare("SELECT u.id, u.funcao_id, u.nome, sf.nome AS funcao 
                                                                                                    FROM seg_profissional_instituicao u 
                                                                                                    LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id 
                                                                                                    WHERE u.status = 1");
                                                                            $result8->execute();
                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <option funcao='<?= $irm['funcao']; ?>' value='<?= $irm['id']; ?>'><?= $irm['nome']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div id="div_funcao_profissional_rede" class="form-group">
                                                                        <label for="funcao_profissional_rede">FUNÇÃO</label>
                                                                        <input type="text" name="funcao_profissional_rede" id="funcao_profissional_rede" class="form-control" disabled />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div id="div_referencia_contato" class="form-group">
                                                                        <label for="referencia_contato">METODOLOGIA DE ACOMPANHAMENTO</label>
                                                                        <textarea name="referencia_contato" id="referencia_contato" class="form-control" cols="30" rows="10"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 text-center">
                                                                    <button id="inserir_membro" class="btn btn-success">INSERIR</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM -->


                                            <!-- INÍCIO -->
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Há instituições/serviços/programas que externos à instituição de acolhimento que estão atuando no caso?</label>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <input type="radio" class="with-gap radio-col-info" <?= $acompanhamento2 == 1 ? "checked='true'" : ""; ?> name="opcao_retono2" id="sim_acolhimento_institucional2" value="1">
                                                                <label for="sim_acolhimento_institucional2">SIM</label>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input type="radio" class="with-gap radio-col-info" <?= $acompanhamento2 == 0 || $acompanhamento2 == "" ? "checked='true'" : ""; ?> name="opcao_retono2" id="nao_acolhimento_institucional2" value="1">
                                                                <label for="nao_acolhimento_institucional2">NÃO</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- EM CASO DE RESPOTA SIM MOSTRAR -->
                                                <div id="div_membros2" <?= $acompanhamento2 == 1 ? "" : "style='display: none'"; ?> class="col-md-12">
                                                    <br>
                                                    <div class="box bl-3 br-3 border-danger">
                                                        <div class="box-header">
                                                            <h4 class="box-title mb-0"><strong>INSTITUIÇÕES</strong></h4>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="row mt-3">
                                                                <div class="col-md-12">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped mb-2">
                                                                            <thead class="thead-light">
                                                                                <tr>
                                                                                    <th scope="col">INSTITUIÇÃO / SERVIÇO / PROGRAMA</th>
                                                                                    <th scope="col">ENDEREÇO</th>
                                                                                    <th scope="col">TELEFONE</th>
                                                                                    <th scope="col">FAMILIAR / ACOMPANHANTE</th>
                                                                                    <th scope="col">METODOLOGIA DE ACOMPANHAMENTO</th>
                                                                                    <th scope="col"></th>
                                                                                </tr>
                                                                            </thead>

                                                                            <tbody id="resultado_membro2">
                                                                                <?php
                                                                                $result = $db->prepare("SELECT macrm.id, macrm.instituicao, macrm.endereco, macrm.numero, macrm.bairro, macrm.contato, macrm.nome, macrm.referencia, sf.nome AS funcao                       
                                                                                        FROM mod_acolhimento_crianca_rede_membros2 macrm  
                                                                                        LEFT JOIN seg_profissional_instituicao AS u ON u.id = macrm.nome  
                                                                                        LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id 
                                                                                        WHERE macrm.acolhimento_crianca_rede_id = ?");
                                                                                $result->bindValue(1, $rede_id);
                                                                                $result->execute();
                                                                                while ($membros = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                                    ?>
                                                                                    <tr id="remover_membro2">
                                                                                        <th><?= $membros['instituicao']; ?></th>
                                                                                        <td><?= $membros['endereco']; ?>, <?= $membros['numero']; ?> - <?= $membros['bairro']; ?></td>
                                                                                        <td><?= $membros['contato']; ?></td>
                                                                                        <td><?= pesquisar("nome", "seg_profissional_instituicao", "id", "=", $membros['nome'], ""); ?></td>
                                                                                        <td><?= $membros['referencia']; ?></td>
                                                                                        <td width="100px">
                                                                                            <a style="cursor: pointer" onclick="editar2(<?= $membros['id']; ?>, '<?= $membros['instituicao']; ?>', '<?= $membros['endereco']; ?>', '<?= $membros['numero']; ?>', '<?= $membros['bairro']; ?>', '<?= $membros['contato']; ?>', '<?= $membros['nome']; ?>', '<?= $membros['referencia']; ?>', '<?= $membros['funcao']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                                            <a style="cursor: pointer" onclick="remover2(this, <?= $membros['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </tbody>

                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row mt-3">
                                                                <div class="col-md-9">
                                                                    <div id="div_instituicao2" class="form-group">
                                                                        <label for="instituicao2">INSTITUIÇÃO / SERVIÇO / PROGRAMA</label>
                                                                        <input type="hidden" id="membro_id2" name="membro_id2" value=""/>
                                                                        <input type="text" class="form-control" name="instituicao2" id="instituicao2" placeholder="Instituição, Serviço ou Programa" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div id="div_contato2" class="form-group">
                                                                        <label for="contato2">CONTATO</label>
                                                                        <input type="text" class="form-control" data-mask="(99)99999-9999" name="contato2" id="contato2" placeholder="Celular, fixo..." value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <div id="div_endereco2" class="form-group">
                                                                        <label for="endereco2">ENDEREÇO</label>
                                                                        <input type="text" class="form-control" name="endereco2" id="endereco2" placeholder="Rua, Travessa, Avenida..." value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div id="div_numero2" class="form-group">
                                                                        <label for="numero2">NÚMERO</label>
                                                                        <input type="text" class="form-control" name="numero2" id="numero2" placeholder="Número" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div id="div_bairro2" class="form-group">
                                                                        <label for="bairro2">BAIRRO</label>
                                                                        <input type="text" class="form-control" name="bairro2" id="bairro2" placeholder="Bairro" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div id="div_nome_irmao2" class="form-group">
                                                                        <label for="nome_irmao2">NOME</label>
                                                                        <select name="nome_irmao2" id="nome_irmao2" class="form-control select2">
                                                                            <option value="">Selecione o profissional da rede</option>
                                                                            <?php
                                                                            $result8 = $db->prepare("SELECT u.id, u.funcao_id, u.nome, sf.nome AS funcao 
                                                                                                    FROM seg_profissional_instituicao u 
                                                                                                    LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id 
                                                                                                    WHERE u.status = 1");
                                                                            $result8->execute();
                                                                            while ($irm = $result8->fetch(PDO::FETCH_ASSOC)) {
                                                                                ?>
                                                                                <option funcao='<?= $irm['funcao']; ?>' value='<?= $irm['id']; ?>'><?= $irm['nome']; ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div id="div_funcao_profissional_rede_2" class="form-group">
                                                                        <label for="funcao_profissional_rede_2">FUNÇÃO</label>
                                                                        <input type="text" name="funcao_profissional_rede_2" id="funcao_profissional_rede_2" class="form-control" disabled />
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div id="div_referencia_contato2" class="form-group">
                                                                        <label for="referencia_contato2">METODOLOGIA DE ACOMPANHAMENTO</label>
                                                                        <textarea name="referencia_contato2" id="referencia_contato2" class="form-control" cols="30" rows="10"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 text-center">
                                                                    <button id="inserir_membro2" class="btn btn-success">INSERIR</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- FIM -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="row mb-2">
                <div class="col-md-12 text-center">
                    <button id="atualizar" type="button" class="btn btn-info">ATUALIZAR</button>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JAVASCRIPT BÁSICOS -->
<script src="<?= PORTAL_URL ?>assets/vendor_components/jquery-steps-master/build/jquery.steps.js"></script>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/pia/rede.js"></script>