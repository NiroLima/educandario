//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('#form_medicamento').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var medicamento = $('#medicamento').val();
            var quantidade = $('#quantidade').val();
            var validade = $('#validade').val();

            $.post(PORTAL_URL + "admin/dao/medicamentos/novo.php", {id: id, medicamento: medicamento, quantidade: quantidade, validade: validade}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário do Medicamento",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Medicamento",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/medicamentos/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    var id = $("#id").val();
    var medicamento = $('#medicamento').val();
    var quantidade = $('#quantidade').val();
    var validade = $('#validade').val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (validade == "") {
            $('div#div_validade').after('<label id="erro_validade" class="error">Validade é obrigatório.</label>');
            valido = false;
            element = $('div#div_validade');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (quantidade == "") {
            $('div#div_quantidade').after('<label id="erro_quantidade" class="error">Quantidade é obrigatório.</label>');
            valido = false;
            element = $('div#div_quantidade');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (medicamento == "") {
            $('div#div_medicamento').after('<label id="erro_medicamento" class="error">Nome do medicamento é obrigatório.</label>');
            valido = false;
            element = $('div#div_medicamento');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------