//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('#form_patrimonio').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var patrimonio = $('#patrimonio').val();
            var codigo = $('#codigo').val();
            var setor = $('#setor').val();
			var responsavel = $('#responsavel').val();
			var situacao = $('#situacao').val();

            $.post(PORTAL_URL + "admin/dao/patrimonios/novo.php", {id: id, patrimonio: patrimonio, codigo: codigo, setor: setor, responsavel: responsavel, situacao: situacao}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário do Patrimônio",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário do Patrimônio",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/patrimonios/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    var id = $("#id").val();
    var patrimonio = $('#patrimonio').val();
	var codigo = $('#codigo').val();
	var setor = $('#setor').val();
	var responsavel = $('#responsavel').val();
	var situacao = $('#situacao').val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS
	
	
        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (setor == "") {
            $('div#div_situacao').after('<label id="erro_situacao" class="error">Situação é obrigatório.</label>');
            valido = false;
            element = $('div#div_situacao');
        }
		
        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (responsavel == "") {
            $('div#div_responsavel').after('<label id="erro_responsavel" class="error">Responsável é obrigatório.</label>');
            valido = false;
            element = $('div#div_responsavel');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (setor == "") {
            $('div#div_setor').after('<label id="erro_setor" class="error">Setor é obrigatório.</label>');
            valido = false;
            element = $('div#div_setor');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (codigo == "") {
            $('div#div_codigo').after('<label id="erro_codigo" class="error">Código é obrigatório.</label>');
            valido = false;
            element = $('div#div_codigo');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (patrimonio == "") {
            $('div#div_patrimonio').after('<label id="erro_patrimonio" class="error">Nome do patrimônio é obrigatório.</label>');
            valido = false;
            element = $('div#div_patrimonio');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------