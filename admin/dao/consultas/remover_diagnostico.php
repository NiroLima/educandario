<?php

include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;

if ($error == false) {
    try {

        $db->beginTransaction();

        $sql2 = $db->prepare("DELETE FROM mod_saude_consulta_exame WHERE diagnostico_id = ?");
        $sql2->bindValue(1, $id);
        $sql2->execute();

        $sql3 = $db->prepare("DELETE FROM mod_saude_consulta_especialistas WHERE diagnostico_id = ?");
        $sql3->bindValue(1, $id);
        $sql3->execute();

        $sql4 = $db->prepare("DELETE FROM mod_saude_consulta_diagnostico WHERE id = ?");
        $sql4->bindValue(1, $id);
        $sql4->execute();

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>