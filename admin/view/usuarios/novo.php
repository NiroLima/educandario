<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- CSS DO PLUGIN DE UPLOAD DE FOTOS -->
<link href="<?= PORTAL_URL; ?>assets/vendor_components/cropper/css/cropper.min.css" rel="stylesheet">
<link href="<?= PORTAL_URL; ?>assets/vendor_components/cropper/css/main.css" rel="stylesheet">
<!-- FIM DO PLUGIN -->

<?php
$_SESSION['foto_cut'] = "";
$_SESSION['foto_origin'] = "";
unset($_SESSION['foto_cut']);
unset($_SESSION['foto_origin']);

$perfil = "";

$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT u.periodo, u.id, u.login, u.categoria_id, u.foto, u.cpf, u.cidade_id, u.nascimento, u.celular, u.status, u.nome, u.contato, u.email,
                 u.rua, u.bairro, u.numero, u.funcao_id, sf.nome AS funcao   
                 FROM seg_usuario u 
                 LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id
                 WHERE u.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_usuario = $result->fetch(PDO::FETCH_ASSOC);

    $usuario_id = $dados_usuario['id'];
    $usuario_nome = ($dados_usuario['nome']);
    $usuario_cpf = ($dados_usuario['cpf']);
    $usuario_login = ($dados_usuario['login']);
    $usuario_status = $dados_usuario['status'];
    $usuario_contato_cel = $dados_usuario['celular'];
    $usuario_contato_fixo = $dados_usuario['contato'];
    $usuario_nascimento = $dados_usuario['nascimento'];
    $usuario_email = $dados_usuario['email'];
    $usuario_foto = $dados_usuario['foto'];
    $usuario_rua = $dados_usuario['rua'];
    $usuario_bairro = $dados_usuario['bairro'];
    $usuario_numero = $dados_usuario['numero'];
    $usuario_municipio = $dados_usuario['cidade_id'];
    $usuario_estado = estado_municipio($usuario_municipio);
    $usuario_funcao = $dados_usuario['funcao_id'];
    $usuario_categoria = $dados_usuario['categoria_id'];
    $periodo = $dados_usuario['periodo'];
} else {
    $usuario_id = "";
    $usuario_nome = "";
    $usuario_cpf = "";
    $usuario_login = "";
    $usuario_status = 1;
    $usuario_contato_cel = "";
    $usuario_contato_fixo = "";
    $usuario_nascimento = "";
    $usuario_email = "";
    $usuario_rua = "";
    $usuario_bairro = "";
    $usuario_numero = "";
    $usuario_municipio = "";
    $usuario_estado = "";
    $usuario_foto = "";
    $usuario_funcao = "";
    $usuario_categoria = "";
    $periodo = "";
}

//if (!ver_nivel(1) && !ver_nivel(3) && $_SESSION['id'] != $usuario_id) {
//    echo "<script 'text/javascript'>history.go(-1);</script>";
//}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/usuarios/lista">Funcionários</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <!-- PERFIL -->
            <div class="box box-solid bg-info">
                <div class="box-header">
                    <h4 class="box-title"><i class="fal fa-user-circle"></i> <strong>PERFIL</strong></h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <fieldset>
                                <legend>Alterar Foto</legend>

                                <div id="crop-avatar">

                                    <!-- Current avatar -->
                                    <div class="avatar-view" title="Trocar o Foto">
                                        <?php
                                        if ($usuario_foto != "") {
                                            ?>
                                            <img src="<?= $usuario_foto ?>" alt="Avatar"/>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="#" class="photo_user"><img src="<?= PORTAL_URL ?>avatar/sem_foto.jpg" alt=""></a>
                                            <?php
                                        }
                                        ?>
                                    </div>

                                    <!-- Cropping modal -->
                                    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
                                                    <div class="modal-header">
                                                        <button class="close" data-dismiss="modal" type="button">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="avatar-body">

                                                            <!-- Upload image and data -->
                                                            <div class="avatar-upload">
                                                                <input class="avatar-src" name="avatar_src" type="hidden">
                                                                <input class="avatar-data" name="avatar_data" type="hidden">
                                                                <label for="avatarInput">Local upload</label>
                                                                <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
                                                            </div>

                                                            <!-- Crop and preview -->
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <div class="avatar-wrapper"></div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="avatar-preview preview-lg"></div>
                                                                    <div class="avatar-preview preview-md"></div>
                                                                    <div class="avatar-preview preview-sm"></div>
                                                                </div>
                                                            </div>

                                                            <div class="row avatar-btns">
                                                                <div class="col-md-9">
                                                                    <div class="btn-group">
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
                                                                    </div>

                                                                    <div class="btn-group" style="margin-top: 15px;">
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
                                                                        <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <button class="btn btn-primary btn-block avatar-save" type="submit">Salvar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="modal-footer">
                                                      <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                                                    </div> -->
                                                </form>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

                                    <!-- Loading state -->
                                    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                </div>

                            </fieldset>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="div_nome" class="form-group">
                                        <label for="nome">NOME</label>
                                        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome Completo" value="<?= $usuario_nome; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="div_cpf" class="form-group">
                                        <label for="cpf">CPF</label>
                                        <input type="text" class="form-control" data-mask="999.999.999-99" name="cpf" id="cpf" placeholder="CPF" value="<?= $usuario_cpf; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="div_nascimento" class="form-group">
                                        <label for="nascimento">NASCIMENTO</label>
                                        <input class="form-control" name="nascimento" id="nascimento" type="date" value="<?= $usuario_nascimento; ?>" />
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="div_email" class="form-group">
                                        <label for="email">E-MAIL</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="<?= $usuario_email; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="div_celular" class="form-group">
                                        <label for="celular">CELULAR</label>
                                        <input type="text" data-mask="(99) 9 9999-9999" class="form-control" name="celular" id="celular" placeholder="(99) 9 9999-9999" value="<?= $usuario_contato_cel; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="div_fixo" class="form-group">
                                        <label for="fixo">FIXO</label>
                                        <input type="text" data-mask="(99)9999-9999" class="form-control" name="fixo" id="fixo" placeholder="(99) 9999-9999" value="<?= $usuario_contato_fixo; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIM PERFIL -->

            <form id="form_usuario" name="form_usuario" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $usuario_id ?>"/>

                <!-- ENDEREÇO -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-home-lg"></i> <strong>ENDEREÇO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10">
                                <div id="div_rua" class="form-group">
                                    <label for="email">RUA</label>
                                    <input type="text" class="form-control" name="rua" id="rua" placeholder="Rua, Avenida e etc..." value="<?= $usuario_rua; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div id="div_numero" class="form-group">
                                    <label for="numero">NÚMERO</label>
                                    <input type="text" class="form-control" name="numero" id="numero" placeholder="1.260" value="<?= $usuario_numero; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_bairro" class="form-group">
                                    <label for="bairro">BAIRRO</label>
                                    <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro" value="<?= $usuario_bairro; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_estado_id" class="form-group">
                                    <label for="estado_id">ESTADO</label>
                                    <select name="estado_id" id="estado_id" class="form-control select2">
                                        <option value="">Selecione o estado</option>
                                        <?php
                                        $result8 = $db->prepare("SELECT nome, id, sigla
                                                     FROM bsc_estado
                                                     ORDER BY nome ASC");
                                        $result8->execute();
                                        while ($estado = $result8->fetch(PDO::FETCH_ASSOC)) {
                                            if ($usuario_estado == $estado['id']) {
                                                ?>
                                                <option selected="true" label='<?= $estado['sigla']; ?>' value='<?= $estado['id']; ?>'><?= $estado['nome']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option label='<?= $estado['sigla']; ?>' value='<?= $estado['id']; ?>'><?= $estado['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="div_municipio_id" class="form-group">
                                    <label for="municipio_id">CIDADE</label>
                                    <select name="municipio_id" id="municipio_id" class="form-control select2">
                                        <option value="">Selecione a cidade</option>
                                        <?php
                                        $result2 = $db->prepare("SELECT nome, id
                                                     FROM bsc_cidade 
                                                     WHERE 1 
                                                     ORDER BY nome ASC");
                                        $result2->execute();
                                        while ($municipio = $result2->fetch(PDO::FETCH_ASSOC)) {
                                            if ($usuario_municipio == $municipio['id']) {
                                                ?>
                                                <option selected="true" value='<?= $municipio['id']; ?>'><?= $municipio['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM ENDEREÇO -->

                <!-- PROFISSIONAL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-briefcase"></i> <strong>FUNCIONÁRIO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div id="div_funcao" class="form-group">
                                    <label for="categoria">CATEGORIA</label>
                                    <select name="categoria" id="categoria" class="form-control select2">
                                        <option value="">Selecione a Categoria</option>
                                        <option <?= $usuario_categoria == 1 ? "selected='true'" : "" ?> value="1">Quadro</option>
                                        <option <?= $usuario_categoria == 2 ? "selected='true'" : "" ?> value="2">Extra-Quadro</option>
                                        <option <?= $usuario_categoria == 3 ? "selected='true'" : "" ?> value="3">Voluntário</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_funcao" class="form-group">
                                    <label for="funcao">FUNÇÃO</label>
                                    <select name="funcao" id="funcao" class="form-control select2">
                                        <option value="">Selecione a função</option>
                                        <?php
                                        $setor_profissional = "";
                                        $result = $db->prepare("SELECT fc.id, fc.nome AS profissional, ss.nome AS setor          
                                                        FROM seg_funcao fc  
                                                        LEFT JOIN seg_setor AS ss ON ss.id = fc.setor_id  
                                                        WHERE fc.status = 1");
                                        $result->execute();
                                        while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
                                            if ($usuario_funcao == $prof['id']) {
                                                $setor_profissional = $prof['setor'];
                                                ?>
                                                <option selected="true" rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_periodo" class="form-group">
                                    <label for="periodo">PERÍODO</label>
                                    <select name="periodo" id="periodo" class="form-control select2">
                                        <option>Selecione o período</option>
                                        <option <?= $periodo == 1 ? "selected='true'" : ""; ?> value="1">MATUTINO</option>
                                        <option <?= $periodo == 2 ? "selected='true'" : ""; ?> value="2">VESPERTINO</option>
                                        <option <?= $periodo == 3 ? "selected='true'" : ""; ?> value="3">NOTURNO</option>
                                        <option <?= $periodo == 4 ? "selected='true'" : ""; ?> value="4">INTEGRAL</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PROFISSIONAL -->

                <!-- ACESSO -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-user-lock"></i> <strong>ACESSO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div id="div_login" class="form-group">
                                    <label for="login">LOGIN</label>
                                    <input type="text" class="form-control" name="login" id="login" placeholder="Login" value="<?= $usuario_login; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_senha" class="form-group">
                                    <label for="senha">SENHA</label>
                                    <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_conf_senha" class="form-group">
                                    <label for="confirmar_senha">CONFIRMAR SENHA</label>
                                    <input type="password" class="form-control" name="confirmar_senha" id="confirmar_senha" placeholder="Confirmar Senha">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div <?= ver_nivel(1, "") || ver_nivel(3, "") ? "" : "style='display: none'"; ?> class="box box-outline-info">
                            <div class="box-header with-border">
                                <h4 class="box-title text-info mb-0"><i class="fal fa-cubes"></i> <strong>NÍVEL DE ACESSO</strong></h4>
                            </div>
                            <div class="box-body">
                                <br>
                                <div id="div_permissao" class="row">
                                    <select id="nivel" name="nivel[]" class="form-control select2" multiple="true">
                                        <option value="">Escolha a permissão</option>
                                        <?php
                                        $result6 = $db->prepare("SELECT nome, nivel, descricao 
                                                     FROM seg_nivel 
                                                     WHERE 1 
                                                     ORDER BY nivel ASC");
                                        $result6->execute();
                                        while ($permissao = $result6->fetch(PDO::FETCH_ASSOC)) {
                                            if (is_numeric(pesquisar2("user_id", "seg_permissoes", "user_id", "=", $usuario_id, "nivel", "=", $permissao['nivel'], ""))) {
                                                ?>
                                                <option selected="true" value='<?= $permissao['nivel']; ?>'><?= $permissao['nome']; ?> - <?= $permissao['descricao']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value='<?= $permissao['nivel']; ?>'><?= $permissao['nome']; ?> - <?= $permissao['descricao']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM ACESSO -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $usuario_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $usuario_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JAVASCRIPT BÁSICOS -->
<script src="<?= PORTAL_URL; ?>assets/vendor_components/cropper/js/cropper.min.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/cropper/js/main.js"></script>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/usuarios/novo.js"></script>