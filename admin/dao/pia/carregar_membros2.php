<?php

//------------------------------------------------------------------------------
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$id = $_POST['id'];

$stmp = $db->prepare("SELECT macrm.id, macrm.instituicao, macrm.endereco, macrm.numero, macrm.bairro, macrm.contato, macrm.nome, macrm.referencia, sf.nome AS funcao                       
                     FROM mod_acolhimento_crianca_rede_membros2 macrm  
                     LEFT JOIN seg_profissional_instituicao AS u ON u.id = macrm.nome  
                     LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id  
                     WHERE macrm.acolhimento_crianca_rede_id = ?");
$stmp->bindValue(1, $id);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    
} else {
    while ($membros = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr id="remover_membro2">
                     <td>' . $membros['instituicao'] . '</td>
                     <td>' . $membros['endereco'] . ', ' . $membros['numero'] . ' - ' . $membros['bairro'] . '</td>
                     <td>' . $membros['contato'] . '</td>
                     <td>' . pesquisar("nome", "seg_profissional_instituicao", "id", "=", $membros['nome'], "") . '</td>
                     <td>' . $membros['referencia'] . '</td>
                     <td width="100px">
                        <a style="cursor: pointer" onclick="editar2(' . $membros['id'] . ', ' . "'" . $membros['instituicao'] . "'" . ', ' . "'" . $membros['endereco'] . "'" . ', ' . "'" . $membros['numero'] . "'" . ', ' . "'" . $membros['bairro'] . "'" . ', ' . "'" . $membros['contato'] . "'" . ', ' . "'" . $membros['nome'] . "'" . ', ' .  "'" . $membros['referencia'] . "'". ', ' . "'" . $membros['funcao'] . "'" . ')" class="text-warning"><i class="fa fa-pencil"></i></a>
                        <a style="cursor: pointer" onclick="remover2(this, ' . $membros['id'] . ')" class="text-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>';
    }
}
//------------------------------------------------------------------------------
?>

