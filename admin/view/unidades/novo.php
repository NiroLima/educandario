<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT * 
                 FROM mod_unidade_saude mus  
                 WHERE mus.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_unidade = $result->fetch(PDO::FETCH_ASSOC);

    $unidade_id = $dados_unidade['id'];
    $unidade_nome = $dados_unidade['nome'];
    $unidade_contato = $dados_unidade['contato'];
    $unidade_endereco = $dados_unidade['endereco'];
    $unidade_bairro = $dados_unidade['bairro'];
    $unidade_numero = $dados_unidade['numero'];
} else {
    $unidade_id = "";
    $unidade_nome = "";
    $unidade_contato = "";
    $unidade_endereco = "";
    $unidade_bairro = "";
    $unidade_numero = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/unidades/lista">Locais de Atendimento</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_unidade" name="form_unidade" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $unidade_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-clinic-medical"></i> <strong>NOVO LOCAL DE ATENDIMENTO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- UNIDADE DE SAÚDE -->
                                <div class="box box-outline-primary mt-3">
                                    <div class="box-header">
                                        <strong>LOCAL</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-8">
                                                <div id="div_unidade_saude" class="form-group">
                                                    <label for="unidade_saude">NOME DO LOCAL</label>
                                                    <input type="text" class="form-control" name="unidade_saude" id="unidade_saude" placeholder="Local de Atendimento" value="<?= $unidade_nome; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div id="div_contato_unidade_saude" class="form-group">
                                                    <label for="contato_unidade_saude">Contato</label>
                                                    <input type="text" data-mask="(99) 9 9999-9999" class="form-control" name="contato_unidade_saude" id="contato_unidade_saude" placeholder="Contato" value="<?= $unidade_contato; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-6">
                                                <div id="div_endereco_unidade_saude" class="form-group">
                                                    <label for="endereco_unidade_saude">ENDEREÇO</label>
                                                    <input type="text" class="form-control" name="endereco_unidade_saude" id="endereco_unidade_saude" placeholder="Travessa, Rua, Avenida..." value="<?= $unidade_endereco; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div id="div_numero_unidade_saude" class="form-group">
                                                    <label for="numero_unidade_saude">Número</label>
                                                    <input type="text" class="form-control" onkeypress="return SomenteNumero(event);" name="numero_unidade_saude" id="numero_unidade_saude" placeholder="Número" value="<?= $unidade_numero; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div id="div_bairro_unidade_saude" class="form-group">
                                                    <label for="bairro_unidade_saude">BAIRRO</label>
                                                    <input type="text" class="form-control" name="bairro_unidade_saude" id="bairro_unidade_saude" placeholder="Bairro" value="<?= $unidade_bairro; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIM UNIDADE DE SAÚDE -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $unidade_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $unidade_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/unidades/novo.js"></script>