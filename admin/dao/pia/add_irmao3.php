<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$nome = isset($_POST['nome']) && $_POST['nome'] != "" ? $_POST['nome'] : NULL;
$idade = isset($_POST['idade']) && $_POST['idade'] != "" ? $_POST['idade'] : NULL;

$irmao_id = isset($_POST['irmao_id']) && $_POST['irmao_id'] != "" ? $_POST['irmao_id'] : "";

$codigo_irmao = pesquisar("id", "mod_acolhimento_crianca_irmaos", "nome", "=", $nome, "AND tipo = 3");

if (is_numeric($codigo_irmao) && $irmao_id != $codigo_irmao) {
    $error = true;
    $msg['msg'] = 'error';
    $msg['retorno'] = "O irmão escolhido já está adicionado na lista";
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($irmao_id)) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_irmaos SET nome = ?, idade = ?, responsavel_id = ? WHERE id = ?");
            $sql4->bindValue(1, $nome);
            $sql4->bindValue(2, $idade);
            $sql4->bindValue(3, $_SESSION['id']);
            $sql4->bindValue(4, $irmao_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_irmaos (familia_id, nome, idade, tipo, responsavel_id, data_cadastro) VALUES (?, ?, ?, 3, ?, NOW())");
            $sql4->bindValue(1, $id);
            $sql4->bindValue(2, $nome);
            $sql4->bindValue(3, $idade);
            $sql4->bindValue(4, $_SESSION['id']);
            $sql4->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
} else {
    echo json_encode($msg);
    exit();
}
?>