//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("button#atualizar").click(function () {
        $("form#form_educacao").submit();
    });

    $("#sim_apresenta_desenvolvimento").click(function () {
        $("textarea#descreva_aspectos").val("");
        $("div#div_aspectos2").show();
    });

    $("#nao_apresenta_desenvolvimento").click(function () {
        $("textarea#descreva_aspectos").val("");
        $("div#div_aspectos2").hide();
    });

    $("#sim_rotinas_estabelecidas").click(function () {
        $("div#div_sea").show();
        $("#div_justifique2").hide();
    });

    $("#nao_rotinas_estabelecidas").click(function () {
        $("div#div_sea").hide();
        $("#div_justifique2").show();
    });

    $("#sim_idade_escolar").click(function () {
        $("#div_escolar").show();
        $("#div_freq").show();
        $("#div_social").show();
        $("#div_inae").show();
        $("#div_rend").show();
        $("#div_part").show();
    });

    $("#nao_idade_escolar").click(function () {
        $("#div_escolar").hide();
        $("#div_freq").hide();
        $("#div_social").hide();
        $("#div_inae").hide();
        $("#div_rend").hide();
        $("#div_part").hide();
    });

    $("#sim_idade_escolar_atual").click(function () {
        $("#div_info_escola").show();
        $("#div_rotinas").show();
        $("#div_sea").show();
        $("#div_contexto_geral").show();
        $("#div_relacao_escola").show();
        $("#div_dre").show();
        $("#div_intervencoes_inciais").show();

        $("#div_crianca_apresenta").hide();
        $("#div_estrutura").hide();
        $("#div_ha_espaco").hide();
    });

    $("#nao_idade_escolar_atual").click(function () {
        $("#div_info_escola").hide();
        $("#div_rotinas").hide();
        $("#div_sea").hide();
        $("#div_contexto_geral").hide();
        $("#div_relacao_escola").hide();
        $("#div_dre").hide();
        $("#div_intervencoes_inciais").hide();

        $("#div_crianca_apresenta").show();
        $("#div_estrutura").show();
        $("#div_ha_espaco").show();
    });

    $("#sim_frequenta_instituicao").click(function () {
        $("#div_instituicao").hide();
    });

    $("#nao_frequenta_instituicao").click(function () {
        $("#div_instituicao").show();
    });

    $("#sim_estrutura").click(function () {
        $("#div_estrutura").show();
        $("#div_aspectos").hide();
    });

    $("#nao_estrutura").click(function () {
        $("#div_estrutura").hide();
        $("#div_aspectos").show();
    });

    $('#form_responsavel').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var responsavel_id = $("#responsavel_id").val();

        $.post(PORTAL_URL + "admin/dao/pia/acolhido_responsavel.php", {id: id, responsavel_id: responsavel_id}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário PIA",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário PIA",
                    text: "Informações do acolhido atualizadas com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/educacao/' + id);
                });
            }
        }
        , "html");
        return false;
    });

    $("#inserir_acomp").click(function () {

        var id = $("#educacao_id").val();
        var qual_atividade = $("#qual_atividade").val();
        var nome_acompanha = $("#nome_acompanha").val();
        var qual_forma = $("#qual_forma").val();
        var frequencia_acompanhamento = $("#frequencia_acompanhamento").val();

        var acomp_id = $("#acomp_id").val();

        if (formulario_validator("")) {
            $.post(PORTAL_URL + "admin/dao/pia/acompanhamentos.php", {id: id, acomp_id: acomp_id, qual_atividade: qual_atividade, nome_acompanha: nome_acompanha, qual_forma: qual_forma, frequencia_acompanhamento: frequencia_acompanhamento}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Acompanhamento",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Acompanhamento",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_acompanhamentos.php",
                                {id: id},
                                function (valor) {
                                    $("input#acomp_id").val("");
                                    $("input#qual_atividade").val("");
                                    $("button#inserir_acomp").html("INSERIR");
                                    $("input#nome_acompanha").val("");
                                    $("input#qual_forma").val("");
                                    $("input#frequencia_acompanhamento").val("");
                                    $("tbody#resultado_acompanhamento").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_acomp2").click(function () {

        var id = $("#educacao_id").val();
        var nome_acompanha = $("#nome_acompanha2").val();
        var qual_forma = $("#qual_forma2").val();
        var frequencia_acompanhamento = $("#frequencia_acompanhamento2").val();

        var acomp_id = $("#acomp_id2").val();

        if (formulario_validator2("")) {
            $.post(PORTAL_URL + "admin/dao/pia/rotinas.php", {id: id, acomp_id: acomp_id, nome_acompanha: nome_acompanha, qual_forma: qual_forma, frequencia_acompanhamento: frequencia_acompanhamento}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Rotina",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Rotina",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_rotinas.php",
                                {id: id},
                                function (valor) {
                                    $("input#acomp_id2").val("");
                                    $("button#inserir_acomp2").html("INSERIR");
                                    $("input#nome_acompanha2").val("");
                                    $("input#qual_forma2").val("");
                                    $("input#frequencia_acompanhamento2").val("");
                                    $("tbody#resultado_rotinas").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $('#form_educacao').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var educacao_id = $("#educacao_id").val();
        var sim_idade_escolar = $("#sim_idade_escolar:checked").val() == 1 ? 1 : 0;
        var escola = $("#escola").val();
        var serie = $("#serie").val();
        var turno = $("#turno").val();
        var escola1 = $("#escola1").val();
        var serie1 = $("#serie1").val();
        var turno1 = $("#turno1").val();
        var sim_frequenta_instituicao = $("#sim_frequenta_instituicao:checked").val() == 1 ? 1 : 0;
        var justificativa_ausencia = $("#justificativa_ausencia").val();
        var frequencia = $("#frequencia").val();
        var sobre_frequencia = $("#sobre_frequencia").val();
        var socializacao = $("#socializacao").val();
        var sobre_socializacao = $("#sobre_socializacao").val();
        var interesse_atividade_escolar = $("#interesse_atividade_escolar").val();
        var sobre_interesse_atividade_escolar = $("#sobre_atividades_escolares").val();
        var rendimento = $("#rendimento").val();
        var sobre_rendimento = $("#sobre_rendimento").val();
        var participacao_familia = $("#participacao_familia").val();
        var sobre_participacao_familia = $("#sobre_participacao_familia").val();
        var sim_apresenta_desenvolvimento = $("#sim_apresenta_desenvolvimento:checked").val() == 1 ? 1 : 0;
        var descreva_aspectos = $("#descreva_aspectos").val();
        var contexto_geral = $("#contexto_geral").val();
        var sim_vontade = $("#sim_vontade:checked").val() == 1 ? 1 : 0;
        var descricao_relacao_escola = $("#descricao_relacao_escola").val();
        var descricao_intervencoes = $("#descricao_intervencoes").val();
        var sim_estrutura = $("#sim_estrutura:checked").val() == 1 ? 1 : 0;
        var sim_rotinas_estabelecidas = $("#sim_rotinas_estabelecidas:checked").val() == 1 ? 1 : 0;
        var sim_idade_escolar_atual = $("#sim_idade_escolar_atual:checked").val() == 1 ? 1 : 0;
        var justique_ausencia = $("#justique_ausencia").val();
        var justifique = $("#justifique").val();
        var justifique2 = $("#justifique2").val();

        $.post(PORTAL_URL + "admin/dao/pia/educacao.php", {id: id, justifique: justifique, justifique2: justifique2, justique_ausencia: justique_ausencia, sim_idade_escolar_atual: sim_idade_escolar_atual, educacao_id: educacao_id, sim_idade_escolar: sim_idade_escolar, escola: escola, serie: serie, turno: turno, escola1: escola1, serie1: serie1, turno1: turno1,
            sim_frequenta_instituicao: sim_frequenta_instituicao, justificativa_ausencia: justificativa_ausencia, frequencia: frequencia, sobre_frequencia: sobre_frequencia,
            socializacao: socializacao, sobre_socializacao: sobre_socializacao, interesse_atividade_escolar: interesse_atividade_escolar, sobre_interesse_atividade_escolar: sobre_interesse_atividade_escolar,
            rendimento: rendimento, sobre_rendimento: sobre_rendimento, participacao_familia: participacao_familia, sobre_participacao_familia: sobre_participacao_familia,
            sim_apresenta_desenvolvimento: sim_apresenta_desenvolvimento, descreva_aspectos: descreva_aspectos, contexto_geral: contexto_geral, sim_vontade: sim_vontade,
            descricao_relacao_escola: descricao_relacao_escola, descricao_intervencoes: descricao_intervencoes, sim_estrutura: sim_estrutura, sim_rotinas_estabelecidas: sim_rotinas_estabelecidas}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário de Educação",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário de Educação",
                    text: "Ação realizada com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/educacao/' + id);
                });
            }
        }
        , "html");
        return false;

    });

});
//------------------------------------------------------------------------------
function editar(id, qual_atividade, quem_realiza, forma_realiza, frequencia) {
    $("input#acomp_id").val(id);
    $("input#qual_atividade").val(qual_atividade);
    $("input#nome_acompanha").val(quem_realiza);
    $("input#qual_forma").val(forma_realiza);
    $("input#frequencia_acompanhamento").val(frequencia);
    $("button#inserir_acomp").html("ALTERAR");
}
//------------------------------------------------------------------------------
function remover(obj, id) {
    swal({
        title: "Deseja mesmo remover este acompanhamento?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do acompanhamento!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_acomp",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend3(obj),
            error: onSuccessSend3(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {
    swal({
        title: "Sucesso!",
        text: "Acompanhamento removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#acomp_id").val("");
        $("button#inserir_acomp").html("INSERIR");
        $("input#nome_acompanha").val("");
        $("input#qual_forma").val("");
        $("input#frequencia_acompanhamento").val("");
        $(obj).parents('tr#remover_acompanhamento').remove();
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var nome_acompanha = $("#nome_acompanha").val();
    var qual_forma = $("#qual_forma").val();
    var frequencia_acompanhamento = $("#frequencia_acompanhamento").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (frequencia_acompanhamento == "") {
            $('div#div_frequencia_acompanhamento').after('<label id="erro_frequencia_acompanhamento" class="error">Frequência é obrigatório.</label>');
            valido = false;
            element = $('div#div_frequencia_acompanhamento');
        }

        if (qual_forma == "") {
            $('div#div_qual_forma').after('<label id="erro_qual_forma" class="error">Forma é obrigatório.</label>');
            valido = false;
            element = $('div#div_qual_forma');
        }

        if (nome_acompanha == "") {
            $('div#div_nome_acompanha').after('<label id="erro_nome_acompanha" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_acompanha');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
function editar2(id, quem_realiza, forma_realiza, frequencia) {
    $("input#acomp_id2").val(id);
    $("input#nome_acompanha2").val(quem_realiza);
    $("input#qual_forma2").val(forma_realiza);
    $("input#frequencia_acompanhamento2").val(frequencia);
    $("button#inserir_acomp2").html("ALTERAR");
}
//------------------------------------------------------------------------------
function remover2(obj, id) {
    swal({
        title: "Deseja mesmo remover está rotina?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados da rotina!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode cancelar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_rotinas",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend2(obj),
            error: onSuccessSend2(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend2(obj) {
    swal({
        title: "Sucesso!",
        text: "Rotina removida com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#acomp_id2").val("");
        $("button#inserir_acomp2").html("INSERIR");
        $("input#nome_acompanha2").val("");
        $("input#qual_forma2").val("");
        $("input#frequencia_acompanhamento2").val("");
        $(obj).parents('tr#remover_rotinas').remove();
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError2(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator2(obj) {
    var valido = true;

    var nome_acompanha = $("#nome_acompanha2").val();
    var qual_forma = $("#qual_forma2").val();
    var frequencia_acompanhamento = $("#frequencia_acompanhamento2").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (frequencia_acompanhamento == "") {
            $('div#div_frequencia_acompanhamento2').after('<label id="erro_frequencia_acompanhamento2" class="error">Frequência é obrigatório.</label>');
            valido = false;
            element = $('div#div_frequencia_acompanhamento2');
        }

        if (qual_forma == "") {
            $('div#div_qual_forma2').after('<label id="erro_qual_forma2" class="error">Forma é obrigatório.</label>');
            valido = false;
            element = $('div#div_qual_forma2');
        }

        if (nome_acompanha == "") {
            $('div#div_nome_acompanha2').after('<label id="erro_nome_acompanha2" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_acompanha2');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function validar(codigo) {
    swal({
        title: "Deseja mesmo confirmar a Etapa 4 Educação?",
        text: "Obs: Caso escolha confirmar, o sistema irá identificar essa etapa como concluída!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode confirmar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/confirmar_acolhido",
            type: "POST",
            data: {id: codigo, op: 4},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------------------------------
function desvalidar(codigo) {
    swal({
        title: "Deseja mesmo desvalidar a Etapa 4 Educação?",
        text: "Obs: Caso escolha desvalidar, o sistema irá identificar essa etapa como pendente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode desvalidar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/desvalidar_acolhido",
            type: "POST",
            data: {id: codigo, op: 4},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    var codigo = $("#id").val();

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/educacao/' + codigo);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
function onSuccessSend3(obj) {

    swal({
        title: "Sucesso!",
        text: "Acompanhamento removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#acomp_id").val("");
        $("input#qual_atividade").val("");
        $("button#inserir_acomp").html("INSERIR");
        $("input#nome_acompanha").val("");
        $("input#qual_forma").val("");
        $("input#frequencia_acompanhamento").val("");
        $(obj).parents('tr#remover_acompanhamento').remove();
    });

    return false;
}
//------------------------------------------------------------------------------------------------------
function editar_responsavel() {
    $("div#responsavel_edite_1").hide();
    $("div#responsavel_edite_2").show();
}
//------------------------------------------------------------------------------  
function gerar_pdf() {
    //pega o Html da DIV
    var divElements = document.getElementById("impressao").innerHTML;
    //pega o HTML de toda tag Body
    var oldPage = document.body.innerHTML;
    //Alterna o body 
    document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
    //Imprime o body atual
    window.print();
    //Retorna o conteudo original da página. 
    document.body.innerHTML = oldPage;
}
//------------------------------------------------------------------------------
