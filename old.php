<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<input type="hidden" id="qtd_mancio_lima" name="qtd_mancio_lima" value="105"/>
<input type="hidden" id="qtd_rodrigues_alves" name="qtd_rodrigues_alves" value="78"/>
<input type="hidden" id="qtd_cruzeiro_sul" name="qtd_cruzeiro_sul" value="1.837"/>
<input type="hidden" id="qtd_porto_walter" name="qtd_porto_walter" value="26"/>
<input type="hidden" id="qtd_tarauaca" name="qtd_tarauaca" value="552"/>
<input type="hidden" id="qtd_marechal_thaumaturgo" name="qtd_marechal_thaumaturgo" value="128"/>
<input type="hidden" id="qtd_jordao" name="qtd_jordao" value="27"/>
<input type="hidden" id="qtd_feijo" name="qtd_feijo" value="187"/>
<input type="hidden" id="qtd_santa_rosa" name="qtd_santa_rosa" value="103"/>
<input type="hidden" id="qtd_manoel_urbano" name="qtd_manoel_urbano" value="43"/>
<input type="hidden" id="qtd_sena_madureira" name="qtd_sena_madureira" value="75"/>
<input type="hidden" id="qtd_assis_brasil" name="qtd_assis_brasil" value="1.42"/>
<input type="hidden" id="qtd_epitaciolandia" name="qtd_epitaciolandia" value="36"/>
<input type="hidden" id="qtd_brasileia" name="qtd_brasileia" value="1.500"/>
<input type="hidden" id="qtd_xapuri" name="qtd_xapuri" value="15"/>
<input type="hidden" id="qtd_capixaba" name="qtd_capixaba" value="2.030"/>
<input type="hidden" id="qtd_rio_branco" name="qtd_rio_branco" value="4.500"/>
<input type="hidden" id="qtd_bujari" name="qtd_bujari" value="65"/>
<input type="hidden" id="qtd_porto_acre" name="qtd_porto_acre" value="23"/>
<input type="hidden" id="qtd_placido_castro" name="qtd_placido_castro" value="251"/>
<input type="hidden" id="qtd_acrelandia" name="qtd_acrelandia" value="83"/>
<input type="hidden" id="qtd_senador_guiomard" name="qtd_senador_guiomard" value="66"/>

<input type="hidden" id="acre_populacao" name="acre_populacao" populacao="874.387" confirmados="11.381" ativos="5.194" obitos="300" realizados="24.784" teste_100="2.538,802" casos_100="972,353" teste_positivo="40.358" casos_ativos="40,358"/>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <ul class="legend">
                <li class="emergencia"><img src="images/bandeiras/emergencia.svg" alt="">EMERGÊNCIA</li>
                <li class="alerta"><img src="images/bandeiras/alerta.svg" alt="">ALERTA</li>
                <li class="atencao"><img src="images/bandeiras/atencao.svg" alt="">ATENÇÃO</li>
                <li class="cuidado"><img src="images/bandeiras/cuidado.svg" alt="">CUIDADO</li>
            </ul>
            <hr>
            <div class="estado">
                <h1 id="nomedomunicipio"><img src="images/bandeiras/emergencia.svg" alt=""><br> ACRE</h1>
                <div class="populacao">
                    <h6>POPULAÇÃO</h6>
                    <h4 id="populacao">874.387</h4>
                </div>
            </div>
            <div class="map">
                <svg version="1.1" id="mapadoacre" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 555" style="enable-background:new 0 0 1000 555;" xml:space="preserve">
                <style type="text/css">
                    .st0{fill:#CDDAF9;stroke:#064BF9;stroke-miterlimit:10;}
                    .st1{font-family: "IBM Plex Sans", sans-serif; font-size: 12px !important;}
                    .emergencia{ fill:#F3E1E6;stroke:#E9363E;stroke-miterlimit:10; }
                    .emergencia:hover{ fill:#eac5cf; }
                    .alerta{ fill:#F4EAE6;stroke:#F28640;stroke-miterlimit:10; }
                    .alerta:hover{ fill:#efd5cb; }
                    .atencao{ fill:#F5F1E6;stroke:#FDCC3F;stroke-miterlimit:10; }
                    .atencao:hover{ fill:#f2e2ba; }
                    .cuidado{ fill:#DDEDE9;stroke:#24AB65;stroke-miterlimit:10; }
                    .cuidado:hover{ fill:#b7e5d9; }
                </style>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Mâncio Lima" populacao="150" confirmados="181" ativos="154" obitos="220" realizados="1.784" teste_100="521,802" casos_100="422,342" teste_positivo="34.234" casos_ativos="20,238" href="#" data-target="#municipio_88">
                    <g>
                    <polygon class="cuidado" points="34.4,10.2 152.8,55.5 153.1,60.5 165.1,80.8 132.8,82.5 102.8,98.3 92.5,89.9 87.2,92.5 67.5,89.9 
                             45.2,93.9 39,89.7 23.8,84 6.7,66.6 13.5,59.8 9.5,40.4 23.3,43.9 27.7,38.4 45.1,35.4 			"/>
                    </g>
                </a>
                <text id="rs_mancio_lima" transform="matrix(1 0 0 1 66.9033 61.3228)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Rodrigues Alves" populacao="35" confirmados="125" ativos="312" obitos="621" realizados="1.361" teste_100="351,23" casos_100="312,342" teste_positivo="34.321" casos_ativos="20,231" href="#" data-target="#municipio_95">
                    <g>
                    <polygon class="cuidado" points="45.2,93.9 67.5,89.9 87.2,92.5 92.5,89.9 102.8,98.3 132.8,82.5 165.1,80.8 174,88.1 185.9,88.1 
                             187.9,101.2 168.4,111.7 129.8,119.1 122.8,112.6 109.1,114.3 97.1,124.3 82.2,129.8 57,133.4 45,119.5 38,116.4 36.5,109.1 
                             45.8,107 			"/>
                    </g>
                </a>
                <text id="rs_rodrigues_alves" transform="matrix(1 0 0 1 72.1934 113.1611)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Cruzeiro do Sul" populacao="28" confirmados="312" ativos="351" obitos="231" realizados="2.512" teste_100="231,512" casos_100="312,523" teste_positivo="34.642" casos_ativos="20,511" href="#" data-target="#municipio_84">
                    <g>
                    <polygon class="emergencia" points="278.3,80.4 193.7,69.5 152.8,55.5 153.1,60.5 165.1,80.8 174,88.1 185.9,88.1 187.9,101.2 
                             168.4,111.7 129.8,119.1 122.8,112.6 109.1,114.3 97.1,124.3 82.2,129.8 57,133.4 59.3,158.9 70.2,180.2 93.7,191.4 162.7,144.4 
                             172.6,154.3 194.8,149.9 219.6,159.6 234.8,153.3 240.6,128.4 270.9,102.8 			"/>
                    </g>
                </a>
                <text id="rs_cruzeiro_sul" transform="matrix(1 0 0 1 189.2256 125.9353)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Porto Walter" populacao="22" confirmados="512" ativos="312" obitos="622" realizados="1.612" teste_100="312,123" casos_100="222,212" teste_positivo="34.512" casos_ativos="20,612" href="#" data-target="#municipio_93">
                    <g>
                    <polygon class="cuidado" points="92.3,207 101.8,218.7 121.4,227.5 154.5,225.9 175.2,217.2 187.1,227.5 232.8,226.7 242.7,220.9 
                             234.8,153.3 219.6,159.6 194.8,149.9 172.6,154.3 162.7,144.4 93.7,191.4 			"/>
                    </g>
                </a>
                <text id="rs_porto_walter" transform="matrix(1 0 0 1 167.1611 193)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Tarauacá" populacao="85" confirmados="512" ativos="231" obitos="512" realizados="1.621" teste_100="312,231" casos_100="312,342" teste_positivo="34.123" casos_ativos="20,122" href="#" data-target="#municipio_99">
                    <g>
                    <polygon class="emergencia" points="471.2,101.5 278.3,80.4 270.9,102.8 240.6,128.4 234.7,152.9 242.7,220.9 253,224.6 253.9,238.8 
                             263.2,241.9 274.3,231.2 297,239.3 329.2,213.9 334.3,225.1 323.5,239 339.4,245.5 395.9,228.6 417.7,207.5 436,215.4 
                             465.4,157.8 466,134.6 456.1,118.6 			"/>
                    </g>
                </a>
                <text id="rs_tarauaca" transform="matrix(1 0 0 1 337.1611 155.0322)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Marechal Thaumaturgo" populacao="43" confirmados="312" ativos="351" obitos="231" realizados="2.512" teste_100="231,512" casos_100="312,523" teste_positivo="34.642" casos_ativos="20,511" href="#" data-target="#municipio_90">
                    <g>
                    <polygon class="atencao" points="122.6,237.5 130.3,247.5 139.6,250.4 149.1,267.8 141.8,281.8 137.5,289.5 127.9,293.2 110.9,315 
                             174.4,315.1 228.6,329.6 251,312.9 265.6,288.5 267.7,263.2 263.2,241.9 253.9,238.8 253,224.6 242.7,220.9 232.8,226.7 
                             187.1,227.5 175.2,217.2 154.5,225.9 121.4,227.5 			"/>
                    </g>
                </a>
                <text id="rs_marechal_thaumaturgo" transform="matrix(1 0 0 1 192.2905 274.3872)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Jordão" populacao="88" confirmados="312" ativos="351" obitos="231" realizados="2.512" teste_100="231,512" casos_100="312,523" teste_positivo="34.642" casos_ativos="20,511" href="#" data-target="#municipio_87">
                    <g>
                    <polygon class="cuidado" points="234.4,357.6 251.2,368.8 285.3,350.2 321,290.5 339.4,245.5 323.5,239 334.3,225.1 329.2,213.9 
                             297,239.3 274.3,231.2 263.2,241.9 267.7,263.2 265.6,288.5 251,312.9 228.6,329.6 236,343.8 			"/>
                    </g>
                </a>
                <text id="rs_jordao" transform="matrix(1 0 0 1 279.4839 295.3545)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Feijó" populacao="123" confirmados="123" ativos="521" obitos="123" realizados="512" teste_100="123,312" casos_100="15,231" teste_positivo="11.231" casos_ativos="12,323" href="#" data-target="#municipio_86">
                    <g>
                    <polygon class="alerta" points="471.2,101.5 456.1,118.6 466,134.6 465.4,157.8 436,215.4 417.7,207.5 395.9,228.6 339.4,245.5 
                             321,290.5 300.6,325.1 285.3,350.2 251.2,369.1 248.7,399 353.6,399 395.2,370.4 398.9,346.4 423.7,320.4 435,321.7 437,316 
                             455.1,310 457.2,296.1 471.2,276.3 480.8,274.1 493.1,257 560.3,256 571.3,235.8 586.9,220.4 597.4,216.8 604.9,198.2 
                             617.3,176.7 532.6,107.2 			"/>
                    </g>
                </a>
                <text id="rs_feijo" transform="matrix(1 0 0 1 428.0967 252.7095)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Santa Rosa Do Purus" populacao="654" confirmados="512" ativos="123" obitos="231" realizados="2.123" teste_100="312,512" casos_100="322,523" teste_positivo="22.642" casos_ativos="12,511" href="#" data-target="#municipio_96">
                    <g>
                    <polygon class="atencao" points="407.6,366.8 463,319.3 473.5,318.2 472.8,325.3 463.1,338 470.7,358.1 458,370.4 458,402.3 
                             523.5,333 537.6,327.8 498.4,312.4 503.4,301.7 523.2,296.6 536.5,297.7 553.8,291.7 568.5,273.8 560.3,256 493.1,257 
                             480.8,274.1 471.2,276.3 457.2,296.1 455.1,310 437,316 435,321.7 423.7,320.3 398.9,346.3 395.2,370.2 			"/>
                    </g>
                </a>
                <text id="rs_santa_rosa" transform="matrix(1 0 0 1 503 282.5161)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Manoel Urbano" populacao="854" confirmados="111" ativos="231" obitos="311" realizados="322" teste_100="11,231" casos_100="312,523" teste_positivo="23.123" casos_ativos="1,232" href="#" data-target="#municipio_89">
                    <g>
                    <polygon class="cuidado" points="617.3,176.7 605,197.9 597.4,216.8 586.9,220.4 571.3,235.8 560.3,256 568.5,273.8 553.8,291.7 
                             536.5,297.7 523.3,296.6 503.5,301.7 498.4,312.4 537.7,327.8 523.7,333 458.1,402.4 458.8,450.5 506.7,449.8 511.6,431.6 
                             523.8,417.4 536.7,392 536.1,375 545.4,355.4 559.7,319.6 569.2,306.3 592.9,286.3 632.3,264.6 653.4,250.9 660.6,252.7 
                             666.3,246.2 685.2,234.9 			"/>
                    </g>
                </a>
                <text id="rs_manoel_urbano" transform="matrix(1 0 0 1 598.3871 248.645)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Sena Madureira" populacao="230" confirmados="23" ativos="11" obitos="15" realizados="12" teste_100="11,323" casos_100="22,311" teste_positivo="11.323" casos_ativos="12,312" href="#" data-target="#municipio_97">
                    <g>
                    <polygon class="emergencia" points="458.8,450.5 459.2,486.4 514.7,484.9 597.2,442.7 607.5,461 626.1,457.7 631.5,455.8 627.9,452.5 
                             632.3,443.3 662.4,425.5 666.5,418.9 718.1,373.8 715.7,350.1 726.6,328.9 752.5,333.4 764.6,319.1 755.6,302.7 779.4,292.8 
                             723.1,266.1 685.2,234.9 666.3,246.2 660.6,252.7 653.4,250.9 632.3,264.6 592.9,286.3 569.2,306.3 559.7,319.6 545.4,355.4 
                             536.1,375 536.7,392 523.8,417.4 511.6,431.6 506.7,449.8 			"/>
                    </g>
                </a>
                <text id="rs_sena_madureira" transform="matrix(1 0 0 1 621.8711 351.0645)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Assis Brasil" populacao="210" confirmados="312" ativos="351" obitos="231" realizados="2.512" teste_100="231,512" casos_100="312,523" teste_positivo="34.642" casos_ativos="15,511" href="#" data-target="#municipio_80">
                    <g>
                    <polygon class="alerta" points="459.8,523.6 469.2,522.2 485.4,531.5 501.6,535.4 527.8,524.7 550.6,515.9 577.7,521.8 619.7,516.2 
                             623.1,504.8 622.6,491 610,486.6 610.7,497.5 604.1,509.2 573,503.4 573,491.8 584.8,490 607.7,468.7 607.5,461 597.1,442.7 
                             514.7,484.9 459.2,486.4 			"/>
                    </g>
                </a>
                <text id="rs_assis_brasil" transform="matrix(1 0 0 1 499.0322 510.29)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Brasiléia" populacao="1.050" confirmados="111" ativos="231" obitos="123" realizados="3.512" teste_100="212,512" casos_100="321,523" teste_positivo="12.111" casos_ativos="15,511" href="#" data-target="#municipio_81">
                    <g>
                    <polygon class="emergencia" points="619.7,516.2 648.8,518.8 677.2,526.4 680.6,524.2 697.8,524.6 707.6,527.7 718.6,518.7 706.5,509.7 
                             698.1,489.7 689.4,496.3 678.3,494.5 688.3,470.7 666.6,471.4 654.3,460.1 659.2,445.3 631.5,455.8 626.1,457.7 607.5,461 
                             607.8,468.7 584.8,490 573,492.3 573,503.4 604.1,509.2 610.6,497.5 609.9,486.6 622.6,491 623.1,504.8 			"/>
                    </g>
                </a>
                <text id="rs_brasileia" transform="matrix(1 0 0 1 638.5488 489.7744)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Epitaciolândia" populacao="1.200" confirmados="222" ativos="351" obitos="231" realizados="1.222" teste_100="222,512" casos_100="111,523" teste_positivo="33.642" casos_ativos="22,511" href="#" data-target="#municipio_85">
                    <g>
                    <polygon class="alerta" points="707.6,527.7 710.2,537.7 716.7,547.1 734.9,542.7 756.7,527.7 753,516.2 756.1,512 750.6,502.7 
                             746.1,504.5 736.4,497.4 738,488.7 724.5,475.1 708.8,481.7 698.1,489.7 706.5,509.7 718.6,518.7 			"/>
                    </g>
                </a>
                <text id="rs_epitaciolandia" transform="matrix(1 0 0 1 721.8389 530.5811)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Xapuri" populacao="250" confirmados="231" ativos="231" obitos="321" realizados="2.512" teste_100="231,512" casos_100="312,523" teste_positivo="34.642" casos_ativos="20,511" href="#" data-target="#municipio_100">
                    <g>
                    <polygon class="emergencia" points="756.7,527.7 774,523.6 778.4,519.4 796.7,493 796.3,488.9 810.3,478.3 810.1,470.4 803,450.3 
                             804.1,442.3 762,443.6 748.5,421 714.8,443.3 659.2,445.3 654.3,460.1 666.6,471.4 688.3,470.7 678.3,494.5 689.4,496.3 
                             702.1,486.7 708.8,481.7 724.5,475.1 738,488.7 736.4,497.6 746.1,504.5 750.6,502.7 756.1,512 753,516.2 			"/>
                    </g>
                </a>
                <text id="rs_xapuri" transform="matrix(1 0 0 1 750.6777 473.8389)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Capixaba" populacao="223" confirmados="212" ativos="351" obitos="231" realizados="2.512" teste_100="231,512" casos_100="312,523" teste_positivo="34.642" casos_ativos="20,511" href="#" data-target="#municipio_83">
                    <g>
                    <polygon class="alerta" points="810.3,478.3 813.8,479.8 821.7,479.1 826.7,477.6 847,487.1 849.2,486.4 852.2,476.9 856.1,474 
                             866.3,460.6 862.7,457 854.8,455.7 850.3,448.5 856.4,441.2 837.1,419.6 804.1,442.3 803,450.3 810.1,470.4 			"/>
                    </g>
                </a>
                <text id="rs_capixaba" transform="matrix(1 0 0 1 820.7744 465.29)" class="st1 st2"></text>
                </g>
                <g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Rio Branco" populacao="2.150" confirmados="4.312" ativos="1.351" obitos="631" realizados="3.512" teste_100="512,512" casos_100="241,523" teste_positivo="53.642" casos_ativos="40,511" href="#" data-target="#municipio_94">
                    <g>
                    <polygon class="emergencia" points="880.3,383.5 844.2,358 832,373.4 814.2,369.9 789.6,385.9 765.3,363.9 751.2,369.4 752.5,333.4 
                             726.6,328.9 715.7,350.1 718.1,373.8 666.5,418.9 662.4,425.5 632.3,443.3 627.9,452.5 631.5,455.8 659.2,445.3 714.8,443.3 
                             748.5,421 762,443.6 804.1,442.3 837.1,419.6 832.5,405.1 841.5,407.7 869.3,401.4 				"/>
                    </g>
                </a>
                </g>
                <text id="rs_rio_branco" transform="matrix(1 0 0 1 731.0645 404.2578)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Bujari" populacao="322" confirmados="312" ativos="351" obitos="231" realizados="2.512" teste_100="231,512" casos_100="232,523" teste_positivo="34.312" casos_ativos="12,231" href="#" data-target="#municipio_82">
                    <g>
                    <polygon class="alerta" points="779.4,292.8 755.6,302.7 764.6,319.1 752.5,333.4 751.2,369.4 765.3,363.9 789.6,385.9 814.2,369.9 
                             832,373.4 840.1,363.2 798.8,333.1 807.4,321.7 801.8,313.4 804.3,305.1 			"/>
                    </g>
                </a>
                <text id="rs_bujari" transform="matrix(1 0 0 1 771.5811 350.7744)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Porto Acre" populacao="520" confirmados="312" ativos="351" obitos="231" realizados="2.512" teste_100="231,512" casos_100="312,523" teste_positivo="12.312" casos_ativos="31,212" href="#" data-target="#municipio_92">
                    <g>
                    <polygon class="emergencia" points="804.3,305.1 801.8,313.4 807.4,321.7 798.8,333.1 840.1,363.2 844.2,358 880.3,383.5 886.1,375.5 
                             885.4,371.3 899.9,356.1 900.3,342 874.2,336 			"/>
                    </g>
                </a>
                <text id="rs_porto_acre" transform="matrix(1 0 0 1 838.7744 347.5811)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Plácido de Castro" populacao="675" confirmados="123" ativos="321" obitos="12" realizados="1.312" teste_100="321,21" casos_100="231,523" teste_positivo="12.231" casos_ativos="31,231" href="#" data-target="#municipio_91">
                    <g>
                    <polygon class="emergencia" points="866.3,460.6 888.5,449.4 888.8,442.7 902,441.2 901.8,436.3 922.3,434.9 938.8,428.6 944.9,422.4 
                             934.5,418.8 931.4,387 905.3,401.2 890.1,405 876.8,428 862.5,433.8 856.4,441.2 850.3,448.5 854.8,455.7 862.7,457 			"/>
                    </g>
                </a>
                <text id="rs_placido_castro" transform="matrix(1 0 0 1 889.3223 429.29)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Acrelândia" populacao="640" confirmados="312" ativos="351" obitos="231" realizados="1.512" teste_100="232,222" casos_100="231,523" teste_positivo="12.231" casos_ativos="12,212" href="#" data-target="#municipio_79">
                    <g>
                    <polygon class="emergencia" points="993,380.2 918.6,349.3 931.4,387 934.5,418.8 944.9,422.4 			"/>
                    </g>
                </a>
                <text id="rs_acrelandia" transform="matrix(1 0 0 1 943.2256 388.7422)" class="st1 st2"></text>
                </g>
                <g>
                <a data-toggle='modal' id="mapanome" rel="Senador Guiomard" populacao="520" confirmados="23" ativos="123" obitos="23" realizados="212" teste_100="111,512" casos_100="231,523" teste_positivo="34.642" casos_ativos="15,511" href="#" data-target="#municipio_98">
                    <g>
                    <polygon class="emergencia" points="918.6,349.3 900.3,342 899.9,356.1 885.4,371.3 886.1,375.5 869.3,401.4 841.5,407.7 832.5,405.1 
                             837.1,419.6 856.4,441.2 862.5,433.8 876.8,428 890.1,405 905.3,401.2 931.4,387 			"/>
                    </g>
                </a>
                <text id="rs_senador_guiomard" transform="matrix(1 0 0 1 891.7422 386.5488)" class="st1 st2"></text>
                </g>
                </svg>
            </div>
            <br>
            <div class="row">
                <div class="col-xl-7">

                    <div class="row" style="margin-top: 30px;">
                        <div class="col-xl-4 col-md-6 col-12">
                            <div class="box">
                                <div class="box-body pt-20">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="icon bg-danger-light rounded-circle">
                                            <i class="text-danger mr-0 font-size-20 fa fa-ambulance"></i>
                                        </div>
                                        <div>
                                            <p class="text-mute mb-0">CONFIRMADOS</p>
                                            <h3 id="rs_confirmados" class="text-dark mb-0 font-weight-500">11.381</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12">
                            <div class="box">
                                <div class="box-body pt-20">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="icon bg-warning-light rounded-circle">
                                            <i class="text-warning mr-0 font-size-20 fas fa-hospital-user"></i>
                                        </div>
                                        <div>
                                            <p class="text-mute mb-0">ATIVOS</p>
                                            <h3 id="rs_ativos" class="text-dark mb-0 font-weight-500">5.194</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-12">
                            <div class="box">
                                <div class="box-body pt-20">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="icon bg-default-light rounded-circle">
                                            <i class="text-default mr-0 font-size-20 fas fa-coffin"></i>
                                        </div>
                                        <div>
                                            <p class="text-mute mb-0">ÓBITOS</p>
                                            <h3 id="rs_obitos" class="text-dark mb-0 font-weight-500">300</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xl-5">
                    <div class="table-responsive-sm">
                        <table class="table table-cases mb-0">
                            <tbody>
                                <tr>
                                    <td>
                                        <strong>TESTE</strong>
                                        REALIZADOS
                                        <span id="rs_realizados">24.784</span>
                                    </td>
                                    <td>
                                        <strong>TESTE</strong>
                                        por 100MIL HAB.
                                        <span id="rs_teste_100">2.538,802</span>
                                    </td>
                                    <td>
                                        <strong>TESTE</strong>
                                        % DE POSITIVOS
                                        <span id="rs_teste_positivos">40.358%</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <strong>CASOS</strong>
                                        por 100MIL HAB.
                                        <span id="rs_casos_100">972,353</span>
                                    </td>
                                    <td>
                                        <strong>CASOS</strong>
                                        % DE ATIVOS
                                        <span id="rs_casos_ativos">40,358%</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <!-- <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
                <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">FAQ</a>
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="#">Purchase Now</a>
                </li>
        </ul> -->
    </div>
    &copy; 2020 <a href="https://www.acre.gov.br/">Governo do Estado do Acre</a>. Todos os direitos reservados.
</footer>

<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript">
    $("a#mapanome").click(function () {//Clicando dentro do Mapa
        var populacao = $(this).attr('populacao');
        var confirmados = $(this).attr('confirmados');
        var ativos = $(this).attr('ativos');
        var obitos = $(this).attr('obitos');
        var realizados = $(this).attr('realizados');
        var teste_100 = $(this).attr('teste_100');
        var casos_100 = $(this).attr('casos_100');
        var teste_positivo = $(this).attr('teste_positivo');
        var casos_ativos = $(this).attr('casos_ativos');

        $("#nomedomunicipio").html('<img src="images/bandeiras/emergencia.svg" alt=""><br> ' + $(this).attr('rel') + '</h1>');
        $("h4#populacao").html(populacao);
        $("h3#rs_confirmados").html(confirmados);
        $("h3#rs_ativos").html(ativos);
        $("h3#rs_obitos").html(obitos);
        $("span#rs_realizados").html(realizados);
        $("span#rs_teste_100").html(teste_100);
        $("span#rs_casos_100").html(casos_100);
        $("span#rs_teste_positivos").html(teste_positivo + "%");
        $("span#rs_casos_ativos").html(casos_ativos + "%");
    });

    $("body").click(function (e) {//Clicando fora do Mapa
        var map = $("a#mapanome");
        var populacao = $("input#acre_populacao").attr('populacao');
        var confirmados = $("input#acre_populacao").attr('confirmados');
        var ativos = $("input#acre_populacao").attr('ativos');
        var obitos = $("input#acre_populacao").attr('obitos');
        var realizados = $("input#acre_populacao").attr('realizados');
        var teste_100 = $("input#acre_populacao").attr('teste_100');
        var casos_100 = $("input#acre_populacao").attr('casos_100');
        var teste_positivo = $("input#acre_populacao").attr('teste_positivo');
        var casos_ativos = $("input#acre_populacao").attr('casos_ativos');

        if (!map.is(e.target) && map.has(e.target).length === 0) {
            $("#nomedomunicipio").html('<img src="images/bandeiras/emergencia.svg" alt=""><br> ACRE</h1>');
            $("h4#populacao").html(populacao);
            $("h3#rs_confirmados").html(confirmados);
            $("h3#rs_ativos").html(ativos);
            $("h3#rs_obitos").html(obitos);
            $("span#rs_realizados").html(realizados);
            $("span#rs_teste_100").html(teste_100);
            $("span#rs_casos_100").html(casos_100);
            $("span#rs_teste_positivos").html(teste_positivo + "%");
            $("span#rs_casos_ativos").html(casos_ativos + "%");
        }
    });

    //Carregando Valores no Mapa
    var qtd_mancio_lima = $("input#qtd_mancio_lima").val();
    var qtd_rodrigues_alves = $("input#qtd_rodrigues_alves").val();
    var qtd_cruzeiro_sul = $("input#qtd_cruzeiro_sul").val();
    var qtd_porto_walter = $("input#qtd_porto_walter").val();
    var qtd_tarauaca = $("input#qtd_tarauaca").val();
    var qtd_marechal_thaumaturgo = $("input#qtd_marechal_thaumaturgo").val();
    var qtd_jordao = $("input#qtd_jordao").val();
    var qtd_feijo = $("input#qtd_feijo").val();
    var qtd_santa_rosa = $("input#qtd_santa_rosa").val();
    var qtd_manoel_urbano = $("input#qtd_manoel_urbano").val();
    var qtd_sena_madureira = $("input#qtd_sena_madureira").val();
    var qtd_assis_brasil = $("input#qtd_assis_brasil").val();
    var qtd_epitaciolandia = $("input#qtd_epitaciolandia").val();
    var qtd_brasileia = $("input#qtd_brasileia").val();
    var qtd_xapuri = $("input#qtd_xapuri").val();
    var qtd_capixaba = $("input#qtd_capixaba").val();
    var qtd_rio_branco = $("input#qtd_rio_branco").val();
    var qtd_bujari = $("input#qtd_bujari").val();
    var qtd_porto_acre = $("input#qtd_porto_acre").val();
    var qtd_placido_castro = $("input#qtd_placido_castro").val();
    var qtd_acrelandia = $("input#qtd_acrelandia").val();
    var qtd_senador_guiomard = $("input#qtd_senador_guiomard").val();

    $("text#rs_mancio_lima").html(qtd_mancio_lima);
    $("text#rs_rodrigues_alves").html(qtd_rodrigues_alves);
    $("text#rs_cruzeiro_sul").html(qtd_cruzeiro_sul);
    $("text#rs_porto_walter").html(qtd_porto_walter);
    $("text#rs_tarauaca").html(qtd_tarauaca);
    $("text#rs_marechal_thaumaturgo").html(qtd_marechal_thaumaturgo);
    $("text#rs_jordao").html(qtd_jordao);
    $("text#rs_feijo").html(qtd_feijo);
    $("text#rs_santa_rosa").html(qtd_santa_rosa);
    $("text#rs_manoel_urbano").html(qtd_manoel_urbano);
    $("text#rs_sena_madureira").html(qtd_sena_madureira);
    $("text#rs_assis_brasil").html(qtd_assis_brasil);
    $("text#rs_epitaciolandia").html(qtd_epitaciolandia);
    $("text#rs_brasileia").html(qtd_brasileia);
    $("text#rs_xapuri").html(qtd_xapuri);
    $("text#rs_capixaba").html(qtd_capixaba);
    $("text#rs_rio_branco").html(qtd_rio_branco);
    $("text#rs_bujari").html(qtd_bujari);
    $("text#rs_porto_acre").html(qtd_porto_acre);
    $("text#rs_placido_castro").html(qtd_placido_castro);
    $("text#rs_acrelandia").html(qtd_acrelandia);
    $("text#rs_senador_guiomard").html(qtd_senador_guiomard);

</script>

<?php include 'layout/footer.php'; ?>