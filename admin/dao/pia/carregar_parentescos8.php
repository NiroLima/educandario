<?php

//------------------------------------------------------------------------------
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$id = $_POST['id'];

$stmp = $db->prepare("SELECT *           
                      FROM mod_acolhimento_crianca_parentes macp   
                      WHERE macp.familia_id = ? AND macp.tipo = 8");
$stmp->bindValue(1, $id);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    
} else {
    while ($parent = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr id="remover_parente">
                     <td>' . grau_parentesco($parent['grau']) . '</td>
                     <td>' . $parent['nome'] . '</td>
                     <td>' . $parent['endereco'] . '</td>
                     <td>' . $parent['celular'] . '</td>
                     <td> 
                         <span style="display: block;"><small>CPF:</small> ' . $parent['cpf'] . '</span>
                         <span style="display: block;"><small>RG:</small> ' . $parent['rg'] . '</span>
                     </td>
                     <td>' . $parent['ocupacao'] . '</td>
                     <td width="100px">
                        <a style="cursor: pointer" onclick="remover8(this, ' . $parent['id'] . ')" class="text-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>';
    }
}
//------------------------------------------------------------------------------
?>

