<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- CSS DO PLUGIN DE UPLOAD DE FOTOS -->
<link href="<?= PORTAL_URL; ?>assets/vendor_components/cropper/css/cropper.min.css" rel="stylesheet">
<link href="<?= PORTAL_URL; ?>assets/vendor_components/cropper/css/main.css" rel="stylesheet">
<!-- FIM DO PLUGIN -->

<?php
$_SESSION['foto_cut'] = "";
$_SESSION['foto_origin'] = "";
unset($_SESSION['foto_cut']);
unset($_SESSION['foto_origin']);

$perfil = "";

$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT u.id, u.instituicao_id, u.registro, u.funcao_id, u.celular, u.status, u.nome, u.contato, u.email, sf.nome AS funcao 
                 FROM seg_profissional_instituicao u 
                 LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id 
                 WHERE u.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_profissional = $result->fetch(PDO::FETCH_ASSOC);

    $profissional_id = $dados_profissional['id'];
    $profissional_nome = ($dados_profissional['nome']);
    $profissional_status = $dados_profissional['status'];
    $profissional_contato_cel = $dados_profissional['celular'];
    $profissional_contato_fixo = $dados_profissional['contato'];
    $profissional_email = $dados_profissional['email'];
    $profissional_instituicao = $dados_profissional['instituicao_id'];
    $usuariro_registro = $dados_profissional['registro'];

    $profissional_funcao = $dados_profissional['funcao_id'];
} else {
    $profissional_id = "";
    $profissional_nome = "";
    $profissional_status = 1;
    $profissional_contato_cel = "";
    $profissional_contato_fixo = "";
    $profissional_email = "";
    $usuariro_registro = "";
    $profissional_instituicao = "";
    $profissional_funcao = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/profissionais_instituicao/lista">Profissionais</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <!-- PERFIL -->
            <div class="box box-solid bg-info">
                <div class="box-header">
                    <h4 class="box-title"><i class="fal fa-user-circle"></i> <strong>PERFIL</strong></h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="div_nome" class="form-group">
                                        <label for="nome">NOME</label>
                                        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome Completo" value="<?= $profissional_nome; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div id="div_email" class="form-group">
                                <label for="email">E-MAIL</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="<?= $profissional_email; ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="div_celular" class="form-group">
                                <label for="celular">CELULAR</label>
                                <input type="text" data-mask="(99) 9 9999-9999" class="form-control" name="celular" id="celular" placeholder="(99) 9 9999-9999" value="<?= $profissional_contato_cel; ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="div_fixo" class="form-group">
                                <label for="fixo">FIXO</label>
                                <input type="text" data-mask="(99)9999-9999" class="form-control" name="fixo" id="fixo" placeholder="(99) 9999-9999" value="<?= $profissional_contato_fixo; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIM PERFIL -->

            <form id="form_profissional" name="form_profissional" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $profissional_id ?>"/>

                <!-- PROFISSIONAL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-briefcase"></i> <strong>PROFISSIONAL</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div id="div_funcao" class="form-group">
                                    <label for="funcao">FUNÇÃO</label>
                                    <select name="funcao" id="funcao" class="form-control select2">
                                        <option value="">Selecione a função</option>
                                        <?php
                                        $setor_profissional = "";
                                        $result = $db->prepare("SELECT fc.id, fc.nome AS profissional, ss.nome AS setor          
                                                        FROM seg_funcao fc  
                                                        LEFT JOIN seg_setor AS ss ON ss.id = fc.setor_id  
                                                        WHERE fc.status = 1");
                                        $result->execute();
                                        while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
                                            if ($profissional_funcao == $prof['id']) {
                                                $setor_profissional = $prof['setor'];
                                                ?>
                                                <option selected="true" rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div id="div_registro_classe" class="form-group">
                                    <label for="registro_classe">REGISTRO DE CLASSE</label>
                                    <input type="text" class="form-control" name="registro_classe" id="registro_classe" placeholder="Informe o registro de classe" value="<?= $usuariro_registro; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_instituicao" class="form-group">
                                    <label for="instituicao">INSTITUIÇÃO</label>
                                    <select name="instituicao" id="instituicao" class="form-control select2">
                                        <option value="">Selecione a instituição</option>
                                        <?php
                                        $setor_profissional = "";
                                        $result = $db->prepare("SELECT fc.id, fc.nome           
                                                        FROM seg_instituicao fc  
                                                        WHERE fc.status = 1");
                                        $result->execute();
                                        while ($inst = $result->fetch(PDO::FETCH_ASSOC)) {
                                            if ($profissional_instituicao == $inst['id']) {
                                                ?>
                                                <option selected="true"  value="<?= $inst['id']; ?>"><?= $inst['nome']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="<?= $inst['id']; ?>"><?= $inst['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PROFISSIONAL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $profissional_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $profissional_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/profissionais_instituicao/novo.js"></script>