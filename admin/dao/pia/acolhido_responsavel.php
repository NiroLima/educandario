<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$responsavel_id = isset($_POST['responsavel_id']) && $_POST['responsavel_id'] != "" ? $_POST['responsavel_id'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        $sql2 = $db->prepare("UPDATE mod_acolhimento_crianca
                              SET usuario_id = ? 
                              WHERE id = ?");
        $sql2->bindValue(1, $responsavel_id);
        $sql2->bindValue(2, $id);
        $sql2->execute();

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>