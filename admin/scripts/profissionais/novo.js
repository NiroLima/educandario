//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("#funcao").change(function () {
        var codigo = $(this).val();
        var setor = $(this).find('option:selected').attr('rel');
        $("input#setor").val(setor);

        $("input#crm").val("");

        if (codigo == 6) {
            $("div#div_crm").show();
        } else {
            $("div#div_crm").hide();
        }

    });

    //COMBO ESTADO E MUNICIPIO
    $("select#estado_id").change(function () {
        $("select#municipio_id").html('<option value="0">Carregando...</option>');
        $.post(PORTAL_URL + "utils/combo_cidades.php",
                {estado: $(this).val()},
                function (valor) {
                    $("select#municipio_id").html(valor);
                    $("select#municipio_id").select2();
                });
    });

    $('#form_profissional').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var nome = $('#nome').val();
            var cpf = $('#cpf').val();
            var nascimento = $('#nascimento').val();
            var email = $('#email').val();
            var celular = $('#celular').val();
            var fixo = $('#fixo').val();
            var rua = $('#rua').val();
            var numero = $('#numero').val();
            var bairro = $('#bairro').val();
            var setor = $('#setor').val();
            var funcao = $('#funcao').val();
            var periodo = $('#periodo').val();
            var municipio_id = $('#municipio_id').val();
            var nivel = $('#nivel').val();
            var categoria = $('#categoria').val();
            var especialidade = $('#especialidade').val();
            var registro = $('#registro_classe').val();
            var crm = $('#crm').val();

            $.post(PORTAL_URL + "admin/dao/profissionais/novo.php", {id: id, crm: crm, categoria: categoria, especialidade: especialidade, registro: registro, nivel: nivel, nome: nome, cpf: cpf, nascimento: nascimento, email: email, celular: celular, fixo: fixo, rua: rua, numero: numero, bairro: bairro, setor: setor, funcao: funcao, periodo: periodo, municipio_id: municipio_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário do Profissional",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário do Profissional",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/profissionais/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    var nome = $("#nome").val();
    var estado_id = $("#estado_id").val();
    var municipio_id = $("#municipio_id").val();
    var categoria = $("#categoria").val();
    var funcao = $("#funcao").val();
    var periodo = $("#periodo").val();
    var registro_classe = $("#registro_classe").val();
    var crm = $("#crm").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (funcao == 6 && crm == "") {
            $('div#div_crm').after('<label id="erro_crm" class="error">CRM é obrigatório.</label>');
            valido = false;
            element = $('div#div_crm');
        }

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (registro_classe == "") {
            $('div#div_registro_classe').after('<label id="erro_registro_classe" class="error">Registro de classe é obrigatório.</label>');
            valido = false;
            element = $('div#div_registro_classe');
        }

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (funcao == "") {
            $('div#div_funcao').after('<label id="erro_funcao" class="error">Função é obrigatório.</label>');
            valido = false;
            element = $('div#div_funcao');
        }

        //VERIFICANDO SE O CAMPO MUNICÍPIO FOI INFORMADO
        if (municipio_id == "") {
            $('div#div_municipio_id').after('<label id="erro_municipio_id" class="error">Município é obrigatório.</label>');
            valido = false;
            element = $('div#div_municipio_id');
        }

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (estado_id == "") {
            $('div#div_estado_id').after('<label id="erro_estado_id" class="error">Estado é obrigatório.</label>');
            valido = false;
            element = $('div#div_estado_id');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome == "") {
            $('div#div_nome').after('<label id="erro_nome" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome');
        }

    } else if (obj.tipo == "cpf") {//VALIDAÇÃO COM BANCO DE DADOS
        $('div#div_cpf').after('<label id="erro_cpf" class="error">' + obj.retorno + '</label>');
        valido = false;
        element = $('div#div_cpf');
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------