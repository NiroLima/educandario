<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);
if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
                            FROM mod_acolhimento ma   
                            WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];

//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result2->bindValue(1, $acolhimento_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_id = $resultado2['id'];
    $acolhimento_responsavel = $resultado2['usuario_id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
//------------------------------------------------------------------------------
    $result3 = $db->prepare("SELECT *      
                            FROM mod_acolhimento_crianca_educacao mace   
                            WHERE mace.acolhimento_crianca_id = ?");
    $result3->bindValue(1, $acolhimento_crianca_id);
    $result3->execute();
    $resultado3 = $result3->fetch(PDO::FETCH_ASSOC);

    $educacao_id = $resultado3['id'];

    $justique_ausencia = $resultado3['justique_ausencia'];

    $justifique = $resultado3['justifique'];
    $justifique2 = $resultado3['justifique2'];

    $idade_escolar = $resultado3['idade_escolar'];
    $idade_escolar_atual = $resultado3['idade_escolar_atual'];
    $escola = $resultado3['escola'];
    $serie = $resultado3['serie'];
    $turno = $resultado3['turno'];
    $escola1 = $resultado3['escola1'];
    $serie1 = $resultado3['serie1'];
    $turno1 = $resultado3['turno1'];
    $frequenta_instituicao = $resultado3['frequenta_instituicao'];
    $justificacao_ausencia = $resultado3['justificacao_ausencia'];

    $frequencia = $resultado3['frequencia'];
    $sobre_frequencia = $resultado3['sobre_frequencia'];
    $socializacao = $resultado3['socializacao'];
    $sobre_socializacao = $resultado3['sobre_socializacao'];
    $interesse = $resultado3['interesse'];
    $sobre_interesse = $resultado3['sobre_interesse'];
    $rendimento = $resultado3['rendimento'];
    $sobre_rendimento = $resultado3['sobre_rendimento'];
    $participacao = $resultado3['participacao'];
    $sobre_participacao = $resultado3['sobre_participacao'];

    $apresenta_desenvolvimento = $resultado3['apresenta_desenvolvimento'];
    $descricao_aspectos = $resultado3['descricao_aspectos'];

    $contexto_geral = $resultado3['contexto_geral'];
    $relacao_escola = $resultado3['relacao_escola'];
    $descricao_relacao_escola = $resultado3['descricao_relacao_escola'];
    $descricao_intervencoes = $resultado3['descricao_intervencoes'];

    $espaco_estrutura = $resultado3['espaco_estrutura'];
    $rotinas = $resultado3['rotinas'];
    $vf = $resultado3['vf'] == "" ? 0 : $resultado3['vf'];
} else {
    $acolhimento_id = "";
    $justique_ausencia = "";
    $justifique = "";
    $justifique2 = "";
    $acolhimento_crianca_id = "";
    $educacao_id = "";
    $idade_escolar = "";
    $idade_escolar_atual = "";
    $escola = "";
    $serie = "";
    $turno = "";
    $escola1 = "";
    $serie1 = "";
    $turno1 = "";
    $frequenta_instituicao = "";
    $justificacao_ausencia = "";
    $frequencia = "";
    $sobre_frequencia = "";
    $socializacao = "";
    $sobre_socializacao = "";
    $interesse = "";
    $sobre_interesse = "";
    $rendimento = "";
    $sobre_rendimento = "";
    $participacao = "";
    $sobre_participacao = "";
    $apresenta_desenvolvimento = "";
    $descricao_aspectos = "";
    $contexto_geral = "";
    $relacao_escola = "";
    $descricao_relacao_escola = "";
    $descricao_intervencoes = "";
    $espaco_estrutura = "";
    $rotinas = "";
    $acolhimento_responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
    $vf = 0;
}
//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);
//------------------------------------------------------------------------------
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/pia/lista">PIA</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="text-center">PLANO INDIVIDUAL DE ATENDIMENTO - PIA</h4>

        <!-- Main content -->
        <section class="content">

            <!-- Step wizard -->
            <div id="impressao" class="box box-solid bg-info">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="box-title">PIA</h4>
                            <h6 class="box-subtitle">PLANO INDIVIDUAL DE ATENDIMENTO</h6>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-right"><?= $barra; ?>%</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <?= $barra; ?>%">
                                    <span class="sr-only"><?= $barra; ?>% Complete</span>
                                </div>
                            </div>
                        </div>      
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="box-title">RESPONSÁVEL DO PIA</h4>

                            <div id="responsavel_edite_1">
                                <h6 class="subtitle"><?= $nome_responsavel; ?> <a href="#" class="edit-responsavel"><i onclick="editar_responsavel()" class="fal fa-pencil-alt"></i></a></h6>
                            </div>

                            <div id="responsavel_edite_2" style="display: none">
                                <form id="form_responsavel" name="form_responsavel" method="POST" action="#" class="tab-wizard wizard-circle">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required="true" id="responsavel_id" name="responsavel_id">
                                                <option value="">Selecione o responsável</option>
                                                <?php
                                                $result = $db->prepare("SELECT su.id, su.nome           
                                                            FROM seg_usuario su    
                                                            WHERE su.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_responsavel == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button href="#" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-2 text-right">
                            <a <?= is_numeric($vf) && $vf == 0 ? "" : "style='display: none'"; ?> href="#" onclick="validar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-success mt-2"><i class="fas fa-check"></i> VALIDAR</a>
                            <a <?= is_numeric($vf) && $vf == 1 ? "" : "style='display: none'"; ?> href="#" onclick="desvalidar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-warning mt-2"><i class="fas fa-remove"></i> DESVALIDAR</a>
                        </div>
                        <div class="col-md-2 text-right">
                            <a onclick="gerar_pdf()" style="cursor: pointer" class="btn-arquivo mt-2"><i class="fas fa-file-pdf"></i> GERAR PDF</a>
                        </div>
                    </div> 
                </div>
                <!-- /.box-header -->
                <div class="box-body wizard-content">

                    <?php include 'layout/menu_wizard.php'; ?>

                    <br/>

                    <form id="form_educacao" name="form_educacao" method="POST" action="#" class="tab-wizard wizard-circle">

                        <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>
                        <input type="hidden" id="acolhimento_crianca_id" name="acolhimento_crianca_id" value="<?= $acolhimento_crianca_id; ?>"/>
                        <input type="hidden" id="educacao_id" name="educacao_id" value="<?= $educacao_id; ?>"/>

                        <!-- Step 4 -->
                        <h6>EDUCAÇÃO</h6>
                        <section>
                            <div class="box box-solid bg-info">
                                <div class="box-header">
                                    <h4 class="box-title mb-0"><strong>SITUAÇÃO ANTERIOR AO ACOLHIMENTO</strong></h4>
                                </div>
                                <div class="box-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box box-outline-info">
                                                <div class="box-header">
                                                    <strong>EM RELAÇÃO À VIDA ESCOLAR DA CRIANÇA, DESCREVA:</strong>
                                                </div>
                                                <div class="box-body">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="genero">CRIANÇA EM IDADE ESCOLAR?</label>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <input type="radio" <?= $idade_escolar == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_idade_escolar" id="sim_idade_escolar" value="1">
                                                                        <label for="sim_idade_escolar">SIM</label>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <input type="radio" <?= $idade_escolar == "" || $idade_escolar == 0 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_idade_escolar" id="nao_idade_escolar" value="0">
                                                                        <label for="nao_idade_escolar">NÃO</label>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="div_escolar" <?= $idade_escolar == 1 ? "" : "style='display: none'"; ?> class="row mt-3">
                                                        <div class="col-md-4">
                                                            <div id="div_escola" class="form-group">
                                                                <label for="escola">ESCOLA</label>
                                                                <input type="text" class="form-control" name="escola" id="escola" placeholder="Nome da escola" value="<?= $escola; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div id="div_serie" class="form-group">
                                                                <label for="serie">SÉRIE</label>
                                                                <input type="text" class="form-control" name="serie" id="serie" placeholder="Série" value="<?= $serie; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div id="div_turno" class="form-group">
                                                                <label for="turno">TURNO</label>
                                                                <input type="text" class="form-control" name="turno" id="turno" placeholder="Turno" value="<?= $turno; ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>

                                                    <div id="div_freq" <?= $idade_escolar == 1 ? "" : "style='display: none'"; ?> class="box box-solid bg-info">
                                                        <div class="box-header">
                                                            <h4 class="box-title mb-0"><strong>FREQUÊNCIA</strong></h4>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="row mt-2">
                                                                <div class="col-md-4">
                                                                    <div id="div_frequencia" class="form-group">
                                                                        <label for="frequencia">FREQUÊNCIA</label>
                                                                        <select name="frequencia" id="frequencia" class="form-control select2">
                                                                            <option value="">Selecione a frequência</option>
                                                                            <option <?= is_numeric($frequencia) && $frequencia == 0 ? "selected='true'" : ""; ?> value="0">Ruim</option>
                                                                            <option <?= is_numeric($frequencia) && $frequencia == 1 ? "selected='true'" : ""; ?> value="1">Regular</option>
                                                                            <option <?= is_numeric($frequencia) && $frequencia == 2 ? "selected='true'" : ""; ?> value="2">Boa</option>
                                                                            <option <?= is_numeric($frequencia) && $frequencia == 3 ? "selected='true'" : ""; ?> value="3">Ótimo</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div id="div_sobre_frequencia" class="form-group">
                                                                        <label for="sobre_frequencia">SOBRE A FREQUÊNCIA</label>
                                                                        <textarea name="sobre_frequencia" id="sobre_frequencia" class="form-control" placeholder="Descreva" cols="30" rows="5"><?= $sobre_frequencia; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="div_social" <?= $idade_escolar == 1 ? "" : "style='display: none'"; ?> class="box box-solid bg-info">
                                                        <div class="box-header">
                                                            <h4 class="box-title mb-0"><strong>SOCIALIZAÇÃO</strong></h4>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="row mt-2">
                                                                <div class="col-md-4">
                                                                    <div id="div_socializacao" class="form-group">
                                                                        <label for="socializacao">SOCIALIZAÇÃO</label>
                                                                        <select name="socializacao" id="socializacao" class="form-control select2">
                                                                            <option value="">Selecione a Socialização</option>
                                                                            <option <?= is_numeric($socializacao) && $socializacao == 0 ? "selected='true'" : ""; ?> value="0">Ruim</option>
                                                                            <option <?= is_numeric($socializacao) && $socializacao == 1 ? "selected='true'" : ""; ?> value="1">Regular</option>
                                                                            <option <?= is_numeric($socializacao) && $socializacao == 2 ? "selected='true'" : ""; ?> value="2">Boa</option>
                                                                            <option <?= is_numeric($socializacao) && $socializacao == 3 ? "selected='true'" : ""; ?> value="3">Ótimo</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div id="div_sobre_socializacao" class="form-group">
                                                                        <label for="sobre_socializacao">SOBRE A SOCIALIZAÇÃO</label>
                                                                        <textarea name="sobre_socializacao" id="sobre_socializacao" class="form-control" placeholder="Descreva" cols="30" rows="5"><?= $sobre_socializacao; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="div_inae" <?= $idade_escolar == 1 ? "" : "style='display: none'"; ?> class="box box-solid bg-info">
                                                        <div class="box-header">
                                                            <h4 class="box-title mb-0"><strong>INTERESSE NAS ATIVIDADES ESCOLARES</strong></h4>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="row mt-2">
                                                                <div class="col-md-4">
                                                                    <div id="div_interesse_atividade" class="form-group">
                                                                        <label for="interesse_atividade_escolar">INTERESSE NAS ATIVIDADES ESCOLARES</label>
                                                                        <select name="interesse_atividade_escolar" id="interesse_atividade_escolar" class="form-control select2">
                                                                            <option value="">Selecione o Interesse a Atividades Escolares</option>
                                                                            <option <?= is_numeric($interesse) && $interesse == 0 ? "selected='true'" : ""; ?> value="0">Ruim</option>
                                                                            <option <?= is_numeric($interesse) && $interesse == 1 ? "selected='true'" : ""; ?> value="1">Regular</option>
                                                                            <option <?= is_numeric($interesse) && $interesse == 2 ? "selected='true'" : ""; ?> value="2">Boa</option>
                                                                            <option <?= is_numeric($interesse) && $interesse == 3 ? "selected='true'" : ""; ?> value="3">Ótimo</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div id="div_sobre_ativades_escolares" class="form-group">
                                                                        <label for="sobre_atividades_escolares">SOBRE O INTERESSE ÀS ATIVIDADES ESCOLARES</label>
                                                                        <textarea name="sobre_atividades_escolares" id="sobre_atividades_escolares" class="form-control" placeholder="Descreva" cols="30" rows="5"><?= $sobre_interesse; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>            

                                                    <div id="div_rend" <?= $idade_escolar == 1 ? "" : "style='display: none'"; ?> class="box box-solid bg-info">
                                                        <div class="box-header">
                                                            <h4 class="box-title mb-0"><strong>RENDIMENTO</strong></h4>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="row mt-2">
                                                                <div class="col-md-4">
                                                                    <div id="div_rendimento" class="form-group">
                                                                        <label for="rendimento">RENDIMENTO</label>
                                                                        <select name="rendimento" id="rendimento" class="form-control select2">
                                                                            <option value="">Selecione o Rendimento</option>
                                                                            <option <?= is_numeric($rendimento) && $rendimento == 0 ? "selected='true'" : ""; ?> value="0">Ruim</option>
                                                                            <option <?= is_numeric($rendimento) && $rendimento == 1 ? "selected='true'" : ""; ?> value="1">Regular</option>
                                                                            <option <?= is_numeric($rendimento) && $rendimento == 2 ? "selected='true'" : ""; ?> value="2">Boa</option>
                                                                            <option <?= is_numeric($rendimento) && $rendimento == 3 ? "selected='true'" : ""; ?> value="3">Ótimo</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div id="div_rendimento" class="form-group">
                                                                        <label for="sobre_rendimento">SOBRE O RENDIMENTO ESCOLAR</label>
                                                                        <textarea name="sobre_rendimento" id="sobre_rendimento" class="form-control" placeholder="Descreva" cols="30" rows="5"><?= $sobre_rendimento; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="div_part" <?= $idade_escolar == 1 ? "" : "style='display: none'"; ?> class="box box-solid bg-info">
                                                        <div class="box-header">
                                                            <h4 class="box-title mb-0"><strong>PARTICIPAÇÃO DA FAMÍLIA NA ESCOLA</strong></h4>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="row mt-2">
                                                                <div class="col-md-4">
                                                                    <div id="div_participacao" class="form-group">
                                                                        <label for="participacao_familia">PARTICIPAÇÃO DA FAMÍLIA NA ESCOLA</label>
                                                                        <select name="participacao_familia" id="participacao_familia" class="form-control select2">
                                                                            <option value="">Selecione a Participação da Família na Escola</option>
                                                                            <option <?= is_numeric($participacao) && $participacao == 0 ? "selected='true'" : ""; ?> value="0">Ruim</option>
                                                                            <option <?= is_numeric($participacao) && $participacao == 1 ? "selected='true'" : ""; ?> value="1">Regular</option>
                                                                            <option <?= is_numeric($participacao) && $participacao == 2 ? "selected='true'" : ""; ?> value="2">Boa</option>
                                                                            <option <?= is_numeric($participacao) && $participacao == 3 ? "selected='true'" : ""; ?> value="3">Ótimo</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div id="div_sobre_participacao_familia" class="form-group">
                                                                        <label for="sobre_participacao_familia">SOBRE A PARTICIPAÇÃO DA FAMILIA NA ESCOLA</label>
                                                                        <textarea name="sobre_participacao_familia" id="sobre_participacao_familia" class="form-control" placeholder="Descreva" cols="30" rows="5"><?= $sobre_participacao; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="div_just" class="row mt-3">
                                                        <div class="col-md-12">
                                                            <div id="div_justique_ausencia" class="form-group">
                                                                <label for="justique_ausencia">JUSTIFIQUE</label>
                                                                <textarea name="justique_ausencia" id="justique_ausencia" class="form-control" placeholder="Justifique ausência" cols="30" rows="10"><?= $justique_ausencia; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="box box-solid bg-info">
                                <div class="box-header">
                                    <h4 class="box-title mb-0"><strong>SITUAÇÃO ATUAL</strong></h4>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="genero">CRIANÇA EM IDADE ESCOLAR?</label>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <input type="radio" <?= $idade_escolar_atual == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_idade_escolar_atual" id="sim_idade_escolar_atual" value="1">
                                                        <label for="sim_idade_escolar_atual">SIM</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="radio" <?= $idade_escolar_atual == "" || $idade_escolar_atual == 0 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_idade_escolar_atual" id="nao_idade_escolar_atual" value="0">
                                                        <label for="nao_idade_escolar_atual">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_info_escola" <?= $idade_escolar_atual == 1 ? "" : "style='display: none'"; ?> class="row mt-20">
                                        <div class="col-md-4">
                                            <div id="div_escola1" class="form-group">
                                                <label for="escola1">ESCOLA</label>
                                                <input type="text" class="form-control" name="escola1" id="escola1" placeholder="Nome da escola" value="<?= $escola1; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_serie1" class="form-group">
                                                <label for="serie1">SÉRIE</label>
                                                <input type="text" class="form-control" name="serie1" id="serie1" placeholder="Série" value="<?= $serie1; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="div_turno1" class="form-group">
                                                <label for="turno1">TURNO</label>
                                                <input type="text" class="form-control" name="turno1" id="turno1" placeholder="Turno" value="<?= $turno1; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- INÍCIO EM CASO DE NÃO APARECER -->
                                    <div id="div_ha_espaco" <?= $idade_escolar_atual == 0 ? "" : "style='display: none'"; ?> class="row mt-2">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">HÁ ESPAÇO NA ESTRUTURA E ROTINA DA INSTITUIÇÃO PARA ATIVIDADES QUE ESTIMULEM O DESENVOLVIMENTO INFANTIL?</label>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <input type="radio" <?= $espaco_estrutura == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_estrutura" id="sim_estrutura" value="1">
                                                        <label for="sim_estrutura">SIM</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="radio" <?= $espaco_estrutura == "" || $espaco_estrutura == 0 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_estrutura" id="nao_estrutura" value="0">
                                                        <label for="nao_estrutura">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_estrutura" <?= $espaco_estrutura == 1 && $idade_escolar_atual == 0 ? "" : "style='display: none'"; ?> class="row">
                                        <div class="col-md-12">
                                            <div class="box box-outline-info">
                                                <div class="box-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-2">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th scope="col">QUAL ATIVIDADE É REALIZADA?</th>
                                                                    <th scope="col">QUEM REALIZA O ACOMPANHAMENTO?</th>
                                                                    <th scope="col">QUAL FORMA É REALIZADO O ACOMPANHAMENTO?</th>
                                                                    <th scope="col">COM QUAL FREQUÊNCIA?</th>
                                                                    <th scope="col"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="resultado_acompanhamento">
                                                                <?php
                                                                $result = $db->prepare("SELECT *           
                                                                                FROM mod_acolhimento_crianca_educacao_acomp macea   
                                                                                WHERE macea.educacao_id = ?");
                                                                $result->bindValue(1, $educacao_id);
                                                                $result->execute();
                                                                while ($acomp = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                    ?>
                                                                    <tr id="remover_acompanhamento">
                                                                        <td><?= $acomp['qual_atividade']; ?></td>
                                                                        <td><?= $acomp['quem_realiza']; ?></td>
                                                                        <td><?= $acomp['forma_realiza']; ?></td>
                                                                        <td><?= $acomp['frequencia']; ?></td>
                                                                        <td width="100px">
                                                                            <a style="cursor: pointer" onclick="editar(<?= $acomp['id']; ?>, '<?= $acomp['qual_atividade']; ?>', '<?= $acomp['quem_realiza']; ?>', '<?= $acomp['forma_realiza']; ?>', '<?= $acomp['frequencia']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                            <a style="cursor: pointer" onclick="remover(this, <?= $acomp['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-6">
                                                            <div id="div_nome_acompanha" class="form-group">
                                                                <label for="qual_atividade">QUAL ATIVIDADE É REALIZADA?</label>
                                                                <input type="hidden" id="acomp_id" name="acomp_id" value=""/>
                                                                <input type="text" class="form-control" name="qual_atividade" id="qual_atividade" placeholder="Qual atividade é realizada..." value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_nome_acompanha" class="form-group">
                                                                <label for="nome_acompanha">QUEM REALIZA O ACOMPANHAMENTO?</label>
                                                                <input type="hidden" id="acomp_id" name="acomp_id" value=""/>
                                                                <input type="text" class="form-control" name="nome_acompanha" id="nome_acompanha" placeholder="Nome de quem acompanha" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_qual_forma" class="form-group">
                                                                <label for="qual_forma">QUAL FORMA É REALIZADO O ACOMPANHAMENTO?</label>
                                                                <input type="text" class="form-control" name="qual_forma" id="qual_forma" placeholder="Qual forma é realizado o acompanhamento" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="div_frequencia_acompanhamento" class="form-group">
                                                                <label for="frequencia_acompanhamento">COM QUAL FREQUÊNCIA?</label>
                                                                <input type="text" class="form-control" name="frequencia_acompanhamento" id="frequencia_acompanhamento" placeholder="Qual frequência" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_acomp" name="inserir_acomp" type="button" class="btn btn-success">INSERIR</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_aspectos" <?= $espaco_estrutura == 0 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="justifique">JUSTIFIQUE <small>para não ser realizada nenhuma atividade de estímulo da criança</small></label>
                                                <textarea name="justifique" id="justifique" class="form-control" placeholder="Descreva" cols="30" rows="10"><?= $justifique; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_crianca_apresenta" <?= $idade_escolar_atual == 0 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">A CRIANÇA APRESENTA DESENVOLVIMENTO CONFORME O ESPERADO PARA SUA FAIXA ETÁRIA?</label>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <input type="radio" <?= $apresenta_desenvolvimento == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_apresenta_desenvolvimento" id="sim_apresenta_desenvolvimento" value="1">
                                                        <label for="sim_apresenta_desenvolvimento">SIM</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="radio" <?= $apresenta_desenvolvimento == "" || $apresenta_desenvolvimento == 0 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_apresenta_desenvolvimento" id="nao_apresenta_desenvolvimento" value="0">
                                                        <label for="nao_apresenta_desenvolvimento">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_aspectos2" <?= $idade_escolar_atual == 0 && $apresenta_desenvolvimento == 1 ? "" : "style='display: none'"; ?> class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="descreva_aspectos">DESCREVA OS ASPECTOS OBSERVADOS</label>
                                                <textarea name="descreva_aspectos" id="descreva_aspectos" class="form-control" placeholder="Descreva" cols="30" rows="10"><?= $descricao_aspectos; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIM EM CASO DE NÃO APARECER -->

                                    <!-- INÍCIO EM CASO DE SIM APARECER -->
                                    <div id="div_rotinas" <?= $idade_escolar_atual == 1 ? "" : "style='display: none'"; ?> class="row mt-20">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">A CRIANÇA POSSUI ROTINAS ESTABELECIDAS PARA A REALIZAÇÃO DE TAREFAS/TRABALHOS ESCOLARES COM ACOMPANHAMENTO?</label>
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <input type="radio" <?= $rotinas == 1 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_rotinas_estabelecidas" id="sim_rotinas_estabelecidas" value="1">
                                                        <label for="sim_rotinas_estabelecidas">SIM</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="radio" <?= $rotinas == 0 || $rotinas == "" ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_rotinas_estabelecidas" id="nao_rotinas_estabelecidas" value="0">
                                                        <label for="nao_rotinas_estabelecidas">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_sea" <?= $idade_escolar_atual == 1 ? "" : "style='display: none'"; ?> class="row mt-20">
                                        <div class="col-md-12">

                                            <div class="box box-outline-info">
                                                <div class="box-header">
                                                    <strong>SITUAÇÃO ESCOLAR ATUAL</strong>
                                                </div>
                                                <div class="box-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-2">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th scope="col">QUEM REALIZA O ACOMPANHAMENTO?</th>
                                                                    <th scope="col">QUAL FORMA É REALIZADO O ACOMPANHAMENTO?</th>
                                                                    <th scope="col">COM QUAL FREQUÊNCIA?</th>
                                                                    <th scope="col"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tbody id="resultado_rotinas">
                                                                <?php
                                                                $result = $db->prepare("SELECT *           
                                                                                FROM mod_acolhimento_crianca_educacao_rotinas macer   
                                                                                WHERE macer.educacao_id = ?");
                                                                $result->bindValue(1, $educacao_id);
                                                                $result->execute();
                                                                while ($acomp = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                    ?>
                                                                    <tr id="remover_rotinas">
                                                                        <td><?= $acomp['quem_realiza']; ?></td>
                                                                        <td><?= $acomp['forma_realiza']; ?></td>
                                                                        <td><?= $acomp['frequencia']; ?></td>
                                                                        <td width="100px">
                                                                            <a style="cursor: pointer" onclick="editar2(<?= $acomp['id']; ?>, '<?= $acomp['quem_realiza']; ?>', '<?= $acomp['forma_realiza']; ?>', '<?= $acomp['frequencia']; ?>')" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                                            <a style="cursor: pointer" onclick="remover2(this, <?= $acomp['id']; ?>)" class="text-danger"><i class="fa fa-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <hr>
                                                    <div class="row mt-3">
                                                        <div class="col-md-4">
                                                            <div id="div_nome_acompanha2" class="form-group">
                                                                <label for="nome_acompanha2">QUEM REALIZA O ACOMPANHAMENTO?</label>
                                                                <input type="hidden" id="acomp_id2" name="acomp_id2" value=""/>
                                                                <input type="text" class="form-control" name="nome_acompanha2" id="nome_acompanha2" placeholder="Nome de quem acompanha" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div id="div_qual_forma2" class="form-group">
                                                                <label for="qual_forma2">QUAL FORMA É REALIZADO O ACOMPANHAMENTO?</label>
                                                                <input type="text" class="form-control" name="qual_forma2" id="qual_forma2" placeholder="Qual forma é realizado o acompanhamento" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div id="div_frequencia_acompanhamento2" class="form-group">
                                                                <label for="frequencia_acompanhamento2">COM QUAL FREQUÊNCIA?</label>
                                                                <input type="text" class="form-control" name="frequencia_acompanhamento2" id="frequencia_acompanhamento2" placeholder="Qual frequência" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-2">
                                                        <div class="col-md-12 text-center">
                                                            <button id="inserir_acomp2" name="inserir_acomp2" type="button" class="btn btn-success">INSERIR</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_justifique2" <?= $idade_escolar_atual == 1 && $rotinas == 0 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="justifique2">JUSTIFIQUE</label>
                                                <textarea name="justifique2" id="justifique2" class="form-control" placeholder="Descreva" cols="30" rows="10"><?= $justifique2; ?></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div id="div_contexto_geral" <?= $idade_escolar_atual == 1 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <div id="div_contexto_geral" class="form-group">
                                                <label for="contexto_geral">QUAL O CONTEXTO GERAL DO DESEMPENHO ESCOLAR DA CRIANÇA?</label>
                                                <textarea name="contexto_geral" id="contexto_geral" class="form-control" placeholder="Descreva" cols="30" rows="10"><?= $contexto_geral; ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_relacao_escola" <?= $idade_escolar_atual == 1 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">EM RELAÇÃO À ESCOLA A CRIANÇA DEMONSTRA:</label>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <input type="radio" <?= $relacao_escola == 1 ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="demonstra" id="sim_vontade" value="1">
                                                        <label for="sim_vontade">VONTADE/INTERESSE</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="radio" <?= $relacao_escola == 0 || $relacao_escola == "" ? "checked='true'" : "" ?> class="with-gap radio-col-info" name="demonstra" id="nao_resistencia" value="0">
                                                        <label for="nao_resistencia">RESITÊNCIA/DESINTERESSE</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_dre" <?= $idade_escolar_atual == 1 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <div id="div_descricao_relacao_escola" class="form-group">
                                                <label for="descricao_relacao_escola">DESCREVA</label>
                                                <textarea name="descricao_relacao_escola" id="descricao_relacao_escola" class="form-control" placeholder="Descreva" cols="30" rows="10"><?= $descricao_relacao_escola ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_intervencoes_inciais" <?= $idade_escolar_atual == 1 ? "" : "style='display: none'"; ?> class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="box box-outline-info">
                                                <div class="box-header">
                                                    <strong>INTERVENÇÕES INICIAIS</strong>
                                                </div>
                                                <div class="box-body">
                                                    <div id="div_descricao_intervencoes" class="form-group">
                                                        <label for="descricao_intervencoes">DESCREVA</label>
                                                        <textarea name="descricao_intervencoes" id="descricao_intervencoes" class="form-control" placeholder="Descreva as interveções iniciais" cols="30" rows="10"><?= $descricao_intervencoes; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- FIM EM CASO DE SIM APARECER -->
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="row mb-2">
                <div class="col-md-12 text-center">
                    <button id="atualizar" type="button" class="btn btn-info">ATUALIZAR</button>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/pia/educacao.js"></script>