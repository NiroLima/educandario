<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT * 
                 FROM seg_setor ss  
                 WHERE ss.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_setor = $result->fetch(PDO::FETCH_ASSOC);

    $setor_id = $dados_setor['id'];
    $setor_nome = $dados_setor['nome'];
	$setor_responsavel = $dados_setor['responsavel'];
} else {
    $setor_id = "";
    $setor_nome = "";
	$setor_responsavel = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/setores/lista">Setores</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_setor" name="form_setor" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $setor_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-chair-office"></i> <strong>NOVO SETOR</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_setor" class="form-group">
                                    <label for="setor">SETOR</label>
                                    <input type="text" class="form-control" name="setor" id="setor" placeholder="Nome do setor" value="<?= $setor_nome; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="div_responsavel" class="form-group">
                                    <label for="responsavel">RESPONSÁVEL</label>
                                    <select name="responsavel" id="responsavel" class="form-control select2">
                                              <option value="">Escolha o responsável</option>
												<?php
												$result8 = $db->prepare("SELECT nome, id 
															 FROM seg_usuario
															 WHERE status = 1 
															 ORDER BY nome ASC");
												$result8->execute();
												while ($responsavel = $result8->fetch(PDO::FETCH_ASSOC)) {
													if ($setor_responsavel == $responsavel['id']) {
														?>
														<option selected="true" value='<?= $responsavel['id']; ?>'><?= $responsavel['nome']; ?></option>
														<?php
													} else {
														?>
														<option value='<?= $responsavel['id']; ?>'><?= $responsavel['nome']; ?></option>
														<?php
													}
												}
												?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
						<button <?= $setor_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $setor_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/setores/novo.js"></script>