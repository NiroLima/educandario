<footer class="main-footer">
    &copy; 2020 <a href="https://www.acre.gov.br/">Governo do Estado do Acre</a>. Todos os direitos reservados.
</footer>

<script type="text/javascript" src="<?= PORTAL_URL; ?>assets/js/jquery.min.js"></script>
<!-- Vendor JS -->
<script src="<?= PORTAL_URL; ?>assets/js/vendors.min.js"></script>
<script src="<?= PORTAL_URL; ?>assets/icons/feather-icons/feather.min.js"></script>	
<script src="<?= PORTAL_URL; ?>assets/vendor_components/jquery-steps-master/build/jquery.steps.js"></script>

<script src="<?= PORTAL_URL; ?>assets/vendor_components/apexcharts-bundle/dist/apexcharts.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/progressbar.js-master/dist/progressbar.js"></script>

<script src="<?= PORTAL_URL; ?>assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/select2/dist/js/select2.full.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/moment/min/moment.min.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?= PORTAL_URL; ?>assets/vendor_plugins/iCheck/icheck.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?= PORTAL_URL; ?>assets/vendor_components/sweetalert/sweetalert2.min.js"></script>

<!-- JS UTIL -->
<script src="<?= PORTAL_URL ?>utils/utils.js" type="text/javascript"></script>
<script src="<?= PORTAL_URL ?>utils/projeto.utils.js" type="text/javascript"></script>

<!-- Máscaras dos Inputs! -->
<script src="<?= PORTAL_URL; ?>assets/input-mask/input-mask.min.js"></script>

<!-- Florence Admin App -->
<script src="<?= PORTAL_URL; ?>assets/js/template.js"></script>
<script src="<?= PORTAL_URL; ?>assets/js/pages/dashboard.js"></script>
<script src="<?= PORTAL_URL; ?>assets/js/demo.js"></script>
<script src="<?= PORTAL_URL; ?>assets/js/pages/advanced-form-element.js"></script>


</body>
</html>