<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
        FROM mod_acolhimento ma   
        WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result22 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result22->bindValue(1, $acolhimento_id);
    $result22->execute();
    $resultado22 = $result22->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_id = $resultado22['id'];
    $acolhimento_responsavel = $resultado22['usuario_id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_avaliacao maca   
        WHERE maca.acolhimento_crianca_id = ?");
    $result2->bindValue(1, $acolhimento_crianca_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $avaliacao_id = $resultado2['id'];
    $contextualiziacao = $resultado2['contextualiziacao'];
    $interesse = $resultado2['interesse'];
    $intervencoes = $resultado2['intervencoes'];
    $modificacoes = $resultado2['modificacoes'];
    $indicativos = $resultado2['indicativos'];
    $justificacao = $resultado2['justificacao'];
    $vf = $resultado2['vf'] == "" ? 0 : $resultado2['vf'];
} else {
    $avaliacao_id = "";
    $contextualiziacao = "";
    $interesse = "";
    $intervencoes = "";
    $modificacoes = "";
    $indicativos = "";
    $justificacao = "";
    $acolhimento_id = "";
    $acolhimento_crianca_id = "";
    $acolhimento_responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
    $vf = 0;
}
//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);
//------------------------------------------------------------------------------
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/pia/lista">PIA</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <h4 class="text-center">PLANO INDIVIDUAL DE ATENDIMENTO - PIA</h4>

        <!-- Main content -->
        <section class="content">

            <!-- Step wizard -->
            <div id="impressao" class="box box-solid bg-info">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="box-title">PIA</h4>
                            <h6 class="box-subtitle">PLANO INDIVIDUAL DE ATENDIMENTO</h6>
                        </div>
                        <div class="col-md-3">
                            <h4 class="text-right"><?= $barra; ?>%</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <?= $barra; ?>%">
                                    <span class="sr-only"><?= $barra; ?>% Complete</span>
                                </div>
                            </div>
                        </div>      
                    </div>    
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="box-title">RESPONSÁVEL DO PIA</h4>

                            <div id="responsavel_edite_1">
                                <h6 class="subtitle"><?= $nome_responsavel; ?> <a href="#" class="edit-responsavel"><i onclick="editar_responsavel()" class="fal fa-pencil-alt"></i></a></h6>
                            </div>

                            <div id="responsavel_edite_2" style="display: none">
                                <form id="form_responsavel" name="form_responsavel" method="POST" action="#" class="tab-wizard wizard-circle">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required="true" id="responsavel_id" name="responsavel_id">
                                                <option value="">Selecione o responsável</option>
                                                <?php
                                                $result = $db->prepare("SELECT su.id, su.nome           
                                                            FROM seg_usuario su    
                                                            WHERE su.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_responsavel == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <button href="#" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="div_validacao" class="col-md-2 text-right">
                            <a <?= is_numeric($vf) && $vf == 0 ? "" : "style='display: none'"; ?> href="#" onclick="validar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-success mt-2"><i class="fas fa-check"></i> VALIDAR</a>
                            <a <?= is_numeric($vf) && $vf == 1 ? "" : "style='display: none'"; ?> href="#" onclick="desvalidar(<?= $acolhimento_crianca_id; ?>)" class="btn-arquivo btn-warning mt-2"><i class="fas fa-remove"></i> DESVALIDAR</a>
                        </div>
                        <div class="col-md-2 text-right">
                            <a onclick="gerar_pdf()" style="cursor: pointer" class="btn-arquivo mt-2"><i class="fas fa-file-pdf"></i> GERAR PDF</a>
                        </div>
                    </div> 
                </div>
                <!-- /.box-header -->
                <div class="box-body wizard-content">

                    <?php include 'layout/menu_wizard.php'; ?>

                    <br/>

                    <form id="form_avaliacao" name="form_avaliacao" method="POST" action="#" class="tab-wizard wizard-circle">

                        <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>
                        <input type="hidden" id="acolhimento_crianca_id" name="acolhimento_crianca_id" value="<?= $acolhimento_crianca_id; ?>"/>
                        <input type="hidden" id="avaliacao_id" name="avaliacao_id" value="<?= $avaliacao_id; ?>"/>
                        <!-- Step 8 -->
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box box-outline-info">
                                        <div class="box-header">
                                            <strong>AVALIAÇÃO</strong>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="div_descreva_contextualizacao_caso" class="form-group">
                                                        <label for="descreva_contextualizacao_caso">CONTEXTUALIZAÇÃO DO CASO</label>
                                                        <textarea name="descreva_contextualizacao_caso" id="descreva_contextualizacao_caso" class="form-control" cols="30" rows="10"><?= $contextualiziacao; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div id="div_descreva_assumir" class="form-group">
                                                        <label for="descreva_assumir">Qual o interesse manifesto e os compromissos assumidos por cada um dos membros da família para assumir/reassumir a guarda da criança?</label>
                                                        <textarea name="descreva_assumir" id="descreva_assumir" class="form-control" cols="30" rows="10"><?= $interesse; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Após intervenções da equipe técnica, ocorreram modificações na situação e/ou no relacionamento familiar que resultaram na superação da(s) causa(s) do acolhimento, justificando a retomada da guarda?</label>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <input type="radio" class="with-gap radio-col-info" <?= $intervencoes == 1 ? "checked=true" : ""; ?> name="opcao_retono_intervencoes" id="sim_intervencao_tecnica" value="1">
                                                                <label for="sim_intervencao_tecnica">SIM</label>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input type="radio" class="with-gap radio-col-info" <?= $intervencoes == 0 || $intervencoes == "" ? "checked=true" : ""; ?> name="opcao_retono_intervencoes" id="nao_intervencao_tecnica" value="1">
                                                                <label for="nao_intervencao_tecnica">NÃO</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="div_modificacoes" <?= $intervencoes == 1 ? "" : "style='display: none'"; ?> class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="modificacoes">Quais modificações?</label>
                                                        <textarea name="modificacoes" id="modificacoes" class="form-control" cols="30" rows="10"><?= $modificacoes; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Há indicativos da possibilidade imediata ou em curto/médio prazo da reinserção familiar da criança?</label>
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <input type="radio" class="with-gap radio-col-info" <?= $indicativos == 1 ? "checked=true" : ""; ?> name="opcao_retono_indicativos" id="sim_reinsercao" value="1">
                                                                <label for="sim_reinsercao">SIM</label>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input type="radio" class="with-gap radio-col-info" <?= $indicativos == 0 || $intervencoes == "" ? "checked=true" : ""; ?> name="opcao_retono_indicativos" id="nao_reinsercao" value="1">
                                                                <label for="nao_reinsercao">NÃO</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="div_justificativa" <?= $indicativos == 1 ? "" : "style='display: none'"; ?> class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="justificativa">JUSTIFIQUE</label>
                                                        <textarea name="justificativa" id="justificativa" class="form-control" cols="30" rows="10"><?= $justificacao; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="row mb-2">
                <div class="col-md-12 text-center">
                    <button id="atualizar" type="button" class="btn btn-info">ATUALIZAR</button>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JAVASCRIPT BÁSICOS -->
<script src="<?= PORTAL_URL ?>assets/vendor_components/jquery-steps-master/build/jquery.steps.js"></script>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/pia/avaliacao.js"></script>