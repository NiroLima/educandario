<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$qual_atividade = isset($_POST['qual_atividade']) && $_POST['qual_atividade'] != "" ? $_POST['qual_atividade'] : NULL;
$nome_acompanha = isset($_POST['nome_acompanha']) && $_POST['nome_acompanha'] != "" ? $_POST['nome_acompanha'] : NULL;
$qual_forma = isset($_POST['qual_forma']) && $_POST['qual_forma'] != "" ? $_POST['qual_forma'] : NULL;
$frequencia_acompanhamento = isset($_POST['frequencia_acompanhamento']) && $_POST['frequencia_acompanhamento'] != "" ? $_POST['frequencia_acompanhamento'] : NULL;
$acomp_id = isset($_POST['acomp_id']) && $_POST['acomp_id'] != "" ? $_POST['acomp_id'] : "";

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($acomp_id)) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_educacao_acomp SET quem_realiza = ?, forma_realiza = ?, frequencia = ?, qual_atividade = ?, responsavel_id = ? WHERE id = ?");
            $sql4->bindValue(1, $nome_acompanha);
            $sql4->bindValue(2, $qual_forma);
            $sql4->bindValue(3, $frequencia_acompanhamento);
            $sql4->bindValue(4, $qual_atividade);
            $sql4->bindValue(5, $_SESSION['id']);
            $sql4->bindValue(6, $acomp_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_educacao_acomp (educacao_id, quem_realiza, forma_realiza, frequencia, qual_atividade, responsavel_id, data_cadastro) VALUES (?, ?, ?, ?, ?, ?, NOW())");
            $sql4->bindValue(1, $id);
            $sql4->bindValue(2, $nome_acompanha);
            $sql4->bindValue(3, $qual_forma);
            $sql4->bindValue(4, $frequencia_acompanhamento);
            $sql4->bindValue(5, $qual_atividade);
            $sql4->bindValue(6, $_SESSION['id']);
            $sql4->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>