//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('#form_setor').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var setor = $('#setor').val();
			var responsavel = $('#responsavel').val();

            $.post(PORTAL_URL + "admin/dao/setores/novo.php", {id: id, setor: setor, responsavel: responsavel}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário do Setor",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Setor",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/setores/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    var id = $("#id").val();
    var setor = $("#setor").val();
	var responsavel = $("#responsavel").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS
	
	    //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (responsavel == "") {
            $('div#div_responsavel').after('<label id="erro_responsavel" class="error">Responsável é obrigatório.</label>');
            valido = false;
            element = $('div#div_responsavel');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (setor == "") {
            $('div#div_setor').after('<label id="erro_setor" class="error">Nome do setor é obrigatório.</label>');
            valido = false;
            element = $('div#div_setor');
        }
    } 

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------