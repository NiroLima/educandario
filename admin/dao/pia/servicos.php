<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$nome_medico = isset($_POST['nome_medico']) && $_POST['nome_medico'] != "" ? $_POST['nome_medico'] : NULL;
$medicamento = isset($_POST['medicamento']) && $_POST['medicamento'] != "" ? $_POST['medicamento'] : NULL;
$posologia = isset($_POST['posologia']) && $_POST['posologia'] != "" ? $_POST['posologia'] : NULL;
$administracao = isset($_POST['administracao']) && $_POST['administracao'] != "" ? $_POST['administracao'] : NULL;
$servico_id = isset($_POST['servico_id']) && $_POST['servico_id'] != "" ? $_POST['servico_id'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        if (is_numeric($servico_id)) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_servico SET medico = ?, medicamento = ?, posologia = ?, administracao = ?, responsavel_id = ? WHERE id = ?");
            $sql4->bindValue(1, $nome_medico);
            $sql4->bindValue(2, $medicamento);
            $sql4->bindValue(3, $posologia);
            $sql4->bindValue(4, $administracao);
            $sql4->bindValue(5, $_SESSION['id']);
            $sql4->bindValue(6, $servico_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_servico (acolhimento_id, medico, medicamento, posologia, administracao, responsavel_id, data_cadastro) VALUES (?, ?, ?, ?, ?, ?, NOW())");
            $sql4->bindValue(1, $id);
            $sql4->bindValue(2, $nome_medico);
            $sql4->bindValue(3, $medicamento);
            $sql4->bindValue(4, $posologia);
            $sql4->bindValue(5, $administracao);
            $sql4->bindValue(6, $_SESSION['id']);
            $sql4->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>