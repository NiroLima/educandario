//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('#form_medico').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var nome_medico = $('#nome_medico').val();
            var especialidade = $('#especialidade').val();
            var tipo_funcionario = $('#tipo_funcionario').val();

            $.post(PORTAL_URL + "admin/dao/medicos/novo.php", {id: id, nome_medico: nome_medico, especialidade: especialidade, tipo_funcionario: tipo_funcionario}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário do Médico",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Médico",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/medicos/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    
    var nome_medico = $('#nome_medico').val();
    var especialidade = $('#especialidade').val();
    var tipo_funcionario = $('#tipo_funcionario').val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (tipo_funcionario == "") {
            $('div#div_tipo_funcionario').after('<label id="erro_tipo_funcionario" class="error">Tipo é obrigatório.</label>');
            valido = false;
            element = $('div#div_tipo_funcionario');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (especialidade == "") {
            $('div#div_especialidade').after('<label id="erro_especialidade" class="error">Especialidade é obrigatório.</label>');
            valido = false;
            element = $('div#div_especialidade');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome_medico == "") {
            $('div#div_nome_medico').after('<label id="erro_nome_medico" class="error">Nome do médico é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_medico');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------