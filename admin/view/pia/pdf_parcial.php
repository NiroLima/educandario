<?php

include_once('conf/config.php');
include_once('utils/permissoes.php');
include_once('conf/Url.php');
$db = Conexao::getInstance();

$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

$pagina = Url::getURL(3);

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
                            FROM mod_acolhimento ma   
                            WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
    $acolhimento_quem_trouxe = $resultado['quem_trouxe'];
    $acolhimento_responsavel_entrega = $resultado['responsavel_entrega'];
    $acolhimento_funcao_responsavel = $resultado['funcao_responsavel'];
    $acolhimento_celular = $resultado['celular'];
    $acolhimento_data_entrada = $resultado['data_entrada'];
    $acolhimento_hora_entrada = $resultado['hora_entrada'];
    $acolhimento_profissional_id = $resultado['profissional_id'];
    $obs = $resultado['obs'] . "  kjad aaldj ajadak aksd akjd adja a dsk ajdajad akdaja dklajdk akaj dkajd kasljddakajd ajdaskl jask djaskdjda kjas skldja aja akld akljdalkj alkjj daskslsjdakl lkaja jdalskdj dalkjd ka dklasj klaj kaljdlkas dlka lka dlka ddlkaj lakajdlka jdkdlaa jklasjdadkl jd";
    $numero_guia = $resultado['numero_guia'];
    $numero_processo = $resultado['numero_processo'];
    $numero_destituicao = $resultado['numero_destituicao'];

    $motivo_aba = $resultado['motivo_aba'];
    $motivo_neg = $resultado['motivo_neg'];
    $motivo_abu = $resultado['motivo_abu'];
    $motivo_out = $resultado['motivo_out'];
    $descricao = $resultado['descricao'];

    $result_prof = $db->prepare("SELECT sp.id, sp.registro, sp.nome AS profissional, ss.nome AS setor, fc.nome AS funcao     
                            FROM seg_profissional sp 
                            LEFT JOIN seg_funcao AS fc ON fc.id = sp.funcao_id    
                            LEFT JOIN seg_setor AS ss ON ss.id = fc.setor_id  
                            WHERE sp.status = 1 AND sp.id = ?");
    $result_prof->bindValue(1, $acolhimento_profissional_id);
    $result_prof->execute();

    $prof = $result_prof->fetch(PDO::FETCH_ASSOC);

    $pro_nome = $prof['profissional'];
    $funcao_profissional = $prof['funcao'];
    $setor_profissional = $prof['setor'];
    $registro_classe = $prof['registro'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result2->bindValue(1, $acolhimento_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $crianca_id = $resultado2['id'];
    $crianca_foto = $resultado2['crianca_foto'];
    $crianca_nome = $resultado2['nome'];
    $pai = $resultado2['pai'];
    $mae = $resultado2['mae'];
    $genero = $resultado2['genero'];
    $nascimento = $resultado2['nascimento'];
    $nascionalidade = $resultado2['nascionalidade'];
    $naturalidade = $resultado2['naturalidade'];
    $religiao = pesquisar("nome", "seg_religiao", "id", "=", $resultado2['religiao_id'], "");
    $cor_id = $resultado2['cor_id'];
    $rua = $resultado2['rua'];
    $numero = $resultado2['numero'];
    $bairro = $resultado2['bairro'];
    $estado_id = pesquisar("nome", "bsc_estado", "id", "=", $resultado2['estado_id'], "");
    $cidade_id = pesquisar("nome", "bsc_cidade", "id", "=", $resultado2['cidade_id'], "");
    $responsavel = $resultado2['usuario_id'];
    $responsavel_nome = pesquisar("nome", "seg_usuario", "id", "=", $responsavel, "");
    $vf = $resultado2['vf'] == "" ? 0 : $resultado2['vf'];
    $qual_deficiencia = $resultado2['qual_deficiencia'];
    $necessita_equipamentos = $resultado2['necessita_equipamentos'];
    $qual_tratamento = $resultado2['qual_tratamento'];
    $indicio_disturbio = $resultado2['indicio_disturbio'];
    $qual_doenca_infectocontagiosa = $resultado2['qual_doenca_infectocontagiosa'];
    $obs_saude = $resultado2['obs_saude'];
//------------------------------------------------------------------------------
    $result333 = $db->prepare("SELECT mm.id, mm.nome           
                             FROM mod_medicamento mm   
                             WHERE mm.status = 1 AND mm.id IN (SELECT medicamento_id FROM mod_acolhimento_crianca_medicacao WHERE acolhimento_crianca_id = ?)");
    $result333->bindValue(1, $crianca_id);
    $result333->execute();

    $qtd_medicamento = "";

    while ($medicamentos = $result333->fetch(PDO::FETCH_ASSOC)) {
        $qtd_medicamento .= $medicamentos['nome'] . "; ";
    }

//------------------------------------------------------------------------------
//Resultado do mod_acolhimento_crianca_geral
    $result33 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca_geral macg   
                            WHERE macg.acolhimento_crianca_id = ?");
    $result33->bindValue(1, $crianca_id);
    $result33->execute();
    $resultado33 = $result33->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_geral_id = $resultado33['id'];
    $peso = $resultado33['peso'];
    $altura = $resultado33['altura'];
    $tipo_sanguineo = $resultado33['tipo_sanguineo'];
    $condicoes_saude = $resultado33['condicao_geral'];
    $atvd_psicologia = $resultado33['atvd'];
    $justificativa = $resultado33['justificativa'];
    $atividade_psicologia = $resultado33['atividade_psicologia'];
    $def_descricao = $resultado33['def_descricao'];
    $bcp = $resultado33['def_beneficiaria'];
    $responsavel_recebimento = $resultado33['def_responsavel_recebimento'];
//------------------------------------------------------------------------------
    $result3 = $db->prepare("SELECT *      
                            FROM mod_acolhimento_situacao mas   
                            WHERE mas.mod_acolhimento_id = ?");
    $result3->bindValue(1, $acolhimento_id);
    $result3->execute();
    $resultado3 = $result3->fetch(PDO::FETCH_ASSOC);
    $acolhimento_situacao_id = $resultado3['id'];
    $acolhimento_residia = $resultado3['residia'];
    $acolhimento_tempo_rua = pesquisar("nome", "mod_acolhimento_tempo", "id", "=", $resultado3['tempo_rua'], "");
    $acolhimento_local = $resultado3['local'];
    $acolhimento_acolhida_anteriomente = $resultado3['acolhida_anteriomente'];
    $acolhimento_servico_acolhimento = $resultado3['servico_acolhimento'];
    $acolhimento_parentes_vinculos = $resultado3['parentes_vinculos'];
    $acolhimento_pessoas_vivia_rua = $resultado3['pessoas_vivia_rua'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result9 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_rede macr   
        WHERE macr.acolhimento_crianca_id = ?");
    $result9->bindValue(1, $crianca_id);
    $result9->execute();
    $resultado9 = $result9->fetch(PDO::FETCH_ASSOC);

    $rede_id = $resultado9['id'];
    $acompanhamento = $resultado9['acompanhamento'];
    $acompanhamento2 = $resultado9['acompanhamento2'];
//------------------------------------------------------------------------------
    $result7 = $db->prepare("SELECT *      
                            FROM mod_acolhimento_crianca_educacao mace   
                            WHERE mace.acolhimento_crianca_id = ?");
    $result7->bindValue(1, $crianca_id);
    $result7->execute();
    $resultado7 = $result7->fetch(PDO::FETCH_ASSOC);

    $educacao_id = $resultado7['id'];

    $justique_ausencia = $resultado7['justique_ausencia'];

    $justifique = $resultado7['justifique'];
    $justifique2 = $resultado7['justifique2'];

    $idade_escolar = $resultado7['idade_escolar'];
    $idade_escolar_atual = $resultado7['idade_escolar_atual'];
    $escola = $resultado7['escola'];
    $serie = $resultado7['serie'];
    $turno = $resultado7['turno'];
    $escola1 = $resultado7['escola1'];
    $serie1 = $resultado7['serie1'];
    $turno1 = $resultado7['turno1'];
    $frequenta_instituicao = $resultado7['frequenta_instituicao'];
    $justificacao_ausencia = $resultado7['justificacao_ausencia'];

    $frequencia = $resultado7['frequencia'];
    $sobre_frequencia = $resultado7['sobre_frequencia'];
    $socializacao = $resultado7['socializacao'];
    $sobre_socializacao = $resultado7['sobre_socializacao'];
    $interesse = $resultado7['interesse'];
    $sobre_interesse = $resultado7['sobre_interesse'];
    $rendimento = $resultado7['rendimento'];
    $sobre_rendimento = $resultado7['sobre_rendimento'];
    $participacao = $resultado7['participacao'];
    $sobre_participacao = $resultado7['sobre_participacao'];
    $apresenta_desenvolvimento = $resultado7['apresenta_desenvolvimento'];
    $descricao_aspectos = $resultado7['descricao_aspectos'];
    $contexto_geral = $resultado7['contexto_geral'];
    $relacao_escola = $resultado7['relacao_escola'];
    $descricao_relacao_escola = $resultado7['descricao_relacao_escola'];
    $descricao_intervencoes = $resultado7['descricao_intervencoes'];
    $espaco_estrutura = $resultado7['espaco_estrutura'];
    $rotinas = $resultado7['rotinas'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result10 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_avaliacao maca   
        WHERE maca.acolhimento_crianca_id = ?");
    $result10->bindValue(1, $crianca_id);
    $result10->execute();
    $result100 = $result10->fetch(PDO::FETCH_ASSOC);

    $avaliacao_id = $result100['id'];
    $contextualiziacao = $result100['contextualiziacao'];
    $interesse_avaliacao = $result100['interesse'];
    $intervencoes = $result100['intervencoes'];
    $modificacoes = $result100['modificacoes'];
    $indicativos = $result100['indicativos'];
    $justificacao = $result100['justificacao'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $resultado11 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_familia macf   
        WHERE macf.acolhimento_crianca_id = ?");
    $resultado11->bindValue(1, $crianca_id);
    $resultado11->execute();
    $result11 = $resultado11->fetch(PDO::FETCH_ASSOC);

    $mod_acolhimento_crianca_familia_id = $result11['id'];
    $mae_nome = $result11['mae_nome'];
    $mae_nascimento = $result11['mae_nascimento'];
    $mae_endereco = $result11['mae_endereco'];
    $mae_numero = $result11['mae_numero'];
    $mae_bairro = $result11['mae_bairro'];
    $mae_rg = $result11['mae_rg'];
    $mae_cpf = $result11['mae_cpf'];
    $mae_celular = $result11['mae_celular'];
    $mae_ocupacao = $result11['mae_ocupacao'];
    $mae_situacao = $result11['mae_situacao'];

    $pai_nome = $result11['pai_nome'];
    $pai_nascimento = $result11['pai_nascimento'];
    $pai_endereco = $result11['pai_endereco'];
    $pai_numero = $result11['pai_numero'];
    $pai_bairro = $result11['pai_bairro'];
    $pai_rg = $result11['pai_rg'];
    $pai_cpf = $result11['pai_cpf'];
    $pai_celular = $result11['pai_celular'];
    $pai_ocupacao = $result11['pai_ocupacao'];
    $pai_situacao = $result11['pai_situacao'];

    $oferecem_condicoes = $result11['oferecem_condicoes'];
    $avaliacao_profunda = $result11['avaliacao_profunda'];
    $possui_irmaos = $result11['possui_irmaos'];

    $possui_irmaos_outro = $result11['possui_irmaos_outro'];
    $conhece_irmaos = $result11['conhece_irmaos'];

    $irmaos_adotados = $result11['irmaos_adotados'];

    $situacao_familiar = $result11['situacao_familiar'];

    $qual_outra_situacao = $result11['outra_situacao'];

    $demonstra_interesse = $result11['demonstra_interesse'];
    $descricao_familia = $result11['descricao'];
    $crianca_demonstra_interesse = $result11['crianca_demonstra_interesse'];
    $crianca_demonstra_descricao = $result11['crianca_demonstra_descricao'];

    $medida_restricao = $result11['medida_restricao'];
    $quem_estabelelceu = $result11['quem_estabelelceu'];
    $recebe_visita = $result11['recebe_visita'];
    $frequencia_encontros = $result11['frequencia_encontros'];
    $como_realizados = $result11['como_realizados'];
    $pais_envolvidos_alcool = $result11['pais_envolvidos_alcool'];
    $pais_envolvidos_outros = $result11['pais_envolvidos_outros'];
    $pais_envolvidos_abuso = $result11['pais_envolvidos_abuso'];
    $situacao_rua = $result11['situacao_rua'];
    $pais_pena = $result11['pais_pena'];
    $pais_reabilitacao = $result11['pais_reabilitacao'];
    $pais_ameacados = $result11['pais_ameacados'];
    $pais_doencas = $result11['pais_doencas'];
    $pais_transtorno = $result11['pais_transtorno'];
    $qual_doenca = $result11['qual_doenca'];
    $qual_transtorno = $result11['qual_transtorno'];

    $familiar_interesse = $result11['familiar_interesse'];

    $descricao_condicao_cuidado = $result11['descreva_condicao_cuidado'];
    $opniao_crianca = $result11['opiniao_crianca'];
    $familiar_condicao = $result11['opcao_familiar_condicao'];
} else {
    $acolhimento_id = "";
    $acolhimento_quem_trouxe = "";
    $acolhimento_responsavel_entrega = "";
    $acolhimento_funcao_responsavel = "";
    $acolhimento_celular = "";
    $acolhimento_data_entrada = "";
    $acolhimento_hora_entrada = "";
    $acolhimento_profissional_id = "";
    $funcao_profissional = "";
    $setor_profissional = "";
    $registro_classe = "";
    $obs = "";
    $numero_guia = "";
    $numero_processo = "";
    $numero_destituicao = "";

    $motivo_aba = "";
    $motivo_neg = "";
    $motivo_abu = "";
    $motivo_out = "";
    $descricao = "";

//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $crianca_foto = "";
    $crianca_id = "";
    $crianca_nome = "";
    $pai = "";
    $mae = "";
    $genero = "";
    $nascimento = "";
    $nascionalidade = "";
    $naturalidade = "";
    $religiao = "";
    $cor_id = "";
    $rua = "";
    $numero = "";
    $bairro = "";
    $estado_id = "";
    $cidade_id = "";
    $responsavel = $_SESSION['id'];
    $responsavel_nome = pesquisar("nome", "seg_usuario", "id", "=", $responsavel, "");
    $vf = 0;
    $qual_deficiencia = 0;
    $necessita_equipamentos = "";
    $qual_tratamento = "";
    $indicio_disturbio = 0;
    $qual_doenca_infectocontagiosa = "";
    $obs_saude = "";
//------------------------------------------------------------------------------
    $acolhimento_situacao_id = "";
    $acolhimento_residia = "";
    $acolhimento_tempo_rua = "";
    $acolhimento_local = "";
    $acolhimento_acolhida_anteriomente = "";
    $acolhimento_servico_acolhimento = "";
    $acolhimento_parentes_vinculos = "";
    $acolhimento_pessoas_vivia_rua = "";
//------------------------------------------------------------------------------   
//Resultado do mod_acolhimento_crianca_geral
    $acolhimento_crianca_geral_id = "";
    $peso = "";
    $altura = "";
    $tipo_sanguineo = "";
    $condicoes_saude = "";
    $atvd_psicologia = 0;
    $justificativa = "";
    $atividade_psicologia = 0;
    $acolhimento_responsavel = $_SESSION['id'];
    $nome_responsavel = pesquisar("nome", "seg_usuario", "id", "=", $acolhimento_responsavel, "");
    $bcp = "";
    $responsavel_recebimento = "";
//------------------------------------------------------------------------------
    $educacao_id = "";
    $idade_escolar = "";
    $idade_escolar_atual = "";
    $escola = "";
    $serie = "";
    $turno = "";
    $escola1 = "";
    $serie1 = "";
    $turno1 = "";
    $frequenta_instituicao = "";
    $justificacao_ausencia = "";
    $frequencia = "";
    $sobre_frequencia = "";
    $socializacao = "";
    $sobre_socializacao = "";
    $interesse = "";
    $sobre_interesse = "";
    $rendimento = "";
    $sobre_rendimento = "";
    $participacao = "";
    $sobre_participacao = "";
    $apresenta_desenvolvimento = "";
    $descricao_aspectos = "";
    $contexto_geral = "";
    $relacao_escola = "";
    $descricao_relacao_escola = "";
    $descricao_intervencoes = "";
    $espaco_estrutura = "";
    $rotinas = "";
//------------------------------------------------------------------------------
    $rede_id = "";
    $acompanhamento = "";
    $acompanhamento2 = "";
//------------------------------------------------------------------------------    
    $avaliacao_id = "";
    $contextualiziacao = "";
    $interesse_avaliacao = "";
    $intervencoes = "";
    $modificacoes = "";
    $indicativos = "";
    $justificacao = "";
//------------------------------------------------------------------------------
    $mod_acolhimento_crianca_familia_id = "";
    $mae_nome = "";
    $mae_nascimento = "";
    $mae_endereco = "";
    $mae_numero = "";
    $mae_bairro = "";
    $mae_rg = "";
    $mae_cpf = "";
    $mae_celular = "";
    $mae_ocupacao = "";
    $mae_situacao = "";

    $pai_nome = "";
    $pai_nascimento = "";
    $pai_endereco = "";
    $pai_numero = "";
    $pai_bairro = "";
    $pai_rg = "";
    $pai_cpf = "";
    $pai_celular = "";
    $pai_ocupacao = "";
    $pai_situacao = "";

    $oferecem_condicoes = "";
    $avaliacao_profunda = "";
    $possui_irmaos = "";

    $possui_irmaos_outro = "";
    $conhece_irmaos = "";
    $irmaos_adotados = "";
    $situacao_familiar = "";
    $qual_outra_situacao = "";
    $demonstra_interesse = "";
    $descricao_familia = "";
    $crianca_demonstra_interesse = "";
    $crianca_demonstra_descricao = "";

    $medida_restricao = "";
    $quem_estabelelceu = "";
    $recebe_visita = "";
    $frequencia_encontros = "";
    $como_realizados = "";
    $pais_envolvidos_alcool = "";
    $pais_envolvidos_outros = "";
    $pais_envolvidos_abuso = "";
    $situacao_rua = "";
    $pais_pena = "";
    $pais_reabilitacao = "";
    $pais_ameacados = "";
    $pais_doencas = "";
    $pais_transtorno = "";
    $qual_doenca = "";
    $qual_transtorno = "";
    $familiar_interesse = "";
    $descricao_condicao_cuidado = "";
    $opniao_crianca = "";
    $familiar_condicao = "";
}
//------------------------------------------------------------------------------
//Carregar dados a Barra
$barra = carregar_info_barra($acolhimento_id);

require('assets/fpdf182/fpdf.php');

$pdf = new FPDF('P', 'mm', 'A4');
$pdf->AddPage();

$pdf->SetFont('Arial', 'B', 10);

$pdf->SetTitle('PLANO INDIVIDUAL DE ATENDIMENTO - PIA');

$pdf->SetXY(15, 5);
$pdf->Cell(180, 10, utf8_decode('PLANO INDIVIDUAL DE ATENDIMENTO - PIA'), 0, '', 'C');

$pdf->SetFont('Arial', '', 10);

$pdf->SetFont('Arial', 'B', 10);
$pdf->SetXY(5, 20);
$pdf->Cell(180, 10, utf8_decode('Responsável pelo Pia: '), 0, '', 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->SetXY(45, 20);
$pdf->Cell(180, 10, utf8_decode($responsavel_nome), 0, '', 'L');

if (carregar_info_menu($acolhimento_id, 1)) {//Acolhido
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 30);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);

    $pdf->SetXY(5, 27);
    $pdf->MultiCell(200, 10, utf8_decode('ACOLHIDO(A)'), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetTextColor(0, 0, 0);
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 36);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 35);
    $pdf->Cell(180, 10, utf8_decode('Nome: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(18, 35);
    $pdf->Cell(180, 10, utf8_decode($crianca_nome), 0, '', 'L');
//------------------------------------------------------------------------------
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 40);
    $pdf->Cell(180, 10, utf8_decode('Gênero: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(21, 40);
    $pdf->Cell(180, 10, $genero == 1 ? "Masculino" : "Feminino", 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(75, 40);
    $pdf->Cell(180, 10, utf8_decode('Nascimento: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(99, 40);
    $pdf->Cell(180, 10, obterDataBRTimestamp($nascimento), 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(150, 40);
    $pdf->Cell(180, 10, utf8_decode('Religião: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(168, 40);
    $pdf->Cell(180, 10, utf8_decode(ctexto($religiao)), 0, '', 'L');
//------------------------------------------------------------------------------
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 45);
    $pdf->Cell(180, 10, utf8_decode('Nacionalidade: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(32, 45);
    $pdf->Cell(180, 10, utf8_decode($nascionalidade), 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(75, 45);
    $pdf->Cell(180, 10, utf8_decode('Naturalidade: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(100, 45);
    $pdf->Cell(180, 10, utf8_decode($naturalidade), 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(150, 45);
    $pdf->Cell(180, 10, utf8_decode('Cor: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(159, 45);
    $pdf->Cell(180, 10, cor($cor_id), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 50);
    $pdf->Cell(180, 10, utf8_decode('Pai: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(14, 50);
    $pdf->Cell(180, 10, utf8_decode($nascionalidade), 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(75, 50);
    $pdf->Cell(180, 10, utf8_decode('Mãe: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(86, 50);
    $pdf->Cell(180, 10, utf8_decode($naturalidade), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 55);
    $pdf->Cell(180, 10, utf8_decode('Rua: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(15, 55);
    $pdf->Cell(180, 10, utf8_decode($rua), 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(75, 55);
    $pdf->Cell(180, 10, utf8_decode('Número: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(92, 55);
    $pdf->Cell(180, 10, utf8_decode($numero), 0, '', 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 60);
    $pdf->Cell(180, 10, utf8_decode('Bairro: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(19, 60);
    $pdf->Cell(180, 10, utf8_decode($bairro), 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(75, 60);
    $pdf->Cell(180, 10, utf8_decode('Estado: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(90, 60);
    $pdf->Cell(180, 10, utf8_decode($estado_id), 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(150, 60);
    $pdf->Cell(180, 10, utf8_decode('Cidade: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(165, 60);
    $pdf->Cell(180, 10, utf8_decode($cidade_id), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 70);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
}

if ($pdf->GetY() > 230) {
    quebra($pdf);
}

if (carregar_info_menu($acolhimento_id, 2)) {//Acolhimento
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);

    $pdf->SetXY(5, 68);
    $pdf->MultiCell(200, 10, utf8_decode('ACOLHIMENTO'), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetTextColor(0, 0, 0);
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 76);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 76);
    $pdf->Cell(180, 10, utf8_decode('Quem trouxe:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(30, 76);
    $pdf->Cell(180, 10, quem($acolhimento_quem_trouxe), 0, '', 'L');
    //------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 81);
    $pdf->Cell(180, 10, utf8_decode('Responsável pela entrega:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(51, 81);
    $pdf->Cell(180, 10, utf8_decode($acolhimento_responsavel_entrega), 0, '', 'L');
    //------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 86);
    $pdf->Cell(180, 10, utf8_decode('Função do responsável:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(48, 86);
    $pdf->Cell(180, 10, utf8_decode($acolhimento_funcao_responsavel), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(150, 86);
    $pdf->Cell(180, 10, utf8_decode('Celular: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(165, 86);
    $pdf->Cell(180, 10, ($acolhimento_celular), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 91);
    $pdf->Cell(180, 10, utf8_decode('Data de entrada:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(35, 91);
    $pdf->Cell(180, 10, obterDataBRTimestamp($acolhimento_data_entrada), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(150, 91);
    $pdf->Cell(180, 10, utf8_decode('Hora de entrada: '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(180, 91);
    $pdf->Cell(180, 10, ($acolhimento_hora_entrada), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 96);
    $pdf->Cell(180, 10, utf8_decode('Nome do profissional:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(44, 96);
    $pdf->Cell(180, 10, utf8_decode($pro_nome), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(150, 96);
    $pdf->Cell(180, 10, utf8_decode('Função:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(165, 96);
    $pdf->Cell(180, 10, utf8_decode($funcao_profissional), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 101);
    $pdf->Cell(180, 10, utf8_decode('Setor:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(17, 101);
    $pdf->Cell(180, 10, utf8_decode($setor_profissional), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(150, 101);
    $pdf->Cell(180, 10, utf8_decode('Registro de classe:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(184, 101);
    $pdf->Cell(180, 10, ($registro_classe), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 110);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 109);
    $pdf->Cell(180, 10, utf8_decode('OBSERVAÇÃO(ÕES) SOBRE A CRIANÇA NO MOMENTO DO ACOLHIMENTO'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(5, 118);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(5, 120);
    $pdf->MultiCell(200, 5, utf8_decode($obs), 0, 'J');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('MOTIVO DO ACOLHIMENTO'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Abandono:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(26);
    $pdf->Cell(180, 10, $motivo_aba == 1 ? "Sim" : "Não", 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(50);
    $pdf->Cell(180, 10, utf8_decode('Negligência:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(74);
    $pdf->Cell(180, 10, $motivo_neg == 1 ? "Sim" : "Não", 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(105);
    $pdf->Cell(180, 10, utf8_decode('Abuso sexual:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(133);
    $pdf->Cell(180, 10, $motivo_abu == 1 ? "Sim" : "Não", 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(165);
    $pdf->Cell(180, 10, utf8_decode('Outros:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(180);
    $pdf->Cell(180, 10, $motivo_out == 1 ? "Sim" : "Não", 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('DADOS PROCESSUAIS'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Nº da guia de acolhimento:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(54);
    $pdf->Cell(180, 10, utf8_decode($numero_guia), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetX(5);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(87);
    $pdf->Cell(180, 10, utf8_decode('Nº do processo:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(117);
    $pdf->Cell(180, 10, utf8_decode($numero_processo), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetX(5);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(150);
    $pdf->Cell(180, 10, utf8_decode('Nº da destituição:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(183);
    $pdf->Cell(180, 10, utf8_decode($numero_destituicao), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('SOBRE A SITUAÇÃO DA CRIANÇA QUANDO ACOLHIDA'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Residia com a família:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(45);
    $pdf->Cell(180, 10, $acolhimento_residia == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(111);
    $pdf->Cell(180, 10, utf8_decode('Por quanto tempo esteve em situação de rua:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(191);
    $pdf->Cell(180, 10, utf8_decode($acolhimento_tempo_rua), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Local onde costumava se abrigar:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(64);
    $pdf->Cell(180, 10, utf8_decode($acolhimento_local), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Pessoas com quem vivia na rua:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(62);
    $pdf->Cell(180, 10, utf8_decode($acolhimento_pessoas_vivia_rua), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Parentes/conhecidos com quem mantinha vínculos:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(95);
    $pdf->Cell(180, 10, utf8_decode($acolhimento_parentes_vinculos), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('ACOLHIMENTOS ANTERIORES'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança já foi acolhida anteriomente:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(73);
    $pdf->Cell(180, 10, $acolhimento_acolhida_anteriomente == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Informe o serviço de acolhimento:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(65);
    $pdf->Cell(180, 10, utf8_decode($acolhimento_servico_acolhimento), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
}

if ($pdf->GetY() > 230) {
    quebra($pdf);
}

if (carregar_info_menu($acolhimento_id, 3)) {//Saúde
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);

    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(200, 10, utf8_decode('SAÚDE'), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetTextColor(0, 0, 0);
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("NECESSIDADES ESPECÍFICAS DE SAÚDE"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Possui alguma deficiência e/ou transtorno?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(80);
    $pdf->Cell(180, 10, $qual_deficiencia == 1 ? "Sim, com laudo" : utf8_decode($qual_deficiencia == 2 ? "Em investigação" : "Não se aplica"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança é beneficiária do benefício de prestação continuada (BPC)?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(124);
    $pdf->Cell(180, 10, $bcp == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');

    if ($bcp == 1) {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável pelo recebimento?'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(62);
        $pdf->Cell(180, 10, utf8_decode($responsavel_recebimento), 0, '', 'L');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('NECESSITA DE EQUIPAMENTOS/RECURSOS DE TECNOLOGIA ASSISTIVA'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($necessita_equipamentos), 0, 'J');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("TRATAMENTO"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Faz tratamento médico?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(48);
    $pdf->Cell(180, 10, $qual_tratamento == "" ? utf8_decode("Não") : "Sim", 0, '', 'L');

    if ($qual_tratamento != "") {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Qual tratamento?'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(36);
        $pdf->Cell(180, 10, utf8_decode($qual_tratamento), 0, '', 'L');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Indícios de Distúrbio Mental?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(56);
    $pdf->Cell(180, 10, $indicio_disturbio == "" ? utf8_decode("Não") : "Sim", 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Possui doença infectocontagiosa?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(65);
    $pdf->Cell(180, 10, $qual_doenca_infectocontagiosa == "" ? utf8_decode("Não") : "Sim", 0, '', 'L');

    if ($qual_doenca_infectocontagiosa != "") {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Qual doença infectocontagiosa?'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(62);
        $pdf->Cell(180, 10, utf8_decode($qual_doenca_infectocontagiosa), 0, '', 'L');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Vieram medicações com a criança?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(66);
    $pdf->Cell(180, 10, $qtd_medicamento != "" ? "Sim" : utf8_decode("Não"), 0, '', 'L');

    if ($qtd_medicamento != "") {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Quais medicamentos?'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(45);
        $pdf->Cell(180, 10, utf8_decode($qtd_medicamento), 0, '', 'L');
    }

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Observação:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(29);
    $pdf->Cell(180, 10, utf8_decode($obs_saude), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("DADOS GERAIS DE SAÚDE"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Peso(KG): '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(23);
    $pdf->Cell(180, 10, $peso, 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(55, $pdf->GetY());
    $pdf->Cell(180, 10, utf8_decode('Altura(CM): '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(76);
    $pdf->Cell(180, 10, $altura, 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetXY(110, $pdf->GetY());
    $pdf->Cell(180, 10, utf8_decode('Tipo sanguíneo (TIPO/ FATOR RH): '), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(171);
    $pdf->Cell(180, 10, $tipo_sanguineo, 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('CONDIÇÕES GERAIS DE SAÚDE DA CRIANÇA'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', '', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($condicoes_saude), 0, 'J');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança faz uso de alguma medicação?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(76);
    $pdf->Cell(180, 10, $atividade_psicologia == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');

    if ($atividade_psicologia == 1) {

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(50, 10, utf8_decode("MÉDICO"), 1, 'C', 1);
        $pdf->SetXY(55, $pdf->GetY() - 10);
        $pdf->MultiCell(50, 10, utf8_decode("MEDICAMENTO"), 1, 'C', 1);
        $pdf->SetXY(105, $pdf->GetY() - 10);
        $pdf->MultiCell(50, 10, utf8_decode("POSOLOGIA"), 1, 'C', 1);
        $pdf->SetXY(155, $pdf->GetY() - 10);
        $pdf->MultiCell(48, 10, utf8_decode("ADMINISTRAÇÃO"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 10);
        $pdf->SetTextColor(0, 0, 0);

        $result5 = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_servico macs   
                            WHERE macs.acolhimento_id = ?");
        $result5->bindValue(1, $acolhimento_id);
        $result5->execute();
        while ($servicos = $result5->fetch(PDO::FETCH_ASSOC)) {
            $pdf->SetX(5);
            $pdf->MultiCell(50, 10, utf8_decode($servicos['medico']), 1, 'C');
            $pdf->SetXY(55, $pdf->GetY() - 10);
            $pdf->MultiCell(50, 10, utf8_decode($servicos['medicamento']), 1, 'C');
            $pdf->SetXY(105, $pdf->GetY() - 10);
            $pdf->MultiCell(50, 10, utf8_decode($servicos['posologia']), 1, 'C');
            $pdf->SetXY(155, $pdf->GetY() - 10);
            $pdf->MultiCell(48, 10, utf8_decode($servicos['administracao']), 1, 'C');
        }
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança está inserida em alguma atividade do serviço de psicologia?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(126);
    $pdf->Cell(180, 10, $atvd_psicologia == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Justifique/Especifique:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(46);
    $pdf->Cell(180, 10, utf8_decode($justificativa), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
}

if ($pdf->GetY() > 230) {
    quebra($pdf);
}

if (carregar_info_menu($acolhimento_id, 4)) {//Educação
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);

    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(200, 10, utf8_decode('EDUCAÇÃO'), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetTextColor(0, 0, 0);
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("SITUAÇÃO ANTERIOR AO ACOLHIMENTO"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Criança em idade escolar?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(52);
    $pdf->Cell(180, 10, $idade_escolar == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');

    if ($idade_escolar == 1) {
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Escola: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(20);
        $pdf->Cell(180, 10, utf8_decode($escola), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetXY(105, $pdf->GetY());
        $pdf->Cell(180, 10, utf8_decode('Série: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(117);
        $pdf->Cell(180, 10, utf8_decode($serie), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetXY(170, $pdf->GetY());
        $pdf->Cell(180, 10, utf8_decode('Turno: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(183);
        $pdf->Cell(180, 10, utf8_decode($turno), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Frequência:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(28);
        $pdf->Cell(180, 10, opcao($frequencia), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Sobre a Frequência:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(42);
        $pdf->Cell(180, 10, utf8_decode($sobre_frequencia), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Socialização:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, opcao($socializacao), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Sobre a Socialização:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(44);
        $pdf->Cell(180, 10, utf8_decode($sobre_socializacao), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Interesse nas Atividades Escolares:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(68);
        $pdf->Cell(180, 10, opcao($interesse), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Sobre o Interesse nas Atividades Escolares:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(82);
        $pdf->Cell(180, 10, utf8_decode($sobre_interesse), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Rendimento:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(29);
        $pdf->Cell(180, 10, opcao($rendimento), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Sobre o Rendimento Escolar:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(56);
        $pdf->Cell(180, 10, utf8_decode($sobre_rendimento), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Participação da Família na Escola:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(66);
        $pdf->Cell(180, 10, opcao($participacao), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Sobre a Participação da Família na Escola:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(80);
        $pdf->Cell(180, 10, utf8_decode($sobre_participacao), 0, '', 'L');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Justifique:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(25);
    $pdf->Cell(180, 10, utf8_decode($justique_ausencia), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    

    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }

//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("SITUAÇÃO ATUAL"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Criança em idade escolar?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(52);
    $pdf->Cell(180, 10, $idade_escolar_atual == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------       
    if ($idade_escolar_atual == 1) {
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Escola: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(20);
        $pdf->Cell(180, 10, utf8_decode($escola1), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetXY(105, $pdf->GetY());
        $pdf->Cell(180, 10, utf8_decode('Série: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(117);
        $pdf->Cell(180, 10, utf8_decode($serie1), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetXY(170, $pdf->GetY());
        $pdf->Cell(180, 10, utf8_decode('Turno: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(183);
        $pdf->Cell(180, 10, utf8_decode($turno1), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('A criança possui rotinas estabelecidas para a realização de tarefas/trabalhos escolares com acompanhamento?'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(195);
        $pdf->Cell(180, 10, $rotinas == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
        if ($rotinas == 1) {
//------------------------------------------------------------------------------    
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Ln();
            $pdf->SetX(5);
            $pdf->MultiCell(70, 10, utf8_decode("QUEM REALIZA O ACOMPANHAMENTO"), 1, 'C', 1);
            $pdf->SetXY(75, $pdf->GetY() - 10);
            $pdf->MultiCell(80, 10, utf8_decode("FORMA QUE É REALIZADA"), 1, 'C', 1);
            $pdf->SetXY(155, $pdf->GetY() - 10);
            $pdf->MultiCell(50, 10, utf8_decode("COM QUAL FREQUÊNCIA"), 1, 'C', 1);

            $pdf->SetFont('Arial', '', 10);
            $pdf->SetTextColor(0, 0, 0);

            $result88 = $db->prepare("SELECT *           
                                         FROM mod_acolhimento_crianca_educacao_rotinas macer   
                                         WHERE macer.educacao_id = ?");
            $result88->bindValue(1, $educacao_id);
            $result88->execute();
            while ($acomp = $result88->fetch(PDO::FETCH_ASSOC)) {
                $pdf->SetX(5);
                $pdf->MultiCell(70, 10, utf8_decode($acomp['quem_realiza']), 1, 'C');
                $pdf->SetXY(75, $pdf->GetY() - 10);
                $pdf->MultiCell(80, 10, utf8_decode($acomp['forma_realiza']), 1, 'C');
                $pdf->SetXY(155, $pdf->GetY() - 10);
                $pdf->MultiCell(50, 10, utf8_decode($acomp['frequencia']), 1, 'C');
            }
//------------------------------------------------------------------------------    
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Ln();
            $pdf->SetX(5);
            $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
            if ($pdf->GetY() > 230) {
                quebra($pdf);
            }
//------------------------------------------------------------------------------
        } else {
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Ln();
            $pdf->SetX(5);
            $pdf->Cell(180, 10, utf8_decode('Justifique:'), 0, '', 'L');
            $pdf->Ln();
            $pdf->SetFont('Arial', '', 10);
            $pdf->SetX(5);
            $pdf->MultiCell(200, 5, utf8_decode($justifique), 0, 'J');
        }
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Qual o contexto geral do desempenho escolar da criança:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(105);
        $pdf->Cell(180, 10, utf8_decode($contexto_geral), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Em relação a escola a criança demostra:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(75);
        $pdf->Cell(180, 10, $relacao_escola == 1 ? "Vontade/Interesse" : utf8_decode("Resistência/Desinteresse"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Descreva:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($descricao_relacao_escola), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
        //------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("INTERVENÇÕES INICIAIS"), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Descreva:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($descricao_intervencoes), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
}
//------------------------------------------------------------------------------  
if ($pdf->GetY() > 230) {
    quebra($pdf);
}
//------------------------------------------------------------------------------  
if (carregar_info_menu($acolhimento_id, 5)) {//Família
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);

    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(200, 10, utf8_decode('FAMÍLIA'), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetTextColor(0, 0, 0);
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("MÃE"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Nome:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(18);
    $pdf->Cell(135, 10, $mae_nome, 0, '', 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(160);
    $pdf->Cell(180, 10, utf8_decode('Nascimento:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(183);
    $pdf->Cell(180, 10, obterDataBRTimestamp($mae_nascimento), 0, '', 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Endereço:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(25);
    $pdf->Cell(135, 10, $mae_endereco, 0, '', 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(90);
    $pdf->Cell(180, 10, utf8_decode('Número:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(107);
    $pdf->Cell(180, 10, $mae_numero, 0, '', 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(130);
    $pdf->Cell(180, 10, utf8_decode('Bairro:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(144);
    $pdf->Cell(180, 10, $mae_bairro, 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('RG:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(13);
    $pdf->Cell(135, 10, $pai_rg, 0, '', 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(90);
    $pdf->Cell(180, 10, utf8_decode('CPF:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(100);
    $pdf->Cell(180, 10, $pai_cpf, 0, '', 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetX(160);
    $pdf->Cell(180, 10, utf8_decode('Celular:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(174);
    $pdf->Cell(180, 10, $pai_celular, 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Ocupação:'), 0, '', 'L');
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($pai_ocupacao), 0, 'J');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Situação do pai da criança:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(54);
    $pdf->Cell(180, 10, utf8_decode(situacao_pai($pai_situacao)), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("PARENTE(S) OU OUTRO(S)"), 0, '', 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
    $pdf->SetXY(35, $pdf->GetY() - 10);
    $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
    $pdf->SetXY(70, $pdf->GetY() - 10);
    $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
    $pdf->SetXY(95, $pdf->GetY() - 10);
    $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
    $pdf->SetXY(120, $pdf->GetY() - 10);
    $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
    $pdf->SetXY(160, $pdf->GetY() - 10);
    $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 8);
    $pdf->SetTextColor(0, 0, 0);

    $guard_a = 0;
    $guard_b = 0;
    $maior = 0;
    $posicao_i = 0;

    $result999 = $db->prepare("SELECT *           
                              FROM mod_acolhimento_crianca_parentes macp   
                              WHERE macp.familia_id = ? AND tipo = 1");
    $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
    $result999->execute();
    while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

        $guard_a = $pdf->GetY();
        $posicao_i = $pdf->GetY();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
        $guard_b = $pdf->GetY();

        $maior = $guard_b - $guard_a;

        $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
        $guard_a = $pdf->GetY();
        $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
        $guard_b = $pdf->GetY();

        if ($maior < ($guard_b - $guard_a)) {
            $maior = $guard_b - $guard_a;
        }

        $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
        $guard_a = $pdf->GetY();
        $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
        $guard_b = $pdf->GetY();

        if ($maior < ($guard_b - $guard_a)) {
            $maior = $guard_b - $guard_a;
        }

        $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
        $guard_a = $pdf->GetY();
        $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
        $guard_b = $pdf->GetY();

        if ($maior < ($guard_b - $guard_a)) {
            $maior = $guard_b - $guard_a;
        }

        $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
        $guard_a = $pdf->GetY();
        $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
        $guard_b = $pdf->GetY();

        if ($maior < ($guard_b - $guard_a)) {
            $maior = $guard_b - $guard_a;
        }

        $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
        $guard_a = $pdf->GetY();
        $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
        $guard_b = $pdf->GetY();

        if ($maior < ($guard_b - $guard_a)) {
            $maior = $guard_b - $guard_a;
        }

        $pdf->SetXY(5, $posicao_i);
        $pdf->MultiCell(30, $maior, "", 1, 'C');

        $pdf->SetXY(35, $pdf->GetY() - $maior);
        $pdf->MultiCell(35, $maior, "", 1, 'C');

        $pdf->SetXY(70, $pdf->GetY() - $maior);
        $pdf->MultiCell(25, $maior, "", 1, 'C');

        $pdf->SetXY(95, $pdf->GetY() - $maior);
        $pdf->MultiCell(25, $maior, "", 1, 'C');

        $pdf->SetXY(120, $pdf->GetY() - $maior);
        $pdf->MultiCell(40, $maior, "", 1, 'C');

        $pdf->SetXY(160, $pdf->GetY() - $maior);
        $pdf->MultiCell(45, $maior, "", 1, 'C');

        if ($pdf->GetY() > 230) {
            quebra($pdf);

            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->Ln();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
            $pdf->SetXY(35, $pdf->GetY() - 10);
            $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
            $pdf->SetXY(70, $pdf->GetY() - 10);
            $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
            $pdf->SetXY(95, $pdf->GetY() - 10);
            $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
            $pdf->SetXY(120, $pdf->GetY() - 10);
            $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
            $pdf->SetXY(160, $pdf->GetY() - 10);
            $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

            $pdf->SetFont('Arial', '', 8);
            $pdf->SetTextColor(0, 0, 0);
        }
    }

//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("INFORMAÇÃO SOBRE A SITUAÇÃO FAMILIAR DA CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Situação familiar da criança:'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(54);
    $pdf->Cell(180, 10, utf8_decode(situacao_familiar($situacao_familiar)), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os familiares demostram interesse no retorno da criança ao convívio familiar?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(139);
    $pdf->Cell(180, 10, $demonstra_interesse == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($demonstra_interesse == 1) {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Descreva:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($descricao_familia), 0, 'J');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
    }
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança demonstra interesse em retornar ao convívio familiar?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(115);
    $pdf->Cell(180, 10, $crianca_demonstra_interesse == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($crianca_demonstra_interesse == 1) {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Descreva:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($crianca_demonstra_descricao), 0, 'J');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
    }
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Existe alguma medida de restrição de contato à criança/proibição de visita?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(135);
    $pdf->Cell(180, 10, $medida_restricao == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($medida_restricao == 1) {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Quem estabeleceu o impedimento:'), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(66);
        $pdf->Cell(180, 10, utf8_decode($quem_estabelelceu), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUEM ESTÁ IMPEDIDO"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                   FROM mod_acolhimento_crianca_parentes macp   
                                   WHERE macp.familia_id = ? AND tipo = 2");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança recebe visita?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(47);
    $pdf->Cell(180, 10, $recebe_visita == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($recebe_visita == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 3");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Frequência desses contatos/encontros:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($frequencia_encontros), 0, 'J');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------   
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Como são realizados e como é a interação entre o(s) membros(s) da família e a criança durante esses contatos:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($como_realizados), 0, 'J');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Hã indícios de que os responsáveis estejam envolvidos com uso abusivo de álcool?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(150);
    $pdf->Cell(180, 10, $pais_envolvidos_alcool == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pais_envolvidos_alcool == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 4");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Hã indícios de que os reponsáveis estejam envolvidos com uso abusivo de outra(s) droga(s)?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(165);
    $pdf->Cell(180, 10, $pais_envolvidos_outros == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pais_envolvidos_outros == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 5");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Hã indícios de que os reponsáveis estejam envolvidos com uso abusivo sexual?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(143);
    $pdf->Cell(180, 10, $pais_envolvidos_abuso == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pais_envolvidos_abuso == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 6");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os responsáveis ou familiares estão em situação de rua?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(105);
    $pdf->Cell(180, 10, $situacao_rua == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($situacao_rua == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 7");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os responsáveis ou familiares em cumprimento de pena no sistema prisional?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(165);
    $pdf->Cell(180, 10, $pais_pena == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pais_pena == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 8");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os responsáveis ou familiares em processo de reabilitação?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(165);
    $pdf->Cell(180, 10, $pais_reabilitacao == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pais_reabilitacao == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 9");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os responsáveis ou familiares em situação de ameaça de morte?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(165);
    $pdf->Cell(180, 10, $pais_ameacados == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pais_ameacados == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 10");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os responsáveis ou familiares com doença grave/degenerativa?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(165);
    $pdf->Cell(180, 10, $pais_doencas == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pais_doencas == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 11");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os responsáveis ou familiares com transtorno mental?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(100);
    $pdf->Cell(180, 10, $pais_doencas == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($pais_doencas == 1) {
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Qual Transtorno:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($qual_transtorno), 0, 'J');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(30, 10, utf8_decode("Parentesco"), 1, 'C', 1);
        $pdf->SetXY(35, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(70, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(95, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(120, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
        $pdf->SetXY(160, $pdf->GetY() - 10);
        $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT *           
                                       FROM mod_acolhimento_crianca_parentes macp   
                                       WHERE macp.familia_id = ? AND tipo = 12");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($parent = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(30, 5, utf8_decode(grau_parentesco($parent['grau'])), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(35, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($parent['nome']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(70, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($parent['endereco']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(95, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, $parent['celular'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(120, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode($parent['cpf'] . ' - ' . $parent['rg']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(160, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(45, 5, utf8_decode($parent['ocupacao']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(35, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(70, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(95, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(120, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(160, $pdf->GetY() - $maior);
            $pdf->MultiCell(45, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(30, 10, utf8_decode("Vínculo"), 1, 'C', 1);
                $pdf->SetXY(35, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(70, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(95, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(120, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Documentos"), 1, 'C', 1);
                $pdf->SetXY(160, $pdf->GetY() - 10);
                $pdf->MultiCell(45, 10, utf8_decode("Ocupação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('INFORMAÇÃO SOBRE OS IRMÃO DA CRIANÇA'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança possui irmãos neste serviço de acolhimento?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(100);
    $pdf->Cell(180, 10, $possui_irmaos == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($possui_irmaos == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(70, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(75, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Idade"), 1, 'C', 1);
        $pdf->SetXY(110, $pdf->GetY() - 10);
        $pdf->MultiCell(30, 10, utf8_decode("Acolhimento"), 1, 'C', 1);
        $pdf->SetXY(140, $pdf->GetY() - 10);
        $pdf->MultiCell(65, 10, utf8_decode("Observação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                                   FROM mod_acolhimento_crianca_irmaos maci  
                                   LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                                   WHERE maci.familia_id = ? AND maci.tipo = 1");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($irmaos = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(70, 5, utf8_decode($irmaos['irmao']), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(75, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, $irmaos['idade'] . " Anos", 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(110, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(30, 5, obterDataBRTimestamp($irmaos['data_acolhimento']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(140, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(65, 5, $irmaos['obs'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(70, $maior, "", 1, 'C');

            $pdf->SetXY(75, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(110, $pdf->GetY() - $maior);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(140, $pdf->GetY() - $maior);
            $pdf->MultiCell(65, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(70, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(75, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Idade"), 1, 'C', 1);
                $pdf->SetXY(110, $pdf->GetY() - 10);
                $pdf->MultiCell(30, 10, utf8_decode("Acolhimento"), 1, 'C', 1);
                $pdf->SetXY(140, $pdf->GetY() - 10);
                $pdf->MultiCell(65, 10, utf8_decode("Observação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança possui irmãos em outros serviços de acolhimento?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(111);
    $pdf->Cell(180, 10, $possui_irmaos_outro == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($possui_irmaos_outro == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(70, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(75, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Idade"), 1, 'C', 1);
        $pdf->SetXY(110, $pdf->GetY() - 10);
        $pdf->MultiCell(30, 10, utf8_decode("Acolhimento"), 1, 'C', 1);
        $pdf->SetXY(140, $pdf->GetY() - 10);
        $pdf->MultiCell(65, 10, utf8_decode("Observação"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                                   FROM mod_acolhimento_crianca_irmaos maci  
                                   LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                                   WHERE maci.familia_id = ? AND maci.tipo = 2");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($irmaos = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(70, 5, utf8_decode($irmaos['irmao']), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(75, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, $irmaos['idade'] . " Anos", 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(110, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(30, 5, obterDataBRTimestamp($irmaos['data_acolhimento']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(140, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(65, 5, $irmaos['obs'], 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(70, $maior, "", 1, 'C');

            $pdf->SetXY(75, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(110, $pdf->GetY() - $maior);
            $pdf->MultiCell(30, $maior, "", 1, 'C');

            $pdf->SetXY(140, $pdf->GetY() - $maior);
            $pdf->MultiCell(65, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(70, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(75, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Idade"), 1, 'C', 1);
                $pdf->SetXY(110, $pdf->GetY() - 10);
                $pdf->MultiCell(30, 10, utf8_decode("Acolhimento"), 1, 'C', 1);
                $pdf->SetXY(140, $pdf->GetY() - 10);
                $pdf->MultiCell(65, 10, utf8_decode("Observação"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança conhece ou tem vínculo com o(s) irmão(s) em outro(s) serviço(s) de acolhimento?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(161);
    $pdf->Cell(180, 10, $conhece_irmaos == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($conhece_irmaos == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(165, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(170, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Idade"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                                   FROM mod_acolhimento_crianca_irmaos maci  
                                   LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                                   WHERE maci.familia_id = ? AND maci.tipo = 3");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($irmaos = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(165, 5, utf8_decode($irmaos['irmao']), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(170, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, $irmaos['idade'] . " Anos", 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(165, $maior, "", 1, 'C');

            $pdf->SetXY(170, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(165, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(170, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Idade"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('A criança possui irmão(s) que foi(ram) adotado(s)?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(94);
    $pdf->Cell(180, 10, $irmaos_adotados == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($irmaos_adotados == 1) {
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode("QUAL(IS) MEMBRO(S) DA FAMÍLIA VISITA(M) / ENTRA(M) EM CONTATO COM A CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(165, 10, utf8_decode("Nome"), 1, 'C', 1);
        $pdf->SetXY(170, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Idade"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result999 = $db->prepare("SELECT maci.id, maci.idade, maci.data_acolhimento, maci.obs, maci.nome AS irmao_id, macp.nome AS irmao            
                                   FROM mod_acolhimento_crianca_irmaos maci  
                                   LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = maci.nome 
                                   WHERE maci.familia_id = ? AND maci.tipo = 4");
        $result999->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result999->execute();
        while ($irmaos = $result999->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(165, 5, utf8_decode($irmaos['irmao']), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(170, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, $irmaos['idade'] . " Anos", 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(165, $maior, "", 1, 'C');

            $pdf->SetXY(170, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(165, 10, utf8_decode("Nome"), 1, 'C', 1);
                $pdf->SetXY(170, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Idade"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("INFORMAÇÃO SOBRE O RESPONSÁVEL PELA CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Pessoa(s) responsáveis:'), 0, '', 'L');

    $responsaveis = "";

    $result = $db->prepare("SELECT *    
                            FROM mod_acolhimento_crianca_familia_responsaveis macfr
                            LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = macfr.responsavel_id  
                            WHERE macp.familia_id = ? AND macp.tipo = 1");
    $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
    $result->execute();
    while ($resp = $result->fetch(PDO::FETCH_ASSOC)) {
        $responsaveis .= "" . $resp['nome'] . "; ";
    }


    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($responsaveis), 0, 'J');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os responsáveis oferecem condições necessárias para o retorno da criança?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(136);
    $pdf->Cell(180, 10, $oferecem_condicoes == 1 ? "Sim" : utf8_decode($oferecem_condicoes == 2 ? "Faz-se necessária uma avaliação profunda" : "Não"), 0, '', 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------     
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Descrição:'), 0, '', 'L');
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($avaliacao_profunda), 0, 'J');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("INFORMAÇÃO SOBRE O RETORNO AO CONVÍVIO DA CRIANÇA"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Familiares com os quais a criança convive ou mantem vínculo de afinidade / afetividade:'), 0, '', 'L');

    $responsaveis2 = "";

    $result = $db->prepare("SELECT *    
                            FROM mod_acolhimento_crianca_familia_convive macfr
                            LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = macfr.responsavel_id  
                            WHERE macp.familia_id = ? AND macp.tipo = 1");
    $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
    $result->execute();
    while ($resp2 = $result->fetch(PDO::FETCH_ASSOC)) {
        $responsaveis2 .= "" . $resp2['nome'] . "; ";
    }

    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($responsaveis2), 0, 'J');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Caso haja familiares, há interessado(s) em acolher a criança?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(111);
    $pdf->Cell(180, 10, $familiar_interesse == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    if ($familiar_interesse == 1) {

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Quem:'), 0, '', 'L');

        $responsaveis3 = "";

        $result = $db->prepare("SELECT *    
                            FROM mod_acolhimento_crianca_familia_convive macfr
                            LEFT JOIN mod_acolhimento_crianca_parentes AS macp ON macp.id = macfr.responsavel_id  
                            WHERE macp.familia_id = ? AND macp.tipo = 1");
        $result->bindValue(1, $mod_acolhimento_crianca_familia_id);
        $result->execute();
        while ($resp3 = $result->fetch(PDO::FETCH_ASSOC)) {
            $responsaveis3 .= "" . $resp3['nome'] . "; ";
        }

        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($responsaveis3), 0, 'J');
    }

//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Os responsáveis oferecem condições necessárias para o retorno da criança?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(136);
    $pdf->Cell(180, 10, $familiar_condicao == 1 ? "Sim" : utf8_decode($familiar_condicao == 3 ? "Faz-se necessária uma avaliação profunda" : "Não"), 0, '', 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------     
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Descrição:'), 0, '', 'L');
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($descricao_condicao_cuidado), 0, 'J');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------     
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Opinião da criança quanto à possibilidade de ficar sob os cuidados desse(s) parente(s):'), 0, '', 'L');
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($opniao_crianca), 0, 'J');
//------------------------------------------------------------------------------         
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
}
//------------------------------------------------------------------------------  
if ($pdf->GetY() > 230) {
    quebra($pdf);
}
//------------------------------------------------------------------------------  
if (carregar_info_menu($acolhimento_id, 6)) {//Rede
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);

    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(200, 10, utf8_decode('REDE'), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetTextColor(0, 0, 0);
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode("ACOMPANHAMENTO DA REDE"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Há instituições/serviços/programas que estiveram acompanhando os responsáveis e/ou a criança?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(175);
    $pdf->Cell(180, 10, $acompanhamento == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');

    if ($acompanhamento == 1) {
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(35, 10, utf8_decode("Instituição"), 1, 'C', 1);
        $pdf->SetXY(40, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(75, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(100, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Familiar"), 1, 'C', 1);
        $pdf->SetXY(140, $pdf->GetY() - 10);
        $pdf->MultiCell(65, 10, utf8_decode("Metodologia de Acompanhamento"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $maior = 0;
        $posicao_i = 0;

        $result99 = $db->prepare("SELECT *           
                                  FROM mod_acolhimento_crianca_rede_membros macrm   
                                  WHERE macrm.acolhimento_crianca_rede_id = ? OR macrm.acolhimento_crianca_rede_id = 0 AND macrm.responsavel_id = ?");
        $result99->bindValue(1, $rede_id);
        $result99->bindValue(2, $_SESSION['id']);
        $result99->execute();
        while ($membros = $result99->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(35, 5, utf8_decode($membros['instituicao']), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(40, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($membros['endereco'] . ", " . $membros['numero'] . " - " . $membros['bairro']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(75, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($membros['contato']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(100, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode(pesquisar("nome", "seg_profissional_instituicao", "id", "=", $membros['nome'], "")), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(140, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(65, 5, utf8_decode($membros['referencia']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(40, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(75, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(100, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(140, $pdf->GetY() - $maior);
            $pdf->MultiCell(65, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(35, 10, utf8_decode("Instituição"), 1, 'C', 1);
                $pdf->SetXY(40, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(75, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(100, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Familiar"), 1, 'C', 1);
                $pdf->SetXY(140, $pdf->GetY() - 10);
                $pdf->MultiCell(65, 10, utf8_decode("Metodologia de Acompanhamento"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
    }
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 200) {
        //Cabeçalho
        $pdf->SetMargins(3, 3, 2);
        $pdf->SetY("-1");
        $pdf->MultiCell(160, 5, ' ', '0', 'C', 0);
        $pdf->SetFont('arial', 'B', 10);
        $pdf->SetTextColor(000);

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTitle('PLANO INDIVIDUAL DE ATENDIMENTO - PIA');

        $pdf->SetXY(15, 5);
        $pdf->Cell(180, 10, utf8_decode('PLANO INDIVIDUAL DE ATENDIMENTO - PIA'), 0, '', 'C');

        $pdf->SetFont('Arial', '', 10);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Há instituições/serviços/programas exeternos à instituição de acolhimento que estão atuando no caso?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(182);
    $pdf->Cell(180, 10, $acompanhamento2 == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');

    if ($acompanhamento2 == 1) {
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->MultiCell(35, 10, utf8_decode("Instituição"), 1, 'C', 1);
        $pdf->SetXY(40, $pdf->GetY() - 10);
        $pdf->MultiCell(35, 10, utf8_decode("Endereço"), 1, 'C', 1);
        $pdf->SetXY(75, $pdf->GetY() - 10);
        $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
        $pdf->SetXY(100, $pdf->GetY() - 10);
        $pdf->MultiCell(40, 10, utf8_decode("Familiar"), 1, 'C', 1);
        $pdf->SetXY(140, $pdf->GetY() - 10);
        $pdf->MultiCell(65, 10, utf8_decode("Metodologia de Acompanhamento"), 1, 'C', 1);

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetTextColor(0, 0, 0);

        $guard_a = 0;
        $guard_b = 0;
        $posicao_i = 0;

        $result99 = $db->prepare("SELECT *           
                                  FROM mod_acolhimento_crianca_rede_membros2 macrm   
                                  WHERE macrm.acolhimento_crianca_rede_id = ?");
        $result99->bindValue(1, $rede_id);
        $result99->execute();
        while ($membros = $result99->fetch(PDO::FETCH_ASSOC)) {

            $guard_a = $pdf->GetY();
            $posicao_i = $pdf->GetY();
            $pdf->SetX(5);
            $pdf->MultiCell(35, 5, utf8_decode($membros['instituicao']), 0, 'C');
            $guard_b = $pdf->GetY();

            $maior = $guard_b - $guard_a;

            $pdf->SetXY(40, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(35, 5, utf8_decode($membros['endereco'] . ", " . $membros['numero'] . " - " . $membros['bairro']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(75, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(25, 5, utf8_decode($membros['contato']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(100, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(40, 5, utf8_decode(pesquisar("nome", "seg_profissional_instituicao", "id", "=", $membros['nome'], "")), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(140, $pdf->GetY() - ($guard_b - $guard_a));
            $guard_a = $pdf->GetY();
            $pdf->MultiCell(65, 5, utf8_decode($membros['referencia']), 0, 'C');
            $guard_b = $pdf->GetY();

            if ($maior < ($guard_b - $guard_a)) {
                $maior = $guard_b - $guard_a;
            }

            $pdf->SetXY(5, $posicao_i);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(40, $pdf->GetY() - $maior);
            $pdf->MultiCell(35, $maior, "", 1, 'C');

            $pdf->SetXY(75, $pdf->GetY() - $maior);
            $pdf->MultiCell(25, $maior, "", 1, 'C');

            $pdf->SetXY(100, $pdf->GetY() - $maior);
            $pdf->MultiCell(40, $maior, "", 1, 'C');

            $pdf->SetXY(140, $pdf->GetY() - $maior);
            $pdf->MultiCell(65, $maior, "", 1, 'C');

            if ($pdf->GetY() > 230) {
                quebra($pdf);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Ln();
                $pdf->SetX(5);
                $pdf->MultiCell(35, 10, utf8_decode("Instituição"), 1, 'C', 1);
                $pdf->SetXY(40, $pdf->GetY() - 10);
                $pdf->MultiCell(35, 10, utf8_decode("Endereço"), 1, 'C', 1);
                $pdf->SetXY(75, $pdf->GetY() - 10);
                $pdf->MultiCell(25, 10, utf8_decode("Telefone"), 1, 'C', 1);
                $pdf->SetXY(100, $pdf->GetY() - 10);
                $pdf->MultiCell(40, 10, utf8_decode("Familiar"), 1, 'C', 1);
                $pdf->SetXY(140, $pdf->GetY() - 10);
                $pdf->MultiCell(65, 10, utf8_decode("Metodologia de Acompanhamento"), 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->SetTextColor(0, 0, 0);
            }
        }
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------    
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
    }

//------------------------------------------------------------------------------  
}

if ($pdf->GetY() > 230) {
    quebra($pdf);
}
//------------------------------------------------------------------------------  
if (carregar_info_menu($acolhimento_id, 7)) {//Plano de Ação
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);

    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(200, 10, utf8_decode('PLANO DE AÇÃO - PARA A FAMÍLIA'), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetTextColor(0, 0, 0);
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('DOCUMENTAÇÃO'), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 1");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('SAÚDE'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 2");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 3");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 4");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('CAPACITAÇÃO PROFISSIONAL/INSERÇÃO NO MUNDO DO TRABALHO'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 5");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('CUIDADO COM A CRIANÇA'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 6");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('OUTRO'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1 AND macp.subtipo = 7");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(200, 10, utf8_decode('PARA A CRIANÇA'), 0, '', 'C');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('DOCUMENTAÇÃO'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 1");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('SAÚDE'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 2");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('EDUCAÇÃO'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 8");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('ESPORTE/CULTURA/LAZER'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 9");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('CONVIVÊNCIA FAMILIAR'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 10");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('ACESSO A BENEFÍCIOS E INCLUSÃO EM PROGRAMAS DE TRANSFERÊNCIA DE RENDA'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 3");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('PARTICIPAÇÃO EM SERVIÇOS, PROGRAMAS E PROJETOS'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 4");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('OUTROS'), 0, '', 'L');
//------------------------------------------------------------------------------   
    $result = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 2 AND macp.subtipo = 7");
    $result->bindValue(1, $crianca_id);
    $result->execute();
    while ($documentos = $result->fetch(PDO::FETCH_ASSOC)) {
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Objetivo:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['objetivo']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Ação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['acao']), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------ 
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Responsável: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(30);
        $pdf->Cell(180, 10, utf8_decode($documentos['responsavel']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(115);
        $pdf->Cell(180, 10, utf8_decode('Prazo Inicial: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(138);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_inicial']), 0, '', 'L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetX(163);
        $pdf->Cell(180, 10, utf8_decode('Prazo Final: '), 0, '', 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(185);
        $pdf->Cell(180, 10, obterDataBRTimestamp($documentos['prazo_final']), 0, '', 'L');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Monitoramento:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['realizada'] == 1 ? "Ação realizada" : ($documentos['alcancado'] == 1 ? "Objetivo alcançado" : ($documentos['finalizada'] == 1 ? "Pode ser finalizada" : "Redefinir prazo"))), 0, 'J');
//------------------------------------------------------------------------------    
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------       
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------     
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Observação(es):'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($documentos['obs']), 0, 'J');
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
}
//------------------------------------------------------------------------------  
if ($pdf->GetY() > 230) {
    quebra($pdf);
}
//------------------------------------------------------------------------------  
if (carregar_info_menu($acolhimento_id, 8)) {//Avaliação
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetTextColor(255, 255, 255);

    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->MultiCell(200, 10, utf8_decode('AVALIAÇÃO'), 1, 'C', 1);

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetTextColor(0, 0, 0);
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------ 
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Contextualização do Caso:'), 0, '', 'L');
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($contextualiziacao), 0, 'J');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Interesse manifesto e os compromissos dos membros da família para assumir/reassumir a guarda da criança:'), 0, '', 'L');
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(5);
    $pdf->MultiCell(200, 5, utf8_decode($interesse_avaliacao), 0, 'J');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------  
    if ($pdf->GetY() > 230) {
        quebra($pdf);
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Após intervenções da equipe, ocorreram modificações que resultaram na superação da causa do acolhimento?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(195);
    $pdf->Cell(180, 10, $intervencoes == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($intervencoes == 1) {
//------------------------------------------------------------------------------  
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Quais Modificações:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($modificacoes), 0, 'J');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
    }
//------------------------------------------------------------------------------  
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(180, 10, utf8_decode('Há indicativos da possibilidade imediata ou em curto/médio prazo da reinserção familiar da criança?'), 0, '', 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetX(177);
    $pdf->Cell(180, 10, $indicativos == 1 ? "Sim" : utf8_decode("Não"), 0, '', 'L');
//------------------------------------------------------------------------------    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Ln();
    $pdf->SetX(5);
    $pdf->Cell(198, 0, "", 1, 1, 'L');
//------------------------------------------------------------------------------
    if ($indicativos == 1) {
//------------------------------------------------------------------------------  
        if ($pdf->GetY() > 230) {
            quebra($pdf);
        }
//------------------------------------------------------------------------------  
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(180, 10, utf8_decode('Justificação:'), 0, '', 'L');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetX(5);
        $pdf->MultiCell(200, 5, utf8_decode($justificacao), 0, 'J');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Ln();
        $pdf->SetX(5);
        $pdf->Cell(198, 0, "", 1, 1, 'L');
    }
//------------------------------------------------------------------------------    
}


$arquivo = "relatorio.pdf";

//Gera o arquivo PDF a ser baixado
$pdf->Output($arquivo, "I");

$pdf->Output();
?>