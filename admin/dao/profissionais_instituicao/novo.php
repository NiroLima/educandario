<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$nome = isset($_POST['nome']) ? $_POST['nome'] : NULL;

$email = isset($_POST['email']) ? $_POST['email'] : NULL;
$celular = isset($_POST['celular']) ? $_POST['celular'] : NULL;
$fixo = isset($_POST['fixo']) ? $_POST['fixo'] : NULL;
$funcao = isset($_POST['funcao']) ? $_POST['funcao'] : NULL;
$instituicao = isset($_POST['instituicao']) ? $_POST['instituicao'] : NULL;
$registro = isset($_POST['registro']) ? $_POST['registro'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "seg_profissional_instituicao", "nome", "=", $nome, "");

if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome do profissional informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO seg_profissional_instituicao (nome, email, celular, contato, funcao_id, registro, data_update, usuario_id, instituicao_id, data_cadastro, status) VALUES (?, ?, ?, ?, ?, ?, NOW(), ?, ?, NOW(), 1)");
            $sql->bindValue(1, $nome);
            $sql->bindValue(2, $email);
            $sql->bindValue(3, $celular);
            $sql->bindValue(4, $fixo);
            $sql->bindValue(5, $funcao);
            $sql->bindValue(6, $registro);
            $sql->bindValue(7, $_SESSION['id']);
            $sql->bindValue(8, $instituicao);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE seg_profissional_instituicao SET nome = ?, email = ?, celular = ?, contato = ?, funcao_id = ?, registro = ?, usuario_id = ?, instituicao_id = ? WHERE id = ?");
            $sql->bindValue(1, $nome);
            $sql->bindValue(2, $email);
            $sql->bindValue(3, $celular);
            $sql->bindValue(4, $fixo);
            $sql->bindValue(5, $funcao);
            $sql->bindValue(6, $registro);
            $sql->bindValue(7, $_SESSION['id']);
            $sql->bindValue(8, $instituicao);
            $sql->bindValue(9, $id);
            $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>