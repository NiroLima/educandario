//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("button#atualizar").click(function () {
        $("form#form_rede").submit();
    });

    $("#sim_acolhimento_institucional").click(function () {
        $("#div_membros").show();
    });

    $("#nao_acolhimento_institucional").click(function () {
        $("#div_membros").hide();
    });


    $("#sim_acolhimento_institucional2").click(function () {
        $("#div_membros2").show();
    });

    $("#nao_acolhimento_institucional2").click(function () {
        $("#div_membros2").hide();
    });


    $("select#nome_irmao").change(function () {
        var funcao = $(this).find(":selected").attr('funcao');

        $("#funcao_profissional_rede").val(funcao);
    });

    $("select#nome_irmao2").change(function () {
        var funcao = $(this).find(":selected").attr('funcao');

        $("#funcao_profissional_rede_2").val(funcao);
    });

    $('#form_responsavel').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var responsavel_id = $("#responsavel_id").val();

        $.post(PORTAL_URL + "admin/dao/pia/acolhido_responsavel.php", {id: id, responsavel_id: responsavel_id}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário PIA",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário PIA",
                    text: "Informações do acolhido atualizadas com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/rede/' + id);
                });
            }
        }
        , "html");
        return false;
    });

    $("#inserir_membro").click(function () {

        var id = $("#rede_id").val();
        var instituicao = $("#instituicao").val();
        var contato = $("#contato").val();
        var endereco = $("#endereco").val();
        var numero = $("#numero").val();
        var bairro = $("#bairro").val();
        var nome_irmao = $("#nome_irmao").val();
        var tipo_acompanhamento = $("#tipo_acompanhamento").val();
        var frequencia_visita = $("#frequencia_visita").val();
        var referencia_contato = $("#referencia_contato").val();

        var membro_id = $("#membro_id").val();

        if (formulario_validator("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_membro.php", {id: id, instituicao: instituicao, contato: contato, endereco: endereco, numero: numero, membro_id: membro_id,
                bairro: bairro, nome_irmao: nome_irmao, tipo_acompanhamento: tipo_acompanhamento, frequencia_visita: frequencia_visita, referencia_contato: referencia_contato}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Membros",
                        html: "O membro escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Membros",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_membros.php",
                                {id: id},
                                function (valor) {
                                    $("input#membro_id").val("");
                                    $("button#inserir_membro").html("INSERIR");
                                    $("#instituicao").val("");
                                    $("#contato").val("");
                                    $("#endereco").val("");
                                    $("#numero").val("");
                                    $("#bairro").val("");
                                    $("#nome_irmao").val("");
                                    $("#nome_irmao").select2();
                                    $("#tipo_acompanhamento").val("");
                                    $("#tipo_acompanhamento").select2();
                                    $("#frequencia_visita").val("");
                                    $("#referencia_contato").val("");
                                    $("#funcao_profissional_rede").val("");
                                    $("tbody#resultado_membro").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_membro2").click(function () {

        var id = $("#rede_id").val();
        var instituicao = $("#instituicao2").val();
        var contato = $("#contato2").val();
        var endereco = $("#endereco2").val();
        var numero = $("#numero2").val();
        var bairro = $("#bairro2").val();
        var nome_irmao = $("#nome_irmao2").val();
        var tipo_acompanhamento = $("#tipo_acompanhamento2").val();
        var frequencia_visita = $("#frequencia_visita2").val();
        var referencia_contato = $("#referencia_contato2").val();

        var membro_id = $("#membro_id2").val();

        if (formulario_validator2("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_membro2.php", {id: id, instituicao: instituicao, contato: contato, endereco: endereco, numero: numero, membro_id: membro_id,
                bairro: bairro, nome_irmao: nome_irmao, tipo_acompanhamento: tipo_acompanhamento, frequencia_visita: frequencia_visita, referencia_contato: referencia_contato}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Membros",
                        html: "O membro escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Membros",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_membros2.php",
                                {id: id},
                                function (valor) {
                                    $("input#membro_id2").val("");
                                    $("button#inserir_membro2").html("INSERIR");
                                    $("#instituicao2").val("");
                                    $("#contato2").val("");
                                    $("#endereco2").val("");
                                    $("#numero2").val("");
                                    $("#bairro2").val("");
                                    $("#nome_irmao2").val("");
                                    $("#nome_irmao2").select2();
                                    $("#tipo_acompanhamento2").val("");
                                    $("#tipo_acompanhamento2").select2();
                                    $("#frequencia_visita2").val("");
                                    $("#referencia_contato2").val("");
                                    $("#funcao_profissional_rede_2").val("");
                                    $("tbody#resultado_membro2").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });
//------------------------------------------------------------------------------
    $('#form_rede').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var rede_id = $("#rede_id").val();
        var sim_acolhimento_institucional = $("#sim_acolhimento_institucional:checked").val() == 1 ? 1 : 0;
        var sim_acolhimento_institucional2 = $("#sim_acolhimento_institucional2:checked").val() == 1 ? 1 : 0;

        $.post(PORTAL_URL + "admin/dao/pia/rede.php", {id: id, rede_id: rede_id, sim_acolhimento_institucional: sim_acolhimento_institucional, sim_acolhimento_institucional2: sim_acolhimento_institucional2}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário de Rede",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário de Rede",
                    text: "Ação realizada com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/rede/' + id);
                });
            }
        }
        , "html");
        return false;

    });

});
//------------------------------------------------------------------------------------------------------
function editar_responsavel() {
    $("div#responsavel_edite_1").hide();
    $("div#responsavel_edite_2").show();
}
//------------------------------------------------------------------------------
function editar(id, instituicao, endereco, numero, bairro, contato, nome, referencia, funcao) {
    $("input#membro_id").val(id);
    $("input#instituicao").val(instituicao);
    $("input#contato").val(contato);
    $("input#endereco").val(endereco);
    $("input#numero").val(numero);
    $("input#bairro").val(bairro);
    $("select#nome_irmao").val(nome);
    $("select#nome_irmao").select2();
    $("select#tipo_acompanhamento").select2();
    $("textarea#referencia_contato").val(referencia);
    $("input#funcao_profissional_rede").val(funcao);
    $("button#inserir_membro").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar2(id, instituicao, endereco, numero, bairro, contato, nome, referencia, funcao) {
    $("input#membro_id2").val(id);
    $("input#instituicao2").val(instituicao);
    $("input#contato2").val(contato);
    $("input#endereco2").val(endereco);
    $("input#numero2").val(numero);
    $("input#bairro2").val(bairro);
    $("select#nome_irmao2").val(nome);
    $("select#nome_irmao2").select2();
    $("select#tipo_acompanhamento2").select2();
    $("textarea#referencia_contato2").val(referencia);
    $("input#funcao_profissional_rede_2").val(funcao);
    $("button#inserir_membro2").html("ALTERAR");
}
//------------------------------------------------------------------------------
function remover(obj, id) {
    swal({
        title: "Deseja mesmo remover este membro?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do membro!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_membro",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend3(obj),
            error: onSuccessSend3(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend3(obj) {
    swal({
        title: "Sucesso!",
        text: "Membro removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#membro_id").val("");
        $("button#inserir_membro").html("INSERIR");
        $("#instituicao").val("");
        $("#contato").val("");
        $("#endereco").val("");
        $("#numero").val("");
        $("#bairro").val("");
        $("#nome_irmao").val("");
        $("#tipo_acompanhamento").val("");
        $("#frequencia_visita").val("");
        $("#referencia_contato").val("");
        $(obj).parents('tr#remover_membro').remove();
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var instituicao = $("#instituicao").val();
    var contato = $("#contato").val();
    var endereco = $("#endereco").val();
    var numero = $("#numero").val();
    var bairro = $("#bairro").val();
    var nome_irmao = $("#nome_irmao").val();
    var tipo_acompanhamento = $("#tipo_acompanhamento").val();
    var referencia_contato = $("#referencia_contato").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (referencia_contato == "") {
            $('div#div_referencia_contato').after('<label id="erro_referencia_contato" class="error">Metodologia é obrigatório.</label>');
            valido = false;
            element = $('div#div_referencia_contato');
        }

        if (tipo_acompanhamento == "") {
            $('div#div_tipo_acompanhamento').after('<label id="erro_tipo_acompanhamento" class="error">Tipo é obrigatório.</label>');
            valido = false;
            element = $('div#div_tipo_acompanhamento');
        }

        if (nome_irmao == "") {
            $('div#div_nome_irmao').after('<label id="erro_nome_irmao" class="error">Membro é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_irmao');
        }

        if (bairro == "") {
            $('div#div_bairro').after('<label id="erro_bairro" class="error">Bairro é obrigatório.</label>');
            valido = false;
            element = $('div#div_bairro');
        }

        if (numero == "") {
            $('div#div_numero').after('<label id="erro_numero" class="error">Número é obrigatório.</label>');
            valido = false;
            element = $('div#div_numero');
        }

        if (endereco == "") {
            $('div#div_endereco').after('<label id="erro_endereco" class="error">Endereço é obrigatório.</label>');
            valido = false;
            element = $('div#div_endereco');
        }

        if (contato == "") {
            $('div#div_contato').after('<label id="erro_contato" class="error">Contato é obrigatório.</label>');
            valido = false;
            element = $('div#div_contato');
        }

        if (instituicao == "") {
            $('div#div_instituicao').after('<label id="erro_instituicao" class="error">Instituição é obrigatório.</label>');
            valido = false;
            element = $('div#div_instituicao');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
function remover2(obj, id) {
    swal({
        title: "Deseja mesmo remover este membro?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do membro!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_membro2",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend2(obj),
            error: onSuccessSend2(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend2(obj) {
    swal({
        title: "Sucesso!",
        text: "Membro removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#membro_id2").val("");
        $("button#inserir_membro2").html("INSERIR");
        $("#instituicao2").val("");
        $("#contato2").val("");
        $("#endereco2").val("");
        $("#numero2").val("");
        $("#bairro2").val("");
        $("#nome_irmao2").val("");
        $("#tipo_acompanhamento2").val("");
        $("#frequencia_visita2").val("");
        $("#referencia_contato2").val("");
        $(obj).parents('tr#remover_membro2').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator2(obj) {
    var valido = true;

    var instituicao = $("#instituicao2").val();
    var contato = $("#contato2").val();
    var endereco = $("#endereco2").val();
    var numero = $("#numero2").val();
    var bairro = $("#bairro2").val();
    var nome_irmao = $("#nome_irmao2").val();
    var tipo_acompanhamento = $("#tipo_acompanhamento2").val();
    var referencia_contato = $("#referencia_contato2").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (referencia_contato == "") {
            $('div#div_referencia_contato2').after('<label id="erro_referencia_contato2" class="error">Metodologia é obrigatório.</label>');
            valido = false;
            element = $('div#div_referencia_contato2');
        }

        if (tipo_acompanhamento == "") {
            $('div#div_tipo_acompanhamento2').after('<label id="erro_tipo_acompanhamento2" class="error">Tipo é obrigatório.</label>');
            valido = false;
            element = $('div#div_tipo_acompanhamento2');
        }

        if (nome_irmao == "") {
            $('div#div_nome_irmao2').after('<label id="erro_nome_irmao2" class="error">Membro é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_irmao2');
        }

        if (bairro == "") {
            $('div#div_bairro2').after('<label id="erro_bairro2" class="error">Bairro é obrigatório.</label>');
            valido = false;
            element = $('div#div_bairro2');
        }

        if (numero == "") {
            $('div#div_numero2').after('<label id="erro_numero2" class="error">Número é obrigatório.</label>');
            valido = false;
            element = $('div#div_numero2');
        }

        if (endereco == "") {
            $('div#div_endereco2').after('<label id="erro_endereco2" class="error">Endereço é obrigatório.</label>');
            valido = false;
            element = $('div#div_endereco2');
        }

        if (contato == "") {
            $('div#div_contato2').after('<label id="erro_contato2" class="error">Contato é obrigatório.</label>');
            valido = false;
            element = $('div#div_contato2');
        }

        if (instituicao == "") {
            $('div#div_instituicao2').after('<label id="erro_instituicao2" class="error">Instituição é obrigatório.</label>');
            valido = false;
            element = $('div#div_instituicao2');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function validar(codigo) {
    swal({
        title: "Deseja mesmo confirmar a Etapa 6 Rede?",
        text: "Obs: Caso escolha confirmar, o sistema irá identificar essa etapa como concluída!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode confirmar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/confirmar_acolhido",
            type: "POST",
            data: {id: codigo, op: 6},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------------------------------
function desvalidar(codigo) {
    swal({
        title: "Deseja mesmo desvalidar a Etapa 6 Rede?",
        text: "Obs: Caso escolha desvalidar, o sistema irá identificar essa etapa como pendente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode desvalidar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/desvalidar_acolhido",
            type: "POST",
            data: {id: codigo, op: 6},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    var codigo = $("#id").val();

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/rede/' + codigo);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
function gerar_pdf() {
    //pega o Html da DIV
    var divElements = document.getElementById("impressao").innerHTML;
    //pega o HTML de toda tag Body
    var oldPage = document.body.innerHTML;
    //Alterna o body 
    document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
    //Imprime o body atual
    window.print();
    //Retorna o conteudo original da página. 
    document.body.innerHTML = oldPage;
}
//------------------------------------------------------------------------------