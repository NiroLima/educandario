//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('input#sim_residia').click(function () {
        $("div#div_rua").hide();
        $("div#div_rua_local").hide();
        $("div#div_rua_pessoas").hide();

        $("select#tempo_rua").val("");
        $("select#tempo_rua").select2();
        $("input#costumava_abrigar").val("");

        $("div#div_pessoas_vivia_rua").find("div.bootstrap-tagsinput").find("span.label-info").each(function () {
            $(this).remove();
        });
    });

    $('input#sim_acolhida_anterior').click(function () {
        $("div#div_servico_acolhimento").show();
    });

    $('input#nao_acolhida_anterior').click(function () {
        $("input#servico_acolhimento").val("");
        $("div#div_servico_acolhimento").hide();
    });

    $('input#nao_residia').click(function () {
        $("div#div_rua").show();
        $("div#div_rua_local").show();
        $("div#div_rua_pessoas").show();
    });

    $("button#atualizar").click(function () {
        $("form#form_acolhimento").submit();
    });

    $("#profissional").change(function () {
        var setor = $(this).find('option:selected').attr('rel');
        $("input#setor_profissional").val(setor);
    });

    $("#conselho_tutelar").click(function () {
        $("div#div_opcao_conselho").show();
        $("div#div_comarca_interior").show();
    });

    $("#juizado").click(function () {
        $("div#div_opcao_conselho").hide();
        $("div#div_comarca_interior").hide();
    });

    $("#policia_militar").click(function () {
        $("div#div_opcao_conselho").hide();
        $("div#div_comarca_interior").hide();
    });

    //COMBO ESTADO E MUNICIPIO
    $("select#estado").change(function () {
        $("select#cidade").html('<option value="0">Carregando...</option>');
        $.post(PORTAL_URL + "utils/combo_cidades.php",
                {estado: $(this).val()},
                function (valor) {
                    $("select#cidade").html(valor);
                    $("select#cidade").select2();
                });
    });

    $('#form_responsavel').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var responsavel_id = $("#responsavel_id").val();

        $.post(PORTAL_URL + "admin/dao/pia/acolhido_responsavel.php", {id: id, responsavel_id: responsavel_id}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário PIA",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário PIA",
                    text: "Informações do acolhido atualizadas com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/acolhimento/' + id);
                });
            }
        }
        , "html");
        return false;
    });

    $('#form_acolhimento').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var quem_trouxe = $('#juizado:checked').val() == 1 ? 1 : ($('#conselho_tutelar:checked').val() == 2 ? 2 : ($('#policia_militar:checked').val() == 3 ? 3 : 0));
            var opcao_conselho = $('#1conselho:checked').val() == 1 ? 1 : ($('#2conselho:checked').val() == 2 ? 2 : ($('#3conselho:checked').val() == 3 ? 3 : $('#conselhos_interior:checked').val() == 4 ? 4 : 0));
            var comarca_interior = quem_trouxe == 2 ? $("#comarca_interior").val() : "";
            var responsavel_entrega = $("#responsavel_entrega").val();
            var funcao_responsavel = $("#funcao_responsavel").val();
            var contato_responsavel = $("#contato_responsavel").val();
            var data_entrada = $("#data_entrada").val();
            var hora_entrada = $("#hora_entrada").val();
            var profissional = $("#profissional").val();
            var setor_profissional = $("#setor_profissional").val();
            var observacao_acolhimento = $("#observacao_acolhimento").val();
            var motivo_aba = $("#abandono:checked").val() == 1 ? 1 : 0;
            var motivo_neg = $("#negligencia:checked").val() == 1 ? 1 : 0;
            var motivo_abu = $("#abuso_sexual:checked").val() == 1 ? 1 : 0;
            var motivo_out = $("#outros:checked").val() == 1 ? 1 : 0;
            var descricao = $("#descricao").val();
            var numero_guia_acolhimento = $("#numero_guia_acolhimento").val();
            var numero_processo = $("#numero_processo").val();
            var numero_destituicao = $("#numero_destituicao").val();

            var sim_residia = $('#sim_residia:checked').val() == 1 ? 1 : 0;
            var tempo_rua = $("#tempo_rua").val();
            var costumava_abrigar = $("#costumava_abrigar").val();
            var pessoas_vivia_rua = sim_residia == 1 ? "" : $("#pessoas_vivia_rua").val();
            var parentes_vinculo = $("#parentes_vinculo").val();
            var sim_acolhida_anterior = $('#sim_acolhida_anterior:checked').val() == 1 ? 1 : 0;
            var servico_acolhimento = $("#servico_acolhimento").val();

            $.post(PORTAL_URL + "admin/dao/pia/acolhimento.php", {id: id, opcao_conselho: opcao_conselho, comarca_interior: comarca_interior, motivo_neg: motivo_neg, motivo_abu: motivo_abu, motivo_out: motivo_out,
                numero_destituicao: numero_destituicao, numero_processo: numero_processo, numero_guia_acolhimento: numero_guia_acolhimento,
                descricao: descricao, motivo_aba: motivo_aba, observacao_acolhimento: observacao_acolhimento, data_entrada: data_entrada, hora_entrada: hora_entrada,
                profissional: profissional, setor_profissional: setor_profissional, quem_trouxe: quem_trouxe, responsavel_entrega: responsavel_entrega,
                funcao_responsavel: funcao_responsavel, contato_responsavel: contato_responsavel, sim_residia: sim_residia, tempo_rua: tempo_rua, costumava_abrigar: costumava_abrigar,
                pessoas_vivia_rua: pessoas_vivia_rua, parentes_vinculo: parentes_vinculo, sim_acolhida_anterior: sim_acolhida_anterior, servico_acolhimento: servico_acolhimento}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Acolhimento",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Acolhimento",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/pia/acolhimento/' + id);
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------------------------------
function editar_responsavel() {
    $("div#responsavel_edite_1").hide();
    $("div#responsavel_edite_2").show();
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var conselho_tutelar = $("#conselho_tutelar:checked").val() == 2 ? 2 : 0;
    var comarca_interior = $("#comarca_interior").val();
    var responsavel_entrega = $("#responsavel_entrega").val();
    var funcao_responsavel = $("#funcao_responsavel").val();
    var contato_responsavel = $("#contato_responsavel").val();
    var data_entrada = $("#data_entrada").val();
    var hora_entrada = $("#hora_entrada").val();
    var profissional = $("#profissional").val();
    var numero_guia_acolhimento = $("#numero_guia_acolhimento").val();
    var numero_processo = $("#numero_processo").val();
    var numero_destituicao = $("#numero_destituicao").val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (numero_destituicao == "") {
            $('div#div_numero_destituicao').after('<label id="erro_numero_destituicao" class="error">Número é obrigatório.</label>');
            valido = false;
            element = $('div#div_numero_destituicao');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (numero_processo == "") {
            $('div#div_numero_processo').after('<label id="erro_numero_processo" class="error">Número é obrigatório.</label>');
            valido = false;
            element = $('div#div_numero_processo');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (numero_guia_acolhimento == "") {
            $('div#div_numero_guia_acolhimento').after('<label id="erro_numero_guia_acolhimento" class="error">Númeroo é obrigatório.</label>');
            valido = false;
            element = $('div#div_numero_guia_acolhimento');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (profissional == "") {
            $('div#div_profissional').after('<label id="erro_cprofissional" class="error">Profissional é obrigatório.</label>');
            valido = false;
            element = $('div#div_profissional');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (hora_entrada == "") {
            $('div#div_hora_entrada').after('<label id="erro_hora_entrada" class="error">Hora da entrada é obrigatório.</label>');
            valido = false;
            element = $('div#div_hora_entrada');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (data_entrada == "") {
            $('div#div_data_entrada').after('<label id="erro_data_entrada" class="error">Data entrada é obrigatório.</label>');
            valido = false;
            element = $('div#div_data_entrada');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (contato_responsavel == "") {
            $('div#div_contato_responsavel').after('<label id="erro_contato_responsavel" class="error">Contato é obrigatório.</label>');
            valido = false;
            element = $('div#div_contato_responsavel');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (funcao_responsavel == "") {
            $('div#div_funcao_responsavel').after('<label id="erro_funcao_responsavel" class="error">Função é obrigatório.</label>');
            valido = false;
            element = $('div#div_funcao_responsavel');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (responsavel_entrega == "") {
            $('div#div_responsavel_entrega').after('<label id="erro_responsavel_entrega" class="error">Responsável é obrigatório.</label>');
            valido = false;
            element = $('div#div_responsavel_entrega');
        }

        if (conselho_tutelar == 2 && comarca_interior == "") {
            $('div#div_comarca_interior').after('<label id="erro_comarca_interior" class="error">Comarca é obrigatório.</label>');
            valido = false;
            element = $('div#div_comarca_interior');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function validar(codigo) {
    swal({
        title: "Deseja mesmo confirmar a Etapa 2 Acolhimento?",
        text: "Obs: Caso escolha confirmar, o sistema irá identificar essa etapa como concluída!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode confirmar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/confirmar_acolhido",
            type: "POST",
            data: {id: codigo, op: 2},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------------------------------
function desvalidar(codigo) {
    swal({
        title: "Deseja mesmo desvalidar a Etapa 2 Acolhimento?",
        text: "Obs: Caso escolha desvalidar, o sistema irá identificar essa etapa como pendente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode desvalidar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/desvalidar_acolhido",
            type: "POST",
            data: {id: codigo, op: 2},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/acolhimento/' + obj.id);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
function gerar_pdf() {
    //pega o Html da DIV
    var divElements = document.getElementById("impressao").innerHTML;
    //pega o HTML de toda tag Body
    var oldPage = document.body.innerHTML;
    //Alterna o body 
    document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
    //Imprime o body atual
    window.print();
    //Retorna o conteudo original da página. 
    document.body.innerHTML = oldPage;
}
//------------------------------------------------------------------------------