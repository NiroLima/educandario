<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- CSS DO PLUGIN DE UPLOAD DE FOTOS -->
<link href="<?= PORTAL_URL; ?>assets/cropper/css/cropper.min.css" rel="stylesheet">
<link href="<?= PORTAL_URL; ?>assets/cropper/css/main.css" rel="stylesheet">

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT *      
                            FROM mod_acolhimento ma   
                            WHERE ma.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $resultado = $result->fetch(PDO::FETCH_ASSOC);

    $acolhimento_id = $resultado['id'];
    $acolhimento_quem_trouxe = $resultado['quem_trouxe'];
    $acolhimento_responsavel_entrega = $resultado['responsavel_entrega'];
    $acolhimento_funcao_responsavel = $resultado['funcao_responsavel'];
    $acolhimento_celular = $resultado['celular'];
    $acolhimento_data_entrada = $resultado['data_entrada'];
    $acolhimento_hora_entrada = $resultado['hora_entrada'];
    $acolhimento_profissional_id = $resultado['profissional_id'];
    $obs = $resultado['obs'];
    $motivo_aba = $resultado['motivo_aba'];
    $motivo_neg = $resultado['motivo_neg'];
    $motivo_abu = $resultado['motivo_abu'];
    $motivo_out = $resultado['motivo_out'];
    $descricao = $resultado['descricao'];
    $numero_guia = $resultado['numero_guia'];
    $numero_processo = $resultado['numero_processo'];
    $numero_destituicao = $resultado['numero_destituicao'];
    $acolhimento_comarca = $resultado['comarca_id'];
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
    $result2->bindValue(1, $acolhimento_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $crianca_id = $resultado2['id'];
    $crianca_foto = $resultado2['crianca_foto'];
    $crianca_nome = $resultado2['nome'];
    $pai = $resultado2['pai'];
    $mae = $resultado2['mae'];
    $genero = $resultado2['genero'];
    $nascimento = $resultado2['nascimento'];
    $nascionalidade = $resultado2['nascionalidade'];
    $naturalidade = $resultado2['naturalidade'];
    $religiao = $resultado2['religiao_id'];
    $cor_id = $resultado2['cor_id'];
    $qual_deficiencia = $resultado2['qual_deficiencia'];
    $necessita_equipamentos = $resultado2['necessita_equipamentos'];
    $qual_tratamento = $resultado2['qual_tratamento'];
    $indicio_disturbio = $resultado2['indicio_disturbio'];
    $qual_doenca_infectocontagiosa = $resultado2['qual_doenca_infectocontagiosa'];
    $obs_saude = $resultado2['obs_saude'];
    $rua = $resultado2['rua'];
    $numero = $resultado2['numero'];
    $bairro = $resultado2['bairro'];
    $estado_id = $resultado2['estado_id'];
    $cidade_id = $resultado2['cidade_id'];
} else {
    $acolhimento_id = "";
    $crianca_foto = "";
    $acolhimento_quem_trouxe = "";
    $acolhimento_responsavel_entrega = "";
    $acolhimento_funcao_responsavel = "";
    $acolhimento_celular = "";
    $acolhimento_data_entrada = "";
    $acolhimento_hora_entrada = "";
    $acolhimento_profissional_id = "";
    $obs = "";
    $motivo_aba = "";
    $motivo_neg = "";
    $motivo_abu = "";
    $motivo_out = "";
    $descricao = "";
    $numero_guia = "";
    $numero_processo = "";
    $numero_destituicao = "";
    $acolhimento_comarca = "";
//------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca
    $crianca_id = "";
    $crianca_nome = "";
    $pai = "";
    $mae = "";
    $genero = "";
    $nascimento = "";
    $nascionalidade = "";
    $naturalidade = "";
    $religiao = "";
    $cor_id = "";
    $qual_deficiencia = "";
    $necessita_equipamentos = "";
    $qual_tratamento = "";
    $indicio_disturbio = "";
    $qual_doenca_infectocontagiosa = "";
    $obs_saude = "";
    $rua = "";
    $numero = "";
    $bairro = "";
    $estado_id = "";
    $cidade_id = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/acolhimento/lista">Acolhimento</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div id="crop-avatar">
            <!-- Current avatar -->
            <div style="display: none" id="div_clicado" class="avatar-view" title="Trocar o Foto"></div>
            <!-- Cropping modal -->
            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">

                            <input type="hidden" id="crop_codigo" name="crop_codigo" value="<?= $acolhimento_id; ?>"/>

                            <div class="modal-header">
                                <button class="close" data-dismiss="modal" type="button">&times;</button>
                                <h4 class="modal-title" id="avatar-modal-label">Trocar Foto</h4>
                            </div>
                            <div class="modal-body">
                                <div class="avatar-body">

                                    <!-- Upload image and data -->
                                    <div class="avatar-upload">
                                        <input class="avatar-src" name="avatar_src" type="hidden">
                                        <input class="avatar-data" name="avatar_data" type="hidden">
                                        <label for="avatarInput">Local upload</label>
                                        <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
                                    </div>

                                    <!-- Crop and preview -->
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="avatar-wrapper"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="avatar-preview preview-lg"></div>
                                            <div class="avatar-preview preview-md"></div>
                                            <div class="avatar-preview preview-sm"></div>
                                        </div>
                                    </div>

                                    <div class="row avatar-btns">
                                        <div class="col-md-9">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
                                            </div>
                                            <div class="btn-group">
                                                <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-primary btn-block avatar-save" type="submit">Salvar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="modal-footer">
                              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div><!-- /.modal -->

            <!-- Loading state -->
            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_acolhimento" name="form_acolhimento" method="POST" action="#">
                <input type="hidden" id="id" name="id" value="<?= $acolhimento_id; ?>"/>
                <!-- INGRESSO NO ACOLHIMENTO -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-sign-in"></i> <strong>INGRESSO NO ACOLHIMENTO</strong></h4>
                    </div>
                    <div class="box-body">
                        <!-- QUEM TROUXE -->
                        <div class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>QUEM TROUXE?</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="radio" <?= $acolhimento_quem_trouxe == 2 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="conselho_tutelar" value="2">
                                        <label for="conselho_tutelar">CONSELHO TUTELAR</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" <?= $acolhimento_quem_trouxe == 1 || $acolhimento_quem_trouxe == "" ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="juizado" value="1">
                                        <label for="juizado">PODER JUDICIÁRIO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" <?= $acolhimento_quem_trouxe == 3 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="policia_militar" value="3">
                                        <label for="policia_militar">POLÍCIAS</label>
                                    </div>
                                </div>
                                <hr>
                                <div class="row mt-3">
                                    <div class="col-md-3">
                                        <input type="radio" <?= $acolhimento_quem_trouxe == 4 || $acolhimento_quem_trouxe == "" ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="1conselho" value="4">
                                        <label for="1conselho">1º CONSELHO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" <?= $acolhimento_quem_trouxe == 5 || $acolhimento_quem_trouxe == "" ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="2conselho" value="5">
                                        <label for="2conselho">2º CONSELHO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" <?= $acolhimento_quem_trouxe == 6 || $acolhimento_quem_trouxe == "" ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="3conselho" value="6">
                                        <label for="3conselho">3º CONSELHO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="radio" <?= $acolhimento_quem_trouxe == 7 || $acolhimento_quem_trouxe == "" ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_quem" id="conselhos_interior" value="7">
                                        <label for="conselhos_interior">CONSELHO DO INTERIOR</label>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-4">
                                        <div id="div_comarca_interior" class="form-group">
                                            <label for="comarca_interior">COMARCAS</label>
                                            <select name="comarca_interior" id="comarca_interior" class="form-control select2">
                                                <option value="">Selecione a Comarca</option>
                                                <?php
                                                $result = $db->prepare("SELECT *           
                                                        FROM mod_comarcas 
                                                        WHERE status = 1
                                                        ORDER BY nome ASC");
                                                $result->execute();
                                                while ($comarcas = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_comarca == $comarcas['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $comarcas['id']; ?>"><?= $comarcas['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $comarcas['id']; ?>"><?= $comarcas['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="div_responsavel_entrega" class="form-group">
                                            <label for="responsavel_entrega">RESPONSÁVEL PELA ENTREGA DA CRIANÇA</label>
                                            <input type="text" class="form-control" name="responsavel_entrega" id="responsavel_entrega" placeholder="Responsável pela entraga da criança" value="<?= $acolhimento_responsavel_entrega; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_funcao_responsavel" class="form-group">
                                            <label for="funcao_responsavel">FUNÇÃO DO RESPONSÁVEL</label>
                                            <input type="text" class="form-control" name="funcao_responsavel" id="funcao_responsavel" placeholder="Função do Responsável" value="<?= $acolhimento_funcao_responsavel; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_contato_responsavel" class="form-group">
                                            <label for="contato_responsavel">CELULAR</label>
                                            <input type="text" class="form-control" name="contato_responsavel" id="contato_responsavel" data-mask="(99)99999-9999" placeholder="Celular do Responsável" value="<?= $acolhimento_celular; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM QUEM TROUXE -->

                        <!-- ENTRADA -->
                        <div class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>ENTRADA</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="div_data_entrada" class="form-group">
                                            <label for="data_entrada">DATA DE ENTRADA</label>
                                            <input class="form-control" name="data_entrada" id="data_entrada" type="date" value="<?= convertDataBR2ISO(obterDataBRTimestamp($acolhimento_data_entrada)); ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div i="div_hora_entrada" class="form-group">
                                            <label for="hora_entrada">HORA DE ENTRADA</label>
                                            <input type="text" name="hora_entrada" id="hora_entrada"  data-mask="99:99" class="form-control" value="<?= $acolhimento_hora_entrada; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="div_nome_profissional" class="form-group">
                                            <label for="nome_profissional">NOME DO PROFISSIONAL</label>
                                            <select name="nome_profissional" id="nome_profissional" class="form-control select2">
                                                <option value="">Selecione o profissional</option>
                                                <?php
                                                $funcao_profissional = "";
                                                $setor_profissional = "";
                                                $registro_classe = "";
                                                $result = $db->prepare("SELECT sp.id, sp.nome, sp.registro, sf.nome AS funcao, ss.nome AS setor           
                                                        FROM seg_profissional sp 
                                                        LEFT JOIN seg_funcao AS sf ON sf.id = sp.funcao_id 
                                                        LEFT JOIN seg_setor AS ss ON ss.id = sf.setor_id  
                                                        WHERE sp.status = 1
                                                        GROUP BY sp.id");
                                                $result->execute();
                                                while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acolhimento_profissional_id == $prof['id']) {
                                                        $funcao_profissional = $prof['funcao'];
                                                        $setor_profissional = $prof['setor'];
                                                        $registro_classe = $prof['registro'];
                                                        ?>
                                                        <option selected="true" registro="<?= $registro_classe; ?>" funcao="<?= $prof['funcao']; ?>" rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option registro="<?= $registro_classe; ?>" funcao="<?= $prof['funcao']; ?>" rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div id="div_funcao_profissional" class="form-group">
                                            <label for="funcao_profissional">FUNÇÃO</label>
                                            <input disabled="true" type="text" name="funcao_profissional" id="funcao_profissional" class="form-control" value="<?= $funcao_profissional; ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="setor_profissional">SETOR DO PROFISSIONAL</label>
                                            <input disabled="true" type="text" name="setor_profissional" id="setor_profissional" class="form-control" value="<?= $setor_profissional; ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div id="div_bairro" class="form-group">
                                            <label for="registro_classe">REGISTRO DE CLASSE</label>
                                            <input disabled="true" type="text" class="form-control" name="registro_classe" id="registro_classe" placeholder="Informe o registro de classe" value="<?= $registro_classe; ?>">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="observacao_acolhimento">OBSERVAÇÃO(ÕES) SOBRE A CRIANÇA NO MOMENTO DO ACOLHIMENTO</label>
                                            <textarea name="observacao_acolhimento" id="observacao_acolhimento" class="form-control" cols="30" rows="10"><?= $obs; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM ENTRADA -->

                        <!-- MOTIVO DO ACOLHIMENTO -->
                        <div class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>MOTIVO DO ACOLHIMENTO</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="checkbox" id="abandono" name="abandono" class="filled-in chk-col-info" value="1" <?= $motivo_aba == 1 ? "checked='true'" : ""; ?> />
                                        <label for="abandono">ABANDONO</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="negligencia" name="negligencia" class="filled-in chk-col-info" value="1" <?= $motivo_neg == 1 ? "checked='true'" : ""; ?> />
                                        <label for="negligencia">NEGLIGÊNCIA</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="abuso_sexual" name="abuso_sexual" class="filled-in chk-col-info" value="1" <?= $motivo_abu == 1 ? "checked='true'" : ""; ?> />
                                        <label for="abuso_sexual">ABUSO SEXUAL</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox" id="outros" name="outros" class="filled-in chk-col-info" value="1" <?= $motivo_out == 1 ? "checked='true'" : ""; ?> />
                                        <label for="outros">OUTROS</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="descricao">DESCRIÇÃO</label>
                                            <textarea name="descricao" id="descricao" class="form-control" cols="30" rows="10"><?= $descricao; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM MOTIVO DO ACOLHIMENTO -->

                        <!-- GUIA DE ACOLHIMENTO -->
                        <div class="box box-outline-info">
                            <div class="box-header">
                                <h5 class="box-title mb-0"><strong>DADOS PROCESSUAIS</strong></h5>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="div_numero_guia_acolhimento" class="form-group">
                                            <label for="numero_guia_acolhimento">Nº DA GUIA DE ACOLHIMENTO</label>
                                            <input type="text" class="form-control" name="numero_guia_acolhimento" id="numero_guia_acolhimento" placeholder="Número da guia de acolhimento" value="<?= $numero_guia; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_numero_processo" class="form-group">
                                            <label for="numero_processo">Nº DO PROCESSO</label>
                                            <input type="text" class="form-control" name="numero_processo" id="numero_processo" placeholder="Número do processo" value="<?= $numero_processo; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_numero_destituicao" class="form-group">
                                            <label for="numero_destituicao">Nº DA DESTITUIÇÃO</label>
                                            <input type="text" class="form-control" name="numero_destituicao" id="numero_destituicao" placeholder="Número da destituição" value="<?= $numero_destituicao; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM GUIA DE ACOLHIMENTO -->
                    </div>
                </div>
                <!-- FIM INGRESSO NO ACOLHIMENTO -->

                <!-- CRIANÇA -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-child"></i> <strong>CRIANÇA</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">

                                <?php
                                if ($crianca_foto != "" && file_exists($_SERVER["DOCUMENT_ROOT"] . "/educandario/" . str_replace("../../../", "", $crianca_foto))) {
                                    ?>
                                    <img title="Trocar o Foto" style="cursor: pointer" id="click_foto" src="<?= "../" . $crianca_foto ?>" alt = "Avatar"/>
                                    <?php
                                } else {
                                    ?>
                                    <img title="Trocar o Foto" style="cursor: pointer" id="click_foto" src="<?= PORTAL_URL; ?>assets/avatar/picture.jpg" alt="Avatar"/>
                                    <?php
                                }
                                ?>

                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="div_crianca_nome" class="form-group">
                                            <label for="crianca_nome">NOME</label>
                                            <input type="text" class="form-control" name="crianca_nome" id="crianca_nome" placeholder="Nome Completo" value="<?= $crianca_nome; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="genero">GÊNERO</label>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="radio" class="with-gap radio-col-info" name="opcao_sexo" id="masculino" <?= $genero == 1 || $genero == "" ? "checked='true'" : ""; ?> value="1">
                                                    <label for="masculino">MASCULINO</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="radio" class="with-gap radio-col-info" name="opcao_sexo" id="feminino" <?= $genero == 2 ? "checked='true'" : ""; ?> value="2">
                                                    <label for="feminino">FEMININO</label>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="div_nascimento" class="form-group">
                                            <label for="nascimento">NASCIMENTO</label>
                                            <input class="form-control" name="nascimento" id="nascimento" type="date" value="<?= $nascimento; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="div_nacionalidade" class="form-group">
                                            <label for="nacionalidade">NACIONALIDADE</label>
                                            <input type="text" class="form-control" name="nacionalidade" id="nacionalidade" placeholder="Nacionalidade" value="<?= $nascionalidade; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_naturalidade" class="form-group">
                                            <label for="naturalidade">NATURALIDADE</label>
                                            <input type="text" class="form-control" name="naturalidade" id="naturalidade" placeholder="Naturalidade" value="<?= $naturalidade; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="religiao">RELIGIÃO</label>
                                            <select name="religiao" id="religiao" class="form-control select2">
                                                <option>Selecione a religião</option>
                                                <?php
                                                $result = $db->prepare("SELECT sr.id, sr.nome           
                                                        FROM seg_religiao sr   
                                                        WHERE sr.status = 1");
                                                $result->execute();
                                                while ($rel = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($religiao == $rel['id']) {
                                                        ?>
                                                        <option selected="true" value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?= $rel['id']; ?>"><?= $rel['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- COR/ETNIA -->
                            <div class="box box-outline-info mt-3">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>COR/ETNIA</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 1 || $cor_id == "" ? "checked='true'" : ""; ?> name="opcao_cor" id="branca" value="1">
                                            <label for="branca">BRANCA</label>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 2 ? "checked='true'" : ""; ?> name="opcao_cor" id="preta" value="2">
                                            <label for="preta">PRETA</label>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 3 ? "checked='true'" : ""; ?> name="opcao_cor" id="parda" value="3">
                                            <label for="parda">PARDA</label>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 4 ? "checked='true'" : ""; ?> name="opcao_cor" id="amarela" value="4">
                                            <label for="amarela">AMARELA</label>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="radio" class="with-gap radio-col-info" <?= $cor_id == 5 ? "checked='true'" : ""; ?> name="opcao_cor" id="indigena" value="5">
                                            <label for="indigena">INDÍGENA</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- FILIACAO -->
                            <div class="box box-outline-info mt-2">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>FILIAÇÃO</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="nome_pai">PAI</label>
                                                <input type="text" class="form-control" name="nome_pai" id="nome_pai" placeholder="Nome completo do pai" value="<?= $pai; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="nome_mae">MÃE</label>
                                                <input type="text" class="form-control" name="nome_mae" id="nome_mae" placeholder="Nome completo da mãe" value="<?= $mae; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIM FILIACAO -->

                            <!-- DEFICIENCIA -->
                            <!-- <div class="box box-outline-info mt-2">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>NECESSIDADES ESPECÍFICAS DE SAÚDE</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="genero">POSSUI ALGUMA DEFICIÊNCIA?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_possui" <?= $qual_deficiencia == "" ? "" : "checked='checked'"; ?> id="possui_sim" value="1">
                                                        <label for="possui_sim">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_possui" <?= $qual_deficiencia == "" ? "checked='checked'" : ""; ?> id="possui_nao" value="0">
                                                        <label for="possui_nao">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div id="div_deficiencia" <?= $qual_deficiencia != "" ? "" : "style='display: none'"; ?> class="row">
                                        <div class="col-md-12">
                                            <div id="div_qual_deficiencia" class="form-group">
                                                <label for="qual_deficiencia">QUAL DEFICIÊNCIA?</label>
                                                <input type="text" class="form-control" name="qual_deficiencia" id="qual_deficiencia" placeholder="Deficiência" value="<?= $qual_deficiencia; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="recursos">NECESSITA DE EQUIPAMENTOS/RECURSOS DE TECNOLOGIA ASSISTIVA?</label>
                                                <textarea name="recursos" id="recursos" class="form-control" placeholder="Recursos e Equipamentos" cols="30" rows="10"><?= $necessita_equipamentos; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- FIM DEFICIÊNCIA -->

                            <!-- SAÚDE -->
                            <div class="box box-outline-info mt-2">
                                <div class="box-header">
                                    <h5 class="box-title mb-0"><strong>SAÚDE</strong></h5>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">FAZ TRATAMENTO MÉDICO?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_tratamento" <?= $qual_tratamento == "" ? "" : "checked='true'"; ?> id="sim_tratamento">
                                                        <label for="sim_tratamento">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" name="opcao_tratamento" <?= $qual_tratamento == "" ? "checked='true'" : ""; ?> id="nao_tratamento">
                                                        <label for="nao_tratamento">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div id="div_qual_tratamento" <?= $qual_tratamento == "" ? "style='display: none'" : ""; ?> class="col-md-8">
                                            <div class="form-group">
                                                <label for="qual_tratamento">QUAL TRATAMENTO?</label>
                                                <input type="text" class="form-control" name="qual_tratamento" id="qual_tratamento" placeholder="Informe o tratamento" value="<?= $qual_tratamento; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">INDÍCIOS DE DISTÚRBIO MENTAL?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" <?= $indicio_disturbio == "" ? "" : "checked='true'"; ?> name="opcao_indicios" id="sim_indicios" value="1">
                                                        <label for="sim_indicios">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" <?= $indicio_disturbio == "" ? "checked='true'" : ""; ?> name="opcao_indicios" id="nao_indicios" value="2">
                                                        <label for="nao_indicios">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">POSSUI DOENÇA INFECTOCONTAGIOSA?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" <?= $qual_doenca_infectocontagiosa == "" ? "" : "checked='true'"; ?> name="opcao_doenca" id="sim_doenca">
                                                        <label for="sim_doenca">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" class="with-gap radio-col-info" <?= $qual_doenca_infectocontagiosa == "" ? "checked='true'" : ""; ?> name="opcao_doenca" id="nao_doenca">
                                                        <label for="nao_doenca">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div id="div_infectocontagiosa" <?= $qual_doenca_infectocontagiosa == "" ? "style='display: none'" : ""; ?> class="col-md-8">
                                            <div class="form-group">
                                                <label for="qual_doenca_infectocontagiosa">QUAL DOENÇA INFECTOCONTAGIOSA?</label>
                                                <input type="text" class="form-control" name="qual_doenca_infectocontagiosa" id="qual_doenca_infectocontagiosa" placeholder="Informe a doença infectocontagiosa" value="<?= $qual_doenca_infectocontagiosa; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $result3 = $db->prepare("SELECT mm.id, mm.nome           
                                                    FROM mod_medicamento mm   
                                                    WHERE mm.status = 1 AND mm.id IN (SELECT medicamento_id FROM mod_acolhimento_crianca_medicacao WHERE acolhimento_crianca_id = ?)");
                                    $result3->bindValue(1, $crianca_id);
                                    $result3->execute();
                                    $qtd_medicamento = $result3->rowCount();
                                    ?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="genero">VIERAM MEDICAÇÕES COM A CRIANÇA?</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $qtd_medicamento > 0 ? "checked='true'" : ""; ?> class="with-gap radio-col-info" name="opcao_medicacao" id="sim_medicacoes">
                                                        <label for="sim_medicacoes">SIM</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input type="radio" <?= $qtd_medicamento > 0 ? "" : "checked='true'"; ?> class="with-gap radio-col-info" name="opcao_medicacao" id="nao_medicacoes">
                                                        <label for="nao_medicacoes">NÃO</label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <div id="div_medicacao_crianca" <?= $qtd_medicamento > 0 ? "" : "style='display: none'"; ?> class="col-md-8">
                                            <div class="form-group">
                                                <label for="qual_medicamentos">QUAL(IS) MEDICAMENTOS?</label>
                                                <select name="qual_medicamentos" id="qual_medicamentos" class="form-control select2" multiple="true">
                                                    <?php
                                                    while ($medicamentos = $result3->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                        <option selected="true" value="<?= $medicamentos['id']; ?>"><?= $medicamentos['nome']; ?></option>
                                                        <?php
                                                    }

                                                    $result33 = $db->prepare("SELECT mm.id, mm.nome           
                                                    FROM mod_medicamento mm   
                                                    WHERE mm.status = 1 AND mm.id NOT IN (SELECT medicamento_id FROM mod_acolhimento_crianca_medicacao WHERE acolhimento_crianca_id = ?)");
                                                    $result33->bindValue(1, $crianca_id);
                                                    $result33->execute();
                                                    while ($medicamentos = $result33->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                        <option value="<?= $medicamentos['id']; ?>"><?= $medicamentos['nome']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="obs_saude">OBSERVAÇÃO</label>
                                                <textarea name="obs_saude" id="obs_saude" class="form-control" placeholder="Observação sobre a saúde da Criança" cols="30" rows="10"><?= $obs_saude; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIM SAÚDE -->

                        </div>
                    </div>
                </div>
                <!-- FIM CRIANÇA -->

                <!-- ENDEREÇO -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-home-lg"></i> <strong>ENDEREÇO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="rua">RUA</label>
                                    <input type="text" class="form-control" name="rua" id="rua" placeholder="Rua, Avenida e etc..." value="<?= $rua ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="numero">NÚMERO</label>
                                    <input type="text" class="form-control" name="numero" id="numero" placeholder="1.260" value="<?= $numero ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="bairro">BAIRRO</label>
                                    <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro" value="<?= $bairro ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="estado">ESTADO</label>
                                    <select name="estado" id="estado" class="form-control select2">
                                        <option value="">Selecione o estado</option>
                                        <?php
                                        $result = $db->prepare("SELECT id, nome            
                                                        FROM bsc_estado    
                                                        WHERE 1 ");
                                        $result->execute();
                                        while ($estado = $result->fetch(PDO::FETCH_ASSOC)) {
                                            if ($estado_id == $estado['id']) {
                                                ?>
                                                <option selected="true" value="<?= $estado['id']; ?>"><?= $estado['nome']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="<?= $estado['id']; ?>"><?= $estado['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cidade">CIDADE</label>
                                    <select name="cidade" id="cidade" class="form-control select2">
                                        <option value="">Escolha primeiro o estado</option>
                                        <?php
                                        $result2 = $db->prepare("SELECT nome, id
                                                     FROM bsc_cidade 
                                                     WHERE 1 
                                                     ORDER BY nome ASC");
                                        $result2->execute();
                                        while ($municipio = $result2->fetch(PDO::FETCH_ASSOC)) {
                                            if ($cidade_id == $municipio['id']) {
                                                ?>
                                                <option selected="true" value='<?= $municipio['id']; ?>'><?= $municipio['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM ENDEREÇO -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $acolhimento_id == "" ? "" : 'style="display: none"'; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $acolhimento_id == "" ? 'style="display: none"' : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JAVASCRIPT BÁSICOS -->
<script src="<?= PORTAL_URL ?>assets/cropper/js/cropper.min.js"></script>
<script src="<?= PORTAL_URL ?>assets/cropper/js/main_servidor.js"></script>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/acolhimento/novo.js"></script>