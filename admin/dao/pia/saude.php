<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$acolhimento_crianca_id = isset($_POST['acolhimento_crianca_id']) && $_POST['acolhimento_crianca_id'] != "" ? $_POST['acolhimento_crianca_id'] : 0;
$qual_deficiencia = isset($_POST['qual_deficiencia']) && $_POST['qual_deficiencia'] != "" ? $_POST['qual_deficiencia'] : NULL;
$recursos = isset($_POST['recursos']) && $_POST['recursos'] != "" ? $_POST['recursos'] : NULL;
$qual_tratamento = isset($_POST['qual_tratamento']) && $_POST['qual_tratamento'] != "" ? $_POST['qual_tratamento'] : NULL;
$disturbio = isset($_POST['disturbio']) && $_POST['disturbio'] != "" ? $_POST['disturbio'] : NULL;
$qual_doenca_infectocontagiosa = isset($_POST['qual_doenca_infectocontagiosa']) && $_POST['qual_doenca_infectocontagiosa'] != "" ? $_POST['qual_doenca_infectocontagiosa'] : NULL;
$qual_medicamentos = isset($_POST['qual_medicamentos']) && $_POST['qual_medicamentos'] != "" ? $_POST['qual_medicamentos'] : NULL;
$obs_saude = isset($_POST['obs_saude']) && $_POST['obs_saude'] != "" ? $_POST['obs_saude'] : NULL;

$peso = isset($_POST['peso']) && $_POST['peso'] != "" ? $_POST['peso'] : NULL;
$altura = isset($_POST['altura']) && $_POST['altura'] != "" ? $_POST['altura'] : NULL;
$tipo_sanguineo = isset($_POST['tipo_sanguineo']) && $_POST['tipo_sanguineo'] != "" ? $_POST['tipo_sanguineo'] : NULL;
$condicoes_saude = isset($_POST['condicoes_saude']) && $_POST['condicoes_saude'] != "" ? $_POST['condicoes_saude'] : NULL;
$justificativa = isset($_POST['justificativa']) && $_POST['justificativa'] != "" ? $_POST['justificativa'] : NULL;
$atividade_psicologia = isset($_POST['atividade_psicologia']) && $_POST['atividade_psicologia'] != "" ? $_POST['atividade_psicologia'] : NULL;

$acolhimento_crianca_geral_id = isset($_POST['acolhimento_crianca_geral_id']) && $_POST['acolhimento_crianca_geral_id'] != "" ? $_POST['acolhimento_crianca_geral_id'] : NULL;
$tipo_deficiencia = isset($_POST['tipo_deficiencia']) && $_POST['tipo_deficiencia'] != "" ? $_POST['tipo_deficiencia'] : NULL;
$descreva_tipo_def = isset($_POST['descreva_tipo_def']) && $_POST['descreva_tipo_def'] != "" ? $_POST['descreva_tipo_def'] : NULL;

$bcp_sim = isset($_POST['bcp_sim']) && $_POST['bcp_sim'] != "" ? $_POST['bcp_sim'] : 0;

$responsavel_recebimento = isset($_POST['responsavel_recebimento']) && $_POST['responsavel_recebimento'] != "" ? $_POST['responsavel_recebimento'] : NULL;

$atvd = isset($_POST['atvd']) && $_POST['atvd'] != "" ? $_POST['atvd'] : 0;

if ($error == false) {
    try {

        $db->beginTransaction();

        $sql2 = $db->prepare("UPDATE mod_acolhimento_crianca SET qual_deficiencia = ?, necessita_equipamentos = ?, qual_tratamento = ?, indicio_disturbio = ?, qual_doenca_infectocontagiosa = ?, obs_saude = ?, usuario_id = ? 
                              WHERE acolhimento_id = ?");
        $sql2->bindValue(1, $qual_deficiencia);
        $sql2->bindValue(2, $recursos);
        $sql2->bindValue(3, $qual_tratamento);
        $sql2->bindValue(4, $disturbio);
        $sql2->bindValue(5, $qual_doenca_infectocontagiosa);
        $sql2->bindValue(6, $obs_saude);
        $sql2->bindValue(7, $_SESSION['id']);
        $sql2->bindValue(8, $id);
        $sql2->execute();

        if (is_numeric(pesquisar("id", "mod_acolhimento_crianca_geral", "acolhimento_crianca_id", "=", $acolhimento_crianca_id, ""))) {
            $sql4 = $db->prepare("UPDATE mod_acolhimento_crianca_geral SET peso = ?, altura = ?, tipo_sanguineo = ?, condicao_geral  = ?, justificativa = ?, atividade_psicologia = ?,
                                  def_descricao = ?, def_beneficiaria  = ?, def_responsavel_recebimento = ?, responsavel_id = ?, atvd = ?    
                                  WHERE acolhimento_crianca_id = ?");
            $sql4->bindValue(1, $peso);
            $sql4->bindValue(2, $altura);
            $sql4->bindValue(3, $tipo_sanguineo);
            $sql4->bindValue(4, $condicoes_saude);
            $sql4->bindValue(5, $justificativa);
            $sql4->bindValue(6, $atividade_psicologia);
            $sql4->bindValue(7, $descreva_tipo_def);
            $sql4->bindValue(8, $bcp_sim);
            $sql4->bindValue(9, $responsavel_recebimento);
            $sql4->bindValue(10, $_SESSION['id']);
            $sql4->bindValue(11, $atvd);
            $sql4->bindValue(12, $acolhimento_crianca_id);
            $sql4->execute();
        } else {
            $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_geral (acolhimento_crianca_id, peso, altura, tipo_sanguineo, condicao_geral, justificativa, atividade_psicologia, 
                                  def_descricao, def_beneficiaria, def_responsavel_recebimento, responsavel_id, atvd, data_cadastro, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1)");
            $sql4->bindValue(1, $acolhimento_crianca_id);
            $sql4->bindValue(2, $peso);
            $sql4->bindValue(3, $altura);
            $sql4->bindValue(4, $tipo_sanguineo);
            $sql4->bindValue(5, $condicoes_saude);
            $sql4->bindValue(6, $justificativa);
            $sql4->bindValue(7, $atividade_psicologia);
            $sql4->bindValue(8, $descreva_tipo_def);
            $sql4->bindValue(9, $bcp_sim);
            $sql4->bindValue(10, $responsavel_recebimento);
            $sql4->bindValue(11, $atvd);
            $sql4->bindValue(12, $_SESSION['id']);
            $sql4->execute();

            $id = $db->lastInsertId();
        }

        $sql3 = $db->prepare("DELETE FROM mod_acolhimento_crianca_medicacao WHERE acolhimento_crianca_id = ?");
        $sql3->bindValue(1, $acolhimento_crianca_id);
        $sql3->execute();

        if (is_array($qual_medicamentos)) {
            foreach ($qual_medicamentos AS $key => $val) {
                if ($val != "" && $val != null) {
                    $sql3 = $db->prepare("INSERT INTO mod_acolhimento_crianca_medicacao (acolhimento_crianca_id, medicamento_id) VALUES (?, ?)");
                    $sql3->bindValue(1, $acolhimento_crianca_id);
                    $sql3->bindValue(2, $val);
                    $sql3->execute();
                }
            }
        }

        $sql33 = $db->prepare("DELETE FROM mod_acolhimento_crianca_geral_tipo WHERE acolhimento_crianca_geral_id = ?");
        $sql33->bindValue(1, $acolhimento_crianca_geral_id);
        $sql33->execute();

        if ($tipo_deficiencia != null) {
            foreach ($tipo_deficiencia AS $key => $val) {
                if (is_numeric($val) && $val != "" && $val != null) {
                    $sql333 = $db->prepare("INSERT INTO mod_acolhimento_crianca_geral_tipo (acolhimento_crianca_geral_id, tipo_deficiencia) VALUES (?, ?)");
                    $sql333->bindValue(1, $acolhimento_crianca_geral_id);
                    $sql333->bindValue(2, $val);
                    $sql333->execute();
                }
            }
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>