<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$nome_medico = isset($_POST['nome_medico']) ? $_POST['nome_medico'] : NULL;
$especialidade = isset($_POST['especialidade']) ? $_POST['especialidade'] : 0;
$tipo_funcionario = isset($_POST['tipo_funcionario']) ? $_POST['tipo_funcionario'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "mod_saude_medicos", "nome", "=", $nome_medico, "");
if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome do médico informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_saude_medicos (nome, especialidade_id, tipo, data_update, responsavel_id, data_cadastro, status) VALUES (?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $nome_medico);
            $sql->bindValue(2, $especialidade);
            $sql->bindValue(3, $tipo_funcionario);
            $sql->bindValue(4, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE mod_saude_medicos SET nome = ?, especialidade_id = ?, tipo = ?, responsavel_id = ? WHERE id = ?");
            $sql->bindValue(1, $nome_medico);
            $sql->bindValue(2, $especialidade);
            $sql->bindValue(3, $tipo_funcionario);
            $sql->bindValue(4, $_SESSION['id']);
            $sql->bindValue(5, $id);
            $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>