<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Acolhimento</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-clipboard-list-check"></i> <strong>LISTA DE ACOLHIDOS</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/acolhimento/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVO ACOLHIDO</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>CRIANÇA</th>
                                            <th>QUEM TROUXE</th>
                                            <th>ENTRADA</th>
                                            <th>HORA</th>
                                            <th>PROFISSIONAL</th>
                                            <th width="22%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $db->prepare("SELECT ma.id, mac.nome AS crianca, ma.quem_trouxe, ma.data_entrada, ma.hora_entrada, fc.nome AS profissional         
                                                                FROM mod_acolhimento ma  
                                                                LEFT JOIN mod_acolhimento_crianca AS mac ON mac.acolhimento_id = ma.id  
                                                                LEFT JOIN seg_funcao AS fc ON fc.id = ma.profissional_id  
                                                                WHERE ma.status = 1 
                                                                ORDER BY mac.nome");
                                        $result->execute();
                                        while ($acolhimento = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $acolhimento['crianca'] ?></td>
                                                <td><?= $acolhimento['quem_trouxe'] == 1 ? "Poder Judiciário" : ($acolhimento['quem_trouxe'] == 2 ? "Conselho Tutelar" : ($acolhimento['quem_trouxe'] == 3 ? "Polícias" : "")); ?></td>
                                                <td><?= obterDataBRTimestamp($acolhimento['data_entrada']) ?></td>
                                                <td><?= $acolhimento['hora_entrada'] ?></td>
                                                <td><?= $acolhimento['profissional'] ?></td>
                                                <td class="text-center">
                                                    <?php
                                                    if (is_numeric(pesquisar("id", "mod_acolhimento_documentacao", "acolhimento_id", "=", $acolhimento['id'], ""))) {
                                                        ?>
                                                        <a title="Gerar Pia" href="<?= PORTAL_URL ?>admin/view/pia/acolhido/<?= $acolhimento['id']; ?>" class="mr-2 waves-effect waves-light btn btn-xs btn-primary">GERAR PIA</a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <input disabled="true" title="Está faltando cadastrar a documentação para o geramento do Pia" type="button" class="mr-2 waves-light btn btn-xs btn-primary" value="GERAR PIA">
                                                        <?php
                                                    }
                                                    ?>
                                                    <a title="Documentação" href="<?= PORTAL_URL; ?>admin/view/acolhimento/novo2/<?= $acolhimento['id']; ?>" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-file-archive-o"></i></a>
                                                    <a title="Editar Acolhimento" href="<?= PORTAL_URL; ?>admin/view/acolhimento/novo/<?= $acolhimento['id']; ?>" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                                    <a title="Cancelar Acolhimento" style="cursor: pointer" id="remover" rel="<?= $acolhimento['id']; ?>" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-times-circle"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/acolhimento/lista.js"></script>
