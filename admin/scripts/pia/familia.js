//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("button#atualizar").click(function () {
        $("form#form_familia").submit();
    });

    $("#familiar_interesse_sim").click(function () {
        $("#div_quem_acolhe").show();
    });

    $("#familiar_interesse_nao").click(function () {
        $("#div_quem_acolhe").hide();
    });

    $("#demonstram_interesse").click(function () {
        $("textarea#descreva_interesse").val("");
        $("#div_descricao_interesse").show();
    });

    $("#nao_demonstram_interesse").click(function () {
        $("textarea#descreva_interesse").val("");
        $("#div_descricao_interesse").hide();
    });

    $("#crianca_demonstra_interesse").click(function () {
        $("textarea#descreva_demonstra_interesse").val("");
        $("#div_crianca_demonstra_interesse").show();
    });

    $("#crianca_nao_demonstra_interesse").click(function () {
        $("textarea#descreva_demonstra_interesse").val("");
        $("#div_crianca_demonstra_interesse").hide();
    });

    $("#com_irmaos_servico_acolhimento").click(function () {
        $("#div_com_irmaos_servico_acolhimento").show();
    });

    $("#sem_irmaos_servico_acolhimento").click(function () {
        $("#div_com_irmaos_servico_acolhimento").hide();
    });

    $("#com_irmaos_outros_servico_acolhimento").click(function () {
        $("#div_com_irmaos_outros_servico_acolhimento").show();
    });

    $("#sem_irmaos_outros_servico_acolhimento").click(function () {
        $("#div_com_irmaos_outros_servico_acolhimento").hide();
    });

    $("#com_irmaos_em_servico_acolhimento").click(function () {
        $("#div_com_irmaos_em_servico_acolhimento").show();
    });

    $("#sem_irmaos_em_servico_acolhimento").click(function () {
        $("#div_com_irmaos_em_servico_acolhimento").hide();
    });

    $("#com_irmaos_adotados").click(function () {
        $("#div_com_irmaos_adotados").show();
    });

    $("#sem_irmaos_adotados").click(function () {
        $("#div_com_irmaos_adotados").hide();
    });

    $("#sim_proibicao_visita").click(function () {
        $("div#div_tabela_medida").show();
        $("#div_quem_estabeleceu_impedimento").show();
    });

    $("#nao_proibicao_visita").click(function () {
        $("#quem_estabeleceu_impedimento").val("");
        $("div#div_tabela_medida").hide();
        $("#div_quem_estabeleceu_impedimento").hide();
    });

    $("#sim_recebe_visita").click(function () {
        $("#div_sim_recebe_visita").show();
    });

    $("#nao_recebe_visita").click(function () {
        $("#div_sim_recebe_visita").hide();
    });

    $("#sim_abusivo_alcool").click(function () {
        $("#div_sim_abusivo_alcool").show();
    });

    $("#nao_abusivo_alcool").click(function () {
        $("#div_sim_abusivo_alcool").hide();
    });

    $("#sim_abusivo_outras_drogas").click(function () {
        $("#div_sim_abusivo_outras_drogas").show();
    });

    $("#nao_abusivo_outras_drogas").click(function () {
        $("#div_sim_abusivo_outras_drogas").hide();
    });

    $("#sim_exploracao_sexual").click(function () {
        $("#div_sim_exploracao_sexual").show();
    });

    $("#nao_exploracao_sexual").click(function () {
        $("#div_sim_exploracao_sexual").hide();
    });

    $("#sim_situacao_rua").click(function () {
        $("#div_sim_situacao_rua").show();
    });

    $("#nao_situacao_rua").click(function () {
        $("#div_sim_situacao_rua").hide();
    });

    $("#sim_cumprimento_pena").click(function () {
        $("#div_sim_cumprimento_pena").show();
    });

    $("#nao_cumprimento_pena").click(function () {
        $("#div_sim_cumprimento_pena").hide();
    });

    $("#sim_reabilitacao").click(function () {
        $("#div_sim_reabilitacao").show();
    });

    $("#nao_reabilitacao").click(function () {
        $("#div_sim_reabilitacao").hide();
    });

    $("#sim_ameaca_morte").click(function () {
        $("#div_sim_ameaca_morte").show();
    });

    $("#nao_ameaca_morte").click(function () {
        $("#div_sim_ameaca_morte").hide();
    });

    $("#sim_doenca_grave").click(function () {
        $("#div_qual_doença").show();
        $("#div_sim_doenca_grave").show();
    });

    $("#nao_doenca_grave").click(function () {
        $("#qual_doença").val("");
        $("#div_qual_doença").hide();
        $("#div_sim_doenca_grave").hide();
    });

    $("#sim_transtorno").click(function () {
        $("#div_transtorno").show();
        $("#div_sim_transtorno").show();
    });

    $("#nao_transtorno").click(function () {
        $("#transtorno").val("");
        $("#div_transtorno").hide();
        $("#div_sim_transtorno").hide();
    });

    $('#form_responsavel').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var responsavel_id = $("#responsavel_id").val();

        $.post(PORTAL_URL + "admin/dao/pia/acolhido_responsavel.php", {id: id, responsavel_id: responsavel_id}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário PIA",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário PIA",
                    text: "Informações do acolhido atualizadas com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/familia/' + id);
                });
            }
        }
        , "html");
        return false;
    });

    $('#form_familia').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var acolhimento_crianca_id = $('#acolhimento_crianca_id').val();
        var familia_id = $("#familia_id").val();
        var nome_mae = $("#nome_mae").val();
        var nascimento_mae = $("#nascimento_mae").val();
        var endereco_mae = $("#endereco_mae").val();
        var numero_mae = $("#numero_mae").val();
        var bairro_mae = $("#bairro_mae").val();
        var rg_mae = $("#rg_mae").val();
        var cpf_mae = $("#cpf_mae").val();
        var celular_mae = $("#celular_mae").val();
        var ocupacao_mae = $("#ocupacao_mae").val();
        var mae_falecida = $("#mae_falecida:checked").val() == 1 ? 1 : 0;
        var mae_reclusa = $("#mae_reclusa:checked").val() == 1 ? 1 : 0;
        var mae_nao_encontrada = $("#mae_nao_encontrada:checked").val() == 1 ? 1 : 0;
        var mae_desconhecida = $("#mae_desconhecida:checked").val() == 1 ? 1 : 0;
        var sem_vinculo_mae = $("#sem_vinculo_mae:checked").val() == 1 ? 1 : 0;

        var nome_pai = $("#nome_pai").val();
        var nascimento_pai = $("#nascimento_pai").val();
        var endereco_pai = $("#endereco_pai").val();
        var numero_pai = $("#numero_pai").val();
        var bairro_pai = $("#bairro_pai").val();
        var rg_pai = $("#rg_pai").val();
        var cpf_pai = $("#cpf_pai").val();
        var celular_pai = $("#celular_pai").val();
        var ocupacao_pai = $("#ocupacao_pai").val();
        var pai_falecida = $("#pai_falecido:checked").val() == 1 ? 1 : 0;
        var pai_reclusa = $("#pai_recluso:checked").val() == 1 ? 1 : 0;
        var pai_nao_encontrada = $("#pai_nao_encontrado:checked").val() == 1 ? 1 : 0;
        var pai_desconhecida = $("#pai_desconhecido:checked").val() == 1 ? 1 : 0;
        var sem_vinculo_pai = $("#sem_vinculo_pai:checked").val() == 1 ? 1 : 0;

        var com_condicoes_convivio = $("#com_condicoes_convivio:checked").val() == 1 ? 1 : ($("#avaliacao_profunda:checked").val() == 1 ? 2 : 0);
        
        var descreva_avaliacao_profunda = $("#descreva_avaliacao_profunda").val();
        var com_irmaos_servico_acolhimento = $("#com_irmaos_servico_acolhimento:checked").val() == 1 ? 1 : 0;

        var com_irmaos_outros_servico_acolhimento = $("#com_irmaos_outros_servico_acolhimento:checked").val() == 1 ? 1 : 0;
        var com_irmaos_em_servico_acolhimento = $("#com_irmaos_em_servico_acolhimento:checked").val() == 1 ? 1 : 0;

        var com_irmaos_adotados = $("#com_irmaos_adotados:checked").val() == 1 ? 1 : 0;

        var com_vinculo = $("#com_vinculo:checked").val() == 1 ? 1 : 0;
        var sem_vinculo = $("#sem_vinculo:checked").val() == 1 ? 1 : 0;
        var familia_desaparecida = $("#familia_desaparecida:checked").val() == 1 ? 1 : 0;
        var orfao = $("#orfao:checked").val() == 1 ? 1 : 0;
        var poder_familiar = $("#poder_familiar:checked").val() == 1 ? 1 : 0;
        var impedimento_judicial = $("#impedimento_judicial:checked").val() == 1 ? 1 : 0;
        var sem_informacao = $("#sem_informacao:checked").val() == 1 ? 1 : 0;
        var outra_situacao = $("#outra_situacao:checked").val() == 1 ? 1 : 0;

        var qual_outra_situacao = $("#qual_outra_situacao").val();

        var demonstram_interesse = $("#demonstram_interesse:checked").val() == 1 ? 1 : 0;
        var descreva_interesse = $("#descreva_interesse").val();
        var crianca_demonstra_interesse = $("#crianca_demonstra_interesse:checked").val() == 1 ? 1 : 0;
        var descreva_demonstra_interesse = $("#descreva_demonstra_interesse").val();
        var sim_proibicao_visita = $("#sim_proibicao_visita:checked").val() == 1 ? 1 : 0;
        var quem_estabeleceu_impedimento = $("#quem_estabeleceu_impedimento").val();

        var sim_recebe_visita = $("#sim_recebe_visita:checked").val() == 1 ? 1 : 0;
        var encontros = $("#encontros").val();
        var como_realizados = $("#como_realizados").val();
        var sim_abusivo_alcool = $("#sim_abusivo_alcool:checked").val() == 1 ? 1 : 0;

        var sim_abusivo_outras_drogas = $("#sim_abusivo_outras_drogas:checked").val() == 1 ? 1 : 0;
        var sim_exploracao_sexual = $("#sim_exploracao_sexual:checked").val() == 1 ? 1 : 0;
        var sim_situacao_rua = $("#sim_situacao_rua:checked").val() == 1 ? 1 : 0;
        var sim_cumprimento_pena = $("#sim_cumprimento_pena:checked").val() == 1 ? 1 : 0;
        var sim_reabilitacao = $("#sim_reabilitacao:checked").val() == 1 ? 1 : 0;
        var sim_ameaca_morte = $("#sim_ameaca_morte:checked").val() == 1 ? 1 : 0;
        var sim_doenca_grave = $("#sim_doenca_grave:checked").val() == 1 ? 1 : 0;
        var sim_transtorno = $("#sim_transtorno:checked").val() == 1 ? 1 : 0;

        var qual_doença = $("#qual_doença").val();
        var transtorno = $("#transtorno").val();
        var pessoa_responsavel = $("#pessoa_responsavel").val();
        var crianca_convive = $("#crianca_convive").val();
        var familiar_interesse_sim = $("#familiar_interesse_sim").val();
        var quem = $("#quem").val();

        var familiar_condicoes_sim = $("#familiar_condicoes_sim:checked").val() == 1 ? 1 : ($("#familia_condicoes_nao:checked").val() == 2 ? 2 : 3);
        var descreva_condicao_cuidado = $("#descreva_condicao_cuidado").val();
        var opiniao_crianca = $("#opiniao_crianca").val();

        $.post(PORTAL_URL + "admin/dao/pia/familia.php", {id: id, opiniao_crianca: opiniao_crianca, descreva_condicao_cuidado: descreva_condicao_cuidado, familiar_condicoes_sim: familiar_condicoes_sim, quem: quem, familiar_interesse_sim: familiar_interesse_sim, crianca_convive: crianca_convive, pessoa_responsavel: pessoa_responsavel, qual_doença: qual_doença, transtorno: transtorno, sim_transtorno: sim_transtorno, sim_doenca_grave: sim_doenca_grave, sim_ameaca_morte: sim_ameaca_morte, sim_reabilitacao: sim_reabilitacao, sim_cumprimento_pena: sim_cumprimento_pena, sim_abusivo_outras_drogas: sim_abusivo_outras_drogas, sim_exploracao_sexual: sim_exploracao_sexual, sim_situacao_rua: sim_situacao_rua,
            sim_abusivo_alcool: sim_abusivo_alcool, como_realizados: como_realizados, sim_recebe_visita: sim_recebe_visita, encontros: encontros, sim_proibicao_visita: sim_proibicao_visita, quem_estabeleceu_impedimento: quem_estabeleceu_impedimento, descreva_demonstra_interesse: descreva_demonstra_interesse, demonstram_interesse: demonstram_interesse, descreva_interesse: descreva_interesse, crianca_demonstra_interesse: crianca_demonstra_interesse,
            qual_outra_situacao: qual_outra_situacao, com_vinculo: com_vinculo, sem_vinculo: sem_vinculo, familia_desaparecida: familia_desaparecida, orfao: orfao, poder_familiar: poder_familiar, impedimento_judicial: impedimento_judicial, sem_informacao: sem_informacao, outra_situacao: outra_situacao,
            com_irmaos_adotados: com_irmaos_adotados, com_condicoes_convivio: com_condicoes_convivio, descreva_avaliacao_profunda: descreva_avaliacao_profunda, acolhimento_crianca_id: acolhimento_crianca_id, familia_id: familia_id, nome_mae: nome_mae, nascimento_mae: nascimento_mae, endereco_mae: endereco_mae, numero_mae: numero_mae, bairro_mae: bairro_mae,
            com_irmaos_em_servico_acolhimento: com_irmaos_em_servico_acolhimento, com_irmaos_outros_servico_acolhimento: com_irmaos_outros_servico_acolhimento, rg_mae: rg_mae, cpf_mae: cpf_mae, celular_mae: celular_mae, ocupacao_mae: ocupacao_mae, mae_falecida: mae_falecida, mae_reclusa: mae_reclusa, mae_nao_encontrada: mae_nao_encontrada, mae_desconhecida: mae_desconhecida, sem_vinculo_mae: sem_vinculo_mae,
            nome_pai: nome_pai, nascimento_pai: nascimento_pai, endereco_pai: endereco_pai, numero_pai: numero_pai, bairro_pai: bairro_pai, com_irmaos_servico_acolhimento: com_irmaos_servico_acolhimento,
            rg_pai: rg_pai, cpf_pai: cpf_pai, celular_pai: celular_pai, ocupacao_pai: ocupacao_pai, pai_falecida: pai_falecida, pai_reclusa: pai_reclusa, pai_nao_encontrada: pai_nao_encontrada, pai_desconhecida: pai_desconhecida, sem_vinculo_pai: sem_vinculo_pai}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário de Família",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário de Família",
                    text: "Ação realizada com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/familia/' + id);
                });
            }
        }
        , "html");
        return false;

    });

    $("#inserir_parentesco").click(function () {

        var id = $("#familia_id").val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var nome_parente = $("#nome_parente").val();
        var nascimento_parente = $("#nascimento_parente").val();
        var rg_parente = $("#rg_parente").val();
        var cpf_parente = $("#cpf_parente").val();
        var celular_parente = $("#celular_parente").val();
        var endereco = $("#endereco").val();
        var ocupacao_parente = $("#ocupacao_parente").val();
        var grau_parentesco = $("#grau_parentesco").val();

        var parente_id = $("#parente_id").val();

        if (formulario_validator("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco.php", {id: id, acolhimento_crianca_id: acolhimento_crianca_id, grau_parentesco: grau_parentesco, parente_id: parente_id, ocupacao_parente: ocupacao_parente, endereco: endereco, celular_parente: celular_parente, cpf_parente: cpf_parente, nome_parente: nome_parente, nascimento_parente: nascimento_parente, rg_parente: rg_parente}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Vínculo",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Vínculo",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos.php",
                                {id: acolhimento_crianca_id},
                                function (valor) {
                                    $("input#parente_id").val("");
                                    $("select#grau_parentesco").val("");
                                    $("input#nome_parente").val("");
                                    $("input#nascimento_parente").val("");
                                    $("input#rg_parente").val("");
                                    $("input#cpf_parente").val("");
                                    $("input#celular_parente").val("");
                                    $("input#endereco").val("");
                                    $("input#ocupacao_parente").val("");
                                    $("button#inserir_parentesco").html("INSERIR");
                                    $("tbody#resultado_parentesco").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco2").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede2").val();
        var parente_id = $("#parente_id2").val();

        if (formulario_validator_6("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco2.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos2.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id2").val("");
                                    $("select#quem_impede2").val("");
                                    $("select#quem_impede2").select2();
                                    $("button#inserir_parentesco2").html("INSERIR");
                                    $("tbody#resultado_parentesco2").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco3").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede3").val();
        var parente_id = $("#parente_id3").val();

        if (formulario_validator_7("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco3.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos3.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id3").val("");
                                    $("select#quem_impede3").val("");
                                    $("select#quem_impede3").select2();
                                    $("button#inserir_parentesco3").html("INSERIR");
                                    $("tbody#resultado_parentesco3").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco4").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede4").val();
        var parente_id = $("#parente_id4").val();

        if (formulario_validator_8("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco4.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos4.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id4").val("");
                                    $("select#quem_impede4").val("");
                                    $("select#quem_impede4").select2();
                                    $("button#inserir_parentesco4").html("INSERIR");
                                    $("tbody#resultado_parentesco4").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco5").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede5").val();
        var parente_id = $("#parente_id5").val();

        if (formulario_validator_9("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco5.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos5.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id5").val("");
                                    $("select#quem_impede5").val("");
                                    $("select#quem_impede5").select2();
                                    $("button#inserir_parentesco5").html("INSERIR");
                                    $("tbody#resultado_parentesco5").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco6").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede6").val();
        var parente_id = $("#parente_id6").val();

        if (formulario_validator_10("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco6.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos6.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id6").val("");
                                    $("select#quem_impede6").val("");
                                    $("select#quem_impede6").select2();
                                    $("button#inserir_parentesco6").html("INSERIR");
                                    $("tbody#resultado_parentesco6").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco7").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede7").val();
        var parente_id = $("#parente_id7").val();

        if (formulario_validator_11("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco7.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos7.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id7").val("");
                                    $("select#quem_impede7").val("");
                                    $("select#quem_impede7").select2();
                                    $("button#inserir_parentesco7").html("INSERIR");
                                    $("tbody#resultado_parentesco7").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco8").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede8").val();
        var parente_id = $("#parente_id8").val();

        if (formulario_validator_12("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco8.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos8.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id8").val("");
                                    $("select#quem_impede8").val("");
                                    $("select#quem_impede8").select2();
                                    $("button#inserir_parentesco8").html("INSERIR");
                                    $("tbody#resultado_parentesco8").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco9").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede9").val();
        var parente_id = $("#parente_id9").val();

        if (formulario_validator_13("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco9.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos9.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id9").val("");
                                    $("select#quem_impede9").val("");
                                    $("select#quem_impede9").select2();
                                    $("button#inserir_parentesco9").html("INSERIR");
                                    $("tbody#resultado_parentesco9").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco10").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede10").val();
        var parente_id = $("#parente_id10").val();

        if (formulario_validator_14("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco10.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos10.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id10").val("");
                                    $("select#quem_impede10").val("");
                                    $("select#quem_impede10").select2();
                                    $("button#inserir_parentesco10").html("INSERIR");
                                    $("tbody#resultado_parentesco10").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco11").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede11").val();
        var parente_id = $("#parente_id11").val();

        if (formulario_validator_15("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco11.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos11.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id11").val("");
                                    $("select#quem_impede11").val("");
                                    $("select#quem_impede11").select2();
                                    $("button#inserir_parentesco11").html("INSERIR");
                                    $("tbody#resultado_parentesco11").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_parentesco12").click(function () {

        var id = $("#familia_id").val();
        var quem_impede = $("#quem_impede12").val();
        var parente_id = $("#parente_id12").val();

        if (formulario_validator_16("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_parentesco12.php", {id: id, quem_impede: quem_impede, parente_id: parente_id}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Parentesco",
                        html: "O parente escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Parentesco",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_parentescos12.php",
                                {id: id},
                                function (valor) {
                                    $("input#parente_id12").val("");
                                    $("select#quem_impede12").val("");
                                    $("select#quem_impede12").select2();
                                    $("button#inserir_parentesco12").html("INSERIR");
                                    $("tbody#resultado_parentesco12").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_irmao").click(function () {

        var id = $("#familia_id").val();
        var nome = $("#nome_irmao").val();
        var idade = $("#idade_irmao").val();
        var data_acolhimento = $("#data_acolhimento").val();
        var obs = $("#descreva_observacao_irmaos_servico_acolhimento").val();

        var irmao_id = $("#irmao_id").val();

        if (formulario_validator_2("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_irmao.php", {id: id, nome: nome, irmao_id: irmao_id, idade: idade, data_acolhimento: data_acolhimento, obs: obs}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Irmão",
                        html: "O irmão escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Irmão",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_irmaos.php",
                                {id: id},
                                function (valor) {
                                    $("input#irmao_id").val("");
                                    $("select#nome_irmao").val("");
                                    $("select#nome_irmao").select2();
                                    $("input#idade_irmao").val("");
                                    $("input#data_acolhimento").val("");
                                    $("textarea#descreva_observacao_irmaos_servico_acolhimento").val("");
                                    $("button#inserir_irmao").html("INSERIR");
                                    $("tbody#resultado_irmaos").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_irmao2").click(function () {

        var id = $("#familia_id").val();
        var nome = $("#nome_irmao2").val();
        var idade = $("#idade_irmao2").val();
        var data_acolhimento = $("#data_acolhimento2").val();
        var obs = $("#descreva_observacao_irmaos_servico_acolhimento2").val();

        var irmao_id = $("#irmao_id2").val();

        if (formulario_validator_3("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_irmao2.php", {id: id, nome: nome, irmao_id: irmao_id, idade: idade, data_acolhimento: data_acolhimento, obs: obs}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Irmão",
                        html: "O irmão escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Irmão",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_irmaos2.php",
                                {id: id},
                                function (valor) {
                                    $("input#irmao_id2").val("");
                                    $("select#nome_irmao2").val("");
                                    $("select#nome_irmao2").select2();
                                    $("input#idade_irmao2").val("");
                                    $("input#data_acolhimento2").val("");
                                    $("textarea#descreva_observacao_irmaos_servico_acolhimento2").val("");
                                    $("button#inserir_irmao2").html("INSERIR");
                                    $("tbody#resultado_irmaos2").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_irmao3").click(function () {

        var id = $("#familia_id").val();
        var nome = $("#nome_irmao3").val();
        var idade = $("#idade_irmao3").val();
        var irmao_id = $("#irmao_id3").val();

        if (formulario_validator_4("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_irmao3.php", {id: id, nome: nome, irmao_id: irmao_id, idade: idade}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Irmão",
                        html: "O irmão escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Irmão",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_irmaos3.php",
                                {id: id},
                                function (valor) {
                                    $("input#irmao_id3").val("");
                                    $("select#nome_irmao3").val("");
                                    $("select#nome_irmao3").select2();
                                    $("input#idade_irmao3").val("");
                                    $("button#inserir_irmao3").html("INSERIR");
                                    $("tbody#resultado_irmaos3").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $("#inserir_irmao4").click(function () {

        var id = $("#familia_id").val();
        var nome = $("#nome_irmao4").val();
        var idade = $("#idade_irmao4").val();
        var irmao_id = $("#irmao_id4").val();

        if (formulario_validator_5("")) {
            $.post(PORTAL_URL + "admin/dao/pia/add_irmao4.php", {id: id, nome: nome, irmao_id: irmao_id, idade: idade}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Irmão",
                        html: "O irmão escolhido já está adicionado na lista!",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Irmão",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_irmaos4.php",
                                {id: id},
                                function (valor) {
                                    $("input#irmao_id4").val("");
                                    $("select#nome_irmao4").val("");
                                    $("select#nome_irmao4").select2();
                                    $("input#idade_irmao4").val("");
                                    $("button#inserir_irmao4").html("INSERIR");
                                    $("tbody#resultado_irmaos4").html(valor);
                                });
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var nome_parente = $("#nome_parente").val();
    var nascimento_parente = $("#nascimento_parente").val();
    var rg_parente = $("#rg_parente").val();
    var cpf_parente = $("#cpf_parente").val();
    var celular_parente = $("#celular_parente").val();
    var endereco = $("#endereco").val();
    var ocupacao_parente = $("#ocupacao_parente").val();
    var grau_parentesco = $("#grau_parentesco").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (ocupacao_parente == "") {
            $('div#div_ocupacao_parente').after('<label id="erro_ocupacao_parente" class="error">Ocupação é obrigatório.</label>');
            valido = false;
            element = $('div#div_ocupacao_parente');
        }

        if (endereco == "") {
            $('div#div_endereco').after('<label id="erro_endereco" class="error">Endereço é obrigatório.</label>');
            valido = false;
            element = $('div#div_endereco');
        }

        if (celular_parente == "") {
            $('div#div_celular_parente').after('<label id="erro_celular_parente" class="error">Celular é obrigatório.</label>');
            valido = false;
            element = $('div#div_rcelular_parente');
        }

        if (cpf_parente == "") {
            $('div#div_cpf_parente').after('<label id="erro_cpf_parente" class="error">CPF é obrigatório.</label>');
            valido = false;
            element = $('div#div_cpf_parente');
        }

        if (rg_parente == "") {
            $('div#div_rg_parente').after('<label id="erro_rg_parente" class="error">RG é obrigatório.</label>');
            valido = false;
            element = $('div#div_rg_parente');
        }

        if (nascimento_parente == "") {
            $('div#div_nascimento_parente').after('<label id="erro_nascimento_parente" class="error">Nascimento é obrigatório.</label>');
            valido = false;
            element = $('div#div_nascimento_parente');
        }

        if (nome_parente == "") {
            $('div#div_nome_parente').after('<label id="erro_nome_parente" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_parente');
        }

        if (grau_parentesco == "") {
            $('div#div_grau_parentesco').after('<label id="erro_grau_parentesco" class="error">Grau de parentesco é obrigatório.</label>');
            valido = false;
            element = $('div#div_grau_parentesco');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_2(obj) {
    var valido = true;

    var nome_irmao = $("#nome_irmao").val();
    var idade_irmao = $("#idade_irmao").val();
    var data_acolhimento = $("#data_acolhimento").val();
    var descreva_observacao_irmaos_servico_acolhimento = $("#descreva_observacao_irmaos_servico_acolhimento").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (descreva_observacao_irmaos_servico_acolhimento == "") {
            $('div#div_descreva_observacao_irmaos_servico_acolhimento').after('<label id="erro_descreva_observacao_irmaos_servico_acolhimento" class="error">Observação é obrigatório.</label>');
            valido = false;
            element = $('div#div_descreva_observacao_irmaos_servico_acolhimento');
        }

        if (data_acolhimento == "") {
            $('div#div_data_acolhimento').after('<label id="erro_data_acolhimento" class="error">Data de acolhimento é obrigatório.</label>');
            valido = false;
            element = $('div#div_data_acolhimento');
        }

        if (idade_irmao == "") {
            $('div#div_idade_irmao').after('<label id="erro_idade_irmao" class="error">Idade é obrigatório.</label>');
            valido = false;
            element = $('div#div_idade_irmao');
        }

        if (nome_irmao == "") {
            $('div#div_nome_irmao').after('<label id="erro_nome_irmao" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_irmao');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_3(obj) {
    var valido = true;

    var nome_irmao = $("#nome_irmao2").val();
    var idade_irmao = $("#idade_irmao2").val();
    var data_acolhimento = $("#data_acolhimento2").val();
    var descreva_observacao_irmaos_servico_acolhimento = $("#descreva_observacao_irmaos_servico_acolhimento2").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (descreva_observacao_irmaos_servico_acolhimento == "") {
            $('div#div_descreva_observacao_irmaos_servico_acolhimento2').after('<label id="erro_descreva_observacao_irmaos_servico_acolhimento2" class="error">Observação é obrigatório.</label>');
            valido = false;
            element = $('div#div_descreva_observacao_irmaos_servico_acolhimento2');
        }

        if (data_acolhimento == "") {
            $('div#div_data_acolhimento2').after('<label id="erro_data_acolhimento2" class="error">Data de acolhimento é obrigatório.</label>');
            valido = false;
            element = $('div#div_data_acolhimento2');
        }

        if (idade_irmao == "") {
            $('div#div_idade_irmao2').after('<label id="erro_idade_irmao2" class="error">Idade é obrigatório.</label>');
            valido = false;
            element = $('div#div_idade_irmao2');
        }

        if (nome_irmao == "") {
            $('div#div_nome_irmao2').after('<label id="erro_nome_irmao2" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_irmao2');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_4(obj) {
    var valido = true;

    var nome_irmao = $("#nome_irmao3").val();
    var idade_irmao = $("#idade_irmao3").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (idade_irmao == "") {
            $('div#div_idade_irmao3').after('<label id="erro_idade_irmao3" class="error">Idade é obrigatório.</label>');
            valido = false;
            element = $('div#div_idade_irmao3');
        }

        if (nome_irmao == "") {
            $('div#div_nome_irmao3').after('<label id="erro_nome_irmao3" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_irmao3');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_5(obj) {
    var valido = true;

    var nome_irmao = $("#nome_irmao4").val();
    var idade_irmao = $("#idade_irmao4").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (idade_irmao == "") {
            $('div#div_idade_irmao4').after('<label id="erro_idade_irmao4" class="error">Idade é obrigatório.</label>');
            valido = false;
            element = $('div#div_idade_irmao4');
        }

        if (nome_irmao == "") {
            $('div#div_nome_irmao4').after('<label id="erro_nome_irmao4" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_irmao4');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_6(obj) {
    var valido = true;

    var quem_impede2 = $("#quem_impede2").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede2 == "") {
            $('div#div_quem_impede2').after('<label id="erro_quem_impede2" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede2');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_7(obj) {
    var valido = true;

    var quem_impede3 = $("#quem_impede3").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede3 == "") {
            $('div#div_quem_impede3').after('<label id="erro_quem_impede3" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede3');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_8(obj) {
    var valido = true;

    var quem_impede4 = $("#quem_impede4").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede4 == "") {
            $('div#div_quem_impede4').after('<label id="erro_quem_impede4" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede4');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_9(obj) {
    var valido = true;

    var quem_impede5 = $("#quem_impede5").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede5 == "") {
            $('div#div_quem_impede5').after('<label id="erro_quem_impede5" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede5');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_10(obj) {
    var valido = true;

    var quem_impede6 = $("#quem_impede6").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede6 == "") {
            $('div#div_quem_impede6').after('<label id="erro_quem_impede6" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede6');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_11(obj) {
    var valido = true;

    var quem_impede7 = $("#quem_impede7").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede7 == "") {
            $('div#div_quem_impede7').after('<label id="erro_quem_impede7" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede7');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_12(obj) {
    var valido = true;

    var quem_impede8 = $("#quem_impede8").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede8 == "") {
            $('div#div_quem_impede8').after('<label id="erro_quem_impede8" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede8');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_13(obj) {
    var valido = true;

    var quem_impede9 = $("#quem_impede9").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede9 == "") {
            $('div#div_quem_impede9').after('<label id="erro_quem_impede9" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede9');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_14(obj) {
    var valido = true;

    var quem_impede10 = $("#quem_impede10").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede10 == "") {
            $('div#div_quem_impede10').after('<label id="erro_quem_impede10" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede10');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_15(obj) {
    var valido = true;

    var quem_impede11 = $("#quem_impede11").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede11 == "") {
            $('div#div_quem_impede11').after('<label id="erro_quem_impede11" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede11');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator_16(obj) {
    var valido = true;

    var quem_impede12 = $("#quem_impede12").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (quem_impede12 == "") {
            $('div#div_quem_impede12').after('<label id="erro_quem_impede12" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_quem_impede12');
        }

    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------
function editar(id, grau, nome, nascimento, rg, cpf, celular, endereco, ocupacao) {
    $("input#parente_id").val(id);
    $("select#grau_parentesco").val(grau);
    $("select#grau_parentesco").select2();
    $("input#nome_parente").val(nome);
    $("input#nascimento_parente").val(nascimento);
    $("input#rg_parente").val(rg);
    $("input#cpf_parente").val(cpf);
    $("input#celular_parente").val(celular);
    $("input#endereco").val(endereco);
    $("input#ocupacao_parente").val(ocupacao);
    $("button#inserir_parentesco").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar2(id, nome) {
    $("input#parente_id2").val(id);
    $("select#quem_impede2").val(nome);
    $("select#quem_impede2").select2();
    $("button#inserir_parentesco2").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar3(id, nome) {
    $("input#parente_id3").val(id);
    $("select#quem_impede3").val(nome);
    $("select#quem_impede3").select2();
    $("button#inserir_parentesco3").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar4(id, nome) {
    $("input#parente_id4").val(id);
    $("select#quem_impede4").val(nome);
    $("select#quem_impede4").select2();
    $("button#inserir_parentesco4").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar5(id, nome) {
    $("input#parente_id5").val(id);
    $("select#quem_impede5").val(nome);
    $("select#quem_impede5").select2();
    $("button#inserir_parentesco5").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar6(id, nome) {
    $("input#parente_id6").val(id);
    $("select#quem_impede6").val(nome);
    $("select#quem_impede6").select2();
    $("button#inserir_parentesco6").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar7(id, nome) {
    $("input#parente_id7").val(id);
    $("select#quem_impede7").val(nome);
    $("select#quem_impede7").select2();
    $("button#inserir_parentesco7").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar8(id, nome) {
    $("input#parente_id8").val(id);
    $("select#quem_impede8").val(nome);
    $("select#quem_impede8").select2();
    $("button#inserir_parentesco8").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar9(id, nome) {
    $("input#parente_id9").val(id);
    $("select#quem_impede9").val(nome);
    $("select#quem_impede9").select2();
    $("button#inserir_parentesco9").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar10(id, nome) {
    $("input#parente_id10").val(id);
    $("select#quem_impede10").val(nome);
    $("select#quem_impede10").select2();
    $("button#inserir_parentesco10").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar11(id, nome) {
    $("input#parente_id11").val(id);
    $("select#quem_impede11").val(nome);
    $("select#quem_impede11").select2();
    $("button#inserir_parentesco11").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar12(id, nome) {
    $("input#parente_id12").val(id);
    $("select#quem_impede12").val(nome);
    $("select#quem_impede12").select2();
    $("button#inserir_parentesco12").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar_irmao(id, nome, idade, data_acolhimento, obs) {
    $("input#irmao_id").val(id);
    $("select#nome_irmao").val(nome);
    $("select#nome_irmao").select2();
    $("input#idade_irmao").val(idade);
    $("input#data_acolhimento").val(data_acolhimento);
    $("textarea#descreva_observacao_irmaos_servico_acolhimento").val(obs);
    $("button#inserir_irmao").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar_irmao2(id, nome, idade, data_acolhimento, obs) {
    $("input#irmao_id2").val(id);
    $("select#nome_irmao2").val(nome);
    $("select#nome_irmao2").select2();
    $("input#idade_irmao2").val(idade);
    $("input#data_acolhimento2").val(data_acolhimento);
    $("textarea#descreva_observacao_irmaos_servico_acolhimento2").val(obs);
    $("button#inserir_irmao2").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar_irmao3(id, nome, idade) {
    $("input#irmao_id3").val(id);
    $("select#nome_irmao3").val(nome);
    $("select#nome_irmao3").select2();
    $("input#idade_irmao3").val(idade);
    $("button#inserir_irmao3").html("ALTERAR");
}
//------------------------------------------------------------------------------
function editar_irmao4(id, nome, idade) {
    $("input#irmao_id4").val(id);
    $("select#nome_irmao4").val(nome);
    $("select#nome_irmao4").select2();
    $("input#idade_irmao4").val(idade);
    $("button#inserir_irmao4").html("ALTERAR");
}
//------------------------------------------------------------------------------
function remover(obj, id) {
    swal({
        title: "Deseja mesmo remover este vínculo?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do vínculo novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend(obj),
            error: onSuccessSend(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {
    swal({
        title: "Sucesso!",
        text: "Vínculo removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id").val("");
        $("select#grau_parentesco").val("");
        $("input#nome_parente").val("");
        $("input#nascimento_parente").val("");
        $("input#rg_parente").val("");
        $("input#cpf_parente").val("");
        $("input#celular_parente").val("");
        $("input#endereco").val("");
        $("input#ocupacao_parente").val("");
        $("button#inserir_parentesco").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover2(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend6(obj),
            error: onSuccessSend6(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend6(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id2").val("");
        $("select#quem_impede2").val("");
        $("button#inserir_parentesco2").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover3(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend7(obj),
            error: onSuccessSend7(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend7(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id3").val("");
        $("select#quem_impede3").val("");
        $("button#inserir_parentesco3").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover4(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend8(obj),
            error: onSuccessSend8(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend8(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id4").val("");
        $("select#quem_impede4").val("");
        $("button#inserir_parentesco4").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover5(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend9(obj),
            error: onSuccessSend9(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend9(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id5").val("");
        $("select#quem_impede5").val("");
        $("button#inserir_parentesco5").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover6(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend10(obj),
            error: onSuccessSend10(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend10(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id6").val("");
        $("select#quem_impede6").val("");
        $("button#inserir_parentesco6").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover7(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend11(obj),
            error: onSuccessSend11(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend11(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id7").val("");
        $("select#quem_impede7").val("");
        $("button#inserir_parentesco7").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover8(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend12(obj),
            error: onSuccessSend12(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend12(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id8").val("");
        $("select#quem_impede8").val("");
        $("button#inserir_parentesco8").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover9(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend13(obj),
            error: onSuccessSend13(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend13(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id9").val("");
        $("select#quem_impede9").val("");
        $("button#inserir_parentesco9").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover10(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend14(obj),
            error: onSuccessSend14(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend14(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id10").val("");
        $("select#quem_impede10").val("");
        $("button#inserir_parentesco10").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover11(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend15(obj),
            error: onSuccessSend15(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend15(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id11").val("");
        $("select#quem_impede11").val("");
        $("button#inserir_parentesco11").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover12(obj, id) {
    swal({
        title: "Deseja mesmo remover este parentesco?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do parentesco novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_parentesco",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend16(obj),
            error: onSuccessSend16(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend16(obj) {
    swal({
        title: "Sucesso!",
        text: "Parentesco removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#parente_id12").val("");
        $("select#quem_impede12").val("");
        $("button#inserir_parentesco12").html("INSERIR");
        $(obj).parents('tr#remover_parente').remove();
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
function remover_irmao(obj, id) {
    swal({
        title: "Deseja mesmo remover este irmão?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do irmão novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode cancelar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_irmao",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend2(obj),
            error: onSuccessSend2(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend2(obj) {
    swal({
        title: "Sucesso!",
        text: "Irmão removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#irmao_id").val("");
        $("select#nome_irmao").val("");
        $("select#nome_irmao").select2();
        $("input#idade_irmao").val("");
        $("input#data_acolhimento").val("");
        $("textarea#descreva_observacao_irmaos_servico_acolhimento").val("");
        $("button#inserir_irmao").html("INSERIR");
        $(obj).parents('tr#remover_irmao').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover_irmao2(obj, id) {
    swal({
        title: "Deseja mesmo remover este irmão?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do irmão novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode cancelar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_irmao",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend3(obj),
            error: onSuccessSend3(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend3(obj) {
    swal({
        title: "Sucesso!",
        text: "Irmão removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#irmao_id2").val("");
        $("select#nome_irmao2").val("");
        $("select#nome_irmao2").select2();
        $("input#idade_irmao2").val("");
        $("input#data_acolhimento2").val("");
        $("textarea#descreva_observacao_irmaos_servico_acolhimento2").val("");
        $("button#inserir_irmao2").html("INSERIR");
        $(obj).parents('tr#remover_irmao').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover_irmao3(obj, id) {
    swal({
        title: "Deseja mesmo remover este irmão?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do irmão novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode cancelar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_irmao",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend4(obj),
            error: onSuccessSend4(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend4(obj) {
    swal({
        title: "Sucesso!",
        text: "Irmão removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#irmao_id3").val("");
        $("select#nome_irmao3").val("");
        $("select#nome_irmao3").select2();
        $("input#idade_irmao3").val("");
        $("button#inserir_irmao3").html("INSERIR");
        $(obj).parents('tr#remover_irmao').remove();
    });

    return false;
}
//------------------------------------------------------------------------------
function remover_irmao4(obj, id) {
    swal({
        title: "Deseja mesmo remover este irmão?",
        text: "Obs: Caso escolha remover não podera mais recuperar os dados do irmão novamente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode cancelar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover_irmao",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend5(obj),
            error: onSuccessSend5(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend5(obj) {
    swal({
        title: "Sucesso!",
        text: "Irmão removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#irmao_id4").val("");
        $("select#nome_irmao4").val("");
        $("select#nome_irmao4").select2();
        $("input#idade_irmao4").val("");
        $("button#inserir_irmao4").html("INSERIR");
        $(obj).parents('tr#remover_irmao').remove();
    });

    return false;
}
//------------------------------------------------------------------------------------------------------
function outra_situacao2(op) {
    if (op == 7) {
        $("#div_qual_outra_situacao").show();
    } else {
        $("#qual_outra_situacao").val("");
        $("#div_qual_outra_situacao").hide();
    }
}
//------------------------------------------------------------------------------------------------------
function validar(codigo) {
    swal({
        title: "Deseja mesmo confirmar a Etapa 5 Família?",
        text: "Obs: Caso escolha confirmar, o sistema irá identificar essa etapa como concluída!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode confirmar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/confirmar_acolhido",
            type: "POST",
            data: {id: codigo, op: 5},
            enctype: 'multipart/form-data',
            success: onSuccessSend33,
            error: onError33
        });
        return false;
    });
}
//------------------------------------------------------------------------------------------------------
function desvalidar(codigo) {
    swal({
        title: "Deseja mesmo desvalidar a Etapa 5 Família?",
        text: "Obs: Caso escolha desvalidar, o sistema irá identificar essa etapa como pendente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode desvalidar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/desvalidar_acolhido",
            type: "POST",
            data: {id: codigo, op: 5},
            enctype: 'multipart/form-data',
            success: onSuccessSend33,
            error: onError33
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend33(obj) {

    var codigo = $("#id").val();

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/familia/' + codigo);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError33(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------------------------------
function editar_responsavel() {
    $("div#responsavel_edite_1").hide();
    $("div#responsavel_edite_2").show();
}
//------------------------------------------------------------------------------
function gerar_pdf() {
    //pega o Html da DIV
    var divElements = document.getElementById("impressao").innerHTML;
    //pega o HTML de toda tag Body
    var oldPage = document.body.innerHTML;
    //Alterna o body 
    document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
    //Imprime o body atual
    window.print();
    //Retorna o conteudo original da página. 
    document.body.innerHTML = oldPage;
}
//------------------------------------------------------------------------------