//------------------------------------------------------------------------------
$('a#ativar').click(function () {
    var id = $(this).attr('rel');

    swal({
        title: 'Deseja mesmo ativar?',
        text: "Obs: Caso escolha ativar, o PIA vai retornar para lista de ativos!",
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Sim, pode ativar!',
        cancelButtonText: "Não",
        showLoaderOnConfirm: true,
        preConfirm: (motivo) => {

            return new Promise(function (resolve, reject) {
                if (motivo == "") {
                    reject('É necessário informar o motivo da ativação!');
                } else {
                    projetouniversal.util.getjson({
                        url: PORTAL_URL + "admin/dao/pia/ativar",
                        type: "POST",
                        data: {id: id, motivo: motivo},
                        enctype: 'multipart/form-data',
                        success: onSuccessSend,
                        error: onError
                    });
                    return false;
                }
            });
        },
        allowOutsideClick: () => !swal.isLoading()
    }).then((result) => {
        if (result.isConfirmed) {
            swal({
                title: `${result.value.login}'s avatar`,
                imageUrl: result.value.avatar_url
            });
        }
    });
});
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/cancelados');
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------