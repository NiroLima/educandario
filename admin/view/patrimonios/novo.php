<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT * 
                 FROM mod_patrimonio mp  
                 WHERE mp.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_patrimonio = $result->fetch(PDO::FETCH_ASSOC);

    $patrimonio_id = $dados_patrimonio['id'];
    $patrimonio_nome = $dados_patrimonio['nome'];
    $patrimonio_codigo = $dados_patrimonio['codigo'];
    $patrimonio_setor = $dados_patrimonio['setor_id'];
	$patrimonio_responsavel = $dados_patrimonio['responsavel_id'];
	$patrimonio_situacao = $dados_patrimonio['situacao'];
} else {
    $patrimonio_id = "";
    $patrimonio_nome = "";
    $patrimonio_codigo = "";
    $patrimonio_setor = "";
	$patrimonio_responsavel = "";
	$patrimonio_situacao = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/patrimonios/lista">Patrimônio</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_patrimonio" name="form_patrimonio" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $patrimonio_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-barcode-read"></i> <strong>NOVO PATRIMÔNIO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div id="div_patrimonio" class="form-group">
                                    <label for="patrimonio">PATRIMÔNIO</label>
                                    <input type="text" class="form-control" name="patrimonio" id="patrimonio" placeholder="Nome do patrimônio" value="<?= $patrimonio_nome; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="div_codigo" class="form-group">
                                    <label for="codigo">CÓDIGO</label>
                                    <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código do patrimônio" value="<?= $patrimonio_codigo; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_setor" class="form-group">
                                    <label for="setor">SETOR</label>
                                    <select name="setor" id="setor" class="form-control select2">
										<option value="">Selecione o setor</option>
                                        <?php
                                        $result8 = $db->prepare("SELECT nome, id 
                                                     FROM seg_setor
                                                     WHERE status = 1 
                                                     ORDER BY nome ASC");
                                        $result8->execute();
                                        while ($setor = $result8->fetch(PDO::FETCH_ASSOC)) {
                                            if ($patrimonio_setor == $setor['id']) {
                                                ?>
                                                <option selected="true" value='<?= $setor['id']; ?>'><?= $setor['nome']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value='<?= $setor['id']; ?>'><?= $setor['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="div_responsavel" class="form-group">
                                    <label for="responsavel">RESPONSÁVEL</label>
                                    <select name="responsavel" id="responsavel" class="form-control select2">
										   <option value="">Escolha o responsável</option>
											<?php
											$result8 = $db->prepare("SELECT nome, id 
														 FROM seg_usuario
														 WHERE status = 1 
														 ORDER BY nome ASC");
											$result8->execute();
											while ($responsavel = $result8->fetch(PDO::FETCH_ASSOC)) {
												if ($patrimonio_responsavel == $responsavel['id']) {
													?>
													<option selected="true" value='<?= $responsavel['id']; ?>'><?= $responsavel['nome']; ?></option>
													<?php
												} else {
													?>
													<option value='<?= $responsavel['id']; ?>'><?= $responsavel['nome']; ?></option>
													<?php
												}
											}
											?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_situacao" class="form-group">
                                    <label for="situacao">SITUAÇÃO</label>
                                    <select name="situacao" id="situacao" class="form-control select2">
                                        <option value="">Selecione a situação</option>
                                        <option <?= $patrimonio_situacao == 1 ? "selected='true'" : ""; ?> value="1">Em Utilização</option>
                                        <option <?= $patrimonio_situacao == 2 ? "selected='true'" : ""; ?> value="2">Inutilizável</option>
                                        <option <?= $patrimonio_situacao == 3 ? "selected='true'" : ""; ?> value="3">Baixado</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $patrimonio_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $patrimonio_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/patrimonios/novo.js"></script>