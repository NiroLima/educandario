<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT * 
     FROM mod_saude_terapias msc  
     WHERE msc.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_tabela = $result->fetch(PDO::FETCH_ASSOC);

    $terapia_id = $dados_tabela['id'];
    $tipo_terapia = $dados_tabela['tipo_terapia'];
    $data_terapia = $dados_tabela['data_terapia'];
    $hora_terapia = $dados_tabela['hora_terapia'];
    $obs = $dados_tabela['obs'];
    $crianca_id = $dados_tabela['crianca_id'];
    $acomp_id = $dados_tabela['acomp_id'];
    $preparativos = $dados_tabela['preparativos'];
    $profissional_id = $dados_tabela['profissional_id'];
    $unidade = $dados_tabela['unidade_id'];
    $endereco = $dados_tabela['endereco'];
    $numero = $dados_tabela['numero'];
    $bairro = $dados_tabela['bairro'];
    $contato = $dados_tabela['contato'];
    $diagnostico = $dados_tabela['diagnostico'];
    $avaliacao_inicial = $dados_tabela['avaliacao_inicial'];
    $avaliacao_final = $dados_tabela['avaliacao_final'];
    $diagnostico_inicial = $dados_tabela['diagnostico_inicial'];
    $diagnostico_final = $dados_tabela['diagnostico_final'];
    $obs_inicial = $dados_tabela['obs_inicial'];
    $obs_final = $dados_tabela['obs_final'];
} else {
    $terapia_id = "";
    $tipo_terapia = "";
    $data_terapia = "";
    $hora_terapia = "";
    $obs = "";
    $crianca_id = "";
    $acomp_id = "";
    $preparativos = "";
    $profissional_id = "";
    $unidade = "";
    $endereco = "";
    $numero = "";
    $bairro = "";
    $contato = "";
    $diagnostico = "";
    $avaliacao_inicial = "";
    $avaliacao_final = "";
    $diagnostico_inicial = "";
    $diagnostico_final = "";
    $obs_inicial = "";
    $obs_final = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/terapia/lista">Terapias</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_terapia" name="form_terapia" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $terapia_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-clipboard-user"></i> <strong>NOVA TERAPIA</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>QUAL O TIPO DA TERAPIA?</label>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_terapia == "" || $tipo_terapia == 1 ? "checked='true'" : ""; ?> name="opcao_retono_terapia" id="particular" value="1">
                                            <label for="particular">PARTICULAR</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_terapia == 2 ? "checked='true'" : ""; ?> name="opcao_retono_terapia" id="voluntario" value="2">
                                            <label for="voluntario">VOLUNTÁRIO</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_terapia == 3 ? "checked='true'" : ""; ?> name="opcao_retono_terapia" id="profissional_quadro" value="3">
                                            <label for="profissional_quadro">SUS - Serviço Único de Saúde</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- BOX DE INFOMAÇÕES DA TERAPIA -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>INFOMAÇÕES DA TERAPIA</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-3">
                                        <div id="div_data_terapia" class="form-group">
                                            <label for="data_terapia">DATA DA TERAPIA</label>
                                            <input class="form-control" name="data_terapia" id="data_terapia" type="date" value="<?= $data_terapia; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="div_hora_terapia" class="form-group">
                                            <label for="hora_terapia">HORA</label>
                                            <input class="form-control" type="time" name="hora_terapia" id="hora_terapia" value="<?= $hora_terapia; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_funcao" class="form-group">
                                            <label for="preparativos">ALTERAÇÃO DO QUADRO DE SAÚDE DA CRIANÇA</label>
                                            <textarea name="preparativos" id="preparativos" class="form-control" cols="30" rows="10"><?= $preparativos; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM BOX DE INFOMAÇÕES DA CONSULTA -->

                        <!-- UNIDADE DE SAÚDE -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>UNIDADE DE SAÚDE</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-8">
                                        <div id="div_unidade_saude" class="form-group">
                                            <label for="unidade_saude">UNIDADE DE SAÚDE</label>
                                            <select name="unidade_saude" id="unidade_saude" class="form-control select2">
                                                <option value="">Selecione a Unidade de Saúde</option>
                                                <?php
                                                $result = $db->prepare("SELECT *  
                                                    FROM mod_unidade_saude mus  
                                                    WHERE mus.status = 1
                                                    ORDER BY mus.nome");
                                                $result->execute();
                                                while ($unidades = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($unidade == $unidades['id']) {
                                                        ?>
                                                        <option contato="<?= $unidades['contato']; ?>" endereco="<?= $unidades['endereco']; ?>" numero="<?= $unidades['numero']; ?>" bairro="<?= $unidades['bairro']; ?>" selected="true" value="<?= $unidades['id']; ?>"><?= $unidades['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option contato="<?= $unidades['contato']; ?>" endereco="<?= $unidades['endereco']; ?>" numero="<?= $unidades['numero']; ?>" bairro="<?= $unidades['bairro']; ?>" value="<?= $unidades['id']; ?>"><?= $unidades['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_contato_unidade_saude" class="form-group">
                                            <label for="contato_unidade_saude">Contato</label>
                                            <input type="text" class="form-control" name="contato_unidade_saude" id="contato_unidade_saude" placeholder="Contato" value="<?= $contato; ?>" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <div id="div_endereco_unidade_saude" class="form-group">
                                            <label for="endereco_unidade_saude">ENDEREÇO</label>
                                            <input type="text" class="form-control" name="endereco_unidade_saude" id="endereco_unidade_saude" placeholder="Travessa, Rua, Avenida..." value="<?= $endereco; ?>" readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="div_numero_unidade_saude" class="form-group">
                                            <label for="numero_unidade_saude">Número</label>
                                            <input type="text" class="form-control" name="numero_unidade_saude" id="numero_unidade_saude" placeholder="Número" value="<?= $numero; ?>" readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_bairro_unidade_saude" class="form-group">
                                            <label for="bairro_unidade_saude">BAIRRO</label>
                                            <input type="text" class="form-control" name="bairro_unidade_saude" id="bairro_unidade_saude" placeholder="Bairro" value="<?= $bairro; ?>" readonly="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM UNIDADE DE SAÚDE -->

                        <!-- BOX DE PACIENTE -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>PACIENTE</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-10">
                                        <div id="div_nome_crianca" class="form-group">
                                            <label for="nome_crianca">NOME DA CRIANÇA</label>
                                            <select name="nome_crianca" id="nome_crianca" class="form-control select2">
                                                <option rel2="" value="">Selecione o nome da Criança</option>
                                                <?php
                                                $idade = "";
                                                $result = $db->prepare("SELECT *  
                                                    FROM mod_acolhimento_crianca mac  
                                                    WHERE mac.status = 1
                                                    ORDER BY mac.nome");
                                                $result->execute();
                                                while ($crianca = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($crianca_id == $crianca['id']) {
                                                        $idade = calcular_idade($crianca['nascimento']);
                                                        ?>
                                                        <option rel2="<?= $idade; ?>" selected="true" value="<?= $crianca['id']; ?>"><?= $crianca['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option rel2="<?= calcular_idade($crianca['nascimento']); ?>" value="<?= $crianca['id']; ?>"><?= $crianca['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="div_idade_crianca" class="form-group">
                                            <label for="idade_crianca">IDADE</label>
                                            <input readonly="true" type="text" class="form-control" name="idade_crianca" id="idade_crianca" placeholder="Idade da Criança" value="<?= $idade; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM BOX DE PACIENTE -->


                        <br>

                        <!-- BOX DE DIAGNOSTICO INICIAL -->
                        <div id='div_diagnostico' class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>AVALIAÇÃO INICIAL</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_avaliacao_inicial" class="form-group">
                                            <label for="avaliacao_inicial">AVALIAÇÃO INICIAL</label>
                                            <textarea name="avaliacao_inicial" id="avaliacao_inicial" class="form-control" cols="30" rows="10"><?= $avaliacao_inicial ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_diagnostico_inicial" class="form-group">
                                            <label for="diagnostico_inicial">DIAGNÓSTICO INICIAL</label>
                                            <textarea name="diagnostico_inicial" id="diagnostico_inicial" class="form-control" cols="30" rows="10"><?= $diagnostico_inicial ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_observacoes_inicial" class="form-group">
                                            <label for="observacoes_inicial">OBSERVAÇÕES INICIAIS</label>
                                            <textarea name="observacoes_inicial" id="observacoes_inicial" class="form-control" cols="30" rows="10"><?= $obs_inicial ?></textarea>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <!-- FIM BOX DE DIAGNOSTICO INICIAL -->

                        <br>

                        <!-- BOX DE SESSÃO TERAPIA -->
                        <div id='div_diagnostico2' class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>SESSÕES DE TERAPIA</strong>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-2">
                                        <thead class="thead-light">
                                            <tr>
                                                <th width="5%" scope="col">SESSÃO</th>
                                                <th width="10%" scope="col">DATA</th>
                                                <th width="20%" scope="col">PROFISSIONAL</th>
                                                <th width="20%" scope="col">ACOMPANHANTE</th>
                                                <th scope="col">OBSERVAÇÃO</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tbody id="resultado_diagnostico">
                                            <?php
                                            $stmp = $db->prepare("SELECT mscd.id, mscd.data_diagnostico, mscd.profissional_id, mscd.acompanhante_id, mscd.obs, sf.nome AS especialidade, sf2.nome AS funcao            
                                                                  FROM mod_saude_terapia_diagnostico mscd 
                                                                  LEFT JOIN seg_profissional AS sp ON sp.id = mscd.profissional_id 
                                                                  LEFT JOIN seg_funcao AS sf ON sf.id = sp.funcao_id 
                                                                  LEFT JOIN seg_usuario AS su ON su.id = mscd.acompanhante_id
                                                                  LEFT JOIN seg_funcao AS sf2 ON sf2.id = su.funcao_id  
                                                                  WHERE mscd.terapia_id = ? OR mscd.terapia_id IS NULL AND mscd.responsavel_id = ?
                                                                  GROUP BY mscd.id 
                                                                  ORDER BY mscd.id ASC");
                                            $stmp->bindValue(1, $id);
                                            $stmp->bindValue(2, $_SESSION['id']);
                                            $stmp->execute();
                                            while ($diag = $stmp->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <tr id="remover_diagnostico">
                                                    <th class="text-center"><?= $diag['id'] ?></th>
                                                    <td><?= obterDataBRTimestamp($diag['data_diagnostico']) ?></td>
                                                    <td><?= pesquisar("nome", "seg_profissional", "id", "=", $diag['profissional_id'], ""); ?></td>
                                                    <td><?= pesquisar("nome", "seg_usuario", "id", "=", $diag['acompanhante_id'], "") ?></td>
                                                    <td><?= $diag['obs'] ?></td>
                                                    <td width="100px">
                                                        <a id="editar" onclick="editar(<?= $diag['id'] ?>, '<?= convertDataBR2ISO(obterDataBRTimestamp($diag['data_diagnostico'])) ?>', '<?= $diag['profissional_id'] ?>', '<?= $diag['especialidade'] ?>', '<?= $diag['acompanhante_id'] ?>', '<?= $diag['funcao'] ?>', '<?= $diag['obs'] ?>')" style="cursor: pointer" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                        <a id="remover" onclick="remover(this, <?= $diag['id'] ?>)" style="cursor: pointer" class="text-danger"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <hr>
                                <div class="row mt-3">
                                    <div class="col-md-3">
                                        <div id="div_data_diagnostico" class="form-group">
                                            <label for="data_diagnostico">DATA</label>
                                            <input type="hidden" id="diagnostico_id" name="diagnostico_id" value=""/>
                                            <input class="form-control" name="data_diagnostico" id="data_diagnostico" type="date" value="" />
                                        </div>
                                    </div>
                                </div>

                                <!-- BOX DE PROFISSIONAL -->
                                <div class="box box-outline-primary mt-3">
                                    <div class="box-header">
                                        <strong>PROFISSIONAL</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-8">
                                                <div id="div_nome_profissional" class="form-group">
                                                    <label for="nome_profissional">NOME PROFISSIONAL</label>
                                                    <select name="nome_profissional" id="nome_profissional" class="form-control select2">
                                                        <option rel2="" value="">Selecione a especialidade</option>
                                                        <?php
                                                        $especialidade_prof = "";
                                                        $result = $db->prepare("SELECT su.id, su.nome, sf.nome AS funcao   
                                                            FROM seg_profissional su  
                                                            LEFT JOIN seg_funcao AS sf ON sf.id = su.funcao_id 
                                                            WHERE su.status = 1 
                                                            ORDER BY su.nome");
                                                        $result->execute();
                                                        while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
                                                            ?>
                                                            <option rel2="<?= $prof['funcao']; ?>" value="<?= $prof['id']; ?>"><?= $prof['nome']; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div id="div_especialidade" class="form-group">
                                                    <label for="especialidade">ESPECIALIDADE</label>
                                                    <input type="text" readonly="true" class="form-control" name="especialidade" id="especialidade" placeholder="Especialidade" value="<?= $especialidade_prof; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIM BOX DE PROFISSIONAL -->

                                <!-- BOX DE ACOMPANHANTE -->
                                <div class="box box-outline-primary mt-3">
                                    <div class="box-header">
                                        <strong>ACOMPANHANTE</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-3">
                                            <div class="col-md-8">
                                                <div id="div_nome_acompanhante" class="form-group">
                                                    <label for="nome_acompanhante">NOME DO ACOMPANHANTE</label>
                                                    <select name="nome_acompanhante" id="nome_acompanhante" class="form-control select2">
                                                        <option rel2="" value="">Selecione o nome do Acompanhante</option>
                                                        <?php
                                                        $funcao_acomp = "";
                                                        $result = $db->prepare("SELECT su.id, su.nome, sf.nome AS funcao   
                                                            FROM seg_usuario su  
                                                            LEFT JOIN seg_funcao AS sf ON sf.id = su.funcao_id 
                                                            WHERE su.status = 1 
                                                            ORDER BY su.nome");
                                                        $result->execute();
                                                        while ($acompanhante = $result->fetch(PDO::FETCH_ASSOC)) {
                                                            ?>
                                                            <option rel2="<?= $acompanhante['funcao']; ?>" value="<?= $acompanhante['id']; ?>"><?= $acompanhante['nome']; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div id="div_funcao" class="form-group">
                                                    <label for="funcao">FUNÇÃO</label>
                                                    <input readonly="true" type="text" class="form-control" name="funcao" id="funcao" placeholder="Função" value="<?= $funcao_acomp; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- FIM BOX DE ACOMPANHANTE -->

                                <br>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_observacao_sessao" class="form-group">
                                            <label for="observacao_sessao">OBSERVAÇÃO</label>
                                            <textarea name="observacao_sessao" id="observacao_sessao" class="form-control" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="row mt-3 mb-2">
                                    <div class="col-md-12 text-center">
                                        <button type="button" id="add" name="add" class="btn btn-success">MARCAR</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM BOX DE SESSOES DE TERAPIA -->

                        <br>

                        <!-- BOX DE DIAGNOSTICO FINAL -->
                        <div id='div_diagnostico' class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>AVALIAÇÃO FINAL</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_avaliacao_final" class="form-group">
                                            <label for="avaliacao_final">AVALIAÇÃO FINAL</label>
                                            <textarea name="avaliacao_final" id="avaliacao_final" class="form-control" cols="30" rows="10"><?= $avaliacao_final ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_diagnostico_final" class="form-group">
                                            <label for="diagnostico_final">DIAGNÓSTICO FINAL</label>
                                            <textarea name="diagnostico_final" id="diagnostico_final" class="form-control" cols="30" rows="10"><?= $diagnostico_final ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_observacoes_final" class="form-group">
                                            <label for="observacoes_final">OBSERVAÇÕES</label>
                                            <textarea name="observacoes_final" id="observacoes_final" class="form-control" cols="30" rows="10"><?= $obs_final ?></textarea>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <!-- FIM BOX DE DIAGNOSTICO INICIAL -->

                        <br>

                        <div class="row mt-2">
                            <div class="col-md-12">
                                <!-- ENCAMINHAMENTO -->
                                <div class="box box-outline-primary mt-3">
                                    <div class="box-header">
                                        <strong>ENCAMINHAMENTO</strong>
                                    </div>
                                    <div class="box-body">
                                        <div class="row mt-4 mb-4">
                                            <!-- /.col -->
                                            <div class="col-md-12 col-12">
                                                <div id="div_especialistas" class="form-group">
                                                    <label>ESPECIALISTA(S)</label>
                                                    <select class="form-control select2" id="especialistas" name="especialistas[]" multiple="multiple" data-placeholder="Selecione o Especialista(s)" style="width: 100%;">
                                                        <?php
                                                        if ($terapia_id != "" && is_numeric($terapia_id)) {
                                                            $result = $db->prepare("SELECT msv.id, msv.nome     
                                                    FROM mod_saude_especialidade msv 
                                                    LEFT JOIN mod_saude_terapia_especialistas AS mscv ON mscv.especialista_id = msv.id 
                                                    WHERE mscv.terapia_id = ?  
                                                    ORDER BY msv.nome");
                                                            $result->bindValue(1, $terapia_id);
                                                            $result->execute();
                                                            while ($especialistas = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <option selected="true" value="<?= $especialistas['id']; ?>"><?= $especialistas['nome']; ?></option>
                                                                <?php
                                                            }
                                                            $result2 = $db->prepare("SELECT msv.id, msv.nome     
                                                    FROM mod_saude_especialidade msv 
                                                    WHERE msv.status = 1 AND msv.id NOT IN(SELECT especialista_id FROM mod_saude_terapia_especialistas WHERE terapia_id = ?)
                                                    ORDER BY msv.nome");
                                                            $result2->bindValue(1, $terapia_id);
                                                            $result2->execute();
                                                            while ($especialistas2 = $result2->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <option value="<?= $especialistas2['id']; ?>"><?= $especialistas2['nome']; ?></option>
                                                                <?php
                                                            }
                                                        } else {
                                                            $result = $db->prepare("SELECT id, nome  
                                                                                    FROM mod_saude_especialidade  
                                                                                    WHERE status = 1
                                                                                    ORDER BY nome");
                                                            $result->execute();
                                                            while ($especialistas = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                ?>
                                                                <option value="<?= $especialistas['id']; ?>"><?= $especialistas['nome']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                    </div>
                                </div>
                                <!-- FIM ENCAMINHAMENTO -->
                            </div>
                        </div>

                    </div>
                </div>

                <!-- FIM PERFIL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $terapia_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">INSERIR</button>
                        <button <?= $terapia_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/terapia/novo.js"></script>