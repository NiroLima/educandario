<?php

//------------------------------------------------------------------------------
include_once('../../../conf/config.php');
$db = Conexao::getInstance();

$id = $_POST['id'];

$stmp = $db->prepare("SELECT *           
                     FROM mod_acolhimento_crianca_servico macs   
                     WHERE macs.acolhimento_id = ?");
$stmp->bindValue(1, $id);
$stmp->execute();

if ($stmp->rowCount() == 0) {
    
} else {
    while ($servicos = $stmp->fetch(PDO::FETCH_ASSOC)) {
        echo '<tr id="remover_servico">
                     <td>' . $servicos['medico'] . '</td>
                     <td>' . $servicos['medicamento'] . '</td>
                     <td>' . $servicos['posologia'] . '</td>
                     <td>' . $servicos['administracao'] . '</td>
                     <td width="100px">
                        <a style="cursor: pointer" onclick="editar(' . $servicos['id'] . ', ' . "'" . $servicos['medico'] . "'" . ', ' . "'" . $servicos['medicamento'] . "'" . ', ' . "'" . $servicos['posologia'] . "'" . ', ' . "'" . $servicos['administracao'] . "'" . ')" class="text-warning"><i class="fa fa-pencil"></i></a>
                        <a style="cursor: pointer" onclick="remover(this, ' . $servicos['id'] . ')" class="text-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>';
    }
}
//------------------------------------------------------------------------------
?>

