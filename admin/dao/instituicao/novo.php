<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$nome = isset($_POST['nome']) ? $_POST['nome'] : NULL;

$contato = isset($_POST['contato']) ? $_POST['contato'] : NULL;
$endereco = isset($_POST['endereco']) ? $_POST['endereco'] : NULL;
$numero = isset($_POST['numero']) ? $_POST['numero'] : NULL;
$bairro = isset($_POST['bairro']) ? $_POST['bairro'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "seg_instituicao", "nome", "=", $nome, "");

if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome da instituição informado já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO seg_instituicao (nome, contato, endereco, numero, bairro, data_update, usuario_id, data_cadastro, status) VALUES (?, ?, ?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $nome);
            $sql->bindValue(2, $contato);
            $sql->bindValue(3, $endereco);
            $sql->bindValue(4, $numero);
            $sql->bindValue(5, $bairro);
            $sql->bindValue(6, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE seg_instituicao SET nome = ?, contato = ?, endereco = ?, numero = ?, bairro = ?, usuario_id = ?  WHERE id = ?");
            $sql->bindValue(1, $nome);
            $sql->bindValue(2, $contato);
            $sql->bindValue(3, $endereco);
            $sql->bindValue(4, $numero);
            $sql->bindValue(5, $bairro);
            $sql->bindValue(6, $_SESSION['id']);
            $sql->bindValue(7, $id);
            $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>