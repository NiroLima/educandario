<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<?php
$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT * 
     FROM mod_saude_consultas msc  
     WHERE msc.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_tabela = $result->fetch(PDO::FETCH_ASSOC);

    $consulta_id = $dados_tabela['id'];
    $tipo_atendimento = $dados_tabela['tipo_atendimento'];
    $tipo = $dados_tabela['tipo'];
    $tipo_consulta = $dados_tabela['tipo_consulta'];
    $data_consulta = $dados_tabela['data_consulta'];
    $hora_consulta = $dados_tabela['hora_consulta'];
    $obs = $dados_tabela['obs'];
    $crianca_id = $dados_tabela['crianca_id'];
    $acomp_id = $dados_tabela['acomp_id'];
    $preparativos = $dados_tabela['preparativos'];
    $profissional_id = $dados_tabela['profissional_id'];
    $unidade = $dados_tabela['unidade_id'];
    $endereco = $dados_tabela['endereco'];
    $numero = $dados_tabela['numero'];
    $bairro = $dados_tabela['bairro'];
    $contato = $dados_tabela['contato'];
    $retorno_consulta = $dados_tabela['retorno_consulta'];
    $data_retorno = $dados_tabela['data_retorno'];
    $hora_retorno = $dados_tabela['hora_retorno'];
    $data_agendada = $dados_tabela['data_agendada'];
} else {
    $consulta_id = "";
    $tipo_atendimento = "";
    $tipo = "";
    $tipo_consulta = "";
    $data_consultav = "";
    $hora_consulta = "";
    $obs = "";
    $crianca_id = "";
    $acomp_id = "";
    $preparativos = "";
    $profissional_id = "";
    $unidade = "";
    $endereco = "";
    $numero = "";
    $bairro = "";
    $contato = "";
    $retorno_consulta = "";
    $data_retorno = "";
    $hora_retorno = "";
    $data_agendada = "";
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/consultas/lista">Consultas</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <form id="form_consulta" name="form_consulta" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $consulta_id ?>"/>
                <!-- PERFIL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-clipboard-user"></i> <strong>NOVA CONSULTA</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>QUAL TIPO DE ATENDIMENTO?</label>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_atendimento == "" || $tipo_atendimento == 1 ? "checked='true'" : ""; ?> name="opcao_retono_atendimento" id="atendimento_medico" value="1">
                                            <label for="atendimento_medico">ATENDIMENTO MÉDICO</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_atendimento == 2 ? "checked='true'" : ""; ?> name="opcao_retono_atendimento" id="atendomento_odontologico" value="2">
                                            <label for="atendomento_odontologico">ATENDIMENTO ODONTOLÓGICO</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_atendimento == 3 ? "checked='true'" : ""; ?> name="opcao_retono_atendimento" id="vacinacao" value="3">
                                            <label for="vacinacao">VACINAÇÃO</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_atendimento == 4 ? "checked='true'" : ""; ?> name="opcao_retono_atendimento" id="cirurgia" value="4">
                                            <label for="cirurgia">CIRURGIA</label>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_atendimento == 5 ? "checked='true'" : ""; ?> name="opcao_retono_atendimento" id="urgencia" value="5">
                                            <label for="urgencia">URGÊNCIA/EMERGÊNCIA</label>
                                        </div>
                                        <!-- QUANDO FOR VACINAÇÃO SUMIR COM: QUAL TIPO, QUAL TIPO DE CONSULTA, PROFISSIONAL, EXAMES -->
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>QUAL O TIPO DA CONSULTA?</label>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_consulta == "" || $tipo_consulta == 1 ? "checked='true'" : ""; ?> name="opcao_retono_consulta" id="particular" value="1">
                                            <label for="particular">PARTICULAR</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_consulta == 2 ? "checked='true'" : ""; ?> name="opcao_retono_consulta" id="voluntario" value="2">
                                            <label for="voluntario">VOLUNTÁRIO</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $tipo_consulta == 3 ? "checked='true'" : ""; ?> name="opcao_retono_consulta" id="profissional_quadro" value="3">
                                            <label for="profissional_quadro">SUS - Serviço Único de Saúde</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- BOX DE INFOMAÇÕES DA CONSULTA -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>INFOMAÇÕES DA CONSULTA</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-3">
                                        <div id="div_data_consulta" class="form-group">
                                            <label for="data_consulta">DATA DA CONSULTA</label>
                                            <input class="form-control" name="data_consulta" id="data_consulta" type="date" value="<?= $data_consulta; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="div_hora_consulta" class="form-group">
                                            <label for="hora_consulta">HORA DA CONSULTA</label>
                                            <input class="form-control" type="time" name="hora_consulta" id="hora_consulta" value="<?= $hora_consulta; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_funcao" class="form-group">
                                            <label for="preparativos">ALTERAÇÃO DO QUADRO DE SAÚDE DA CRIANÇA</label>
                                            <textarea name="preparativos" id="preparativos" class="form-control" cols="30" rows="10"><?= $preparativos; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_observacao" class="form-group">
                                            <label for="observacao">OBSERVAÇÃO</label>
                                            <textarea name="observacao" id="observacao" class="form-control" cols="30" rows="10"><?= $obs; ?></textarea>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <!-- FIM BOX DE INFOMAÇÕES DA CONSULTA -->

                        <!-- UNIDADE DE SAÚDE -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>UNIDADE DE SAÚDE</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-8">
                                        <div id="div_unidade_saude" class="form-group">
                                            <label for="unidade_saude">UNIDADE DE SAÚDE</label>
                                            <select name="unidade_saude" id="unidade_saude" class="form-control select2">
                                                <option value="">Selecione a Unidade de Saúde</option>
                                                <?php
                                                $result = $db->prepare("SELECT *  
                                                    FROM mod_unidade_saude mus  
                                                    WHERE mus.status = 1
                                                    ORDER BY mus.nome");
                                                $result->execute();
                                                while ($unidades = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($unidade == $unidades['id']) {
                                                        ?>
                                                        <option contato="<?= $unidades['contato']; ?>" endereco="<?= $unidades['endereco']; ?>" numero="<?= $unidades['numero']; ?>" bairro="<?= $unidades['bairro']; ?>" selected="true" value="<?= $unidades['id']; ?>"><?= $unidades['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option contato="<?= $unidades['contato']; ?>" endereco="<?= $unidades['endereco']; ?>" numero="<?= $unidades['numero']; ?>" bairro="<?= $unidades['bairro']; ?>" value="<?= $unidades['id']; ?>"><?= $unidades['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_contato_unidade_saude" class="form-group">
                                            <label for="contato_unidade_saude">Contato</label>
                                            <input type="text" class="form-control" name="contato_unidade_saude" id="contato_unidade_saude" placeholder="Contato" value="<?= $contato; ?>" readonly="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <div id="div_endereco_unidade_saude" class="form-group">
                                            <label for="endereco_unidade_saude">ENDEREÇO</label>
                                            <input type="text" class="form-control" name="endereco_unidade_saude" id="endereco_unidade_saude" placeholder="Travessa, Rua, Avenida..." value="<?= $endereco; ?>" readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="div_numero_unidade_saude" class="form-group">
                                            <label for="numero_unidade_saude">Número</label>
                                            <input type="text" class="form-control" name="numero_unidade_saude" id="numero_unidade_saude" placeholder="Número" value="<?= $numero; ?>" readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_bairro_unidade_saude" class="form-group">
                                            <label for="bairro_unidade_saude">BAIRRO</label>
                                            <input type="text" class="form-control" name="bairro_unidade_saude" id="bairro_unidade_saude" placeholder="Bairro" value="<?= $bairro; ?>" readonly="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM UNIDADE DE SAÚDE -->

                        <!-- BOX DE PACIENTE -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>PACIENTE</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-10">
                                        <div id="div_nome_crianca" class="form-group">
                                            <label for="nome_crianca">NOME DA CRIANÇA</label>
                                            <select name="nome_crianca" id="nome_crianca" class="form-control select2">
                                                <option rel2="" value="">Selecione o nome da Criança</option>
                                                <?php
                                                $idade = "";
                                                $result = $db->prepare("SELECT *  
                                                    FROM mod_acolhimento_crianca mac  
                                                    WHERE mac.status = 1
                                                    ORDER BY mac.nome");
                                                $result->execute();
                                                while ($crianca = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($crianca_id == $crianca['id']) {
                                                        $idade = calcular_idade($crianca['nascimento']);
                                                        ?>
                                                        <option rel2="<?= $idade; ?>" selected="true" value="<?= $crianca['id']; ?>"><?= $crianca['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option rel2="<?= calcular_idade($crianca['nascimento']); ?>" value="<?= $crianca['id']; ?>"><?= $crianca['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="div_idade_crianca" class="form-group">
                                            <label for="idade_crianca">IDADE</label>
                                            <input readonly="true" type="text" class="form-control" name="idade_crianca" id="idade_crianca" placeholder="Idade da Criança" value="<?= $idade; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM BOX DE PACIENTE -->

                        <!-- BOX DE PACIENTE -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>ACOMPANHANTE</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-8">
                                        <div id="div_nome_acompanhante" class="form-group">
                                            <label for="nome_acompanhante">NOME DO ACOMPANHANTE</label>
                                            <select name="nome_acompanhante" id="nome_acompanhante" class="form-control select2">
                                                <option rel2="" value="">Selecione o nome do Acompanhante</option>
                                                <?php
                                                $funcao_acomp = "";
                                                $result = $db->prepare("SELECT su.id, su.nome, sf.nome AS funcao   
                                                    FROM seg_usuario su  
                                                    LEFT JOIN seg_funcao AS sf ON sf.id = su.funcao_id 
                                                    WHERE su.status = 1 
                                                    ORDER BY su.nome");
                                                $result->execute();
                                                while ($acompanhante = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($acomp_id == $acompanhante['id']) {
                                                        $funcao_acomp = $acompanhante['funcao'];
                                                        ?>
                                                        <option rel2="<?= $funcao_acomp; ?>" selected="true" value="<?= $acompanhante['id']; ?>"><?= $acompanhante['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option rel2="<?= $acompanhante['funcao']; ?>" value="<?= $acompanhante['id']; ?>"><?= $acompanhante['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_funcao" class="form-group">
                                            <label for="funcao">FUNÇÃO</label>
                                            <input readonly="true" type="text" class="form-control" name="funcao" id="funcao" placeholder="Função" value="<?= $funcao_acomp; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM BOX DE PACIENTE -->

                        <!-- BOX DE PROFISSIONAL -->
                        <div class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>PROFISSIONAL</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-8">
                                        <div id="div_nome_profissional" class="form-group">
                                            <label for="nome_profissional">NOME PROFISSIONAL</label>
                                            <select name="nome_profissional" id="nome_profissional" class="form-control select2">
                                                <option rel2="" value="">Selecione o profissional</option>
                                                <?php
                                                $especialidade_prof = "";
                                                $result = $db->prepare("SELECT su.id, su.nome, sf.nome AS funcao   
                                                    FROM seg_profissional su  
                                                    LEFT JOIN seg_funcao AS sf ON sf.id = su.funcao_id 
                                                    WHERE su.status = 1 
                                                    ORDER BY su.nome");
                                                $result->execute();
                                                while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
                                                    if ($profissional_id == $prof['id']) {
                                                        $especialidade_prof = $prof['funcao'];
                                                        ?>
                                                        <option rel2="<?= $especialidade_prof; ?>" selected="true" value="<?= $prof['id']; ?>"><?= $prof['nome']; ?></option>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option rel2="<?= $prof['funcao']; ?>" value="<?= $prof['id']; ?>"><?= $prof['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="div_especialidade" class="form-group">
                                            <label for="especialidade">ESPECIALIDADE</label>
                                            <input type="text" readonly="true" class="form-control" name="especialidade" id="especialidade" placeholder="Especialidade" value="<?= $especialidade_prof; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM BOX DE PROFISSIONAL -->

                        <!-- VACINA -->
                        <div id="div_vacinas_aplicadas" <?= $tipo_atendimento == 3 ? "" : "style='display: none'"; ?> class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>VACINAS APLICADAS</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-4 mb-4">
                                    <!-- /.col -->
                                    <div class="col-md-12 col-12">
                                        <div class="form-group">
                                            <label>VACINAS APLICADAS</label>
                                            <select class="form-control select2" id="vacinas_aplicadas" name="vacinas_aplicadas[]" multiple="multiple" data-placeholder="Selecione as Vacinas" style="width: 100%;">
                                                <option value="">Selecione as vacinas aplicadas</option>
                                                <?php
                                                if ($consulta_id != "" && is_numeric($consulta_id)) {
                                                    $result = $db->prepare("SELECT *    
                                                    FROM mod_saude_vacinas msv 
                                                    LEFT JOIN mod_saude_consulta_vacina AS mscv ON mscv.vacina_id = msv.id 
                                                    WHERE mscv.consulta_id = ? 
                                                    ORDER BY msv.nome");
                                                    $result->bindValue(1, $consulta_id);
                                                    $result->execute();
                                                    while ($vacinas = $result->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                        <option selected="true" value="<?= $vacinas['id']; ?>"><?= $vacinas['nome']; ?></option>
                                                        <?php
                                                    }
                                                    $result2 = $db->prepare("SELECT *    
                                                    FROM mod_saude_vacinas msv 
                                                    WHERE msv.status = 1 AND msv.id NOT IN(SELECT vacina_id FROM mod_saude_consulta_vacina WHERE consulta_id = ?)
                                                    ORDER BY msv.nome");
                                                    $result2->bindValue(1, $consulta_id);
                                                    $result2->execute();
                                                    while ($vacinas2 = $result2->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                        <option value="<?= $vacinas2['id']; ?>"><?= $vacinas2['nome']; ?></option>
                                                        <?php
                                                    }
                                                } else {
                                                    $result = $db->prepare("SELECT *    
                                                    FROM mod_saude_vacinas msv 
                                                    WHERE msv.status = 1
                                                    ORDER BY msv.nome");
                                                    $result->execute();
                                                    while ($vacinas = $result->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                        <option value="<?= $vacinas['id']; ?>"><?= $vacinas['nome']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                </div>

                            </div>
                        </div>
                        <!-- FIM VACINA -->

                        <!-- BOX DE PACIENTE -->
                        <div id='div_diagnostico' class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>DIAGNÓSTICO PROFISSIONAL</strong>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-2">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">DIAGNÓTICO</th>
                                                <th scope="col">ORIENTAÇÕES</th>
                                                <th width="10%" scope="col">EXAMES SOLICITADOS</th>
                                                <th>MEDICAÇÕES POSOLOGIA</th>
                                                <th width="10%" scope="col">ENCAMINHAMENTO A ESPECIALISTA</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tbody id="resultado_diagnostico">
                                            <?php
                                            $stmp = $db->prepare("SELECT *           
                                                                  FROM mod_saude_consulta_diagnostico mscd   
                                                                  WHERE mscd.consulta_id = ? OR mscd.consulta_id IS NULL AND mscd.responsavel_id = ?");
                                            $stmp->bindValue(1, $id);
                                            $stmp->bindValue(2, $_SESSION['id']);
                                            $stmp->execute();
                                            while ($diag = $stmp->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <tr id="remover_diagnostico">
                                                    <td><?= $diag['diagnostico'] ?></td>
                                                    <td><?= $diag['orientacoes'] ?></td>
                                                    <td>
                                                        <?php
                                                        $stmp3 = $db->prepare("SELECT msce.id, mse.nome AS exame            
                                                                  FROM mod_saude_consulta_exame msce 
                                                                  LEFT JOIN mod_saude_exames AS mse ON mse.id = msce.exame_id 
                                                                  WHERE msce.diagnostico_id = ?
                                                                  ORDER BY mse.nome ASC");
                                                        $stmp3->bindValue(1, $diag['id']);
                                                        $stmp3->execute();
                                                        while ($exames3 = $stmp3->fetch(PDO::FETCH_ASSOC)) {
                                                            ?>
                                                <li><?= $exames3['exame']; ?></li>
                                                <?php
                                            }
                                            ?>
                                            </td>
                                            <td><?= $diag['medicacoes'] ?></td>
                                            <td>
                                                <?php
                                                $stmp4 = $db->prepare("SELECT msce.id, mse.nome AS especialista            
                                                                  FROM mod_saude_consulta_especialistas msce 
                                                                  LEFT JOIN mod_saude_especialidade AS mse ON mse.id = msce.especialista_id 
                                                                  WHERE msce.diagnostico_id = ?
                                                                  ORDER BY mse.nome ASC");
                                                $stmp4->bindValue(1, $diag['id']);
                                                $stmp4->execute();
                                                while ($especialistas4 = $stmp4->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                <li><?= $especialistas4['especialista']; ?></li>
                                                <?php
                                            }
                                            ?>
                                            </td>
                                            <td width="100px">
                                                <a id="editar" onclick="editar(<?= $diag['id'] ?>, '<?= $diag['diagnostico'] ?>', '<?= $diag['orientacoes'] ?>', '<?= $diag['medicacoes'] ?>')" style="cursor: pointer" class="text-warning"><i class="fa fa-pencil"></i></a>
                                                <a id="remover" onclick="remover(this, <?= $diag['id'] ?>)" style="cursor: pointer" class="text-danger"><i class="fa fa-trash"></i></a>
                                            </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <hr>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_diagnostico_medico" class="form-group">
                                            <label for="diagnostico_medico">DIAGNÓSTICO MÉDICO</label>
                                            <input type="hidden" id="diagnostico_id" name="diagnostico_id" value=""/>
                                            <textarea name="diagnostico_medico" id="diagnostico_medico" class="form-control" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_orientacoes_medicas" class="form-group">
                                            <label for="orientacoes_medicas">ORIENTAÇÕES</label>
                                            <textarea name="orientacoes_medicas" id="orientacoes_medicas" class="form-control" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- EXAMES -->
                                        <div class="box box-outline-primary mt-3">
                                            <div class="box-header">
                                                <strong>EXAMES SOLICITADOS</strong>
                                            </div>
                                            <div class="box-body">
                                                <div class="row mt-4 mb-4">
                                                    <!-- /.col -->
                                                    <div class="col-md-12 col-12">
                                                        <div id="div_exames_solicitados" class="form-group">
                                                            <label>EXAMES SOLICITADOS</label>
                                                            <select class="form-control select2" id="exames_solicitados" name="exames_solicitados[]" multiple="multiple" data-placeholder="Selecione os Exames" style="width: 100%;">
                                                                <?php
                                                                $result = $db->prepare("SELECT *    
                                                                FROM mod_saude_exames mse  
                                                                WHERE mse.status = 1
                                                                ORDER BY mse.nome");
                                                                $result->execute();
                                                                while ($exames = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                    ?>
                                                                    <option value="<?= $exames['id']; ?>"><?= $exames['nome']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                            </div>
                                        </div>

                                        <!-- FIM EXAMES -->
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12">
                                        <div id="div_orientacoes_medicas" class="form-group">
                                            <label for="medicacoes_posologia">MEDICAÇÕES/POSOLOGIA</label>
                                            <textarea name="medicacoes_posologia" id="medicacoes_posologia" class="form-control" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- ENCAMINHAMENTO -->
                                        <div class="box box-outline-primary mt-3">
                                            <div class="box-header">
                                                <strong>ENCAMINHAMENTO</strong>
                                            </div>
                                            <div class="box-body">
                                                <div class="row mt-4 mb-4">
                                                    <!-- /.col -->
                                                    <div class="col-md-12 col-12">
                                                        <div id="div_especialistas" class="form-group">
                                                            <label>ESPECIALISTA(S)</label>
                                                            <select class="form-control select2" id="especialistas" name="especialistas[]" multiple="multiple" data-placeholder="Selecione o Especialista(s)" style="width: 100%;">
                                                                <?php
                                                                $result = $db->prepare("SELECT *  
                                                                FROM mod_saude_especialidade  
                                                                WHERE status = 1
                                                                ORDER BY nome");
                                                                $result->execute();
                                                                while ($especialidaes = $result->fetch(PDO::FETCH_ASSOC)) {
                                                                    ?>
                                                                    <option value="<?= $especialidaes['id']; ?>"><?= $especialidaes['nome']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- FIM ENCAMINHAMENTO -->
                                    </div>
                                </div>
                                <div class="row mt-3 mb-2">
                                    <div class="col-md-12 text-center">
                                        <button type="button" id="add" name="add" class="btn btn-success">INSERIR</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- FIM BOX DE PACIENTE -->

                        <br>

                        <div class="row mt-4">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>SOLICITADO RETORNO PARA CONSULTA?</label>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $retorno_consulta == 1 ? "checked='true'" : ""; ?> name="opcao_solicitado_retorno" id="solicitacao_retorno_sim" value="1">
                                            <label for="solicitacao_retorno_sim">SIM</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" class="with-gap radio-col-info" <?= $retorno_consulta == 0 || $retorno_consulta == "" ? "checked='true'" : ""; ?> name="opcao_solicitado_retorno" id="solicitacao_retorno_nao" value="0">
                                            <label for="solicitacao_retorno_nao">NÃO</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="div_retorno" <?= $retorno_consulta == 1 ? "" : "style='display: none'"; ?> class="box box-outline-primary mt-3">
                            <div class="box-header">
                                <strong>RETORNO</strong>
                            </div>
                            <div class="box-body">
                                <div class="row mt-3">
                                    <div class="col-md-3">
                                        <div id="div_data_provavel_retorno" class="form-group">
                                            <label for="data_provavel_retorno">DATA PROVÁVEL DO RETORNO</label>
                                            <input class="form-control" name="data_provavel_retorno" id="data_provavel_retorno" type="date" value="<?= convertDataBR2ISO(obterDataBRTimestamp($data_retorno)); ?>" />
                                        </div>
                                    </div>

                                    <div class="col-md-1"></div>

                                    <div class="col-md-3">
                                        <div id="div_data_agendada_retorno" class="form-group">
                                            <label for="data_agendada_retorno">DATA AGENDADA PARA RETORNO</label>
                                            <input class="form-control" name="data_agendada_retorno" id="data_agendada_retorno" type="date" value="<?= convertDataBR2ISO(obterDataBRTimestamp($data_agendada)); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="div_hora_retorno" class="form-group">
                                            <label for="hora_retorno">HORA DO RETORNO</label>
                                            <input class="form-control" type="time" name="hora_retorno" id="hora_retorno" value="<?= $hora_retorno; ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- FIM PERFIL -->
                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $consulta_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">MARCAR</button>
                        <button <?= $consulta_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/consultas/novo.js"></script>