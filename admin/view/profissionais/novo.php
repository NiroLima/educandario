<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- CSS DO PLUGIN DE UPLOAD DE FOTOS -->
<link href="<?= PORTAL_URL; ?>assets/vendor_components/cropper/css/cropper.min.css" rel="stylesheet">
<link href="<?= PORTAL_URL; ?>assets/vendor_components/cropper/css/main.css" rel="stylesheet">
<!-- FIM DO PLUGIN -->

<?php
$_SESSION['foto_cut'] = "";
$_SESSION['foto_origin'] = "";
unset($_SESSION['foto_cut']);
unset($_SESSION['foto_origin']);

$perfil = "";

$id = (!isset($_POST['id']) && isset($_GET['id']) ? $_GET['id'] : (isset($_POST['id']) ? $_POST['id'] : 0 ) );
$param = Url::getURL(4);
$param = $param == '' && $id != '' ? $id : $param;

if ($param != null && $param != '' && $param != NULL && $param != 0) {
    $id = $param;

    $result = $db->prepare("SELECT u.id, u.registro, u.crm, u.categoria_id, u.cpf, u.periodo, u.funcao_id, u.cidade_id, u.nascimento, u.celular, u.status, u.nome, u.contato, u.email, sf.nome AS funcao,
                 u.rua, u.bairro, u.numero 
                 FROM seg_profissional u 
                 LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id 
                 WHERE u.id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    $dados_profissiional = $result->fetch(PDO::FETCH_ASSOC);

    $profissiional_id = $dados_profissiional['id'];
    $profissiional_nome = ($dados_profissiional['nome']);
    $profissiional_cpf = ($dados_profissiional['cpf']);
    $profissiional_status = $dados_profissiional['status'];
    $profissiional_contato_cel = $dados_profissiional['celular'];
    $profissiional_contato_fixo = $dados_profissiional['contato'];
    $profissiional_nascimento = $dados_profissiional['nascimento'];
    $profissiional_email = $dados_profissiional['email'];

    $usuariro_registro = $dados_profissiional['registro'];
    $profissiional_categoria = $dados_profissiional['categoria_id'];

    $profissiional_rua = $dados_profissiional['rua'];
    $profissiional_bairro = $dados_profissiional['bairro'];
    $profissiional_numero = $dados_profissiional['numero'];
    $profissiional_municipio = $dados_profissiional['cidade_id'];
    $profissiional_estado = estado_municipio($profissiional_municipio);

    $profissiional_funcao = $dados_profissiional['funcao_id'];
    $periodo = $dados_profissiional['periodo'];
    
    $usuariro_crm = $dados_profissiional['crm'];
} else {
    $profissiional_id = "";
    $profissiional_nome = "";
    $profissiional_cpf = "";
    $profissiional_status = 1;
    $profissiional_contato_cel = "";
    $profissiional_contato_fixo = "";
    $profissiional_nascimento = "";
    $profissiional_email = "";
    $usuariro_registro = "";
    $profissiional_rua = "";
    $profissiional_bairro = "";
    $profissiional_categoria = "";
    $profissiional_numero = "";
    $profissiional_municipio = "";
    $profissiional_estado = "";
    $profissiional_funcao = "";
    $periodo = "";
    $usuariro_crm = "";
}

//if (!ver_nivel(1) && !ver_nivel(3) && $_SESSION['id'] != $profissiional_id) {
//    echo "<script 'text/javascript'>history.go(-1);</script>";
//}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page"><a href="<?= PORTAL_URL ?>admin/view/profissionais/lista">Profissionais</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Novo</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <!-- PERFIL -->
            <div class="box box-solid bg-info">
                <div class="box-header">
                    <h4 class="box-title"><i class="fal fa-user-circle"></i> <strong>PERFIL</strong></h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="div_nome" class="form-group">
                                        <label for="nome">NOME</label>
                                        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome Completo" value="<?= $profissiional_nome; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="div_cpf" class="form-group">
                                        <label for="cpf">CPF</label>
                                        <input type="text" data-mask="999.999.999-99" class="form-control" name="cpf" id="cpf" placeholder="CPF" value="<?= $profissiional_cpf; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="div_nascimento" class="form-group">
                                        <label for="nascimento">NASCIMENTO</label>
                                        <input class="form-control" name="nascimento" id="nascimento" type="date" value="<?= $profissiional_nascimento; ?>" />
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="div_email" class="form-group">
                                        <label for="email">E-MAIL</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="<?= $profissiional_email; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="div_celular" class="form-group">
                                        <label for="celular">CELULAR</label>
                                        <input type="text" data-mask="(99) 9 9999-9999" class="form-control" name="celular" id="celular" placeholder="(99) 9 9999-9999" value="<?= $profissiional_contato_cel; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="div_fixo" class="form-group">
                                        <label for="fixo">FIXO</label>
                                        <input type="text" data-mask="(99)9999-9999" class="form-control" name="fixo" id="fixo" placeholder="(99) 9999-9999" value="<?= $profissiional_contato_fixo; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIM PERFIL -->

            <form id="form_profissional" name="form_profissional" action="#" method="POST">
                <input type="hidden" id="id" name="id" value="<?= $profissiional_id ?>"/>

                <!-- ENDEREÇO -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-home-lg"></i> <strong>ENDEREÇO</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10">
                                <div id="div_rua" class="form-group">
                                    <label for="email">RUA</label>
                                    <input type="text" class="form-control" name="rua" id="rua" placeholder="Rua, Avenida e etc..." value="<?= $profissiional_rua; ?>">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div id="div_numero" class="form-group">
                                    <label for="numero">NÚMERO</label>
                                    <input type="text" class="form-control" name="numero" id="numero" placeholder="1.260" value="<?= $profissiional_numero; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_bairro" class="form-group">
                                    <label for="bairro">BAIRRO</label>
                                    <input type="text" class="form-control" name="bairro" id="bairro" placeholder="Bairro" value="<?= $profissiional_bairro; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_estado_id" class="form-group">
                                    <label for="estado_id">ESTADO</label>
                                    <select name="estado_id" id="estado_id" class="form-control select2">
                                        <option value="">Selecione o estado</option>
                                        <?php
                                        $result8 = $db->prepare("SELECT nome, id, sigla
                                                     FROM bsc_estado
                                                     ORDER BY nome ASC");
                                        $result8->execute();
                                        while ($estado = $result8->fetch(PDO::FETCH_ASSOC)) {
                                            if ($profissiional_estado == $estado['id']) {
                                                ?>
                                                <option selected="true" label='<?= $estado['sigla']; ?>' value='<?= $estado['id']; ?>'><?= $estado['nome']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option label='<?= $estado['sigla']; ?>' value='<?= $estado['id']; ?>'><?= $estado['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="div_municipio_id" class="form-group">
                                    <label for="municipio_id">CIDADE</label>
                                    <select name="municipio_id" id="municipio_id" class="form-control select2">
                                        <option value="">Selecione a cidade</option>
                                        <?php
                                        $result2 = $db->prepare("SELECT nome, id
                                                     FROM bsc_cidade 
                                                     WHERE 1 
                                                     ORDER BY nome ASC");
                                        $result2->execute();
                                        while ($municipio = $result2->fetch(PDO::FETCH_ASSOC)) {
                                            if ($profissiional_municipio == $municipio['id']) {
                                                ?>
                                                <option selected="true" value='<?= $municipio['id']; ?>'><?= $municipio['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM ENDEREÇO -->

                <!-- PROFISSIONAL -->
                <div class="box box-solid bg-info">
                    <div class="box-header">
                        <h4 class="box-title"><i class="fal fa-briefcase"></i> <strong>PROFISSIONAL</strong></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="div_categoria" class="form-group">
                                    <label for="categoria">CATEGORIA</label>
                                    <select name="categoria" id="categoria" class="form-control select2">
                                        <option value="">Selecione a Categoria</option>
                                        <option <?= $profissiional_categoria == 1 ? "selected='true'" : "" ?> value="1">Quadro</option>
                                        <option <?= $profissiional_categoria == 2 ? "selected='true'" : "" ?> value="2">Extra-Quadro</option>
                                        <option <?= $profissiional_categoria == 3 ? "selected='true'" : "" ?> value="3">Voluntário</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="div_funcao" class="form-group">
                                    <label for="funcao">FUNÇÃO</label>
                                    <select name="funcao" id="funcao" class="form-control select2">
                                        <option value="">Selecione a função</option>
                                        <?php
                                        $setor_profissional = "";
                                        $result = $db->prepare("SELECT fc.id, fc.nome AS profissional, ss.nome AS setor          
                                                        FROM seg_funcao fc  
                                                        LEFT JOIN seg_setor AS ss ON ss.id = fc.setor_id  
                                                        WHERE fc.status = 1");
                                        $result->execute();
                                        while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
                                            if ($profissiional_funcao == $prof['id']) {
                                                $setor_profissional = $prof['setor'];
                                                ?>
                                                <option selected="true" rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                <?php
                                            } else {
                                                ?>
                                                <option rel="<?= $prof['setor']; ?>" value="<?= $prof['id']; ?>"><?= $prof['profissional']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="div_setor" class="form-group">
                                    <label for="setor">SETOR</label>
                                    <input disabled="true" type="text" name="setor" id="setor" class="form-control" value="<?= $setor_profissional; ?>">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div id="div_periodo" class="form-group">
                                    <label for="periodo">PERÍODO</label>
                                    <select name="periodo" id="periodo" class="form-control select2">
                                        <option value="">Selecione o período</option>
                                        <option <?= $periodo == 1 ? "selected='true'" : ""; ?> value="1">MATUTINO</option>
                                        <option <?= $periodo == 2 ? "selected='true'" : ""; ?> value="2">VESPERTINO</option>
                                        <option <?= $periodo == 3 ? "selected='true'" : ""; ?> value="3">NOTURNO</option>
                                        <option <?= $periodo == 4 ? "selected='true'" : ""; ?> value="4">INTEGRAL</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="div_espe" class="col-md-6">
                                <div id="div_especialidade" class="form-group">
                                    <label for="especialidade">ESPECIALIDADE</label>
                                    <select name="especialidade[]" id="especialidade" multiple="true" class="form-control select2">
                                        <option value="">Selecione a especialidade</option>
                                        <?php
                                        if (is_numeric($profissiional_id)) {
                                            $result = $db->prepare("SELECT *        
                                                                    FROM seg_profissional_especialidade AS spe 
                                                                    LEFT JOIN mod_saude_especialidade AS mse ON mse.id = spe.especialidade_id   
                                                                    WHERE spe.profissional_id = ?  
                                                                    ORDER BY mse.nome ASC");
                                            $result->bindValue(1, $profissiional_id);
                                            $result->execute();
                                            while ($especialidades = $result->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <option selected="true" value="<?= $especialidades['id']; ?>"><?= $especialidades['nome']; ?></option>
                                                <?php
                                            }

                                            $result2 = $db->prepare("SELECT *        
                                                                     FROM mod_saude_especialidade AS mse    
                                                                     WHERE mse.id NOT IN (SELECT especialidade_id FROM seg_profissional_especialidade WHERE profissional_id = ?)  
                                                                     ORDER BY mse.nome ASC");
                                            $result2->bindValue(1, $profissiional_id);
                                            $result2->execute();
                                            while ($especialidades2 = $result2->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <option value="<?= $especialidades2['id']; ?>"><?= $especialidades2['nome']; ?></option>
                                                <?php
                                            }
                                        } else {
                                            $result = $db->prepare("SELECT *        
                                                                    FROM mod_saude_especialidade   
                                                                    WHERE status = 1  
                                                                    ORDER BY nome");
                                            $result->execute();
                                            while ($especialidades = $result->fetch(PDO::FETCH_ASSOC)) {
                                                ?>
                                                <option value="<?= $especialidades['id']; ?>"><?= $especialidades['nome']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div id="div_registro_classe" class="form-group">
                                    <label for="registro_classe">REGISTRO DE CLASSE</label>
                                    <input type="text" class="form-control" name="registro_classe" id="registro_classe" placeholder="Informe o registro de classe" value="<?= $usuariro_registro; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div id="div_crm" <?= $profissiional_funcao == 6 ? "" : "style='display: none'"; ?> class="form-group">
                                    <label for="crm">CRM</label>
                                    <input type="text" class="form-control" name="crm" id="crm" placeholder="Informe o CRM" value="<?= $usuariro_crm; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIM PROFISSIONAL -->

                <hr>
                <div class="row mb-2">
                    <div class="col-md-12 text-center">
                        <button <?= $profissiional_id == "" ? "" : "style='display: none'"; ?> type="submit" class="btn btn-success">CADASTRAR</button>
                        <button <?= $profissiional_id == "" ? "style='display: none'" : ""; ?> type="submit" class="btn btn-info">ATUALIZAR</button>
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include 'layout/footer.php'; ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/profissionais/novo.js"></script>