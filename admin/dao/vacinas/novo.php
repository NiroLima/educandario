<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$nome_vacina = isset($_POST['nome_vacina']) ? $_POST['nome_vacina'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("id", "mod_saude_vacinas", "nome", "=", $nome_vacina, "");
if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "O nome da vacina informada já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_saude_vacinas (nome, data_update, responsavel_id, data_cadastro, status) VALUES (?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $nome_vacina);
            $sql->bindValue(2, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE mod_saude_vacinas SET nome = ?, responsavel_id = ? WHERE id = ?");
            $sql->bindValue(1, $nome_vacina);
            $sql->bindValue(2, $_SESSION['id']);
            $sql->bindValue(3, $id);
            $sql->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>