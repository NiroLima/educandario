<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Downloads</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-arrow-square-down"></i> <strong>ARQUIVOS</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/downloads/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVO ARQUIVO</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nome do Arquivo</th>
                                            <th>Profissional</th>
                                            <th>Especialidade</th>
                                            <th>Categoria</th>
                                            <th>Data</th>
                                            <th width="15%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $db->prepare("SELECT md.id, md.arquivo, md.nome_arquivo, md.data_cadastro, sp.categoria_id, md.profissional_id, sp.nome AS profissional          
                                                                FROM mod_downloads md 
                                                                LEFT JOIN seg_profissional AS sp ON sp.id = md.profissional_id 
                                                                WHERE 1   
                                                                ORDER BY md.id");
                                        $result->execute();
                                        while ($downloads = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $downloads['nome_arquivo'] ?></td>
                                                <td><?= $downloads['profissional'] ?></td>
                                                <td><?= especialidade($downloads['profissional_id']) ?></td>
                                                <td><?= categoria_nome($downloads['categoria_id']); ?></td>
                                                <td><?= obterDataBRTimestamp($downloads['data_cadastro']); ?></td>
                                                <td class="text-center">

                                                    <a title="Remover" style="cursor: pointer" id="remover" rel="<?= $downloads['id']; ?>" class="mr-2 waves-effect waves-light btn btn-xs btn-danger"><i class="fal fa-trash-alt"></i></a>

                                                    <?php
                                                    if ($downloads['arquivo'] != "" && $downloads['arquivo'] != null && $downloads['arquivo'] != " ") {
                                                        ?>
                                                        <a target="_blank" href="<?= PORTAL_URL . $downloads['arquivo'] ?>" title="Download" class="mr-2 waves-effect waves-light btn btn-xs btn-success"><i class="fal fa-arrow-square-down"></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                    <a href="<?= PORTAL_URL . "admin/view/downloads/novo/" . $downloads['id'] ?>" title="Mudar Arquivo" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/downloads/lista.js"></script>
