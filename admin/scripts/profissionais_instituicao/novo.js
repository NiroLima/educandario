//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $('#form_profissional').submit(function () {

        if (formulario_validator("")) {

            $("#submit").attr("disabled", true);

            var id = $('#id').val();
            var nome = $('#nome').val();
            var email = $('#email').val();
            var celular = $('#celular').val();
            var fixo = $('#fixo').val();
            var funcao = $('#funcao').val();
            var registro = $('#registro_classe').val();
            var instituicao = $('#instituicao').val();

            $.post(PORTAL_URL + "admin/dao/profissionais_instituicao/novo.php", {id: id, instituicao: instituicao, registro: registro, nome: nome, email: email, celular: celular, fixo: fixo, funcao: funcao}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário do Profissional",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário do Profissional",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {
                        postToURL(PORTAL_URL + 'admin/view/profissionais_instituicao/lista');
                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

});
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;
    var nome = $("#nome").val();
    var funcao = $("#funcao").val();
    var registro_classe = $("#registro_classe").val();
    var instituicao = $('#instituicao').val();

    var element = null;

    //LIMPA MENSAGENS DE ERRO
    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {//VALIDAÇÃO SEM BANCO DE DADOS

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (instituicao == "") {
            $('div#div_instituicao').after('<label id="erro_instituicao" class="error">Instituição é obrigatório.</label>');
            valido = false;
            element = $('div#div_instituicao');
        }

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (funcao == 6) {
            $('div#div_crm').after('<label id="erro_crm" class="error">CRM é obrigatório.</label>');
            valido = false;
            element = $('div#div_crm');
        }

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (registro_classe == "") {
            $('div#div_registro_classe').after('<label id="erro_registro_classe" class="error">Registro de classe é obrigatório.</label>');
            valido = false;
            element = $('div#div_registro_classe');
        }

        //VERIFICANDO SE O CAMPO ESTADO FOI INFORMADO
        if (funcao == "") {
            $('div#div_funcao').after('<label id="erro_funcao" class="error">Função é obrigatório.</label>');
            valido = false;
            element = $('div#div_funcao');
        }

        //VERIFICANDO SE O CAMPO NOME FOI INFORMADO
        if (nome == "") {
            $('div#div_nome').after('<label id="erro_nome" class="error">Nome é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------