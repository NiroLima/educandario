<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Profissionais</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-user"></i> <strong>LISTA DE PROFISSIONAIS</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/profissionais/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVO PROFISSIONAL</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Categoria</th>
                                            <th>Funcão</th>
                                            <th>Especialidada</th>
                                            <th>E-mail</th>
                                            <th>Celular</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $result = $db->prepare("SELECT u.id, u.especialidade_id, u.categoria_id, u.status, u.nome, u.celular, u.email, sf.nome AS funcao, mse.nome AS especialidade 
                                                                FROM seg_profissional u 
                                                                LEFT JOIN seg_funcao AS sf ON sf.id = u.funcao_id 
                                                                LEFT JOIN mod_saude_especialidade AS mse ON mse.id = u.especialidade_id 
                                                                WHERE 1
                                                                ORDER BY u.nome");
                                        $result->execute();
                                        while ($profissionais = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $profissionais['nome']; ?></td>
                                                <td><?= categoria_nome($profissionais['categoria_id']); ?></td>
                                                <td><?= $profissionais['funcao']; ?></td>
                                                <td><?= especialidade($profissionais['id']); ?></td>
                                                <td><?= $profissionais['email']; ?></td>
                                                <td><?= $profissionais['celular']; ?></td>
                                                <td class="text-center" style="width: 150px">
                                                    <?php
                                                    if (ver_nivel(1, "") || ver_nivel(3, "")) {
                                                        ?>
                                                        <a id="ativar" rel="<?= $profissionais['id']; ?>" <?= $profissionais['status'] == 0 && $profissionais['id'] != 1 || $profissionais['status'] == 0 && $profissionais['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> title="Desbloquear o Profissional" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                        <a id="remover" rel="<?= $profissionais['id']; ?>" <?= $profissionais['status'] == 1 && $profissionais['id'] != 1 || $profissionais['status'] == 1 && $profissionais['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> title="Bloquear o Profissional" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                        <a <?= $profissionais['id'] != 1 && ver_nivel(1, "") || $profissionais['id'] != 1 && ver_nivel(3, "") || $profissionais['id'] == $_SESSION['id'] ? "" : "style='display: none'"; ?> href="<?= PORTAL_URL ?>admin/view/profissionais/novo/<?= $profissionais['id']; ?>" title="Editar Profissional" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                                            <?php
                                                        }
                                                        ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/profissionais/lista.js"></script>
