<?php include 'layout/hearder.php'; ?>
<?php include 'layout/menu.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container-full">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= PORTAL_URL ?>admin/view/painel/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item" aria-current="page">Setores</li>
                                <li class="breadcrumb-item active" aria-current="page"><strong>Lista</strong></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                  
                <div class="col-12">
                    <div class="box box-solid bg-info">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-sm-8"><h4 class="box-title"><i class="fal fa-chair-office"></i> <strong>LISTA DE SETORES</strong></h4></div>
                                <div class="col-sm-4 text-right"><a href="<?= PORTAL_URL ?>admin/view/setores/novo" class="waves-effect waves-light btn btn-success btn-xs"><i class="fal fa-plus"></i> NOVO SETOR</a></div>
                            </div>                     
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="complex_header" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Setor</th>
                                            <th>Responsável</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
									
									    <?php
                                        $result = $db->prepare("SELECT ss.id, ss.status, ss.nome, su.nome AS responsavel, ss.data_cadastro   
                                                                FROM seg_setor ss 
                                                                LEFT JOIN seg_usuario AS su ON su.id = ss.responsavel  
                                                                WHERE 1
                                                                ORDER BY ss.nome");
                                        $result->execute();
                                        while ($setor = $result->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?= $setor['nome']; ?></td>
                                                <td><?= $setor['responsavel']; ?></td>
                                                <td class="text-center">

                                                    <?php
                                                    if (ver_nivel(1, "") || ver_nivel(3, "")) {
                                                        ?>
                                                        <a id="ativar" rel="<?= $setor['id']; ?>" <?= $setor['status'] == 0 ? "" : "style='display: none'"; ?> title="Desbloquear Setor" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-info"><i class="fal fa-lock-alt"></i></a>
                                                        <a id="remover" rel="<?= $setor['id']; ?>" <?= $setor['status'] == 1 ? "" : "style='display: none'"; ?> title="Bloquear Setor" href="#" class="mr-2 waves-effect waves-light btn btn-xs btn-default"><i class="fal fa-lock-open-alt"></i></a>
                                                        <?php
                                                    }
                                                    ?>

                                                    <a href="<?= PORTAL_URL ?>admin/view/setores/novo/<?= $setor['id']; ?>" title="Editar Setor" class="mr-2 waves-effect waves-light btn btn-xs btn-warning"><i class="fal fa-edit"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

<?php include ('layout/footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="<?= PORTAL_URL; ?>admin/scripts/setores/lista.js"></script>
