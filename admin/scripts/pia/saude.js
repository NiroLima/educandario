//------------------------------------------------------------------------------------------------------
//Cadastrar
$(document).ready(function () {

    $('select').each(function () {
        $(this).select2();
        $('.select2').attr('style', 'width: 100%');
    });

    $("button#atualizar").click(function () {
        $("form#form_saude").submit();
    });

    $("#possui_sim").click(function () {
        $("#div_deficiencia").show();
    });

    $("#possui_nao").click(function () {
        $("#div_deficiencia").hide();
    });

    $("#sim_tratamento").click(function () {
        $("#qual_tratamento").val("");
        $("#div_qual_tratamento").show();
    });

    $("#nao_tratamento").click(function () {
        $("#qual_tratamento").val("");
        $("#div_qual_tratamento").hide();
    });

    $("#bcp_sim").click(function () {
        $("#responsavel_recebimento").val("");
        $("#div_responsavel_recebimento").show();
    });

    $("#bcp_nao").click(function () {
        $("#responsavel_recebimento").val("");
        $("#div_responsavel_recebimento").hide();
    });

    $("#sim_doenca").click(function () {
        $("#qual_doenca_infectocontagiosa").val("");
        $("#div_infectocontagiosa").show();
    });

    $("#nao_doenca").click(function () {
        $("#qual_doenca_infectocontagiosa").val("");
        $("#div_infectocontagiosa").hide();
    });

    $("#sim_medicacoes").click(function () {
        $("#div_medicacao_crianca").show();
    });

    $("#nao_medicacoes").click(function () {
        $("#div_medicacao_crianca").hide();
        $("select#qual_medicamentos").val("");
        $("select#qual_medicamentos").select2();
    });

    $("#sim_atividade_psicologia").click(function () {
        $("div#div_atividade_psicologica").show();
    });

    $("#nao_atividade_psicologia").click(function () {
        $("div#div_atividade_psicologica").hide();
    });

    $("#possui_def_sim").click(function () {
        $("#customFileLaudo").val("");
        $("#tipo_deficiencia").val("");
        $("#tipo_deficiencia").select2();
        $("#descreva_tipo_def").val("");

        $("div#div_laudo").show();
        $("div#div_qual_tipo").show();
        $("div#div_descricao_def").show();
    });

    $("#possui_def_nao").click(function () {

        $("#customFileLaudo").val("");
        $("#tipo_deficiencia").val("");
        $("#tipo_deficiencia").select2();
        $("#descreva_tipo_def").val("");

        $("div#div_laudo").hide();
        $("div#div_qual_tipo").hide();
        $("div#div_descricao_def").hide();
    });

    $("#em_def_investigacao").click(function () {
        $("#customFileLaudo").val("");
        $("#tipo_deficiencia").val("");
        $("#tipo_deficiencia").select2();
        $("#descreva_tipo_def").val("");

        $("div#div_laudo").hide();
        $("div#div_qual_tipo").hide();
        $("div#div_descricao_def").show();
    });

    $('#form_responsavel').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var responsavel_id = $("#responsavel_id").val();

        $.post(PORTAL_URL + "admin/dao/pia/acolhido_responsavel.php", {id: id, responsavel_id: responsavel_id}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário PIA",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário PIA",
                    text: "Informações do acolhido atualizadas com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/saude/' + id);
                });
            }
        }
        , "html");
        return false;
    });

    $("#inserir_servico").click(function () {

        var id = $("#id").val();
        var nome_medico = $("#nome_medico").val();
        var medicamento = $("#medicamento").val();
        var posologia = $("#posologia").val();
        var administracao = $("#administracao").val();

        var servico_id = $("#servico_id").val();

        if (formulario_validator("")) {
            $.post(PORTAL_URL + "admin/dao/pia/servicos.php", {id: id, servico_id: servico_id, nome_medico: nome_medico, medicamento: medicamento, posologia: posologia, administracao: administracao}, function (data) {
                if (isNaN(data)) {
                    swal({
                        title: "Formulário de Serviço",
                        html: data,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    });
                    $("#submit").attr("disabled", false);
                    return false;
                } else {
                    swal({
                        title: "Formulário de Serviço",
                        text: "Ação realizada com sucesso!",
                        type: "success",
                        confirmButtonClass: "btn btn-success",
                        confirmButtonText: "Ok"
                    }).then(function () {

                        $.post(PORTAL_URL + "admin/dao/pia/carregar_servicos.php",
                                {id: id},
                                function (valor) {
                                    $("input#servico_id").val("");
                                    $("button#inserir_servico").html("INSERIR");
                                    $("input#nome_medico").val("");
                                    $("input#medicamento").val("");
                                    $("input#posologia").val("");
                                    $("input#administracao").val("");
                                    $("tbody#resultado_servico").html(valor);
                                });

                    });
                }
            }
            , "html");
            return false;
        } else {
            return false;
        }
    });

    $('#form_saude').submit(function () {

        $("#submit").attr("disabled", true);

        var id = $('#id').val();
        var acolhimento_crianca_id = $("#acolhimento_crianca_id").val();
        var qual_deficiencia = $("#possui_def_sim:checked").val() == 1 ? 1 : ($("#possui_def_nao:checked").val() == 0 ? 0 : ($("#em_def_investigacao:checked").val() == 2 ? 2 : 1));
        var recursos = $("#recursos").val();
        var qual_tratamento = $("#qual_tratamento").val();
        var disturbio = $("#sim_indicios:checked").val() == 1 ? 1 : ($("#nao_indicios:checked").val() == 2 ? 2 : 0);
        var qual_doenca_infectocontagiosa = $("#qual_doenca_infectocontagiosa").val();
        var qual_medicamentos = $("#qual_medicamentos").val();
        var obs_saude = $("#obs_saude").val();

        var peso = $("#peso").val();
        var altura = $("#altura").val();
        var tipo_sanguineo = $("#tipo_sanguineo").val();
        var condicoes_saude = $("#condicoes_saude").val();
        var justificativa = $("#justificativa").val();
        var atividade_psicologia = $("#sim_atividade_psicologia:checked").val() == 1 ? 1 : 0;

        var acolhimento_crianca_geral_id = $("#acolhimento_crianca_geral_id").val();
        var tipo_deficiencia = qual_deficiencia == 1 ? $("#tipo_deficiencia").val() : "";
        var descreva_tipo_def = qual_deficiencia == 0 || qual_deficiencia == 1 ? $("#descreva_tipo_def").val() : "";

        var bcp_sim = $("#bcp_sim:checked").val() == 1 ? 1 : 0;
        var responsavel_recebimento = $("#responsavel_recebimento").val();
        var atvd = $("#sim_atvd_psicologia:checked").val() == 1 ? 1 : 0;

        $.post(PORTAL_URL + "admin/dao/pia/saude.php", {id: id, atvd: atvd, bcp_sim: bcp_sim, responsavel_recebimento: responsavel_recebimento, descreva_tipo_def: descreva_tipo_def, acolhimento_crianca_geral_id: acolhimento_crianca_geral_id, tipo_deficiencia: tipo_deficiencia, acolhimento_crianca_id: acolhimento_crianca_id, obs_saude: obs_saude,
            qual_medicamentos: qual_medicamentos, qual_doenca_infectocontagiosa: qual_doenca_infectocontagiosa, disturbio: disturbio, qual_deficiencia: qual_deficiencia,
            recursos: recursos, qual_tratamento: qual_tratamento, peso: peso, altura: altura, tipo_sanguineo: tipo_sanguineo, condicoes_saude: condicoes_saude,
            justificativa: justificativa, atividade_psicologia: atividade_psicologia}, function (data) {
            if (isNaN(data)) {
                swal({
                    title: "Formulário de Saúde",
                    html: data,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    closeOnConfirm: false
                });
                $("#submit").attr("disabled", false);
                return false;
            } else {
                swal({
                    title: "Formulário de Saúde",
                    text: "Ação realizada com sucesso!",
                    type: "success",
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok"
                }).then(function () {
                    postToURL(PORTAL_URL + 'admin/view/pia/saude/' + id);
                });
            }
        }
        , "html");
        return false;

    });

});
//------------------------------------------------------------------------------------------------------
function editar_responsavel() {
    $("div#responsavel_edite_1").hide();
    $("div#responsavel_edite_2").show();
}
//------------------------------------------------------------------------------
function editar(id, medico, medicamento, posologia, administracao) {
    $("input#servico_id").val(id);
    $("input#nome_medico").val(medico);
    $("input#medicamento").val(medicamento);
    $("input#posologia").val(posologia);
    $("input#administracao").val(administracao);
    $("button#inserir_servico").html("ALTERAR");
}
//------------------------------------------------------------------------------
function remover(obj, id) {
    swal({
        title: "Deseja mesmo remover este serviço?",
        text: "Obs: Caso escolha remover não podera mais recuperar os daddos do serviço!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode remover!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/remover",
            type: "POST",
            data: {id: id},
            enctype: 'multipart/form-data',
            success: onSuccessSend(obj),
            error: onSuccessSend(obj)
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {
    swal({
        title: "Sucesso!",
        text: "Serviço removido com sucesso!",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        $("input#servico_id").val("");
        $("button#inserir_servico").html("INSERIR");
        $("input#nome_medico").val("");
        $("input#medicamento").val("");
        $("input#posologia").val("");
        $("input#administracao").val("");
        $(obj).parents('tr#remover_servico').remove();
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
//VALIDATOR DO LOGIN
function formulario_validator(obj) {
    var valido = true;

    var nome_medico = $("#nome_medico").val();
    var medicamento = $("#medicamento").val();
    var posologia = $("#posologia").val();
    var administracao = $("#administracao").val();

    var element = null;

    $('label.error').each(function () {
        $(this).remove();
    });

    if (obj.tipo == "" || obj.tipo == null) {

        if (administracao == "") {
            $('div#div_administracao').after('<label id="erro_administracao" class="error">Administração é obrigatório.</label>');
            valido = false;
            element = $('div#div_administracao');
        }
        if (posologia == "") {
            $('div#div_posologia').after('<label id="erro_posologia" class="error">Posologia é obrigatório.</label>');
            valido = false;
            element = $('div#div_posologia');
        }

        if (medicamento == "") {
            $('div#div_medicamento').after('<label id="erro_medicamento" class="error">Medicamento é obrigatório.</label>');
            valido = false;
            element = $('div#div_medicamento');
        }

        if (nome_medico == "") {
            $('div#div_nome_medico').after('<label id="erro_nome_medico" class="error">Médico é obrigatório.</label>');
            valido = false;
            element = $('div#div_nome_medico');
        }
    }

    if (element != null) {
        var topPosition = element.offset().top - 135;
        $('html, body').animate({
            scrollTop: topPosition
        }, 800);
    }
    return valido;
}
//------------------------------------------------------------------------------------------------------
function validar(codigo) {
    swal({
        title: "Deseja mesmo confirmar a Etapa 3 Saúde?",
        text: "Obs: Caso escolha confirmar, o sistema irá identificar essa etapa como concluída!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode confirmar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/confirmar_acolhido",
            type: "POST",
            data: {id: codigo, op: 3},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------------------------------
function desvalidar(codigo) {
    swal({
        title: "Deseja mesmo desvalidar a Etapa 3 Saúde?",
        text: "Obs: Caso escolha desvalidar, o sistema irá identificar essa etapa como pendente!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-success",
        cancelButtonClass: "btn btn-danger m-l-10",
        confirmButtonText: "Sim, pode desvalidar!",
        cancelButtonText: "Não"
    }).then(function () {
        projetouniversal.util.getjson({
            url: PORTAL_URL + "admin/dao/pia/desvalidar_acolhido",
            type: "POST",
            data: {id: codigo, op: 3},
            enctype: 'multipart/form-data',
            success: onSuccessSend,
            error: onError
        });
        return false;
    });
}
//------------------------------------------------------------------------------
function onSuccessSend(obj) {

    var codigo = $("#id").val();

    swal({
        title: "Sucesso!",
        text: "" + obj.retorno + "",
        type: "success",
        confirmButtonClass: "btn btn-success",
        confirmButtonText: "Ok"
    }).then(function () {
        postToURL(PORTAL_URL + 'admin/view/pia/saude/' + codigo);
    });

    return false;
}
//------------------------------------------------------------------------------    
/* ERRO AO ENVIAR AJAX */
function onError(args) {
    swal("Error!", "" + args.retorno + "", "error");
    return false;
}
//------------------------------------------------------------------------------
function gerar_pdf() {
    //pega o Html da DIV
    var divElements = document.getElementById("impressao").innerHTML;
    //pega o HTML de toda tag Body
    var oldPage = document.body.innerHTML;
    //Alterna o body 
    document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";
    //Imprime o body atual
    window.print();
    //Retorna o conteudo original da página. 
    document.body.innerHTML = oldPage;
}
//------------------------------------------------------------------------------