<?php
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;

if ($error == false) {
    try {

        $db->beginTransaction();

        $sql4 = $db->prepare("DELETE FROM mod_acolhimento_crianca_servico WHERE id = ?");
        $sql4->bindValue(1, $id);
        $sql4->execute();

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>