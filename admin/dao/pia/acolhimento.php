<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$quem_trouxe = isset($_POST['quem_trouxe']) && $_POST['quem_trouxe'] != "" ? $_POST['quem_trouxe'] : NULL;
$opcao_conselho = isset($_POST['opcao_conselho']) && $_POST['opcao_conselho'] != "" ? $_POST['opcao_conselho'] : NULL;
$comarca_interior = isset($_POST['comarca_interior']) && $_POST['comarca_interior'] != "" ? $_POST['comarca_interior'] : NULL;
$responsavel_entrega = isset($_POST['responsavel_entrega']) && $_POST['responsavel_entrega'] != "" ? $_POST['responsavel_entrega'] : NULL;
$funcao_responsavel = isset($_POST['funcao_responsavel']) && $_POST['funcao_responsavel'] != "" ? $_POST['funcao_responsavel'] : NULL;
$contato_responsavel = isset($_POST['contato_responsavel']) && $_POST['contato_responsavel'] != "" ? $_POST['contato_responsavel'] : NULL;
$data_entrada = isset($_POST['data_entrada']) && $_POST['data_entrada'] != "" ? $_POST['data_entrada'] : NULL;
$hora_entrada = isset($_POST['hora_entrada']) && $_POST['hora_entrada'] != "" ? $_POST['hora_entrada'] : NULL;
$profissional = isset($_POST['profissional']) && $_POST['profissional'] != "" ? $_POST['profissional'] : NULL;
$observacao_acolhimento = isset($_POST['observacao_acolhimento']) && $_POST['observacao_acolhimento'] != "" ? $_POST['observacao_acolhimento'] : NULL;
$motivo_aba = isset($_POST['motivo_aba']) && $_POST['motivo_aba'] != "" ? $_POST['motivo_aba'] : NULL;
$motivo_neg = isset($_POST['motivo_neg']) && $_POST['motivo_neg'] != "" ? $_POST['motivo_neg'] : NULL;
$motivo_abu = isset($_POST['motivo_abu']) && $_POST['motivo_abu'] != "" ? $_POST['motivo_abu'] : NULL;
$motivo_out = isset($_POST['motivo_out']) && $_POST['motivo_out'] != "" ? $_POST['motivo_out'] : NULL;
$descricao = isset($_POST['descricao']) && $_POST['descricao'] != "" ? $_POST['descricao'] : NULL;
$numero_guia_acolhimento = isset($_POST['numero_guia_acolhimento']) && $_POST['numero_guia_acolhimento'] != "" ? $_POST['numero_guia_acolhimento'] : NULL;
$numero_processo = isset($_POST['numero_processo']) && $_POST['numero_processo'] != "" ? $_POST['numero_processo'] : NULL;
$numero_destituicao = isset($_POST['numero_destituicao']) && $_POST['numero_destituicao'] != "" ? $_POST['numero_destituicao'] : NULL;

$sim_residia = isset($_POST['sim_residia']) && $_POST['sim_residia'] != "" ? $_POST['sim_residia'] : NULL;
$tempo_rua = isset($_POST['tempo_rua']) && $_POST['tempo_rua'] != "" ? $_POST['tempo_rua'] : NULL;
$costumava_abrigar = isset($_POST['costumava_abrigar']) && $_POST['costumava_abrigar'] != "" ? $_POST['costumava_abrigar'] : NULL;
$pessoas_vivia_rua = isset($_POST['pessoas_vivia_rua']) && $_POST['pessoas_vivia_rua'] != "" ? $_POST['pessoas_vivia_rua'] : NULL;
$parentes_vinculo = isset($_POST['parentes_vinculo']) && $_POST['parentes_vinculo'] != "" ? $_POST['parentes_vinculo'] : NULL;
$sim_acolhida_anterior = isset($_POST['sim_acolhida_anterior']) && $_POST['sim_acolhida_anterior'] != "" ? $_POST['sim_acolhida_anterior'] : NULL;
$servico_acolhimento = isset($_POST['servico_acolhimento']) && $_POST['servico_acolhimento'] != "" ? $_POST['servico_acolhimento'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

        $sql2 = $db->prepare("UPDATE mod_acolhimento SET quem_trouxe = ?, responsavel_entrega = ?, funcao_responsavel = ?, celular = ?, data_entrada = ?, hora_entrada = ?,
                                 profissional_id = ?, obs = ?, motivo_aba = ?, motivo_neg = ?, motivo_abu = ?, motivo_out = ?, descricao = ?, numero_guia = ?, numero_processo = ?,
                                 numero_destituicao = ?, comarca_id = ?, opcao_conselho = ?, usuario_id = ? WHERE id = ?");
        $sql2->bindValue(1, $quem_trouxe);
        $sql2->bindValue(2, $responsavel_entrega);
        $sql2->bindValue(3, $funcao_responsavel);
        $sql2->bindValue(4, $contato_responsavel);
        $sql2->bindValue(5, $data_entrada);
        $sql2->bindValue(6, $hora_entrada);
        $sql2->bindValue(7, $profissional);
        $sql2->bindValue(8, $observacao_acolhimento);
        $sql2->bindValue(9, $motivo_aba);
        $sql2->bindValue(10, $motivo_neg);
        $sql2->bindValue(11, $motivo_abu);
        $sql2->bindValue(12, $motivo_out);
        $sql2->bindValue(13, $descricao);
        $sql2->bindValue(14, $numero_guia_acolhimento);
        $sql2->bindValue(15, $numero_processo);
        $sql2->bindValue(16, $numero_destituicao);
        $sql2->bindValue(17, $comarca_interior);
        $sql2->bindValue(18, $opcao_conselho);
        $sql2->bindValue(19, $_SESSION['id']);
        $sql2->bindValue(20, $id);
        $sql2->execute();

        if (is_numeric(pesquisar("id", "mod_acolhimento_situacao", "mod_acolhimento_id", "=", $id, ""))) {
            $sql3 = $db->prepare("UPDATE mod_acolhimento_situacao SET residia = ?, tempo_rua = ?, local = ?, pessoas_vivia_rua = ?, parentes_vinculos  = ?, acolhida_anteriomente = ?, servico_acolhimento = ?, responsavel_id = ?, data_update  = NOW() WHERE mod_acolhimento_id = ?");
            $sql3->bindValue(1, $sim_residia);
            $sql3->bindValue(2, $tempo_rua);
            $sql3->bindValue(3, $costumava_abrigar);
            $sql3->bindValue(4, $pessoas_vivia_rua);
            $sql3->bindValue(5, $parentes_vinculo);
            $sql3->bindValue(6, $sim_acolhida_anterior);
            $sql3->bindValue(7, $servico_acolhimento);
            $sql3->bindValue(8, $_SESSION['id']);
            $sql3->bindValue(9, $id);
            $sql3->execute();
        } else {
            $sql3 = $db->prepare("INSERT INTO mod_acolhimento_situacao (mod_acolhimento_id, residia, tempo_rua, local, pessoas_vivia_rua, parentes_vinculos, acolhida_anteriomente, servico_acolhimento, responsavel_id, data_cadastro, status) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1)");
            $sql3->bindValue(1, $id);
            $sql3->bindValue(2, $sim_residia);
            $sql3->bindValue(3, $tempo_rua);
            $sql3->bindValue(4, $costumava_abrigar);
            $sql3->bindValue(5, $pessoas_vivia_rua);
            $sql3->bindValue(6, $parentes_vinculo);
            $sql3->bindValue(7, $sim_acolhida_anterior);
            $sql3->bindValue(8, $servico_acolhimento);
            $sql3->bindValue(9, $_SESSION['id']);
            $sql3->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>