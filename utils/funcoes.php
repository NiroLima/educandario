<?php

//----------------------------------------------------------------------------------------------------------------------------
function situacao_familiar($op) {
    $rs = "";

    if ($op == 0) {
        $rs = "Com Vínculo";
    } else if ($op == 1) {
        $rs = "Sem Vínculo";
    } else if ($op == 2) {
        $rs = "Família Desaparecida";
    } else if ($op == 3) {
        $rs = utf8_decode("Órfão");
    } else if ($op == 4) {
        $rs = utf8_decode("Destituído do Poder Familiar");
    } else if ($op == 5) {
        $rs = utf8_decode("Com Impedimento Judicial de Contato");
    } else if ($op == 6) {
        $rs = utf8_decode("Sem Informação");
    } else if ($op == 7) {
        $rs = utf8_decode("Outra Situação");
    }


    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function situacao_pai($op) {
    $rs = "";

    if ($op == 0) {
        $rs = "Pai Falecido";
    } else if ($op == 1) {
        $rs = "Pai Recluso em Sistema Prisional";
    } else if ($op == 2) {
        $rs = "Pai não Encontrado";
    } else if ($op == 3) {
        $rs = utf8_decode("Pai Descolhecido");
    } else if ($op == 4) {
        $rs = utf8_decode("Sem Vínculo com o Pai");
    }

    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function opcao($op) {

    $rs = "";

    if ($op == 0) {
        $rs = "Ruim";
    } else if ($op == 1) {
        $rs = "Regular";
    } else if ($op == 2) {
        $rs = "Boa";
    } else if ($op == 3) {
        $rs = utf8_decode("Ótimo");
    }

    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function quebra($pdf) {
    if ($pdf->GetY() > 230) {
        //Cabeçalho
        $pdf->SetMargins(3, 3, 2);
        $pdf->SetY("-1");
        $pdf->MultiCell(160, 5, ' ', '0', 'C', 0);
        $pdf->SetFont('arial', 'B', 10);
        $pdf->SetTextColor(000);

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTitle('PLANO INDIVIDUAL DE ATENDIMENTO - PIA');

        $pdf->SetXY(15, 5);
        $pdf->Cell(180, 10, utf8_decode('PLANO INDIVIDUAL DE ATENDIMENTO - PIA'), 0, '', 'C');

        $pdf->SetFont('Arial', '', 10);
    }
}

//----------------------------------------------------------------------------------------------------------------------------
function quem($id) {
    $rs = "";

    if ($id == 1) {
        $rs = "Poder Juiciário";
    } else if ($id == 2) {
        $rs = "Conselho Tutelar";
    } else if ($id == 3) {
        $rs = "Polícias";
    }

    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function cor($id) {

    $rs = "";

    if ($id == 1) {
        $rs = "Branca";
    } else if ($id == 2) {
        $rs = "Preta";
    } else if ($id == 3) {
        $rs = "Parda";
    } else if ($id == 4) {
        $rs = "Amarela";
    } else if ($id == 5) {
        $rs = "Indígena";
    }

    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function especialidade($profissional_id) {

    $rs = "";

    $db = Conexao::getInstance();

    $result = $db->prepare("SELECT sp.id, spe.especialidade_id, mse.nome AS especialidade 
                            FROM seg_profissional sp 
                            LEFT JOIN seg_profissional_especialidade AS spe ON spe.profissional_id = sp.id 
                            LEFT JOIN mod_saude_especialidade AS mse ON mse.id = spe.especialidade_id 
                            WHERE sp.id = ?
                            ORDER BY mse.nome");
    $result->bindValue(1, $profissional_id);
    $result->execute();
    while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
        $rs .= is_numeric($prof['especialidade_id']) && $prof['especialidade_id'] != "" && $prof['especialidade_id'] != null ? $prof['especialidade'] . "; " : "Sem Especialidade";
    }

    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function categoria($profissional_id) {

    $rs = "";

    $db = Conexao::getInstance();

    $result = $db->prepare("SELECT sp.categoria_id 
                            FROM seg_profissional sp 
                            WHERE sp.id = ?");
    $result->bindValue(1, $profissional_id);
    $result->execute();
    while ($prof = $result->fetch(PDO::FETCH_ASSOC)) {
        $rs = categoria_nome($prof['categoria_id']);
    }

    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function periodo_nome($op) {
    if ($op == 1) {
        return "Matutino";
    } else if ($op == 2) {
        return "Vespertino";
    } else if ($op == 3) {
        return "Norturno";
    } else if ($op == 4) {
        return "Integral";
    } else {
        return "";
    }
}

//----------------------------------------------------------------------------------------------------------------------------
function categoria_nome($op) {
    if ($op == 1) {
        return "Quadro";
    } else if ($op == 2) {
        return "Extra-Quadro";
    } else {
        return "Voluntário";
    }
}

//----------------------------------------------------------------------------------------------------------------------------
function vf_exame($consulta_id, $exame_id) {
    $rs = 0;

    $db = Conexao::getInstance();

    $result23 = $db->prepare("SELECT *    
                             FROM mod_saude_consulta_exame
                             WHERE consulta_id = ? AND exame_id = ?");
    $result23->bindValue(1, $consulta_id);
    $result23->bindValue(2, $exame_id);
    $result23->execute();

    while ($exames = $result23->fetch(PDO::FETCH_ASSOC)) {
        $rs = 1;
    }

    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function vf_vacinacao($consulta_id, $vacina_id) {
    $rs = 0;

    $db = Conexao::getInstance();

    $result23 = $db->prepare("SELECT *    
                             FROM mod_saude_consulta_vacina
                             WHERE consulta_id = ? AND vacina_id = ?");
    $result23->bindValue(1, $consulta_id);
    $result23->bindValue(2, $vacina_id);
    $result23->execute();

    while ($exames = $result23->fetch(PDO::FETCH_ASSOC)) {
        $rs = 1;
    }

    return $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function calcular_idade($data_nascimento) {
    $idade = 0;

    $data_nascimento = date('Y-m-d', strtotime($data_nascimento));

    $data = explode("-", $data_nascimento);

    $anoNasc = $data[0];
    $mesNasc = $data[1];
    $diaNasc = $data[2];

    $anoAtual = date("Y");
    $mesAtual = date("m");
    $diaAtual = date("d");

    $idade = $anoAtual - $anoNasc;

    if ($mesAtual < $mesNasc) {

        $idade -= 1;
    } elseif (($mesAtual == $mesNasc) && ($diaAtual <= $diaNasc)) {

        $idade -= 1;
    }

    return $idade;
}

//----------------------------------------------------------------------------------------------------------------------------
function tipo_membro($op) {
    $result = "";

    if ($op == 1) {
        $result = "Presencial";
    } else if ($op == 2) {
        $result = "Virtual";
    }

    return $result;
}

//----------------------------------------------------------------------------------------------------------------------------
function grau_parentesco($op) {

    $result = "";

    if ($op == 1) {
        $result = "Irmão / Irmã";
    } else if ($op == 2) {
        $result = "Tio / Tia";
    } else if ($op == 3) {
        $result = "Avô / Avó";
    } else if ($op == 4) {
        $result = "Primo / Prima";
    } else if ($op == 5) {
        $result = "Padrinho / Madrinha";
    } else if ($op == 6) {
        $result = "Vizinho / Vizinha";
    } else if ($op == 7) {
        $result = "Amigo / Amiga";
    }

    return $result;
}

//----------------------------------------------------------------------------------------------------------------------------
function carregar_info_menu($acolhimento_id, $op) {
    $db = Conexao::getInstance();
//MÓDULO PIA MENU  1 -----------------------------------------------------------------------------------------------------------
    if ($op == 1) {
        $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?
                            GROUP BY mac.id");
        $result2->bindValue(1, $acolhimento_id);
        $result2->execute();
        $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

        $acolhimento_crianca_id = $resultado2['id'];

        if ($resultado2['vf'] == 1) {
            return true;
        } else {
            return false;
        }
    } else if ($op == 2) {
//MÓDULO PIA MENU  2 -----------------------------------------------------------------------------------------------------------
        $result3 = $db->prepare("SELECT *      
                             FROM mod_acolhimento_situacao mas   
                             WHERE mas.mod_acolhimento_id = ?");
        $result3->bindValue(1, $acolhimento_id);
        $result3->execute();
        $resultado3 = $result3->fetch(PDO::FETCH_ASSOC);

        if ($resultado3['vf'] == 1) {
            return true;
        } else {
            return false;
        }
    } else if ($op == 3) {

//MÓDULO PIA MENU  3 -----------------------------------------------------------------------------------------------------------
        $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?
                            GROUP BY mac.id");
        $result2->bindValue(1, $acolhimento_id);
        $result2->execute();
        $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

        $acolhimento_crianca_id = $resultado2['id'];

        $result4 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca_geral macg   
                            WHERE macg.acolhimento_crianca_id = ?");
        $result4->bindValue(1, $acolhimento_crianca_id);
        $result4->execute();
        $resultado4 = $result4->fetch(PDO::FETCH_ASSOC);

        if ($resultado4['vf'] == 1) {
            return true;
        } else {
            return false;
        }
    } else if ($op == 4) {
//MÓDULO PIA MENU  4 -----------------------------------------------------------------------------------------------------------

        $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?
                            GROUP BY mac.id");
        $result2->bindValue(1, $acolhimento_id);
        $result2->execute();
        $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

        $acolhimento_crianca_id = $resultado2['id'];

        $result5 = $db->prepare("SELECT *      
                            FROM mod_acolhimento_crianca_educacao mace   
                            WHERE mace.acolhimento_crianca_id = ?");
        $result5->bindValue(1, $acolhimento_crianca_id);
        $result5->execute();
        $resultado5 = $result5->fetch(PDO::FETCH_ASSOC);

        if ($resultado5['vf'] == 1) {
            return true;
        } else {
            return false;
        }
    } else if ($op == 5) {

        $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?
                            GROUP BY mac.id");
        $result2->bindValue(1, $acolhimento_id);
        $result2->execute();
        $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

        $acolhimento_crianca_id = $resultado2['id'];

        $result6 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_familia macf   
        WHERE macf.acolhimento_crianca_id = ?");
        $result6->bindValue(1, $acolhimento_crianca_id);
        $result6->execute();
        $resultado6 = $result6->fetch(PDO::FETCH_ASSOC);

        if ($resultado6['vf'] == 1) {
            return true;
        } else {
            return false;
        }
    } else if ($op == 6) {

        //------------------------------------------------------------------------------
//Resultado do mod_acolhimneto_crianca

        $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?
                            GROUP BY mac.id");
        $result2->bindValue(1, $acolhimento_id);
        $result2->execute();
        $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

        $acolhimento_crianca_id = $resultado2['id'];

        $result7 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_rede macr   
        WHERE macr.acolhimento_crianca_id = ?");
        $result7->bindValue(1, $acolhimento_crianca_id);
        $result7->execute();
        $resultado7 = $result7->fetch(PDO::FETCH_ASSOC);

        if ($resultado7['vf'] == 1) {
            return true;
        } else {
            return false;
        }
    } else if ($op == 7) {
        //------------------------------------------------------------------------------

        $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?");
        $result2->bindValue(1, $acolhimento_id);
        $result2->execute();
        $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

        $acolhimento_crianca_id = $resultado2['id'];

        $result8 = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ? AND macp.tipo = 1");
        $result8->bindValue(1, $acolhimento_crianca_id);
        $result8->execute();
        $resultado8 = $result8->fetch(PDO::FETCH_ASSOC);

        if ($resultado8['vf'] == 1) {
            return true;
        } else {
            return false;
        }
    } else if ($op == 8) {

        $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?
                            GROUP BY mac.id");
        $result2->bindValue(1, $acolhimento_id);
        $result2->execute();
        $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

        $acolhimento_crianca_id = $resultado2['id'];

        $result9 = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_avaliacao macp   
                            WHERE macp.acolhimento_crianca_id = ?");
        $result9->bindValue(1, $acolhimento_crianca_id);
        $result9->execute();
        $resultado9 = $result9->fetch(PDO::FETCH_ASSOC);

        if ($resultado9['vf'] == 1) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//----------------------------------------------------------------------------------------------------------------------------
function carregar_info_barra($acolhimento_id) {

    $db = Conexao::getInstance();
//MÓDULO PIA MENU  1 -----------------------------------------------------------------------------------------------------------
    $rs = 0;

    $result2 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca mac   
                            WHERE mac.acolhimento_id = ?
                            GROUP BY mac.id");
    $result2->bindValue(1, $acolhimento_id);
    $result2->execute();
    $resultado2 = $result2->fetch(PDO::FETCH_ASSOC);

    $acolhimento_crianca_id = $resultado2['id'];

    if ($resultado2['vf'] == 1) {
        $rs += 12.5;
    }

//MÓDULO PIA MENU  2 -----------------------------------------------------------------------------------------------------------
    $result3 = $db->prepare("SELECT *      
                             FROM mod_acolhimento_situacao mas   
                             WHERE mas.mod_acolhimento_id = ?");
    $result3->bindValue(1, $acolhimento_id);
    $result3->execute();
    $resultado3 = $result3->fetch(PDO::FETCH_ASSOC);

    if ($resultado3['vf'] == 1) {
        $rs += 12.5;
    }
//MÓDULO PIA MENU  3 -----------------------------------------------------------------------------------------------------------
    $result4 = $db->prepare("SELECT *        
                            FROM mod_acolhimento_crianca_geral macg   
                            WHERE macg.acolhimento_crianca_id = ?");
    $result4->bindValue(1, $acolhimento_crianca_id);
    $result4->execute();
    $resultado4 = $result4->fetch(PDO::FETCH_ASSOC);

    if ($resultado4['vf'] == 1) {
        $rs += 12.5;
    }
//MÓDULO PIA MENU  4 -----------------------------------------------------------------------------------------------------------
    $result5 = $db->prepare("SELECT *      
                            FROM mod_acolhimento_crianca_educacao mace   
                            WHERE mace.acolhimento_crianca_id = ?");
    $result5->bindValue(1, $acolhimento_crianca_id);
    $result5->execute();
    $resultado5 = $result5->fetch(PDO::FETCH_ASSOC);

    if ($resultado5['vf'] == 1) {
        $rs += 12.5;
    }
//MÓDULO PIA MENU  5 -----------------------------------------------------------------------------------------------------------
    $result6 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_familia macf   
        WHERE macf.acolhimento_crianca_id = ?");
    $result6->bindValue(1, $acolhimento_crianca_id);
    $result6->execute();
    $resultado6 = $result6->fetch(PDO::FETCH_ASSOC);

    if ($resultado6['vf'] == 1) {
        $rs += 12.5;
    }
//MÓDULO PIA MENU  6 -----------------------------------------------------------------------------------------------------------
    $result9 = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_rede macr   
                            WHERE macr.acolhimento_crianca_id = ?");
    $result9->bindValue(1, $acolhimento_crianca_id);
    $result9->execute();
    $resultado9 = $result9->fetch(PDO::FETCH_ASSOC);

    if ($resultado9['vf'] == 1) {
        $rs += 12.5;
    }
//MÓDULO PIA MENU  7 -----------------------------------------------------------------------------------------------------------
    $result8 = $db->prepare("SELECT *           
                            FROM mod_acolhimento_crianca_plano macp   
                            WHERE macp.acolhimento_crianca_id = ?");
    $result8->bindValue(1, $acolhimento_crianca_id);
    $result8->execute();
    $resultado8 = $result8->fetch(PDO::FETCH_ASSOC);

    if ($resultado8['vf'] == 1) {
        $rs += 12.5;
    }
    //MÓDULO PIA MENU  8 -----------------------------------------------------------------------------------------------------------
    $result7 = $db->prepare("SELECT *        
        FROM mod_acolhimento_crianca_avaliacao maca   
        WHERE maca.acolhimento_crianca_id = ?");
    $result7->bindValue(1, $acolhimento_crianca_id);
    $result7->execute();
    $resultado7 = $result7->fetch(PDO::FETCH_ASSOC);

    if ($resultado7['vf'] == 1) {
        $rs += 12.5;
    }

    return $rs >= 100 ? 100 : $rs;
}

//----------------------------------------------------------------------------------------------------------------------------
function contador_views_emendas($emenda_id, $qtd_views) {

    @session_start();

    $db = Conexao::getInstance();

    if (!isset($_SESSION['user_views_e_' . $emenda_id])) {
        $result = $db->prepare("UPDATE emendas SET qtd_views = ? WHERE id = ?");
        $result->bindValue(1, ($qtd_views + 1));
        $result->bindValue(2, $emenda_id);
        $result->execute();

        $_SESSION['user_views_e_' . $emenda_id] = 1;
    }
}

//----------------------------------------------------------------------------------------------------------------------------
function contador_views($lei_id, $qtd_views) {

    @session_start();

    $db = Conexao::getInstance();

    if (!isset($_SESSION['user_views_' . $lei_id])) {
        $result = $db->prepare("UPDATE leis SET qtd_views = ? WHERE id = ?");
        $result->bindValue(1, ($qtd_views + 1));
        $result->bindValue(2, $lei_id);
        $result->execute();

        $_SESSION['user_views_' . $lei_id] = 1;
    }
}

//----------------------------------------------------------------------------------------------------------------------------
//FUNÇÃO PARA REMOVER SQL DOS PARÂMETROS PASSADOS POR URL
function antiSQL($campo, $adicionaBarras = false, $op = 0, $tipo = 0) {
// remove palavras que contenham sintaxe sql
    $campo = preg_replace("/(from| or | and |alter table|select|insert|delete|update|were|drop table|show tables|#|\*|--|\\\\)/i", "Anti Sql-Injection !", $campo);
    $campo = trim($campo); //limpa espaços vazio
    $campo = strip_tags($campo); //tira tags html e php
    if ($adicionaBarras || !get_magic_quotes_gpc())
        $campo = addslashes($campo);

    if ($op == 1) {
        if ($tipo == 0) {//SE FOR DO TIPO ID
            if (is_numeric($campo)) {
                return $campo;
            } else {
                echo "<script>history.go(-1)</script>";
            }
        } else {//SE FOR DO TIPO TEXTOS
            if (strpos($campo, "Anti Sql-Injection !") !== false) {
                echo "<script>history.go(-1)</script>";
            } else {
                return $campo;
            }
        }
    } else {
        return $campo;
    }
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function vf_cidadao_disponivel($codigo) {

    $vf = true;

    $con = Conexao::getInstance();

//VERIFICANDO EM VISITAS
    $rs = $con->prepare("SELECT sv.id
             FROM sis_visitas sv
             WHERE sv.seg_visitante_id = ? AND sv.situacao = 0");
    $rs->bindValue(1, $codigo);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0) {
        $vf = false;
    }

//VERIFICANDO EM REUNIÕES
    $rs2 = $con->prepare("SELECT srp.id
             FROM sis_reuniao_participantes srp
             WHERE srp.sis_visitante_id = ? AND srp.situacao IN (0,1)");
    $rs2->bindValue(1, $codigo);
    $rs2->execute();

    $rowCount2 = $rs2->rowCount();

    if ($rowCount2 > 0) {
        $vf = false;
    }

//VERIFICANDO EM SOLENIDADES
    $rs3 = $con->prepare("SELECT ssp.id
             FROM sis_solenidade_participantes ssp
             WHERE ssp.sis_visitante_id = ? AND ssp.situacao IN (0,1)");
    $rs3->bindValue(1, $codigo);
    $rs3->execute();

    $rowCount3 = $rs3->rowCount();

    if ($rowCount3 > 0) {
        $vf = false;
    }

    return $vf;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function traduzir_dia_semana($dia) {
    $rs = "";

    if ($dia == 'Monday') {
        $rs = "SEG";
    } else if ($dia == 'Tuesday') {
        $rs = "TER";
    } else if ($dia == 'Wednesday') {
        $rs = "QUA";
    } else if ($dia == 'Thursday') {
        $rs = "QUI";
    } else if ($dia == 'Friday') {
        $rs = "SEX";
    } else if ($dia == 'Saturday') {
        $rs = "SÁB";
    } else if ($dia == 'Sunday') {
        $rs = "DOM";
    }

    return $rs;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function vf_credencial_remover($id) {
    $vf = true;

    $con = Conexao::getInstance();

//VERIFICANDO EM VISITAS
    $rs = $con->prepare("SELECT sv.id
             FROM sis_visitas sv
             WHERE sv.sis_numero_acesso_id = ? AND sv.situacao IN (0,1)");
    $rs->bindValue(1, $id);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0) {
        $vf = false;
    }

//VERIFICANDO EM REUNIÕES
    $rs2 = $con->prepare("SELECT srp.id
             FROM sis_reuniao_participantes srp
             WHERE srp.sis_numero_acesso_id = ? AND srp.situacao IN (0,1)");
    $rs2->bindValue(1, $id);
    $rs2->execute();

    $rowCount2 = $rs2->rowCount();

    if ($rowCount2 > 0) {
        $vf = false;
    }

//VERIFICANDO EM SOLENIDADES
    $rs3 = $con->prepare("SELECT ssp.id
             FROM sis_solenidade_participantes ssp
             WHERE ssp.sis_numero_acesso_id = ? AND ssp.situacao IN (0,1)");
    $rs3->bindValue(1, $id);
    $rs3->execute();

    $rowCount3 = $rs3->rowCount();

    if ($rowCount3 > 0) {
        $vf = false;
    }

    return $vf;
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// NOME: PESQUISAR TABELA
// DESCRIÇÃO: PESQUISAR NO BANCO DE DADOS POR ALGUMA INFORMAÇÃO
function pesquisar_tabela($retorno, $tabela, $campo, $cond, $variavel, $add) {

    $db = Conexao::getInstance();

    $rs = $db->prepare("SELECT $retorno FROM $tabela WHERE $campo $cond ? $add");
    $rs->bindValue(1, $variavel);
    $rs->execute();

    $dados = $rs->fetch(PDO::FETCH_ASSOC);

    return $dados[$retorno];
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// NOME: VERIFICAR NÍVEL
// DESCRIÇÃO: VERIFICA NÍVEL DE ACESSO AO SISTEMA
function ver_nivel($nivel, $redir = '') {
    if (isset($_SESSION['id'])) {
        $erro = false;
        if ($nivel == '')
            $erro = false;
        if (pesquisar_tabela('user_id', 'seg_permissoes', 'user_id', '=', $_SESSION['id'], ' AND nivel IN (SELECT sn.nivel FROM seg_nivel sn WHERE sn.nivel = ' . $nivel . ')')) {
            return true;
        } else {
            if ($redir == '')
                return false;
            else {
                msg('Você não possui permissão para acessar essa área.');
                url(PORTAL_URL . 'admin/painel');
            }
        }
    } else {
        msg('Você não possui permissão para acessar essa área.');
        url("" . PORTAL_URL . "admin/logout");
    }
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// NOME: URL
// DESCRIÇÃO: JOGA O USUÁRIO PARA A URL PASSADA POR PARÂMETRO
function url($end) {
    echo "<script language='javaScript'>window.location.href='$end'</script>";
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// NOME: MOSTRAR MENSAGEM
// DESCRIÇÃO: MOSTRA UMA MENSAGEM PASSADO POR PARÂMETRO
function msg($texto) {
    echo "<script language='javaScript'>window.alert('$texto')</script>";
}

//------------------------------------------------------------------------------
function vf_horarios_utilizados($codigo) {

    $vf = true;

    $con = Conexao::getInstance();

    $rs = $con->prepare("SELECT srhs.id
             FROM sis_reuniao_horarios_sugeridos srhs
             WHERE srhs.sala_horarios_id = ?");
    $rs->bindValue(1, $codigo);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0) {
        $vf = false;
    }

    $rs2 = $con->prepare("SELECT sshs.id
             FROM sis_solenidade_horarios_sugeridos sshs
             WHERE sshs.sala_horarios_id = ?");
    $rs2->bindValue(1, $codigo);
    $rs2->execute();

    $rowCount2 = $rs2->rowCount();

    if ($rowCount2 > 0) {
        $vf = false;
    }

    return $vf;
}

//------------------------------------------------------------------------------
function vf_rem_sala($sala_id) {
    $vf = true;

    $con = Conexao::getInstance();

    $rs = $con->prepare("SELECT sr.id
             FROM sis_reuniao sr
             WHERE sr.sis_sala_id = ?");
    $rs->bindValue(1, $sala_id);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0) {
        $vf = false;
    }

    $rs2 = $con->prepare("SELECT sr.id
             FROM sis_solenidade sr
             WHERE sr.sis_sala_id = ?");
    $rs2->bindValue(1, $sala_id);
    $rs2->execute();

    $rowCount2 = $rs2->rowCount();

    if ($rowCount2 > 0) {
        $vf = false;
    }

    return $vf;
}

//------------------------------------------------------------------------------
function vf_nova_qtd_equip($equip_id) {
    $usado = 0;

    $con = Conexao::getInstance();

    $rs = $con->prepare("SELECT sre.id
             FROM sis_reuniao_equipamentos sre
             LEFT JOIN sis_reuniao AS sr ON sr.id = sre.reuniao_id 
             WHERE sre.equipamento_id = ? AND sr.situacao <> 3 AND sr.situacao <> 4");
    $rs->bindValue(1, $equip_id);
    $rs->execute();

    $usado = $rs->rowCount();

    $rs2 = $con->prepare("SELECT sre.id
             FROM sis_solenidade_equipamentos sre
             LEFT JOIN sis_solenidade AS sr ON sr.id = sre.solenidade_id 
             WHERE sre.equipamento_id = ? AND sr.situacao <> 3 AND sr.situacao <> 4");
    $rs2->bindValue(1, $equip_id);
    $rs2->execute();

    $usado += $rs2->rowCount();

    return $usado;
}

//------------------------------------------------------------------------------
function vf_equip_disponivel($equip_id, $qtd) {

    $con = Conexao::getInstance();

    $usado = 0;

    $rs = $con->prepare("SELECT sre.id
             FROM sis_reuniao_equipamentos sre
             LEFT JOIN sis_reuniao AS sr ON sr.id = sre.reuniao_id 
             WHERE sre.equipamento_id = ? AND sr.situacao <> 3 AND sr.situacao <> 4");
    $rs->bindValue(1, $equip_id);
    $rs->execute();

    $usado = $rs->rowCount();

    $rs2 = $con->prepare("SELECT sre.id
             FROM sis_solenidade_equipamentos sre
             LEFT JOIN sis_solenidade AS sr ON sr.id = sre.solenidade_id 
             WHERE sre.equipamento_id = ? AND sr.situacao <> 3 AND sr.situacao <> 4");
    $rs2->bindValue(1, $equip_id);
    $rs2->execute();

    $usado += $rs2->rowCount();

    if ($usado > $qtd) {
        return true;
    } else {
        return false;
    }
}

//------------------------------------------------------------------------------
function vf_rem_equip($equip_id) {

    $vf = true;

    $con = Conexao::getInstance();

    $rs = $con->prepare("SELECT sre.id
             FROM sis_reuniao_equipamentos sre
             LEFT JOIN sis_reuniao AS sr ON sr.id = sre.reuniao_id 
             WHERE sre.equipamento_id = ? AND sr.situacao <> 3 AND sr.situacao <> 4");
    $rs->bindValue(1, $equip_id);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0) {
        $vf = false;
    }

    $rs2 = $con->prepare("SELECT sre.id
             FROM sis_solenidade_equipamentos sre
             LEFT JOIN sis_solenidade AS sr ON sr.id = sre.solenidade_id 
             WHERE sre.equipamento_id = ? AND sr.situacao <> 3 AND sr.situacao <> 4");
    $rs2->bindValue(1, $equip_id);
    $rs2->execute();

    $rowCount2 = $rs2->rowCount();

    if ($rowCount2 > 0) {
        $vf = false;
    }

    return $vf;
}

//------------------------------------------------------------------------------
function vf_equip_checked_reuniao($reuniao_id, $equip_id) {

    $con = Conexao::getInstance();

    $rs = $con->prepare("SELECT id
             FROM sis_reuniao_equipamentos
             WHERE reuniao_id = ? AND equipamento_id = ?");
    $rs->bindValue(1, $reuniao_id);
    $rs->bindValue(2, $equip_id);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function vf_equip_checked_sala($sala_id, $equip_id) {

    $con = Conexao::getInstance();

    $rs = $con->prepare("SELECT id
             FROM sis_sala_equipamentos
             WHERE sis_sala_id = ? AND sis_equipamentos_id = ?");
    $rs->bindValue(1, $sala_id);
    $rs->bindValue(2, $equip_id);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function vf_equip_checked_solenidade($solenidade_id, $equip_id) {
    $con = Conexao::getInstance();

    $rs = $con->prepare("SELECT id
             FROM sis_solenidade_equipamentos
             WHERE solenidade_id = ? AND equipamento_id = ?");
    $rs->bindValue(1, $solenidade_id);
    $rs->bindValue(2, $equip_id);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function vf_horario_disponivel_rejeitado($data_reuniao, $reuniao_id, $sala_id) {
    $rs = "";

    $db = Conexao::getInstance();

    $result21 = $db->prepare("SELECT srh.id, ssrh.sis_sala_reuniao_id, ssrh.hora_inicial, ssrh.hora_final   
                             FROM sis_reuniao_horarios_sugeridos srh
                             LEFT JOIN sis_sala_reuniao_horarios AS ssrh ON ssrh.id = srh.sala_horarios_id 
                             WHERE srh.status = 1 AND ssrh.sis_sala_reuniao_id = ? AND srh.reuniao_id = ? 
                             GROUP BY ssrh.id
                             ORDER BY ssrh.hora_inicial ASC");
    $result21->bindValue(1, $sala_id);
    $result21->bindValue(2, $reuniao_id);
    $result21->execute();

    while ($horarios21 = $result21->fetch(PDO::FETCH_ASSOC)) {

        $codigo = vf_horas_disponiveis($data_reuniao, $sala_id, $horarios21['hora_inicial'], $horarios21['hora_final']); //Reunião

        $codigo2 = vf_horas_disponiveis_solenidade($data_reuniao, $sala_id, $horarios21['hora_inicial'], $horarios21['hora_final']); //Solenidade

        if ($codigo == 0 && $codigo2 == 0) {
            $rs .= "<li>" . $horarios21['hora_inicial'] . " - " . $horarios21['hora_final'] . "</li>";
        } else {
            if ($codigo != 0) {
                $rs .= novo_horario_sala($data_reuniao, $horarios21['id'], $codigo, $sala_id, $horarios21['hora_inicial'], $horarios21['hora_final']);
            } else if ($codigo2 != 0) {
                $rs .= novo_horario_sala_solenidade($data_reuniao, $horarios21['id'], $codigo2, $sala_id, $horarios21['hora_inicial'], $horarios21['hora_final']);
            }
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function vf_horario_disponivel_rejeitado_solenidade($data_solenidade, $solenidade_id, $sala_id) {
    $rs = "";

    $db = Conexao::getInstance();

    $result23 = $db->prepare("SELECT srh.id, ssrh.sis_sala_reuniao_id, ssrh.hora_inicial, ssrh.hora_final   
                             FROM sis_solenidade_horarios_sugeridos srh
                             LEFT JOIN sis_sala_reuniao_horarios AS ssrh ON ssrh.id = srh.sala_horarios_id 
                             WHERE srh.status = 1 AND ssrh.sis_sala_reuniao_id = ? AND srh.solenidade_id = ? 
                             GROUP BY ssrh.id
                             ORDER BY ssrh.hora_inicial ASC");
    $result23->bindValue(1, $sala_id);
    $result23->bindValue(2, $solenidade_id);
    $result23->execute();

    while ($horarios23 = $result23->fetch(PDO::FETCH_ASSOC)) {

        $codigo2 = vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $horarios23['hora_inicial'], $horarios23['hora_final']); //Solenidade

        $codigo22 = vf_horas_disponiveis($data_solenidade, $sala_id, $horarios23['hora_inicial'], $horarios23['hora_final']); //Reunião

        if ($codigo2 == 0 && $codigo22 == 0) {
            $rs .= "<li>" . $horarios23['hora_inicial'] . " - " . $horarios23['hora_final'] . "</li>";
        } else {
            if ($codigo2 != 0) {
                $rs .= novo_horario_sala_solenidade($data_solenidade, $horarios23['id'], $codigo2, $sala_id, $horarios23['hora_inicial'], $horarios23['hora_final']);
            } else if ($codigo22 != 0) {
                $rs .= novo_horario_sala($data_solenidade, $horarios23['id'], $codigo22, $sala_id, $horarios23['hora_inicial'], $horarios23['hora_final']);
            }
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function horarios_disponiveis_rejeitado($reuniao_id, $sala_id, $hora_inicial, $hora_final) {

    $db = Conexao::getInstance();

    $qtd_rs = 0;

    $result24 = $db->prepare("SELECT * 
                             FROM sis_reuniao_horarios_sugeridos srhs 
                             LEFT JOIN sis_sala_reuniao_horarios AS ssrh ON ssrh.id = srhs.sala_horarios_id  
                             WHERE srhs.status = 1 AND srhs.reuniao_id = ? AND ssrh.sis_sala_reuniao_id = ? 
                             AND ? BETWEEN ssrh.hora_inicial AND ssrh.hora_final
                             AND ? BETWEEN ssrh.hora_inicial AND ssrh.hora_final 
                             ORDER BY ssrh.hora_inicial ASC");
    $result24->bindValue(1, $reuniao_id);
    $result24->bindValue(2, $sala_id);
    $result24->bindValue(3, $hora_inicial);
    $result24->bindValue(4, $hora_final);
    $result24->execute();
    $qtd_rs = $result24->rowCount();

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function horarios_disponiveis_rejeitado_solenidade($solenidade_id, $sala_id, $hora_inicial, $hora_final) {

    $db = Conexao::getInstance();

    $qtd_rs = 0;

    $result25 = $db->prepare("SELECT * 
                             FROM sis_solenidade_horarios_sugeridos srhs 
                             LEFT JOIN sis_sala_reuniao_horarios AS ssrh ON ssrh.id = srhs.sala_horarios_id  
                             WHERE srhs.status = 1 AND srhs.solenidade_id = ? AND ssrh.sis_sala_reuniao_id = ? 
                             AND ? BETWEEN ssrh.hora_inicial AND ssrh.hora_final
                             AND ? BETWEEN ssrh.hora_inicial AND ssrh.hora_final 
                             ORDER BY ssrh.hora_inicial ASC");
    $result25->bindValue(1, $solenidade_id);
    $result25->bindValue(2, $sala_id);
    $result25->bindValue(3, $hora_inicial);
    $result25->bindValue(4, $hora_final);
    $result25->execute();
    $qtd_rs = $result25->rowCount();

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function vf_horario_disponivel_recusar_data($data_horario, $reuniao_id, $sala_id) {
    $rs = "";

    $db = Conexao::getInstance();

    $result26 = $db->prepare("SELECT * 
                             FROM sis_sala_reuniao_horarios ssrh
                             WHERE ssrh.status = 1 AND ssrh.sis_sala_reuniao_id = ?
                             ORDER BY ssrh.hora_inicial ASC");
    $result26->bindValue(1, $sala_id);
    $result26->execute();
    while ($horarios26 = $result26->fetch(PDO::FETCH_ASSOC)) {

        $codigo = vf_horas_disponiveis_data($data_horario, $sala_id, $horarios26['hora_inicial'], $horarios26['hora_final']);

        $codigo2 = vf_horas_disponiveis_data_solenidade($data_horario, $sala_id, $horarios26['hora_inicial'], $horarios26['hora_final']);

        if ($codigo == 0 && $codigo2 == 0 && $horarios26['hora_inicial'] >= date("H:i") && $data_horario >= date('Y-m-d')) {
            $rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $horarios26['id'] . "' class='checkbox-foreach' id='box-" . $reuniao_id . '-' . $horarios26['id'] . "' name='box-" . $reuniao_id . '-' . $horarios26['id'] . "'><label for='box-" . $reuniao_id . '-' . $horarios26['id'] . "'>" . $horarios26['hora_inicial'] . " - " . $horarios26['hora_final'] . "</label></div></div>";
        } else {

            if ($codigo != 0) {
                $rs .= novo_horario_recusar_sala_reuniao($reuniao_id, $data_horario, $horarios26['id'], $codigo, $sala_id, $horarios26['hora_inicial'], $horarios26['hora_final']);
            } else if ($codigo2 != 0) {
                
            }
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function novo_horario_recusar_sala_reuniao($reuniao_id, $data_reuniao, $id, $codigo, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs = "";

    $result2 = $db->prepare("SELECT sr.hora_inicial_agendamento, sr.hora_final_agendamento 
                             FROM sis_reuniao sr
                             WHERE sr.id = ?
                             ORDER BY sr.hora_inicial_agendamento ASC");
    $result2->bindValue(1, $reuniao_id);
    $result2->execute();
    while ($horarios = $result2->fetch(PDO::FETCH_ASSOC)) {
        if (vf_horas_disponiveis($data_reuniao, $sala_id, $hora_inicial, $horarios['hora_inicial_agendamento']) != 0 && $horarios['hora_inicial_agendamento'] > $hora_inicial) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_reuniao >= date('Y-m-d')) {///SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $id . "' class='checkbox-foreach' id='box-" . $reuniao_id . '-' . $id . "' name='box-" . $reuniao_id . '-' . $id . "'><label for='box-" . $reuniao_id . '-' . $id . "'>" . $hora_inicial . " - " . date("H:i", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes")) . "</label></div></div>";
            }
        }

        if (vf_horas_disponiveis($data_reuniao, $sala_id, $horarios['hora_final_agendamento'], $hora_final) != 0 && $horarios['hora_final_agendamento'] < $hora_final) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_reuniao >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $id . "' class='checkbox-foreach' id='box3-" . $reuniao_id . '-' . $id . "' name='box3-" . $reuniao_id . '-' . $id . "'><label for='box3-" . $reuniao_id . '-' . $id . "'>" . date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) . " - " . $hora_final . "</label></div></div>";
            }
        }
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function novo_horario_recusar_sala_solenidade($solenidade_id, $data_solenidade, $id, $codigo, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs = "";

    $result2 = $db->prepare("SELECT sr.hora_inicial_agendamento, sr.hora_final_agendamento 
                             FROM sis_solenidade sr
                             WHERE sr.id = ?
                             ORDER BY sr.hora_inicial_agendamento ASC");
    $result2->bindValue(1, $solenidade_id);
    $result2->execute();
    while ($horarios = $result2->fetch(PDO::FETCH_ASSOC)) {
        if (vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $hora_inicial, $horarios['hora_inicial_agendamento']) != 0 && $horarios['hora_inicial_agendamento'] > $hora_inicial) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_solenidade >= date('Y-m-d')) {///SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $id . "' class='checkbox-foreach' id='box2-" . $solenidade_id . '-' . $id . "' name='box2-" . $solenidade_id . '-' . $id . "'><label for='box2-" . $solenidade_id . '-' . $id . "'>" . $hora_inicial . " - " . date("H:i", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes")) . "</label></div></div>";
            }
        }

        if (vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $horarios['hora_final_agendamento'], $hora_final) != 0 && $horarios['hora_final_agendamento'] < $hora_final) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_solenidade >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $id . "' class='checkbox-foreach' id='box4-" . $solenidade_id . '-' . $id . "' name='box4-" . $solenidade_id . '-' . $id . "'><label for='box4-" . $solenidade_id . '-' . $id . "'>" . date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) . " - " . $hora_final . "</label></div></div>";
            }
        }
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function novo_horario_recusar_sala($reuniao_id, $data_reuniao, $id, $codigo, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs = "";

    $result2 = $db->prepare("SELECT sr.hora_inicial_agendamento, sr.hora_final_agendamento 
                             FROM sis_reuniao sr
                             WHERE sr.id = ?
                             ORDER BY sr.hora_inicial_agendamento ASC");
    $result2->bindValue(1, $codigo);
    $result2->execute();
    while ($horarios = $result2->fetch(PDO::FETCH_ASSOC)) {
        if (vf_horas_disponiveis($data_reuniao, $sala_id, $hora_inicial, $horarios['hora_inicial_agendamento']) != 0 && $horarios['hora_inicial_agendamento'] > $hora_inicial) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_reuniao >= date('Y-m-d')) {///SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $id . "' class='checkbox-foreach' id='box2-" . $reuniao_id . '-' . $id . "' name='box2-" . $reuniao_id . '-' . $id . "'><label for='box2-" . $reuniao_id . '-' . $id . "'>" . $hora_inicial . " - " . date("H:i", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes")) . "</label></div></div>";
            }
        }

        if (vf_horas_disponiveis($data_reuniao, $sala_id, $horarios['hora_final_agendamento'], $hora_final) != 0 && $horarios['hora_final_agendamento'] < $hora_final) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_reuniao >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $id . "' class='checkbox-foreach' id='box4-" . $reuniao_id . '-' . $id . "' name='box4-" . $reuniao_id . '-' . $id . "'><label for='box4-" . $reuniao_id . '-' . $id . "'>" . date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) . " - " . $hora_final . "</label></div></div>";
            }
        }
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function vf_horario_disponivel_recusar_data_solenidade($data_horario, $solenidade_id, $sala_id) {
    $rs = "";

    $db = Conexao::getInstance();

    $result27 = $db->prepare("SELECT * 
                             FROM sis_sala_reuniao_horarios ssrh
                             WHERE ssrh.status = 1 AND ssrh.sis_sala_reuniao_id = ?
                             ORDER BY ssrh.hora_inicial ASC");
    $result27->bindValue(1, $sala_id);
    $result27->execute();
    while ($horarios27 = $result27->fetch(PDO::FETCH_ASSOC)) {

        $codigo = vf_horas_disponiveis_data_solenidade($data_horario, $sala_id, $horarios27['hora_inicial'], $horarios27['hora_final']);

        $codigo2 = vf_horas_disponiveis_data($data_horario, $sala_id, $horarios27['hora_inicial'], $horarios27['hora_final']);

        if ($codigo == 0 && $codigo2 == 0 && $horarios27['hora_inicial'] >= date("H:i") && $data_horario >= date('Y-m-d')) {
            $rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $horarios27['id'] . "' class='checkbox-foreach' id='box2-" . $solenidade_id . '-' . $horarios27['id'] . "' name='box2-" . $solenidade_id . '-' . $horarios27['id'] . "'><label for='box2-" . $solenidade_id . '-' . $horarios27['id'] . "'>" . $horarios27['hora_inicial'] . " - " . $horarios27['hora_final'] . "</label></div></div>";
        } else {
            $rs .= novo_horario_recusar_sala_solenidade($solenidade_id, $data_horario, $horarios27['id'], $codigo, $sala_id, $horarios27['hora_inicial'], $horarios27['hora_final']);
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function vf_horario_disponivel_recusar($reuniao_id, $sala_id) {
    $rs = "";

    $db = Conexao::getInstance();

    $result28 = $db->prepare("SELECT * 
                             FROM sis_sala_reuniao_horarios ssrh
                             WHERE ssrh.status = 1 AND ssrh.sis_sala_reuniao_id = ?
                             ORDER BY ssrh.hora_inicial ASC");
    $result28->bindValue(1, $sala_id);
    $result28->execute();
    while ($horarios28 = $result28->fetch(PDO::FETCH_ASSOC)) {

        $codigo = vf_horas_disponiveis($sala_id, $horarios28['hora_inicial'], $horarios28['hora_final']);

        if ($codigo == 0) {
            $rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $horarios28['id'] . "' class='checkbox-foreach' id='box-" . $reuniao_id . '-' . $horarios28['id'] . "' name='box-" . $reuniao_id . '-' . $horarios28['id'] . "'><label for='box-" . $reuniao_id . '-' . $horarios28['id'] . "'>" . $horarios28['hora_inicial'] . " - " . $horarios28['hora_final'] . "</label></div></div>";
        } else {
//$rs .= novo_horario_sala($horarios['id'], $codigo, $sala_id, $horarios['hora_inicial'], $horarios['hora_final']);
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function vf_horario_disponivel_recusar_solenidade($data_solenidade, $reuniao_id, $sala_id) {
    $rs = "";

    $db = Conexao::getInstance();

    $result29 = $db->prepare("SELECT * 
                             FROM sis_sala_reuniao_horarios ssrh
                             WHERE ssrh.status = 1 AND ssrh.sis_sala_reuniao_id = ?
                             ORDER BY ssrh.hora_inicial ASC");
    $result29->bindValue(1, $sala_id);
    $result29->execute();
    while ($horarios29 = $result29->fetch(PDO::FETCH_ASSOC)) {

        $codigo = vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $horarios29['hora_inicial'], $horarios29['hora_final']);

        if ($codigo == 0) {
            $rs .= "<div class='col-md-6'><div class='boxes'><input type='checkbox' value='" . $horarios29['id'] . "' class='checkbox-foreach' id='box-" . $reuniao_id . '-' . $horarios29['id'] . "' name='box-" . $reuniao_id . '-' . $horarios29['id'] . "'><label for='box-" . $reuniao_id . '-' . $horarios29['id'] . "'>" . $horarios29['hora_inicial'] . " - " . $horarios29['hora_final'] . "</label></div></div>";
        } else {
//$rs .= novo_horario_sala($horarios['id'], $codigo, $sala_id, $horarios['hora_inicial'], $horarios['hora_final']);
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function vf_horario_disponivel($data_reuniao, $sala_id) {
    $rs = "";

    $db = Conexao::getInstance();

    $result30 = $db->prepare("SELECT * 
                             FROM sis_sala_reuniao_horarios ssrh
                             WHERE ssrh.status = 1 AND ssrh.sis_sala_reuniao_id = ?
                             ORDER BY ssrh.hora_inicial ASC");
    $result30->bindValue(1, $sala_id);
    $result30->execute();
    while ($horarios30 = $result30->fetch(PDO::FETCH_ASSOC)) {

        $codigo = vf_horas_disponiveis($data_reuniao, $sala_id, $horarios30['hora_inicial'], $horarios30['hora_final']); //Reunião

        $codigo2 = vf_horas_disponiveis_solenidade($data_reuniao, $sala_id, $horarios30['hora_inicial'], $horarios30['hora_final']); //Solenidade

        if ($codigo == 0 && $codigo2 == 0 && $horarios30['hora_inicial'] >= date("H:i") && $data_reuniao >= date('Y-m-d')) {
            $rs .= "<li>" . $horarios30['hora_inicial'] . " - " . $horarios30['hora_final'] . "</li>";
        } else {
            if ($codigo != 0) {
                $rs .= novo_horario_sala($data_reuniao, $horarios30['id'], $codigo, $sala_id, $horarios30['hora_inicial'], $horarios30['hora_final']);
            } else if ($codigo2 != 0) {
                $rs .= novo_horario_sala_solenidade($data_reuniao, $horarios30['id'], $codigo2, $sala_id, $horarios30['hora_inicial'], $horarios30['hora_final']);
            }
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function vf_horario_disponivel_solenidade($data_solenidade, $sala_id) {
    $rs = "";

    $db = Conexao::getInstance();

    $result2222 = $db->prepare("SELECT * 
                                FROM sis_sala_reuniao_horarios ssrh
                                WHERE ssrh.status = 1 AND ssrh.sis_sala_reuniao_id = ?
                                ORDER BY ssrh.hora_inicial ASC");
    $result2222->bindValue(1, $sala_id);
    $result2222->execute();
    while ($horarios = $result2222->fetch(PDO::FETCH_ASSOC)) {

        $codigo = vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $horarios['hora_inicial'], $horarios['hora_final']); //Solenidade

        $codigo2 = vf_horas_disponiveis($data_solenidade, $sala_id, $horarios['hora_inicial'], $horarios['hora_final']); //Reunião

        if ($codigo == 0 && $codigo2 == 0) {
            if ($horarios['hora_inicial'] >= date("H:i") && $data_solenidade >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $rs .= "<li>" . $horarios['hora_inicial'] . " - " . $horarios['hora_final'] . "</li>";
            }
        } else {
            if ($codigo != 0) {
                $rs .= novo_horario_sala_solenidade($data_solenidade, $horarios['id'], $codigo, $sala_id, $horarios['hora_inicial'], $horarios['hora_final']);
            } else if ($codigo2 != 0) {
                $rs .= novo_horario_sala($data_solenidade, $horarios['id'], $codigo2, $sala_id, $horarios['hora_inicial'], $horarios['hora_final']);
            }
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function novo_horario_sala($data_reuniao, $id, $codigo, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

//16:00 - 23:00

    $qtd_rs = "";

    $result2 = $db->prepare("SELECT sr.hora_inicial_agendamento, sr.hora_final_agendamento 
                             FROM sis_reuniao sr
                             WHERE sr.id = ? 
                             ORDER BY sr.hora_inicial_agendamento ASC");
    $result2->bindValue(1, $codigo);
    $result2->execute();
    while ($horarios = $result2->fetch(PDO::FETCH_ASSOC)) {

        if (vf_horas_disponiveis($data_reuniao, $sala_id, $hora_inicial, $horarios['hora_inicial_agendamento']) != 0 && $horarios['hora_inicial_agendamento'] > $hora_inicial) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_reuniao >= date('Y-m-d')) {///SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<li>" . $hora_inicial . " - " . date("H:i", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes")) . "</li>";
            }
        }

        if (vf_horas_disponiveis($data_reuniao, $sala_id, $horarios['hora_final_agendamento'], $hora_final) != 0 && $horarios['hora_final_agendamento'] < $hora_final) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_reuniao >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<li>" . date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) . " - " . $hora_final . "</li>";
            }
        }
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function vf_novo_horario_sala($data_reuniao, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $rs = 0;

    $result30 = $db->prepare("SELECT * 
                             FROM sis_sala_reuniao_horarios ssrh
                             WHERE ssrh.status = 1 AND ssrh.sis_sala_reuniao_id = ?
                             ORDER BY ssrh.hora_inicial ASC");
    $result30->bindValue(1, $sala_id);
    $result30->execute();
    while ($horarios30 = $result30->fetch(PDO::FETCH_ASSOC)) {

        $codigo = vf_horas_disponiveis($data_reuniao, $sala_id, $horarios30['hora_inicial'], $horarios30['hora_final']); //Reunião

        $codigo2 = vf_horas_disponiveis_solenidade($data_reuniao, $sala_id, $horarios30['hora_inicial'], $horarios30['hora_final']); //Solenidade

        if ($codigo == 0 && $codigo2 == 0 && $horarios30['hora_inicial'] >= date("H:i") && $data_reuniao >= date('Y-m-d')) {
            if ($hora_inicial >= $horarios30['hora_inicial'] && $hora_inicial <= $horarios30['hora_final'] &&
                    $hora_final >= $horarios30['hora_inicial'] && $hora_final <= $horarios30['hora_final']) {
                $rs = $horarios30['id'];
            }
        } else {
            if ($codigo != 0) {
                $rs = x_novo_horario_sala($data_reuniao, $horarios30['id'], $codigo, $sala_id, $horarios30['hora_inicial'], $horarios30['hora_final'], $hora_inicial, $hora_final);
            } else if ($codigo2 != 0) {
                $rs = x_novo_horario_sala_solenidade($data_reuniao, $horarios30['id'], $codigo2, $sala_id, $horarios30['hora_inicial'], $horarios30['hora_final'], $hora_inicial, $hora_final);
            }
        }
    }

    return $rs;
}

//------------------------------------------------------------------------------
function x_novo_horario_sala($data_reuniao, $id, $codigo, $sala_id, $hora_inicial, $hora_final, $h_i, $h_f) {
    $db = Conexao::getInstance();

    $qtd_rs = 0;

    $result2 = $db->prepare("SELECT sr.id, sr.hora_inicial_agendamento, sr.hora_final_agendamento 
                             FROM sis_reuniao sr
                             WHERE sr.id = ? 
                             ORDER BY sr.hora_inicial_agendamento ASC");
    $result2->bindValue(1, $codigo);
    $result2->execute();
    while ($horarios = $result2->fetch(PDO::FETCH_ASSOC)) {

        if (vf_horas_disponiveis($data_reuniao, $sala_id, $hora_inicial, $horarios['hora_inicial_agendamento']) != 0 && $horarios['hora_inicial_agendamento'] > $hora_inicial) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_reuniao >= date('Y-m-d')) {///SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                if ($h_i >= $hora_inicial && $h_i <= date("H:i", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes")) &&
                        $h_f >= $hora_inicial && $h_f <= date("H:i", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes"))) {
                    $qtd_rs = $horarios['id'];
                }
            }
        }

        if (vf_horas_disponiveis($data_reuniao, $sala_id, $horarios['hora_final_agendamento'], $hora_final) != 0 && $horarios['hora_final_agendamento'] < $hora_final) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_reuniao >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                if ($h_i >= date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) && $h_i <= $hora_final &&
                        $h_f >= date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) && $h_f <= $hora_final) {
                    $qtd_rs = $horarios['id'];
                }
            }
        }
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function x_novo_horario_sala_solenidade($data_solenidade, $id, $codigo, $sala_id, $hora_inicial, $hora_final, $h_i, $h_f) {
    $db = Conexao::getInstance();

    $qtd_rs = 0;

    $result2 = $db->prepare("SELECT sr.id, sr.hora_inicial_agendamento, sr.hora_final_agendamento 
                             FROM sis_solenidade sr
                             WHERE sr.id = ? 
                             ORDER BY sr.hora_inicial_agendamento ASC");
    $result2->bindValue(1, $codigo);
    $result2->execute();
    while ($horarios = $result2->fetch(PDO::FETCH_ASSOC)) {

        if (vf_horas_disponiveis($data_solenidade, $sala_id, $hora_inicial, $horarios['hora_inicial_agendamento']) != 0 && $horarios['hora_inicial_agendamento'] > $hora_inicial) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_solenidade >= date('Y-m-d')) {///SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                if ($h_i >= $hora_inicial && $h_i <= date("H:i", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes")) &&
                        $h_f >= $hora_inicial && $h_f <= date("H:i", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes"))) {
                    $qtd_rs = $horarios['id'];
                }
            }
        }

        if (vf_horas_disponiveis($data_solenidade, $sala_id, $horarios['hora_final_agendamento'], $hora_final) != 0 && $horarios['hora_final_agendamento'] < $hora_final) {
            if (date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_solenidade >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                if ($h_i >= date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) && $h_i <= $hora_final &&
                        $h_f >= date("H:i", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) && $h_f <= $hora_final) {
                    $qtd_rs = $horarios['id'];
                }
            }
        }
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function vf_novo_horario_sala_solenidade(
        $data_solenidade, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs = 0;

    $result2 = $db->prepare("SELECT sr.hora_inicial_agendamento, sr.hora_final_agendamento
            FROM sis_solenidade sr
            WHERE sr.sis_sala_id = ? AND sr.situacao IN(0, 1) AND sr.data_solenidade = ?
            ORDER BY sr.hora_inicial_agendamento ASC");
    $result2->bindValue(1, $sala_id);
    $result2->bindValue(2, $data_solenidade);
    $result2->execute();
    while ($horarios = $result2->fetch(PDO::FETCH_ASSOC)) {

        if (vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $hora_inicial, $horarios['hora_inicial_agendamento']) != 0 && $horarios['hora_inicial_agendamento'] > $hora_inicial) {
            if (date("H:i ", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_solenidade >= date('Y-m-d')) {///SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs = 1;
            }
        }

        if (vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $horarios['hora_final_agendamento'], $hora_final) != 0 && $horarios['hora_final_agendamento'] < $hora_final) {
            if (date("H:i ", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_solenidade >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs = 1;
            }
        }
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function novo_horario_sala_solenidade(
        $data_solenidade, $id, $codigo, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs = "";

    $result2 = $db->prepare("SELECT sr.hora_inicial_agendamento, sr.hora_final_agendamento
            FROM sis_solenidade sr
            WHERE sr.sis_sala_id = ? AND sr.situacao IN(0, 1) AND sr.data_solenidade = ?
            ORDER BY sr.hora_inicial_agendamento ASC");
    $result2->bindValue(1, $sala_id);
    $result2->bindValue(2, $data_solenidade);
    $result2->execute();
    while ($horarios = $result2->fetch(PDO::FETCH_ASSOC)) {

        if (vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $hora_inicial, $horarios['hora_inicial_agendamento']) != 0 && $horarios['hora_inicial_agendamento'] > $hora_inicial) {
            if (date("H:i ", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_solenidade >= date('Y-m-d')) {///SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<li>" . $hora_inicial . " - " . date("H:i ", strtotime("" . $horarios['hora_inicial_agendamento'] . " - 1 minutes")) . " < /li>";
            }
        }

        if (vf_horas_disponiveis_solenidade($data_solenidade, $sala_id, $horarios['hora_final_agendamento'], $hora_final) != 0 && $horarios['hora_final_agendamento'] < $hora_final) {
            if (date("H:i ", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) >= date("H:i") && $data_solenidade >= date('Y-m-d')) {//SOMENTE SE A HORA FINAL DE DATA FOR MAIOR QUE A HORA ATUAL E DATA DE HOJE
                $qtd_rs .= "<li>" . date("H:i ", strtotime("" . $horarios['hora_final_agendamento'] . " + 1 minutes")) . " - " . $hora_final . "</li>";
            }
        }
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function vf_horas_disponiveis(
        $data_reuniao, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs = 0;
//14 a 21
    $result31 = $db->prepare("SELECT MAX(sr.id) AS id  
                             FROM sis_reuniao sr
                             WHERE sr.status = 1 AND sr.situacao IN (0, 1) AND sr.sis_sala_id = ? 
                             AND sr.hora_inicial_agendamento BETWEEN ? AND ? AND sr.data_reuniao = ? 
                             OR sr.status = 1 AND sr.situacao IN (0, 1) AND sr.sis_sala_id = ? 
                             AND sr.hora_final_agendamento BETWEEN ? AND ? AND sr.data_reuniao = ?");

    $result31->bindValue(1, $sala_id);
    $result31->bindValue(2, $hora_inicial);
    $result31->bindValue(3, $hora_final);
    $result31->bindValue(4, $data_reuniao);
    $result31->bindValue(5, $sala_id);
    $result31->bindValue(6, $hora_inicial);
    $result31->bindValue(7, $hora_final);
    $result31->bindValue(8, $data_reuniao);
    $result31->execute();

    $dados_reuniao31 = $result31->fetch(PDO::FETCH_ASSOC);

    if ($result31->rowCount() > 0) {
        $qtd_rs = $dados_reuniao31['id'];
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function vf_horas_disponiveis_solenidade(
        $data_solenidade, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs222 = 0;

    $result222 = $db->prepare("SELECT MAX(sr.id) AS id   
                               FROM sis_solenidade sr
                               WHERE sr.status = 1 AND sr.situacao IN (0, 1) AND sr.sis_sala_id = ? 
                               AND sr.hora_inicial_agendamento BETWEEN ? AND ? AND sr.data_solenidade = ? 
                               OR sr.status = 1 AND sr.situacao IN (0, 1) AND sr.sis_sala_id = ? 
                               AND sr.hora_final_agendamento BETWEEN ? AND ? AND sr.data_solenidade = ?");
    $result222->bindValue(1, $sala_id);
    $result222->bindValue(2, $hora_inicial);
    $result222->bindValue(3, $hora_final);
    $result222->bindValue(4, $data_solenidade);
    $result222->bindValue(5, $sala_id);
    $result222->bindValue(6, $hora_inicial);
    $result222->bindValue(7, $hora_final);
    $result222->bindValue(8, $data_solenidade);
    $result222->execute();

    $dados_reuniao222 = $result222->fetch(PDO::FETCH_ASSOC);

    if ($result222->rowCount() > 0) {
        $qtd_rs222 = $dados_reuniao222['id'];
    }

    return $qtd_rs222;
}

//------------------------------------------------------------------------------
function vf_horas_disponiveis_data(
        $data_horario, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs = 0;

    $result32 = $db->prepare("SELECT sr.id
            FROM sis_reuniao sr
            WHERE sr.status = 1 AND sr.situacao IN(0, 1) AND sr.sis_sala_id = ?
            AND sr.hora_inicial_agendamento BETWEEN ? AND ? AND sr.data_reuniao = ?
            OR sr.status = 1 AND sr.situacao IN(0, 1) AND sr.sis_sala_id = ?
            AND sr.hora_final_agendamento BETWEEN ? AND ? AND sr.data_reuniao = ?");
    $result32->bindValue(1, $sala_id);
    $result32->bindValue(2, $hora_inicial);
    $result32->bindValue(3, $hora_final);
    $result32->bindValue(4, $data_horario);
    $result32->bindValue(5, $sala_id);
    $result32->bindValue(6, $hora_inicial);
    $result32->bindValue(7, $hora_final);
    $result32->bindValue(8, $data_horario);
    $result32->execute();

    $dados_reuniao32 = $result32->fetch(PDO::FETCH_ASSOC);

    if ($result32->rowCount() > 0) {
        $qtd_rs = $dados_reuniao32['id'];
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function vf_horas_disponiveis_data_solenidade(
        $data_horario, $sala_id, $hora_inicial, $hora_final) {
    $db = Conexao::getInstance();

    $qtd_rs = 0;

    $result33 = $db->prepare("SELECT sr.id
            FROM sis_solenidade sr
            WHERE sr.status = 1 AND sr.situacao IN(0, 1) AND sr.sis_sala_id = ?
            AND sr.hora_inicial_agendamento BETWEEN ? AND ? AND sr.data_solenidade = ?
            OR sr.status = 1 AND sr.situacao IN(0, 1) AND sr.sis_sala_id = ?
            AND sr.hora_final_agendamento BETWEEN ? AND ? AND sr.data_solenidade = ?");
    $result33->bindValue(1, $sala_id);
    $result33->bindValue(2, $hora_inicial);
    $result33->bindValue(3, $hora_final);
    $result33->bindValue(4, $data_horario);
    $result33->bindValue(5, $sala_id);
    $result33->bindValue(6, $hora_inicial);
    $result33->bindValue(7, $hora_final);
    $result33->bindValue(8, $data_horario);
    $result33->execute();

    $dados_reuniao33 = $result33->fetch(PDO::FETCH_ASSOC);

    if ($result33->rowCount() > 0) {
        $qtd_rs = $dados_reuniao33['id'];
    }

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function horarios_disponiveis(
        $sala_id, $hora_inicial, $hora_final) {

    $db = Conexao::getInstance();

    $qtd_rs = 0;

    $result34 = $db->prepare("SELECT *
            FROM sis_sala_reuniao_horarios ssrh
            WHERE ssrh.status = 1 AND ssrh.sis_sala_reuniao_id = ?
            AND ? BETWEEN ssrh.hora_inicial AND ssrh.hora_final
            AND ? BETWEEN ssrh.hora_inicial AND ssrh.hora_final
            ORDER BY ssrh.hora_inicial ASC");
    $result34->bindValue(1, $sala_id);
    $result34->bindValue(2, $hora_inicial);
    $result34->bindValue(3, $hora_final);
    $result34->execute();

    $qtd_rs = $result34->rowCount();

    return $qtd_rs;
}

//------------------------------------------------------------------------------
function obter_numero_acesso_disponivel() {


    $numero = 0;

    $db = Conexao::getInstance();

    $result = $db->prepare("SELECT id
            FROM sis_numero_acesso
            WHERE situacao = 1");
    $result->execute();

    $qtd = $result->rowCount();

    $dados_numero = $result->fetch(PDO::FETCH_ASSOC);

    if ($qtd > 0) {
        $numero = $dados_numero['id'];
    }

    return $numero;
}

//------------------------------------------------------------------------------
function vf_encaminhar($codigo) {

    $vf = false;

    $db = Conexao::getInstance();

    $result = $db->prepare("SELECT MAX(visita_id) AS visita_id
                             FROM sis_visitas_acompanhantes
                             WHERE visitante_id = ?");
    $result->bindValue(1, $codigo);
    $result->execute();

    $dados_acomp = $result->fetch(PDO::FETCH_ASSOC);

    $result3 = $db->prepare("SELECT id
                             FROM sis_visitas
                             WHERE id = ? AND situacao IN (0,1)");
    $result3->bindValue(1, $dados_acomp['visita_id']);
    $result3->execute();

    $total3 = $result3->rowCount();

    if ($total3 > 0) {
        $vf = true;
    }

    $result4 = $db->prepare("SELECT id
                             FROM sis_visitas
                             WHERE seg_visitante_id = ? AND situacao = 1");
    $result4->bindValue(1, $codigo);
    $result4->execute();

    $total4 = $result4->rowCount();

    if ($total4 > 0) {
        $vf = true;
    }

    return $vf;
}

//------------------------------------------------------------------------------
function situacao_visita($codigo) {
    if ($codigo == 0) {
        return "Aberto";
    } else if ($codigo == 1) {
        return "Em andamento";
    } else if ($codigo == 2) {
        return "Finalizado";
    }
}

//------------------------------------------------------------------------------
function vf_numero_acesso_visita($numero_id) {

    $db = Conexao::getInstance();

    $result3 = $db->prepare("SELECT id
                             FROM sis_visitas
                             WHERE sis_numero_acesso_id = ? AND situacao IN (0,1)");
    $result3->bindValue(1, $numero_id);
    $result3->execute();
    $total = $result3->rowCount();

    if ($total > 0) {
        return false;
    } else {
        return true;
    }
}

//------------------------------------------------------------------------------
function vf_visita($id) {

    $vf = false;

    $db = Conexao::getInstance();

    $result3 = $db->prepare("SELECT id
                             FROM sis_visitas
                             WHERE seg_visitante_id = ? AND situacao IN (0, 1)");
    $result3->bindValue(1, $id);
    $result3->execute();
    $total = $result3->rowCount();

    if ($total > 0) {
        $vf = true;
    }

    $result4 = $db->prepare("SELECT id
                             FROM sis_reuniao_participantes
                             WHERE sis_visitante_id = ? AND situacao IN (0, 1)");
    $result4->bindValue(1, $id);
    $result4->execute();
    $total4 = $result4->rowCount();

    if ($total4 > 0) {
        $vf = true;
    }

    $result5 = $db->prepare("SELECT id
                             FROM sis_solenidade_participantes
                             WHERE sis_visitante_id = ? AND situacao IN (0, 1)");
    $result5->bindValue(1, $id);
    $result5->execute();
    $total5 = $result5->rowCount();

    if ($total5 > 0) {
        $vf = true;
    }

    return $vf;
}

//------------------------------------------------------------------------------
function referencia_disponibilidade($id) {

    $db = Conexao::getInstance();

    //QUANTIDADE DE SERVIDORES COM A REFERENCIA ESCOLHIDA
    $result3 = $db->prepare("SELECT id
                             FROM seg_servidores
                             WHERE seg_cec_id = ?");
    $result3->bindValue(1, $id);
    $result3->execute();
    $total_ref = $result3->rowCount();

    //QUANTIDADE MAXIMA DE REFERENCIAS PERMITIDAS
    $qtd_ref = pesquisar("qtd", "seg_cec", "id", "=", $id, "");

    if (($total_ref + 1) > $qtd_ref) {
        return false;
    } else {
        return true;
    }
}

//------------------------------------------------------------------------------
function vf_rem_acao($acao_id) {

    $con = Conexao::getInstance();

    $rs0 = $con->prepare("SELECT id
             FROM seg_modulo_objeto_acao
             WHERE acao_id = ?");
    $rs0->bindValue(1, $acao_id);
    $rs0->execute();

    $modulo_objeto_acao = $rs0->fetch(PDO::FETCH_ASSOC);

    $rs = $con->prepare("SELECT id
             FROM seg_usuario_modulo_objeto_acao
             WHERE modulo_objeto_acao_id = ?");
    $rs->bindValue(1, $modulo_objeto_acao['id']);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function vf_rem_autor($autor_id) {

    $con = Conexao::getInstance();

    $rs = $con->prepare("SELECT id
             FROM seg_servidores
             WHERE autor_indicacao = ?");
    $rs->bindValue(1, $autor_id);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function vf_rem_objeto($objeto_id) {

    $con = Conexao::getInstance();

    $rs0 = $con->prepare("SELECT id
             FROM seg_modulo_objeto_acao
             WHERE objeto_id = ?");
    $rs0->bindValue(1, $objeto_id);
    $rs0->execute();

    $modulo_objeto_acao = $rs0->fetch(PDO::FETCH_ASSOC);

    $rs = $con->prepare("SELECT id
             FROM seg_usuario_modulo_objeto_acao
             WHERE modulo_objeto_acao_id = ?");
    $rs->bindValue(1, $modulo_objeto_acao['id']);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function vf_rem_cargo($cargo_id) {

    $con = Conexao::getInstance();

    $rs0 = $con->prepare("SELECT id
             FROM seg_servidores
             WHERE seg_cec_id = ?");
    $rs0->bindValue(1, $cargo_id);
    $rs0->execute();

    $rowCount = $rs0->rowCount();

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function vf_rem_orgao($orgao_id) {

    $con = Conexao::getInstance();

    $rs0 = $con->prepare("SELECT id
             FROM seg_servidores
             WHERE unidade_organizacional_id = ?");
    $rs0->bindValue(1, $orgao_id);
    $rs0->execute();

    $rowCount0 = $rs0->rowCount();

    $rs1 = $con->prepare("SELECT id
             FROM seg_usuario
             WHERE unidade_organizacional_id = ?");
    $rs1->bindValue(1, $orgao_id);
    $rs1->execute();

    $rowCount1 = $rs1->rowCount();

    $rowCount = $rowCount0 + $rowCount1;

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function vf_rem_modulo($modulo_id) {

    $con = Conexao::getInstance();

    $rs0 = $con->prepare("SELECT id
             FROM seg_modulo_objeto_acao
             WHERE modulo_id = ?");
    $rs0->bindValue(1, $modulo_id);
    $rs0->execute();

    $modulo_objeto_acao = $rs0->fetch(PDO::FETCH_ASSOC);

    $rs = $con->prepare("SELECT id
             FROM seg_usuario_modulo_objeto_acao
             WHERE modulo_objeto_acao_id = ?");
    $rs->bindValue(1, $modulo_objeto_acao['id']);
    $rs->execute();

    $rowCount = $rs->rowCount();

    if ($rowCount > 0)
        return true;
    else
        return false;
}

//------------------------------------------------------------------------------
function preparar_array_sql($array) {
    $resultado = "";
    $novo = $array;
    rsort($novo);
    $novo = array_unique($novo);
    foreach ($novo AS $obj => $val) {
        $resultado .= "'$val',";
    }

    return substr($resultado, 0, -1);
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA VERIFICAR A CONEXÃO EXTERNA DE URL
function endereco_existe($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $content = curl_exec($ch);
    $info = curl_getinfo($ch);

    if ($info['http_code'] == 200) {
        return true;
    } else {
        return false;
    }
}

//------------------------------------------------------------------------------
function formato_arquivo($arquivo) {
    $tipo = ctexto(end(explode(".", $arquivo)), 'min');

    if ($tipo == 'pdf') {
        return 'ic-pdf';
    } else if ($tipo == 'excel' || $tipo == 'xls') {
        return 'ic-xls';
    } else if ($tipo == 'doc' || $tipo == 'docx') {
        return 'ic-docx';
    } else if ($tipo == 'pptx') {
        return 'ic-ppt';
    } else if ($tipo == 'jpg' || $tipo == 'png' || $tipo == 'jpg' || $tipo == 'jpeg' || $tipo == 'gif') {
        return 'ic-jpg';
    } else if ($tipo == 'mp3' || $tipo == 'wav') {
        return 'ic-mp3';
    } else if ($tipo == 'avi' || $tipo == 'mp4' || $tipo == 'mkv') {
        return 'ic-avi';
    } else if ($tipo == 'zip' || $tipo == 'rar') {
        return 'ic-zip';
    } else {
        return 'ic-outros';
    }
}

//------------------------------------------------------------------------------
function vf_tipo_pdf($arquivo) {
    $erro = true;

    $formatoArquivo = strrchr($arquivo, ".");

    if ($formatoArquivo == '.pdf' || $formatoArquivo == '.PDF' || $formatoArquivo == 'pdf' || $formatoArquivo == 'PDF') {
        $erro = false;
    }

    return $erro;
}

//------------------------------------------------------------------------------
function resume($var, $limite) {
    // Se o texto for maior que o limite, ele corta o texto e adiciona 3 pontinhos.
    if (strlen($var) > $limite) {
        $var = substr($var, 0, $limite);
        $var = trim($var) . "...";
    }
    return $var;
}

//------------------------------------------------------------------------------
function vf_online($id) {
    // SE O USUÁRIO NÃO REALIZAR NENHUMA AÇÃO EM 30 MINUTOS, ENTÃO ELE É CONSIDERADO COMO OFFLINE
    $db = Conexao::getInstance();

    $rs = $db->prepare(" SELECT id 
             FROM seg_sessao
             WHERE usuario_id = ? AND DATE(atualizacao) = DATE(NOW()) AND HOUR(atualizacao) = HOUR(NOW())
             AND MINUTE(atualizacao) >= (MINUTE(NOW())-30)"); // 30 MINUTOS PASSADO COMO PARÂMETRO
    $rs->bindValue(1, $id);
    $rs->execute();

    if (is_numeric($rs->rowCount()) && $rs->rowCount() > 0) {
        return true;
    } else {
        return false;
    }
}

//------------------------------------------------------------------------------
function info_usuario($id_usuario) {
    $db = Conexao::getInstance();

    $rs = $db->prepare("SELECT u.nome, o.sigla 
            FROM seg_usuario AS u 
            LEFT JOIN bsc_unidade_organizacional AS o ON o.id = u.unidade_organizacional_id
            WHERE u.id = ?");
    $rs->bindValue(1, $id_usuario);
    $rs->execute();
    $usuario = $rs->fetch(PDO::FETCH_ASSOC);

    return "O usuário <span class='text-info'>" . $usuario['nome'] . " (" . $usuario['sigla'] . ")</span>";
}

//------------------------------------------------------------------------------
function vf_usuario_pagina($pagina) {
    $vf = 0;

    @session_start();

    $db = Conexao::getInstance();

    $rs = $db->prepare("SELECT usuario_id FROM seg_sessao WHERE usuario_id <> ? AND pagina = ? ORDER BY atualizacao DESC");
    $rs->bindValue(1, $_SESSION['id']);
    $rs->bindValue(2, $pagina);
    $rs->execute();

    while ($sessao = $rs->fetch(PDO::FETCH_ASSOC)) {
        if (vf_on_usuario($sessao['usuario_id'])) {
            $vf = $sessao['usuario_id'];
        }
    }
    return $vf;
}

//------------------------------------------------------------------------------
function inserir_sessao() {
    @session_start();
    $db = Conexao::getInstance();
    if (Url::getURL(4) != 'css' && Url::getURL(3) != 'media' && Url::getURL(4) != 'js') {
        $rs = $db->prepare("UPDATE seg_sessao SET atualizacao = NOW(), pagina = ? WHERE usuario_id = ?");
        $rs->bindValue(1, Url::getURL(3) . "/" . Url::getURL(4));
        $rs->bindValue(2, $_SESSION['id']);
        $rs->execute();
    }
}

//------------------------------------------------------------------------------
function vf_usuario_login() {
    if (isset($_SESSION['id'])) {
        inserir_sessao(); // INSERI NA SESSÃO A DATA E HORA DA ÚLTIMA AÇÃO DO USUÁRIO
    } else if (Url::getURL(0) != 'login') {
        echo "<script>window.location = '" . PORTAL_URL . "logout';</script>";
        exit();
    }
}

//------------------------------------------------------------------------------
function vf_on_usuario($id) {
    $db = Conexao::getInstance();
    $result = $db->prepare("SELECT id, online, status from seg_usuario WHERE id = ?");
    $result->bindValue(1, $id);
    $result->execute();
    while ($usuario = $result->fetch(PDO::FETCH_ASSOC)) {
        if ($usuario['online'] == 1 && vf_online($usuario['id']))
            return true;
        if ($usuario['status'] == 0)
            return false;
    }
}

//------------------------------------------------------------------------------
// FUNÇÃO QUE RECEBE UM ARRAY E ARRUMA TODOS OS DADOS PARA SEREM PEGOS
function retorna_campos($post) {
    $fields = explode("&", $post);
    foreach ($fields as $field) {
        $field_key_value = explode("=", $field);
        $key = ($field_key_value[0]);
        $value = ($field_key_value[1]);
        if ($value != '')
            $data[$key] = (urldecode($value));
    }
    return $data;
}

//------------------------------------------------------------------------------
// MÉTOD PARA VALIDAR O CPF
function valida_cpf($cpfx) {
    $cpf = "";
    $guard = "";

    for ($i = 0; ($i < 14); $i++) {
        if ($cpfx[$i] != '.' && $cpfx[$i] != '-') {
            $cpf += $cpfx[$i];
            $guard = "$guard$cpfx[$i]";
        }
    }

    $cpf = $guard;

    // Verifica se o cpf possui números
    if (!is_numeric($cpf)) {
        $status = false;
    } else {
        // VERIFICA
        if (($cpf == '11111111111') || ($cpf == '22222222222') || ($cpf == '33333333333') || ($cpf == '44444444444') || ($cpf == '55555555555') || ($cpf == '66666666666') || ($cpf == '77777777777') || ($cpf == '88888888888') || ($cpf == '99999999999') || ($cpf == '00000000000')) {
            $status = false;
        } else {
            // PEGA O DIGITO VERIFIACADOR
            $dv_informado = substr($cpf, 9, 2);

            for ($i = 0; $i <= 8; $i++) {
                $digito[$i] = substr($cpf, $i, 1);
            }

            // CALCULA O VALOR DO 10º DIGITO DE VERIFICAÇÂO
            $posicao = 10;
            $soma = 0;

            for ($i = 0; $i <= 8; $i++) {
                $soma = $soma + $digito[$i] * $posicao;
                $posicao = $posicao - 1;
            }

            $digito[9] = $soma % 11;

            if ($digito[9] < 2) {
                $digito[9] = 0;
            } else {
                $digito[9] = 11 - $digito[9];
            }

            // CALCULA O VALOR DO 11º DIGITO DE VERIFICAÇÃO
            $posicao = 11;
            $soma = 0;

            for ($i = 0; $i <= 9; $i++) {
                $soma = $soma + $digito[$i] * $posicao;
                $posicao = $posicao - 1;
            }

            $digito[10] = $soma % 11;

            if ($digito[10] < 2) {
                $digito[10] = 0;
            } else {
                $digito[10] = 11 - $digito[10];
            }

            // VERIFICA SE O DV CALCULADO É IGUAL AO INFORMADO
            $dv = $digito[9] * 10 + $digito[10];
            if ($dv != $dv_informado) {
                $status = false;
            } else
                $status = true;
        } // FECHA ELSE
    } // FECHA ELSE(is_numeric)
    return $status;
}

//------------------------------------------------------------------------------
// MÉTODO PARA REALIZAR UMA PESQUISA DE COMPARAÇÃO NO BANCO DE DADOS
function pesquisar($retorno, $tabela, $campo, $cond, $variavel, $add) {
    $db = Conexao::getInstance();

    $rs = $db->prepare("SELECT $retorno FROM $tabela WHERE $campo $cond ? $add");
    $rs->bindValue(1, $variavel);
    $rs->execute();
    $dados = $rs->fetch(PDO::FETCH_ASSOC);

    return $dados[$retorno];
}

//------------------------------------------------------------------------------
// MÉTODO PARA REALIZAR UMA PESQUISA DE COMPARAÇÃO NO BANCO DE DADOS
function pesquisar2($retorno, $tabela, $campo, $cond, $variavel, $campo2, $cond2, $variavel2, $add) {
    $db = Conexao::getInstance();

    $rs = $db->prepare("SELECT $retorno FROM $tabela WHERE $campo $cond ? AND $campo2 $cond2 ? $add");
    $rs->bindValue(1, $variavel);
    $rs->bindValue(2, $variavel2);
    $rs->execute();
    $dados = $rs->fetch(PDO::FETCH_ASSOC);

    return $dados[$retorno];
}

//------------------------------------------------------------------------------
// Método para verificar se o email informado é do ac.gov.br
function VfEmailAc($email) {
    $emailDigitado = ""; // vai guardar tudo que vem depois do @ para depois comparar
    $vf = false;
    $emailvf = false;

    for ($k = 0; ($k < strlen($email)); $k++) {
        if ($email[$k] == '@' || $vf == true) {
            $emailDigitado = "$emailDigitado$email[$k]";
            $vf = true;
        }
    }
    if ($emailDigitado == "@ac.gov.br") {
        $emailvf = true;
        return true;
    } else if ($emailvf == false) {
        return false;
    }
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O TIPO DE ACESSO
function tipo_acesso($codigo) {
    if ($codigo == 1)
        return "A";
    else
        return "B";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O STATUS
function status($codigo) {
    if ($codigo == 1)
        return "Ativo";
    else
        return "Inativo";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O STATUS
function status_visualizacao($codigo) {
    if ($codigo == 1)
        return "Visualizado";
    else
        return "Não Visualizado";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR A SITUAÇÃO
function situacao($codigo) {
    if ($codigo == 0)
        return "Cadastrando";
    else if ($codigo == 1)
        return "Análise";
    else if ($codigo == 2)
        return "Confirmado";
    else
        return "Cadastrando";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR A SITUAÇÃO
function situacao_reuniao($codigo) {
    if ($codigo == 0)
        return "Aguardando Confirmação";
    else if ($codigo == 1)
        return "Confirmado";
    else if ($codigo == 2)
        return "Finalizado";
    else if ($codigo == 3)
        return "Indisponível";
    else if ($codigo == 4)
        return "Cancelado";
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O STATUS
function status_inverso($codigo) {
    if (strtoupper($codigo) == "ATIVO")
        return 1;
    else if (strtoupper($codigo) == "INATIVO")
        return 0;
    else
        return 2;
}

//------------------------------------------------------------------------------
// FUNÇÃO PARA RETORNAR O ESTADO DE UM MUNICÍPIO
function estado_municipio($municipio) {
    if (is_numeric($municipio)) {
        $con = Conexao::getInstance();

        $rs = $con->prepare("SELECT estado_id FROM bsc_cidade WHERE id = ?");
        $rs->bindValue(1, $municipio);
        $rs->execute();
        $dados = $rs->fetch(PDO::FETCH_ASSOC);

        return $dados['estado_id'];
    } else {
        return "";
    }
}

//------------------------------------------------------------------------------
// CALCULA A DIFERENÇA ENTRE MESES ENTRE DUAS DATAS
function diff_data_meses($inicio, $fim) {
    if ($inicio != "00/00/0000 00:00:00" && $fim != "00/00/0000 00:00:00" && $inicio != "0000-00-00 00:00:00" && $fim != "0000-00-00 00:00:00") {
        // CONVERTE AS DATAS PARA O FORMATO AMERICANO
        $inicio = explode('/', $inicio);
        $inicio = "{$inicio[2]}-{$inicio[1]}-{$inicio[0]}";

        $fim = explode('/', $fim);
        $fim = "{$fim[2]}-{$fim[1]}-{$fim[0]}";

        // AGORA CONVERTEMOS A DATA PARA UM INTEIRO
        // QUE REPRESENTA A DATA E É PASSÍVEL DE OPERAÇÕES SIMPLES
        // COMO SUBITRAÇÃO E ADIÇÃO
        $inicio = strtotime($inicio);
        $fim = strtotime($fim);

        // CALCULA A DIFERENÇA ENTRE AS DATAS
        $intervalo = $fim - $inicio;

        $meses = floor(($intervalo / (30 * 60 * 60 * 24)));

        if ($meses > 1) {
            return "$meses meses";
        } else if ($meses == 1) {
            return "$meses mês";
        } else if ($meses == 0) {
            return "Esse mês";
        } else if ($meses < 0) {
            if ($meses < - 1) {
                return "Atrasado " . abs($meses) . " meses";
            } else {
                return "Atrasado " . abs($meses) . " mês";
            }
        }
    } else {
        return "";
    }
}

//------------------------------------------------------------------------------
// CALCULA A DIFERENÇA ENTRE DIAS ENTRE DUAS DATAS
function diff_data_dias($data_inicial, $data_final) {
    if ($data_inicial != "00/00/0000 00:00:00" && $data_final != "00/00/0000 00:00:00" && $data_inicial != "0000-00-00 00:00:00" && $data_final != "0000-00-00 00:00:00") {

        $diferenca = strtotime($data_final) - strtotime($data_inicial);

        $dias = floor($diferenca / (60 * 60 * 24));

        if ($dias > 1) {
            return "$dias Dias";
        } else if ($dias == 1) {
            return "$dias Dia";
        } else if ($dias == 0) {
            return "Hoje";
        } else if ($dias < 0) {
            if ($dias < - 1) {
                return "" . abs($dias) . " Dias";
            } else {
                return "" . abs($dias) . " Dia";
            }
        }
    } else {
        return "";
    }
}

//------------------------------------------------------------------------------
function diff_data_dias2($data_inicial, $data_final) {
    if ($data_inicial != "00/00/0000 00:00:00" && $data_final != "00/00/0000 00:00:00" && $data_inicial != "0000-00-00 00:00:00" && $data_final != "0000-00-00 00:00:00") {

        $diferenca = strtotime($data_final) - strtotime($data_inicial);

        $dias = floor($diferenca / (60 * 60 * 24));

        if ($dias > 1) {
            return $dias;
        } else if ($dias == 1) {
            return $dias;
        } else if ($dias == 0) {
            return $dias;
        } else if ($dias < 0) {
            return 0;
        }
    } else {
        return "";
    }
}

//------------------------------------------------------------------------------
function formata_data($data) {
    if ($data == '')
        return '';
    $d = explode('/', $data);
    return $d[2] . '-' . $d[1] . '-' . $d[0];
}

//------------------------------------------------------------------------------
function data_volta($data) {
    if ($data == '' || $data == '0000-00-00')
        return '';
    $d = explode('-', $data);
    return $d[2] . '/' . $d[1] . '/' . $d[0];
}

//------------------------------------------------------------------------------
function hora($hora) { // Deixa a hora 20:00
    $h = explode(':', $hora);
    return $h[0] . ':' . $h[1];
}

//------------------------------------------------------------------------------
function getSemana($dia, $completo = 0) {
    switch ($dia) {
        case 1 :
            $r = 'SEG';
            $comp = 'Segunda-feira';
            break;
        case 2 :
            $r = 'TER';
            $comp = 'Terça-feira';
            break;
        case 3 :
            $r = 'QUA';
            $comp = 'Quarta-feira';
            break;
        case 4 :
            $r = 'QUI';
            $comp = 'Quinta-feira';
            break;
        case 5 :
            $r = 'SEX';
            $comp = 'Sexta-feira';
            break;
        case 6 :
            $r = 'SAB';
            $comp = 'Sábado';
            break;
        case 7 :
            $r = 'DOM';
            $comp = 'Domingo';
            break;
    }
    if ($completo == 1)
        return $comp;
    else
        return $r;
}

//------------------------------------------------------------------------------
function getSemana2($dia, $completo = 0) {
    switch ($dia) {
        case 1 :
            $r = 'Seg';
            $comp = 'Segunda-feira';
            break;
        case 2 :
            $r = 'Ter';
            $comp = 'Terça-feira';
            break;
        case 3 :
            $r = 'Qua';
            $comp = 'Quarta-feira';
            break;
        case 4 :
            $r = 'Qui';
            $comp = 'Quinta-feira';
            break;
        case 5 :
            $r = 'Sex';
            $comp = 'Sexta-feira';
            break;
        case 6 :
            $r = 'Sab';
            $comp = 'Sábado';
            break;
        case 7 :
            $r = 'Dom';
            $comp = 'Domingo';
            break;
    }
    if ($completo == 1)
        return $comp;
    else
        return $r;
}

//------------------------------------------------------------------------------
function getDiaSemana($dia, $completo = 0) {
    switch ($dia) {
        case 1 :
            $r = 'Dom';
            $comp = 'Domingo';
            break;
        case 2 :
            $r = 'Seg';
            $comp = 'Segunda-feira';
            break;
        case 3 :
            $r = 'Ter';
            $comp = 'Terça-feira';
            break;
        case 4 :
            $r = 'Qua';
            $comp = 'Quarta-feira';
            break;
        case 5 :
            $r = 'Qui';
            $comp = 'Quinta-feira';
            break;
        case 6 :
            $r = 'Sex';
            $comp = 'Sexta-feira';
            break;
        case 7 :
            $r = 'Sab';
            $comp = 'Sábado';
            break;
    }
    if ($completo == 1)
        return $comp;
    else
        return $r;
}

//------------------------------------------------------------------------------
function hoje($data) {
    $dt = explode('/', $data);
    return getSemana(date("N", mktime(0, 0, 0, $dt[1], $dt[0], intval($dt[2]))), 1);
}

//------------------------------------------------------------------------------
function timeDiff($firstTime, $lastTime) {
    $firstTime = strtotime($firstTime);
    $lastTime = strtotime($lastTime);
    $timeDiff = $lastTime - $firstTime;
    return $timeDiff;
}

//------------------------------------------------------------------------------
function separa_hora($hora, $op) { // $op = minutos = 1; hora = 0
    $hr = explode(':', $hora);
    return $hr[$op];
}

//------------------------------------------------------------------------------
function dataExtenso($dt) {
    $da = explode('/', $dt);
    return $da[0] . ' de ' . getMes($da[1]) . ' de ' . $da[2];
}

//------------------------------------------------------------------------------
function dataExtensoTimeline($dt) {
    $da = explode('/', $dt);
    $diasemana = date("w", mktime(0, 0, 0, $da[1], $da[0], $da[2]));
    return getSemana2($diasemana, 0) . '  ' . getMes2($da[1]) . '  ' . $da[0] . ' ' . $da[2];
}

//------------------------------------------------------------------------------
function getMes($m) {
    $mes = "";
    switch ($m) {
        case 1 :
            $mes = "Janeiro";
            break;
        case 2 :
            $mes = "Fevereiro";
            break;
        case 3 :
            $mes = "Março";
            break;
        case 4 :
            $mes = "Abril";
            break;
        case 5 :
            $mes = "Maio";
            break;
        case 6 :
            $mes = "Junho";
            break;
        case 7 :
            $mes = "Julho";
            break;
        case 8 :
            $mes = "Agosto";
            break;
        case 9 :
            $mes = "Setembro";
            break;
        case 10 :
            $mes = "Outubro";
            break;
        case 11 :
            $mes = "Novembro";
            break;
        case 12 :
            $mes = "Dezembro";
            break;
    }
    return $mes;
}

//------------------------------------------------------------------------------
function getMes2($m) {
    $mes = "";
    switch ($m) {
        case 1 :
            $mes = "Jan";
            break;
        case 2 :
            $mes = "Fev";
            break;
        case 3 :
            $mes = "Mar";
            break;
        case 4 :
            $mes = "Abr";
            break;
        case 5 :
            $mes = "Mai";
            break;
        case 6 :
            $mes = "Jun";
            break;
        case 7 :
            $mes = "Jul";
            break;
        case 8 :
            $mes = "Ago";
            break;
        case 9 :
            $mes = "Set";
            break;
        case 10 :
            $mes = "Out";
            break;
        case 11 :
            $mes = "Nov";
            break;
        case 12 :
            $mes = "Dez";
            break;
    }
    return $mes;
}

//------------------------------------------------------------------------------
function getMes3($m) {
    switch ($m) {
        case 1 :
            $mes = "janeiro";
            break;
        case 2 :
            $mes = "fevereiro";
            break;
        case 3 :
            $mes = "marco";
            break;
        case 4 :
            $mes = "abril";
            break;
        case 5 :
            $mes = "maio";
            break;
        case 6 :
            $mes = "junho";
            break;
        case 7 :
            $mes = "julho";
            break;
        case 8 :
            $mes = "agosto";
            break;
        case 9 :
            $mes = "setembro";
            break;
        case 10 :
            $mes = "outubro";
            break;
        case 11 :
            $mes = "novembro";
            break;
        case 12 :
            $mes = "dezembro";
            break;
    }
    return $mes;
}

//------------------------------------------------------------------------------
function getMes4($m) {
    $mes = "";
    switch ($m) {
        case 1 :
            $mes = "JANEIRO";
            break;
        case 2 :
            $mes = "FEVEREIRO";
            break;
        case 3 :
            $mes = "MARÇO";
            break;
        case 4 :
            $mes = "ABRIL";
            break;
        case 5 :
            $mes = "MAIO";
            break;
        case 6 :
            $mes = "JUNHO";
            break;
        case 7 :
            $mes = "JULHO";
            break;
        case 8 :
            $mes = "AGOSTO";
            break;
        case 9 :
            $mes = "SETEMBRO";
            break;
        case 10 :
            $mes = "OUTUBRO";
            break;
        case 11 :
            $mes = "NOVEMBRO";
            break;
        case 12 :
            $mes = "DEZEMBRO";
            break;
    }
    return $mes;
}

//------------------------------------------------------------------------------
function ctexto($texto, $frase = 'pal') {
    switch ($frase) {
        case 'fra' : // Apenas a a primeira letra em maiusculo
            $texto = ucfirst(mb_strtolower($texto));
            break;
        case 'min' :
            $texto = mb_strtolower($texto);
            break;
        case 'mai' :
            $texto = colocaAcentoMaiusculo((mb_strtoupper($texto)));
            break;
        case 'pal' : // Todas as palavras com a primeira em maiusculo
            $texto = ucwords(mb_strtolower($texto));
            break;
        case 'pri' : // Todos os primeiros caracteres de cada palavra em maiusuclo, menos as junções
            $texto = titleCase($texto);
            break;
    }
    return $texto;
}

//------------------------------------------------------------------------------
function titleCase($string, $delimiters = array(" ", "-", ".", "'", "O'", "Mc"), $exceptions = array("de", "da", "dos", "das", "do", "I", "II", "III", "IV", "V", "VI")) {
    /*
     * Exceptions in lower case are words you don't want converted
     * Exceptions all in upper case are any words you don't want converted to title case
     * but should be converted to upper case, e.g.:
     * king henry viii or king henry Viii should be King Henry VIII
     */
    $string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
    foreach ($delimiters as $dlnr => $delimiter) {
        $words = explode($delimiter, $string);
        $newwords = array();
        foreach ($words as $wordnr => $word) {
            if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtoupper($word, "UTF-8");
            } elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtolower($word, "UTF-8");
            } elseif (!in_array($word, $exceptions)) {
                // convert to uppercase (non-utf8 only)
                $word = ucfirst($word);
            }
            array_push($newwords, $word);
        }
        $string = join($delimiter, $newwords);
    } // foreach
    return $string;
}

//------------------------------------------------------------------------------
function colocaAcentoMaiusculo($texto) {
    $array1 = array(
        "á",
        "à",
        "â",
        "ã",
        "ä",
        "é",
        "è",
        "ê",
        "ë",
        "í",
        "ì",
        "î",
        "ï",
        "ó",
        "ò",
        "ô",
        "õ",
        "ö",
        "ú",
        "ù",
        "û",
        "ü",
        "ç"
    );

    $array2 = array(
        "Á",
        "À",
        "Â",
        "Ã",
        "Ä",
        "É",
        "È",
        "Ê",
        "Ë",
        "Í",
        "Ì",
        "Î",
        "Ï",
        "Ó",
        "Ò",
        "Ô",
        "Õ",
        "Ö",
        "Ú",
        "Ù",
        "Û",
        "Ü",
        "Ç"
    );
    return str_replace($array1, $array2, $texto);
}

//------------------------------------------------------------------------------
function retira_acentos($texto) {
    $array1 = array(
        "á",
        "à",
        "â",
        "ã",
        "ä",
        "é",
        "è",
        "ê",
        "ë",
        "í",
        "ì",
        "î",
        "ï",
        "ó",
        "ò",
        "ô",
        "õ",
        "ö",
        "ú",
        "ù",
        "û",
        "ü",
        "ç",
        "Á",
        "À",
        "Â",
        "Ã",
        "Ä",
        "É",
        "È",
        "Ê",
        "Ë",
        "Í",
        "Ì",
        "Î",
        "Ï",
        "Ó",
        "Ò",
        "Ô",
        "Õ",
        "Ö",
        "Ú",
        "Ù",
        "Û",
        "Ü",
        "Ç"
    );
    $array2 = array(
        "a",
        "a",
        "a",
        "a",
        "a",
        "e",
        "e",
        "e",
        "e",
        "i",
        "i",
        "i",
        "i",
        "o",
        "o",
        "o",
        "o",
        "o",
        "u",
        "u",
        "u",
        "u",
        "c",
        "A",
        "A",
        "A",
        "A",
        "A",
        "E",
        "E",
        "E",
        "E",
        "I",
        "I",
        "I",
        "I",
        "O",
        "O",
        "O",
        "O",
        "O",
        "U",
        "U",
        "U",
        "U",
        "C"
    );
    return str_replace($array1, $array2, $texto);
}

//------------------------------------------------------------------------------
// Cria uma função que retorna o timestamp de uma data no formato DD/MM/AAAA
function geraTimestamp($data) {
    $partes = explode('/', $data);
    return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
}

//------------------------------------------------------------------------------
function obterDataBRTimestamp($data) {
    if ($data != '') {
        $data = substr($data, 0, 10);
        $explodida = explode("-", $data);
        $dataIso = $explodida[2] . "/" . $explodida[1] . "/" . $explodida[0];
        return $dataIso;
    }
    return NULL;
}

//------------------------------------------------------------------------------
function convertDataBR2ISO($data) {
    if ($data == '')
        return false;
    $explodida = explode("/", $data);
    $dataIso = $explodida[2] . "-" . $explodida[1] . "-" . $explodida[0];
    return $dataIso;
}

//------------------------------------------------------------------------------
function obterHoraTimestamp($data) {
    return substr($data, 11, 5);
}

//------------------------------------------------------------------------------
function obterDiaTimestamp($data) {
    return substr($data, 8, 2);
}

//------------------------------------------------------------------------------
function obterMesTimestamp($data) {
    return substr($data, 5, 2);
}

//------------------------------------------------------------------------------
function obterAnoTimestamp($data) {
    return substr($data, 0, 4);
}

//------------------------------------------------------------------------------
function calculaDiferencaDatas($data_inicial, $data_final) {
    // Usa a função criada e pega o timestamp das duas datas:
    $time_inicial = geraTimestamp($data_inicial);
    $time_final = geraTimestamp($data_final);

    // Calcula a diferença de segundos entre as duas datas:
    $diferenca = $time_final - $time_inicial; // 19522800 segundos
    // Calcula a diferença de dias
    $dias = (int) floor($diferenca / (60 * 60 * 24)); // 225 dias
    // Exibe uma mensagem de resultado:
    // echo "A diferença entre as datas ".$data_inicial." e ".$data_final." é de <strong>".$dias."</strong> dias";
    return $dias;
}

//------------------------------------------------------------------------------
function apelidometadatos($variavel) {
    /*
     * $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ ,;:./';
     * $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr______';
     * //$string = ($string);
     * $string = strtr($string, ($a), $b); //substitui letras acentuadas por "normais"
     * $string = str_replace(" ","",$string); // retira espaco
     * $string = strtolower($string); // passa tudo para minusculo
     */
    $string = strtolower(ereg_replace("[^a-zA-Z0-9-]", "-", strtr((trim($variavel)), ("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"), "aaaaeeiooouuncAAAAEEIOOOUUNC-")));
    return ($string); // finaliza, gerando uma saída para a funcao
}

//------------------------------------------------------------------------------
function getExtensaoArquivo($extensao) {
    switch ($extensao) {
        case 'image/jpeg' :
            $ext = ".jpeg";
            break;
        case 'image/jpg' :
            $ext = ".jpg";
            break;
        case 'image/pjpeg' :
            $ext = ".pjpg";
            break;
        case 'image/JPEG' :
            $ext = ".JPEG";
            break;
        case 'image/gif' :
            $ext = ".gif";
            break;
        case 'image/png' :
            $ext = ".png";
            break;
        case 'video/webm' :
            $ext = ".webm";
            break;
        case 'video/mp4' :
            $ext = ".mp4";
            break;
        case 'video/flv' :
            $ext = ".flv";
            break;
        case 'video/webm' :
            $ext = ".webm";
            break;
        case 'audio/mp4' :
            $ext = ".acc";
            break;
        case 'audio/mpeg' :
            $ext = ".mp3";
            break;
        case 'audio/ogg' :
            $ext = ".ogg";
            break;
    }
    return $ext;
}

//------------------------------------------------------------------------------
function uploadArquivoPermitido($arquivo) {
    $tiposPermitidos = array(
        'image/gif',
        'image/jpeg',
        'image/jpg',
        'image/pjpeg',
        'image/png',
        'video/webm',
        'video/mp4',
        'video/ogv',
        'audio/mp3',
        'audio/mp4',
        'audio/mpeg',
        'audio/ogg'
    );
    if (array_search($arquivo, $tiposPermitidos) === false) {
        return false;
    } else {
        return true;
    } // end if
}

//------------------------------------------------------------------------------
function converteValorMonetario($valor) {
    $valor = str_replace('.', '', $valor);
    $valor = str_replace('.', '', $valor);
    $valor = str_replace('.', '', $valor);
    $valor = str_replace(',', '.', $valor);
    return $valor;
}

//------------------------------------------------------------------------------
function valorMonetario($valor) {
    $valor = number_format($valor, 2, ',', '.');
    return $valor;
}

//------------------------------------------------------------------------------
function real2float($num) {
    $num = str_replace(".", "", $num);
    $num = str_replace(",", ".", $num);
    return $num;
}

//------------------------------------------------------------------------------
function fdec($numero, $formato = NULL, $tmp = NULL) {
    switch ($formato) {
        case null :
            if ($numero != 0)
                $numero = number_format($numero, 2, ',', '.');
            else
                $numero = '0,00';
            break;
        case '%' :
            if ($numero > 0)
                $numero = number_format((($numero / $tmp) * 100), 2, ',', '.') . '%';
            else
                $numero = '0%';
            break;
        case '-' :
            $numero = "<font color='red'>" . fdec($numero) . "</font>";
            break;
    }
    return $numero;
}

//------------------------------------------------------------------------------
function verificarloginduplicado($usuario, $idsessao, $query) {
    $oConexao = Conexao::getInstance();
    $retorno = true;
    $querysessao = $oConexao->query($query);
    $qtdsessao = $querysessao->rowCount();
    if ($qtdsessao == 0) {
        $retorno = false;
    }
    return $retorno;
}

//------------------------------------------------------------------------------
function historicoacesso($pagina, $apelido, $operacao, $usuario, $ip) {
    $oConn = Conexao::getInstance();
    $retorno = true;
    if ($pagina != '' && $operacao != '') {
        $rsUsuarioHist = $oConn->prepare("SELECT count(id) total FROM usuario_hist WHERE datacadastro = now() AND ip = ? AND apelido = ? AND operacao = ?");
        $rsUsuarioHist->bindValue(1, $ip);
        $rsUsuarioHist->bindValue(2, $apelido);
        $rsUsuarioHist->bindValue(3, $operacao);
        $rsUsuarioHist->execute();
        $countUsuarioHist = $rsUsuarioHist->fetch(PDO::FETCH_OBJ)->total;
        if ($countUsuarioHist <= 0) {
            $usuarioHist = $oConn->prepare("INSERT INTO usuario_hist(pagina, apelido, operacao, datacadastro, idusuario, ip) VALUES(?, ?, ?, now(), ?, ?)");
            $usuarioHist->bindValue(1, $pagina);
            $usuarioHist->bindValue(2, $apelido);
            $usuarioHist->bindValue(3, $operacao);
            $usuarioHist->bindValue(4, $usuario);
            $usuarioHist->bindValue(5, $ip);
            // $usuarioHist->bindValue(6, $ip);
            if (!$usuarioHist->execute()) {
                $retorno = false;
            }
        }
    }
    return $retorno;
}

//------------------------------------------------------------------------------
function envia_email($para, $assunto, $mensagem, $emaile, $nome_email) {

    // Inicia a classe PHPMailer
    $mail = new PHPMailer();

    // Define os dados do servidor e tipo de conexão
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsSMTP(); // Define que a mensagem será SMTP
    $mail->Host = ""; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
    $mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
    $mail->Username = ''; // Usuário do servidor SMTP (endereço de email)
    $mail->Password = ''; // Senha do servidor SMTP (senha do email usado)
    // Define o remetente
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->From = $emaile;
    $mail->Sender = $emaile;
    $mail->FromName = $nome_email;
    // Adicionando copia da mensagem

    $mail->AddAddress($para); // E-mail do destinatário
    // Define os dados técnicos da Mensagem
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
    // Define a mensagem (Texto e Assunto)
    // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    $mail->Subject = $assunto; // Assunto da mensagem
    $mail->Body = $mensagem;
    $mail->AltBody = $mensagem;

    // Envia o e-mail
    $enviado = $mail->Send();

    // Limpa os destinatários e os anexos
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();

    if (!$mail->Send()) {
        return true;
    } else {
        return false;
    }
}

//------------------------------------------------------------------------------
function pegar_nome_campo_municipio($municipio_id) {
    $rs = "";

    if ($municipio_id == 5565) {
        $rs = "acre";
    } else if ($municipio_id == 79) {
        $rs = "acrelandia";
    } else if ($municipio_id == 80) {
        $rs = "assis_brasil";
    } else if ($municipio_id == 81) {
        $rs = "brasileia";
    } else if ($municipio_id == 82) {
        $rs = "bujari";
    } else if ($municipio_id == 83) {
        $rs = "capixaba";
    } else if ($municipio_id == 84) {
        $rs = "cruzeiro_do_sul";
    } else if ($municipio_id == 85) {
        $rs = "epitaciolandia";
    } else if ($municipio_id == 86) {
        $rs = "feijo";
    } else if ($municipio_id == 87) {
        $rs = "jordao";
    } else if ($municipio_id == 88) {
        $rs = "mancio_lima";
    } else if ($municipio_id == 89) {
        $rs = "manoel_urbano";
    } else if ($municipio_id == 90) {
        $rs = "marechal_thaumaturgo";
    } else if ($municipio_id == 91) {
        $rs = "placido_de_castro";
    } else if ($municipio_id == 92) {
        $rs = "porto_acre";
    } else if ($municipio_id == 93) {
        $rs = "porto_walter";
    } else if ($municipio_id == 94) {
        $rs = "rio_branco";
    } else if ($municipio_id == 95) {
        $rs = "rodrigues_alves";
    } else if ($municipio_id == 96) {
        $rs = "santa_rosa";
    } else if ($municipio_id == 97) {
        $rs = "sena_madureira";
    } else if ($municipio_id == 98) {
        $rs = "senador_guiomard";
    } else if ($municipio_id == 99) {
        $rs = "tarauaca";
    } else if ($municipio_id == 100) {
        $rs = "xapuri";
    }

    return $rs;
}

//------------------------------------------------------------------------------
// FUNÇÃO QUE CAPTURA O VALOR DE UM ARRAY E IGNORA OS VALORES VAZIOS
function pegar_valor_array($valor_array) {
    $rs = "";

    foreach ($valor_array as $key => $value) {
        if ($value != "" && $value != NULL && $value != "0" && $value != "undefined") {
            $rs = $value;
        }
    }
    return $rs;
}

//------------------------------------------------------------------------------
function romano($N) {
    $N1 = $N;
    $Y = "";
    while ($N / 1000 >= 1) {
        $Y .= "M";
        $N = $N - 1000;
    }
    if ($N / 900 >= 1) {
        $Y .= "CM";
        $N = $N - 900;
    }
    if ($N / 500 >= 1) {
        $Y .= "D";
        $N = $N - 500;
    }
    if ($N / 400 >= 1) {
        $Y .= "CD";
        $N = $N - 400;
    }
    while ($N / 100 >= 1) {
        $Y .= "C";
        $N = $N - 100;
    }
    if ($N / 90 >= 1) {
        $Y .= "XC";
        $N = $N - 90;
    }
    if ($N / 50 >= 1) {
        $Y .= "L";
        $N = $N - 50;
    }
    if ($N / 40 >= 1) {
        $Y .= "XL";
        $N = $N - 40;
    }
    while ($N / 10 >= 1) {
        $Y .= "X";
        $N = $N - 10;
    }
    if ($N / 9 >= 1) {
        $Y .= "IX";
        $N = $N - 9;
    }
    if ($N / 5 >= 1) {
        $Y .= "V";
        $N = $N - 5;
    }
    if ($N / 4 >= 1) {
        $Y .= "IV";
        $N = $N - 4;
    }
    while ($N >= 1) {
        $Y .= "I";
        $N = $N - 1;
    }
    return $Y;
}

//------------------------------------------------------------------------------
function valorPorExtenso($valor = 0, $bolExibirMoeda = true, $bolPalavraFeminina = false) {
    $singular = null;
    $plural = null;

    if ($bolExibirMoeda) {
        $singular = array(
            "centavo",
            "real",
            "mil",
            "milhão",
            "bilhão",
            "trilhão",
            "quatrilhão"
        );
        $plural = array(
            "centavos",
            "reais",
            "mil",
            "milhões",
            "bilhões",
            "trilhões",
            "quatrilhões"
        );
    } else {
        $singular = array(
            "",
            "",
            "mil",
            "milhão",
            "bilhão",
            "trilhão",
            "quatrilhão"
        );
        $plural = array(
            "",
            "",
            "mil",
            "milhões",
            "bilhões",
            "trilhões",
            "quatrilhões"
        );
    }

    $c = array(
        "",
        "cem",
        "duzentos",
        "trezentos",
        "quatrocentos",
        "quinhentos",
        "seiscentos",
        "setecentos",
        "oitocentos",
        "novecentos"
    );
    $d = array(
        "",
        "dez",
        "vinte",
        "trinta",
        "quarenta",
        "cinquenta",
        "sessenta",
        "setenta",
        "oitenta",
        "noventa"
    );
    $d10 = array(
        "dez",
        "onze",
        "doze",
        "treze",
        "quatorze",
        "quinze",
        "dezesseis",
        "dezesete",
        "dezoito",
        "dezenove"
    );
    $u = array(
        "",
        "um",
        "dois",
        "três",
        "quatro",
        "cinco",
        "seis",
        "sete",
        "oito",
        "nove"
    );

    if ($bolPalavraFeminina) {

        if ($valor == 1) {
            $u = array(
                "",
                "uma",
                "duas",
                "três",
                "quatro",
                "cinco",
                "seis",
                "sete",
                "oito",
                "nove"
            );
        } else {
            $u = array(
                "",
                "um",
                "duas",
                "três",
                "quatro",
                "cinco",
                "seis",
                "sete",
                "oito",
                "nove"
            );
        }

        $c = array(
            "",
            "cem",
            "duzentas",
            "trezentas",
            "quatrocentas",
            "quinhentas",
            "seiscentas",
            "setecentas",
            "oitocentas",
            "novecentas"
        );
    }

    $z = 0;

    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);

    for ($i = 0; $i < count($inteiro); $i++) {
        for ($ii = mb_strlen($inteiro[$i]); $ii < 3; $ii++) {
            $inteiro[$i] = "0" . $inteiro[$i];
        }
    }

    // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
    $rt = null;
    $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
    for ($i = 0; $i < count($inteiro); $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
        $t = count($inteiro) - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000")
            $z++;
        elseif ($z > 0)
            $z--;

        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
            $r .= (($z > 1) ? " de " : "") . $plural[$t];

        if ($r)
            $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
    }

    $rt = mb_substr($rt, 1);

    return ($rt ? trim($rt) : "zero");
}

//------------------------------------------------------------------------------
function mask($val, $mask) {
    $maskared = '';
    $k = 0;
    for ($i = 0; $i <= strlen($mask) - 1; $i++) {
        if ($mask[$i] == '#') {
            if (isset($val[$k])) {
                $maskared .= $val[$k++];
            }
        } else {
            if (isset($mask[$i])) {
                $maskared .= $mask[$i];
            }
        }
    }
    return $maskared;
}

//------------------------------------------------------------------------------
// retorna diferença em horas
function retorna_dif_horas($data) {
    $hora_data_atual = date("Y-m-d H:i:s");
    $data = strtotime($data);
    $hora_data_atual = strtotime($hora_data_atual);
    $diferenca = $hora_data_atual - $data;
    $horas = floor($diferenca / 3600);
    return $horas;
}

//------------------------------------------------------------------------------
function removeAcentos($string) {
    return preg_replace(array("/(ç)/", "/(Ç)/", "/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "c C a A e E i I o O u U n N"), $string);
}

//------------------------------------------------------------------------------
?>