<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$quem_impede = isset($_POST['quem_impede']) && $_POST['quem_impede'] != "" ? $_POST['quem_impede'] : NULL;
$parente_id = isset($_POST['parente_id']) && $_POST['parente_id'] != "" ? $_POST['parente_id'] : "";

$result = $db->prepare("SELECT *  
                        FROM mod_acolhimento_crianca_parentes 
                        WHERE nome = ? AND familia_id = ?");
$result->bindValue(1, $quem_impede);
$result->bindValue(2, $id);
$result->execute();
$dados_parente = $result->fetch(PDO::FETCH_ASSOC);

$codigo_irmao = pesquisar2("id", "mod_acolhimento_crianca_parentes", "nome", "=", $dados_parente['nome'], "familia_id", "=", $id, "AND tipo = 7");

if (is_numeric($codigo_irmao) && $parente_id != $codigo_irmao) {
    $error = true;
    $msg['msg'] = 'error';
    $msg['retorno'] = "O parente escolhido já está adicionado na lista";
}

if ($error == false) {
    try {

        $db->beginTransaction();

        $sql4 = $db->prepare("INSERT INTO mod_acolhimento_crianca_parentes (familia_id, grau, nome, nascimento, endereco, rg, cpf, celular, ocupacao, tipo, responsavel_id, data_cadastro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 7, ?, NOW())");
        $sql4->bindValue(1, $id);
        $sql4->bindValue(2, $dados_parente['grau']);
        $sql4->bindValue(3, $dados_parente['nome']);
        $sql4->bindValue(4, $dados_parente['nascimento']);
        $sql4->bindValue(5, $dados_parente['endereco']);
        $sql4->bindValue(6, $dados_parente['rg']);
        $sql4->bindValue(7, $dados_parente['cpf']);
        $sql4->bindValue(8, $dados_parente['celular']);
        $sql4->bindValue(9, $dados_parente['ocupacao']);
        $sql4->bindValue(10, $_SESSION['id']);
        $sql4->execute();

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
} else {
    echo json_encode($msg);
    exit();
}
?>