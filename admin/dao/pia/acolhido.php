<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$crianca_nome = isset($_POST['crianca_nome']) && $_POST['crianca_nome'] != "" ? $_POST['crianca_nome'] : NULL;
$sexo = isset($_POST['sexo']) && $_POST['sexo'] != "" ? $_POST['sexo'] : NULL;
$nascimento = isset($_POST['nascimento']) && $_POST['nascimento'] != "" ? $_POST['nascimento'] : NULL;
$nacionalidade = isset($_POST['nacionalidade']) && $_POST['nacionalidade'] != "" ? $_POST['nacionalidade'] : NULL;
$naturalidade = isset($_POST['naturalidade']) && $_POST['naturalidade'] != "" ? $_POST['naturalidade'] : NULL;
$religiao = isset($_POST['religiao']) && $_POST['religiao'] != "" ? $_POST['religiao'] : NULL;
$cor = isset($_POST['cor']) && $_POST['cor'] != "" ? $_POST['cor'] : NULL;
$nome_pai = isset($_POST['nome_pai']) && $_POST['nome_pai'] != "" ? $_POST['nome_pai'] : NULL;
$nome_mae = isset($_POST['nome_mae']) && $_POST['nome_mae'] != "" ? $_POST['nome_mae'] : NULL;
$rua = isset($_POST['rua']) && $_POST['rua'] != "" ? $_POST['rua'] : NULL;
$numero = isset($_POST['numero']) && $_POST['numero'] != "" ? $_POST['numero'] : NULL;
$bairro = isset($_POST['bairro']) && $_POST['bairro'] != "" ? $_POST['bairro'] : NULL;
$estado_id = isset($_POST['estado']) && $_POST['estado'] != "" ? $_POST['estado'] : NULL;
$cidade_id = isset($_POST['cidade']) && $_POST['cidade'] != "" ? $_POST['cidade'] : NULL;

if ($error == false) {
    try {

        $db->beginTransaction();

            $sql2 = $db->prepare("UPDATE mod_acolhimento_crianca SET nome = ?, pai = ?, mae = ?, genero = ?, nascimento = ?, nascionalidade = ?, naturalidade = ?, religiao_id = ?, cor_id = ?,
            rua = ?, numero = ?, bairro = ?, estado_id = ?, cidade_id = ?, usuario_id = ?
            WHERE acolhimento_id = ?");
            $sql2->bindValue(1, $crianca_nome);
            $sql2->bindValue(2, $nome_pai);
            $sql2->bindValue(3, $nome_mae);
            $sql2->bindValue(4, $sexo);
            $sql2->bindValue(5, $nascimento);
            $sql2->bindValue(6, $nacionalidade);
            $sql2->bindValue(7, $naturalidade);
            $sql2->bindValue(8, $religiao);
            $sql2->bindValue(9, $cor);
            $sql2->bindValue(10, $rua);
            $sql2->bindValue(11, $numero);
            $sql2->bindValue(12, $bairro);
            $sql2->bindValue(13, $estado_id);
            $sql2->bindValue(14, $cidade_id);
            $sql2->bindValue(15, $_SESSION['id']);
            $sql2->bindValue(16, $id);
            $sql2->execute();
        

        //ATUALIZANDO O NOME DA FOTO TEMPORARIA
        if (isset($_SESSION['foto_cut']) && isset($_SESSION['foto_origin']) && $_SESSION['foto_cut'] != "" && $_SESSION['foto_origin'] != "") {
            $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca SET crianca_foto = ? WHERE acolhimento_id = ?");
            $stmt5->bindValue(1, $_SESSION['foto_cut']);
            $stmt5->bindValue(2, $id);
            $stmt5->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>