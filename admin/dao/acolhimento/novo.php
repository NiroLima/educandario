<?php

session_start();
include_once('../../../conf/config.php');
include_once('../../../utils/funcoes.php');
$db = Conexao::getInstance();

$msg = array();
$error = false;

$id = isset($_POST['id']) && $_POST['id'] != "" ? $_POST['id'] : 0;
$quem_trouxe = isset($_POST['quem_trouxe']) && $_POST['quem_trouxe'] != "" ? $_POST['quem_trouxe'] : NULL;
$responsavel_entrega = isset($_POST['responsavel_entrega']) && $_POST['responsavel_entrega'] != "" ? $_POST['responsavel_entrega'] : NULL;
$funcao_responsavel = isset($_POST['funcao_responsavel']) && $_POST['funcao_responsavel'] != "" ? $_POST['funcao_responsavel'] : NULL;
$contato_responsavel = isset($_POST['contato_responsavel']) && $_POST['contato_responsavel'] != "" ? $_POST['contato_responsavel'] : NULL;
$data_entrada = isset($_POST['data_entrada']) && $_POST['data_entrada'] != "" ? $_POST['data_entrada'] : NULL;
$hora_entrada = isset($_POST['hora_entrada']) && $_POST['hora_entrada'] != "" ? $_POST['hora_entrada'] : NULL;
$profissional = isset($_POST['profissional']) && $_POST['profissional'] != "" ? $_POST['profissional'] : NULL;
$observacao_acolhimento = isset($_POST['observacao_acolhimento']) && $_POST['observacao_acolhimento'] != "" ? $_POST['observacao_acolhimento'] : NULL;
$motivo_aba = isset($_POST['motivo_aba']) && $_POST['motivo_aba'] != "" ? $_POST['motivo_aba'] : NULL;
$motivo_neg = isset($_POST['motivo_neg']) && $_POST['motivo_neg'] != "" ? $_POST['motivo_neg'] : NULL;
$motivo_abu = isset($_POST['motivo_abu']) && $_POST['motivo_abu'] != "" ? $_POST['motivo_abu'] : NULL;
$motivo_out = isset($_POST['motivo_out']) && $_POST['motivo_out'] != "" ? $_POST['motivo_out'] : NULL;
$descricao = isset($_POST['descricao']) && $_POST['descricao'] != "" ? $_POST['descricao'] : NULL;
$numero_guia_acolhimento = isset($_POST['numero_guia_acolhimento']) && $_POST['numero_guia_acolhimento'] != "" ? $_POST['numero_guia_acolhimento'] : NULL;
$numero_processo = isset($_POST['numero_processo']) && $_POST['numero_processo'] != "" ? $_POST['numero_processo'] : NULL;
$numero_destituicao = isset($_POST['numero_destituicao']) && $_POST['numero_destituicao'] != "" ? $_POST['numero_destituicao'] : NULL;
$crianca_nome = isset($_POST['crianca_nome']) && $_POST['crianca_nome'] != "" ? $_POST['crianca_nome'] : NULL;
$sexo = isset($_POST['sexo']) && $_POST['sexo'] != "" ? $_POST['sexo'] : NULL;
$nascimento = isset($_POST['nascimento']) && $_POST['nascimento'] != "" ? $_POST['nascimento'] : NULL;
$nacionalidade = isset($_POST['nacionalidade']) && $_POST['nacionalidade'] != "" ? $_POST['nacionalidade'] : NULL;
$naturalidade = isset($_POST['naturalidade']) && $_POST['naturalidade'] != "" ? $_POST['naturalidade'] : NULL;
$religiao = isset($_POST['religiao']) && $_POST['religiao'] != "" ? $_POST['religiao'] : NULL;
$cor = isset($_POST['cor']) && $_POST['cor'] != "" ? $_POST['cor'] : NULL;
$nome_pai = isset($_POST['nome_pai']) && $_POST['nome_pai'] != "" ? $_POST['nome_pai'] : NULL;
$nome_mae = isset($_POST['nome_mae']) && $_POST['nome_mae'] != "" ? $_POST['nome_mae'] : NULL;
$qual_deficiencia = isset($_POST['qual_deficiencia']) && $_POST['qual_deficiencia'] != "" ? $_POST['qual_deficiencia'] : NULL;
$recursos = isset($_POST['recursos']) && $_POST['recursos'] != "" ? $_POST['recursos'] : NULL;
$qual_tratamento = isset($_POST['qual_tratamento']) && $_POST['qual_tratamento'] != "" ? $_POST['qual_tratamento'] : NULL;
$disturbio = isset($_POST['disturbio']) && $_POST['disturbio'] != "" ? $_POST['disturbio'] : NULL;
$qual_doenca_infectocontagiosa = isset($_POST['qual_doenca_infectocontagiosa']) && $_POST['qual_doenca_infectocontagiosa'] != "" ? $_POST['qual_doenca_infectocontagiosa'] : NULL;
$qual_medicamentos = isset($_POST['qual_medicamentos']) && $_POST['qual_medicamentos'] != "" ? $_POST['qual_medicamentos'] : NULL;
$obs_saude = isset($_POST['obs_saude']) && $_POST['obs_saude'] != "" ? $_POST['obs_saude'] : NULL;
$rua = isset($_POST['rua']) && $_POST['rua'] != "" ? $_POST['rua'] : NULL;
$numero = isset($_POST['numero']) && $_POST['numero'] != "" ? $_POST['numero'] : NULL;
$bairro = isset($_POST['bairro']) && $_POST['bairro'] != "" ? $_POST['bairro'] : NULL;
$estado_id = isset($_POST['estado']) && $_POST['estado'] != "" ? $_POST['estado'] : NULL;
$cidade_id = isset($_POST['cidade']) && $_POST['cidade'] != "" ? $_POST['cidade'] : NULL;
$comarca_interior = isset($_POST['comarca_interior']) && $_POST['comarca_interior'] != "" ? $_POST['comarca_interior'] : NULL;

//VERIFICAÇÃO SE O USUÁRIO INFORMADO JÁ EXISTE NA BASE DE DADOS
$codigo = pesquisar("acolhimento_id", "mod_acolhimento_crianca", "nome", "=", $crianca_nome, "");
if (is_numeric($codigo) && $codigo != $id) {
    $error = true;
    echo "A criança informada já existe no sistema.";
    exit();
}

if ($error == false) {
    try {

        $db->beginTransaction();

        if ($id == 0) {
            $sql = $db->prepare("INSERT INTO mod_acolhimento (quem_trouxe, responsavel_entrega, funcao_responsavel, celular, data_entrada, hora_entrada, profissional_id, 
                                 obs, motivo_aba, motivo_neg, motivo_abu, motivo_out, descricao, numero_guia, numero_processo, numero_destituicao, comarca_id, data_update, usuario_id, data_cadastro, status) 
                                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql->bindValue(1, $quem_trouxe);
            $sql->bindValue(2, $responsavel_entrega);
            $sql->bindValue(3, $funcao_responsavel);
            $sql->bindValue(4, $contato_responsavel);
            $sql->bindValue(5, $data_entrada);
            $sql->bindValue(6, $hora_entrada);
            $sql->bindValue(7, $profissional);
            $sql->bindValue(8, $observacao_acolhimento);
            $sql->bindValue(9, $motivo_aba);
            $sql->bindValue(10, $motivo_neg);
            $sql->bindValue(11, $motivo_abu);
            $sql->bindValue(12, $motivo_out);
            $sql->bindValue(13, $descricao);
            $sql->bindValue(14, $numero_guia_acolhimento);
            $sql->bindValue(15, $numero_processo);
            $sql->bindValue(16, $numero_destituicao);
            $sql->bindValue(17, $comarca_interior);
            $sql->bindValue(18, $_SESSION['id']);
            $sql->execute();

            $id = $db->lastInsertId();

            $sql2 = $db->prepare("INSERT INTO mod_acolhimento_crianca (acolhimento_id, nome, pai, mae, genero, nascimento, nascionalidade, naturalidade, religiao_id, cor_id,
                qual_deficiencia, necessita_equipamentos, qual_tratamento, indicio_disturbio, qual_doenca_infectocontagiosa, obs_saude, rua, numero, bairro, estado_id, cidade_id,
                data_update, usuario_id, data_cadastro, status) 
                                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, NOW(), 1)");
            $sql2->bindValue(1, $id);
            $sql2->bindValue(2, $crianca_nome);
            $sql2->bindValue(3, $nome_pai);
            $sql2->bindValue(4, $nome_mae);
            $sql2->bindValue(5, $sexo);
            $sql2->bindValue(6, $nascimento);
            $sql2->bindValue(7, $nacionalidade);
            $sql2->bindValue(8, $naturalidade);
            $sql2->bindValue(9, $religiao);
            $sql2->bindValue(10, $cor);
            $sql2->bindValue(11, $qual_deficiencia);
            $sql2->bindValue(12, $recursos);
            $sql2->bindValue(13, $qual_tratamento);
            $sql2->bindValue(14, $disturbio);
            $sql2->bindValue(15, $qual_doenca_infectocontagiosa);
            $sql2->bindValue(16, $obs_saude);
            $sql2->bindValue(17, $rua);
            $sql2->bindValue(18, $numero);
            $sql2->bindValue(19, $bairro);
            $sql2->bindValue(20, $estado_id);
            $sql2->bindValue(21, $cidade_id);
            $sql2->bindValue(22, $_SESSION['id']);
            $sql2->execute();

            $acolhimento_crianca_id = $db->lastInsertId();
        } else {
            $sql = $db->prepare("UPDATE mod_acolhimento SET quem_trouxe = ?, responsavel_entrega = ?, funcao_responsavel = ?, celular = ?, data_entrada = ?, hora_entrada = ?,
                                 profissional_id = ?, obs = ?, motivo_aba = ?, motivo_neg = ?, motivo_abu = ?, motivo_out = ?, descricao = ?, numero_guia = ?, numero_processo = ?, numero_destituicao = ?, comarca_id = ?, usuario_id = ? WHERE id = ?");
            $sql->bindValue(1, $quem_trouxe);
            $sql->bindValue(2, $responsavel_entrega);
            $sql->bindValue(3, $funcao_responsavel);
            $sql->bindValue(4, $contato_responsavel);
            $sql->bindValue(5, $data_entrada);
            $sql->bindValue(6, $hora_entrada);
            $sql->bindValue(7, $profissional);
            $sql->bindValue(8, $observacao_acolhimento);
            $sql->bindValue(9, $motivo_aba);
            $sql->bindValue(10, $motivo_neg);
            $sql->bindValue(11, $motivo_abu);
            $sql->bindValue(12, $motivo_out);
            $sql->bindValue(13, $descricao);
            $sql->bindValue(14, $numero_guia_acolhimento);
            $sql->bindValue(15, $numero_processo);
            $sql->bindValue(16, $numero_destituicao);
            $sql->bindValue(17, $comarca_interior);
            $sql->bindValue(18, $_SESSION['id']);
            $sql->bindValue(19, $id);
            $sql->execute();

            $sql2 = $db->prepare("UPDATE mod_acolhimento_crianca SET nome = ?, pai = ?, mae = ?, genero = ?, nascimento = ?, nascionalidade = ?, naturalidade = ?, religiao_id = ?, cor_id = ?,
            qual_deficiencia = ?, necessita_equipamentos = ?, qual_tratamento = ?, indicio_disturbio = ?, qual_doenca_infectocontagiosa = ?, obs_saude = ?, rua = ?, numero = ?, bairro = ?, estado_id = ?, cidade_id = ?, usuario_id = ?
            WHERE acolhimento_id = ?");
            $sql2->bindValue(1, $crianca_nome);
            $sql2->bindValue(2, $nome_pai);
            $sql2->bindValue(3, $nome_mae);
            $sql2->bindValue(4, $sexo);
            $sql2->bindValue(5, $nascimento);
            $sql2->bindValue(6, $nacionalidade);
            $sql2->bindValue(7, $naturalidade);
            $sql2->bindValue(8, $religiao);
            $sql2->bindValue(9, $cor);
            $sql2->bindValue(10, $qual_deficiencia);
            $sql2->bindValue(11, $recursos);
            $sql2->bindValue(12, $qual_tratamento);
            $sql2->bindValue(13, $disturbio);
            $sql2->bindValue(14, $qual_doenca_infectocontagiosa);
            $sql2->bindValue(15, $obs_saude);
            $sql2->bindValue(16, $rua);
            $sql2->bindValue(17, $numero);
            $sql2->bindValue(18, $bairro);
            $sql2->bindValue(19, $estado_id);
            $sql2->bindValue(20, $cidade_id);
            $sql2->bindValue(21, $_SESSION['id']);
            $sql2->bindValue(22, $id);
            $sql2->execute();

            $acolhimento_crianca_id = pesquisar("id", "mod_acolhimento_crianca", "acolhimento_id", "=", $id, "");
        }

        $sql3 = $db->prepare("DELETE FROM mod_acolhimento_crianca_medicacao WHERE acolhimento_crianca_id = ?");
        $sql3->bindValue(1, $acolhimento_crianca_id);
        $sql3->execute();

        if (is_array($qual_medicamentos)) {
            foreach ($qual_medicamentos AS $key => $val) {
                if ($val != "" && $val != null) {
                    $sql3 = $db->prepare("INSERT INTO mod_acolhimento_crianca_medicacao (acolhimento_crianca_id, medicamento_id) VALUES (?, ?)");
                    $sql3->bindValue(1, $acolhimento_crianca_id);
                    $sql3->bindValue(2, $val);
                    $sql3->execute();
                }
            }
        }

        //ATUALIZANDO O NOME DA FOTO TEMPORARIA
        if (isset($_SESSION['foto_cut']) && isset($_SESSION['foto_origin']) && $_SESSION['foto_cut'] != "" && $_SESSION['foto_origin'] != "") {
            $stmt5 = $db->prepare("UPDATE mod_acolhimento_crianca SET crianca_foto = ? WHERE acolhimento_id = ?");
            $stmt5->bindValue(1, $_SESSION['foto_cut']);
            $stmt5->bindValue(2, $id);
            $stmt5->execute();
        }

        $db->commit();
    } catch (PDOException $e) {
        $db->rollback();
        $msg['msg'] = 'error';
        $msg['retorno'] = "Erro ao tentar realizar a ação desejada:" . $e->getMessage();
        echo json_encode($msg);
        exit();
    }
}
?>